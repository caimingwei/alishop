package com.alipay.rebate.shop.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Date;

public class MDCUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger("scef.transaction.log");

    public static void putUserId(Long userId){
        MDC.put("common.userId",String.valueOf(userId));
    }

    public static void putRequestTime(Date date){
        MDC.put("common.requestTime",String.valueOf(date.getTime()));
    }

    public static void putRequestAddr(String requestAddr){
        MDC.put("tx.requestAddr",requestAddr);
    }

    public static void putDestination(String destination){
        MDC.put("tx.requestAddr",destination);
    }

    public static void putAction(String action){
        MDC.put("tx.action",action);
    }

    public static void putResult(String result){
        MDC.put("tx.result",result);
    }

    public static void putErrorCode(String errorCode){
        MDC.put("tx.errorCode",errorCode);
    }
}
