package com.alipay.rebate.shop.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

public class ReflectUtil {

  public static Map<String,String> buildParaMap(Map<String,String> paraMap, Object request) throws Exception{
    Class clazz = request.getClass();
    Field[] fields = clazz.getDeclaredFields();
    for(Field field: fields){
      field.setAccessible(true);
      String fieldName = field.getName();
      StringBuilder builder = new StringBuilder();
      builder.append("get");
      builder.append(String.valueOf(fieldName.charAt(0)).toUpperCase());
      if(fieldName.length() > 1){
        builder.append(fieldName.substring(1));
      }
      String methodName = builder.toString();
      Method method = clazz.getMethod(methodName);
      Object obj = method.invoke(request);
      if(obj != null){
        paraMap.put(fieldName,obj.toString());
      }
    }
    return paraMap;
  }
}
