package com.alipay.rebate.shop.utils;

import com.alipay.rebate.shop.dao.mapper.JdOrdersMapper;

import com.alipay.rebate.shop.model.JdOrders;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.dingdanxia.DingdanxiaRsp;
import com.alipay.rebate.shop.pojo.jd.SkuInfo;
import com.alipay.rebate.shop.pojo.jd.TkJdOrdersRequest;
import com.alipay.rebate.shop.pojo.jd.TkJdOrdersResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JdUtils {

    private final static String APIKEY = "0TT6fg4Mk4OOmnf2pcaqWAlPhJKiRjbv";
    private final static String KEY = "b57733a7028b090109198ad4a3960e30a2ed1f5e701c2c90aeeabac616c1f8e9f4823b8aee6e09ea";
     
    static ObjectMapper objectMapper = new ObjectMapper();

    private final static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Resource
    static JdOrdersMapper jdOrdersMapper;


    //获取京东订单信息
    public static DingdanxiaRsp<List<TkJdOrdersResponse>> getJdOrders(TkJdOrdersRequest req) throws IOException {
        String url = "http://api.tbk.dingdanxia.com/jd/order_details";
        Map<String, Object> paraMap = new HashMap<>();
        paraMap.put("apikey",APIKEY);
        if(!StringUtils.isEmpty(req.getSignature())){
            paraMap.put("signature",req.getSignature());
        }
        paraMap.put("time",req.getTime());
        if (req.getPageNo() != null){
            paraMap.put("pageNo",req.getPageNo());
        }
        if (req.getPageSize() != null){
            paraMap.put("pageSize",req.getPageSize());
        }
        if (req.getType() != null){
            paraMap.put("type",req.getType());
        }

        paraMap.put("key", KEY);

        String responseBody = HttpUtils.sendGetObject(url, paraMap);
        return objectMapper.readValue(responseBody,new TypeReference<DingdanxiaRsp<List<TkJdOrdersResponse>>>(){});
    }


    //根据下单时间导入订单
    public static List<TkJdOrdersResponse> importOrdersByOrderTime(String time) throws IOException {

        TkJdOrdersRequest request = new TkJdOrdersRequest();
        request.setTime(time);
        request.setPageNo(1);
        request.setPageSize(null);
        request.setType(1);
        DingdanxiaRsp<List<TkJdOrdersResponse>> jdOrders = getJdOrders(request);
        List<TkJdOrdersResponse> ordersResponseList = jdOrders.getData();
        for (TkJdOrdersResponse ordersResponse : ordersResponseList){
            JdOrders orders = buildOrders(ordersResponse);
            //查看订单是否已存在
            Boolean ordersIsExistence = ordersIsExistence(ordersResponse.getOrderId());
            //插入或更新数据库
            saveOrUpdateJdOrders(orders,ordersIsExistence);
        }

        return ordersResponseList;



    }

    //根据完成时间导入订单
    public static List<TkJdOrdersResponse> importOrdersByFinishTime(String time) throws IOException {

        TkJdOrdersRequest request = new TkJdOrdersRequest();
        request.setTime(time);
        request.setPageNo(1);
        request.setPageSize(null);
        request.setType(2);
        DingdanxiaRsp<List<TkJdOrdersResponse>> jdOrders = getJdOrders(request);
        List<TkJdOrdersResponse> ordersResponseList = jdOrders.getData();
        for (TkJdOrdersResponse ordersResponse : ordersResponseList){
            JdOrders orders = buildOrders(ordersResponse);
            //查看订单是否已存在
            Boolean ordersIsExistence = ordersIsExistence(ordersResponse.getOrderId());
            //插入或更新数据库
            saveOrUpdateJdOrders(orders,ordersIsExistence);
        }
        return ordersResponseList;

    }

    //根据更新时间导入订单
    public static List<TkJdOrdersResponse> importOrdersByUpdateTime(String time) throws IOException {

        TkJdOrdersRequest request = new TkJdOrdersRequest();
        request.setTime(time);
        request.setPageNo(1);
        request.setPageSize(null);
        request.setType(3);
        DingdanxiaRsp<List<TkJdOrdersResponse>> jdOrders = getJdOrders(request);
        List<TkJdOrdersResponse> ordersResponseList = jdOrders.getData();

        for (TkJdOrdersResponse ordersResponse : ordersResponseList){
            JdOrders orders = buildOrders(ordersResponse);
            //查看订单是否已存在
            Boolean ordersIsExistence = ordersIsExistence(ordersResponse.getOrderId());
            //插入或更新数据库
            saveOrUpdateJdOrders(orders,ordersIsExistence);
        }
        return ordersResponseList;

    }




    private static Boolean ordersIsExistence(Long orderId){
        //根据订单id查询订单
//        Orders orders = jdOrdersMapper.selectOrderByOrderId(orderId);
//
//        if (orders == null){
//            return true;
//        }

        return false;

    }



    private static JdOrders buildOrders(TkJdOrdersResponse ordersResponse){

        JdOrders order = new JdOrders();
        order.setFinishTime(sdf.format(ordersResponse.getFinishTime()));
        order.setOrderEmt(ordersResponse.getOrderEmt());
        order.setOrderId(ordersResponse.getOrderId());
        order.setOrderTime(sdf.format(ordersResponse.getOrderTime()));
        order.setParentId(ordersResponse.getParentId());
        order.setPayMonth(ordersResponse.getPayMonth());
        order.setPlus(ordersResponse.getPlus());
        order.setPopId(ordersResponse.getPopId());
        order.setUnionId(ordersResponse.getUnionId());
        order.setExt1(ordersResponse.getExt1());
//        order.setValidCode(ordersResponse.getValidCode());
//        List<SkuInfo> skuList = ordersResponse.getSkuList();
//        for (SkuInfo skuInfo : skuList){
//            order.setActualCosPrice(skuInfo.getActualCosPrice());
//            order.setActualFee(skuInfo.getActualFee());
//            order.setCommissionRate(skuInfo.getCommissionRate());
//            order.setEstimateCosPrice(skuInfo.getEstimateCosPrice());
//            order.setEstimateFee(skuInfo.getEstimateFee());
//            order.setFinalRate(skuInfo.getFinalRate());
//            order.setCid1(skuInfo.getCid1());
//            order.setFrozenSkuNum(skuInfo.getFrozenSkuNum());
//            order.setPid(skuInfo.getPid());
//            order.setPositionId(skuInfo.getPositionId());
//            order.setPrice(skuInfo.getPrice());
//            order.setCid2(skuInfo.getCid2());
//            order.setSiteId(skuInfo.getSiteId());
//            order.setSkuId(skuInfo.getSkuId());
//            order.setSkuName(skuInfo.getSkuName());
//            order.setSkuNum(skuInfo.getSkuNum());
//            order.setSkuReturnNum(skuInfo.getSkuReturnNum());
//            order.setSubSideRate(skuInfo.getSubSideRate());
//            order.setSubsidyRate(skuInfo.getSubsidyRate());
//            order.setCid3(skuInfo.getCid3());
//            order.setUnionAlias(skuInfo.getUnionAlias());
//            order.setUnionTag(skuInfo.getUnionTag());
//            order.setUnionTrafficGroup(skuInfo.getUnionTrafficGroup());
//            order.setValidCode(skuInfo.getValidCode());
//            order.setSubUnionId(skuInfo.getSubUnionId());
//            order.setTraceType(skuInfo.getTraceType());
//            order.setSkuinfoPayMonth(skuInfo.getPayMonth());
//            order.setSkuinfoPopId(skuInfo.getPopId());
//            order.setSkuinfoExt1(skuInfo.getExt1());
//            order.setCpActid(skuInfo.getCpActId());
//            order.setUnionRole(skuInfo.getUnionRole());
//        }
//
        return order;
    }


    private static void saveOrUpdateJdOrders(JdOrders orders,Boolean flag){
        //若订单不存在则插入数据库，存在则更新数据库
        if (flag){
            jdOrdersMapper.insert(orders);
        }else{
            jdOrdersMapper.updateByPrimaryKeySelective(orders);
        }

    }









//    public static void main(String[] args) throws IOException {
//
//        String time = DateUtil.getLastHour(LocalDateTime.now());
//        System.out.println(importOrdersByFinishTime(time));
//        System.out.println(importOrdersByOrderTime(time));
//        System.out.println(importOrdersByUpdateTime(time));
//
//    }



}
