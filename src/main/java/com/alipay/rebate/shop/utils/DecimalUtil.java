package com.alipay.rebate.shop.utils;

import java.math.BigDecimal;

public class DecimalUtil {

  public static BigDecimal convertUitToMoney(Long uit){

    BigDecimal decimal = new BigDecimal(uit).
        divide(new BigDecimal(100), 2,BigDecimal.ROUND_HALF_UP);
    return decimal;
  }

  public static Long convertMoneyToUit(BigDecimal money){
    return money.multiply(new BigDecimal(100)).longValue();
  }

  public static boolean biggerThanZero(BigDecimal decimal){
    if(decimal == null) return false;
    return decimal.compareTo(new BigDecimal(0)) > 0;
  }

}
