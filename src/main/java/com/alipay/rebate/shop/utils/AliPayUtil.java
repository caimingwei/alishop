package com.alipay.rebate.shop.utils;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayFundTransToaccountTransferRequest;
import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;
import com.alipay.rebate.shop.model.AlipayOrder;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AliPayUtil {

  private static Logger logger = LoggerFactory.getLogger(AliPayUtil.class);
    final static String privateKey = "MIIEugIBADANBgkqhkiG9w0BAQEFAASCBKQwggSgAgEAAoIBAQCByYFDig12xBY6lDnWKjzIPlPYrnf1VIPK2KclIFM6GxBUI76YeIMyd9Oq+EdGkx+QXI3umswQrHC9d8H4FhSFfnogBMvMkRl/hKYDCFO1Wo5qyxXOwzvAIyDIKqrvbq8UvbIdupjPAMZduuzPoq44BhMtY4rrhTaMbbS5HDP/oIOInNO8X4C6ONIyWO440+jRV0+HzUkVRn/pAnzkh2WC3A1kFVri2K5ADsXNejwmm9rzmrls7urweXCSP2HF+yk9V41hVLfldy5QG2kXGP7vSz+8QtQ9KWFIefxhc/IHH/4F5Gx7imElnVa94tWG+hzwzGp+6qH2ENjmIgTk8GQrAgMBAAECggEAMCUGvX4dpnvw3wQTPF3otENiqRN3TltulDWwMzZ9iSV29i9YSYkS6grHSznUftqer9eVkvh7UIgXPkGJg7T1IdwcA3qj+xusQMB9MMzK+JBCGl2BlnSX2pttUrN5O15sth30T9DjVI6uByELLa7kP8YJISI26008QfWEP9K1e1KN33PeikFtIunHliLL4ZLpVt0z45jrP9x6NTV1zJtFsluJCsmSXXXMKQTFH9p7cigZVc3jDvXdtIR57st9khkDgBhhd+bowunmUMauZV2rNWY9r2RdsdyuBk5PqLbrdaTivFe6SV+2b6H9MlQTz5XFs2LlqQMb6itb/f5WT3D1AQKBgQDmeEdpOHX6Dz4naqCadTQ2C+lfwBoM3on4Dd8xxJXAU4ihMSpWkPvFvcVCbsE1RjtQwFGsU3GYt9//m1Tdsr6ju3fHj4W3y7zLiFlYbcBvsAWQKsaXgyx44bQ0V/AJ6BtPkKEZLf8u8GGBEaMdYPIF1kgHF1/+jthikTjQ0hSTgQKBgQCQKgpmH3RPZCdiN6RBpfZfmG2aLp0qXg8jtNp/fabLFkWlGcwJDtuC/+ceCq9uQWAtODYOXWOe1bqzk2avMK7qWqEXSfGd4xciMP8+ai9erlx46OrIUAdrDMnTSy+kYojM0HMi78L8uKrYkLhUtx8LCMco7l9xspp8fPbjtZxdqwJ/JAKnJcmspWW8UGxYq8mkXs19qXhamagr5pZESYJt458pugX0l5IPxNXh/H9RsQFIQ1XJbSOcnEAev9GFcR58qxifno7OVel/lk23qBl99/WI6P3GCeZ1DG43ufYDi4i4u7XddhG5zZvBoGkhE3YSWtcgafs9BIlNRhlqxzYMAQKBgFGOtxtXKCHR80XpXI+XKG6keT0Eqf1SARaeNg5yM24pR3VDxt5I/X5673FbcivZ6GMsrcgqpTUyjNESzsDFte73HrNiLcK4ulmphnz07Z+O3Nm6SzjPraKuNW/H1UgWRnY4ZyqYoaL/hgfGt8GVZ4aCjMlnMa/M7wEGYVYpZDDJAoGAV55BKLcXu3ZXglpeHe04d8DzskM8OH5nVaIpgf4bsQcYX5hVmhZsRAh3Y7SqML6QJDZtTcJOpIdLtaBHRPOVmjBLqczR1WBj1cLOxaL4MriEKkVrVhMhIWyvSHjujNJmgND/YX1ppVYCosMKmxvsKSSNow7Suva6OcBoaBAXPxs=";
    final static String aliPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsKLWt7OJ+VZbUwDcBFleHPGhyaW+N7gNiiCk+7KM7Xz99+lBdXOm6ui+lQTqBrVI+TR+Z4vqVbliFhNTauvWzIrzGRNYNBZYP6T6SteFe1/PUTvSz3F+nU76QJEEXOE3pYyUIjQzCAed3yGa1yNo0PK9GsLgKvHlD7TcBDWNxm29/X/OPbsgA3dcXrcdNNTaIod4nhMczpR3+b0+6rkkSoGnIQb9MVUl7aJKHZU7YC8Dv7VaFc+yXY1sx4Ggp2F/qnzNwlDokhgdqKQg6UH5oUtICejxeNoZcWcdM7GkN4gcp5YoxnXgyq0nhSay4cVT+m5IY3uPhmFXjcBxP6pZpwIDAQAB";
    final static String appId = "2019040663808190";
    final static String APPID = "2019052365357159";
    final static String PRIVATEKEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCVxmIS0YFkPZEdzomGAcWoeBgSpRJbJSczrg6lqga3rJ4h1MkuuOQcfzBwkHT89/2BC2cQZY6TovSw5A10odUZBYDHXGKx5nlJWDMjq2drqZ+rTAaKfOKeMNIQBd0SXbIuPMylUWlV5O61pOOLvPlgeNQu6rUmnM/PhLhsiSQiRIh+L/9Exfmd7stT5hBTuhX7tYnqZzeZizSn7PhTrj3vrnLOYxJMLN/juhgyQLqfJlJRVqxxkOHtRI0Mfp9fHyxSPOnCkyIXxXtu9K7TqePLmvic+R3akcp/kSZ22bdwUNF0htCMsfY/ncKxil7Uo0Gk3IulXzuvowd3bE5qf9qpAgMBAAECggEAcSi4snUdqImnmXiRiPDP0NAjh/q12q1AaBhqspm6h41g0Di/Y5CHsEwcYf+SlAoduyDNVqMUTNFCxF8ZF3Sp10TEHlvbqYNzQYnk4V9APSSkITzIeFuQsGo8W6TCeXzXAX4oNANFbYMRI6YX0V0OoOfJkET+ZtT1YVlqKX6YEVbEOwEGocG4qUeXPzKGkgWlA7mtVfJ/EuXFXnOGlbjo6WmJlw017zX1LAmY5USKShL2ZFcQyX9h2hVfeo4Yg+J2ux4x8O+r8KVyugBKl79jkpjbbsz/ITRGOEsapbiH3DJ2C6PxF9PiIaX9UHuZUZCaH62e1+OBbFgK0tgMUj0UAQKBgQDIuWmCnbc0KugWJ/vEcBzwkN9Hg4mTzGyOpxvyGsu7jJ5brIZ9AbIBaiSuaUr/XqxDc9FHtlhy/4D63G+TwSvV16DP2+088iRWwJRCzvEzciTP6GPrKd869u88DwjApcy+RdYQFGv8d0dQa0bmIaKavBR63j7KU78W+KUGYqwxgQKBgQC/BSm+z5mmUzPfFPX/aM+zH8NGNtNbNykKAMqLwW0I+YMMc+8Dy6lx6C9ITHM37UcFZwIt+Kqzwa6dD5j8ELZk4UYZwBdO4/N3Nb8hgDiOLMIaarhFzaCo0BUQx3/Taxa67BawGjE2bPmvCWvzGgihkn+JNWzHSaHWk+4hB9htKQKBgDB3FFLFYgbmlUNERDYaG+K6GC9cQ0u8gDmxf73lWmveeB4Kei6UyjYtOs4h/knKwi1HObDnBlQ9/RaVQxuZ+MuZVOrbX1zn2n5X4XSAIx0Y4mqQ4g9V/OBv3zPRLXvWjKkkk5suUySSaFWykVvfozNZVliUcPTJY8pYHwB7srgBAoGBAJKHA167IMHVURTun0UswSZGVySlKbvutCxYLwbFeT5JJ52F0PK4Z2JZGOv8uOVNvg0G10UsWsVI7QjNziyNJVx88akPENITSc9ICzVHhhqJetuIwZHKClN0SrR0VDl5DBaWw1CSxu1Y6q66K3Rvo+9M0UgPE0JL2TkggKoVuo8BAoGBAKf5AL90Pqb9Z07kKHMfOqbrlJYLLlNUtGaVSbINEwdCEjqmO3ssWbUh0wXH4aBItNdQS8qlINwm0YLOj7EjaOY8pjrOXn2G38/gSbnpLlUq5x9OvxCN0OdLs5YqmYWJNd0S4ISX+Ae+lB1kusDTG4rq25VKQ75tJfmCOUjvld50";
    final static String PUBLICKEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlgkYD9vUl0hzOHWNcof46ddiOX4GbXJKu1G6nceWChmS1F5wXGR4GPKS8pYrORshQSOnkrTYSwRXrl8Bg6YBJRbu6VH7/sBJnYYoCsiVCWeynw3WZsssDvg8tOFT0iti40KMHVHAo+6GUTk9ivT0oTikDYCsu3Cl1SOcAvH3UM/vX5Hjn17QsWzlCov1ncFihqMvsFGBnFrk1WH89Q+ASCTRW3O6j1HyZTzHRlqkxTV4+ipVUG5kqT3M/wwHltnMqzTy+2fggXpTxw7c3CtMhrfPc0FviUAgbnfM/7sJgzFJKq34hKVKj/Llqa/jfxsP/RUCRLx0+QsirkL1BBZ/pQIDAQAB";

    public static void main(String[] args) throws AlipayApiException {
//        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",appId,privateKey,"json","gbk",aliPublicKey,"RSA2");
//       AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
//       String str = "{" + "\"out_biz_no\":\"1001038586344301975121\"," + "\"payee_type\":\"ALIPAY_LOGONID\"," + "\"payee_account\":\"413644942@qq.com\"," + "\"amount\":\"0.1\"," + "\"payer_show_name\":\"1\"," + "\"payee_real_name\":\"邓俊如\"," + "\"remark\":\"测试\"" + "}";
//        request.setBizContent(str);
//        System.out.println(str);
        AlipayOrder alipayOrder = new AlipayOrder();
        alipayOrder.setOrderNo("1001038586344301975121");
        alipayOrder.setPayeeAccount("18819258225");
        alipayOrder.setAmount("0.1");
        alipayOrder.setPayeeShowName("1");
        alipayOrder.setPayeeRealName("蔡名炜");
        alipayOrder.setRemark("测试转账");
        AlipayFundTransToaccountTransferResponse response = alipayTransfer(alipayOrder);
        System.out.println(response.getCode());
        System.out.println(response.getMsg());
        System.out.println(response.getOrderId());
        System.out.println(response.getOutBizNo());
        if(response.getCode().equals("10000")){
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }

    public static AlipayFundTransToaccountTransferResponse alipayTransfer(AlipayOrder order) throws AlipayApiException{
        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",APPID,PRIVATEKEY,"json","gbk",PUBLICKEY,"RSA2");
        AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
        String str = "{" + "\"out_biz_no\":\""+order.getOrderNo()+"\"," + "\"payee_type\":\"ALIPAY_LOGONID\"," + "\"payee_account\":\""+order.getPayeeAccount()+"\"," + "\"amount\":\""+order.getAmount()+"\"," + "\"payer_show_name\":\""+order.getPayeeShowName()+"\"," + "\"payee_real_name\":\""+order.getPayeeRealName()+"\"," + "\"remark\":\""+order.getRemark()+"\"" + "}";
        request.setBizContent(str);
        AlipayFundTransToaccountTransferResponse response = alipayClient.execute(request);
        logger.debug("code :{}",response.getCode());
        logger.debug("msg :{}",response.getMsg());
        logger.debug("orderId :{}",response.getOrderId());
        logger.debug("outBizNo :{}",response.getOutBizNo());
        return  response;
    }

    public static String getRandomChar(int length) {            //生成随机字符串
        char[] chr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        Random random = new Random();
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            buffer.append(chr[random.nextInt(10)]);
        }
        return buffer.toString();
    }

}
