package com.alipay.rebate.shop.utils;

import org.apache.shiro.crypto.hash.Sha256Hash;

public class EncryptUtil {

  public static final String encryptSalt = "F12839WhsnnEV$#23b";

  public static String encryptPassword(String plainText){
    return new Sha256Hash(plainText, encryptSalt).toHex();
  }

}
