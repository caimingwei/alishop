package com.alipay.rebate.shop.utils;

import com.alipay.rebate.shop.pojo.pdd.*;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkRpPromUrlGenerateRequest.DiyRedPacketParam;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkRpPromUrlGenerateRequest.DiyRedPacketParamRangeItemsItem;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsSearchRequest.RangeListItem;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.PopHttpClient;
import com.pdd.pop.sdk.http.api.pop.request.*;
import com.pdd.pop.sdk.http.api.pop.response.*;
import com.sun.org.apache.bcel.internal.generic.RETURN;

import java.util.ArrayList;
import java.util.List;

public class PddUtil {

    private static final String CLIENT_ID = "9fb745b0de8045e787ff6eb10c90cbba";
    private static final String CLIENT_SECRET = "38620d2703578514b11484778949327ceefdfc79";

    private static PopClient client = new PopHttpClient(CLIENT_ID, CLIENT_SECRET);

    public static PddDdkThemeGoodsSearchResponse getThemeGoodsSearch(Long theme_id) throws Exception {
        PddDdkThemeGoodsSearchRequest request = new PddDdkThemeGoodsSearchRequest();
        request.setThemeId(theme_id);
        PddDdkThemeGoodsSearchResponse response = client.syncInvoke(request);
        return response;
    }

    public static PddDdkThemeListGetResponse getPddThemeList(Integer page,Integer size) throws Exception {
        PddDdkThemeListGetRequest request = new PddDdkThemeListGetRequest();
        request.setPage(page);
        request.setPageSize(size);
        PddDdkThemeListGetResponse response = client.syncInvoke(request);
        return response;
    }

    public static PddDdkGoodsRecommendGetResponse getPddGoodsRecommend(PddGoodsRecommendReq req) throws Exception {
        PddDdkGoodsRecommendGetRequest request = new PddDdkGoodsRecommendGetRequest();
        request.setChannelType(req.getChannel_type());
        request.setCustomParameters(req.getCustom_parameters());
        request.setLimit(req.getLimit());
        request.setListId(req.getList_id());
        request.setOffset(req.getOffset());
        request.setPid(req.getPid());
        request.setCatId(req.getCat_id());
        request.setGoodsIds(req.getGoods_ids());
        PddDdkGoodsRecommendGetResponse response = client.syncInvoke(request);
        return response;

    }

    public static PddDdkRpPromUrlGenerateResponse getPddRppPomUrlGenerate(PddRppPomUrlGenerateReq req) throws Exception {
        PddDdkRpPromUrlGenerateRequest request = new PddDdkRpPromUrlGenerateRequest();
        request.setChannelType(req.getChannel_type());
        request.setCustomParameters(req.getCustom_parameters());
        DiyRedPacketParam diyRedPacketParam = new DiyRedPacketParam();
        diyRedPacketParam.setAmountProbability(req.getAmount_probability());
        diyRedPacketParam.setDisText(req.getDis_text());
        diyRedPacketParam.setNotShowBackground(req.getNot_show_background());
        diyRedPacketParam.setOptId(req.getOpt_id());
        List<DiyRedPacketParamRangeItemsItem> rangeItems = new ArrayList<>();
        DiyRedPacketParamRangeItemsItem item = new DiyRedPacketParamRangeItemsItem();
        item.setRangeFrom(5l);
        item.setRangeId(1);
        item.setRangeTo(req.getRange_to());
        rangeItems.add(item);
        diyRedPacketParam.setRangeItems(rangeItems);
        request.setDiyRedPacketParam(diyRedPacketParam);
        request.setGenerateQqApp(req.getGenerate_qq_app());
        request.setGenerateSchemaUrl(req.getGenerate_schema_url());
        request.setGenerateShortUrl(req.getGenerate_short_url());
        request.setGenerateWeApp(req.getGenerate_we_app());
        request.setPIdList(req.getP_id_list());
        PddDdkRpPromUrlGenerateResponse response = client.syncInvoke(request);
        return response;

    }

    public static PddDdkResourceUrlGenResponse getPddResourceUrlGen(PddResourceUrlGenReq req) throws Exception {

        PddDdkResourceUrlGenRequest request = new PddDdkResourceUrlGenRequest();
        request.setCustomParameters(req.getCustom_parameters());
        request.setGenerateQqApp(req.getGenerate_qq_app());
        request.setGenerateSchemaUrl(req.getGenerate_schema_url());
        request.setGenerateWeApp(req.getGenerate_we_app());
        request.setPid(req.getPid());
        request.setResourceType(req.getResource_type());
        request.setUrl(req.getUrl());
        PddDdkResourceUrlGenResponse response = client.syncInvoke(request);
        return response;
    }

    public static PddDdkGoodsSearchResponse getPddDdkGoodsSearch(PddDdkGoodsSearchReq req) throws Exception {

        PddDdkGoodsSearchRequest request = new PddDdkGoodsSearchRequest();

        request.setActivityTags(req.getActivity_tags());
        request.setCatId(req.getCat_id());
        request.setCustomParameters(req.getCustom_parameters());

        request.setGoodsIdList(req.getGoods_id_list());
        request.setIsBrandGoods(req.getIs_brand_goods());
        request.setKeyword(req.getKeyword());
        request.setListId(req.getList_id());
        request.setMerchantType(req.getMerchant_type());

        request.setMerchantTypeList(req.getMerchant_type_list());
        request.setOptId(req.getOpt_id());
        request.setPage(req.getPage());
        request.setPageSize(req.getPage_size());
        request.setPid(req.getPid());
        List<RangeListItem> rangeList = new ArrayList<>();
        RangeListItem item = new RangeListItem();
        item.setRangeFrom(1l);
        item.setRangeId(0);
        item.setRangeTo(req.getRange_to());
        rangeList.add(item);
        request.setRangeList(rangeList);
        request.setSortType(req.getSort_type());
        request.setWithCoupon(req.getWith_coupon());
        PddDdkGoodsSearchResponse response = client.syncInvoke(request);
        return response;

    }

    public static PddDdkWeappQrcodeUrlGenResponse getPddDdkWeappQrcodeUrlGen(PddDdkWeappQrcodeUrlGenReq req) throws Exception {
        PddDdkWeappQrcodeUrlGenRequest request = new PddDdkWeappQrcodeUrlGenRequest();
        request.setCustomParameters(req.getCustom_parameters());
        request.setGenerateMallCollectCoupon(req.getGenerate_mall_collect_coupon());
        request.setGoodsIdList(req.getGoods_id_list());
        request.setPId(req.getP_id());
        request.setZsDuoId(req.getZs_duo_id());
        PddDdkWeappQrcodeUrlGenResponse response = client.syncInvoke(request);
        return response;
    }


    public static PddDdkThemePromUrlGenerateResponse getPddDdkThemePromUrlGenerate(PddDdkThemePromUrlGenerateReq req) throws Exception {
        PddDdkThemePromUrlGenerateRequest request = new PddDdkThemePromUrlGenerateRequest();
        request.setCustomParameters(req.getCustom_parameters());
        request.setGenerateMobile(req.getGenerate_mobile());
        request.setGenerateQqApp(req.getGenerate_qq_app());
        request.setGenerateSchemaUrl(req.getGenerate_schema_url());
        request.setGenerateShortUrl(req.getGenerate_short_url());
        request.setGenerateWeappWebview(req.getGenerate_weapp_webview());
        request.setGenerateWeApp(req.getGenerate_we_app());
        request.setPid(req.getPid());
        request.setThemeIdList(req.getTheme_id_list());
        PddDdkThemePromUrlGenerateResponse response = client.syncInvoke(request);
        return response;
    }

    public static PddDdkGoodsZsUnitUrlGenResponse getPddDdkGoodsZsUnitUrlGen(PddDdkGoodsZsUnitUrlGenReq req) throws Exception {
        PddDdkGoodsZsUnitUrlGenRequest request = new PddDdkGoodsZsUnitUrlGenRequest();
        request.setPid(req.getPid());
        request.setSourceUrl(req.getSource_url());
        request.setCustomParameters(req.getCustom_parameters());
        PddDdkGoodsZsUnitUrlGenResponse response = client.syncInvoke(request);
        return response;
    }
}
