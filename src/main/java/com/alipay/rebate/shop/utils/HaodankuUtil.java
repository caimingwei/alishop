package com.alipay.rebate.shop.utils;

import com.alipay.rebate.shop.constants.ProductConstant;
import com.alipay.rebate.shop.model.Product;
import com.alipay.rebate.shop.pojo.haodanku.HaodankuProduct;
import com.alipay.rebate.shop.pojo.haodanku.HaodankuProductListRsp;
import com.alipay.rebate.shop.pojo.pdtframework.ProductChoice;
import com.alipay.rebate.shop.scheduler.HaodankuCidMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HaodankuUtil {

  private static ObjectMapper objectMapper = new ObjectMapper();
  private static Logger logger  = LoggerFactory.getLogger(HaodankuUtil.class);

  private static HaodankuProductListRsp send(String url){
    String body = HttpUtils.doGet(url);
    HaodankuProductListRsp haodankuProductListRsp = null;
    try {
      haodankuProductListRsp = objectMapper.readValue(body, HaodankuProductListRsp.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return haodankuProductListRsp;
  }

  public static HaodankuProductListRsp getAllProducts(String minId){

    String url = "http://v2.api.haodanku.com/update_item/apikey/alphaking/sort/1/back/500/min_id/" + minId;
    return send(url);

  }

  public static HaodankuProductListRsp pullProductsByTime(
      String minId,
      String startTime,
      String endTime
  ){
    String url = "http://v2.api.haodanku.com/timing_items/apikey/alphaking/start/"
        +startTime+"/end/" + endTime+"/min_id/" + minId +"/back/500";
    return send(url);
  }

  public static HaodankuProductListRsp getNewestProducts(String minId){

    String url = "http://v2.api.haodanku.com/update_item/apikey/alphaking/sort/1/back/500/min_id/1" + minId;
    return send(url);

  }

  public static HaodankuProductListRsp getStaleProductsByTime(
      String minId,
      String startTime,
      String endTime
  ) {
    String url = "http://v2.api.haodanku.com/get_down_items/apikey/alphaking/start/"
        +startTime+"/end/" + endTime+"/min_id/" + minId +"/back/500";
    return send(url);
  }

  public static Object getDeserveItem() throws IOException {
    String url = "http://v2.api.haodanku.com/get_deserve_item/apikey/alphaking/start/";
    String body = HttpUtils.doGet(url);
    ObjectMapper objectMapper = new ObjectMapper();
    Object obj = objectMapper.readValue(body,Object.class);
    return obj;
  }

  public static HaodankuProductListRsp selectProductByChoice(
      ProductChoice productChoice,
      Integer minId,
      Integer pageSize
  ){
    StringBuilder builder = new StringBuilder("http://v2.api.haodanku.com/column/apikey/alphaking");
    if(productChoice.getCategory() != null){
      for(Integer category: productChoice.getCategory()){
        builder.append("/cid/");
        builder.append(category);
      }
    }
    if(productChoice.getType() != null){
      builder.append("/type/");
      builder.append(productChoice.getType());
    }
    if(productChoice.getMinAfterPrice() != null){
      builder.append("/price_min/");
      builder.append(productChoice.getMinAfterPrice());
    }
    if(productChoice.getMinAfterPrice() != null){
      builder.append("/price_max/");
      builder.append(productChoice.getMaxAfterPrice());
    }
    if(productChoice.getMinTicket() != null){
      builder.append("/coupon_min/");
      builder.append(productChoice.getMinTicket());
    }
    if(productChoice.getMaxTicket() != null){
      builder.append("/coupon_max/");
      builder.append(productChoice.getMaxTicket());
    }
    if(productChoice.getSell() != null){
      builder.append("/sale_min/");
      builder.append(productChoice.getSell());
    }
    if(productChoice.getSort() != null){
      builder.append("/sort/");
      builder.append(productChoice.getSort());
    }
    builder.append("/min_id/");
    builder.append(minId);
    builder.append("/back/");
    builder.append(pageSize);
    logger.debug("final url is: {}", builder.toString());
    return send(builder.toString());
  }

  public static Object selectTalentInfo(Integer talentcat) throws IOException {
    String url = "http://v2.api.haodanku.com/talent_info/apikey/alphaking/talentcat/"
        + talentcat;
    String body = HttpUtils.doGet(url);
    ObjectMapper objectMapper = new ObjectMapper();
    Object rsp = objectMapper.readValue(body,Object.class);
    return rsp;
  }

  public static Object selectTalentArticle(Integer id) throws IOException {
    String url = "http://v2.api.haodanku.com/talent_article/apikey/alphaking/id/"
        + id;
    String body = HttpUtils.doGet(url);
    ObjectMapper objectMapper = new ObjectMapper();
    Object rsp = objectMapper.readValue(body,Object.class);
    return rsp;
  }

  public static Object selectSalesList(
      Integer sale_type,
      Integer cid,
      Integer min_id,
      Integer back,
      String item_type
      ) throws IOException {

    StringBuilder builder = new StringBuilder();
    builder.append("http://v2.api.haodanku.com/sales_list");
    builder.append("/apikey/");
    builder.append("alphaking");
    if(sale_type != null){
      builder.append("/sale_type/");
      builder.append(sale_type);
    }
    if(cid != null){
      builder.append("/cid/");
      builder.append(cid);
    }
    if(min_id != null){
      builder.append("/min_id/");
      builder.append(min_id);
    }
    if(back != null){
      builder.append("/back/");
      builder.append(back);
    }
    if(item_type != null){
      builder.append("/item_type/");
      builder.append(item_type);
    }
    logger.debug("url is : {}",builder.toString());
    String body = HttpUtils.doGet(builder.toString());
    ObjectMapper objectMapper = new ObjectMapper();
    Object rsp = objectMapper.readValue(body,Object.class);
    return rsp;
  }

  public static Product convert2Product(HaodankuProduct haodankuProduct){
    Product product = new Product();
    product.setGoodsId(Long.valueOf(haodankuProduct.getItemid()));
    product.setItemLink(haodankuProduct.getItempic());
    product.setTitle(haodankuProduct.getItemtitle());
    product.setDtitle(haodankuProduct.getItemshorttitle());
    product.setDescription(haodankuProduct.getItemdesc());
    if(haodankuProduct.getFqcat() != null){
      product.setCid(HaodankuCidMapper.getCid(haodankuProduct.getFqcat()));
    }
//    product.setTbcid(haodankuProduct.getTbcid());
    product.setMainPic(haodankuProduct.getItempic());
//    product.setMarketingMainPic(haodankuProduct.getMarketingMainPic());
    product.setOriginalPrice(haodankuProduct.getItemprice());
    product.setActualPrice(haodankuProduct.getItemendprice());
    product.setDiscounts(haodankuProduct.getDiscount());
//    product.setCommissionType(haodankuProduct.getTktype());
    product.setCommissionRate(haodankuProduct.getTkrates());
    product.setCouponLink(haodankuProduct.getCouponurl());
    product.setCouponTotalNum(haodankuProduct.getCouponnum());
    product.setCouponReceiveNum(haodankuProduct.getCouponreceive2());
    product.setCouponEndTime(DateUtil.getDateTimeStrOfTimestamp(haodankuProduct.getCouponendtime()));
    product.setCouponStartTime(DateUtil.getDateTimeStrOfTimestamp(haodankuProduct.getCouponstarttime()));
    product.setCouponPrice(haodankuProduct.getCouponmoney());
    product.setCouponConditions(haodankuProduct.getCouponexplain());
    product.setMonthSales(haodankuProduct.getItemsale());
    product.setTwoHoursSales(haodankuProduct.getItemsale2());
    product.setDailySales(haodankuProduct.getTodaysale());
    product.setBrand(haodankuProduct.getIs_brand());
//    product.setBrandId(haodankuProduct.getBrandId());
//    product.setBrandName(haodankuProduct.getBrandName());
    product.setCreateTime(DateUtil.getDateTimeStrOfTimestamp(haodankuProduct.getStarttime()));
//    product.setTchaoshi(haodankuProduct.getTchaoshi());
    product.setActivityType(getActivityType(haodankuProduct.getActivity_type()));
    product.setActivityStartTime(DateUtil.getDateTimeStrOfTimestamp(haodankuProduct.getStart_time()));
    product.setActivityEndTime(DateUtil.getDateTimeStrOfTimestamp(haodankuProduct.getEnd_time()));
    product.setShopType(getShopType(haodankuProduct.getShoptype()));
    product.setHaitao(haodankuProduct.getCuntao());
//    product.setGoldSellers(haodankuProduct.getGoldSellers());
//    product.setSellerId(haodankuProduct.getSellerId());
    product.setShopName(haodankuProduct.getShopname());
//    product.setShopLevel(haodankuProduct.getShoptype());
//    product.setDescScore(haodankuProduct.getDescScore());
//    product.setDsrScore(haodankuProduct.getDsrScore());
//    product.setDsrPercent(haodankuProduct.getDsrPercent());
//    product.setShipScore(haodankuProduct.getShipScore());
//    product.setShipPercent(haodankuProduct.getShipPercent());
//    product.setServiceScore(haodankuProduct.getServiceScore());
//    product.setServicePercent(haodankuProduct.getServicePercent());
//    product.setHotPush(haodankuProduct.getHotPush());
//    product.setTeamName(haodankuProduct.getTeamName());
    product.setProductType(ProductConstant.HAODANKU_TYPE);

    return product;
  }

  private static Integer getActivityType(String type){
    if(type == null) return null;
    switch (type){
      case "普通活动":
        return 1;
      case "聚划算":
        return 2;
      case "淘抢购":
        return 3 ;
    }
    return null;
  }

  private static Integer getShopType(String type){
    if(type == null) return null;
    switch (type){
      case "B":
        return 1;
      case "C":
        return 0;
    }
    return null;
  }
}
