package com.alipay.rebate.shop.constants;

public class LuckyMonCouponConstant {

  public static final Integer IS_CURRENT_USE = 1;
  public static final Integer NOT_CURRENT_USE = 0;

  public static final Integer CAN_EDIT = 1;
  public static final Integer CAN_NOT_EDIT = 0;

  public static final Integer CONDITION_TYPE_ORDER_REWARD = 1;
  public static final Integer CONDITION_TYPE_WITHDRAW_MONEY = 2;
  public static final Integer CONDITION_TYPE_PAID_MONEY = 3;

}
