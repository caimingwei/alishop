package com.alipay.rebate.shop.constants;

import com.alipay.rebate.shop.helper.AppSettingsHelper;

public class RedisConstant {

    public static final String APP_ALIAS = AppSettingsHelper.getAppAlias();
    public static final String MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX =APP_ALIAS + "_mobile_code_customer_";
    public static final String MOBILE_CODE_CUSTOMER_ALIPAY_BIND_PREFIX =APP_ALIAS + "_mobile_code_customer_alipay_bind_";
    public static final String MOBILE_CODE_CUSTOMER_ORIGINAL_PHONE_PREFIX = APP_ALIAS + "_mobile_code_customer_original_phone_";
    public static final String MOBILE_CODE_CUSTOMER_NEW_PHONE_PREFIX = APP_ALIAS + "_mobile_code_customer_new_phone_";
    public static final String MOBILE_CODE_CUSTOMER_PASSWORD_PHONE_PREFIX = APP_ALIAS + "_mobile_code_customer_passwordl_phone_";
    public static final int MOBILE_CODE_CUSTOMER_TIME_OUT = 30;
    public static final String MOBILE_CODE_PHONE_BIND_PREFIX = APP_ALIAS + "_mobile_code_phone_bind_";

    public static String getRegisterRedisKey(String phone){
        return  RedisConstant.MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX + phone;
    }
}
