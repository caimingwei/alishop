package com.alipay.rebate.shop.constants;

public class TaskSettingsConstant {

  public static final Integer IS_CURRENT_USE = 1;
  public static final Integer NOT_CURRENT_USE = 0;


  public static final Integer NEW_USER_TASK = 1;
  public static final Integer DAILY_TASK = 2;



  public static final Integer BIND_ALI_ACCOUNT = 11;
  public static final Integer BIND_WECHAT_ACCOUNT = 12;
  public static final Integer TAOBAO_AUTH = 13;
  public static final Integer FIRST_ORDER = 14;
  public static final Integer BROWSE_PRODUCT = 21;

}
