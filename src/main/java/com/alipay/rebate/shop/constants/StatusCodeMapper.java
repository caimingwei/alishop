package com.alipay.rebate.shop.constants;

import java.util.HashMap;
import java.util.Map;

public class StatusCodeMapper {

    private static Map<Integer,String> map = new HashMap<>();

    static {
        map.put(StatusCode.SUCCESS.code,"成功");
        map.put(StatusCode.PHONE_NOT_BIND.code,"手机号没绑定");
        map.put(StatusCode.INTERNAL_ERROR.code,"内部错误");
        map.put(StatusCode.TOKEN_EXPIRE.code,"token过期");
        map.put(StatusCode.TOKEN_PHONE_NOT_RIGHT.code,"token手机号错误");
        map.put(StatusCode.TOKEN_ID_NOT_RIGHT.code,"token Id错误");
        map.put(StatusCode.TOKEN_NOT_VALID.code,"token无效");
        map.put(StatusCode.USER_ALREADY_EXISTS.code,"用户已经存在");
        map.put(StatusCode.MOBILE_CODE_NOT_RIGHT.code,"手机验证码错误");
        map.put(StatusCode.MOBILE_CODE_SEND_ERROR.code,"验证码发送失败");
        map.put(StatusCode.RELATION_SPECIAL_ID_GENERATE_FAILED.code,"relationId 生成失败");
        map.put(StatusCode.PHONE_NOT_EXISTS.code,"手机号不存在");
        map.put(StatusCode.USER_NOT_EXISTS.code,"用户不存在");
        map.put(StatusCode.USER_PASSWORD_NOT_RIGHT.code,"密码错误");
        map.put(StatusCode.WECHAT_FIRST_LOGIN.code,"微信首次登陆");
        map.put(StatusCode.ALIPAY_ACCOUNT_NOT_BIND.code,"支付宝账号没绑定");
        map.put(StatusCode.AMOUNT_NOT_ENOUGTH.code,"余额不足");
        map.put(StatusCode.UIT_NOT_ENOUGTH.code,"集分宝不足");
        map.put(StatusCode.RELATION_ID_SHOULD_NOT_NULL.code,"渠道id不能为空");
        map.put(StatusCode.TAOBAO_USER_ID_SHOULD_NOT_NULL.code,"淘宝用户id不能为空");
        map.put(StatusCode.TAOBAO_USER_NAME_SHOULD_NOT_NULL.code,"淘宝用户昵称不能为空");
        map.put(StatusCode.ACCESS_TOKEN_SHOULE_NOT_NULL.code,"access_token不能为空");
        map.put(StatusCode.PRIVILEGE_LINK_GENERATE_FAIL.code,"转链失败");
        map.put(StatusCode.TPWD_GENERATE_FAIL.code,"生成淘口令失败");
        map.put(StatusCode.REDIS_CONNECT_FAIL.code,"redis连接失败");
        map.put(StatusCode.MYSQL_CONNNECT_FAIL.code,"mysql连接失败");
        map.put(StatusCode.PRODUCT_PAGE_FRAMEWORK_NOT_EXISTS.code,"商品板块不存在");
        map.put(StatusCode.SPECIAL_ID_SHOULD_NOT_NULL.code,"specialId 不能为空");
        map.put(StatusCode.REPEAT_BIND_TAOBAO_ACCOUNT.code,"重复绑定淘宝账号");
        map.put(StatusCode.BIND_PARENT_USER_ERROR.code,"绑定用户关系出错");
        map.put(StatusCode.USER_IN_BLACK_LIST.code,"你已被拉黑");
        map.put(StatusCode.PRODUCT_REPO_NOTEXISTS.code,"商品库不存在");
        map.put(StatusCode.MUTIL_LOGIN.code,"您的账号已在别处登陆");
        map.put(StatusCode.TOKEN_NULL.code,"access_token不能为空");
    }

    public static String getMsg(int code){
        return map.get(code);
    }

}
