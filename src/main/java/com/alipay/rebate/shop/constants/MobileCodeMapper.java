package com.alipay.rebate.shop.constants;

import java.util.HashMap;
import java.util.Map;

public class MobileCodeMapper {

  private static Map<Integer,String> map = new HashMap<>();

  static {
    map.put(0,RedisConstant.MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX);
    map.put(1,RedisConstant.MOBILE_CODE_CUSTOMER_ALIPAY_BIND_PREFIX);
    map.put(2,RedisConstant.MOBILE_CODE_CUSTOMER_ORIGINAL_PHONE_PREFIX);
    map.put(3,RedisConstant.MOBILE_CODE_CUSTOMER_NEW_PHONE_PREFIX);
    map.put(4,RedisConstant.MOBILE_CODE_CUSTOMER_PASSWORD_PHONE_PREFIX);
  }

  public static String getRedisValidCodeKeyPrefixByType(Integer type){
    return map.get(type);
  }

  public static int getRedisValidCodeValuePrefixByKey(String value){
    int key = 0;
    for (Map.Entry<Integer,String> m : map.entrySet()){
      if (m.getValue().equals(value)){
        key = m.getKey();
      }
    }
    return key;
  }

}
