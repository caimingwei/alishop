package com.alipay.rebate.shop.constants;

public class PatternContant {

//    public static final String PHONE_PATTERN = "^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\\d{8}$";
    public static final String PHONE_PATTERN = "^1[3456789]\\d{9}$";
    public static final String IP_PATTERN = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.";
    public static final String URL_PATTERN = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    public static final String PHONE_PATTERN_ERROR_MESSAGE = "手机号码格式错误";
    public static final String PHONE_NULL_MESSAGE = "手机号码不能为空";

    public static final String GOODSID_ERROR_MESSAGE = "商品id不能为空";
    public static final String SESSIONKEY_ERROR_MESSAGE = "token不能为空";

    public static final String TPWD_TEXT_ERROR_MESSAGE = "口令弹框内容不能为空";
    public static final String TPWD_TEXT_RANGE_ERROR_MESSAGE = "长度必须5个字符以上";
    public static final String TPWD_URL_ERROR_MESSAGE = "口令跳转目标页不能为空";
    public static final String TPWD_LOGO_ERROR_MESSAGE = "口令logo不能为空";
}
