package com.alipay.rebate.shop.constants;

public class ActivityProductConstant {

  public static final int UNLIMITED = 0 ;
  public static final int NEW_USER = 1 ;
  public static final int CONDITION = 2 ;
  public static final int TODAY_INVITE = 3 ;

}
