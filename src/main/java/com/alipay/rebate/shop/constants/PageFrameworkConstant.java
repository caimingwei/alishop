package com.alipay.rebate.shop.constants;

public class PageFrameworkConstant {

  public static final Integer IS_CURRENT_USE = 1;
  public static final Integer NOT_CURRENT_USE = 0;
  public static final Integer IS_DEFAULT = 1;
  public static final Integer NOT_DEFAULT = 0;

}
