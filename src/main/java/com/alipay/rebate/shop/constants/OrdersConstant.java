package com.alipay.rebate.shop.constants;

public class OrdersConstant {

  public static final Long PAID_STATUS = 12L;
  public static final Long ACCOUNTING_STATUS = 3L;
  public static final Long OVERDUE_STATUS = 13L;
  public static final Long COMPLETE_STATUS = 14L;
  public static final Integer SCALE = 2;
  public static final Integer REFUND_STATUS = 1;
}
