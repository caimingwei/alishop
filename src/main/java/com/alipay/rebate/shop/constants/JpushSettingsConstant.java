package com.alipay.rebate.shop.constants;

public class JpushSettingsConstant {

  public static final int ORDER_COMMISSION = 1;
  public static final int WITH_SUCCESS = 2;
  public static final int WITH_FAILED = 3;

}
