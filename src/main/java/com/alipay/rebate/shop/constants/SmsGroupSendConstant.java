package com.alipay.rebate.shop.constants;

public class SmsGroupSendConstant {

  public static final int PHONE_OBJ = 0;
  public static final int ALL_USER_OBJ = 1;
  public static final int ACTIVE_USER_OBJ = 2;
  public static final int SLEEP_USER_OBJ = 3;
  public static final int NEW_USER_OBJ = 4;
  public static final int BUY_USER_OBJ = 5;

  public static final int RIGHT_NOW_SEND_TYPE = 0 ;
  public static final int TIMING_SEND_TYPE = 1 ;

  public static final int NOTSTART_STATUS = 0 ;
  public static final int SENDING_STATUS = 1 ;
  public static final int COMPLETE_STATUS = 2 ;

}
