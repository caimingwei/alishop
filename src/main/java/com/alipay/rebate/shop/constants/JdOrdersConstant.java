package com.alipay.rebate.shop.constants;

public class JdOrdersConstant {

    public static final Integer JD_UNPAID_STATUS = 15;
    public static final Integer JD_PAID_STATUS = 16; //已支付
    public static final Integer JD_ACCOUNTING_STATUS = 18; //结算
    public static final Integer JD_SUCCESS_STATUS = 17; //已成功
}
