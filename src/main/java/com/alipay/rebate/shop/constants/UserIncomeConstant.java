package com.alipay.rebate.shop.constants;

public class UserIncomeConstant {

//  public static final int MONEY_AWARD_TYPE = 1 ;
  public static final int ORDERS_AWARD_TYPE = 1 ;
  public static final int BROWSE_AWAED_TYPE = 2 ;
  public static final int INVITE_AWAED_TYPE = 3 ;
  public static final int SIGN_IN_AWAED_TYPE = 4 ;
  public static final int REFUND_UN_AWAED_TYPE = 5 ;
  public static final int LOTTERY_AWAED_TYPE = 6 ;
  public static final int WETCHAT_AUTH_AWAED_TYPE = 7 ;
  public static final int TAOBAO_AUTH_AWAED_TYPE = 8 ;
  public static final int BIND_ALICOUNT_AWAED_TYPE = 9 ;
  public static final int FIRST_DONE_ORDER_AWARD_TYPE = 10 ;
  public static final int LUCKY_MONEY_AWARD_TYPE = 11 ;
  public static final int ADMIN_AWAED_TYPE = 12;
  public static final int WITHDRAW_EXAMINE_TYPE = 13;
  public static final int WITHDRAW_SUCCESS_TYPE = 14;
  public static final int WITHDRAW_REJECT_TYPE = 15;
  public static final int PDD_ORDERS_AWARD_TYPE = 16;
  public static final int JD_ORDERS_AWARD_TYPE = 17;
  public static final int PDD_REFUND_UN_AWARD_TYPE = 18;
  public static final int JD_REFUND_UN_AWARD_TYPE = 19;
  public static final int PUNISH_WORK_ORDER_TYPE = 20;
  public static final int MEITUAN_ORDER_TYPE = 21;
  public static final int MEITUAN_REFUND_ORDER_TYPE = 22;

  public static final int IS_PAID = 1;
  public static final int UN_PAID = 0;
  public static final int FREEZE = 2;

  public static final int MONEY_AWARD_TYPE =1;
  public static final int UIT_AWARD_TYPE = 2;
  public static final int IT_AWARD_TYPE = 3;

  public static final int FREEZING_TYPE = 0;
  public static final int FREEZING_PDD_TYPE = 1;
  public static final int FREEZING_JD_TYPE = 2;
}
