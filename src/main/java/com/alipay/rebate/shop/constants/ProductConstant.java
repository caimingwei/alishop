package com.alipay.rebate.shop.constants;

public class ProductConstant {

  public static final Integer DATAOKE_TYPE = 1 ;
  public static final Integer HAODANKU_TYPE = 2 ;

  public static final String OWN_CHOICE_FRAMEWORK = "1";
  public static final String OFFICIAL_CHOICE_FRAMEWORK = "2";
  public static final String TAOBAO_JINPIN_FRAMEWORK = "3";
  public static final String PRODUCT_CHOICE_FRAMEWORK = "4";

  public static final String DAILY_FREE_PRODUCT_PREFIX = "daily_free_prifix_";

}
