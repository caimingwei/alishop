package com.alipay.rebate.shop.constants;

public class PddOrdersConstant {

    public static final Integer PDD_UNPAID_STATUS = -1;//未支付
    public static final Integer PDD_PAID_STATUS = 0; //已支付
    public static final Integer PDD_ACCOUNTING_STATUS = 5; //结算
    public static final Integer PDD_OVERDUE_STATUS = 4; //审核失败
    public static final Integer PDD_SUCCESS_STATUS = 3; //审核成功
    public static final Integer PDD_CONFIRM_RECEIPT_STATUS = 2; //确认收货
    public static final Integer PDD_CLUSTERING_STATUS = 1;//已成团


}
