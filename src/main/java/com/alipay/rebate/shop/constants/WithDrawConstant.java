package com.alipay.rebate.shop.constants;

public class WithDrawConstant {

    // 提现审核中
    public static final int WITHDRAW_ONGOING = 1;
    // 提现成功
    public static final int WITHDRAW_SUCCESS = 2;
    // 提现驳回
    public static final int WITHDRAW_REJECT = 3;

    // 余额提现
    public static final int WITHDRAW_AMOUNT_TYPE = 0;
    // 集分宝提现
    public static final int WITHDRAW_UIT_TYPE = 1;

}
