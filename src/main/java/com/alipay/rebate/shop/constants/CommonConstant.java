package com.alipay.rebate.shop.constants;

public class CommonConstant {

    public static final String AUTH_HEADER_KEY = "x-auth-token";

    public static final String ACCESS_HEADER_KEY = "access_token";
    public static final String REFRESH_HEADER_KEY = "refresh_token";

    public static final String JWT_SALT = "123asdzxcqwe";

    public static final long JWT_TOKEN_EXPIRE_TIME = 3600 * 24 * 30;

    public static final long ACCESS_TOKEN_EXPIRE_TIME = 3600 * 7 * 24  ;
    public static final long REFRESH_TOKEN_EXPIRE_TIME = 3600 * 14 * 24 ;

    public static final long ACCESS_TOKEN_EXPIRE_TIME_ONEDAY = 3600 * 1 * 24;
    public static final long REFRESH_TOKEN_EXPIRE_TIME_ONEDAY = 3600 * 1 * 24;

}
