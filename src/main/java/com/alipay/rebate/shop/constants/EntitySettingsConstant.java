package com.alipay.rebate.shop.constants;

public class EntitySettingsConstant {

  public static final Integer WITHDRAW_SETTINGS_TYPE = 1;
  public static final Integer INVITENUM_SETTINGS_TYPE = 2;
  public static final Integer INCOME_SETTINGS_TYPE = 3;
  public static final Integer PCF_SETTINGS_TYPE = 4;
  public static final Integer SIGN_IN_SETTINGS_TYPE = 5;

}
