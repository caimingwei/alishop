package com.alipay.rebate.shop.constants;

public class UserContant {

    public static final Integer ADMIN_USER_TYPE = 0;
    public static final Integer CUSTOMER_USER_TYPE = 1;

    public static final String  HEAD_IMG_URL = "http://data.appwangzhan.fltlm.com/headImage.png";

    public static final String UIT_BASE = "100";


    public static final Integer USER_STATUS_NORMAL = 1;
    public static final Integer USER_STATUS_BLACK = 0;
    public static final Integer USER_STATUS_LOGOFF = 2;

    public static final String LOGIN_KEY_PREFIX = "login_";

    public static final String APP_LOGIN_TYPE = "APP";
    public static final String PC_LOGIN_TYPE = "PC";

    public static final Integer IS_AUTH = 1;
    public static final Integer NOT_AUTH = 0;

}
