package com.alipay.rebate.shop.constants;

public class CodeConstant {

    public final static String MOBILE_CODE_CUSTOMER_REGISTER = "注册";
    public final static String MOBILE_CODE_CUSTOMER_LOGIN = "登录";
    public final static String MOBILE_CODE_CUSTOMER_WITHDRAW = "提现";
    public final static String MOBILE_CODE_CUSTOMER_PASSWORD = "修改密码";
    public final static String MOBILE_CODE_CUSTOMER_ALIPAY_ACCOUNT = "绑定支付宝账号";
    public final static String MOBILE_CODE_CUSTOMER_PHONE = "修改手机号";

}
