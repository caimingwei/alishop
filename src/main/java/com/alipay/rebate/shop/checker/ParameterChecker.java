package com.alipay.rebate.shop.checker;

import com.alipay.rebate.shop.constants.ActivityConstant;
import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.pojo.user.customer.H5RegisterRequest;
import com.alipay.rebate.shop.pojo.user.customer.PhoneRegisterRequest;
import com.alipay.rebate.shop.pojo.user.customer.PlusOrReduceVirMoneyReq;
import com.alipay.rebate.shop.pojo.user.customer.UserRequest;
import com.alipay.rebate.shop.pojo.user.customer.WeixinRegisterRequest;
import com.alipay.rebate.shop.pojo.user.product.CheckActivityConditionReq;
import java.math.BigDecimal;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParameterChecker {

  private Logger logger = LoggerFactory.getLogger(ParameterChecker.class);

  private BigDecimal zeroDecimal = new BigDecimal("0.00");

  @Autowired
  Validator validator;

  public  Integer checkAndGetPageNo(Integer pageNo){
    pageNo  = pageNo == null ? 1: pageNo;
    pageNo = pageNo <= 0 ? 1: pageNo;
    return pageNo;
  }

  public Integer checkAndGetPageSize(Integer pageSize){
    pageSize  = pageSize == null ? 20: pageSize;
    pageSize = pageSize <= 0 ? 20: pageSize;
    return pageSize;
  }

  public void checkParams(Object obj){
    Set<ConstraintViolation<Object>>
        constraintViolations = validator.validate(obj);
    if(constraintViolations != null && constraintViolations.size() >0){
      StringBuilder errorInfo = new StringBuilder();
      String errorMessage ;
      for (ConstraintViolation<?> item : constraintViolations) {
        errorInfo.append(item.getMessage()).append(",");
      }
      errorMessage = errorInfo.toString().substring(0, errorInfo.toString().length()-1);
      throw new BusinessException(errorMessage, StatusCode.PARAMETER_CHECK_ERROR);
    }
  }

  public void checkH5Register(UserRequest userRequest){
    H5RegisterRequest h5RegisterRequest = new H5RegisterRequest();
    h5RegisterRequest.setUserPhone(userRequest.getUserPhone());
    h5RegisterRequest.setMobileCode(userRequest.getMobileCode());
    h5RegisterRequest.setParentUserId(userRequest.getParentUserId());
    h5RegisterRequest.setUserNickName(userRequest.getUserPhone());
    h5RegisterRequest.setPassword(userRequest.getPassword());
    h5RegisterRequest.setToken(userRequest.getToken());
    checkParams(h5RegisterRequest);
  }

  public void checkWeixinRegister(UserRequest userRequest){
    WeixinRegisterRequest weixinRegisterRequest = new WeixinRegisterRequest();
    weixinRegisterRequest.setUserPhone(userRequest.getUserPhone());
    weixinRegisterRequest.setOpenId(userRequest.getOpenId());
    weixinRegisterRequest.setMobileCode(userRequest.getMobileCode());
    weixinRegisterRequest.setPassword(userRequest.getPassword());
    weixinRegisterRequest.setUserNickName(userRequest.getUserNickName());
    weixinRegisterRequest.setHeadImageUrl(userRequest.getHeadImageUrl());
    checkParams(weixinRegisterRequest);
  }

  public void checkPhoneRegister(UserRequest userRequest){
    PhoneRegisterRequest phoneRegisterRequest = new PhoneRegisterRequest();
    phoneRegisterRequest.setMobileCode(userRequest.getMobileCode());
    phoneRegisterRequest.setUserPhone(userRequest.getUserPhone());
    phoneRegisterRequest.setPassword(userRequest.getPassword());
    phoneRegisterRequest.setUserNickName(userRequest.getUserNickName());
    checkParams(phoneRegisterRequest);
  }

  public void checkGetPrivilegeLinkAndTpwd(
      String userId,
      String userName,
      String accessToken,
      boolean flag,
      Long relationId,
      Long specialId
  ){
    if(flag){
      if(relationId == null){
        throw new BusinessException(StatusCode.RELATION_ID_SHOULD_NOT_NULL);
      }
//      if(specialId == null){
//        throw new BusinessException(StatusCode.SPECIAL_ID_SHOULD_NOT_NULL);
//      }
    }else{
      checkTaoBaoInfoWhenFlagFalse(userId,userName,accessToken,flag);
    }
  }

  public void checkTaoBaoInfoWhenFlagFalse(
      String userId,
      String userName,
      String accessToken,
      boolean flag
  ){
    if(!flag){
      if(userId == null){
        throw new BusinessException(StatusCode.TAOBAO_USER_ID_SHOULD_NOT_NULL);
      }
      if(userName == null){
        throw new BusinessException(StatusCode.TAOBAO_USER_NAME_SHOULD_NOT_NULL);
      }
      if(accessToken == null){
        throw new BusinessException(StatusCode.ACCESS_TOKEN_SHOULE_NOT_NULL);
      }
    }
  }

  public void checkActivityJson(
      CheckActivityConditionReq req
  ){
    if(req.getActivityId().equals(ActivityConstant.ZERO_PURCHASE_ACTIVITY) &&
        req.getJson() == null){
      throw new BusinessException("条件json不能为空",StatusCode.AVTIVITY_ERROR);
    }
  }

  public void checkPlusOrReduceVirMoneyReq(PlusOrReduceVirMoneyReq req){
    if(req.getType() != UserIncomeConstant.BROWSE_AWAED_TYPE &&
        req.getType() != UserIncomeConstant.LOTTERY_AWAED_TYPE &&
        req.getType() != UserIncomeConstant.SIGN_IN_AWAED_TYPE
    ){
      throw new BusinessException("只允许抽奖或计时奖励",StatusCode.PARAMETER_CHECK_ERROR);
    }
  }
}
