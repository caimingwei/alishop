package com.alipay.rebate.shop.checker;

import com.alipay.rebate.shop.model.ProductPageFramework;
import com.alipay.rebate.shop.pojo.user.admin.ProductPageFrameworkRequest;

public class ProductPageFrameworkConverter {

  public static ProductPageFramework convert2ProductPageFramework(ProductPageFrameworkRequest request){
    ProductPageFramework productPageFramework = new ProductPageFramework();
    productPageFramework.setProductPageId(request.getProductPageId());
    productPageFramework.setPageName(request.getPageName());
    productPageFramework.setClassifyShow(request.getClassifyShow());
    productPageFramework.setShowType(request.getShowType());
    productPageFramework.setShowRebate(request.getShowRebate());
    productPageFramework.setBannerPicPath(request.getBannerPicPath());
    productPageFramework.setIsUse(request.getIsUse());
    productPageFramework.setDataSource(request.getDataSource());
    productPageFramework.setPageCatagory(request.getPageCatagory());
    return productPageFramework;
  }
}
