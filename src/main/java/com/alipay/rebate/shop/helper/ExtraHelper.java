package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.dao.mapper.ExtraMapper;
import com.alipay.rebate.shop.model.Extra;
import com.alipay.rebate.shop.utils.DateUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class ExtraHelper {

    private final Logger logger = LoggerFactory.getLogger(ExtraHelper.class);

    @Autowired
    ExtraMapper extraMapper;

    Gson gson = new GsonBuilder().create();

    public List<Extra> selectExtra(Date theSameDay){
        List<Extra> extras = extraMapper.selectExtra(theSameDay);
        return extras;

    }

    public void checkExtra(String extra){
        logger.debug("extra is : {}",extra);

        try {
            Date nowDate = DateUtil.getStartTimeOfToday();
            logger.debug("nowDate");
            Extra fromJson = gson.fromJson(extra, Extra.class);
            logger.debug("fromJson is : {}", fromJson);

            Extra extra1 = new Extra();

            if ("1.0.0".equals(fromJson.getVersion())) {

                if (fromJson.getComefromparent() != null && fromJson.getComefromchlidren().equals("") && fromJson.getPri().equals("")){
                    noComefromchlidren(nowDate,fromJson);
                }

                if (fromJson.getComefromparent() != null && !fromJson.getComefromchlidren().equals("") && fromJson.getPri().equals("")){
                    hasComefromchlidren(nowDate,fromJson);
                }

                if (fromJson.getComefromparent() != null && !fromJson.getComefromchlidren().equals("") && !fromJson.getPri().equals("")){
                    hasPri(nowDate,fromJson);
                }

            }


        }catch (Exception e){
            logger.error(e.getMessage());
            logger.debug("checkExtra exception is : {}",e.getMessage());
        }


    }

    private void noComefromchlidren(Date nowDate,Extra fromJson){

        Extra selectExtraByTheSameDay = extraMapper.selectNoComefromchlidrenExtra(nowDate, fromJson.getComefromparent());

        logger.debug("noComefromchlidren nowDate is : {}",nowDate);
        logger.debug("noComefromchlidren fromJson is : {}",fromJson);
        Extra extra = new Extra();
        extra.setComefromparent(fromJson.getComefromparent());
        extra.setComefromchlidren(fromJson.getComefromchlidren());
        extra.setTheSameDay(nowDate);
        extra.setVersion(fromJson.getVersion());
        extra.setRemark(fromJson.getRemark());
        extra.setPri(fromJson.getPri());
        String htmlName = compareComefromparentAndComefromchlidren(fromJson.getComefromparent(), fromJson.getComefromchlidren(),fromJson.getPri());
        extra.setHtmlName(htmlName);

        //若记录为空则插入记录
        if (selectExtraByTheSameDay == null){
            extra.setOneDayCount(1l);
            int insertExtra = extraMapper.insertExtra(extra);
            logger.debug("insertExtra is ：{}",insertExtra);
        }else{
            //记录不为空则更新访问次数，与更新时间
            Date udpateTime = new Date();
            int updateOneDayCount = extraMapper.updateNoComefromchlidren(nowDate, udpateTime, extra.getComefromparent());
            logger.debug("updateOneDayCount is : {}",updateOneDayCount);

        }

    }

    private void hasComefromchlidren(Date nowDate,Extra fromJson){

        logger.debug("hasComefromchlidren nowDate is : {}",nowDate);
        logger.debug("hasComefromchlidren fromJson is : {}",fromJson);

        Extra selectHasComefromchlidrenExtra = extraMapper.selectHasComefromchlidrenExtra(nowDate,
                fromJson.getComefromparent(), fromJson.getComefromchlidren());

        Extra extra = new Extra();
        extra.setComefromparent(fromJson.getComefromparent());
        extra.setComefromchlidren(fromJson.getComefromchlidren());
        extra.setTheSameDay(nowDate);
        extra.setVersion(fromJson.getVersion());
        extra.setRemark(fromJson.getRemark());
        extra.setPri(fromJson.getPri());
        String htmlName = compareComefromparentAndComefromchlidren(fromJson.getComefromparent(), fromJson.getComefromchlidren(),fromJson.getPri());
        extra.setHtmlName(htmlName);

        //若记录为空则插入记录
        if (selectHasComefromchlidrenExtra == null){
            extra.setOneDayCount(1l);
            int insertExtra = extraMapper.insertExtra(extra);
            logger.debug("insertExtra is ：{}",insertExtra);
        }else{
            //记录不为空则更新访问次数，与更新时间
            Date udpateTime = new Date();
            int result = extraMapper.updateHasComefromchlidren(nowDate, udpateTime, extra.getComefromparent(), extra.getComefromchlidren());
            logger.debug("hasComefromchlidren result is : {}",result);
        }


    }

    private void hasPri(Date nowDate,Extra fromJson){
        logger.debug("hasPri nowDate is : {}",nowDate);
        logger.debug("hasPri fromJson is : {}",fromJson);
        Extra selectHasPriExtra = extraMapper.selectHasPriExtra(nowDate, fromJson.getComefromparent(),
                fromJson.getComefromchlidren(), fromJson.getPri());
        logger.debug("selectHasPriExtra is : {}",selectHasPriExtra);

        Extra extra = new Extra();
        extra.setComefromparent(fromJson.getComefromparent());
        extra.setComefromchlidren(fromJson.getComefromchlidren());
        extra.setTheSameDay(nowDate);
        extra.setVersion(fromJson.getVersion());
        extra.setRemark(fromJson.getRemark());
        extra.setPri(fromJson.getPri());
        String htmlName = compareComefromparentAndComefromchlidren(fromJson.getComefromparent(), fromJson.getComefromchlidren(),fromJson.getPri());
        extra.setHtmlName(htmlName);
        //若记录为空则插入记录
        if (selectHasPriExtra == null){
            extra.setOneDayCount(1l);
            int insertExtra = extraMapper.insertExtra(extra);
            logger.debug("insertExtra is ：{}",insertExtra);
        }else{
            //记录不为空则更新访问次数，与更新时间
            Date udpateTime = new Date();
            int result = extraMapper.updateHasPriExtra(nowDate, udpateTime, extra.getComefromparent(), extra.getComefromchlidren(),extra.getPri());
            logger.debug("hasPri result is : {}",result);
        }

    }

    private String compareComefromparentAndComefromchlidren(String comefromparent,String comefromchlidren,String pri){

        String htmlName = null;

        switch (comefromparent){
            case "main_home.html" :
                htmlName = "首页第一个bar首页/自定义页面";
                break;
            case "search_result_win.html":

                htmlName = "搜索";

                if (comefromchlidren.equals("0")){
                    htmlName = "搜平台";
                    if (pri.equals("0")){
                        htmlName = "搜平台/正常搜索";
                    }
                    if (pri.equals("0")){
                        htmlName = "搜平台/淘口令搜索";
                    }
                }
                if (comefromchlidren.equals("1")){
                    htmlName = "搜全网";
                    if (pri.equals("0")){
                        htmlName = "搜全网/正常搜索";
                    }
                    if (pri.equals("0")){
                        htmlName = "搜全网/淘口令搜索";
                    }
                }
                if (comefromchlidren.equals("2")){
                    htmlName = "搜京东";
                    if (pri.equals("0")){
                        htmlName = "搜京东/正常搜索";
                    }
                    if (pri.equals("0")){
                        htmlName = "搜京东/淘口令搜索";
                    }
                }
                break;
            case "main_home_nav_frm.html":
                htmlName = "首页导航下的商品";
                break;
            case "main_home_data_frm.html":
                htmlName = "首页导航栏内分类商品";
                break;
            case "main_rush.html":
                htmlName = "首页第三个bar/快抢";
                break;
            case "free_shipping.html":
                htmlName = "9.9包邮";
                break;
            case "saleList.html":
                htmlName = "实时榜单";
                if (comefromchlidren.equals("0")){
                    htmlName = "实时榜单/近两小时";
                }
                if (comefromchlidren.equals("1")){
                    htmlName = "实时榜单/全天榜单";
                }
                break;
            case "commodity_tpl.html":
                htmlName = "商品板块";
                break;
            case "real_time_list.html":
                htmlName = "实时榜单2";
                if (comefromchlidren.equals("0")){
                    htmlName = "实时榜单2/两小时热搜";
                }
                if (comefromchlidren.equals("1")){
                    htmlName = "实时榜单2/今日爆款";
                }
                if (comefromchlidren.equals("2")){
                    htmlName = "实时榜单2/昨日爆款";
                }
                if (comefromchlidren.equals("3")){
                    htmlName = "实时榜单2/出单指数";
                }
                break;
            case "all_share_win0.html":
                htmlName = "实时热销爆款";
                break;
            case "all_share_win1.html":
                htmlName = "大额卷热卖榜";
                break;
            case "brand_sale.html":
                htmlName = "超值大牌";
                if (comefromchlidren.equals("brand_sale_more.html")){
                    htmlName = "超值大牌-品牌专区";
                }
                break;
            case "iteminfo_frm.html":
                htmlName = "商品详情";
                break;
            case "real_time_rush_list.html":
                htmlName = "实时抢疯榜";
                if (comefromchlidren.equals("0")){
                    htmlName = "实时抢疯榜/实时榜";
                }
                if (comefromchlidren.equals("1")){
                    htmlName = "实时抢疯榜/全天榜";
                }
                if (comefromchlidren.equals("2")){
                    htmlName = "实时抢疯榜/热推榜";
                }
                if (comefromchlidren.equals("3")){
                    htmlName = "实时抢疯榜/复购榜";
                }

                break;
            case "saleListPdd.html":
                htmlName = "拼多多实时榜单";
                break;

        }

        return htmlName;

    }



}
