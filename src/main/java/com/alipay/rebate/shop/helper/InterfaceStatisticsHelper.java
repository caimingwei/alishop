package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.dao.mapper.InterfaceStatisticsMapper;
import com.alipay.rebate.shop.model.InterfaceStatistics;
import com.alipay.rebate.shop.pojo.InterfaceStatisticsRsp;
import com.alipay.rebate.shop.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class InterfaceStatisticsHelper {



    @Resource
    InterfaceStatisticsMapper interfaceStatisticsMapper;

    private final Logger logger = LoggerFactory.getLogger(InterfaceStatisticsHelper.class);


    public void getCount(String name){

        long count=1l;
        Date startTimeOfToday = DateUtil.getStartTimeOfToday();
        String nowMinute = DateUtil.getNowMinute();

        logger.debug("nowDate is : {}",startTimeOfToday);

        List<InterfaceStatistics> interfaceStatistics1 = interfaceStatisticsMapper.selectByMinute(nowMinute);
        List<InterfaceStatistics> interfaceStatistics2 = interfaceStatisticsMapper.selectByDay(startTimeOfToday);

        logger.debug("interfaceStatistics1 is : {}",interfaceStatistics1);
        logger.debug("interfaceStatistics2 is : {}",interfaceStatistics2);

        if (interfaceStatistics1.size() < 1){
            int updateByMinute = interfaceStatisticsMapper.updateByMinute(name, nowMinute);
            logger.debug("updateByMinute is : {}",updateByMinute);
        }

        if (interfaceStatistics2.size() < 1){
            int updateByTheSameDay = interfaceStatisticsMapper.updateByTheSameDay(startTimeOfToday);
            logger.debug("updateByTheSameDay is : {}",updateByTheSameDay);
        }
        InterfaceStatistics interfaceStatistics = new InterfaceStatistics();
        interfaceStatistics.setName(name);
        interfaceStatistics.setOneDayCount(count);
        interfaceStatistics.setTheSameDay(startTimeOfToday);
        interfaceStatistics.setOneMinuteCount(count);
        interfaceStatistics.setMinute(nowMinute);
        InterfaceStatistics statistics = interfaceStatisticsMapper.selectByName(name);
        logger.debug("statistics is : {}",statistics);

        if (statistics == null) {
            int result = interfaceStatisticsMapper.insertInterfaceStatics(interfaceStatistics);
            logger.debug("interfacestatisticsMapper result is : {}",result);
        }else{
            logger.debug("interfaceName is : {}",name );
            int result1 = interfaceStatisticsMapper.updateOneMinuteCount(name);
            logger.debug("interfacestatisticsMapper result is : {}",result1);
            int result2 = interfaceStatisticsMapper.updateOneDayCount(name);
            logger.debug("interfacestatisticsMapper result is : {}",result2);
        }


    }




}
