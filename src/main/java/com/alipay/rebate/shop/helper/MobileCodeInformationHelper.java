package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.constants.CodeConstant;
import com.alipay.rebate.shop.dao.mapper.MobileCodeInformationMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.MobileCodeInformation;
import com.alipay.rebate.shop.pojo.mobilecode.MobileCodeStatistical;
import com.alipay.rebate.shop.pojo.mobilecode.SendSmsResponse;
import com.alipay.rebate.shop.utils.DateUtil;
import com.aliyuncs.CommonResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class MobileCodeInformationHelper {

    private Logger logger = LoggerFactory.getLogger(MobileCodeInformationHelper.class);
    @Resource
    private UserMapper userMapper;
    @Resource
    private MobileCodeInformationMapper informationMapper;

    public MobileCodeInformation getMobileCodeInformation(String phone,int type,String mobileCode){

            String username = userMapper.selectUserNameByPhone(phone);
            if (StringUtils.isEmpty(username)){
                //用户名为空，则说明在注册状态,接受人设置问当前的电话号码
                username = phone;
            }
            MobileCodeInformation information = new MobileCodeInformation();
            information.setPhone(phone);
            information.setSendTime(new Date());
            information.setRecipientName(username);
            information.setStatus(1l);
            information.setSendContent(mobileCode);
            switch (type){
                case 0 : information.setType(CodeConstant.MOBILE_CODE_CUSTOMER_LOGIN);break;
                case 1 : information.setType(CodeConstant.MOBILE_CODE_CUSTOMER_ALIPAY_ACCOUNT);break;
                case 2 :
                case 3 : information.setType(CodeConstant.MOBILE_CODE_CUSTOMER_PHONE);break;
                case 4 : information.setType(CodeConstant.MOBILE_CODE_CUSTOMER_PASSWORD);break;
                default: information.setType(CodeConstant.MOBILE_CODE_CUSTOMER_WITHDRAW);break;
            }

            //若存入的用户名为手机号，则为注册状态
            if (phone.equals(information.getRecipientName())){
                information.setType(CodeConstant.MOBILE_CODE_CUSTOMER_REGISTER);
            }
            return information;
    }

    public void insertMobileCodeInformation(SendSmsResponse commonResponse, MobileCodeInformation information){
        logger.debug("commonResponse.getHttpResponse is : {}",commonResponse.getCode());
        if (commonResponse.getCode().equals("OK")){
            logger.debug("sendSms is ok");
            informationMapper.insertMobileCodeInformation(information);
        }else{
            // 短信发送失败
            information.setStatus(2l);
            informationMapper.insertMobileCodeInformation(information);
        }
    }
    public List<MobileCodeStatistical> getMobileCodeRecord(Integer start){
        List<MobileCodeStatistical> mobileCodeStatisticalList = new ArrayList<>();
        for (int i = 0;i <= start; i ++){
            MobileCodeStatistical mobileCodeStatistical = buildMobileCodeStatistical(i);
            mobileCodeStatisticalList.add(mobileCodeStatistical);
        }
        getMobileCodeStatisticalInDb(start,0,mobileCodeStatisticalList);
        logger.debug("mobileCodeStatisticalList is : {}",mobileCodeStatisticalList);

        return mobileCodeStatisticalList;
    }


    private void getMobileCodeStatisticalInDb(Integer start,Integer end,List<MobileCodeStatistical> mobileCodeStatisticalList){
        // 查询近七天的数据
        List<MobileCodeStatistical> sendSmsNum =
                informationMapper.getSendSmsNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> registerSendSmsNum =
                informationMapper.getRegisterSendSmsNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> sendSmsSuccessNum =
                informationMapper.getSendSmsSuccessNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> sendSmsFailNum =
                informationMapper.getSendSmsFailNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> loginSendSmsNum =
                informationMapper.getLoginSendSmsNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> updatePasswordSendSmsNum =
                informationMapper.getUpdatePasswordSendSmsNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> updatePhoneSendSmsNum =
                informationMapper.getUpdatePhoneSendSmsNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> updateAlipayAccountSendSmsNum =
                informationMapper.getUpdateAlipayAccountSendSmsNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));
        List<MobileCodeStatistical> sendWithDrawSmsNum =
                informationMapper.getSendWithDrawSmsNum(DateUtil.getDaysBefore(start), DateUtil.getDaysBefore(end));

        setMobileCodeStatistical(mobileCodeStatisticalList,sendSmsNum,registerSendSmsNum,
                sendSmsSuccessNum,sendSmsFailNum,loginSendSmsNum,updatePasswordSendSmsNum,
                updatePhoneSendSmsNum,updateAlipayAccountSendSmsNum,sendWithDrawSmsNum);
    }

    private void setMobileCodeStatistical(List<MobileCodeStatistical> mobileCodeStatisticalList,
                                          List<MobileCodeStatistical> sendSmsNum,
                                          List<MobileCodeStatistical> registerSendSmsNum,
                                          List<MobileCodeStatistical> sendSmsSuccessNum,
                                          List<MobileCodeStatistical> sendSmsFailNum,
                                          List<MobileCodeStatistical> loginSendSmsNum,
                                          List<MobileCodeStatistical> updatePasswordSendSmsNum,
                                          List<MobileCodeStatistical> updatePhoneSendSmsNum,
                                          List<MobileCodeStatistical> updateAlipayAccountSendSmsNum,
                                          List<MobileCodeStatistical> sendWithDrawSmsNum){
        for (MobileCodeStatistical mobileCodeStatistical : mobileCodeStatisticalList) {
            for (MobileCodeStatistical codeStatistical : sendSmsNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setSendSmsNum(codeStatistical.getSendSmsNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : registerSendSmsNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setRegisterSendSmsNum(codeStatistical.getRegisterSendSmsNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : sendSmsFailNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setSendSmsFailNum(codeStatistical.getSendSmsFailNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : sendSmsSuccessNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setSendSmsSuccessNum(codeStatistical.getSendSmsSuccessNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : loginSendSmsNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setLoginSendSmsNum(codeStatistical.getLoginSendSmsNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : updatePasswordSendSmsNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setUpdatePasswordSendSmsNum(codeStatistical.getUpdatePasswordSendSmsNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : updateAlipayAccountSendSmsNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setUpdateAlipayAccountSendSmsNum(codeStatistical.getUpdateAlipayAccountSendSmsNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : updatePhoneSendSmsNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setUpdatePhoneSendSmsNum(codeStatistical.getUpdatePhoneSendSmsNum());
                }
            }
            for (MobileCodeStatistical codeStatistical : sendWithDrawSmsNum) {
                if (codeStatistical.getDate().equals(mobileCodeStatistical.getDate())){
                    mobileCodeStatistical.setSendWithDrawSmsNum(codeStatistical.getSendWithDrawSmsNum());
                }
            }
        }
    }

    private MobileCodeStatistical buildMobileCodeStatistical(int date){
        MobileCodeStatistical mobileCodeStatistical = new MobileCodeStatistical();
        mobileCodeStatistical.setSendWithDrawSmsNum(0);
        mobileCodeStatistical.setLoginSendSmsNum(0);
        mobileCodeStatistical.setRegisterSendSmsNum(0);
        mobileCodeStatistical.setSendSmsFailNum(0);
        mobileCodeStatistical.setSendSmsNum(0);
        mobileCodeStatistical.setSendSmsSuccessNum(0);
        mobileCodeStatistical.setUpdateAlipayAccountSendSmsNum(0);
        mobileCodeStatistical.setUpdatePasswordSendSmsNum(0);
        mobileCodeStatistical.setUpdatePhoneSendSmsNum(0);
        mobileCodeStatistical.setDate(DateUtil.getDaysBefore(date));
        return mobileCodeStatistical;
    }
}
