package com.alipay.rebate.shop.helper;

import com.alibaba.fastjson.JSON;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.dao.mapper.UserStatisticalMapper;
import com.alipay.rebate.shop.model.UserStatistical;
import com.alipay.rebate.shop.pojo.user.admin.AuthNum;
import com.alipay.rebate.shop.pojo.user.admin.PhoneType;
import com.alipay.rebate.shop.service.admin.UserStatisticalService;
import com.alipay.rebate.shop.service.admin.impl.UserStatisticalServiceImpl;
import com.alipay.rebate.shop.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class UserStatisticalHelper {

    private Logger logger = LoggerFactory.getLogger(UserStatisticalHelper.class);

    @Resource
    private UserStatisticalMapper userStatisticalMapper;
    @Resource
    private UserMapper userMapper;

    public int addOrUpdateUserStatistical() {

        logger.debug("addOrUpdateUserStatistical start");
        int beforeAgeDayRegisterUser = userMapper.getBeforeAgeDayRegisterUser(DateUtil.getAgeDayDate());
        int beforeAgeDayRegisterUserAndHasParent = userMapper.getBeforeAgeDayRegisterUserAndHasParent(DateUtil.getAgeDayDate());
        int beforeAgeDayRegisterUserAndNoParent = userMapper.getBeforeAgeDayRegisterUserAndNoParent(DateUtil.getAgeDayDate());
        int beforeThirtyDaysRegisterUser = userMapper.getBeforeThirtyDaysRegisterUser(DateUtil.getBeforeThirtyDayDate());
        int beforeThirtyDaysRegisterUserAndHasParent = userMapper.getBeforeThirtyDaysRegisterUserAndHasParent(DateUtil.getBeforeThirtyDayDate());
        int beforeThirtyDaysRegisterUserAndNoParent = userMapper.getBeforeThirtyDaysRegisterUserAndNoParent(DateUtil.getBeforeThirtyDayDate());
        int hasParentUserBeforeThirtyDayLoginNum =
                userMapper.getHasParentUserThirtyDayLoginNum(DateUtil.getThirtyDayDate(), DateUtil.getTodayDate(),DateUtil.getAgeDayDate());
        int noParentUserBeforeThirtyDayLoginNum =
                userMapper.getNoParentUserThirtyDayLoginNum(DateUtil.getThirtyDayDate(), DateUtil.getTodayDate(),DateUtil.getBeforeThirtyDayDate());
        int hasParentUserAgeDayLoginNum =
                userMapper.getHasParentUserSevenDayLoginNum(DateUtil.getSevenDayDate(), DateUtil.getTodayDate(),DateUtil.getAgeDayDate());
        int noParentUserAgeDayLoginNum =
                userMapper.getNoParentUserSevenDayLoginNum(DateUtil.getSevenDayDate(), DateUtil.getTodayDate(),DateUtil.getBeforeThirtyDayDate());
        int hasParentAuthNum = userMapper.getHasParentAuthNum();
        int noParentAuthNum = userMapper.getNoParentAuthNum();
        int noParentButHasOrdersNum = userMapper.getNoParentButHasOrdersNum();
        int hasParentAndOrdersNum = userMapper.getHasParentAndOrdersNum();
        int todayRegisterUser = userMapper.getTodayRegisterUser();
        int todayRegisterUserAndHasParent = userMapper.getTodayRegisterUserAndHasParent();
        int todayRegisterUserAndNoParent = userMapper.getTodayRegisterUserAndNoParent();
        int hasParentUserLoginNum = userMapper.getHasParentUserLoginNum(DateUtil.getYesterday());
        int noParentUserLoginNum = userMapper.getNoParentUserLoginNum(DateUtil.getYesterday());
        int yesterdayUserLoginNum = hasParentUserLoginNum + noParentUserLoginNum;
        int yesterdayRegisterHasParentNum = userMapper.getYesterdayRegisterHasParentNum(DateUtil.getYesterday());
        int yesterdayRegisterNoParentNum = userMapper.getYesterdayRegisterNoParentNum(DateUtil.getYesterday());

        logger.debug("beforeAgeDayRegisterUser is : {}",beforeAgeDayRegisterUser);
        logger.debug("beforeAgeDayRegisterUserAndHasParent is : {}",beforeAgeDayRegisterUserAndHasParent);
        logger.debug("beforeAgeDayRegisterUserAndNoParent is : {}",beforeAgeDayRegisterUserAndNoParent);
        logger.debug("beforeThirtyDaysRegisterUser is : {}",beforeThirtyDaysRegisterUser);
        logger.debug("beforeThirtyDaysRegisterUserAndHasParent is : {}",beforeThirtyDaysRegisterUserAndHasParent);
        logger.debug("beforeThirtyDaysRegisterUserAndNoParent is : {}",beforeThirtyDaysRegisterUserAndNoParent);
        logger.debug("hasParentAuthNum is : {}",hasParentAuthNum);
        logger.debug("noParentAuthNum is : {}",noParentAuthNum);
        logger.debug("noParentButHasOrdersNum is : {}",noParentButHasOrdersNum);
        logger.debug("hasParentAndOrdersNum is : {}",hasParentAndOrdersNum);
        logger.debug("todayRegisterUser is : {}",todayRegisterUser);
        logger.debug("todayRegisterUserAndHasParent is : {}",todayRegisterUserAndHasParent);
        logger.debug("todayRegisterUserAndNoParent is : {}",todayRegisterUserAndNoParent);
        logger.debug("hasParentUserLoginNum is : {}",hasParentUserLoginNum);
        logger.debug("noParentUserLoginNum is : {}",noParentUserLoginNum);
        logger.debug("hasParentUserBeforeThirtyDayLoginNum is : {}",hasParentUserBeforeThirtyDayLoginNum);
        logger.debug("noParentUserBeforeThirtyDayLoginNum is : {}",noParentUserBeforeThirtyDayLoginNum);
        logger.debug("hasParentUserAgeDayLoginNum is : {}",hasParentUserAgeDayLoginNum);
        logger.debug("noParentUserAgeDayLoginNum is : {}",noParentUserAgeDayLoginNum);

        BigDecimal userTotal = new BigDecimal(todayRegisterUser);
        BigDecimal hasParent = new BigDecimal(todayRegisterUserAndHasParent);
        BigDecimal noParent = new BigDecimal(todayRegisterUserAndNoParent);
        BigDecimal hasParentAndOrders = new BigDecimal(hasParentAndOrdersNum);
        BigDecimal noParentButHasOrders = new BigDecimal(noParentButHasOrdersNum);
        BigDecimal hasParentUserLogin = new BigDecimal(hasParentUserLoginNum);
        BigDecimal noParentUserLogin = new BigDecimal(noParentUserLoginNum);
        BigDecimal hasParentUserBeforeThirtyDayLogin = new BigDecimal(hasParentUserBeforeThirtyDayLoginNum);
        BigDecimal noParentUserBeforeThirtyDayLogin = new BigDecimal(noParentUserBeforeThirtyDayLoginNum);
        BigDecimal hasParentUserAgeDayLogin = new BigDecimal(hasParentUserAgeDayLoginNum);
        BigDecimal noParentUserAgeDayLogin = new BigDecimal(noParentUserAgeDayLoginNum);
        BigDecimal ageDayRegisterUser = new BigDecimal(beforeAgeDayRegisterUser);
        BigDecimal thirtyDaysRegisterUser = new BigDecimal(beforeThirtyDaysRegisterUser);
        BigDecimal yesterdayRegisterHasParent = new BigDecimal(yesterdayRegisterHasParentNum);
        BigDecimal yesterdayRegisterNoParent = new BigDecimal(yesterdayRegisterNoParentNum);
        BigDecimal yesterdayUserLogin = new BigDecimal(yesterdayUserLoginNum);

        BigDecimal hasParentScale = null;
        BigDecimal noParentScale = null;
        BigDecimal hasParentAndOrdersScale = null;
        BigDecimal noParentButHasOrdersScale = null;
        BigDecimal hasParentActive = null;
        BigDecimal noParentActive = null;
        BigDecimal sevenDayHasParentActive = null;
        BigDecimal sevenDayNoParentActive = null;
        BigDecimal thirtyDayHasParentActive = null;
        BigDecimal thirtyDayNoParentActive = null;

        if (todayRegisterUser != 0) {

            if (todayRegisterUserAndHasParent != 0) {
                hasParentScale = hasParent.divide(userTotal,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (todayRegisterUserAndNoParent != 0) {
                noParentScale = noParent.divide(userTotal,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (hasParentAndOrdersNum != 0){
                hasParentAndOrdersScale = hasParentAndOrders.divide(userTotal,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (noParentButHasOrdersNum != 0){
                noParentButHasOrdersScale = noParentButHasOrders.divide(userTotal,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (hasParentUserLoginNum != 0){
                hasParentActive = yesterdayRegisterHasParent.divide(yesterdayUserLogin,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (noParentUserLoginNum != 0){
                noParentActive = yesterdayRegisterNoParent.divide(yesterdayUserLogin,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (hasParentUserAgeDayLoginNum != 0 && beforeAgeDayRegisterUser != 0){
                sevenDayHasParentActive = hasParentUserAgeDayLogin.divide(ageDayRegisterUser,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (noParentUserAgeDayLoginNum != 0 && beforeAgeDayRegisterUser != 0){
                sevenDayNoParentActive = noParentUserAgeDayLogin.divide(ageDayRegisterUser,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (noParentUserBeforeThirtyDayLoginNum != 0 && beforeThirtyDaysRegisterUser != 0){
                thirtyDayNoParentActive = noParentUserBeforeThirtyDayLogin.divide(thirtyDaysRegisterUser,2,BigDecimal.ROUND_HALF_DOWN);
            }
            if (hasParentUserBeforeThirtyDayLoginNum != 0 && beforeThirtyDaysRegisterUser != 0){
                thirtyDayHasParentActive = hasParentUserBeforeThirtyDayLogin.divide(thirtyDaysRegisterUser,2,BigDecimal.ROUND_HALF_DOWN);
            }

        }
        logger.debug("hasParentScale is : {}",hasParentScale);
        logger.debug("noParentScale is : {}",noParentScale);
        logger.debug("hasParentAndOrdersScale is : {}",hasParentAndOrdersScale);
        logger.debug("noParentButHasOrdersScale is : {}",noParentButHasOrdersScale);
        logger.debug("hasParentActive is : {}",hasParentActive);
        logger.debug("noParentActive is : {}",noParentActive);
        logger.debug("sevenDayHasParentActive is : {}",sevenDayHasParentActive);
        logger.debug("sevenDayNoParentActive is : {}",sevenDayNoParentActive);
        logger.debug("thirtyDayHasParentActive is : {}",thirtyDayHasParentActive);
        logger.debug("thirtyDayNoParentActive is : {}",thirtyDayNoParentActive);

        if(hasParentScale == null){
            hasParentScale = new BigDecimal("0.00");
        }
        if (noParentScale == null){
            noParentScale = new BigDecimal("0.00");
        }
        if (hasParentAndOrdersScale == null){
            hasParentAndOrdersScale = new BigDecimal("0.00");
        }
        if (noParentButHasOrdersScale == null){
            noParentButHasOrdersScale = new BigDecimal("0.00");
        }
        if (hasParentActive == null){
            hasParentActive = new BigDecimal("0.00");
        }
        if (noParentActive == null){
            noParentActive = new BigDecimal("0.00");
        }
        if (sevenDayHasParentActive == null){
            sevenDayHasParentActive = new BigDecimal("0.00");
        }
        if (sevenDayNoParentActive == null){
            sevenDayNoParentActive = new BigDecimal("0.00");
        }
        if (thirtyDayHasParentActive == null){
            thirtyDayHasParentActive = new BigDecimal("0.00");
        }
        if (thirtyDayNoParentActive == null){
            thirtyDayNoParentActive = new BigDecimal("0.00");
        }
//        List<PhoneType> deviceModel = userMapper.getDeviceModel();
//        int deviceModelIsNull = userMapper.getDeviceModelIsNull();
//        for (PhoneType type : deviceModel){
//            if (type.getPhoneNum() == 0){
//                type.setPhoneNum(deviceModelIsNull);
//                type.setDeviceModel("未知");
//                BigDecimal phoneNum = new BigDecimal(type.getPhoneNum());
//                BigDecimal active = phoneNum.divide(userTotal,2,BigDecimal.ROUND_HALF_DOWN);
//                type.setActive(active);
//            }
//            BigDecimal phoneNum = new BigDecimal(type.getPhoneNum());
//            BigDecimal active = phoneNum.divide(userTotal,2,BigDecimal.ROUND_HALF_DOWN);
//            type.setActive(active);
//        }
        List<PhoneType> deviceModelTwo = userMapper.getDeviceModelTwo();
        logger.debug("deviceModelTwo is : {}",deviceModelTwo);
        if (deviceModelTwo.size() > 0) {
            for (PhoneType type : deviceModelTwo) {
                if (userTotal.compareTo(new BigDecimal("0")) == 1) {
                    BigDecimal phoneNum = new BigDecimal(type.getPhoneNum());
                    BigDecimal active = phoneNum.divide(userTotal, 2, BigDecimal.ROUND_HALF_DOWN);
                    type.setActive(active);
                }
            }
        }
        String jsonString = JSON.toJSONString(deviceModelTwo);
        logger.debug("jsonString is : {}",jsonString);
        UserStatistical userStatistical = new UserStatistical();
        userStatistical.setRegisterNum(todayRegisterUser);
        int totalAuthNum = hasParentAuthNum + noParentAuthNum;
        userStatistical.setAuthNum(0);

        String authRatio;
        if (totalAuthNum != 0 && todayRegisterUser != 0){
            BigDecimal authNum = new BigDecimal(totalAuthNum);
            BigDecimal registerUser = new BigDecimal(todayRegisterUser);
            BigDecimal ratio = authNum.divide(registerUser,4,BigDecimal.ROUND_HALF_DOWN);
            BigDecimal bigDecimal = new BigDecimal("100");
            BigDecimal multiply = bigDecimal.multiply(ratio).setScale(2);
            authRatio = multiply+"%";
        }else{
            authRatio = "0%";
        }
        userStatistical.setAuthRatio(authRatio);
        userStatistical.setHasParentAuthNum(hasParentAuthNum);
        userStatistical.setNoParentAuthNum(noParentAuthNum);
        userStatistical.setHasParentAndOrdersNum(hasParentAndOrdersNum);
        userStatistical.setNoParentButHasOrdersNum(noParentButHasOrdersNum);
        userStatistical.setHasParentUserNum(todayRegisterUserAndHasParent);
        userStatistical.setNoParentUserNum(todayRegisterUserAndNoParent);
        userStatistical.setHasParentUserScale(hasParentScale.setScale(2));
        userStatistical.setNoParentUserScale(noParentScale.setScale(2));
        userStatistical.setHasParentAndOrdersScale(hasParentAndOrdersScale.setScale(2));
        userStatistical.setNoParentButHasOrdersScale(noParentButHasOrdersScale.setScale(2));
        userStatistical.setHasParentActive(hasParentActive.setScale(2));
        userStatistical.setNoParentActive(noParentActive.setScale(2));
        userStatistical.setIntoServenDayHasParentActive(sevenDayHasParentActive.setScale(2));
        userStatistical.setIntoServenDayNoParentActive(sevenDayNoParentActive.setScale(2));
        userStatistical.setIntoThirtyDayHasParentActive(thirtyDayHasParentActive.setScale(2));
        userStatistical.setIntoThirtyDayNoParentActive(thirtyDayNoParentActive.setScale(2));
        userStatistical.setDifferentPhoneUserScale(jsonString);
        userStatistical.setUpdateTime(new Date());
        String createTimeDate = userStatisticalMapper.getCreateTimeDate();
        logger.debug("createTimeDate is : {}",createTimeDate);
        int result = 0;
        if (createTimeDate == null){
            logger.debug("insert into");
            userStatistical.setCreateTime(new Date());
            result = userStatisticalMapper.insert(userStatistical);
        }
        if (createTimeDate != null){
            logger.debug("update into");
            result = userStatisticalMapper.updateStatisticalByDate(userStatistical);
        }

        List<AuthNum> userRegisterByAuth = userMapper.getUserRegisterByAuth();
        List<String> createTime = userStatisticalMapper.getCreateTime();
        logger.debug("userRegisterByAuth is : {}",userRegisterByAuth);
        if (userRegisterByAuth.size() > 0) {
            for (AuthNum authNum : userRegisterByAuth) {
                for (String s : createTime) {
                    if (authNum.getDate().equals(s)) {
                        userStatisticalMapper.updateUserStatisticalByCreateTime(authNum.getAuthNum(), authNum.getDate());
                    }
                }
            }
        }

        return result;
    }

//    public static void main(String[] args) {
//        BigDecimal bigDecimal = new BigDecimal("3");
//        BigDecimal bigDecimal1 = new BigDecimal("1");
//        BigDecimal bigDecimal2 = bigDecimal1.divide(bigDecimal,4,BigDecimal.ROUND_HALF_DOWN);
//        BigDecimal bigDecimal3 = new BigDecimal("100");
//        BigDecimal multiply = bigDecimal3.multiply(bigDecimal2).setScale(2);
//        System.out.println(bigDecimal2);
//        System.out.println(multiply);
//    }

}
