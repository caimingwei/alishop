package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.model.CommissionLevelSettings;
import com.alipay.rebate.shop.model.UserGrade;
import com.alipay.rebate.shop.pojo.usergrade.UserGradeRsp;
import java.util.List;

public class UserGradeBuilder {

  public static UserGradeRsp convert2UserGradeRsp(
      UserGrade req, List<CommissionLevelSettings> settings){
    UserGradeRsp userGradeRsp = new UserGradeRsp();
    userGradeRsp.setId(req.getId());
    userGradeRsp.setGradeName(req.getGradeName());
    userGradeRsp.setGradeWeight(req.getGradeWeight());
    userGradeRsp.setFirstCommission(req.getFirstCommission());
    userGradeRsp.setSecondCommission(req.getSecondCommission());
    userGradeRsp.setThirdCommission(req.getThirdCommission());
    userGradeRsp.setEnableCmlevelSettings(req.getEnableCmlevelSettings());
    userGradeRsp.setCreateTime(req.getCreateTime());
    userGradeRsp.setUpdateTime(req.getUpdateTime());
    userGradeRsp.setLevelSettings(settings);
    return userGradeRsp;
  }
}
