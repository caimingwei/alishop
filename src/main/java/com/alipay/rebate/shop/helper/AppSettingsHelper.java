package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.controller.admin.AppSettingsController;
import com.alipay.rebate.shop.dao.mapper.AppSettingsMapper;
import com.alipay.rebate.shop.model.AppSettings;
import com.alipay.rebate.shop.pojo.jpush.JpushAuthMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AppSettingsHelper {

  private static final Logger logger = LoggerFactory.getLogger(AppSettingsHelper.class);


  @Autowired
  static  AppSettingsMapper appSettingsMapper;

  public static String getActivityAdzoneId(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String activityPid = appSettings.getActivityPid();
    String adzoneId = activityPid.substring(activityPid.lastIndexOf("_") + 1 );
    return adzoneId;
  }

  public static String getPid(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    return appSettings.getPid();
  }

  public static String getActivityPid(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    return appSettings.getActivityPid();
  }

  public static String getAppKey(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    return appSettings.getTaobaoAppKey();
  }

  public static JpushAuthMsg getJpushKeyAndSecret(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    JpushAuthMsg authMsg = new JpushAuthMsg();
    authMsg.setKey(appSettings.getJpushKey());
    authMsg.setSecret(appSettings.getJpushSecret());
    return authMsg;
  }

  public static AppSettings getAppSettings(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    return appSettings;
  }

  public static String getSmsAccesskeyId(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String smsAccesskeyId = appSettings.getSmsAccesskeyId();
    logger.debug("smsAccesskeyId is : {}",smsAccesskeyId);
    return smsAccesskeyId;
  }

  public static String getSmsSecret(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String smsSecret = appSettings.getSmsSecret();
    logger.debug("smsSecret is : {}",smsSecret);
    return smsSecret;
  }

  public static Long getUnionId(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    Long unionId = appSettings.getUnionId();
    logger.debug("unionId is : {}",unionId);
    return unionId;
  }

  public static String getJdKey(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String key = appSettings.getJdKey();
    logger.debug("key is : {}",key);
    return key;
  }

  public static Long getSiteId(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    Long siteId = appSettings.getSiteId();
    logger.debug("siteId is : {}",siteId);
    return siteId;
  }

  public static String getDataokeAppSecret(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String dataokeAppSecret = appSettings.getDataokeAppSecret();
    return dataokeAppSecret;
  }

  public static String getDataokeAppKey(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String dataokeAppKey = appSettings.getDataokeAppKey();
    return dataokeAppKey;
  }

  public static String getAppAlias(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String appAlias = appSettings.getAppAlias();
    return appAlias;
  }

  public static String getTbAuthId(){
    AppSettingsMapper appSettingsMapper = SpringBeanHelper.getAppSettingsMapper();
    AppSettings appSettings = appSettingsMapper.selectByPrimaryKey(1);
    String tbAuthId = appSettings.getTbAuthId();
    return tbAuthId;
  }

}
