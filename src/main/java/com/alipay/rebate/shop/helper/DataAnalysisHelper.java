package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.controller.admin.AdminOrdersController;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.pojo.dataanalysis.DataAnalysisRsp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Component
public class DataAnalysisHelper {


    private final Logger logger = LoggerFactory.getLogger(DataAnalysisHelper.class);

    @Resource
    private OrdersMapper ordersMapper;

    public DataAnalysisRsp getAllDataAnalysis(){

        DataAnalysisRsp rsp = new DataAnalysisRsp();
        Map<String,Object> map = new HashMap<>();
        int allSelfPurchaseOrderNum = ordersMapper.getAllSelfPurchaseOrderNum();
        int allSelfPurchaseOrderNumToday = ordersMapper.getAllSelfPurchaseOrderNumToday();
        int allShareOrderNum = ordersMapper.getAllShareOrderNum();
        int allShareOrderNumToday = ordersMapper.getAllShareOrderNumToday();
        BigDecimal allSelfCommissionNum = ordersMapper.getAllSelfCommissionNum();
        BigDecimal allSelfCommissionNumToday = ordersMapper.getAllSelfCommissionNumToday();
        BigDecimal allShareCommissionNum = ordersMapper.getAllShareCommissionNum();
        BigDecimal allShareCommissionNumToday = ordersMapper.getAllShareCommissionNumToday();
        map.put("allSelfPurchaseOrderNum",allSelfPurchaseOrderNum);
        map.put("allSelfPurchaseOrderNumToday",allSelfPurchaseOrderNumToday);
        map.put("allShareOrderNum",allShareOrderNum);
        map.put("allShareOrderNumToday",allShareOrderNumToday);
        map.put("allSelfCommissionNum",allSelfCommissionNum);
        map.put("allSelfCommissionNumToday",allSelfCommissionNumToday);
        map.put("allShareCommissionNum",allShareCommissionNum);
        map.put("allShareCommissionNumToday",allShareCommissionNumToday);
        logger.debug("this map is : {}",map);
        rsp.setBuyByYourself(map);

        return rsp;
    }

}
