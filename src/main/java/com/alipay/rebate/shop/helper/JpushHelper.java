package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.constants.JpushSettingsConstant;
import com.alipay.rebate.shop.dao.mapper.JpushSettingsMapper;
import com.alipay.rebate.shop.dao.mapper.MobileCodeInformationMapper;
import com.alipay.rebate.shop.model.JpushSettings;
import com.alipay.rebate.shop.model.MobileCodeInformation;
import com.alipay.rebate.shop.utils.JpushUtils;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class JpushHelper {

  @Resource
  private JpushSettingsMapper jpushSettingsMapper;
  @Resource
  private MobileCodeInformationHelper mobileCodeInformationHelper;
  @Resource
  private MobileCodeInformationMapper mobileCodeInformationMapper;

  public void orderCommissionPush(String alia){
    List<String> alias = new ArrayList<>();
    alias.add(alia);
    orderCommissionPush(alias);
  }

  public void withDrawSuccessPush(String alia){
    List<String> alias = new ArrayList<>();
    alias.add(alia);
    withDrawSuccessPush(alias);
  }

  public void withDrawFailedPush(String alia){
    List<String> alias = new ArrayList<>();
    alias.add(alia);
    withDrawFailedPush(alias);
  }

  public void withDrawFailedPush(String alia, JpushSettings jpushSettings){
    List<String> alias = new ArrayList<>();
    alias.add(alia);
    withDrawFailedPush(alias,jpushSettings);
  }

  public void orderCommissionPush(List<String> alias){
    // 推送返利消息给用户
    push(JpushSettingsConstant.ORDER_COMMISSION,alias);
  }

  public void withDrawSuccessPush(List<String> alias){
    // 推送提现成功消息
    push(JpushSettingsConstant.WITH_SUCCESS,alias);
  }

  public void withDrawFailedPush(List<String> alias){
    // 推送提现失败
    push(JpushSettingsConstant.WITH_FAILED,alias);
  }

  public void withDrawFailedPush(List<String> alias, JpushSettings jpushSettings){
    // 推送提现失败
    push(jpushSettings,alias);
  }

  public void push(Integer type, List<String> alias){
    JpushSettings jpushSettings = jpushSettingsMapper.selectByType(type);
    if(jpushSettings != null && jpushSettings.getAppEnable() == 1 ){
      JpushUtils.sendPushWithCallback(jpushSettings,alias);
    }
  }

  public void push(JpushSettings jpushSettings, List<String> alias){
    if(jpushSettings != null && jpushSettings.getAppEnable() == 1 ){
      JpushUtils.sendPushWithCallback(jpushSettings,alias);
    }
  }
}
