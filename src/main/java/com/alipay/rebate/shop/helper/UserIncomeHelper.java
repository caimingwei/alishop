package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.constants.JdOrdersConstant;
import com.alipay.rebate.shop.constants.OrdersConstant;
import com.alipay.rebate.shop.constants.PddOrdersConstant;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.model.JdOrders;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.utils.DateUtil;
import java.math.BigDecimal;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UserIncomeHelper {

  private Logger logger = LoggerFactory.getLogger(UserIncomeHelper.class);

  @Resource
  private UserIncomeMapper userIncomeMapper;

  public UserIncome inserOrderCommissionIncome(
      Orders orders,
      BigDecimal money,
      Long userId,
      Integer roadType,
      boolean freezeCommission
      ){
    UserIncome userIncome = new UserIncome();
    userIncome.setRelationId(orders.getRelationId());
    userIncome.setSpecialId(orders.getSpecialId());
    userIncome.setCreateTime(orders.getCreateTime());
    userIncome.setUpdateTime(DateUtil.getNowStr());
    if(OrdersConstant.PAID_STATUS.equals(orders.getTkStatus())){
      userIncome.setStatus(UserIncomeConstant.UN_PAID);
    }
    if(OrdersConstant.ACCOUNTING_STATUS.equals(orders.getTkStatus())){
      userIncome.setStatus(UserIncomeConstant.IS_PAID);
      if(freezeCommission){
        userIncome.setStatus(UserIncomeConstant.FREEZE);
      }
    }
    if(OrdersConstant.COMPLETE_STATUS.equals(orders.getTkStatus())){
      userIncome.setStatus(UserIncomeConstant.IS_PAID);
      if(freezeCommission){
        userIncome.setStatus(UserIncomeConstant.FREEZE);
      }
    }
    userIncome.setRoadType(roadType);
    userIncome.setTradeId(orders.getTradeId());
    userIncome.setParentTradeId(orders.getTradeParentId());
    userIncome.setMoney(money);
    userIncome.setTkStatus(orders.getTkStatus());
    userIncome.setUserId(userId);
    userIncome.setType(UserIncomeConstant.ORDERS_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    logger.debug("UserIncome is : {}",userIncome);

    userIncomeMapper.insertSelective(userIncome);
    return userIncome;
  }


  public UserIncome insertPddOrderCommissionIncome(
          PddOrders orders,
          BigDecimal money,
          Long userId,
          Integer roadType,
          boolean freezeCommission
  ){
    UserIncome userIncome = new UserIncome();
//    userIncome.setRelationId(orders.getRelationId());
//    userIncome.setSpecialId(orders.getSpecialId());
    userIncome.setCreateTime(orders.getOrderCreateTime());
    userIncome.setUpdateTime(DateUtil.getNowStr());
    if(PddOrdersConstant.PDD_UNPAID_STATUS.equals(orders.getOrderStatus()) ||
            PddOrdersConstant.PDD_PAID_STATUS.equals(orders.getOrderStatus()) ||
            PddOrdersConstant.PDD_CLUSTERING_STATUS.equals(orders.getOrderStatus()) ||
            PddOrdersConstant.PDD_CONFIRM_RECEIPT_STATUS.equals(orders.getOrderStatus())){
      userIncome.setStatus(UserIncomeConstant.UN_PAID);
    }
    if(PddOrdersConstant.PDD_ACCOUNTING_STATUS.equals(orders.getOrderStatus())){
      userIncome.setStatus(UserIncomeConstant.IS_PAID);
      if(freezeCommission){
        userIncome.setStatus(UserIncomeConstant.FREEZE);
      }
    }
    userIncome.setRoadType(roadType);
    userIncome.setTradeId(orders.getOrderSn());
    userIncome.setMoney(money);
    Integer orderStatus = orders.getOrderStatus();
    long status = orderStatus.longValue();
    userIncome.setTkStatus(status);
    userIncome.setUserId(userId);
    userIncome.setParentTradeId(orders.getOrderSn());
    userIncome.setType(UserIncomeConstant.PDD_ORDERS_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    logger.debug("UserIncome is : {}",userIncome);

    userIncomeMapper.insertSelective(userIncome);
    return userIncome;
  }


  public UserIncome insertJdOrderCommissionIncome(
          JdOrders orders,
          BigDecimal money,
          Long userId,
          Integer roadType,
          boolean freezeCommission
  ){
    UserIncome userIncome = new UserIncome();
//    userIncome.setRelationId(orders.getRelationId());
//    userIncome.setSpecialId(orders.getSpecialId());
    userIncome.setCreateTime(orders.getOrderTime());
    userIncome.setUpdateTime(DateUtil.getNowStr());
    if(JdOrdersConstant.JD_UNPAID_STATUS.equals(orders.getValidCode()) ||
            JdOrdersConstant.JD_PAID_STATUS.equals(orders.getValidCode()) ||
            JdOrdersConstant.JD_SUCCESS_STATUS.equals(orders.getValidCode())){
      userIncome.setStatus(UserIncomeConstant.UN_PAID);
    }
    if(JdOrdersConstant.JD_ACCOUNTING_STATUS == orders.getValidCode()){
      userIncome.setStatus(UserIncomeConstant.IS_PAID);
      if(freezeCommission){
        userIncome.setStatus(UserIncomeConstant.FREEZE);
      }
    }
    userIncome.setRoadType(roadType);
    userIncome.setTradeId(orders.getOrderId().toString());
    userIncome.setParentTradeId(orders.getParentId().toString());
    userIncome.setMoney(money);
    Integer orderStatus = orders.getValidCode();
    long status = orderStatus.longValue();
    userIncome.setTkStatus(status);
    userIncome.setUserId(userId);
    userIncome.setType(UserIncomeConstant.JD_ORDERS_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    logger.debug("UserIncome is : {}",userIncome);

    userIncomeMapper.insertSelective(userIncome);
    return userIncome;
  }


  public void updateUserIncome(Long userId,int type){
    try {
      //查询用户收益表是否存在收益记录
      UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(userId, UserIncomeConstant.WITHDRAW_EXAMINE_TYPE);
      logger.debug("userIncome is : {}",userIncome);
      //存在则更新用户收益记录
      if (userIncome != null) {
        if (type == 2){
          logger.debug("update user_income");
          userIncomeMapper.updateUserIncomeByUserIdAndType(userId, UserIncomeConstant.WITHDRAW_EXAMINE_TYPE);
        }
        if (type == 3) {
          logger.debug("update user_income");
          userIncomeMapper.updateUserIncome(userId, UserIncomeConstant.WITHDRAW_EXAMINE_TYPE);
        }
      }

    }catch (Exception e){
      e.printStackTrace();
    }
  }

}
