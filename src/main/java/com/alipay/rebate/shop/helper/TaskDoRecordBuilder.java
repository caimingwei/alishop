package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.model.TaskDoRecord;
import java.util.Date;

public class TaskDoRecordBuilder {

  public static TaskDoRecord buildTaskDoRecord(
      Integer type,
      Integer subType,
      Long userId
  ){
    TaskDoRecord taskDoRecord = new TaskDoRecord();
    taskDoRecord.setType(type);
    taskDoRecord.setSubType(subType);
    taskDoRecord.setUserId(userId);
    Date date = new Date();
    taskDoRecord.setCreateTime(date);
    taskDoRecord.setUpdateTime(date);
    return taskDoRecord;
  }
}
