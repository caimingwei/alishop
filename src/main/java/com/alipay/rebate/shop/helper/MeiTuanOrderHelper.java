package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.dao.mapper.MeituanOrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.UserIncome;
import io.lettuce.core.dynamic.annotation.CommandNaming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

@Component
public class MeiTuanOrderHelper {

    @Resource
    private UserIncomeMapper userIncomeMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private MeituanOrdersMapper meituanOrdersMapper;
    private Logger logger = LoggerFactory.getLogger(MeiTuanOrderHelper.class);

    @Transactional
    public void handleLastMonthCompletionMeituanOrder(){
        LocalDate localDate = LocalDate.now();
        //上个月第一天
        LocalDate lastMonth = LocalDate.of(localDate.getYear(), localDate.getMonthValue() - 1, 1);
        //上个月的最后一天
        LocalDate lastDay = lastMonth.with(TemporalAdjusters.lastDayOfMonth());
        List<UserIncome> userIncomeList = userIncomeMapper.selectMeiTuanByCreateTime(lastDay.toString());
        for (UserIncome userIncome : userIncomeList) {
            BigDecimal multiply = userIncome.getMoney().multiply(new BigDecimal("100"));
            userMapper.plusUserUit(multiply.longValue(),userIncome.getUserId());
            userIncomeMapper.updateMeiTuanUserIncome(userIncome.getUserIncomeId());
        }
    }

}
