package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.dao.mapper.IncomeCommissionFreezeLevelMapper;
import com.alipay.rebate.shop.model.IncomeCommissionFreezeLevel;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class IncomeCommissionFreezeLevelHelper {

  @Resource
  private IncomeCommissionFreezeLevelMapper incomeCommissionFreezeLevelMapper;

  public void insert(
      Long incomeId,
      Integer levelId
  ){
    IncomeCommissionFreezeLevel incomeCommissionFreezeLevel =
        new IncomeCommissionFreezeLevel();
    incomeCommissionFreezeLevel.setUserIncomeId(incomeId);
    incomeCommissionFreezeLevel.setFreezeLevelId(levelId);
    incomeCommissionFreezeLevelMapper.insertSelective(incomeCommissionFreezeLevel);
  }

}
