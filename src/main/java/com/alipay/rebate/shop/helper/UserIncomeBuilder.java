package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.model.*;
import com.alipay.rebate.shop.utils.DateUtil;
import com.alipay.rebate.shop.utils.DecimalUtil;
import java.math.BigDecimal;

public class UserIncomeBuilder {

  public static UserIncome buildUitPaidUserIncome(
      Orders orders,
      BigDecimal money,
      Integer type,
      Long userId,
      Integer isPaid
      ){
    UserIncome nUserIncome = new UserIncome();
    nUserIncome.setUserId(userId);
    nUserIncome.setParentTradeId(orders.getTradeParentId());
    nUserIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    nUserIncome.setCreateTime(orders.getCreateTime());
    nUserIncome.setUpdateTime(DateUtil.getNowStr());
    nUserIncome.setSpecialId(orders.getSpecialId());
    nUserIncome.setRelationId(orders.getRelationId());
    nUserIncome.setType(type);
    nUserIncome.setMoney(money);
    nUserIncome.setStatus(isPaid);
    nUserIncome.setTkStatus(orders.getTkStatus());
    nUserIncome.setTradeId(orders.getTradeId());
    return nUserIncome;
  }

  public static UserIncome buildNotOrdersRelateUserIncome(
      Integer type,
      Integer awardType,
      Long num,
      Long userId
  ){
    UserIncome userIncome = new UserIncome();
    userIncome.setUserId(userId);
    switch (awardType){
      case UserIncomeConstant.MONEY_AWARD_TYPE:
        userIncome.setMoney(new BigDecimal(num));
        break;
      case UserIncomeConstant.UIT_AWARD_TYPE:
        userIncome.setMoney(DecimalUtil.convertUitToMoney(num));
        break;
      case UserIncomeConstant.IT_AWARD_TYPE:
        userIncome.setIntgeralNum(num);
        break;
    }
    userIncome.setType(type);
    userIncome.setAwardType(awardType);
    userIncome.setStatus(UserIncomeConstant.IS_PAID);
    String date = DateUtil.getNowStr();
    userIncome.setCreateTime(date);
    userIncome.setUpdateTime(date);
    return userIncome;
  }

  public static UserIncome buildLuckyMoneyUserIncome(
      BigDecimal money,
      Long userId
  ){
    UserIncome userIncome = new UserIncome();
    userIncome.setUserId(userId);
    userIncome.setMoney(money);
    userIncome.setStatus(UserIncomeConstant.IS_PAID);
    userIncome.setType(UserIncomeConstant.LUCKY_MONEY_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.MONEY_AWARD_TYPE);
    String date = DateUtil.getNowStr();
    userIncome.setCreateTime(date);
    userIncome.setUpdateTime(date);
    return userIncome;
  }

  public static UserIncome buildBrowseProductIncome(
      Long userId,
      Long uit,
      Long integral,
      Integer awardType
      ){
    UserIncome userIncome = new UserIncome();
    userIncome.setStatus(UserIncomeConstant.IS_PAID);
    userIncome.setUserId(userId);
    String nowStr = DateUtil.getNowStr();
    userIncome.setCreateTime(nowStr);
    userIncome.setUpdateTime(nowStr);
    userIncome.setType(UserIncomeConstant.BROWSE_AWAED_TYPE);
    if(uit != null){
      BigDecimal money = DecimalUtil.convertUitToMoney(uit);
      userIncome.setMoney(money);
    }
    if(integral != null){
      userIncome.setIntgeralNum(integral);
    }
    userIncome.setAwardType(awardType);
    return userIncome;
  }

  public static UserIncome buildInviteUserIncome(Orders orders,Long awardNum,Long parentUserId,Integer rodaType){
    UserIncome userIncome = new UserIncome();
    userIncome.setRelationId(orders.getRelationId());
    userIncome.setSpecialId(orders.getSpecialId());
    userIncome.setCreateTime(orders.getCreateTime());
    userIncome.setTradeId(orders.getTradeId());
    userIncome.setStatus(UserIncomeConstant.IS_PAID);
    BigDecimal money = DecimalUtil.convertUitToMoney(awardNum);
    userIncome.setMoney(money);
    userIncome.setTkStatus(orders.getTkStatus());
    userIncome.setUserId(parentUserId);
    userIncome.setType(UserIncomeConstant.INVITE_AWAED_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    userIncome.setParentTradeId(orders.getTradeParentId());
    userIncome.setUpdateTime(DateUtil.getNowStr());
    userIncome.setRoadType(rodaType);
    return userIncome;
  }

  public static UserIncome buildPddUserIncome(
          PddOrders orders,
          BigDecimal money,
          Integer type,
          Long userId,
          Integer isPaid
  ){
    UserIncome nUserIncome = new UserIncome();
    nUserIncome.setUserId(userId);
    nUserIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    nUserIncome.setCreateTime(orders.getOrderCreateTime());
    nUserIncome.setUpdateTime(DateUtil.getNowStr());
//    nUserIncome.setSpecialId(orders.getSpecialId());
//    nUserIncome.setRelationId(orders.getRelationId());
    nUserIncome.setType(type);
    nUserIncome.setMoney(money);
    nUserIncome.setStatus(isPaid);
    nUserIncome.setTkStatus(orders.getOrderStatus().longValue());
    nUserIncome.setTradeId(orders.getOrderSn());
    nUserIncome.setParentTradeId(orders.getOrderSn());
    return nUserIncome;
  }

  public static UserIncome buildPddInviteUserIncome(PddOrders orders,Long awardNum,Long parentUserId,Integer roadType){
    UserIncome userIncome = new UserIncome();
//    userIncome.setRelationId(orders.getRelationId());
//    userIncome.setSpecialId(orders.getSpecialId());
    userIncome.setCreateTime(orders.getOrderCreateTime());
    userIncome.setTradeId(orders.getOrderSn());
    userIncome.setStatus(UserIncomeConstant.IS_PAID);
    BigDecimal money = DecimalUtil.convertUitToMoney(awardNum);
    userIncome.setMoney(money);
    userIncome.setTkStatus(orders.getOrderStatus().longValue());
    userIncome.setUserId(parentUserId);
    userIncome.setType(UserIncomeConstant.IT_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    userIncome.setParentTradeId(orders.getOrderSn());
    userIncome.setUpdateTime(DateUtil.getNowStr());
    userIncome.setRoadType(roadType);
    userIncome.setParentTradeId(orders.getOrderSn());
    return userIncome;
  }


  public static UserIncome buildJdUserIncome(
          JdOrders orders,
          BigDecimal money,
          Integer type,
          Long userId,
          Integer isPaid
  ){
    UserIncome nUserIncome = new UserIncome();
    nUserIncome.setUserId(userId);
    nUserIncome.setParentTradeId(orders.getParentId().toString());
    nUserIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    nUserIncome.setCreateTime(orders.getOrderTime());
    nUserIncome.setUpdateTime(DateUtil.getNowStr());
//    nUserIncome.setSpecialId(orders.getSpecialId());
//    nUserIncome.setRelationId(orders.getRelationId());
    nUserIncome.setType(type);
    nUserIncome.setMoney(money);
    nUserIncome.setStatus(isPaid);
    nUserIncome.setParentTradeId(orders.getOrderId().toString());
    nUserIncome.setTkStatus(orders.getValidCode().longValue());
    nUserIncome.setTradeId(orders.getOrderId().toString());
    nUserIncome.setParentTradeId(orders.getParentId().toString());
    return nUserIncome;
  }


  public static UserIncome buildJdInviteUserIncome(JdOrders orders, Long awardNum, Long parentUserId,Integer roadType){
    UserIncome userIncome = new UserIncome();
    userIncome.setCreateTime(orders.getOrderTime());
    userIncome.setTradeId(orders.getOrderId().toString());
    userIncome.setStatus(UserIncomeConstant.IS_PAID);
    BigDecimal money = DecimalUtil.convertUitToMoney(awardNum);
    userIncome.setMoney(money);
    userIncome.setTkStatus(orders.getValidCode().longValue());
    userIncome.setUserId(parentUserId);
    userIncome.setType(UserIncomeConstant.IT_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    userIncome.setParentTradeId(orders.getParentId().toString());
    userIncome.setUpdateTime(DateUtil.getNowStr());
    userIncome.setParentTradeId(orders.getOrderId().toString());
    userIncome.setRoadType(roadType);
    userIncome.setParentTradeId(orders.getParentId().toString());
    return userIncome;
  }

  public static UserIncome buildMeiTuanOrdersUserIncome(MeituanOrders orders, Long awardNum, Long userId, Integer roadType){
    UserIncome userIncome = new UserIncome();
    userIncome.setCreateTime(orders.getPayTime());
    userIncome.setTradeId(orders.getOrderId());
    userIncome.setParentTradeId(orders.getOrderId());
    userIncome.setStatus(UserIncomeConstant.UN_PAID);
    BigDecimal money = DecimalUtil.convertUitToMoney(awardNum);
    userIncome.setMoney(money);
    userIncome.setUserId(userId);
    userIncome.setType(UserIncomeConstant.MEITUAN_ORDER_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    userIncome.setUpdateTime(DateUtil.getNowStr());
    userIncome.setRoadType(roadType);
    return userIncome;
  }



}
