package com.alipay.rebate.shop.helper;

import com.alibaba.fastjson.JSON;
import com.alipay.rebate.shop.pojo.dingdanxia.PddGoodsDetailReq;
import com.alipay.rebate.shop.pojo.historyprice.GoodInfo;
import com.alipay.rebate.shop.pojo.historyprice.HistoryPriceRsp;
import com.alipay.rebate.shop.pojo.historyprice.ResultResp;
import com.alipay.rebate.shop.pojo.pdd.GoodsPicture;
import com.alipay.rebate.shop.utils.DingdanxiaUtil;
import com.alipay.rebate.shop.utils.HttpUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class HistoryPriceHelper {

    private Logger logger = LoggerFactory.getLogger(HistoryPriceHelper.class);
    private Gson gson = new Gson();
    private ObjectMapper objectMapper = new ObjectMapper();

//    public ResultResp getHistoryPrice(Long id, Integer type) throws IOException {
//        String productUrl = null;
//        GoodsPicture goodsPicture = null;
//        StringBuilder url = new StringBuilder();
//
//        switch (type){
//            case 0 :
//                // 天猫
//                productUrl = "https://detail.tmall.com/item.htm?id="+id;
////                productUrl + "&_t=" + id;
//                break;
//            case 1 :
//                // 京东
//                productUrl = "http://item.jd.com/"+id+".html";
////                url = "https://api.yhmai.cn/router?v=1&api=yhmai.query.history&" + "url=" + productUrl + "&_t=" + id;
//                Object queryGoodsPromotioninfo = DingdanxiaUtil.getQueryGoodsPromotioninfo(null, id.toString());
//                logger.debug("queryGoodsPromotioninfo is : {}",queryGoodsPromotioninfo);
//                String json = JSON.toJSONString(queryGoodsPromotioninfo);
//                logger.debug("json is : {}",json);
//                String substring1 = json.substring(json.indexOf("{",json.indexOf("{")+1),json.lastIndexOf("}")-1);
//                logger.debug("substring1 is : {}",substring1);
//                goodsPicture = gson.fromJson(substring1, GoodsPicture.class);
//                break;
//            case 2 :
//                // 淘宝
//                productUrl = "https://item.taobao.com/item.htm?id="+id;
////                url = "https://api.yhmai.cn/router?v=1&api=yhmai.query.history&" + "url=" + productUrl + "&_t=" + id;
//                break;
//            case 3 :
//                //拼多多
//                productUrl = "https://mobile.yangkeduo.com/goods.html?goods_id="+id;
////                url = "https://api.yhmai.cn/router?v=1&api=yhmai.query.history&" + "url=" + productUrl + "&_t=" + id;
//                PddGoodsDetailReq req = new PddGoodsDetailReq();
//                logger.debug("id is : {}",id);
//                req.setGoods_id_list(id);
//                Object pddGoodesDetail = DingdanxiaUtil.getPddGoodesDetail(req);
//                String jsonString = JSON.toJSONString(pddGoodesDetail);
//                logger.debug("jsonString is : {}",jsonString);
//                String substring2 = jsonString.substring(jsonString.indexOf("{", jsonString.indexOf("{")+1), jsonString.lastIndexOf("}") - 1);
//                goodsPicture = gson.fromJson(substring2, GoodsPicture.class);
//                logger.debug("pddGoodsDetail is : {}", goodsPicture);
//                break;
//        }
//        url.append("https://api.yhmai.cn/router?v=1&api=yhmai.query.history&url=");
//        url.append(productUrl);
//        url.append("&_t=");
//        url.append(id);
//        String sendGet = HttpUtils.sendGet(url.toString());
//        ResultResp resultResp = gson.fromJson(sendGet, ResultResp.class);
//        logger.debug("resultResp is : {}",resultResp);
//        HistoryPriceRsp data = resultResp.getData();
//        GoodInfo goodInfo = data.getGoodInfo();
//        switch (type) {
//            case 3:
//                goodInfo.setGoodPicture(goodsPicture.getGoods_thumbnail_url());
//                break;
//            case 1 :
//                goodInfo.setGoodPicture(goodsPicture.getImgUrl());
//                break;
//        }
//        return resultResp;
//    }

    public Object getHistoryPrice(Long id, Integer type) throws IOException {
        Map<String,String> map = new HashMap<>();
        map.put("id",String.valueOf(id));
        map.put("type",String.valueOf(type));
        String sendGet = HttpUtils.sendGet("http://47.105.131.27:8080/rebateshop/common/getHistoryMoney", map);
        Object obj = objectMapper.readValue(sendGet, Object.class);
        return obj;
    }

}
