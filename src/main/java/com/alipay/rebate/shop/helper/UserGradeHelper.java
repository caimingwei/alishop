package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.dao.mapper.CommissionLevelSettingsMapper;
import com.alipay.rebate.shop.dao.mapper.UserGradeMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.CommissionLevelSettings;
import com.alipay.rebate.shop.model.UserGrade;
import com.alipay.rebate.shop.pojo.usergrade.UserGradeRsp;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class UserGradeHelper {

  @Resource
  private UserGradeMapper userGradeMapper;
  @Resource
  private CommissionLevelSettingsMapper commissionLevelSettingsMapper;

  public UserGradeRsp getUserGradeRsp(Integer id){
    UserGrade userGrade = userGradeMapper.selectByPrimaryKey(id);
    if(userGrade != null){
      List<CommissionLevelSettings> levelSettings =
          commissionLevelSettingsMapper.selectByUserGradeId(userGrade.getId());
      return UserGradeBuilder.convert2UserGradeRsp(userGrade,levelSettings);
    }
    return null;
  }
}
