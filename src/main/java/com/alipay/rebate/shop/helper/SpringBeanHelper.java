package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.dao.mapper.AppSettingsMapper;
import com.alipay.rebate.shop.service.admin.AdminUserService;
import com.alipay.rebate.shop.service.common.UserService;
import com.alipay.rebate.shop.service.customer.CustomerUserService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringBeanHelper implements ApplicationContextAware {

  static ApplicationContext applicationContext;
  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  public static AppSettingsMapper getAppSettingsMapper(){
    return applicationContext.getBean("appSettingsMapper",AppSettingsMapper.class);
  }

  public static UserService getAdminUserService(){
    return applicationContext.getBean(AdminUserService.class);
  }

  public static UserService getCustomerUserService(){
    return applicationContext.getBean(CustomerUserService.class);
  }
}
