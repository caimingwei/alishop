package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.constants.OrdersConstant;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.customer.PlusOrReduceVirMoneyReq;
import java.math.BigDecimal;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserHelper {

  private Logger logger = LoggerFactory.getLogger(UserHelper.class);
  @Resource
  private UserMapper userMapper;
  @Resource
  private UserIncomeMapper userIncomeMapper;
  @Autowired
  private JpushHelper jpushHelper;

  public void pluUserVirtualMoneyByType(TaskSettings taskSettings, Long userId){
    switch (taskSettings.getAwardType()){
      case UserIncomeConstant.MONEY_AWARD_TYPE:
        userMapper.plusUserAM(new BigDecimal(taskSettings.getAwardNum()),userId);
        break;
      case UserIncomeConstant.UIT_AWARD_TYPE:
        userMapper.plusUserUit(new Long(taskSettings.getAwardNum()),userId);
        break;
      case UserIncomeConstant.IT_AWARD_TYPE:
        userMapper.plusUserIntgeral(new Long(taskSettings.getAwardNum()),userId);
        break;
    }
  }

  public void pluUserVirtualMoneyAndUserIncomeByType(PlusOrReduceVirMoneyReq req,Long userId){
    UserIncome userIncome = UserIncomeBuilder.buildNotOrdersRelateUserIncome(
        req.getType(),req.getAwardType(),req.getNum(),userId
    );
    switch (req.getAwardType()){
      case UserIncomeConstant.MONEY_AWARD_TYPE:
        // 增加用户金额
        userMapper.plusUserAM(new BigDecimal(req.getNum()),userId);
        userIncomeMapper.insertSelective(userIncome);
        break;
      case UserIncomeConstant.UIT_AWARD_TYPE:
        // 增加用户集分宝
        userMapper.plusUserUit(req.getNum(),userId);
        userIncomeMapper.insertSelective(userIncome);
        break;
      case UserIncomeConstant.IT_AWARD_TYPE:
        // 增加用户积分
        userMapper.plusUserIntgeral(req.getNum(),userId);
        userIncomeMapper.insertSelective(userIncome);
        break;
    }
  }

  public void plusUserUit(
      Orders orders,
      Orders previousOrder,
      User user,
      User oneLevelUser
  ){

    Long userUit = orders.getUserUit();
    Long oneLevelUit = orders.getOneLevelUserUit();
    Long twoLevelUit = orders.getTwoLevelUserUit();
    // 如果订单是从付款状态进入结算状态，那么增加用户的收益信息
    plusUserUit(orders,previousOrder,user,oneLevelUser,userUit,oneLevelUit,twoLevelUit);
  }

  private void plusUserUit(
          Orders orders,
          Orders previousOrder,
          User user,
          User oneLevelUser,
          Long userUit,
          Long oneLevelUit,
          Long twoLevelUit
  ){
    if(shouldAddUserUit(previousOrder, orders)){
      plusUserUit(userUit,user.getUserId());
      if(oneLevelUser != null){
        plusUserUit(oneLevelUit,oneLevelUser.getUserId());
        if(oneLevelUser.getParentUserId() != null){
          plusUserUit(twoLevelUit,oneLevelUser.getParentUserId());
        }
      }
    }
  }

  public void plusUser(
          Orders orders,
          User user,
          User oneLevelUser
  ){
    Long userUit = orders.getUserUit();
    Long oneLevelUit = orders.getOneLevelUserUit();
    Long twoLevelUit = orders.getTwoLevelUserUit();
    if (OrdersConstant.ACCOUNTING_STATUS.equals(orders.getTkStatus())
    || OrdersConstant.COMPLETE_STATUS.equals(orders.getTkStatus())
    ){
      plusUserUit(userUit,user.getUserId());
      if(oneLevelUser != null){
        plusUserUit(oneLevelUit,oneLevelUser.getUserId());
        if(oneLevelUser.getParentUserId() != null){
          plusUserUit(twoLevelUit,oneLevelUser.getParentUserId());
        }
      }
    }
  }

  // 先检查是否为空再更新
  private void plusUserUit(Long userUit,Long userId){
    if(userId != null && userUit != null){
      userMapper.plusUserUit(userUit,userId);
      // 推送返利到账消息给用户
      jpushHelper.orderCommissionPush(String.valueOf(userId));
    }
  }

  private boolean shouldAddUserUit(
      Orders previousOrders,
      Orders orders){
    logger.debug("previousOrders and orders is : {}",previousOrders,orders);
    // 如果订单是直接进入结算或者完成状态的,那么用户集分宝增加
    if(previousOrders == null){
      return OrdersConstant.ACCOUNTING_STATUS.equals(orders.getTkStatus())
          || OrdersConstant.COMPLETE_STATUS.equals(orders.getTkStatus());
      // 如果订单是从订单付款进入订单结算或者订单完成状态，那么用户集分宝增加
    }else if(OrdersConstant.PAID_STATUS.equals(previousOrders.getTkStatus())){
      logger.debug("previousOrders tkStatus and orders tkStatus is : {},{}"
          ,previousOrders.getTkStatus(),orders.getTkStatus());
      return OrdersConstant.ACCOUNTING_STATUS.equals(orders.getTkStatus())
          || OrdersConstant.COMPLETE_STATUS.equals(orders.getTkStatus());
    }
    return false;
  }
}
