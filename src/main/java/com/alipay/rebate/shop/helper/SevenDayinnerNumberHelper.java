package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.PddOrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.pojo.adminindex.SevenDayInnerNumber;
import com.alipay.rebate.shop.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class SevenDayinnerNumberHelper {

    private Logger logger = LoggerFactory.getLogger(SevenDayinnerNumberHelper.class);

    @Resource
    private OrdersMapper ordersMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    PddOrdersMapper pddOrdersMapper;

    public List<SevenDayInnerNumber> getSevenDayInnerNumber(){

        List<SevenDayInnerNumber> sevenDayInnerNumbers = new ArrayList<>();
        for (int i = 0 ; i <= 6 ; i++){
            sevenDayInnerNumbers.add(initializationSevenInnerNumber(i));
        }
        logger.debug("sevenDayInnerNumbers is : {}",sevenDayInnerNumbers);

        //近七天
        List<SevenDayInnerNumber> paidOrderSevenDayInnerNumbers = ordersMapper.selectPaidOrdersCountOfSevenDayInner();
        List<SevenDayInnerNumber> estimateCommissionInnerNumbers = ordersMapper.selectEstimateCommissionCountOfSevenDayInner();
        List<SevenDayInnerNumber> userSevenDayInnerNumbers = userMapper.selectRegisterCountOfSevenDayInner();
        List<SevenDayInnerNumber> balanceSevenDayInner = ordersMapper.selectBalanceCommissionCountOfSevenDayInner();
        List<SevenDayInnerNumber> pddCommissionFeeCountOfSevenDayInner = pddOrdersMapper.getPddCommissionFeeCountOfSevenDayInner();
        logger.debug("orderSevenDayInnerNumbers is : {}",paidOrderSevenDayInnerNumbers);
        logger.debug("userSevenDayInnerNumbers is : {}",userSevenDayInnerNumbers);

        for (SevenDayInnerNumber order : sevenDayInnerNumbers){
            logger.debug("order is : {}",order);
            for (SevenDayInnerNumber userSevenDay : userSevenDayInnerNumbers){
                logger.debug("userSevenDay is : {}",userSevenDay);
                if ((order.getDate()).equals(userSevenDay.getDate())){
                    order.setRegisterCountOfSevenDayInner(userSevenDay.getRegisterCountOfSevenDayInner());
                }
            }
            for (SevenDayInnerNumber orderSevenDay : paidOrderSevenDayInnerNumbers){
                logger.debug("orderSevenDay is : {}",orderSevenDay);
                if ((order.getDate()).equals(orderSevenDay.getDate())){
                   order.setPaidOrdersCountOfSevenDayInner(orderSevenDay.getPaidOrdersCountOfSevenDayInner());
                }
            }
            for (SevenDayInnerNumber balanceOrder : balanceSevenDayInner){
                logger.debug("userSevenDay is : {}",balanceOrder);
                if ((order.getDate()).equals(balanceOrder.getDate())){
                    order.setBalanceCommissionCountOfSevenDayInner(balanceOrder.getBalanceCommissionCountOfSevenDayInner());
                }
            }

            for (SevenDayInnerNumber estimateCommission : estimateCommissionInnerNumbers){
                logger.debug("userSevenDay is : {}",estimateCommission);
                if ((order.getDate()).equals(estimateCommission.getDate())){
                    order.setEstimateCommissionCountOfSevenDayInner(estimateCommission.getEstimateCommissionCountOfSevenDayInner());
                }
            }
            for (SevenDayInnerNumber pddCommissionFee : pddCommissionFeeCountOfSevenDayInner){
                if (order.getDate().equals(pddCommissionFee.getDate())){
                    order.setPddCommissionFeeCountOfSevenDayInner(pddCommissionFee.getPddCommissionFeeCountOfSevenDayInner());
                }
            }
        }
        return sevenDayInnerNumbers;
    }

    private SevenDayInnerNumber initializationSevenInnerNumber(int day){

        logger.debug("day is : {}", day);
        SevenDayInnerNumber innerNumber = new SevenDayInnerNumber();
        innerNumber.setDate(DateUtil.getDaysBefore(day));
        BigDecimal bigDecimal = new BigDecimal(0.00);
        innerNumber.setEstimateCommissionCountOfSevenDayInner(bigDecimal);
        innerNumber.setPaidOrdersCountOfSevenDayInner(0);
        innerNumber.setRegisterCountOfSevenDayInner(0);
        innerNumber.setBalanceCommissionCountOfSevenDayInner(bigDecimal);
        logger.debug("innerNumber is : {}",innerNumber);
        return innerNumber;
    }



}
