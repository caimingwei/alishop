package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.model.LuckyMoneyCoupon;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.user.customer.InviteDetailRsp;
import com.alipay.rebate.shop.utils.DateUtil;
import com.alipay.rebate.shop.utils.IpUtil;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;

public class UserBuilder {

  public static void setRegisterCommonField(User user, HttpServletRequest request){
    Date date = new Date();
    user.setCreateTime(date);
    user.setUserRegisterTime(date);
    user.setUpdateTime(date);
    user.setUserRegisterIp(IpUtil.getIpAddress(request));
    user.setUserType(UserContant.CUSTOMER_USER_TYPE);
    user.setUserStatus(UserContant.USER_STATUS_NORMAL);
  }

  public static Map<String, InviteDetailRsp> buildInviteDetailRspMap(
      Map<String,Long> validUserMap,
      Map<String,Long> buyUserMap,
      Map<String, BigDecimal> inviteIncomeMap
  ){
    Map<String,InviteDetailRsp> result = new TreeMap<>();
    InviteDetailRsp inviteDetailRsp1 = new InviteDetailRsp();
    InviteDetailRsp inviteDetailRsp2 = new InviteDetailRsp();
    InviteDetailRsp inviteDetailRsp3 = new InviteDetailRsp();
    InviteDetailRsp inviteDetailRsp4 = new InviteDetailRsp();
    InviteDetailRsp inviteDetailRsp5 = new InviteDetailRsp();
    InviteDetailRsp inviteDetailRsp6 = new InviteDetailRsp();

    inviteDetailRsp1.setValidUserNum(validUserMap.get("thisMonth"));
    inviteDetailRsp1.setFirstBuyUserNum(buyUserMap.get("thisMonth"));
    inviteDetailRsp1.setIncome(inviteIncomeMap.get("thisMonth"));

    inviteDetailRsp2.setValidUserNum(validUserMap.get("lastMonth"));
    inviteDetailRsp2.setFirstBuyUserNum(buyUserMap.get("lastMonth"));
    inviteDetailRsp2.setIncome(inviteIncomeMap.get("lastMonth"));

    inviteDetailRsp3.setValidUserNum(validUserMap.get("threeMonth"));
    inviteDetailRsp3.setFirstBuyUserNum(buyUserMap.get("threeMonth"));
    inviteDetailRsp3.setIncome(inviteIncomeMap.get("threeMonth"));

    inviteDetailRsp4.setValidUserNum(validUserMap.get("fourMonth"));
    inviteDetailRsp4.setFirstBuyUserNum(buyUserMap.get("fourMonth"));
    inviteDetailRsp4.setIncome(inviteIncomeMap.get("fourMonth"));

    inviteDetailRsp5.setValidUserNum(validUserMap.get("fiveMonth"));
    inviteDetailRsp5.setFirstBuyUserNum(buyUserMap.get("fiveMonth"));
    inviteDetailRsp5.setIncome(inviteIncomeMap.get("fiveMonth"));

    inviteDetailRsp6.setValidUserNum(validUserMap.get("sixMonth"));
    inviteDetailRsp6.setFirstBuyUserNum(buyUserMap.get("sixMonth"));
    inviteDetailRsp6.setIncome(inviteIncomeMap.get("sixMonth"));

    LocalDate localDate = LocalDate.now();
    result.put(DateUtil.getMonthStr(localDate,0L),inviteDetailRsp1);
    result.put(DateUtil.getMonthStr(localDate,1L),inviteDetailRsp2);
    result.put(DateUtil.getMonthStr(localDate,2L),inviteDetailRsp3);
    result.put(DateUtil.getMonthStr(localDate,3L),inviteDetailRsp4);
    result.put(DateUtil.getMonthStr(localDate,4L),inviteDetailRsp5);
    result.put(DateUtil.getMonthStr(localDate,5L),inviteDetailRsp6);
    return ((TreeMap<String, InviteDetailRsp>) result).descendingMap();
  }

  public static CouponGetRecord buildCouponGetRecord(Long userId, LuckyMoneyCoupon luckyMoneyCoupon){

    if (luckyMoneyCoupon == null){
      return null;
    }
    if (luckyMoneyCoupon.getLimitNum() == null || luckyMoneyCoupon.getLimitNum() <=0){
      return null;
    }
    if (luckyMoneyCoupon.getStock() == null || luckyMoneyCoupon.getStock() <=0){
      return null;
    }
    CouponGetRecord couponGetRecord = new CouponGetRecord();
    couponGetRecord.setCouponId(luckyMoneyCoupon.getId());
    couponGetRecord.setUserId(userId);
    couponGetRecord.setType(1);
    Date date = new Date();
    couponGetRecord.setCreateTime(date);
    couponGetRecord.setUpdateTime(date);
    return couponGetRecord;
  }
}
