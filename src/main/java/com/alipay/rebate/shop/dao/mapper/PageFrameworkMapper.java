package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.PageFramework;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PageFrameworkMapper extends Mapper<PageFramework> {

    List<PageFramework> selectAll();
    PageFramework selectCurrentUsePageFramework();
    PageFramework selectDefaultPageFramework();
    List<PageFramework> selectUsePageFramework(@Param("isUse")Integer isUse);
    List<PageFramework> selectAllByType(@Param("isIndex")Integer isIndex);
    PageFramework selectIOSPageFramework();
    PageFramework selectPageIndex();
    int deletePageFramewordByTitle(String title);
    int updatePageIndex(PageFramework pageFramework);
    List<PageFramework> selectPgeIndexList();

}