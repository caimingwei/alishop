package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.JpushSettings;
import java.util.List;

public interface JpushSettingsMapper extends Mapper<JpushSettings> {

  List<JpushSettings> selectAllOrderById();
  JpushSettings selectByType(Integer type);
}