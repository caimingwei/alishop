package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.pojo.adminindex.SevenDayInnerNumber;
import com.alipay.rebate.shop.pojo.user.admin.PddOrdersPageReq;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import com.alipay.rebate.shop.pojo.user.customer.PddOrdersListRes;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface PddOrdersMapper extends Mapper<PddOrders> {

    PddOrders selectPddOrdersByOrderSn(String orderSn);

    Long selectCountByUserId(Long userId);

    List<OrdersListResponse> selectUserOrdersByType(@Param("orderStatus") Integer orderStatus, @Param("userId") Long userId);

    List<OrdersListResponse> selectFansOrdersByType(@Param("orderStatus") Integer orderStatus, @Param("parentUserIds") List<Long> parentUserIds);

    List<PddOrders> selectOrdersByCondition(PddOrdersPageReq req);
    List<PddOrders> selectOrdersByDate(String date);

    int updatePddOrders(PddOrders pddOrders);

    int deletePddOrdersByOrderSn(String orderSn);

    int getTotalPddOrdersNum();
    int getTodayPaidOrdersNum();
    int getYesterdayPaidOrdersNum(String yesterdayTime);
    BigDecimal getTotalCommission();
    BigDecimal getTodayCommissionFee();
    BigDecimal getYesterdayCommissionFee(String yesterdayTime);
    List<SevenDayInnerNumber> getPddCommissionFeeCountOfSevenDayInner();

}