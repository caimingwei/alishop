package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.AppSettings;

public interface AppSettingsMapper extends Mapper<AppSettings> {
    String selectPassword();
    String selectSmsAccesskeyId();
    String selectSmsSecret();
    int updatePassword(String password);
    int updateCsWetchat();
    int updateAppSettings(AppSettings appSettings);
}