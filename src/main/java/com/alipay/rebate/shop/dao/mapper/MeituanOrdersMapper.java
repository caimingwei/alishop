package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.MeituanOrders;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MeituanOrdersMapper extends Mapper<MeituanOrders> {
    int insertMeituanOrders(MeituanOrders meituanOrders);
    int updateMeituanOrders(MeituanOrders meituanOrders);
    MeituanOrders selectMeituanOrdersByOrderId(@Param("orderId") String orderId);
    List<MeituanOrders> selectAllMeiTuanOrders(@Param("orderId") String orderId,@Param("userName") String userName,
                                               @Param("promoterName") String promoterName,@Param("userId") Long userId,
                                               @Param("promoterId") Long promoterId,@Param("startTime") String startTime,
                                               @Param("endTime") String endTime);
    List<OrdersListResponse> selectMeituanOrdersByUserId(@Param("userId") Long userId);
    List<OrdersListResponse> selectFansMeituanOrders(@Param("parentUserIds") List<Long> parentUserIds);

    // 测试
    int deleteByOrderId(String orderId);
}