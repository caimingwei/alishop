package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.CommissionFreezeLevelSettings;
import java.math.BigDecimal;
import java.util.List;

public interface CommissionFreezeLevelSettingsMapper extends Mapper<CommissionFreezeLevelSettings> {

  List<CommissionFreezeLevelSettings> selectAllOrderByBegin();
  List<CommissionFreezeLevelSettings> selectByBeginAndEnd(BigDecimal money,Integer type);

  List<CommissionFreezeLevelSettings> getCommissionFreezeLevelSettingsMapperByType(Integer type);

}