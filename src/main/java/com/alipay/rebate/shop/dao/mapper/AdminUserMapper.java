package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.AdminUser;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserObjRsp;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AdminUserMapper extends Mapper<AdminUser> {

  List<AdminUserObjRsp> selectAllWithGroupMsg(
      @Param("account") String account);

  UserDto selectUserDtoInfoForAdminLogin(String account);

  UserDto selectUserDtoInfoById(long id);

  AdminUserObjRsp selectUserWithGroupMsg(Long id);

  AdminUser selectUserByAccount(String account);

  int deleteAdminUserByAccount(String account);

}