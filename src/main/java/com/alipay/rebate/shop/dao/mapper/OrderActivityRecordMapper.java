package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.model.OrderActivityRecord;

public interface OrderActivityRecordMapper {
    int insert(OrderActivityRecord record);

    int insertSelective(OrderActivityRecord record);

    OrderActivityRecord selectCustRecord(Long userId);
    OrderActivityRecord selectZeroNewUserRecord(Long userId);
    OrderActivityRecord selectZeroConditionRecord(Long userId);
    OrderActivityRecord selectZeroTodayInviteUserRecord(Long userId);

}