package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.RefundOrders;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RefundOrdersMapper extends Mapper<RefundOrders> {

  RefundOrders selectById(String tbTradeId);
  List<RefundOrders> selectAllRefundOrders(@Param("userId") Long userId,@Param("tradeId") String tradeId);
}