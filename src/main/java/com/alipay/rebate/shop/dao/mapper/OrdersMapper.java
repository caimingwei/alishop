package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.AdminIndexRsp;
import com.alipay.rebate.shop.pojo.adminindex.SevenDayInnerNumber;
import com.alipay.rebate.shop.pojo.user.admin.OrdersPageRequest;
import com.alipay.rebate.shop.pojo.user.admin.UpdateOrdersRequest;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import java.math.BigDecimal;

import com.mysql.cj.x.protobuf.MysqlxCrud;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrdersMapper{

    int deleteByPrimaryKey(String tradeId);
    int insert(Orders record);
    int insertSelective(Orders record);
    Orders selectByPrimaryKey(String tradeId);
    int updateByPrimaryKeySelective(Orders record);
    int updateByPrimaryKey(Orders record);
    List<Orders> selectOrdersByCondition(OrdersPageRequest ordersPageRequest);
    Integer selectTotalCount();
    List<OrdersListResponse> selectUserOrdersByType(
        @Param("tkStatus") Long tkStatus,
        @Param("userId") Long userId,
        @Param("relationId") Long relationId);
    List<OrdersListResponse> selectFansOrdersByType(@Param("tkStatus") Long tkStatus,@Param("parentUserIds") List<Long> parentUserIds);
    Long selectTradeIdByTradeId(String tradeId);
    Orders selectTradeIdAndTkStatus(String tradeId);
    List<Orders> selectOrderByGoodIdAndOrderType(@Param("goodId")String goodId,@Param("orderType")String orderType);
    Long selectCountByTimeAndUser(
        @Param("userId")Long userId,
        @Param("startTime")String startTime,
        @Param("endTime")String endTime,
        @Param("tkStatus") Long tkStatus
        );

    Long selectCountByUserId(@Param("userId")Long userId);

    Long selectCountByActivityIdAndGoodsId(
        @Param("goodsId") String goodsId,
        @Param("activityId")Integer activityId
    );
    Map selectPayedOrderPriceByTime(@Param("startTime") Date startTime, @Param("endTime")Date endTime);
    Map selectEarnedOrderByTime(@Param("startTime") Date startTime, @Param("endTime")Date endTime);
    Long selectDoneOrdersCount(Long userId);
    BigDecimal selectPaidMoneyByUserId(Long userId);
//    void plusOrdersNumByOne(Long userId);
    List<String> selectRecentBuyUserPhone(Integer day);
    Orders selectNotRecordOrder(String id);
    AdminIndexRsp selectIndexInfo();
    List<Orders> selectCurDayOrder();

    int selectPaymentOrderQuantity(@Param("startTime") Date startTime, @Param("endTime")Date endTime);
    int selectSettlementOrdersQuantity(@Param("startTime") Date startTime, @Param("endTime")Date endTime);

    List<SevenDayInnerNumber> selectPaidOrdersCountOfSevenDayInner();

    List<SevenDayInnerNumber> selectEstimateCommissionCountOfSevenDayInner();

    List<SevenDayInnerNumber> selectBalanceCommissionCountOfSevenDayInner();

    int getAllShareOrderNum();
    int getAllSelfPurchaseOrderNum();
    int getAllShareOrderNumToday();
    int getAllSelfPurchaseOrderNumToday();

    BigDecimal getAllShareCommissionNum();
    BigDecimal getAllSelfCommissionNum();
    BigDecimal getAllShareCommissionNumToday();
    BigDecimal getAllSelfCommissionNumToday();

    int selectTodayPaidOrderNum();

    List<Orders> selectOrdersAll(
            @Param("tkStatus") Long tkStatus,@Param("parentUserIds") List<Long> parentUserIds);

    List<Orders> selectOrdersByUserId(@Param("userId")Long userId);

    Orders selectOrdersByTradeId(String tradeId);

    int updateOrdersStatus(Orders orders);

    BigDecimal getTodayPaidCm();

    List<Orders> selectOrdersByConditionTwo(OrdersPageRequest ordersPageRequest);
    List<Orders> selectOrdersByTradeParentId(String tradeParentId);
    List<Orders> selectOrdersByIsRecord(OrdersPageRequest ordersPageRequest);
    List<Orders> selectOrdersByDate(String date);
}