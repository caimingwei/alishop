package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.ProductPageFramework;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductPageFrameworkMapper extends Mapper<ProductPageFramework>{

    List<ProductPageFramework> selectAll();

    ProductPageFramework selectUsedProductPageFramework();

    void deletePageById(@Param("id")Integer id);

    List<ProductPageFramework> selectProductPageFrameworkByPageCatagory(@Param("pageCatagory") Integer pageCatagory);

    int deletePageByPageName(String pageName);
}