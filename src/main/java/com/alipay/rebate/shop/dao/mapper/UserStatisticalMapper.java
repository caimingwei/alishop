package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.UserStatistical;


import java.util.List;

public interface UserStatisticalMapper extends Mapper<UserStatistical> {

    String getCreateTimeDate();

    int updateStatisticalByDate(UserStatistical userStatistical);



    List<UserStatistical> getUserStatistical();

    List<String> getCreateTime();

    int updateUserStatisticalByCreateTime(Integer authNum,String createTime);
}