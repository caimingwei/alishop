package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.pojo.user.customer.UserCouponRsp;
import java.util.List;

public interface CouponGetRecordMapper extends Mapper<CouponGetRecord> {

  List<UserCouponRsp> selectCouponDetailByUserId(Long userId);
  List<CouponGetRecord> selectNoUserCouponByUserId(Long userId);
  int deleteCouponDetailByUserId(Long userId);

}