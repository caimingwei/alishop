package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.SuperCategory;

import java.util.List;

public interface SuperCategoryMapper extends Mapper<SuperCategory> {

    int insertSuperCategory(SuperCategory superCategory);

    List<SuperCategory> selectAllSuperCategory();

    SuperCategory selectSuperCategoryByCid(Integer cid);

    int updateSuperCategoryByCid(SuperCategory superCategory);

}