package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.YjUserId;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YjUserIdMapper extends Mapper<YjUserId> {
    List<YjUserId> selectAll();
    String selectYjUserId(@Param("uid") String uid);
    String selectUserId(@Param("phone") String phone);
    int insertYjUserId(@Param("uid") String uid,@Param("phone") String phone);
    int deleteAll();
}