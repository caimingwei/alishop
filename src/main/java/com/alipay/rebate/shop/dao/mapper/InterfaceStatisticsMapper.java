package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.InterfaceStatistics;
import com.alipay.rebate.shop.pojo.InterfaceStatisticsRsp;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface InterfaceStatisticsMapper extends Mapper<InterfaceStatistics> {

    int insertInterfaceStatics(InterfaceStatistics interfaceStatistics);

    int updateOneMinuteCount(@Param("name") String name);

    int updateOneDayCount(@Param("name") String name);

    int updateByTheSameDay(@Param("theSameDay") Date theSameDay);

    InterfaceStatistics selectByName(@Param("name") String name);

    List<InterfaceStatistics> selectByMinute(@Param("minute") String minute);

    List<InterfaceStatistics> selectByDay(@Param("theSameDay") Date theSameDay);

    int updateByMinute(@Param("name") String name,@Param("minute") String minute);

    List<InterfaceStatisticsRsp> selectInterfaceStatistics();

}