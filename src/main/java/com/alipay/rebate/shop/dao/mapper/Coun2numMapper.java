package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.Coun2num;

public interface Coun2numMapper extends Mapper<Coun2num> {
}