package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.LogOffApply;
import com.alipay.rebate.shop.pojo.logoffapply.LogoffApplySearchReq;
import java.util.List;

public interface LogOffApplyMapper extends Mapper<LogOffApply> {

  List<LogOffApply> selectRecordByCondition(LogoffApplySearchReq req);
  LogOffApply selectLatestByUserId(Long userId);
  int deleteByUserId(Long userId);
}