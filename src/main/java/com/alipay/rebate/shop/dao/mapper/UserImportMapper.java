package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.UserImport;

import java.util.List;

public interface UserImportMapper extends Mapper<UserImport> {
    int insert(UserImport userImport);
    int deleteAll();
    List<UserImport> selectAll();
    List<UserImport> selectUser();
}
