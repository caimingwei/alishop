package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.CommissionLevelSettings;
import java.math.BigDecimal;
import java.util.List;

public interface CommissionLevelSettingsMapper extends Mapper<CommissionLevelSettings> {
  List<CommissionLevelSettings> selectAllOrderByBegin();
  List<CommissionLevelSettings> selectByBeginAndEnd(BigDecimal money);
  List<CommissionLevelSettings> selectByUserGradeId(Integer userGradeId);
  void deleteByUserGradeId(Integer userGradeId);
}