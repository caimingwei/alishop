package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.model.PageModule;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

public interface PageModuleMapper {
    int deleteByPrimaryKey(Long moduleId);

    int insert(PageModule record);

    int insertSelective(PageModule record);

    PageModule selectByPrimaryKey(Long moduleId);

    int updateByPrimaryKeySelective(PageModule record);

    int updateByPrimaryKey(PageModule record);

    int insertPageModuleList(List<PageModule> list);

    int deleteByPagePrimaryKey(Long pageId);

    List<PageModule> selectByPagePrimaryKey(Long pageId);
}