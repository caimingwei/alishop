package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.AlipayOrder;

import java.util.List;

public interface AlipayOrderMapper extends Mapper<AlipayOrder> {
    public List<AlipayOrder> getAllTransferOrder();
    public List<AlipayOrder> getAllSuccessTransferOrder();
}