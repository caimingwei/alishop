package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.CommissionFreezeSettings;

public interface CommissionFreezeSettingsMapper extends Mapper<CommissionFreezeSettings> {
}