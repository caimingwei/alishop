package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.model.InviteOrderFirstRecord;
import org.apache.ibatis.annotations.Param;

public interface InviteOrderFirstRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InviteOrderFirstRecord record);

    int insertSelective(InviteOrderFirstRecord record);

    InviteOrderFirstRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InviteOrderFirstRecord record);

    int updateByPrimaryKey(InviteOrderFirstRecord record);

    InviteOrderFirstRecord selectByUserIdAndParentUserId(
        @Param("userId") Long userId,
        @Param("parentUserId") Long parentUserId);

    int selectCountByParentUserId(Long parengUserId);

}