package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.JpushHistory;
import java.util.List;

public interface JpushHistoryMapper extends Mapper<JpushHistory> {

  List<JpushHistory> selectAllOrderById();
}