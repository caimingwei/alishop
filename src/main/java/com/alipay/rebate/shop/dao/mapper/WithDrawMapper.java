package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.model.AlipayOrder;
import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.pojo.user.admin.AdminWithDrawRsp;
import com.alipay.rebate.shop.pojo.user.admin.UserIncomeRsp;
import com.alipay.rebate.shop.pojo.user.admin.WithDrawUto;
import com.alipay.rebate.shop.pojo.user.customer.UserMiddleModel;
import com.alipay.rebate.shop.pojo.user.admin.WithDrawPageRequest;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

public interface WithDrawMapper{
    int deleteById(Long withdrawId);
    int deleteByPrimaryKey(Long withdrawId);

    int insertWithDraw(WithDraw record);

    int insertWithDrawSelective(WithDraw record);

    WithDraw selectByPrimaryKey(Long withdrawId);

    int updateByPrimaryKeySelective(WithDraw record);

    int updateByPrimaryKey(WithDraw record);

    List<WithDraw> selectWithDrawByUserId(Long userId);

    List<WithDrawUto> selectWithDrawByCondition(WithDrawPageRequest withDrawPageRequest);

    List<WithDraw> selectWithDrawByUserIdAndType(
        @Param("userId")
        Long userId,
        @Param("type")
        Integer type);

    UserMiddleModel selectWithDrawDetailForCustomer(Long userId);

    List<WithDraw> selectAllWithDraws();

    Map selectApplyNumberByTime(@Param("startTime") Date startTime,@Param("endTime")Date endTime);

    BigDecimal selectWithDrawMoneyByUserId(Long userId);

    int selectCountByStatusUserId(
        @Param("userId") Long userId,
        @Param("status") Integer status
    );

    Integer selectCurdayCount();

    Integer selectCountByUserId(Long userId);

    //未处理提现的申请数
    int selectNotHandleWithDrawCount();

    BigDecimal selectWithDrawSuccessMoneyNum();

    int updateOrderStatus(Long withdrawId);

    List<WithDrawUto> selectNotHandleWithDraw(WithDrawPageRequest withDrawPageRequest);
    List<WithDrawUto> selectHandleWithDraw(WithDrawPageRequest withDrawPageRequest);

    List<AdminWithDrawRsp> selectWithDraw(UserIncomeRsp userIncome);
    int deleteWithDrawByUserId(Long userId);
    WithDraw selectWithdrawByStatusAndUserId(Integer status,Long userId);
    List<WithDraw> selectNoHandle();
}