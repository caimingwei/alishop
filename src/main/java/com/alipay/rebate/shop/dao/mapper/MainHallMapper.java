package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.MainHall;

public interface MainHallMapper extends Mapper<MainHall> {

    int addMainHall(MainHall mainHall);

    int updateMainHallById(MainHall mainHall);

    MainHall selectMainHallById(Long id);

    int deleteMainHallById(Long id);
}