package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.UserGrade;

import java.util.List;

public interface UserGradeMapper extends Mapper<UserGrade> {
    List<UserGrade> getAllSortByWeight();

    UserGrade selectUserGrade();
    void deleteByGradeName(String gradeName);
}