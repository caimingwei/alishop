package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.Lottery;

public interface LotteryMapper extends Mapper<Lottery> {

  Lottery selectCurrentUseLottery();
  void resetCurrentUseFlag();
  Lottery selectLotteryById(Integer id);
  int deleteLotteryById(Integer id);
}