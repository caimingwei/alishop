package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.HtmlClick;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface HtmlClickMapper extends Mapper<HtmlClick> {

    List<HtmlClick> selectHtmlClickAll(Date thySameDay);

    int insertHtmlClick(HtmlClick htmlClick);

    HtmlClick selectHtmlClick(@Param("htmlName") String htmlName,
                                   @Param("htmlChineseName") String htmlChineseName,
                                   @Param("theSameDay") Date theSameDay);

    int updateHtmlClick(@Param("htmlName") String htmlName,
                             @Param("htmlChineseName") String htmlChineseName,
                             @Param("theSameDay") Date theSameDay,
                             @Param("updateTime") Date updateTime);
    HtmlClick selectHtmlClickByHtmlName(@Param("htmlName") String htmlName,@Param("theSameDay") Date theSameDay);
    int updateHtmlClickByHtmlName(@Param("htmlName") String htmlName,
                        @Param("theSameDay") Date theSameDay,
                        @Param("updateTime") Date updateTime);
}