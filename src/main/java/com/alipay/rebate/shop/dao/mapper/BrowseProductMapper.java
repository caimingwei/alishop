package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.BrowseProduct;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BrowseProductMapper extends Mapper<BrowseProduct>{
    List<BrowseProduct> selectBrowseProductByUserId(Long userId);

    BrowseProduct selectBrowseProductByUserIdAndDate(
        @Param("userId")
        Long userId,
        @Param("goodsId")
        Long goodsId
    );

    List<String> selectActiveUserPhone(Integer day);
    List<String> selectSleepUserPhone(Integer day);
    int deleteByUserId(Long userId);
    List<BrowseProduct> selectBrowseProductByUserIdAndGoodsId(
            @Param("userId")
                    Long userId,
            @Param("goodsId")
                    Long goodsId
    );


}