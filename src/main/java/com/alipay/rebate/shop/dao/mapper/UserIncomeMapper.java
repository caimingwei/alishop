package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.entitysettings.IrUser;
import com.alipay.rebate.shop.pojo.user.admin.UserIncomeRsp;
import com.alipay.rebate.shop.pojo.user.customer.UserIncomeRecordResponse;
import com.alipay.rebate.shop.pojo.user.customer.UserMiddleModel;
import com.alipay.rebate.shop.pojo.userincome.IncomeSchedulerRsp;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.alipay.rebate.shop.pojo.userincome.CalculateUserIncome;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

public interface UserIncomeMapper extends Mapper<UserIncome> {

    BigDecimal selectUserTotalIncomeMessage(Long userId);

    BigDecimal selectUserTodayIncomeMessage(
            @Param("userId") Long userId,
            @Param("thisDayStartTime") String startTime,
            @Param("nextDayStartTime") String endTime);

   UserIncomeRecordResponse selectUserIncomeRecordDetail(
            @Param("thisMonthStartTime") String thisMonthStartTime,
            @Param("nextMonthStartTime") String nextMonthStartTime,
            @Param("lastMonthStartTime") String lastMonthStartTime,
            @Param("thisDayStartTime") String thisDayStartTime,
            @Param("nextDayStartTime") String nextDayStartTime,
            @Param("lastDayStartTime") String lastDayStartTime,
            @Param("userId") Long userId
    );

   UserMiddleModel selectSettlementingInfo(Long userId);

   int deleteByTradeId(String tradeId);

   List<UserIncome> seleteUserIncomeByTradeId(String tradeId);

   List<UserIncome> selectUserIncomeByOrdersId(String tradeId);

   UserIncome selectTodayReadAwardByUserId(
       @RequestParam("userId") Long userId,
       @RequestParam("type") Integer type,
       @RequestParam("time") String time
   );

  List<UserIncome> selectUserAccountDetailIncome(
      @RequestParam("userId") Long userId
  );

  List<UserIncome> selectUserOrdersDetailIncome(
      @RequestParam("userId") Long userId
  );

  List<UserIncome> selectPddOrdersDetailIncome(@RequestParam("userId") Long userId);
  List<UserIncome> selectJdOrdersDetailIncome(@RequestParam("userId") Long userId);
  List<UserIncome> selectMeituanOrdersDetailIncome(@RequestParam("userId") Long userId);


    List<UserIncome> selectAllIncome();

    List<UserIncomeRsp> selectAllByCondition(UserIncomeRsp userIncomeRsp);

    Map<String,BigDecimal> selectSMAIncome(
        @Param("userId") Long userId,
        @Param("now") String now
    );

    BigDecimal selectOrderRewardIncomeMoneyByUserId(Long userId);

    List<IncomeSchedulerRsp> selectFreezeIncome();
    List<IncomeSchedulerRsp> selectJdOrdersFreezeIncome();
    List<IncomeSchedulerRsp> selectPddOrdersFreezeIncome();

    List<IncomeSchedulerRsp> selectFreezeIncomeByUserId(Long userId);

    List<IncomeSchedulerRsp> selectFansFreezeIncomeByUserId(@Param("userIds") List<Long> userIds);

    BigDecimal selectFreezeMoney(Long userId);

    int selectTodayCountByType(Integer type, Long userId);

    int selectInviteIncomeByUserId(
        @Param("parentUserId") Long parentUserId,
        @Param("userId") Long userId
    );

    int selectCountByUserIdAndType(
        @Param("userId") Long userId,
        @Param("type") Integer type
    );

    List<IrUser> selectIrRankUser(Integer num, String startTime, String endTime);

    BigDecimal selectIncomeByTradeId(String tradeId,Long userId);

    UserIncome selectUserIncomeByUserIdAndType(Long userId,Integer type);
    int updateUserIncomeByUserIdAndType(Long userId,Integer type);
    int updateUserIncome(Long userId,Integer type);
    int updateUserIncomeByUserIncome(UserIncome userIncome);
    int updateUserIncomeByOrderId(String tradeId);

    UserIncome getUserIncomeByTypeAndUserId(Long userId);

    int deleteUserIncomeByUserId(Long userId);

    List<IncomeSchedulerRsp> selectUserIncomeByType();
    int updateUserIncomeByTradeId(Integer roadType,String tradeId,BigDecimal money);
    List<IncomeSchedulerRsp> selectUserIncomeByTradeId(@Param("tradeIds") List<String> tradeIds);
    UserIncome selectUserIncomeByUserId(Long userId);
    List<UserIncome> getUserIncomeByUserId(Long userId);
    UserIncome selectUserIncomeByTradeIdAndStatus(String tradeId);
    // 查询用户的总集分宝，总提现金额，总收益
    List<CalculateUserIncome> selectUserTotalIncome();
    UserIncome selectUserIncomeByTradeIdAndType(String tradeId);
    List<UserIncome> selectMeiTuanByCreateTime(@Param("days")String days);
    List<UserIncome> selectJdOrderUserIncomeByCreateTime(@Param("days")String days);

    int updateMeiTuanUserIncome(Long id);
}