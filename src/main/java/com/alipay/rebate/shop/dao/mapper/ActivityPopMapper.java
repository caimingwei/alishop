package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.ActivityPop;

public interface ActivityPopMapper extends Mapper<ActivityPop> {
}