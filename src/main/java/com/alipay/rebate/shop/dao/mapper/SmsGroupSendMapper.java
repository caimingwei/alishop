package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.SmsGroupSend;
import com.alipay.rebate.shop.pojo.smsgoupsend.SmsGroupSendSearchReq;
import java.util.List;

public interface SmsGroupSendMapper extends Mapper<SmsGroupSend> {

  List<SmsGroupSend> selectSmsGroupSendByCondition(SmsGroupSendSearchReq req);
  List<SmsGroupSend> selectOnTimeRecord();
  void deleteByPhone(String phone);

}