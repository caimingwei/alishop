package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.JdOrders;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.user.admin.JdOrdersReq;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface JdOrdersMapper extends Mapper<JdOrders> {

    int deleteJdOrdersByOrdersId(Long orderId);

    JdOrders selectOrderByOrderId(Long orderId);

    Long selectCountByUserId(Long userId);

    int updateJdOrders(JdOrders orders);

    List<JdOrders> selectOrdersByCondition(JdOrdersReq req);

    List<OrdersListResponse> selectJdOrdersByStatus(Long validCode,Long userId);

    List<OrdersListResponse> selectFansJdOrdersByType(@Param("validCode") Long validCode,@Param("parentUserIds") List<Long> parentUserIds);

    int getTotalJdOrdersNum();
    int getTodayPaidOrdersNum();
    int getYesterdayPaidOrdersNum(String yesterdayTime);
    BigDecimal getTotalCommission();
    BigDecimal getTodayCommissionFee();
    BigDecimal getYesterdayCommissionFee(String yesterdayTime);

    //查询上个月，已完成状态的订单
    List<JdOrders> selectLastMonthJdOrdersByTime(String startTime,String endTime);
    // 查询已结算的京东订单
    List<JdOrders> selectJdOrdersByDate(@Param("date") String date);
    List<JdOrders> selectJdOrdersByMonth(String startTime,String endTime);
}