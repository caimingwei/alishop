package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.ImgGroup;

public interface ImgGroupMapper extends Mapper<ImgGroup> {
}