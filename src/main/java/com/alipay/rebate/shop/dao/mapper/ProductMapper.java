package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.Product;
import com.alipay.rebate.shop.pojo.pdtframework.OfficialChoice;
import com.alipay.rebate.shop.pojo.pdtframework.OwnChoice;
import com.alipay.rebate.shop.pojo.pdtframework.ProductRepoChoice;
import com.alipay.rebate.shop.pojo.user.customer.SearchProductRequest;
import java.util.List;

public interface ProductMapper extends Mapper<Product> {

    int insertProducts(List<Product> products);
    int deleteProductsById(List<Product> products);
    int updateBatch(List<Product> products);
    int deleteDataokeGoodsFriendsCircle();

    List<Long> selectGoodsIdsIndb();
    List<Long> selectDataokeGoodsIdsIndb();
    List<Long> selectHaodankuGoodsIdsIndb();
    List<Product> selectDataokeGoodsFriendsCircleList();

    List<Product> selectGoodsByCondition(SearchProductRequest request);

    List<Product> selectOwnChoiceProduct(OwnChoice ownChoice);
    List<Product> selectOfficialChoiceProduct(OfficialChoice officialChoice);
    List<Product> selectALLOfficialChoiceProduct(OfficialChoice officialChoice);

    List<Product> selectProductRepoChoiceProduct(ProductRepoChoice productRepoChoice);
    List<Product> selectALLProductRepoChoiceProduct(ProductRepoChoice productRepoChoice);

}