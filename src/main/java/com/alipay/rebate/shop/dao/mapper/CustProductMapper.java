package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.CustProduct;

public interface CustProductMapper extends Mapper<CustProduct> {
}