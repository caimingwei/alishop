package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.UserJdPid;

import java.util.List;

public interface UserJdPidMapper extends Mapper<UserJdPid> {

    UserJdPid selectUserJdPidByUserId(Long userId);

    String selectUserPddPidByUserId(Long userId);

    int updateUserJdPidByUserId(String positionId,Long userId);

    int updateUserPddPIdByUserId(String pddPId,Long userId);

    Long selectUserIdByPddPId(String pddPId);

    Long selectUserIdByPositionId(String positionId);

    int deleteUserJdPidByUserId(Long userId);


}