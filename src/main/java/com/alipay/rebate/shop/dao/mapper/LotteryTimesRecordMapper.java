package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.LotteryTimesRecord;

public interface LotteryTimesRecordMapper extends Mapper<LotteryTimesRecord> {

  LotteryTimesRecord selectCurDayTimesRecordByUserId(Long userId);
  void plusCurdayLotteryTimes(Long userId);

  void reduceCirdayLotteryNum(Long userId);
  int deleteLotteryTimesRecordById(Long id);
}