package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.ImgSpace;
import com.alipay.rebate.shop.pojo.imgspace.SearchCondition;
import java.util.List;

public interface ImgSpaceMapper extends Mapper<ImgSpace> {

  List<ImgSpace> selectISByCondition(SearchCondition searchCondition);
  void updateGroupNameByGroupId(Long groupId, String groupName);
  ImgSpace selectImgSpaceById(Long id);
}