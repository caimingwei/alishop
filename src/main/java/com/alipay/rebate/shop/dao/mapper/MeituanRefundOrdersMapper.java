package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.MeituanRefundOrders;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MeituanRefundOrdersMapper extends Mapper<MeituanRefundOrders> {
    int insertMeituanRefundOrders(MeituanRefundOrders meituanRefundOrders);
    int updateMeituanRefundOrders(MeituanRefundOrders meituanRefundOrders);
    MeituanRefundOrders selectMeituanRefundOrders(@Param("orderId") String orderId);
    List<MeituanRefundOrders> selectAllMeituanRefundOrder(@Param("userId") Long userId);

    // 测试
    int deleteByOrderId(String orderId);
}