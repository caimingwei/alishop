package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.TaskDoRecord;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TaskDoRecordMapper extends Mapper<TaskDoRecord> {

  List<TaskDoRecord> selectNewUserAndDailyTask(Long userId);
  List<TaskDoRecord> selectNewUserRecordBySubType(
      @Param("userId") Long userId,
      @Param("subType") Integer subType
  );
  Integer selectCountBySubType(
      @Param("userId") Long userId,
      @Param("subType") Integer subType
  );

  List<TaskDoRecord> selectTaskDoRecordByUserId(Long userId);
  int deleteTaskDoRecordByUserId(Long userId);
}