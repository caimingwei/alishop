package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.UjuecCreditsAdd;

import java.util.List;

public interface UjuecCreditsAddMapper extends Mapper<UjuecCreditsAdd> {
    int insertUjuecCreditsAdd(String phone);
    UjuecCreditsAdd selectByPhone(String phone);
}