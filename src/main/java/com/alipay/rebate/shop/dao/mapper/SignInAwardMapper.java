package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.SignInAward;
import java.math.BigDecimal;
import org.apache.ibatis.annotations.Param;

public interface SignInAwardMapper extends Mapper<SignInAward> {

    SignInAward selectByUserId(Long userId);

    void plusOrReduceMoneyOrIntgeral(
        @Param("money") BigDecimal money,
        @Param("integral") Integer integral,
        @Param("userId") Long userId
    );
    int deleteByUserId(Long userId);
}