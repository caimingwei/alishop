package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.ActivityBrowseProduct;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ActivityBrowseProductMapper extends Mapper<ActivityBrowseProduct> {

    List<ActivityBrowseProduct> getByProductIdAndTime(
        @Param("goodsId") String goodsId,
        @Param("startTime") String startTime,
        @Param("endTime") String endTime,
        @Param("userId") Long userId
    );

    void deleteByUserId(Long userId);

    List<ActivityBrowseProduct> getActivityBrowseProductByUserId(@Param("goodsId") String goodsId,@Param("userId") Long userId);

}