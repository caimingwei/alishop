package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.ActivityProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityProductMapper extends Mapper<ActivityProduct> {
    void deleteByActivityId(@Param("activityId")Integer activityId);

    List<ActivityProduct> getProductsByActivityId(@Param("activityId")Integer activityId);

    ActivityProduct selectProductByActivityAndProductId(
        @Param("activityId") Integer activityId,
        @Param("productId") String productId);

    List<ActivityProduct> selectByQualType(Integer qualType);

    int updateActivityProductById(Integer id);

    ActivityProduct selectActivityProductById(Integer id);

}