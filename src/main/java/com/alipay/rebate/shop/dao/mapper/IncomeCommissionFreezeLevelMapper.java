package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.IncomeCommissionFreezeLevel;

public interface IncomeCommissionFreezeLevelMapper extends Mapper<IncomeCommissionFreezeLevel> {

  void deleteByIncomeId(Long id);

}