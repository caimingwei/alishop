package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.AdminIndexRsp;
import com.alipay.rebate.shop.pojo.adminindex.SevenDayInnerNumber;
import com.alipay.rebate.shop.pojo.alipay.WithdrawUserInfo;
import com.alipay.rebate.shop.pojo.entitysettings.InrUser;
import com.alipay.rebate.shop.pojo.user.admin.AuthNum;
import com.alipay.rebate.shop.pojo.user.admin.PhoneType;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import com.alipay.rebate.shop.pojo.user.customer.UserFansDetailInfo;
import com.alipay.rebate.shop.pojo.user.admin.UserListResponse;
import com.alipay.rebate.shop.pojo.user.admin.UserPageRequest;
import java.math.BigDecimal;

import io.netty.channel.udt.UdtServerChannel;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface UserMapper {

    User selectById(Long id);
    User selectUserByPhone(String phone);
    Long selectUserIdByPhone(String phone);
    Long insertUser(User user);
    int insertSelective(User record);
    int updateUser(User user);
    int deleteUserById(Long id);
    int deleteUserByPhone(String phone);
    int updateUserFansNumByUserId(Long userId);
    List<UserListResponse> selectUserByCondition(UserPageRequest userPageRequest);
    int selectTotalCountByCondition( Map<String,Object> map);
    UserDto selectUserDtoInfoByPhone(String phone);
    UserDto selectUserDtoInfoById(long id);
    UserDto selectUserDtoInfoByOpenId(String openId);
    User selectUserByOpenId(String openId);
    UserDto selectUserDtoInfoForPasswordLogin(String phone);
    UserDto selectUserDtoInfoForAdminLogin(String phone);
    User selectAllUserInfoForTestByPhone(String phone);
    void deleteUserInfoForTestByPhone(String phone);
    User selectAliPayMessageById(Long userId);
    List<UserFansDetailInfo> selectOneLevelFansInfo(
        @Param("parentUserId") Long parentUserId,
        @Param("userNameOrPhone") String userNameOrPhone
    );
    List<UserFansDetailInfo> selectSecondLevelFansInfo(
        @Param("parentUserId") Long parentUserId,
        @Param("userNameOrPhone") String userNameOrPhone
    );
    List<Long> selectOneLevelFansIds(Long parentUserId);
    List<Long> selectSecondLevelFansIds(List<Long> parentUserIds);
    User selectUserInfoByTaobaoUserIdFinalSixNumber(String idFinalSixNumber);
    List<User> selectUserByTaoBaoUserIdFinalSixNumber(String idFinalSixNumber);
    User selectUserByRelationId(Long relationId);
    User selectUserByRelationIdOrSpecialId(
        @Param("relationId") Long relationId,
        @Param("specialId") Long specialId
    );
    void plusUserUit(@Param("uit") Long uit, @Param("userId") Long userId);
    void plusUserIntgeral(@Param("intgeral") Long intgeral, @Param("userId") Long userId);
    void plusUserAM(@Param("uAM") BigDecimal uAM, @Param("userId") Long userId);

    void plusOrReductVirtualMoney(
        @Param("uit") Long uit,
        @Param("intgeral") Long intgeral,
        @Param("uAM") BigDecimal uAM,
        @Param("userId") Long userId
    );

    void plusOrReductVirtualMoneyDirectly(
        @Param("uit") Long uit,
        @Param("intgeral") Long intgeral,
        @Param("uAM") BigDecimal uAM,
        @Param("userId") Long userId
    );

    void plusTotalFanAndStraightFanNum(
        @Param("tfn") Integer tfn,
        @Param("sfn") Integer sfn,
        @Param("userId") Long userId);
    Long selectInviteNumThisDay(
        @Param("startTime") String startTime,
        @Param("endTime") String endTime,
        @Param("userId") Long userId
    );

    Map<String,Long> selectSMACount(
        @Param("userId") Long userId,
        @Param("now") String now
    );

    Map<String,Long> selectSMABuyOrderCount(
        @Param("userId") Long userId,
        @Param("now") String now
    );

    void updateUserGradeToDefault(Integer userGradeId);

    void plusOrdersNumByOne(Long userId);

    void setTaobaoAuthRelationIdToNullById(Long userId);

    List<String> selectAllPhone();

    List<String> selectNewRegisUserPhone(Integer day);

    AdminIndexRsp selectAdminIndexInfo();

    List<InrUser> selectInrRankUser(Integer num, String startTime, String endTime);

    List<WithdrawUserInfo> selectByWithDrawIds(List<Long> ids);

    List<SevenDayInnerNumber> selectRegisterCountOfSevenDayInner();

    String selectUserNameByPhone(String phone);

    int updateUserByUserId(User user);

    int updateUserAlipayAccount(Long userId,String realName,String alipayAccount);

    int selectCountByTbid(String idFinalSixNumber);

    User selectUserByUserId(Long userId);

    User selectUserBySpecialId(Long specialId);

    //查询当天的注册用户数量
    int getTodayRegisterUser();
    //查询当天有上级的注册用户注册
    int getTodayRegisterUserAndHasParent();
    //查询当天没有上级的注册用户数量
    int getTodayRegisterUserAndNoParent();

    // 有上级或没上级，有订单
    int getHasParentAndOrdersNum();
    int getNoParentButHasOrdersNum();

    // 有上级或没上级，没订单
    int getNoParentAuthNum();
    int getHasParentAuthNum();

    // 昨天注册的用户数量
    int getYesterdayRegisterNum(String yesterdayDate);
    int getYesterdayRegisterHasParentNum(String yesterdayDate);
    int getYesterdayRegisterNoParentNum(String yesterdayDate);
    // 昨天注册今天登录
    int getHasParentUserLoginNum(String yesterdayDate);
    int getNoParentUserLoginNum(String yesterdayDate);

    List<PhoneType> getDeviceModel();
    List<PhoneType> getDeviceModelTwo();

    // 查询七天前的一天的注册量
    int getBeforeAgeDayRegisterUser(String startTime);
    int getBeforeAgeDayRegisterUserAndHasParent(String startTime);
    int getBeforeAgeDayRegisterUserAndNoParent(String startTime);
    // 查询30天前的与一天的注册量
    int getBeforeThirtyDaysRegisterUser(String startTime);
    int getBeforeThirtyDaysRegisterUserAndHasParent(String startTime);
    int getBeforeThirtyDaysRegisterUserAndNoParent(String startTime);
    // 七天前的一天的注册量，七天内登录的用户数量
    int getHasParentUserSevenDayLoginNum(String startTime, String endTime,String registerDate);
    int getNoParentUserSevenDayLoginNum(String startTime, String endTime,String registerDate);
    // 30天前的与一天的注册量，且30天内登录的用户数量
    int getHasParentUserThirtyDayLoginNum(String startTime, String endTime,String registerDate);
    int getNoParentUserThirtyDayLoginNum(String startTime, String endTime,String registerDate);

    List<User> getPrivacyProtectionByUserId(@Param("userIds") List<Long> userIds);
    User getUserByPhone(String phone);
    int updateUserPassword(User user);
    // 查询授权的用户的注册时间
    List<AuthNum> getUserRegisterByAuth();
    List<User> selectUserByAlipayAccount(String alipayAccount);
    List<User> selectTheBlacklist(Long userId,String phone,String userName);
    List<User> selectAll();
    List<User> selectParentUserIdNotNull();
    String selectPhone(Long userId);
}