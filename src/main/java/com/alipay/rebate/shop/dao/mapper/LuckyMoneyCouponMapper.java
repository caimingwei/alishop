package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.LuckyMoneyCoupon;
import java.util.List;

public interface LuckyMoneyCouponMapper extends Mapper<LuckyMoneyCoupon> {

  void setCurrentUserCouponToNotUse();
  List<LuckyMoneyCoupon> selectCurrentUseCoupon();
  void decreaseStockByOne(Integer id);
  List<LuckyMoneyCoupon> selectAll();
  int deleteById(Integer id);
}