package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.TaskSettings;
import java.util.List;

public interface TaskSettingsMapper extends Mapper<TaskSettings> {

  List<TaskSettings> selectUseSettings();
  List<TaskSettings> selectALLSettings(Integer type);
  void setIsUseToNotUseBySubType(Integer type);
  TaskSettings selectUseSettingsBySubType(Integer subType);
  TaskSettings selectUseSettingsBySubTypeAndIsUse(Integer subType,Integer isUse);
}