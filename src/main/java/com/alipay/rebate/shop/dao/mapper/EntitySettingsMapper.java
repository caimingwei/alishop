package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.EntitySettings;

public interface EntitySettingsMapper extends Mapper<EntitySettings> {
  EntitySettings selectByType(Integer type);
  void updateByType(EntitySettings entitySettings);
}