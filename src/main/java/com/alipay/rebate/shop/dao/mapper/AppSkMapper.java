package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.AppSk;

public interface AppSkMapper extends Mapper<AppSk> {

  AppSk selectAppSkBySk(String sk);
}