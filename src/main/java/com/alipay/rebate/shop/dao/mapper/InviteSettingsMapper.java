package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.InviteSettings;

public interface InviteSettingsMapper extends Mapper<InviteSettings> {
}