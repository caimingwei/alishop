package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.MobileCodeInformation;
import com.alipay.rebate.shop.pojo.mobilecode.CodePageInformation;
import com.alipay.rebate.shop.pojo.mobilecode.MobileCodeStatistical;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MobileCodeInformationMapper extends Mapper<MobileCodeInformation> {

    int insertMobileCodeInformation(MobileCodeInformation information);
    List<MobileCodeInformation> selectAllMobileCodeInforMation(CodePageInformation pageInformation);
    MobileCodeStatistical getMobileCodeStatistical(@Param("date") String date);
    // 指定时间段的数据
    List<MobileCodeStatistical> getSendSmsNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getRegisterSendSmsNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getSendSmsSuccessNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getSendSmsFailNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getLoginSendSmsNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getUpdatePasswordSendSmsNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getUpdatePhoneSendSmsNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getUpdateAlipayAccountSendSmsNum(@Param("startDate") String startDate,@Param("endDate") String endDate);
    List<MobileCodeStatistical> getSendWithDrawSmsNum(@Param("startDate") String startDate,@Param("endDate") String endDate);

}
