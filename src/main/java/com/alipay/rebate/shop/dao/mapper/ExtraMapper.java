package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.Extra;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ExtraMapper extends Mapper<Extra> {

    List<Extra> selectExtra(@Param("theSameDay") Date theSameDay);

    int insertExtra(Extra extra);

    Extra selectNoComefromchlidrenExtra(@Param("theSameDay") Date theSameDay,@Param("comefromparent") String comefromparent);

    Extra selectHasComefromchlidrenExtra(@Param("theSameDay") Date theSameDay,@Param("comefromparent") String comefromparent,
                                         @Param("comefromchlidren") String comefromchlidren);

    Extra selectHasPriExtra(@Param("theSameDay") Date theSameDay,@Param("comefromparent") String comefromparent,
                            @Param("comefromchlidren") String comefromchlidren,@Param("pri") String pri);


    int updateNoComefromchlidren(@Param("theSameDay") Date theSameDay,@Param("udpateTime")Date udpateTime,
                          @Param("comefromparent") String comefromparent);


    int updateHasComefromchlidren(@Param("theSameDay") Date theSameDay,@Param("udpateTime")Date udpateTime,
                          @Param("comefromparent") String comefromparent,@Param("comefromchlidren") String comefromchlidren);


    int updateHasPriExtra(@Param("theSameDay") Date theSameDay,@Param("udpateTime")Date udpateTime,
                          @Param("comefromparent") String comefromparent,@Param("comefromchlidren") String comefromchlidren,
                          @Param("pri") String pri);

}