package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.SignInRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestMapping;

public interface SignInRecordMapper extends Mapper<SignInRecord> {

    SignInRecord selectYesterDayRecord(
        @Param("userId") Long userId,
        @Param("signInTime") String signInTime
    );

    void upateMoneyAndIntegralByUserId(
        @Param("userId") Long userId,
        @Param("startTime") String startTime,
        @Param("endTime") String endTime);

    SignInRecord selectToDayRecord(
        @Param("userId") Long userId
    );

    int deleteByUserId(Long userId);

}