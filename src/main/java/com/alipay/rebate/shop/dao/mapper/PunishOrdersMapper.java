package com.alipay.rebate.shop.dao.mapper;

import com.alipay.rebate.shop.core.Mapper;
import com.alipay.rebate.shop.model.PunishOrders;
import com.alipay.rebate.shop.pojo.order.PunishOrdersRsp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PunishOrdersMapper extends Mapper<PunishOrders> {
    PunishOrders getPunishOrdersByTradeId(String tradeId);
    List<PunishOrdersRsp> selectAllPunishOrders(@Param("tradeId") String tradeId,@Param("userId") Long userId);
}