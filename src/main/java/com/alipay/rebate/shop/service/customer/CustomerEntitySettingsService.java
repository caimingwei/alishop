package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.EntitySettings;
import com.alipay.rebate.shop.pojo.entitysettings.PcfSettings;
import java.io.IOException;


/**
 * Created by CodeGenerator on 2019/12/16.
 */
public interface CustomerEntitySettingsService extends Service<EntitySettings> {
  PcfSettings pcfSettingsDetail() throws IOException;
}
