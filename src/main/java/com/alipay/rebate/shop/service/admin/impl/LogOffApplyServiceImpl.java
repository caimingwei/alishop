package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.constants.LogOffApplyConstant;
import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.LogOffApplyMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.LogOffApply;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.logoffapply.LogoffApplySearchReq;
import com.alipay.rebate.shop.service.admin.LogOffApplyService;
import com.github.pagehelper.PageHelper;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2020/01/19.
 */
@Service
@Transactional
public class LogOffApplyServiceImpl extends AbstractService<LogOffApply> implements
    LogOffApplyService {
    @Resource
    private LogOffApplyMapper logOffApplyMapper;
    @Resource
    private UserMapper userMapper;

    @Override
    public List<LogOffApply> selectRecordByCondition(LogoffApplySearchReq req) {
        PageHelper.startPage(req.getPageNo(),req.getPageSize());
        return logOffApplyMapper.selectRecordByCondition(req);
    }

    @Override
    public int updateRecord(LogOffApply logOffApply) {
        logOffApplyMapper.updateByPrimaryKeySelective(logOffApply);
        if(LogOffApplyConstant.SUCCESS.equals(logOffApply.getStatus())){
            LogOffApply dbRecord = logOffApplyMapper.selectByPrimaryKey(logOffApply.getId());
            if(dbRecord == null){
                throw  new BusinessException("记录不存在", StatusCode.INTERNAL_ERROR);
            }
            User user = new User();
            user.setUserId(dbRecord.getUserId());
            user.setUserStatus(UserContant.USER_STATUS_LOGOFF);
            userMapper.updateUser(user);
        }
        return 0;
    }
}
