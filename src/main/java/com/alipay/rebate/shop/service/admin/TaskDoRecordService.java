package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.TaskDoRecord;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/10/17.
 */
public interface TaskDoRecordService extends Service<TaskDoRecord> {

}
