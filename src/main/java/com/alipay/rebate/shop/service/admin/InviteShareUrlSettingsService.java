package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.InviteShareUrlSettings;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/10/31.
 */
public interface InviteShareUrlSettingsService extends Service<InviteShareUrlSettings> {

}
