package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.constants.EntitySettingsConstant;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.EntitySettingsMapper;
import com.alipay.rebate.shop.model.EntitySettings;
import com.alipay.rebate.shop.pojo.entitysettings.IncomeRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.InviteNumRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.PcfSettings;
import com.alipay.rebate.shop.pojo.entitysettings.WithDrawSettings;
import com.alipay.rebate.shop.service.admin.EntitySettingsService;
import com.alipay.rebate.shop.service.customer.CustomerEntitySettingsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/12/16.
 */
@Service
@Transactional
public class CustomerEntitySettingsServiceImpl extends AbstractService<EntitySettings> implements
    CustomerEntitySettingsService {

    private Logger logger = LoggerFactory.getLogger(CustomerEntitySettingsServiceImpl.class);
    @Resource
    private EntitySettingsMapper entitySettingsMapper;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public PcfSettings pcfSettingsDetail() throws IOException {
        EntitySettings entitySettings = entitySettingsMapper.selectByType(
            EntitySettingsConstant.PCF_SETTINGS_TYPE
        );
        String json = entitySettings.getJson();
        PcfSettings pcfSettings = objectMapper.readValue(json,PcfSettings.class);
        return pcfSettings;
    }
}
