package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.ActivityPopMapper;
import com.alipay.rebate.shop.model.ActivityPop;
import com.alipay.rebate.shop.service.admin.ActivityPopService;
import com.alipay.rebate.shop.service.customer.CustomerActivityPopService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/09/05.
 */
@Service
@Transactional
public class CustomerActivityPopServiceImpl extends AbstractService<ActivityPop> implements
    CustomerActivityPopService {
    @Resource
    private ActivityPopMapper activityPopMapper;

}
