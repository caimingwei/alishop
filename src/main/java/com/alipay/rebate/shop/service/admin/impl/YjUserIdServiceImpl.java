package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.YjUserIdMapper;
import com.alipay.rebate.shop.model.YjUserId;
import com.alipay.rebate.shop.service.admin.YjUserIdService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/09/18.
 */
@Service
@Transactional
public class YjUserIdServiceImpl extends AbstractService<YjUserId> implements YjUserIdService {
    @Resource
    private YjUserIdMapper yjUserIdMapper;

}
