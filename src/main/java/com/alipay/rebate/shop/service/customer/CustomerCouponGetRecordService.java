package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.pojo.user.customer.UserCouponRsp;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/10/26.
 */
public interface CustomerCouponGetRecordService extends Service<CouponGetRecord> {

  List<UserCouponRsp> selectUserCouponList(Long userId);

  void openCoupon(Long id);

}
