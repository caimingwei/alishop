package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.ImgGroupMapper;
import com.alipay.rebate.shop.dao.mapper.ImgSpaceMapper;
import com.alipay.rebate.shop.model.ImgGroup;
import com.alipay.rebate.shop.service.admin.ImgGroupService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/02/02.
 */
@Service
@Transactional
public class ImgGroupServiceImpl extends AbstractService<ImgGroup> implements ImgGroupService {
    @Resource
    private ImgGroupMapper imgGroupMapper;
    @Resource
    private ImgSpaceMapper imgSpaceMapper;
    @Override
    public void updateImgGroup(ImgGroup imgGroup) {
        if(imgGroup == null) return;
        imgGroupMapper.updateByPrimaryKeySelective(imgGroup);
        imgSpaceMapper.updateGroupNameByGroupId(imgGroup.getId(),imgGroup.getName());
    }
}
