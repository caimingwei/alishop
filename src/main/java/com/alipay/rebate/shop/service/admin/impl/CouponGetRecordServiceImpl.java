package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.CouponGetRecordMapper;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.service.admin.CouponGetRecordService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/10/26.
 */
@Service
@Transactional
public class CouponGetRecordServiceImpl extends AbstractService<CouponGetRecord> implements
    CouponGetRecordService {
    @Resource
    private CouponGetRecordMapper couponGetRecordMapper;

}
