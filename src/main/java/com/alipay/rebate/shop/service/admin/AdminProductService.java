package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.Product;
import com.alipay.rebate.shop.model.ProductPageFramework;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import java.util.List;

import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

public interface AdminProductService extends Service<Product> {
    ResponseResult<Long> createOrUpdatePage(ProductPageFramework productPageFramework);
    PageInfo<ProductPageFramework> selectAllPageFramework(Integer page,Integer size,Integer pageCatagory);
    ResponseResult<ProductPageFramework> selectPageFrameworkByPageId(Integer pageId);
    void deletePageById(@Param("id")Integer id);
}
