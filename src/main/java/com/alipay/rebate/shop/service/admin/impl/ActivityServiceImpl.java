package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.ActivityMapper;
import com.alipay.rebate.shop.model.Activity;
import com.alipay.rebate.shop.service.admin.ActivityService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/08/25.
 */
@Service
@Transactional
public class ActivityServiceImpl extends AbstractService<Activity> implements ActivityService {
    @Resource
    private ActivityMapper activityMapper;

}
