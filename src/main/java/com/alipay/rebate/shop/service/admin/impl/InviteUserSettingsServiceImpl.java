package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.InviteUserSettingsMapper;
import com.alipay.rebate.shop.model.InviteUserSettings;
import com.alipay.rebate.shop.service.admin.InviteUserSettingsService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/12/15.
 */
@Service
@Transactional
public class InviteUserSettingsServiceImpl extends AbstractService<InviteUserSettings> implements
    InviteUserSettingsService {
    @Resource
    private InviteUserSettingsMapper inviteUserSettingsMapper;

}
