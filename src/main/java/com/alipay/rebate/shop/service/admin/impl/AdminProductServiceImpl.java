package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.ProductPageFrameworkMapper;
import com.alipay.rebate.shop.model.Product;
import com.alipay.rebate.shop.model.ProductPageFramework;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.AdminProductService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AdminProductServiceImpl extends AbstractService<Product> implements AdminProductService {
    private Logger logger = LoggerFactory.getLogger(AdminProductServiceImpl.class);

    @Autowired
    ProductPageFrameworkMapper productPageFrameworkMapper;

    @Override
    @Transactional
    public ResponseResult<Long> createOrUpdatePage(ProductPageFramework productPageFramework) {
        ResponseResult<Long> responseResult = new ResponseResult<>();

        if(productPageFramework.getProductPageId() == null){
            logger.debug("perform save action");
            // 如果页面id为空，那么进行保存
            long id = save(productPageFramework);
            responseResult.setData(id);
        }else{
            logger.debug("perform update action");
            // 如果页面id不为空，那么进行更新
            update(productPageFramework);
        }
        return responseResult;
    }

    @Transactional
    public long save(ProductPageFramework productPageFrameworkRequest){

        // 插入页面
        ProductPageFramework productPageFramework = new ProductPageFramework();
        productPageFramework.setBannerPicPath(productPageFrameworkRequest.getBannerPicPath());
        productPageFramework.setClassifyShow(productPageFrameworkRequest.getClassifyShow());
        productPageFramework.setCreateTime(new Date());
        productPageFramework.setDataSource(productPageFrameworkRequest.getDataSource());
        productPageFramework.setIsUse(productPageFrameworkRequest.getIsUse());
        productPageFramework.setPageName(productPageFrameworkRequest.getPageName());
        productPageFramework.setPageCatagory(productPageFrameworkRequest.getPageCatagory());
        productPageFramework.setShowRebate(productPageFrameworkRequest.getShowRebate());
        productPageFramework.setShowType(productPageFrameworkRequest.getShowType());
        productPageFramework.setUpdateTime(new Date());
        logger.debug("page frame work ready to save is : {}",productPageFramework);
        productPageFrameworkMapper.insertSelective(productPageFramework);
        logger.debug("productPageFramework:{}",productPageFramework);
        return productPageFramework.getProductPageId();

    }

    @Transactional
    public void update(ProductPageFramework productPageFrameworkRequest){
        // 更新页面
        productPageFrameworkRequest.setUpdateTime(new Date());
        logger.debug("page frame work ready to update is : {}",productPageFrameworkRequest);
        productPageFrameworkMapper.updateByPrimaryKeySelective(productPageFrameworkRequest);
    }


    @Override
    public PageInfo<ProductPageFramework> selectAllPageFramework(Integer page,Integer size,Integer pageCatagory) {
        logger.debug("page is : {}",page);
        logger.debug("size is : {}",size);
        logger.debug("pageCatagory is : {}",pageCatagory);
        page = page == null ? 1 : page;
        size = size == null ? 20 : size;
        PageHelper.startPage(page,size);
        List<ProductPageFramework> productPageFrameworks = productPageFrameworkMapper.selectProductPageFrameworkByPageCatagory(pageCatagory);
        PageInfo<ProductPageFramework> pageInfo = new PageInfo<>(productPageFrameworks);
        logger.debug("pageInfo is : {}",pageInfo);
        return pageInfo;
    }

//    @Override
//    public ResponseResult<List<ProductPageFramework>> selectAllPageFramework() {
//        ResponseResult<List<ProductPageFramework>> responseResult = new ResponseResult<>();
//        // 查询所有的页面模板
//        List<ProductPageFramework> pageFrameworks = productPageFrameworkMapper.selectAll();
//        logger.debug("All pageFromeworks is: {}",pageFrameworks);
//        List<ProductPageFramework> pageFrameworkResponses = new ArrayList<>();
//        Optional.ofNullable(pageFrameworks).ifPresent(pageFrameworkList -> {
//            pageFrameworks.forEach(pageFramework -> {
//                logger.debug("pageFrameworkResponse is: {}",pageFramework);
////        List<PageModule> pageModules = pageModuleMapper.selectByPagePrimaryKey(pageFramework.getPageId());
////        handlePageModuleList(pageModules);
////        pageFrameworkResponse.setPageModules(pageModules);
//                pageFrameworkResponses.add(pageFramework);
//            });
//        });
//        responseResult.setData(pageFrameworks);
//        return responseResult;
//    }

    @Override
    public ResponseResult<ProductPageFramework> selectPageFrameworkByPageId(Integer productPageId) {
        ResponseResult<ProductPageFramework> responseResult = new ResponseResult<>();
        // 查询所有的页面模板
        ProductPageFramework pageFramework = productPageFrameworkMapper.selectByPrimaryKey(productPageId);
        logger.debug("pageFromeworks is: {}",pageFramework);
        responseResult.setData(pageFramework);
        return responseResult;
    }

    @Override
    public void deletePageById(Integer id) {
        productPageFrameworkMapper.deletePageById(id);
    }


}
