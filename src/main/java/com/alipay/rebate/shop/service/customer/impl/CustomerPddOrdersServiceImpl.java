package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.PddOrdersMapper;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import com.alipay.rebate.shop.pojo.user.customer.PddOrdersListRes;
import com.alipay.rebate.shop.service.customer.CustomerPddOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/05/12.
 */
@Service
@Transactional
public class CustomerPddOrdersServiceImpl extends AbstractService<PddOrders> implements CustomerPddOrdersService {

    private Logger logger = LoggerFactory.getLogger(CustomerPddOrdersServiceImpl.class);

    @Resource
    private PddOrdersMapper pddOrdersMapper;

    @Override
    public PageInfo<OrdersListResponse> selectUserOrdersByType(Integer orderStatus, Integer pageNo, Integer pageSize,Long userId) {


        pageNo = pageNo == null ? 1 : pageNo;
        pageSize = pageSize == null ? 20: pageSize;
        logger.debug("ordersStatus is : {}",orderStatus);
        logger.debug("page is : {}",pageNo);
        logger.debug("size is : {}",pageSize);
        logger.debug("pId is : {}",userId);
        PageHelper.startPage(pageNo,pageSize);

        List<OrdersListResponse> pddOrdersListRes = pddOrdersMapper.selectUserOrdersByType(orderStatus, userId);

        PageInfo<OrdersListResponse> pageInfo = new PageInfo<>(pddOrdersListRes);

        return pageInfo;
    }
}
