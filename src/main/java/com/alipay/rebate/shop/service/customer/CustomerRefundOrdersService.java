package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.RefundOrders;


/**
 * Created by CodeGenerator on 2019/09/18.
 */
public interface CustomerRefundOrdersService extends Service<RefundOrders> {

}
