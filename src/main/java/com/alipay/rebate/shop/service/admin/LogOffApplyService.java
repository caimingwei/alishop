package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.LogOffApply;
import com.alipay.rebate.shop.pojo.logoffapply.LogoffApplySearchReq;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/01/19.
 */
public interface LogOffApplyService extends Service<LogOffApply> {

  List<LogOffApply> selectRecordByCondition(LogoffApplySearchReq req);
  int updateRecord(LogOffApply logOffApply);

}
