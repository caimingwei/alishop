package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.constants.TaskSettingsConstant;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.TaskSettingsMapper;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.service.admin.TaskSettingsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/10/09.
 */
@Service
@Transactional
public class TaskSettingsServiceImpl extends AbstractService<TaskSettings> implements
    TaskSettingsService {
    @Resource
    private TaskSettingsMapper taskSettingsMapper;

    @Override
    public void addTaskSettings(TaskSettings taskSettings) {
        Date date = new Date();
        taskSettings.setCreateTime(date);
        taskSettings.setUpdateTime(date);
        // 如果设置为正在使用状态,那么要把其他正在使用状态设置为空
        if(TaskSettingsConstant.IS_CURRENT_USE.equals(taskSettings.getIsUse())){
            taskSettingsMapper.setIsUseToNotUseBySubType(taskSettings.getSubType());
        }
        taskSettingsMapper.insert(taskSettings);
    }

    @Override
    public void updateTaskSettings(TaskSettings taskSettings) {
        taskSettings.setUpdateTime(new Date());
        // 如果设置为正在使用状态,那么要把其他正在使用状态设置为空
        if(TaskSettingsConstant.IS_CURRENT_USE.equals(taskSettings.getIsUse())){
            Integer subType = taskSettings.getSubType();
            if(subType == null){
                TaskSettings dbSettings = taskSettingsMapper.selectByPrimaryKey(taskSettings.getId());
                subType = dbSettings.getSubType();
            }
            taskSettingsMapper.setIsUseToNotUseBySubType(subType);
        }
        taskSettingsMapper.updateByPrimaryKeySelective(taskSettings);
    }

    @Override
    public List<TaskSettings> selectUseSettings() {
        return taskSettingsMapper.selectUseSettings();
    }

    @Override
    public PageInfo<TaskSettings> selectAllSettings(Integer pageNo, Integer pageSize, Integer type) {
        PageHelper.startPage(pageNo,pageSize);
        List<TaskSettings> page =   taskSettingsMapper.selectALLSettings(type);
        PageInfo<TaskSettings> pageInfo = new PageInfo<>(page);
        return pageInfo;
    }
}
