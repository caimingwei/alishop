package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.order.FindOrdersReq;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import com.github.pagehelper.PageInfo;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface CustomerOrdersService {

  int delete(String id);
  Orders get(String id);

  PageInfo<OrdersListResponse> selectUserOrdersByType(
      Long tkStatus,
      Integer pageNo,
      Integer pageSize,
      Long userId
  );
  PageInfo<OrdersListResponse> selectFansOrdersByType(
      Long tkStatus,
      Integer pageNo,
      Integer pageSize,
      Long userId
  );

  List<Orders> findOrder(FindOrdersReq req);

  List<Orders> selectOrderByGoodIdAndOrderType(@Param("goodId")String goodId,@Param("order_type")String orderType);

  PageInfo<OrdersListResponse> selectUserOrdersByTypeTwo(
          Long tkStatus,
          Integer type,
          Integer pageNo,
          Integer pageSize,
          Long userId
  ) throws ParseException;

  PageInfo<OrdersListResponse> selectFansOrdersByTypeTwo(
          Long tkStatus,
          Integer type,
          Integer pageNo,
          Integer pageSize,
          Long userId
  );

}
