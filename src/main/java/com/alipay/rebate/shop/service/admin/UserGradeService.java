package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.UserGrade;
import com.alipay.rebate.shop.core.Service;

import com.alipay.rebate.shop.pojo.usergrade.UserGradeReq;
import com.alipay.rebate.shop.pojo.usergrade.UserGradeRsp;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/08/16.
 */
public interface UserGradeService extends Service<UserGrade> {
    List<UserGrade> getAllSortByWeight();
    void deleteUserGrade(Integer id);
    void addUserGrade(UserGradeReq req);
    void updateUserGrade(UserGradeReq req);
    UserGradeRsp userGradeDetail(Integer id);
}
