package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.JdOrdersMapper;
import com.alipay.rebate.shop.model.JdOrders;
import com.alipay.rebate.shop.pojo.user.admin.JdOrdersReq;
import com.alipay.rebate.shop.service.admin.JdOrdersService;
import com.alipay.rebate.shop.core.AbstractService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/05/05.
 */
@Service
@Transactional
public class JdOrdersServiceImpl extends AbstractService<JdOrders> implements JdOrdersService {

    @Resource
    private JdOrdersMapper jdOrdersMapper;

    private Logger logger = LoggerFactory.getLogger(JdOrdersServiceImpl.class);

    @Override
    public PageInfo<JdOrders> selectOrdersByCondition(JdOrdersReq req, Integer pageNo, Integer pageSize) {
        logger.debug("pageNo is : {}",pageNo);
        logger.debug("pageSize is : {}",pageSize);
        PageHelper.startPage(pageNo,pageSize);
        List<JdOrders> jdOrders = jdOrdersMapper.selectOrdersByCondition(req);
        PageInfo<JdOrders> pageInfo = new PageInfo<>(jdOrders);
        return pageInfo;
    }
}
