package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.UserIncome;
import com.github.pagehelper.PageInfo;

public interface CustomerUserIncomeService extends Service<UserIncome> {

  PageInfo<UserIncome> selectUserAccountDetailIncome(Long userId,Integer pageNo, Integer pageSize);

  PageInfo<UserIncome> selectUserOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize);

  PageInfo<UserIncome> selectPddOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize);

  PageInfo<UserIncome> selectJdOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize);

  PageInfo<UserIncome> selectMeituanOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize);
}
