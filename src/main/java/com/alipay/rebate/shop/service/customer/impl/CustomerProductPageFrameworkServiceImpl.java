package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.model.ProductPageFramework;
import com.alipay.rebate.shop.service.customer.CustomerProductPageFrameworkService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerProductPageFrameworkServiceImpl extends AbstractService<ProductPageFramework>
implements CustomerProductPageFrameworkService {

}
