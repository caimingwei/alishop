package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.BrowseAd;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/09/08.
 */
public interface BrowseAdService extends Service<BrowseAd> {

}
