package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.TaskDoRecord;
import java.util.List;

public interface CustomerTaskDoRecordService extends Service<TaskDoRecord> {

  List<TaskDoRecord> selectNewUserAndDailyTask(Long userId);

  void addTaskDoRecord(TaskDoRecord taskDoRecord);

}
