package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.EntitySettings;
import com.alipay.rebate.shop.model.SignInSettings;
import com.alipay.rebate.shop.pojo.entitysettings.IncomeRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.InviteNumRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.PcfSettings;
import com.alipay.rebate.shop.pojo.entitysettings.WithDrawSettings;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;


/**
 * Created by CodeGenerator on 2019/12/16.
 */
public interface EntitySettingsService extends Service<EntitySettings> {

  void updateWithDrawSettings(WithDrawSettings withDrawSettings) throws JsonProcessingException;
  void updateInviteNumRankSettings(InviteNumRankSettings inviteNumRankSettings) throws JsonProcessingException;
  void updateIncomeRankSettings(IncomeRankSettings incomeRankSettings) throws JsonProcessingException;
  void updatePcfSettings(PcfSettings pcfSettings) throws JsonProcessingException;
  void updateCustomerSignInSetting(SignInSettings signInSettings) throws JsonProcessingException;
  WithDrawSettings withDrawSettingsDetail() throws IOException;
  InviteNumRankSettings inviteNumRankSettingsDetail() throws IOException;
  IncomeRankSettings incomeRankSettingsDetail() throws IOException;
  PcfSettings pcfSettingsDetail() throws IOException;
}
