package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.AppSkMapper;
import com.alipay.rebate.shop.model.AppSk;
import com.alipay.rebate.shop.service.admin.AppSkService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/08/28.
 */
@Service
@Transactional
public class AppSkServiceImpl extends AbstractService<AppSk> implements AppSkService {
    @Resource
    private AppSkMapper appSkMapper;

}
