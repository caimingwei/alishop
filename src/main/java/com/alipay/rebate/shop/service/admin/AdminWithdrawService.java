package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.pojo.alipay.WithDeawTotal;
import com.alipay.rebate.shop.pojo.user.admin.WithDrawPageRequest;
import com.alipay.rebate.shop.pojo.user.admin.WithDrawUto;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface AdminWithdrawService {

  int insert(WithDraw withDraw);
  int delete(Long id);
  int update(WithDraw withDraw);
  WithDraw get(Long id);
  List<WithDraw> selectWithDrawRecordByUserId(Long userId);

  PageInfo<WithDrawUto> selectWithDrawByCondition(WithDrawPageRequest withDrawPageRequest);

  Map selectApplyNumberByTime(@Param("startTime") Date startTime, @Param("endTime")Date endTime);

  int updateWithDrawRecord(WithDraw withDraw);

  WithDeawTotal selectWithDrawSuccessMoneyNum();


  PageInfo<WithDrawUto> selectNotHandleWithDraw(WithDrawPageRequest withDrawPageRequest);
  PageInfo<WithDrawUto> selectHandleWithDraw(WithDrawPageRequest withDrawPageRequest);
}
