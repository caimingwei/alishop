package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.ActivityProductMapper;
import com.alipay.rebate.shop.model.ActivityProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerActivityProductService extends AbstractService<ActivityProduct> implements
    com.alipay.rebate.shop.service.customer.CustomerActivityProductService {

  @Autowired
  ActivityProductMapper productMapper;

}
