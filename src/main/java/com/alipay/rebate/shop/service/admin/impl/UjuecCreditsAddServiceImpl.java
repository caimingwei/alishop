package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.UjuecCreditsAddMapper;
import com.alipay.rebate.shop.model.UjuecCreditsAdd;
import com.alipay.rebate.shop.service.admin.UjuecCreditsAddService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/09/25.
 */
@Service
@Transactional
public class UjuecCreditsAddServiceImpl extends AbstractService<UjuecCreditsAdd> implements UjuecCreditsAddService {
    @Resource
    private UjuecCreditsAddMapper ujuecCreditsAddMapper;

}
