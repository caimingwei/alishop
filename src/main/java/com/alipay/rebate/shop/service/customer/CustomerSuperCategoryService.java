package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.SuperCategory;
import com.alipay.rebate.shop.pojo.dataoke.SuperCategoryRes;
import com.alipay.rebate.shop.pojo.dataoke.SuperCategoryResp;

import java.util.List;


/**
 * Created by CodeGenerator on 2020/05/25.
 */
public interface CustomerSuperCategoryService extends Service<SuperCategory> {

    int insertOrUpdateSuperCategory();

    List<SuperCategory> selectAllSuperCategory();

}
