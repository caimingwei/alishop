package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.Product;
import com.alipay.rebate.shop.pojo.pdtframework.AppPdtFrameworkAndPdtRsp;
import com.alipay.rebate.shop.pojo.pdtframework.OfficialChoice;
import com.alipay.rebate.shop.pojo.pdtframework.OwnChoice;
import com.alipay.rebate.shop.pojo.pdtframework.ProductChoice;
import com.alipay.rebate.shop.pojo.pdtframework.ProductRepoChoice;
import com.alipay.rebate.shop.pojo.pdtframework.TaobaoJingPin;
import com.alipay.rebate.shop.pojo.user.customer.SearchProductRequest;
import com.alipay.rebate.shop.pojo.user.product.ActivityProductRsp;
import com.github.pagehelper.PageInfo;

public interface CustomerProductService extends Service<Product> {

  PageInfo<Product> searchByCondition(SearchProductRequest request);

  AppPdtFrameworkAndPdtRsp selectProductFrameworkAndProducts(
      Integer productPageId
  );

  PageInfo<Product> selectOwnChoiceProduct(
      OwnChoice ownChoice, Integer pageNo, Integer pageSize
  );

  PageInfo<Product> selectOfficialChoiceProduct(
      OfficialChoice officialChoice, Integer pageNo, Integer pageSize
  );

  PageInfo<Product> selectTaobaoJinpinProduct(
      TaobaoJingPin taobaoJingPin, Integer pageNo, Integer pageSize
  ) throws Exception;

  PageInfo<Product> selectProductChoiceProduct(
      ProductChoice productChoice, Integer pageNo, Integer pageSize
  );

  PageInfo<Product> selectProductRepoChoiceProduct(
      ProductRepoChoice productRepoChoice,Integer productRepoId, Integer pageNo,
      Integer pageSize
  );

  ActivityProductRsp selectDailyFreeActivityProduct(Integer pageNo, Integer pageSize);

}
