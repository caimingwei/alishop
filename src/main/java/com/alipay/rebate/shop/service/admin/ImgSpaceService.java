package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.ImgSpace;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.imgspace.GroupBPReq;
import com.alipay.rebate.shop.pojo.imgspace.SearchCondition;
import java.util.List;
import javax.persistence.Id;


/**
 * Created by CodeGenerator on 2020/02/02.
 */
public interface ImgSpaceService extends Service<ImgSpace> {

  void batchDelete(List<Long> ids);
  void batchUpdateGroup(GroupBPReq groupBPReq);
  List<ImgSpace> selectISByCondition(SearchCondition searchCondition);

}
