package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.constants.LuckyMonCouponConstant;
import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.CouponGetRecordMapper;
import com.alipay.rebate.shop.dao.mapper.LuckyMoneyCouponMapper;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.dao.mapper.WithDrawMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.helper.UserIncomeBuilder;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.model.LuckyMoneyCoupon;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.customer.UserCouponRsp;
import com.alipay.rebate.shop.service.customer.CustomerCouponGetRecordService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/10/26.
 */
@Service
@Transactional
public class CustomerCouponGetRecordServiceImpl extends AbstractService<CouponGetRecord> implements
    CustomerCouponGetRecordService {

    private Logger logger = LoggerFactory.getLogger(CustomerCouponGetRecordServiceImpl.class);
    @Resource
    private CouponGetRecordMapper couponGetRecordMapper;
    @Resource
    private LuckyMoneyCouponMapper luckyMoneyCouponMapper;
    @Resource
    private UserIncomeMapper userIncomeMapper;
    @Resource
    private WithDrawMapper withDrawMapper;
    @Resource
    private OrdersMapper ordersMapper;
    @Resource
    private UserMapper userMapper;

    @Override
    public List<UserCouponRsp> selectUserCouponList(Long userId) {
        return couponGetRecordMapper.selectCouponDetailByUserId(userId);
    }

    @Override
    public void openCoupon(Long id) {
        CouponGetRecord couponGetRecord = couponGetRecordMapper.selectByPrimaryKey(id);
        LuckyMoneyCoupon luckyMoneyCoupon = luckyMoneyCouponMapper.selectByPrimaryKey(couponGetRecord.getCouponId());
        Long userId = couponGetRecord.getUserId();
        if(couponGetRecord.getIsUse() == 1 ){
            throw  new BusinessException("红包已使用", StatusCode.LUCKY_MONEY_COUPON_ERROR);
        }
        Date now = new Date();
        if (now.getTime() > luckyMoneyCoupon.getEndTime().getTime()){
            throw  new BusinessException("红包卷过期", StatusCode.LUCKY_MONEY_COUPON_ERROR);
        }
        if (LuckyMonCouponConstant.CONDITION_TYPE_ORDER_REWARD.equals(luckyMoneyCoupon.getConditionType())){
            BigDecimal orderRewardMoney = userIncomeMapper.selectOrderRewardIncomeMoneyByUserId(userId);
            logger.debug("orderRewardMoney is : {}",orderRewardMoney);
            if(orderRewardMoney == null || orderRewardMoney.compareTo(luckyMoneyCoupon.getConditionMoney())<0){
                throw  new BusinessException("订单返利金额不符合要求", StatusCode.LUCKY_MONEY_COUPON_ERROR);
            }
            doOpenCoupon(userId,luckyMoneyCoupon,couponGetRecord);
        }
        if (LuckyMonCouponConstant.CONDITION_TYPE_WITHDRAW_MONEY.equals(luckyMoneyCoupon.getConditionType())){
            BigDecimal withdrawMoney = withDrawMapper.selectWithDrawMoneyByUserId(userId);
            logger.debug("withdrawMoney is : {}",withdrawMoney);
            if(withdrawMoney == null || withdrawMoney.compareTo(luckyMoneyCoupon.getConditionMoney())<0){
                throw  new BusinessException("提现金额不符合要求", StatusCode.LUCKY_MONEY_COUPON_ERROR);
            }
            doOpenCoupon(userId,luckyMoneyCoupon,couponGetRecord);
        }
        if (LuckyMonCouponConstant.CONDITION_TYPE_PAID_MONEY.equals(luckyMoneyCoupon.getConditionType())){
            BigDecimal paidMoney = ordersMapper.selectPaidMoneyByUserId(userId);
            logger.debug("paidMoney is : {}",paidMoney);
            if(paidMoney == null || paidMoney.compareTo(luckyMoneyCoupon.getConditionMoney())<0){
                throw  new BusinessException("付款金额不符合要求", StatusCode.LUCKY_MONEY_COUPON_ERROR);
            }
            doOpenCoupon(userId,luckyMoneyCoupon,couponGetRecord);
        }
    }

    private void doOpenCoupon(Long userId, LuckyMoneyCoupon luckyMoneyCoupon,CouponGetRecord couponGetRecord){
        userMapper.plusUserAM(luckyMoneyCoupon.getAmountOfMoney(),userId);
        UserIncome userIncome =  UserIncomeBuilder.buildLuckyMoneyUserIncome(luckyMoneyCoupon.getAmountOfMoney(),userId);
        userIncomeMapper.insertSelective(userIncome);
        CouponGetRecord nGetRecord = new CouponGetRecord();
        nGetRecord.setId(couponGetRecord.getId());
        nGetRecord.setIsUse(1);
        couponGetRecordMapper.updateByPrimaryKeySelective(nGetRecord);
    }
}
