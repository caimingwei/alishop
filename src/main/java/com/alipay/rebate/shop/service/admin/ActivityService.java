package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.Activity;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/08/25.
 */
public interface ActivityService extends Service<Activity> {

}
