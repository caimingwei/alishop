package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.CommissionLevelSettingsMapper;
import com.alipay.rebate.shop.model.CommissionLevelSettings;
import com.alipay.rebate.shop.service.admin.CommissionLevelSettingsService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/11/08.
 */
@Service
@Transactional
public class CommissionLevelSettingsServiceImpl extends AbstractService<CommissionLevelSettings> implements
    CommissionLevelSettingsService {
    @Resource
    private CommissionLevelSettingsMapper commissionLevelSettingsMapper;

    @Override
    public List<CommissionLevelSettings> findAllOrderByBegin() {
        return commissionLevelSettingsMapper.selectAllOrderByBegin();
    }
}
