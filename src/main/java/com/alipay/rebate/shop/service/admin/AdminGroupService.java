package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.AdminGroup;


/**
 * Created by CodeGenerator on 2019/12/08.
 */
public interface AdminGroupService extends Service<AdminGroup> {

}
