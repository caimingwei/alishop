package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.JpushHistory;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/12/05.
 */
public interface JpushHistoryService extends Service<JpushHistory> {

  List<JpushHistory> selectAllOrderByIdDesc();
}
