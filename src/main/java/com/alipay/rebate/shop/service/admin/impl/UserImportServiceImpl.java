package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.UserImportMapper;
import com.alipay.rebate.shop.model.UserImport;
import com.alipay.rebate.shop.service.admin.UserImportService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/09/06.
 */
@Service
@Transactional
public class UserImportServiceImpl extends AbstractService<UserImport> implements UserImportService {
    @Resource
    private UserImportMapper userImportMapper;

}
