package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.BrowseAdMapper;
import com.alipay.rebate.shop.model.BrowseAd;
import com.alipay.rebate.shop.service.customer.CustomerBrowseAdService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/09/08.
 */
@Service
@Transactional
public class CustomerBrowseAdServiceImpl extends AbstractService<BrowseAd> implements
    CustomerBrowseAdService {
    @Resource
    private BrowseAdMapper browseAdMapper;

}
