package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.YjUserId;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2020/09/18.
 */
public interface YjUserIdService extends Service<YjUserId> {

}
