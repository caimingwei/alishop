package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.admin.UserIncomeRsp;

import java.util.List;

public interface AdminUserIncomeService extends Service<UserIncome> {
    List<UserIncome> selectAllIncome();
    List<UserIncomeRsp> selectAllByCondition(UserIncomeRsp userIncome);
}
