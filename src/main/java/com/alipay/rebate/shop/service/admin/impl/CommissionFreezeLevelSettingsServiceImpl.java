package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.CommissionFreezeLevelSettingsMapper;
import com.alipay.rebate.shop.model.CommissionFreezeLevelSettings;
import com.alipay.rebate.shop.service.admin.CommissionFreezeLevelSettingsService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/11/08.
 */
@Service
@Transactional
public class CommissionFreezeLevelSettingsServiceImpl extends AbstractService<CommissionFreezeLevelSettings> implements
    CommissionFreezeLevelSettingsService {
    @Resource
    private CommissionFreezeLevelSettingsMapper commissionFreezeLevelSettingsMapper;

    @Override
    public List<CommissionFreezeLevelSettings> findAllOrderByBegin() {
        return commissionFreezeLevelSettingsMapper.selectAllOrderByBegin();
    }
}
