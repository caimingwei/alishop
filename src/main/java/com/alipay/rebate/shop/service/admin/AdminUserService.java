package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.AdminUser;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.AdminIndexRsp;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.TokenRsp;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserObjRsp;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserRewordReq;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import com.alipay.rebate.shop.pojo.user.customer.UserPersonalCenterResponse;
import com.alipay.rebate.shop.service.common.UserService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.subject.Subject;


/**
 * Created by CodeGenerator on 2019/12/08.
 */
public interface AdminUserService extends Service<AdminUser>, UserService {

  void addNewAdminUser(AdminUser adminUser);

  void updateAdminUser(AdminUser adminUser);

  List<AdminUserObjRsp> selectAllWithGroupMsg(String account);

  AdminUserObjRsp adminLogin(
      UserDto loginInfo,
      HttpServletRequest request,
      HttpServletResponse response,
      Subject subject
  );

  UserDto getUserDtoInfoForAdminLogin(String phone);

  AdminIndexRsp selectIndexInfo();

  TokenRsp getNewToken(long userId);

  int rewordUser(AdminUserRewordReq req,int type);

}
