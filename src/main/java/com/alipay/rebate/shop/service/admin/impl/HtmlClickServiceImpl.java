package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.HtmlClickMapper;
import com.alipay.rebate.shop.model.HtmlClick;
import com.alipay.rebate.shop.service.admin.HtmlClickService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/05/04.
 */
@Service
@Transactional
public class HtmlClickServiceImpl extends AbstractService<HtmlClick> implements HtmlClickService {
    @Resource
    private HtmlClickMapper htmlClickMapper;

}
