package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.LotteryTimesRecord;
import com.alipay.rebate.shop.pojo.user.customer.PlusOrReduceVirMoneyReq;


/**
 * Created by CodeGenerator on 2019/10/22.
 */
public interface CustomerLotteryTimesRecordService extends Service<LotteryTimesRecord> {

  Integer addOrUpdateCurDayTimes(Long userId, PlusOrReduceVirMoneyReq req);
  Integer selectCurdayTimes(Long userId);
}
