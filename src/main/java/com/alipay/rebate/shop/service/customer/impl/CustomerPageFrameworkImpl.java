package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.PageFrameworkMapper;
import com.alipay.rebate.shop.model.PageFramework;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkResponse;
import com.alipay.rebate.shop.service.customer.CustomerPageFrameworkService;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerPageFrameworkImpl extends AbstractService<PageFramework> implements
    CustomerPageFrameworkService {

  private Logger logger = LoggerFactory.getLogger(CustomerPageFrameworkImpl.class);

  @Resource
  private PageFrameworkMapper pageFrameworkMapper;

  @Override
  @Transactional
  public PageFrameworkResponse selectDefaultPageFramework() {
    // 查询所有的页面模板
    PageFramework pageFramework = pageFrameworkMapper.selectDefaultPageFramework();
    PageFrameworkResponse pageFrameworkResponse = null;
    // 获取所有模块信息
    if(pageFramework != null){
      pageFrameworkResponse = new PageFrameworkResponse();
      converPageFrameworkToResponse(pageFrameworkResponse,pageFramework);
      logger.debug("DefaultPageFramework is: {}",pageFrameworkResponse);
//      List<PageModule> pageModules = pageModuleMapper.selectByPagePrimaryKey(pageFramework.getPageId());
//      handlePageModuleList(pageModules);
//      pageFrameworkResponse.setPageModules(pageModules);
    }
    return pageFrameworkResponse;
  }

  @Override
  public PageFrameworkResponse selectIOSPageFramework() {

    // 查询所有的页面模板
    PageFramework pageFramework = pageFrameworkMapper.selectIOSPageFramework();
    PageFrameworkResponse pageFrameworkResponse = null;
    // 获取所有模块信息
    if(pageFramework != null){
      pageFrameworkResponse = new PageFrameworkResponse();
      converPageFrameworkToResponse(pageFrameworkResponse,pageFramework);
      logger.debug("DefaultPageFramework is: {}",pageFrameworkResponse);
    }
    return pageFrameworkResponse;
  }

  private PageFrameworkResponse converPageFrameworkToResponse(
      PageFrameworkResponse pageFrameworkResponse,
      PageFramework pageFramework){
    pageFrameworkResponse.setPageId(pageFramework.getPageId());
    pageFrameworkResponse.setTitle(pageFramework.getTitle());
    pageFrameworkResponse.setIllustration(pageFramework.getIllustration());
    pageFrameworkResponse.setCreateTime(pageFramework.getCreateTime());
    pageFrameworkResponse.setUpdateTime(pageFramework.getUpdateTime());
    pageFrameworkResponse.setIsDefault(pageFramework.getIsDefault());
    pageFrameworkResponse.setIsCurrentUse(pageFramework.getIsCurrentUse());
    pageFrameworkResponse.setPageModules(pageFramework.getPageModules());
    pageFrameworkResponse.setStartColor(pageFramework.getStartColor());
    pageFrameworkResponse.setEndColor(pageFramework.getEndColor());
    return pageFrameworkResponse;
  }
}
