package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.JpushHistoryMapper;
import com.alipay.rebate.shop.dao.mapper.JpushSettingsMapper;
import com.alipay.rebate.shop.model.JpushHistory;
import com.alipay.rebate.shop.model.JpushSettings;
import com.alipay.rebate.shop.service.admin.JpushSettingsService;
import com.alipay.rebate.shop.utils.JpushUtils;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/11/30.
 */
@Service
@Transactional
public class JpushSettingsServiceImpl extends AbstractService<JpushSettings> implements
    JpushSettingsService {
    @Resource
    private JpushSettingsMapper jpushSettingsMapper;
    @Resource
    private JpushHistoryMapper jpushHistoryMapper;

    @Override
    public List<JpushSettings> selectAllOrderById() {
        return jpushSettingsMapper.selectAllOrderById();
    }

    @Override
    public void pushToAllUser(JpushSettings jpushSettings) {
        JpushUtils.sendPushWithCallback(
            jpushSettings.getAppContent(),
            jpushSettings.getAppIntent(),
            jpushSettings.getAppUriActivity(),
            jpushSettings.getAppUriAction(),
            jpushSettings.getAppExtrasJson()
        );

        JpushHistory jpushHistory = new JpushHistory();
        jpushHistory.setContent(jpushSettings.getAppContent());
        jpushHistory.setExtrasJson(jpushSettings.getAppExtrasJson());
        jpushHistory.setCreateTime(new Date());
        jpushHistoryMapper.insertSelective(jpushHistory);

    }
}
