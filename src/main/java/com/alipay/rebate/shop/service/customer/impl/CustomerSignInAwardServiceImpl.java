package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.dao.mapper.SignInAwardMapper;
import com.alipay.rebate.shop.dao.mapper.SignInRecordMapper;
import com.alipay.rebate.shop.model.SignInAward;
import com.alipay.rebate.shop.model.SignInRecord;
import com.alipay.rebate.shop.model.SignInSettings;
import com.alipay.rebate.shop.pojo.user.customer.SignInSettingAwardRsp;
import com.alipay.rebate.shop.service.customer.CustomerSignInAwardService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Random;


/**
 * Created by CodeGenerator on 2019/09/21.
 */
@Service
@Transactional
public class CustomerSignInAwardServiceImpl implements CustomerSignInAwardService {
    @Resource
    private SignInAwardMapper signInAwardMapper;
    @Resource
    private SignInRecordMapper signInRecordMapper;

    public SignInSettingAwardRsp getSignInSettingAwardRsp(Long userId){
        SignInSettingAwardRsp rsp = new SignInSettingAwardRsp();
        // TODO entity_settings
        SignInSettings signInSettings = null;
        rsp.setAwardType(signInSettings.getAwardType());
        rsp.setRuleDesc(signInSettings.getRuleDesc());
        rsp.setUserId(userId);
        SignInAward signInAward = signInAwardMapper.selectByUserId(userId);
        if(signInAward != null){
            rsp.setNewlySignInTime(signInAward.getNewlySignInTime());
            rsp.setSignInDays(signInAward.getSignInDays());
            rsp.setTotalAwardMoney(signInAward.getTotalAwardMoney());
            rsp.setTotalIntegralMoney(signInAward.getTotalAwardIntegral());
        }
        // 固定式
        if(signInSettings.getAwardForm() == 1){
            rsp.setNum(signInSettings.getFixAwardNum());
        }
        // 随机式
        if(signInSettings.getAwardForm() == 2){
            Integer minNum = signInSettings.getMinAwardNum();
            Integer maxNum = signInSettings.getMaxAwardNum();
            Random random = new Random();
            int randomNum = random.nextInt(maxNum) % (maxNum - minNum + 1) + minNum;
            rsp.setNum(randomNum);
        }
        SignInRecord signInRecord = signInRecordMapper.selectToDayRecord(userId);
        if(signInRecord != null){
            rsp.setSignToday(true);
        }
        return rsp;
    }

}
