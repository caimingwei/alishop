package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.ImgGroup;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2020/02/02.
 */
public interface ImgGroupService extends Service<ImgGroup> {

  void updateImgGroup(ImgGroup imgGroup);

}
