package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.CommissionFreezeLevelSettings;
import com.alipay.rebate.shop.core.Service;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/11/08.
 */
public interface CommissionFreezeLevelSettingsService extends Service<CommissionFreezeLevelSettings> {

  List<CommissionFreezeLevelSettings> findAllOrderByBegin();
}
