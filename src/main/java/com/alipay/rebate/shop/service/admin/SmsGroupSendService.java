package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.SmsGroupSend;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.smsgoupsend.SmsGroupSendSearchReq;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/01/04.
 */
public interface SmsGroupSendService extends Service<SmsGroupSend> {

  void addSmsGroupSend(SmsGroupSend smsGroupSend);
  void deleteSmsGroupSend(Integer id);
  void updateSmsGroupSend(SmsGroupSend smsGroupSend);
  List<SmsGroupSend> searchByCondition(SmsGroupSendSearchReq req);
}
