package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.model.MobileCodeInformation;
import com.alipay.rebate.shop.pojo.mobilecode.CodePageInformation;
import com.github.pagehelper.PageInfo;

public interface MobileCodeInformationService {

    PageInfo<MobileCodeInformation> selectAllMobileCodeInforMation(CodePageInformation pageInformation);

}
