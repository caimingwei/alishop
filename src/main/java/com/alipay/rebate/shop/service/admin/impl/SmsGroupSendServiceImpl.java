package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.constants.SmsGroupSendConstant;
import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.SmsGroupSendMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.helper.SmsMessageSender;
import com.alipay.rebate.shop.model.SmsGroupSend;
import com.alipay.rebate.shop.pojo.smsgoupsend.SmsGroupSendSearchReq;
import com.alipay.rebate.shop.service.admin.SmsGroupSendService;
import com.github.pagehelper.PageHelper;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2020/01/04.
 */
@Service
@Transactional
public class SmsGroupSendServiceImpl extends AbstractService<SmsGroupSend> implements
    SmsGroupSendService {

    private Logger logger = LoggerFactory.getLogger(SmsGroupSendServiceImpl.class);

    @Resource
    private SmsGroupSendMapper smsGroupSendMapper;
    @Resource
    private SmsMessageSender smsMessageSender;
    @Resource
    @Qualifier("sharePool")
    private ExecutorService executorService;

    @Override
    public void addSmsGroupSend(SmsGroupSend smsGroupSend) {
        logger.debug("SmsGroupSend obj is : {}",smsGroupSend);
        smsGroupSend.setStatus(SmsGroupSendConstant.NOTSTART_STATUS);
        Date date = new Date();
        smsGroupSend.setCreateTime(date);
        smsGroupSend.setUpdateTime(date);
        smsGroupSendMapper.insertSelective(smsGroupSend);
        // 立即发送类型
        if(smsGroupSend.getSendType() == SmsGroupSendConstant.RIGHT_NOW_SEND_TYPE){
            CompletableFuture.runAsync(()->{
                smsMessageSender.sendSms(smsGroupSend);
            },executorService);
        }
    }

    @Override
    public void deleteSmsGroupSend(Integer id) {
        SmsGroupSend smsGroupSend = smsGroupSendMapper.selectByPrimaryKey(id);
        if(smsGroupSend == null){
            return;
        }
        if(smsGroupSend.getStatus() == SmsGroupSendConstant.SENDING_STATUS){
            throw new BusinessException("发送中", StatusCode.INTERNAL_ERROR);
        }
        smsGroupSendMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateSmsGroupSend(SmsGroupSend smsGroupSend) {
        SmsGroupSend dbRecord = smsGroupSendMapper.selectByPrimaryKey(smsGroupSend.getId());
        logger.debug("dbRecord is {}",dbRecord);
        if(dbRecord == null){
            throw new BusinessException("记录不存在", StatusCode.INTERNAL_ERROR);
        }
        if(dbRecord.getSendType() == SmsGroupSendConstant.RIGHT_NOW_SEND_TYPE){
            throw new BusinessException("立即发送类型不能修改", StatusCode.INTERNAL_ERROR);
        }
        if(dbRecord.getStatus() == SmsGroupSendConstant.SENDING_STATUS){
            throw new BusinessException("发送中", StatusCode.INTERNAL_ERROR);
        }
        if(dbRecord.getStatus() == SmsGroupSendConstant.COMPLETE_STATUS){
            throw new BusinessException("发送完成", StatusCode.INTERNAL_ERROR);
        }
        smsGroupSendMapper.updateByPrimaryKeySelective(smsGroupSend);
        if(smsGroupSend.getSendType() == SmsGroupSendConstant.RIGHT_NOW_SEND_TYPE){
            CompletableFuture.runAsync(()->{
                smsMessageSender.sendSms(smsGroupSend);
            },executorService);
        }
    }

    @Override
    public List<SmsGroupSend> searchByCondition(SmsGroupSendSearchReq req) {
        PageHelper.startPage(req.getPageNo(), req.getPageSize());
        return smsGroupSendMapper.selectSmsGroupSendByCondition(req);
    }
}
