package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.UserJdPidMapper;
import com.alipay.rebate.shop.model.UserJdPid;
import com.alipay.rebate.shop.service.admin.UserJdPidService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/04/28.
 */
@Service
@Transactional
public class UserJdPidServiceImpl extends AbstractService<UserJdPid> implements UserJdPidService {
    @Resource
    private UserJdPidMapper userJdPidMapper;

}
