package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.Extra;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2020/04/28.
 */
public interface ExtraService extends Service<Extra> {

}
