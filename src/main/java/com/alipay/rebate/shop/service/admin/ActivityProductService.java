package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.ActivityProduct;
import com.alipay.rebate.shop.core.Service;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Created by CodeGenerator on 2019/08/25.
 */
public interface ActivityProductService extends Service<ActivityProduct> {
    void deleteByActivityId(@Param("activityId")Integer activityId);
    List<ActivityProduct> getProductsByActivityId(@Param("activityId")Integer activityId);
    void updateActivityProduct();
}
