package com.alipay.rebate.shop.service.admin;

import com.alibaba.fastjson.JSONObject;
import com.alipay.rebate.shop.pojo.user.admin.UpdateUserRequest;
import com.alipay.rebate.shop.pojo.user.admin.UserListResponse;
import com.alipay.rebate.shop.pojo.user.admin.UserPageRequest;
import com.alipay.rebate.shop.pojo.user.customer.UpdateUserStatusReq;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import com.alipay.rebate.shop.service.common.UserService;
import com.github.pagehelper.PageInfo;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface AdminCustomerUserService extends UserService {

  PageInfo<UserListResponse> searchUserByCondition(
      UserPageRequest userPageRequest);
  UserDto getUserDtoInfoForAdminLogin(String phone);
//  ResponseResult<UserPersonalCenterResponse> adminLogin(UserDto loginInfo, HttpServletResponse response, Subject subject);
  int selectTotalCountByCondition(@Param("map") Map<String,Object> map);
  void cancelTaobaoAuth(Long id);

  int updateUserById(UpdateUserRequest updateUserRequest);
  int updateUserStatus(UpdateUserStatusReq req);
}
