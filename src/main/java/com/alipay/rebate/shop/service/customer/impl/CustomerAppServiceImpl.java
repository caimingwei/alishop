package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.constants.EntitySettingsConstant;
import com.alipay.rebate.shop.dao.mapper.EntitySettingsMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.EntitySettings;
import com.alipay.rebate.shop.pojo.entitysettings.IncomeRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.InrUser;
import com.alipay.rebate.shop.pojo.entitysettings.InviteNumRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.IrUser;
import com.alipay.rebate.shop.service.customer.CustomerAppService;
import com.alipay.rebate.shop.utils.ListUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CustomerAppServiceImpl implements CustomerAppService {

  @Resource
  private EntitySettingsMapper entitySettingsMapper;
  @Resource
  private UserMapper userMapper;
  @Resource
  private UserIncomeMapper userIncomeMapper;
  private Logger logger = LoggerFactory.getLogger(CustomerAppServiceImpl.class);

  @Resource
  private ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public List<InrUser> inviteNumRank() throws IOException {
    EntitySettings entitySettings = entitySettingsMapper.selectByType(
        EntitySettingsConstant.INVITENUM_SETTINGS_TYPE
    );
    String json = entitySettings.getJson();
    InviteNumRankSettings inviteNumRankSettings = objectMapper.readValue(json,InviteNumRankSettings.class);
    Integer realUserNum = inviteNumRankSettings.getRealUserNum();
    logger.debug("realUserNum is : {}",realUserNum);
    if (realUserNum == null){
      realUserNum = 0;
    }
    List<InrUser> realInrUser = userMapper.selectInrRankUser(realUserNum,inviteNumRankSettings.getStartTime(),inviteNumRankSettings.getEndTime());

    if(!ListUtil.isEmptyList(inviteNumRankSettings.getInrUsers())){
      realInrUser.addAll(inviteNumRankSettings.getInrUsers());
    }

    Collections.sort(realInrUser);
    Collections.reverse(realInrUser);
    return realInrUser;
  }

  @Override
  public List<IrUser> incomeRank() throws IOException {

    EntitySettings entitySettings = entitySettingsMapper.selectByType(
        EntitySettingsConstant.INCOME_SETTINGS_TYPE
    );
    String json = entitySettings.getJson();
    IncomeRankSettings incomeRankSettings = objectMapper.readValue(json,IncomeRankSettings.class);
    Integer realUserNum = incomeRankSettings.getRealUserNum();
    if (realUserNum == null){
      realUserNum = 0;
    }
    List<IrUser> realIrUser = userIncomeMapper.selectIrRankUser(realUserNum,incomeRankSettings.getStartTime(),incomeRankSettings.getEndTime());

    if(!ListUtil.isEmptyList(incomeRankSettings.getIrUsers())){
      realIrUser.addAll(incomeRankSettings.getIrUsers());
    }

    Collections.sort(realIrUser);
    Collections.reverse(realIrUser);
    return realIrUser;
  }
}
