package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.core.Service;
import com.github.pagehelper.PageInfo;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/10/09.
 */
public interface TaskSettingsService extends Service<TaskSettings> {

  void addTaskSettings(TaskSettings taskSettings);
  void updateTaskSettings(TaskSettings taskSettings);
  List<TaskSettings> selectUseSettings();
  PageInfo<TaskSettings> selectAllSettings(Integer pageNo, Integer pageSize,Integer type);
}
