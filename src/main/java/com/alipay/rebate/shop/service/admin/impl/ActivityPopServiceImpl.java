package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.ActivityPopMapper;
import com.alipay.rebate.shop.model.ActivityPop;
import com.alipay.rebate.shop.service.admin.ActivityPopService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/09/05.
 */
@Service
@Transactional
public class ActivityPopServiceImpl extends AbstractService<ActivityPop> implements ActivityPopService {
    @Resource
    private ActivityPopMapper activityPopMapper;

}
