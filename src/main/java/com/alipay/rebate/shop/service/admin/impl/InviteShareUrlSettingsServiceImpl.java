package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.InviteShareUrlSettingsMapper;
import com.alipay.rebate.shop.model.InviteShareUrlSettings;
import com.alipay.rebate.shop.service.admin.InviteShareUrlSettingsService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/10/31.
 */
@Service
@Transactional
public class InviteShareUrlSettingsServiceImpl extends AbstractService<InviteShareUrlSettings> implements
    InviteShareUrlSettingsService {
    @Resource
    private InviteShareUrlSettingsMapper inviteShareUrlSettingsMapper;

}
