package com.alipay.rebate.shop.service.common;

import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.user.TokenRsp;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import java.util.List;

public interface UserService {

    Long insert(User user);
    int delete(Long id);
    int update(User user);
    User get(Long id);

    List<String> getUserRoles(Long userId);
    UserDto getJwtTokenInfo(String id);


}
