package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.constants.EntitySettingsConstant;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.EntitySettingsMapper;
import com.alipay.rebate.shop.model.EntitySettings;
import com.alipay.rebate.shop.model.SignInSettings;
import com.alipay.rebate.shop.pojo.entitysettings.IncomeRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.InviteNumRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.PcfSettings;
import com.alipay.rebate.shop.pojo.entitysettings.WithDrawSettings;
import com.alipay.rebate.shop.service.admin.EntitySettingsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;


/**
 * Created by CodeGenerator on 2019/12/16.
 */
@Service
@Transactional
public class EntitySettingsServiceImpl extends AbstractService<EntitySettings> implements
    EntitySettingsService {

    private Logger logger = LoggerFactory.getLogger(EntitySettingsServiceImpl.class);
    @Resource
    private EntitySettingsMapper entitySettingsMapper;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void updateCustomerSignInSetting(SignInSettings signInSettings)
            throws JsonProcessingException {

        EntitySettings entitySettings = new EntitySettings();
        entitySettings.setUpdateTime(new Date());
        String json = objectMapper.writeValueAsString(signInSettings);
        logger.debug("json is : {}",json);
        entitySettings.setJson(json);
        entitySettings.setType(EntitySettingsConstant.SIGN_IN_SETTINGS_TYPE);
        logger.debug("EntitySettings is : {}",entitySettings);
        entitySettingsMapper.updateByType(entitySettings);
    }

    @Override
    public void updateWithDrawSettings(WithDrawSettings withDrawSettings)
        throws JsonProcessingException {
        EntitySettings entitySettings = new EntitySettings();
        entitySettings.setUpdateTime(new Date());
        String json = objectMapper.writeValueAsString(withDrawSettings);
        logger.debug("json is : {}",json);
        entitySettings.setJson(json);
        entitySettings.setType(EntitySettingsConstant.WITHDRAW_SETTINGS_TYPE);
        logger.debug("EntitySettings is : {}",entitySettings);
        entitySettingsMapper.updateByType(entitySettings);
    }

    @Override
    public WithDrawSettings withDrawSettingsDetail() throws IOException {
        EntitySettings entitySettings = entitySettingsMapper.selectByType(
            EntitySettingsConstant.WITHDRAW_SETTINGS_TYPE
        );
        String json = entitySettings.getJson();
        WithDrawSettings withDrawSettings = objectMapper.readValue(json,WithDrawSettings.class);
        return withDrawSettings;
    }

    @Override
    public void updateInviteNumRankSettings(InviteNumRankSettings inviteNumRankSettings)
        throws JsonProcessingException {
        EntitySettings entitySettings = new EntitySettings();
        entitySettings.setUpdateTime(new Date());
        String json = objectMapper.writeValueAsString(inviteNumRankSettings);
        logger.debug("json is : {}",json);
        entitySettings.setJson(json);
        entitySettings.setType(EntitySettingsConstant.INVITENUM_SETTINGS_TYPE);
        logger.debug("EntitySettings is : {}",entitySettings);
        entitySettingsMapper.updateByType(entitySettings);
    }

    @Override
    public void updateIncomeRankSettings(IncomeRankSettings incomeRankSettings)
        throws JsonProcessingException {
        EntitySettings entitySettings = new EntitySettings();
        entitySettings.setUpdateTime(new Date());
        String json = objectMapper.writeValueAsString(incomeRankSettings);
        logger.debug("json is : {}",json);
        entitySettings.setJson(json);
        entitySettings.setType(EntitySettingsConstant.INCOME_SETTINGS_TYPE);
        logger.debug("EntitySettings is : {}",entitySettings);
        entitySettingsMapper.updateByType(entitySettings);
    }

    @Override
    public InviteNumRankSettings inviteNumRankSettingsDetail() throws IOException {
        EntitySettings entitySettings = entitySettingsMapper.selectByType(
            EntitySettingsConstant.INVITENUM_SETTINGS_TYPE
        );
        String json = entitySettings.getJson();
        InviteNumRankSettings inviteNumRankSettings = objectMapper.readValue(json,InviteNumRankSettings.class);
        return inviteNumRankSettings;
    }

    @Override
    public IncomeRankSettings incomeRankSettingsDetail() throws IOException {
        EntitySettings entitySettings = entitySettingsMapper.selectByType(
            EntitySettingsConstant.INCOME_SETTINGS_TYPE
        );
        String json = entitySettings.getJson();
        IncomeRankSettings incomeRankSettings = objectMapper.readValue(json,IncomeRankSettings.class);
        return incomeRankSettings;
    }

    @Override
    public void updatePcfSettings(PcfSettings pcfSettings) throws JsonProcessingException {
        EntitySettings entitySettings = new EntitySettings();
        entitySettings.setUpdateTime(new Date());
        String json = objectMapper.writeValueAsString(pcfSettings);
        logger.debug("json is : {}",json);
        entitySettings.setJson(json);
        entitySettings.setType(EntitySettingsConstant.PCF_SETTINGS_TYPE);
//        entitySettings.setStartColor(pcfSettings.getStartColor());
//        entitySettings.setEndColor(pcfSettings.getEndColor());
//        entitySettings.setIncomeStartColor(pcfSettings.getIncomeStartColor());
//        entitySettings.setIncomeEndColor(pcfSettings.getIncomeEndColor());
        logger.debug("EntitySettings is : {}",entitySettings);
        entitySettingsMapper.updateByType(entitySettings);
    }

    @Override
    public PcfSettings pcfSettingsDetail() throws IOException {
        EntitySettings entitySettings = entitySettingsMapper.selectByType(
            EntitySettingsConstant.PCF_SETTINGS_TYPE
        );
        String json = entitySettings.getJson();
        PcfSettings pcfSettings = objectMapper.readValue(json,PcfSettings.class);
        return pcfSettings;
    }

}
