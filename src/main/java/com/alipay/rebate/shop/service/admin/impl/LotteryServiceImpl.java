package com.alipay.rebate.shop.service.admin.impl;
import com.alipay.rebate.shop.constants.LotteryConstant;
import com.alipay.rebate.shop.dao.mapper.LotteryMapper;
import com.alipay.rebate.shop.model.Lottery;
import com.alipay.rebate.shop.service.admin.LotteryService;
import com.alipay.rebate.shop.core.AbstractService;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/08/13.
 */
@Service
@Transactional
public class LotteryServiceImpl extends AbstractService<Lottery> implements LotteryService {

    private Logger logger = LoggerFactory.getLogger(LotteryServiceImpl.class);
    @Resource
    private LotteryMapper lotteryMapper;

    public void create(Lottery lottery){
        logger.debug("create method");
        lottery.setCreateTime(new Date());
        lottery.setUpdateTime(new Date());
        // 如果设置了当前正在使用,那么要取消掉其他当前正在使用的标志
        if(LotteryConstant.IS_CURRENT_USE.equals(lottery.getIsCurrentUse())){
            lotteryMapper.resetCurrentUseFlag();
        }
        save(lottery);
    }

    public void updateLottery(Lottery lottery){
        logger.debug("update method");
        lottery.setUpdateTime(new Date());
        // 如果设置了当前正在使用,那么要取消掉其他当前正在使用的标志
        if(LotteryConstant.IS_CURRENT_USE.equals(lottery.getIsCurrentUse())){
            lotteryMapper.resetCurrentUseFlag();
        }
        update(lottery);
    }

}
