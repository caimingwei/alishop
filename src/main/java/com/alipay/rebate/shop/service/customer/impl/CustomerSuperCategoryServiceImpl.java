package com.alipay.rebate.shop.service.customer.impl;

import com.alibaba.fastjson.JSON;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.SuperCategoryMapper;
import com.alipay.rebate.shop.model.SuperCategory;
import com.alipay.rebate.shop.pojo.dataoke.SuperCategoryRes;
import com.alipay.rebate.shop.pojo.dataoke.SuperCategoryResp;
import com.alipay.rebate.shop.pojo.dataoke.SuperCategoryResponse;
import com.alipay.rebate.shop.service.customer.CustomerSuperCategoryService;
import com.alipay.rebate.shop.utils.DataokeUtil;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/05/25.
 */
@Service
@Transactional
public class CustomerSuperCategoryServiceImpl extends AbstractService<SuperCategory> implements CustomerSuperCategoryService {

    private Logger logger = LoggerFactory.getLogger(CustomerSuperCategoryServiceImpl.class);

    Gson gson = new Gson();
    @Resource
    private SuperCategoryMapper superCategoryMapper;

    @Override
    public int insertOrUpdateSuperCategory() {
        SuperCategory superCategory = new SuperCategory();
        SuperCategoryResponse superCategoryResponse = DataokeUtil.getSuperCategory();
        logger.debug("superCategory is : {}",superCategoryResponse);
        List<SuperCategoryResp> superCategoryResps = superCategoryResponse.getData();

        int result = 0;
        for (SuperCategoryResp resp : superCategoryResps){
            superCategory.setCid(resp.getCid());
            superCategory.setCname(resp.getCname());
            superCategory.setCpic(resp.getCpic());
            List<SuperCategoryRes> subcategories = resp.getSubcategories();
            String jsonString = JSON.toJSONString(subcategories);
            logger.debug("jsonString is : {}",jsonString);
            superCategory.setSubcategories(jsonString);
            SuperCategory superCategory1 = superCategoryMapper.selectSuperCategoryByCid(resp.getCid());
            if (superCategory1 == null){

                int insertSuperCategory = superCategoryMapper.insertSuperCategory(superCategory);
                result += insertSuperCategory;
            }
            if (superCategory1 != null){
                int updateSuperCategoryByCid = superCategoryMapper.updateSuperCategoryByCid(superCategory);
                result += updateSuperCategoryByCid;
            }
        }
        return result;
    }

    @Override
    public List<SuperCategory> selectAllSuperCategory() {

        List<SuperCategoryResp> respList = new ArrayList<>();
        SuperCategoryResp response = new SuperCategoryResp();
        List<SuperCategory> superCategories = superCategoryMapper.selectAllSuperCategory();
//        List<SuperCategoryRes> res = new ArrayList<>();
//        logger.debug("superCategories is : {}",superCategories);
//        for (SuperCategory superCategory : superCategories){
//            response.setCid(superCategory.getCid());
//            response.setCname(superCategory.getCname());
//            response.setCpic(superCategory.getCpic());
//            SuperCategoryRes superCategoryRes = gson.fromJson(superCategory.getSubcategories(), SuperCategoryRes.class);
//            res.add(superCategoryRes);
//            response.setSubcategories(res);
//            respList.add(response);
//        }
        return superCategories;
    }
}
