package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import com.alipay.rebate.shop.pojo.user.customer.PddOrdersListRes;
import com.github.pagehelper.PageInfo;


/**
 * Created by CodeGenerator on 2020/05/12.
 */
public interface CustomerPddOrdersService extends Service<PddOrders> {

    PageInfo<OrdersListResponse> selectUserOrdersByType(Integer orderStatus, Integer page, Integer size, Long userId);

}
