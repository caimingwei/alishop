package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.CommissionFreezeSettingsMapper;
import com.alipay.rebate.shop.model.CommissionFreezeSettings;
import com.alipay.rebate.shop.service.admin.CommissionFreezeSettingsService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/11/02.
 */
@Service
@Transactional
public class CommissionFreezeSettingsServiceImpl extends AbstractService<CommissionFreezeSettings> implements
    CommissionFreezeSettingsService {
    @Resource
    private CommissionFreezeSettingsMapper commissionFreezeSettingsMapper;

}
