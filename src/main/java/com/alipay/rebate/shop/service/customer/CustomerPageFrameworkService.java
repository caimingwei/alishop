package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.PageFramework;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkResponse;

public interface CustomerPageFrameworkService extends Service<PageFramework> {

  PageFrameworkResponse selectDefaultPageFramework();

  PageFrameworkResponse selectIOSPageFramework();


}
