package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.AppSettings;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/08/22.
 */
public interface AppSettingsService extends Service<AppSettings> {

}
