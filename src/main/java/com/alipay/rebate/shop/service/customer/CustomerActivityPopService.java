package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.ActivityPop;


/**
 * Created by CodeGenerator on 2019/09/05.
 */
public interface CustomerActivityPopService extends Service<ActivityPop> {

}
