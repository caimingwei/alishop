package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.taobao.TkOrderRsponse;
import com.alipay.rebate.shop.scheduler.TypeTkStatus;
import com.alipay.rebate.shop.service.customer.CustomerUserIncomeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerUserIncomeServiceImpl extends AbstractService<UserIncome> implements
    CustomerUserIncomeService {

  private Logger logger = LoggerFactory.getLogger(CustomerUserIncomeServiceImpl.class);

  @Resource
  private UserIncomeMapper userIncomeMapper;
  @Resource
  private OrdersMapper ordersMapper;

  public PageInfo<UserIncome> selectUserAccountDetailIncome(Long userId, Integer pageNo, Integer pageSize){

    logger.debug("user account detail pageNo and pageSize is : {}",pageNo,pageSize);
    PageHelper.startPage(pageNo,pageSize);
    List<UserIncome> userIncomeList = userIncomeMapper.selectUserAccountDetailIncome(userId);
    PageInfo<UserIncome> pageInfo = new PageInfo<>(userIncomeList);
    logger.debug("pageInfo is : {}",pageInfo);
    return pageInfo;
  }

  public PageInfo<UserIncome> selectUserOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize){

    logger.debug("orders detail pageNo and pageSize is : {}",pageNo,pageSize);
    PageHelper.startPage(pageNo,pageSize);
    List<UserIncome> userIncomeList = userIncomeMapper.selectUserOrdersDetailIncome(userId);
    PageInfo<UserIncome> pageInfo = new PageInfo<>(userIncomeList);
    logger.debug("pageInfo is : {}",pageInfo);
    return pageInfo;
  }

  @Override
  public PageInfo<UserIncome> selectPddOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize) {

    logger.debug("orders detail pageNo and pageSize is : {}",pageNo,pageSize);
    PageHelper.startPage(pageNo,pageSize);

    List<UserIncome> userIncomes = userIncomeMapper.selectPddOrdersDetailIncome(userId);
    PageInfo<UserIncome> pageInfo = new PageInfo<>(userIncomes);
    logger.debug("pageInfo is : {}",pageInfo);
    return pageInfo;
  }

  @Override
  public PageInfo<UserIncome> selectJdOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize) {

    logger.debug("userId is : {}",userId);
    logger.debug("orders detail pageNo and pageSize is : {}",pageNo,pageSize);
    PageHelper.startPage(pageNo,pageSize);

    List<UserIncome> userIncomes = userIncomeMapper.selectJdOrdersDetailIncome(userId);
    logger.debug("userIncomes is : {}",userIncomes);
    PageInfo<UserIncome> pageInfo = new PageInfo<>(userIncomes);
    logger.debug("pageInfo is : {}",pageInfo);
    return pageInfo;
  }

  @Override
  public PageInfo<UserIncome> selectMeituanOrdersDetailIncome(Long userId, Integer pageNo, Integer pageSize) {
    logger.debug("userId is : {}",userId);
    logger.debug("orders detail pageNo and pageSize is : {}",pageNo,pageSize);
    PageHelper.startPage(pageNo,pageSize);

    List<UserIncome> userIncomes = userIncomeMapper.selectMeituanOrdersDetailIncome(userId);
    logger.debug("userIncomes is : {}",userIncomes);
    PageInfo<UserIncome> pageInfo = new PageInfo<>(userIncomes);
    logger.debug("pageInfo is : {}",pageInfo);
    return pageInfo;
  }


}
