package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.pojo.user.customer.UserWithdrawResponse;
import com.github.pagehelper.PageInfo;

public interface CustomerWithdrawService {

  PageInfo<WithDraw> selectUserUitWithDraw(Long userId,Integer pageNo, Integer pageSize);

  PageInfo<WithDraw> selectUserAmWithDraw(Long userId, Integer pageNo, Integer pageSize);

  PageInfo<WithDraw> selectUserWithDraw(Long userId, Integer pageNo, Integer pageSize);

  UserWithdrawResponse withdrawAmount(WithDraw withDraw);
  UserWithdrawResponse withdrawUit(WithDraw withDraw);
  UserWithdrawResponse selectWithDrawDetailForCustomer(Long userId);
}
