package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.pojo.user.customer.SignInReq;
import java.text.ParseException;


/**
 * Created by CodeGenerator on 2019/09/21.
 */
public interface CustomerSignInRecordService{

  Integer signIn(Long userId, SignInReq req) throws ParseException;
}
