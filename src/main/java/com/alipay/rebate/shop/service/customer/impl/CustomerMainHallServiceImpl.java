package com.alipay.rebate.shop.service.customer.impl;


import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.MainHallMapper;
import com.alipay.rebate.shop.model.MainHall;
import com.alipay.rebate.shop.pojo.mainhall.MainHallReq;
import com.alipay.rebate.shop.pojo.mainhall.UpdateMainHallReq;
import com.alipay.rebate.shop.service.admin.MainHallService;
import com.alipay.rebate.shop.service.customer.CustomerMainHallService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/04/24.
 */
@Service
@Transactional
public class CustomerMainHallServiceImpl extends AbstractService<MainHall> implements CustomerMainHallService {
    @Resource
    private MainHallMapper mainHallMapper;

}
