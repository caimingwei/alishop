package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.LogOffApply;
import com.alipay.rebate.shop.pojo.logoffapply.LogoffApplySearchReq;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/01/19.
 */
public interface CustomerLogOffApplyService extends Service<LogOffApply> {

  int add(Long userId);
  LogOffApply latestApply(Long userId);
}
