package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.ActivityProduct;


public interface CustomerActivityProductService extends Service<ActivityProduct> {
}
