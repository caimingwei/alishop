package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.CommissionLevelSettings;
import com.alipay.rebate.shop.core.Service;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/11/08.
 */
public interface CommissionLevelSettingsService extends Service<CommissionLevelSettings> {

  List<CommissionLevelSettings> findAllOrderByBegin();
}
