package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.SuperCategoryMapper;
import com.alipay.rebate.shop.model.SuperCategory;
import com.alipay.rebate.shop.service.admin.SuperCategoryService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/05/25.
 */
@Service
@Transactional
public class SuperCategoryServiceImpl extends AbstractService<SuperCategory> implements SuperCategoryService {
    @Resource
    private SuperCategoryMapper superCategoryMapper;

}
