package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.LuckyMoneyCoupon;
import com.alipay.rebate.shop.core.Service;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/10/26.
 */
public interface LuckyMoneyCouponService extends Service<LuckyMoneyCoupon> {

  Integer addNewLuckyMoneyCoupon(LuckyMoneyCoupon luckyMoneyCoupon);
  Integer updateNewLuckyMoneyCoupon(LuckyMoneyCoupon luckyMoneyCoupon);

  List<LuckyMoneyCoupon> selectAll();
}
