package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.BrowseProduct;
import com.github.pagehelper.PageInfo;

public interface CustomerBrowseProductService extends Service<BrowseProduct> {

  PageInfo<BrowseProduct> selectUserBrowseProduct(
      Long userId,
      Integer pageNo,
      Integer pageSize);
  int insertWhenNotExists(BrowseProduct browseProduct);

}
