package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.ImgSpaceMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.ImgSpace;
import com.alipay.rebate.shop.pojo.imgspace.GroupBPReq;
import com.alipay.rebate.shop.pojo.imgspace.SearchCondition;
import com.alipay.rebate.shop.service.admin.ImgSpaceService;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.utils.ListUtil;
import java.util.List;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/02/02.
 */
@Service
@Transactional
public class ImgSpaceServiceImpl extends AbstractService<ImgSpace> implements ImgSpaceService {
    @Resource
    private ImgSpaceMapper imgSpaceMapper;
    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void batchDelete(List<Long> ids) {
        if(ListUtil.isEmptyList(ids)) return;
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        ImgSpaceMapper mapper = sqlSession.getMapper(ImgSpaceMapper.class);
        for(Long id : ids){
            mapper.deleteByPrimaryKey(id);
        }
        sqlSession.flushStatements();
    }

    @Override
    public void batchUpdateGroup(GroupBPReq groupBPReq) {
        if(groupBPReq == null) return;
        if(ListUtil.isEmptyList(groupBPReq.getIds())) return;;
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        ImgSpaceMapper mapper = sqlSession.getMapper(ImgSpaceMapper.class);
        for(Long id : groupBPReq.getIds()){
           ImgSpace imgSpace = new ImgSpace();
           imgSpace.setId(id);
           imgSpace.setGroupId(groupBPReq.getGroupId());
           imgSpace.setGroupName(groupBPReq.getGroupName());
           mapper.updateByPrimaryKeySelective(imgSpace);
        }
        sqlSession.flushStatements();
    }

    @Override
    public List<ImgSpace> selectISByCondition(SearchCondition searchCondition) {
        return imgSpaceMapper.selectISByCondition(searchCondition);
    }
}
