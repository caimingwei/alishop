package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.CommissionFreezeSettings;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/11/02.
 */
public interface CommissionFreezeSettingsService extends Service<CommissionFreezeSettings> {

}
