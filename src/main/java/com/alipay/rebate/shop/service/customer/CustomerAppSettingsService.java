package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.AppSettings;


/**
 * Created by CodeGenerator on 2019/08/22.
 */
public interface CustomerAppSettingsService extends Service<AppSettings> {

}
