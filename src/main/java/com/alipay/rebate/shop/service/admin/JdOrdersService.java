package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.JdOrders;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.user.admin.JdOrdersReq;
import com.github.pagehelper.PageInfo;


/**
 * Created by CodeGenerator on 2020/05/05.
 */
public interface JdOrdersService extends Service<JdOrders> {

    PageInfo<JdOrders> selectOrdersByCondition(JdOrdersReq req,Integer pageNo, Integer pageSize);

}
