package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.user.admin.OrdersPageRequest;
import com.alipay.rebate.shop.pojo.user.admin.UpdateOrdersRequest;
import com.alipay.rebate.shop.scheduler.orderimport.AdminImportOrders;
import com.alipay.rebate.shop.service.admin.AdminOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminOrdersServiceImpl implements AdminOrdersService  {

  private Logger logger = LoggerFactory.getLogger(AdminOrdersServiceImpl.class);

  @Resource
  private OrdersMapper ordersMapper;
  @Resource
  private AdminImportOrders adminImportOrders;
  @Resource
  UserMapper userMapper;

  @Override
  public Orders get(String id) {
    return ordersMapper.selectByPrimaryKey(id);
  }

  @Override
  @Transactional
  public PageInfo<Orders> selectOrdersByCondition(OrdersPageRequest ordersPageRequest) {
    logger.debug("pageSize is: {}",ordersPageRequest.getPageSize());
    logger.debug("pageNo is: {}",ordersPageRequest.getPageNo());
    PageHelper.startPage(ordersPageRequest.getPageNo(),ordersPageRequest.getPageSize());
    List<Orders> page = null;
    page =  ordersMapper.selectOrdersByCondition(ordersPageRequest);
    logger.debug("page is : {}",page);
    if (page.size() < 1){
      logger.debug("page is null");
      PageHelper.startPage(ordersPageRequest.getPageNo(),ordersPageRequest.getPageSize());
      if (ordersPageRequest.getTradeId() != null) {
        ordersPageRequest.setTradeParentId(ordersPageRequest.getTradeId().toString());
        page = ordersMapper.selectOrdersByConditionTwo(ordersPageRequest);
      }
    }
//    // 把id转成String类型
//    convertIdFromLongToStr(page);
    PageInfo<Orders> pageInfo = new PageInfo<>(page);
    logger.debug("pageInfo is:{}",pageInfo);
    return pageInfo;
  }

  @Override
  public Map selectPayedOrderPriceByTime(Date startTime, Date endTime) {
    return ordersMapper.selectPayedOrderPriceByTime(startTime,endTime);
  }

  @Override
  public Map selectEarnedOrderByTime(Date startTime, Date endTime) {
    return ordersMapper.selectEarnedOrderByTime(startTime,endTime);
  }

  @Override
  public List<Orders> selectOrderByGoodIdAndOrderType(String goodId, String orderType) {
    return ordersMapper.selectOrderByGoodIdAndOrderType(goodId,orderType);
  }

  @Override
  public int updateOrdersById(UpdateOrdersRequest updateOrdersRequest) {

    // 查询该订单是否已经奖励过用户
    Orders orders = ordersMapper.selectOrdersByTradeId(updateOrdersRequest.getTradeId());
    if (orders.getIsRecord() == 1){
      throw new BusinessException("此订单已处理过",StatusCode.INTERNAL_ERROR);
    }
    User user = userMapper.selectById(updateOrdersRequest.getUserId());
    User preUser = userMapper.selectById(user.getParentUserId());
    logger.debug("updateOrdersRequest is : {}",updateOrdersRequest);
    orders.setPromoterName(user.getUserNickName());
    orders.setPromoterId(updateOrdersRequest.getUserId());
    Orders orders1 = ordersMapper.selectOrdersByTradeId(updateOrdersRequest.getTradeId());
    orders1.setTkStatus(0L);
    adminImportOrders.handleOrdersByStatus(orders,orders1,user,preUser);
    return 1;
  }

  @Override
  public PageInfo<Orders> selectOrdersByIsRecord(OrdersPageRequest ordersPageRequest) {

    logger.debug("pageSize is: {}",ordersPageRequest.getPageSize());
    logger.debug("pageNo is: {}",ordersPageRequest.getPageNo());
    PageHelper.startPage(ordersPageRequest.getPageNo(),ordersPageRequest.getPageSize());
    List<Orders> orders = ordersMapper.selectOrdersByIsRecord(ordersPageRequest);

    PageInfo<Orders> pageInfo = new PageInfo<>(orders);
    return pageInfo;
  }


}
