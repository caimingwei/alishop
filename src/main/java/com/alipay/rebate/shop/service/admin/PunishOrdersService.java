package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.PunishOrders;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.order.PunishOrdersRsp;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Created by CodeGenerator on 2020/06/04.
 */
public interface PunishOrdersService extends Service<PunishOrders> {

    List<PunishOrdersRsp> selectAllPunishOrders(@Param("tradeId") String tradeId, @Param("userId") Long userId);

}
