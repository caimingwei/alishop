package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.InviteSettingsMapper;
import com.alipay.rebate.shop.model.InviteSettings;
import com.alipay.rebate.shop.service.admin.InviteSettingsService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/09/21.
 */
@Service
@Transactional
public class InviteSettingsServiceImpl extends AbstractService<InviteSettings> implements InviteSettingsService {
    @Resource
    private InviteSettingsMapper inviteSettingsMapper;

}
