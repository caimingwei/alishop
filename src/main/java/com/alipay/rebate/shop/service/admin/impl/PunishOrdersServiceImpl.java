package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.PunishOrdersMapper;
import com.alipay.rebate.shop.model.PunishOrders;
import com.alipay.rebate.shop.pojo.order.PunishOrdersRsp;
import com.alipay.rebate.shop.service.admin.PunishOrdersService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/06/04.
 */
@Service
@Transactional
public class PunishOrdersServiceImpl extends AbstractService<PunishOrders> implements PunishOrdersService {
    @Resource
    private PunishOrdersMapper punishOrdersMapper;

    @Override
    public List<PunishOrdersRsp> selectAllPunishOrders(String tradeId, Long userId) {
        List<PunishOrdersRsp> punishOrdersRsps = punishOrdersMapper.selectAllPunishOrders(tradeId, userId);
        return punishOrdersRsps;
    }
}
