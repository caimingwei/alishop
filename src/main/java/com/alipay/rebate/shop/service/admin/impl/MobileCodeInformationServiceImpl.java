package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.MobileCodeInformationMapper;
import com.alipay.rebate.shop.model.MobileCodeInformation;
import com.alipay.rebate.shop.pojo.mobilecode.CodePageInformation;
import com.alipay.rebate.shop.service.admin.MobileCodeInformationService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MobileCodeInformationServiceImpl implements MobileCodeInformationService {

    @Resource
    private MobileCodeInformationMapper informationMapper;

    @Override
    @Transactional
    public PageInfo<MobileCodeInformation> selectAllMobileCodeInforMation(CodePageInformation pageInformation) {
        PageHelper.startPage(pageInformation.getPageNo(),pageInformation.getPageSize());
        List<MobileCodeInformation> mobileCodeInformations = informationMapper.selectAllMobileCodeInforMation(pageInformation);
        PageInfo<MobileCodeInformation> pageInfo = new PageInfo<>(mobileCodeInformations);
        return pageInfo;
    }
}
