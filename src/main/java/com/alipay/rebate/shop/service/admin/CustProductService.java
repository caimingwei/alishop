package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.CustProduct;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/08/28.
 */
public interface CustProductService extends Service<CustProduct> {

}
