package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.user.admin.PddOrdersPageReq;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * Created by CodeGenerator on 2020/05/12.
 */
public interface PddOrdersService extends Service<PddOrders> {

    PageInfo<PddOrders> selectOrdersByCondition(PddOrdersPageReq req,Integer page,Integer size);

}
