package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.SuperCategory;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2020/05/25.
 */
public interface SuperCategoryService extends Service<SuperCategory> {

}
