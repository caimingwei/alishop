package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.WithDrawMapper;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.admin.AdminWithDrawRsp;
import com.alipay.rebate.shop.pojo.user.admin.UserIncomeRsp;
import com.alipay.rebate.shop.service.admin.AdminUserIncomeService;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class AdminUserIncomeServiceImpl extends AbstractService<UserIncome> implements
    AdminUserIncomeService {

    private final Logger logger = LoggerFactory.getLogger(AdminUserIncomeServiceImpl.class);

    @Resource
    private UserIncomeMapper userIncomeMapper;

    @Resource
    private WithDrawMapper withDrawMapper;

    @Override
    public List<UserIncome> selectAllIncome() {
        return userIncomeMapper.selectAllIncome();
    }

    @Override
    public List<UserIncomeRsp> selectAllByCondition(UserIncomeRsp userIncome) {

        List<UserIncomeRsp> userIncomeRsps = userIncomeMapper.selectAllByCondition(userIncome);

        return userIncomeRsps;
    }
}
