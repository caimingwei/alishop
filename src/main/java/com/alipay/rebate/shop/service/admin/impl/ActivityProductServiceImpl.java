package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.ActivityProductMapper;
import com.alipay.rebate.shop.model.ActivityProduct;
import com.alipay.rebate.shop.service.admin.ActivityProductService;
import com.alipay.rebate.shop.core.AbstractService;
import gherkin.lexer.Da;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/08/25.
 */
@Service
@Transactional
public class ActivityProductServiceImpl extends AbstractService<ActivityProduct> implements ActivityProductService {
    @Resource
    private ActivityProductMapper activityProductMapper;

    @Override
    public void deleteByActivityId(Integer activityId) {
        activityProductMapper.deleteByActivityId(activityId);
    }

    @Override
    public List<ActivityProduct> getProductsByActivityId(Integer activityId) {
        return activityProductMapper.getProductsByActivityId(activityId);
    }

    @Override
    public void updateActivityProduct() {
        List<ActivityProduct> activityProducts = activityProductMapper.selectAll();
        Date date = new Date();
        for (ActivityProduct activityProduct : activityProducts) {
            if (activityProduct.getEndTime().compareTo(date) == -1){
                activityProductMapper.updateActivityProductById(activityProduct.getId());
            }
        }
    }

}
