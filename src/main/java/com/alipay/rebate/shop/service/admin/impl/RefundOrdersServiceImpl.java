package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.RefundOrdersMapper;
import com.alipay.rebate.shop.model.RefundOrders;
import com.alipay.rebate.shop.service.admin.RefundOrdersService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by CodeGenerator on 2019/09/19.
 */
@Service
@Transactional
public class RefundOrdersServiceImpl extends AbstractService<RefundOrders> implements
    RefundOrdersService {
    @Resource
    private RefundOrdersMapper refundOrdersMapper;

    @Override
    public List<RefundOrders> selectAllRefundOrders(Long userId,String tradeId) {
        List<RefundOrders> refundOrders = refundOrdersMapper.selectAllRefundOrders(userId,tradeId);
        return refundOrders;
    }

    @Override
    public RefundOrders selectById(String tbTradeId) {
        return refundOrdersMapper.selectById(tbTradeId);
    }
}
