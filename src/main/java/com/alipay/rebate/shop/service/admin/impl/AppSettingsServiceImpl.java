package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.AppSettingsMapper;
import com.alipay.rebate.shop.model.AppSettings;
import com.alipay.rebate.shop.service.admin.AppSettingsService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/08/22.
 */
@Service
@Transactional
public class AppSettingsServiceImpl extends AbstractService<AppSettings> implements AppSettingsService {
    @Resource
    private AppSettingsMapper appSettingsMapper;

}
