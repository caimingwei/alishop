package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.pojo.entitysettings.InrUser;
import com.alipay.rebate.shop.pojo.entitysettings.IrUser;
import java.io.IOException;
import java.util.List;

public interface CustomerAppService {

  List<InrUser> inviteNumRank() throws IOException;
  List<IrUser> incomeRank() throws IOException;

}
