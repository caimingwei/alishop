package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.ActivityBrowseProduct;

public interface CustomerActivityBrowseProductService extends Service<ActivityBrowseProduct> {
}
