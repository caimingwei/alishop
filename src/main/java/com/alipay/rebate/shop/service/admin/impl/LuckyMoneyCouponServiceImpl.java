package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.constants.LuckyMonCouponConstant;
import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.LuckyMoneyCouponMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.LuckyMoneyCoupon;
import com.alipay.rebate.shop.service.admin.LuckyMoneyCouponService;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/10/26.
 */
@Service
@Transactional
public class LuckyMoneyCouponServiceImpl extends AbstractService<LuckyMoneyCoupon> implements
    LuckyMoneyCouponService {
    @Resource
    private LuckyMoneyCouponMapper luckyMoneyCouponMapper;

    @Override
    public Integer addNewLuckyMoneyCoupon(LuckyMoneyCoupon luckyMoneyCoupon) {
        // 如果是当前正在使用的红包卷, 把其他正在使用的红包卷状态设置成未启用,并且设置成除时间不可编辑状态
        if (LuckyMonCouponConstant.IS_CURRENT_USE.equals(luckyMoneyCoupon.getIsCurrentUse())){
//            luckyMoneyCouponMapper.setCurrentUserCouponToNotUse();
            luckyMoneyCoupon.setCanEdit(LuckyMonCouponConstant.CAN_NOT_EDIT);
        }
        Date date = new Date();
        luckyMoneyCoupon.setCreateTime(date);
        luckyMoneyCoupon.setUpdateTime(date);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);
        return luckyMoneyCoupon.getId();
    }

    @Override
    public Integer updateNewLuckyMoneyCoupon(LuckyMoneyCoupon luckyMoneyCoupon) {
        // 根据id获取到当前记录
        LuckyMoneyCoupon dbRecord = luckyMoneyCouponMapper.selectByPrimaryKey(luckyMoneyCoupon.getId());
        if (dbRecord == null){
            throw  new BusinessException("红包卷记录不存在", StatusCode.LUCKY_MONEY_COUPON_ERROR);
        }
        // 如果设置当前正在使用的红包卷, 并且之前也是未启用状态
//        if (LuckyMonCouponConstant.IS_CURRENT_USE.equals(luckyMoneyCoupon.getIsCurrentUse())
//            && !LuckyMonCouponConstant.IS_CURRENT_USE.equals(dbRecord.getIsCurrentUse())
//        ){
//            luckyMoneyCouponMapper.setCurrentUserCouponToNotUse();
//        }
        if (LuckyMonCouponConstant.IS_CURRENT_USE.equals(luckyMoneyCoupon.getIsCurrentUse())){
            luckyMoneyCoupon.setCanEdit(LuckyMonCouponConstant.CAN_NOT_EDIT);
        }
        Date date = new Date();
        luckyMoneyCoupon.setUpdateTime(date);
        luckyMoneyCouponMapper.updateByPrimaryKeySelective(luckyMoneyCoupon);
        return dbRecord.getId();
    }

    @Override
    public List<LuckyMoneyCoupon> selectAll() {
        return luckyMoneyCouponMapper.selectAll();
    }
}
