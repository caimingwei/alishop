package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.pdtframework.ProductRepoChoiceRsp;
import com.alipay.rebate.shop.pojo.user.TaobaoAuthRsp;
import com.alipay.rebate.shop.pojo.user.TokenRsp;
import com.alipay.rebate.shop.pojo.user.customer.InviteDetailRsp;
import com.alipay.rebate.shop.pojo.user.customer.MultiPrivilegeLinkRequest;
import com.alipay.rebate.shop.pojo.user.customer.PasswordRequest;
import com.alipay.rebate.shop.pojo.user.customer.PlusOrReduceVirMoneyReq;
import com.alipay.rebate.shop.pojo.user.customer.PrivilegeLinkAndTpwdResponse;
import com.alipay.rebate.shop.pojo.user.customer.UserAlipayResponse;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import com.alipay.rebate.shop.pojo.user.customer.UserFansListResponse;
import com.alipay.rebate.shop.pojo.user.customer.UserIncomeRecordResponse;
import com.alipay.rebate.shop.pojo.user.customer.UserPersonalCenterResponse;
import com.alipay.rebate.shop.pojo.user.customer.UserRelationIdResponse;
import com.alipay.rebate.shop.pojo.user.customer.UserRequest;
import com.alipay.rebate.shop.pojo.user.product.ActivityProductCondition;
import com.alipay.rebate.shop.pojo.user.product.CheckActivityConditionRsp;
import com.alipay.rebate.shop.service.common.UserService;
import com.taobao.api.ApiException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.subject.Subject;

public interface CustomerUserService extends UserService {

  String generateValidCodeByType(String phone,int type,HttpServletRequest request);
  void phoneRegister(UserRequest userRequest, HttpServletRequest request);
  void weixinRegister(UserRequest userRequest, HttpServletRequest request);
  void h5Register(UserRequest userRequest, HttpServletRequest request);

  UserDto getUserDtoInfoForPhoneLogin(String phone);
  UserDto getUserInfoForWeixinLogin(String openId);
  UserDto getUserDtoInfoForUserNamePasswordLogin(String phone);

  UserPersonalCenterResponse weixinLogin(
      UserDto loginInfo,
      HttpServletRequest request,
      HttpServletResponse response,
      Subject subject);
  UserPersonalCenterResponse userNamePasswordLogin(
      UserDto loginInfo,
      HttpServletRequest request,
      HttpServletResponse response,
      Subject subject);
  UserRelationIdResponse generateRelationAndSpecialId(String sessionKey, long userId);

  // for test
  ResponseResult<User> selectAllUserInfoForTestByPhone(String phone);
  ResponseResult<Void> deleteUserInfoForTestByPhone(String phone);

  ResponseResult<UserAlipayResponse> getAliPayMessage(Long userId);
  UserPersonalCenterResponse getPersonalCenterMessage(Long userId);

  void updateAlipayAccount(UserRequest userRequest, Long userId);
  void updatePhone(UserRequest userRequest, Long userId);
  void updatePassword(UserRequest userRequest, Long userId);
  void findPassword(PasswordRequest passwordRequest);

  UserFansListResponse getOneLevelFansDetail(
      Integer pageNo,
      Integer pageSize,
      Long userId,
      String userNameOrPhone);

  UserFansListResponse getSecondLevelFansDetail(
      Integer pageNo,
      Integer pageSize,
      Long userId,
      String userNameOrPhone);

  ResponseResult<UserIncomeRecordResponse> getUserIncomeRecordDetail(Long userId);

  PrivilegeLinkAndTpwdResponse getPrivilegeLinkAndTpwd(
      boolean flag,
      String goodsId,
      String taobaoUserId,
      String taobaoUserName,
      String text,
      String logo,
      String accessToken,
      Long relationId,
      Long specialId,
      Long userId,
      String extra
  ) throws ApiException;

  List<PrivilegeLinkAndTpwdResponse> getMultiPrivilegeLinkAndTpwd(
      MultiPrivilegeLinkRequest privilegeLinkRequest,
      Long userId
  ) throws ApiException;

  CheckActivityConditionRsp checkIfUserCanBuyActivity(
      Long userId,
      String goodsId,
      boolean flag,
      String access_token,
      String taobaoUserId,
      String userName,
      Integer activityId,
      Integer productRepoId,
      ActivityProductCondition condition,
      Integer itemType
  );

  void rewardUser(Long userId);

  void plusOrReduceVirtualMoney(PlusOrReduceVirMoneyReq req,Long userId);

  ProductRepoChoiceRsp decodeSecretAndBindRelation(String sk, Long userId);

  TaobaoAuthRsp taobaoAuth(Long userId,String taobaoUserId, String userName,String accessToken);

  Map<String, InviteDetailRsp> selectInviteDetail(Long userId);

  boolean checkRelationId(Long userId, Long relationId);

  void weixinAuth(Long userId , String openId);

  TokenRsp getNewToken(long userId);

  String generateInternationalValidCodeByType(String areaCode,String phone, int type,HttpServletRequest request);

//  void logoff(Long userId);
}
