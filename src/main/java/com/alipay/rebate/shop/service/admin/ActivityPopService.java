package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.ActivityPop;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/09/05.
 */
public interface ActivityPopService extends Service<ActivityPop> {

}
