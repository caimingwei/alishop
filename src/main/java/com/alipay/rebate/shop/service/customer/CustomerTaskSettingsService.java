package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.TaskSettings;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/10/09.
 */
public interface CustomerTaskSettingsService extends Service<TaskSettings> {
  List<TaskSettings> selectUseSettings();
}
