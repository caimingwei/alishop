package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.BrowseAdMapper;
import com.alipay.rebate.shop.model.BrowseAd;
import com.alipay.rebate.shop.service.admin.BrowseAdService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/09/08.
 */
@Service
@Transactional
public class BrowseAdServiceImpl extends AbstractService<BrowseAd> implements BrowseAdService {
    @Resource
    private BrowseAdMapper browseAdMapper;

}
