package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.model.ActivityBrowseProduct;
import com.alipay.rebate.shop.service.customer.CustomerActivityBrowseProductService;
import org.springframework.stereotype.Service;

@Service
public class CustomerActivityBrowseProductServiceImpl extends
    AbstractService<ActivityBrowseProduct> implements CustomerActivityBrowseProductService {

}
