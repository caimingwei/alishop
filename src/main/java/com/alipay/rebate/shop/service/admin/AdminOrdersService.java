package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.user.admin.OrdersPageRequest;
import com.alipay.rebate.shop.pojo.user.admin.UpdateOrdersRequest;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.Map;

public interface AdminOrdersService {

  PageInfo<Orders> selectOrdersByCondition(OrdersPageRequest ordersPageRequest);
  Map selectPayedOrderPriceByTime(@Param("startTime") Date startTime, @Param("endTime")Date endTime);
  Map selectEarnedOrderByTime(@Param("startTime") Date startTime, @Param("endTime")Date endTime);
  Orders get(String id);
  List<Orders> selectOrderByGoodIdAndOrderType(@Param("goodId")String goodId,@Param("order_type")String orderType);

  int updateOrdersById(UpdateOrdersRequest updateOrdersRequest);

  PageInfo<Orders> selectOrdersByIsRecord(OrdersPageRequest ordersPageRequest);

}
