package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.BrowseAd;


/**
 * Created by CodeGenerator on 2019/09/08.
 */
public interface CustomerBrowseAdService extends Service<BrowseAd> {

}
