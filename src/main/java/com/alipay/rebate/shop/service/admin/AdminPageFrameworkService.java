package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.PageFramework;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkRequest;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminPageFrameworkService extends Service<PageFramework> {

  ResponseResult<Long> createOrUpdatePage(PageFrameworkRequest pageFrameworkRequest);
  ResponseResult<Integer> updatePageFrameWork(PageFramework pageFramework);
  ResponseResult<List<PageFrameworkResponse>> selectAllPageFramework();
  ResponseResult<PageFrameworkResponse> selectPageFrameworkByPageId(Long pageId);
  List<PageFramework> selectUsePageFramework(@Param("isUse")Integer isUse);
  List<PageFramework> selectAllByType(@Param("isIndex")Integer isIndex);
  int createPageIndex(PageFrameworkRequest pageFrameworkRequest);
  List<PageFramework> selectPageIndex();
  int updatePageIndex(PageFrameworkRequest pageFrameworkRequest);
}
