package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.MeituanRefundOrdersMapper;
import com.alipay.rebate.shop.model.MeituanRefundOrders;
import com.alipay.rebate.shop.service.admin.MeituanRefundOrdersService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/08/25.
 */
@Service
@Transactional
public class MeituanRefundOrdersServiceImpl extends AbstractService<MeituanRefundOrders> implements MeituanRefundOrdersService {
    @Resource
    private MeituanRefundOrdersMapper meituanRefundOrdersMapper;

}
