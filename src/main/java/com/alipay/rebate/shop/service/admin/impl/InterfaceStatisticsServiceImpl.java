package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.InterfaceStatisticsMapper;
import com.alipay.rebate.shop.helper.InterfaceStatisticsHelper;
import com.alipay.rebate.shop.model.InterfaceStatistics;
import com.alipay.rebate.shop.pojo.InterfaceStatisticsRsp;
import com.alipay.rebate.shop.service.admin.InterfaceStatisticsService;
import com.alipay.rebate.shop.core.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/04/27.
 */
@Service
@Transactional
public class InterfaceStatisticsServiceImpl extends AbstractService<InterfaceStatistics> implements InterfaceStatisticsService {
    @Resource
    private InterfaceStatisticsMapper interfaceStatisticsMapper;

    private final Logger logger = LoggerFactory.getLogger(InterfaceStatisticsServiceImpl.class);

    @Override
    public List<InterfaceStatisticsRsp> selectInterfaceStatistics() {
        List<InterfaceStatisticsRsp> interfaceStatisticsRsps = interfaceStatisticsMapper.selectInterfaceStatistics();
        return interfaceStatisticsRsps;
    }
}
