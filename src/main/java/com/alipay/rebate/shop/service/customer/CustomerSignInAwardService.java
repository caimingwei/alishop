package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.pojo.user.customer.SignInSettingAwardRsp;

/**
 * Created by CodeGenerator on 2019/09/21.
 */
public interface CustomerSignInAwardService {

  SignInSettingAwardRsp getSignInSettingAwardRsp(Long userId);

}
