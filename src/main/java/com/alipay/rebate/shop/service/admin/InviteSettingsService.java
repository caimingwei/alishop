package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.InviteSettings;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/09/21.
 */
public interface InviteSettingsService extends Service<InviteSettings> {

}
