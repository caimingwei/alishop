package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.Coun2numMapper;
import com.alipay.rebate.shop.model.Coun2num;
import com.alipay.rebate.shop.service.admin.Coun2numService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/08/05.
 */
@Service
@Transactional
public class Coun2numServiceImpl extends AbstractService<Coun2num> implements Coun2numService {
    @Resource
    private Coun2numMapper coun2numMapper;

}
