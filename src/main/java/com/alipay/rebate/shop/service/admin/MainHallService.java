package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.MainHall;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.mainhall.MainHallReq;
import com.alipay.rebate.shop.pojo.mainhall.UpdateMainHallReq;


/**
 * Created by CodeGenerator on 2020/04/24.
 */
public interface MainHallService extends Service<MainHall> {


    int addMainHall(MainHallReq req);

    int updateMainHallById(UpdateMainHallReq req);


}
