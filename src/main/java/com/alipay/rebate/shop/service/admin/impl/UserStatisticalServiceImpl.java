package com.alipay.rebate.shop.service.admin.impl;


import com.alipay.rebate.shop.dao.mapper.UserStatisticalMapper;
import com.alipay.rebate.shop.model.UserStatistical;
import com.alipay.rebate.shop.service.admin.UserStatisticalService;
import com.alipay.rebate.shop.core.AbstractService;

import com.alibaba.fastjson.JSON;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.dao.mapper.UserStatisticalMapper;
import com.alipay.rebate.shop.model.UserStatistical;
import com.alipay.rebate.shop.pojo.user.admin.PhoneType;
import com.alipay.rebate.shop.service.admin.UserStatisticalService;
import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;



/**
 * Created by CodeGenerator on 2020/05/27.
 */
@Service
@Transactional
public class UserStatisticalServiceImpl extends AbstractService<UserStatistical> implements UserStatisticalService {
    @Resource
    private UserStatisticalMapper userStatisticalMapper;

    @Resource
    private UserMapper userMapper;

    private Logger logger = LoggerFactory.getLogger(UserStatisticalServiceImpl.class);



}
