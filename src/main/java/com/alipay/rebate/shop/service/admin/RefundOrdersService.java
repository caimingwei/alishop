package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.RefundOrders;
import com.alipay.rebate.shop.core.Service;

import java.util.List;


/**
 * Created by CodeGenerator on 2019/09/19.
 */
public interface RefundOrdersService extends Service<RefundOrders> {
    List<RefundOrders> selectAllRefundOrders(Long userId,String tradeId);
    RefundOrders selectById(String tbTradeId);
}
