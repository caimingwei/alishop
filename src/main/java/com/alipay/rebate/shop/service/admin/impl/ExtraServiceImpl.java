package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.ExtraMapper;
import com.alipay.rebate.shop.model.Extra;
import com.alipay.rebate.shop.service.admin.ExtraService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/04/28.
 */
@Service
@Transactional
public class ExtraServiceImpl extends AbstractService<Extra> implements ExtraService {
    @Resource
    private ExtraMapper extraMapper;

}
