package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.RefundOrdersMapper;
import com.alipay.rebate.shop.model.RefundOrders;
import com.alipay.rebate.shop.service.customer.CustomerRefundOrdersService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/09/18.
 */
@Service
@Transactional
public class CustomerRefundOrdersServiceImpl extends AbstractService<RefundOrders> implements
    CustomerRefundOrdersService {
    @Resource
    private RefundOrdersMapper refundOrdersMapper;

}
