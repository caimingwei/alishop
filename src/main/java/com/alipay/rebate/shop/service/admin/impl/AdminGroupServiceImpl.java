package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.AdminGroupMapper;
import com.alipay.rebate.shop.model.AdminGroup;
import com.alipay.rebate.shop.service.admin.AdminGroupService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/12/08.
 */
@Service
@Transactional
public class AdminGroupServiceImpl extends AbstractService<AdminGroup> implements
    AdminGroupService {
    @Resource
    private AdminGroupMapper adminGroupMapper;

}
