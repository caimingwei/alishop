package com.alipay.rebate.shop.service.admin;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.JpushSettings;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/11/30.
 */
public interface JpushSettingsService extends Service<JpushSettings> {
  List<JpushSettings> selectAllOrderById();
  void pushToAllUser(JpushSettings jpushSettings);
}
