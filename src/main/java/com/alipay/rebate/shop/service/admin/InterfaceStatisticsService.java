package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.InterfaceStatistics;
import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.pojo.InterfaceStatisticsRsp;

import java.util.List;


/**
 * Created by CodeGenerator on 2020/04/27.
 */
public interface InterfaceStatisticsService extends Service<InterfaceStatistics> {

    List<InterfaceStatisticsRsp> selectInterfaceStatistics();

}
