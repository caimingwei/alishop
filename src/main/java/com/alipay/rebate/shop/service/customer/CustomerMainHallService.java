package com.alipay.rebate.shop.service.customer;

import com.alipay.rebate.shop.core.Service;
import com.alipay.rebate.shop.model.MainHall;
import com.alipay.rebate.shop.pojo.mainhall.MainHallReq;
import com.alipay.rebate.shop.pojo.mainhall.UpdateMainHallReq;


/**
 * Created by CodeGenerator on 2020/04/24.
 */
public interface CustomerMainHallService extends Service<MainHall> {


}
