package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.HtmlClick;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2020/05/04.
 */
public interface HtmlClickService extends Service<HtmlClick> {

}
