package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.PddOrdersMapper;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.pojo.user.admin.PddOrdersPageReq;
import com.alipay.rebate.shop.service.admin.PddOrdersService;
import com.alipay.rebate.shop.core.AbstractService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2020/05/12.
 */
@Service
@Transactional
public class PddOrdersServiceImpl extends AbstractService<PddOrders> implements PddOrdersService {
    @Resource
    private PddOrdersMapper pddOrdersMapper;

    private Logger logger = LoggerFactory.getLogger(PddOrdersServiceImpl.class);

    @Override
    public PageInfo<PddOrders> selectOrdersByCondition(PddOrdersPageReq req, Integer page, Integer size) {

        logger.debug("page is : {}",page);
        logger.debug("size is : {}",size);
        logger.debug("req is : {}",req);
        PageHelper.startPage(page,size);

        List<PddOrders> pddOrders = pddOrdersMapper.selectOrdersByCondition(req);

        PageInfo<PddOrders> pageInfo = new PageInfo<>(pddOrders);

        return pageInfo;
    }
}
