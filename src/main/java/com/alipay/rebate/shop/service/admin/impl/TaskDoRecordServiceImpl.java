package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.TaskDoRecordMapper;
import com.alipay.rebate.shop.model.TaskDoRecord;
import com.alipay.rebate.shop.service.admin.TaskDoRecordService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/10/17.
 */
@Service
@Transactional
public class TaskDoRecordServiceImpl extends AbstractService<TaskDoRecord> implements
    TaskDoRecordService {
    @Resource
    private TaskDoRecordMapper taskDoRecordMapper;

}
