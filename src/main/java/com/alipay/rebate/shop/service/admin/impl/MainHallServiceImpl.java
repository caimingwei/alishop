package com.alipay.rebate.shop.service.admin.impl;


import com.alipay.rebate.shop.dao.mapper.MainHallMapper;
import com.alipay.rebate.shop.model.MainHall;
import com.alipay.rebate.shop.pojo.mainhall.MainHallReq;
import com.alipay.rebate.shop.pojo.mainhall.UpdateMainHallReq;
import com.alipay.rebate.shop.service.admin.MainHallService;
import com.alipay.rebate.shop.core.AbstractService;

import com.alipay.rebate.shop.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Date;


/**
 * Created by CodeGenerator on 2020/04/24.
 */
@Service
@Transactional
public class MainHallServiceImpl extends AbstractService<MainHall> implements MainHallService {
    @Resource
    private MainHallMapper mainHallMapper;

    private final Logger logger = LoggerFactory.getLogger(MainHallServiceImpl.class);

    @Override
    public int addMainHall(MainHallReq req) {
        logger.debug("req is : {}",req);
        MainHall mainHall = new MainHall();
        mainHall.setName(req.getName());
        mainHall.setMosUrl(req.getMosUrl());
        mainHall.setFigureUrl(req.getFigureUrl());
        mainHall.setLeaguesActivityId(req.getLeaguesActivityId());
        mainHall.setMainDiagramUrl(req.getMainDiagramUrl());
        mainHall.setPid(req.getPid());
        mainHall.setType(req.getType());
        logger.debug("mainHall is : {}",mainHall);
        int result = mainHallMapper.addMainHall(mainHall);
        return result;
    }

    @Override
    public int updateMainHallById(UpdateMainHallReq req) {
        MainHall mainHall = new MainHall();
        mainHall.setId(req.getId());
        mainHall.setName(req.getName());
        mainHall.setFigureUrl(req.getFigureUrl());
        mainHall.setMainDiagramUrl(req.getMainDiagramUrl());
        mainHall.setPid(req.getPid());
        mainHall.setLeaguesActivityId(req.getLeaguesActivityId());
        mainHall.setMosUrl(req.getMosUrl());
        mainHall.setType(req.getType());
        logger.debug("mainHall is : {}",mainHall);
        int result = mainHallMapper.updateMainHallById(mainHall);
        return result;
    }


}
