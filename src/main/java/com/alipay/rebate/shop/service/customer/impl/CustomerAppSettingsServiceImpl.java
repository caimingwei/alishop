package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.AppSettingsMapper;
import com.alipay.rebate.shop.model.AppSettings;
import com.alipay.rebate.shop.service.admin.AppSettingsService;
import com.alipay.rebate.shop.service.customer.CustomerAppSettingsService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/08/22.
 */
@Service
@Transactional
public class CustomerAppSettingsServiceImpl extends AbstractService<AppSettings> implements
    CustomerAppSettingsService {
    @Resource
    private AppSettingsMapper appSettingsMapper;

}
