package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.MeituanOrdersMapper;
import com.alipay.rebate.shop.model.MeituanOrders;
import com.alipay.rebate.shop.service.admin.MeituanOrdersService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/08/25.
 */
@Service
@Transactional
public class MeituanOrdersServiceImpl extends AbstractService<MeituanOrders> implements MeituanOrdersService {
    @Resource
    private MeituanOrdersMapper meituanOrdersMapper;

}
