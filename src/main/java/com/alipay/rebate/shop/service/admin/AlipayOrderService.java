package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.AlipayOrder;
import com.alipay.rebate.shop.core.Service;

import com.alipay.rebate.shop.pojo.alipay.TransferReq;
import java.util.List;


/**
 * Created by CodeGenerator on 2019/09/12.
 */
public interface AlipayOrderService extends Service<AlipayOrder> {

     List<AlipayOrder> transfer(TransferReq transferReq);
     List<AlipayOrder> getAllTransferOrder();
     List<AlipayOrder> getAllSuccessTransferOrder();

}
