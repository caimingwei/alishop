package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.JpushHistoryMapper;
import com.alipay.rebate.shop.model.JpushHistory;
import com.alipay.rebate.shop.service.admin.JpushHistoryService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/12/05.
 */
@Service
@Transactional
public class JpushHistoryServiceImpl extends AbstractService<JpushHistory> implements
    JpushHistoryService {
    @Resource
    private JpushHistoryMapper jpushHistoryMapper;

    @Override
    public List<JpushHistory> selectAllOrderByIdDesc() {
        return jpushHistoryMapper.selectAllOrderById();
    }
}
