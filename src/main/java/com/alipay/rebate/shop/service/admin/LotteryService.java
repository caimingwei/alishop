package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.Lottery;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/08/13.
 */
public interface LotteryService extends Service<Lottery> {

  void create(Lottery lottery);
  void updateLottery(Lottery lottery);

}
