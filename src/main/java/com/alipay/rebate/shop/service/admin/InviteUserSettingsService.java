package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.InviteUserSettings;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/12/15.
 */
public interface InviteUserSettingsService extends Service<InviteUserSettings> {

}
