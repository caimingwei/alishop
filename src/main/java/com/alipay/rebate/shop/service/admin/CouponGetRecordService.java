package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2019/10/26.
 */
public interface CouponGetRecordService extends Service<CouponGetRecord> {

}
