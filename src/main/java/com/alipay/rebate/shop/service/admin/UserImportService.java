package com.alipay.rebate.shop.service.admin;
import com.alipay.rebate.shop.model.UserImport;
import com.alipay.rebate.shop.core.Service;


/**
 * Created by CodeGenerator on 2020/09/06.
 */
public interface UserImportService extends Service<UserImport> {

}
