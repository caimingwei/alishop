package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.dao.mapper.CustProductMapper;
import com.alipay.rebate.shop.model.CustProduct;
import com.alipay.rebate.shop.service.admin.CustProductService;
import com.alipay.rebate.shop.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2019/08/28.
 */
@Service
@Transactional
public class CustProductServiceImpl extends AbstractService<CustProduct> implements CustProductService {
    @Resource
    private CustProductMapper custProductMapper;

}
