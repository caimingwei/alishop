package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.core.AbstractService;
import com.alipay.rebate.shop.dao.mapper.TaskSettingsMapper;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.service.customer.CustomerTaskSettingsService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by CodeGenerator on 2019/10/09.
 */
@Service
@Transactional
public class CustomerTaskSettingsServiceImpl extends AbstractService<TaskSettings> implements
    CustomerTaskSettingsService {
    @Resource
    private TaskSettingsMapper taskSettingsMapper;

    @Override
    public List<TaskSettings> selectUseSettings() {
        return taskSettingsMapper.selectUseSettings();
    }
}
