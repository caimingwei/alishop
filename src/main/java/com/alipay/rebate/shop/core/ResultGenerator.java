package com.alipay.rebate.shop.core;

import com.alipay.rebate.shop.pojo.common.ResponseResult;

/**
 * 响应结果生成工具
 */
public class ResultGenerator {
    private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

    public static Result genSuccessResult() {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE);
    }

    public static <T> Result<T> genSuccessResult(T data) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(ResultCode.FAIL)
                .setMessage(message);
    }

    public static <T> ResponseResult<T> genSuccessResponseResult(T data){
        ResponseResult<T> responseResult  = new ResponseResult<>();
        responseResult.setMsg(DEFAULT_SUCCESS_MESSAGE);
        responseResult.setData(data);
        return responseResult;
    }

    public static ResponseResult genSuccessResponseResult(){
        ResponseResult responseResult  = new ResponseResult<>();
        responseResult.setMsg(DEFAULT_SUCCESS_MESSAGE);
        return responseResult;
    }
}
