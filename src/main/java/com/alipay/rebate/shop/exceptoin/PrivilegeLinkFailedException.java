package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class PrivilegeLinkFailedException extends BusinessException{

  public PrivilegeLinkFailedException() {
    super("转链失败", StatusCode.PRIVILEGE_LINK_GENERATE_FAIL);
  }
}
