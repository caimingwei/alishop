package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class GetRelationIdFailException extends BusinessException {

  public GetRelationIdFailException(String message) {
    super("生成relationId失败:" + message, StatusCode.RELATION_SPECIAL_ID_GENERATE_FAILED);
  }
}
