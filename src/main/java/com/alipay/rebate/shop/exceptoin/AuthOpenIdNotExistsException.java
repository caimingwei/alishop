package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class AuthOpenIdNotExistsException extends BusinessException {

  public AuthOpenIdNotExistsException() {
    super("openId 对应用户不存在", StatusCode.USER_NOT_EXISTS);
  }
}
