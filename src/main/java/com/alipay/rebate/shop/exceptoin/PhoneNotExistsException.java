package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class PhoneNotExistsException extends BusinessException{

  public PhoneNotExistsException() {
    super("手机号不存在", StatusCode.PHONE_NOT_EXISTS);
  }
}
