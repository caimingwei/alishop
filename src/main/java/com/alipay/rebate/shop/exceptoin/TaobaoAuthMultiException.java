package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class TaobaoAuthMultiException extends BusinessException{

  private Object obj;

  public TaobaoAuthMultiException(StatusCode statusCode) {
    super(statusCode);
  }

  public TaobaoAuthMultiException(String message,
      StatusCode statusCode, Object obj) {
    super(message, statusCode);
    this.obj = obj;
  }

  public Object getObj() {
    return obj;
  }

  public void setObj(Object obj) {
    this.obj = obj;
  }
}
