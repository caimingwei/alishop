package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class TokenPhoneNotRightException extends BusinessException {

    public TokenPhoneNotRightException() {
        super("用户电话号码不正确",StatusCode.TOKEN_PHONE_NOT_RIGHT);
    }
}
