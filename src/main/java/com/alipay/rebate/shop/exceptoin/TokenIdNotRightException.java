package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class TokenIdNotRightException extends BusinessException {

    public TokenIdNotRightException() {
        super("用户id不正确", StatusCode.TOKEN_ID_NOT_RIGHT);
    }
}
