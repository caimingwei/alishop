package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class TokenExpireException extends BusinessException {

    public TokenExpireException() {
        super("token过期", StatusCode.TOKEN_EXPIRE);
    }
}
