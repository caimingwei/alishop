package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class MobileCodeNotRightException extends BusinessException {
    public MobileCodeNotRightException() {
        super("短信验证码错误", StatusCode.MOBILE_CODE_NOT_RIGHT);
    }
}
