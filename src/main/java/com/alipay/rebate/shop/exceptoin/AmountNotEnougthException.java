package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class AmountNotEnougthException extends BusinessException {

  public AmountNotEnougthException() {
    super("余额不足", StatusCode.AMOUNT_NOT_ENOUGTH);
  }
}
