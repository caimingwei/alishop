package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class UitNotEnougthException extends BusinessException {

  public UitNotEnougthException() {
    super("集分宝不足", StatusCode.UIT_NOT_ENOUGTH);
  }
}
