package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class BusinessException extends RuntimeException {

    private StatusCode statusCode;

    public BusinessException(StatusCode statusCode){
        this.statusCode = statusCode;
    }

    public BusinessException(String message, StatusCode statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }
}
