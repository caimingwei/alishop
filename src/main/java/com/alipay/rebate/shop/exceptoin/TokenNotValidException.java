package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class TokenNotValidException extends BusinessException {

    public TokenNotValidException() {
        super("token 不正确", StatusCode.TOKEN_NOT_VALID);
    }
}
