package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

/**
 * 手机号已经注册异常
 */
public class PhoneAlreadyRegistedException extends BusinessException {

  public PhoneAlreadyRegistedException() {
    super("手机号已经注册", StatusCode.USER_ALREADY_EXISTS);
  }
}
