package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class TpwdGenerateFailException extends BusinessException {

  public TpwdGenerateFailException(String message) {
    super(message, StatusCode.TPWD_GENERATE_FAIL);
  }
}
