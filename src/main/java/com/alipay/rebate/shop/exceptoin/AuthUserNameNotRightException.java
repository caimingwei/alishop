package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class AuthUserNameNotRightException extends BusinessException {

  public AuthUserNameNotRightException() {
    super("用户不存在", StatusCode.USER_NOT_EXISTS);
  }
}
