package com.alipay.rebate.shop.exceptoin;

import com.alipay.rebate.shop.constants.StatusCode;

public class AlipayAccountNotBoundException extends BusinessException {

  public AlipayAccountNotBoundException() {
    super("支付宝账号没绑定", StatusCode.ALIPAY_ACCOUNT_NOT_BIND);
  }
}
