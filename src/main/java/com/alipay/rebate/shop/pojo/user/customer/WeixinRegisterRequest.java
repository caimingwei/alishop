package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.constants.PatternContant;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class WeixinRegisterRequest {

    @NotNull(message = PatternContant.PHONE_NULL_MESSAGE)
    @Pattern(regexp= PatternContant.PHONE_PATTERN,
            message=PatternContant.PHONE_PATTERN_ERROR_MESSAGE)
    private String userPhone;
    @NotNull(message = "openId不能为空")
    private String openId;
    @NotNull(message = "用户昵称不能为空")
    private String userNickName;
    @NotNull(message = "手机验证码不能为空")
    private String mobileCode;
    @NotNull(message = "密码不能为空")
    private String password;
    @NotNull(message = "头像地址不能为空")
    private String headImageUrl;

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHeadImageUrl() {
        return headImageUrl;
    }

    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl;
    }

    @Override
    public String toString() {
        return "WeixinRegisterRequest{" +
                "userPhone='" + userPhone + '\'' +
                ", openId='" + openId + '\'' +
                ", userNickName='" + userNickName + '\'' +
                ", mobileCode='" + mobileCode + '\'' +
                ", password='" + password + '\'' +
                ", headImageUrl='" + headImageUrl + '\'' +
                '}';
    }
}
