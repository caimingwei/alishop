package com.alipay.rebate.shop.pojo.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AccountLoginRequest {

//    @NotNull(message = PatternContant.PHONE_NULL_MESSAGE)
//    @Pattern(regexp= PatternContant.PHONE_PATTERN,
//            message=PatternContant.PHONE_PATTERN_ERROR_MESSAGE)
    @NotEmpty(message = "账号不能为空")
    @NotNull(message = "账号不能为空")
    private String account;
    @NotEmpty(message = "密码不能为空")
    @NotNull(message = "密码不能为空")
    private String password;
    private Integer loginPlatform;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Integer getLoginPlatform() {
        return loginPlatform;
    }

    public void setLoginPlatform(Integer loginPlatform) {
        this.loginPlatform = loginPlatform;
    }

    @Override
    public String toString() {
        return "AccountLoginRequest{" +
            "account='" + account + '\'' +
            ", password='" + password + '\'' +
            ", loginPlatform=" + loginPlatform +
            '}';
    }
}
