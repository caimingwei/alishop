package com.alipay.rebate.shop.pojo.user.admin;


import java.util.List;

public class AdminImportRefundOrdersReq {

    private List<RefundOrdersImpoertReq> orders;

    public List<RefundOrdersImpoertReq> getOrders() {
        return orders;
    }

    public void setOrders(List<RefundOrdersImpoertReq> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "ImportOrdersRequest{" +
                "orders=" + orders +
                '}';
    }
}
