package com.alipay.rebate.shop.pojo.historyprice;

public class ResultResp {

    private HistoryPriceRsp data;
    private String message;
    private Integer status;

    public HistoryPriceRsp getData() {
        return data;
    }

    public void setData(HistoryPriceRsp data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ResultRsponse{" +
                "data=" + data +
                ", message='" + message + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
