package com.alipay.rebate.shop.pojo.dataanalysis;

import java.util.Map;

public class DataAnalysisRsp {

    private Map<String,Object> buyByYourself;

    public Map<String, Object> getBuyByYourself() {
        return buyByYourself;
    }

    public void setBuyByYourself(Map<String, Object> buyByYourself) {
        this.buyByYourself = buyByYourself;
    }

    @Override
    public String toString() {
        return "DataAnalysisRsp{" +
                "buyByYourself=" + buyByYourself +
                '}';
    }
}
