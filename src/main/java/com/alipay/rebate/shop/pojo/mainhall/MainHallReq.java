package com.alipay.rebate.shop.pojo.mainhall;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class MainHallReq {

    private Long id;
    @NotNull(message = "name不能为空")
    private String name;
    private String leaguesActivityId;
    @NotNull(message = "type不能为空")
    private Integer type;
    private String mosUrl;
    private String pid;
    private Date addTime;
    @NotNull(message = "附图不能为空")
    private String figureUrl;
    @NotNull(message = "主图不能为空")
    private String mainDiagramUrl;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLeaguesActivityId() {
        return leaguesActivityId;
    }

    public void setLeaguesActivityId(String leaguesActivityId) {
        this.leaguesActivityId = leaguesActivityId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMosUrl() {
        return mosUrl;
    }

    public void setMosUrl(String mosUrl) {
        this.mosUrl = mosUrl;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getFigureUrl() {
        return figureUrl;
    }

    public void setFigureUrl(String figureUrl) {
        this.figureUrl = figureUrl;
    }

    public String getMainDiagramUrl() {
        return mainDiagramUrl;
    }

    public void setMainDiagramUrl(String mainDiagramUrl) {
        this.mainDiagramUrl = mainDiagramUrl;
    }

    @Override
    public String toString() {
        return "MainHallReq{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", leaguesActivityId=" + leaguesActivityId +
                ", type=" + type +
                ", mosUrl='" + mosUrl + '\'' +
                ", pid='" + pid + '\'' +
                ", addTime=" + addTime +
                ", figureUrl='" + figureUrl + '\'' +
                ", mainDiagramUrl='" + mainDiagramUrl + '\'' +
                '}';
    }
}
