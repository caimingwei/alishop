package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;

public class PddOrdersResponse {

    private Integer match_channel;
    private Long goods_price;
    private Long promotion_rate;
    private Integer type;
    private Integer order_status;
    private Long order_create_time;
    private Long order_settle_time;
    private Long order_verify_time;
    private Long order_group_success_time;
    private Long order_amount;
    private Long order_modify_at;
    private Long auth_duo_id;
    private Integer cpa_new;
    private String goods_name;
    private String batch_no;
    private Long goods_quantity;
    private Long goods_id;
    private String goods_thumbnail_url;
    private Long order_receive_time;
    private String custom_parameters;
    private Long promotion_amount;
    private Long order_pay_time;
    private Long group_id;
    private Integer duo_coupon_amount;
    private Integer scene_at_market_fee;
    private String order_status_desc;
    private String fail_reason;
    private String order_id;
    private String order_sn;
    private String p_id;
    private Integer zs_duo_id;



    public Integer getMatch_channel() {
        return match_channel;
    }

    public void setMatch_channel(Integer match_channel) {
        this.match_channel = match_channel;
    }

    public Long getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(Long goods_price) {
        this.goods_price = goods_price;
    }

    public Long getPromotion_rate() {
        return promotion_rate;
    }

    public void setPromotion_rate(Long promotion_rate) {
        this.promotion_rate = promotion_rate;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }

    public Long getOrder_create_time() {
        return order_create_time;
    }

    public void setOrder_create_time(Long order_create_time) {
        this.order_create_time = order_create_time;
    }

    public Long getOrder_settle_time() {
        return order_settle_time;
    }

    public void setOrder_settle_time(Long order_settle_time) {
        this.order_settle_time = order_settle_time;
    }

    public Long getOrder_verify_time() {
        return order_verify_time;
    }

    public void setOrder_verify_time(Long order_verify_time) {
        this.order_verify_time = order_verify_time;
    }

    public Long getOrder_group_success_time() {
        return order_group_success_time;
    }

    public void setOrder_group_success_time(Long order_group_success_time) {
        this.order_group_success_time = order_group_success_time;
    }

    public Long getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(Long order_amount) {
        this.order_amount = order_amount;
    }

    public Long getOrder_modify_at() {
        return order_modify_at;
    }

    public void setOrder_modify_at(Long order_modify_at) {
        this.order_modify_at = order_modify_at;
    }

    public Long getAuth_duo_id() {
        return auth_duo_id;
    }

    public void setAuth_duo_id(Long auth_duo_id) {
        this.auth_duo_id = auth_duo_id;
    }

    public Integer getCpa_new() {
        return cpa_new;
    }

    public void setCpa_new(Integer cpa_new) {
        this.cpa_new = cpa_new;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getBatch_no() {
        return batch_no;
    }

    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public Long getGoods_quantity() {
        return goods_quantity;
    }

    public void setGoods_quantity(Long goods_quantity) {
        this.goods_quantity = goods_quantity;
    }

    public Long getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(Long goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_thumbnail_url() {
        return goods_thumbnail_url;
    }

    public void setGoods_thumbnail_url(String goods_thumbnail_url) {
        this.goods_thumbnail_url = goods_thumbnail_url;
    }

    public Long getOrder_receive_time() {
        return order_receive_time;
    }

    public void setOrder_receive_time(Long order_receive_time) {
        this.order_receive_time = order_receive_time;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public Long getPromotion_amount() {
        return promotion_amount;
    }

    public void setPromotion_amount(Long promotion_amount) {
        this.promotion_amount = promotion_amount;
    }

    public Long getOrder_pay_time() {
        return order_pay_time;
    }

    public void setOrder_pay_time(Long order_pay_time) {
        this.order_pay_time = order_pay_time;
    }

    public Long getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Long group_id) {
        this.group_id = group_id;
    }

    public Integer getDuo_coupon_amount() {
        return duo_coupon_amount;
    }

    public void setDuo_coupon_amount(Integer duo_coupon_amount) {
        this.duo_coupon_amount = duo_coupon_amount;
    }

    public Integer getScene_at_market_fee() {
        return scene_at_market_fee;
    }

    public void setScene_at_market_fee(Integer scene_at_market_fee) {
        this.scene_at_market_fee = scene_at_market_fee;
    }

    public String getOrder_status_desc() {
        return order_status_desc;
    }

    public void setOrder_status_desc(String order_status_desc) {
        this.order_status_desc = order_status_desc;
    }

    public String getFail_reason() {
        return fail_reason;
    }

    public void setFail_reason(String fail_reason) {
        this.fail_reason = fail_reason;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public Integer getZs_duo_id() {
        return zs_duo_id;
    }

    public void setZs_duo_id(Integer zs_duo_id) {
        this.zs_duo_id = zs_duo_id;
    }

    @Override
    public String toString() {
        return "PddOrdersResponse{" +
                "match_channel=" + match_channel +
                ", goods_price=" + goods_price +
                ", promotion_rate=" + promotion_rate +
                ", type=" + type +
                ", order_status=" + order_status +
                ", order_create_time=" + order_create_time +
                ", order_settle_time=" + order_settle_time +
                ", order_verify_time=" + order_verify_time +
                ", order_group_success_time=" + order_group_success_time +
                ", order_amount=" + order_amount +
                ", order_modify_at=" + order_modify_at +
                ", auth_duo_id=" + auth_duo_id +
                ", cpa_new=" + cpa_new +
                ", goods_name='" + goods_name + '\'' +
                ", batch_no='" + batch_no + '\'' +
                ", goods_quantity=" + goods_quantity +
                ", goods_id=" + goods_id +
                ", goods_thumbnail_url='" + goods_thumbnail_url + '\'' +
                ", order_receive_time=" + order_receive_time +
                ", custom_parameters='" + custom_parameters + '\'' +
                ", promotion_amount=" + promotion_amount +
                ", order_pay_time=" + order_pay_time +
                ", group_id=" + group_id +
                ", duo_coupon_amount=" + duo_coupon_amount +
                ", scene_at_market_fee=" + scene_at_market_fee +
                ", order_status_desc='" + order_status_desc + '\'' +
                ", fail_reason='" + fail_reason + '\'' +
                ", order_id='" + order_id + '\'' +
                ", order_sn='" + order_sn + '\'' +
                ", p_id='" + p_id + '\'' +
                ", zs_duo_id=" + zs_duo_id +
                '}';
    }

}
