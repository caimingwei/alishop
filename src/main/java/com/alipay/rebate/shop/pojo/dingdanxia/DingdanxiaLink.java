package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.Arrays;

public class DingdanxiaLink {

  private Long category_id;
  private String coupon_click_url;
  private String coupon_start_time;
  private String coupon_end_time;
  private String coupon_info;
  private String coupon;
  private Long item_id;
  private String max_commission_rate;
  private Integer coupon_total_count;
  private Integer coupon_remain_count;
  private Integer mm_coupon_remain_count;
  private Integer mm_coupon_total_count;
  private String mm_coupon_click_url;
  private String mm_coupon_start_time;
  private String mm_coupon_end_time;
  private String mm_coupon_info;
  private Integer coupon_type;
  private String item_url;
  private String coupon_tpwd;
  private String item_tpwd;
  private String[] itemInfo;
  private String title;
  private String pict_url;
  private String reserve_price;
  private String zk_final_price;
  private String qh_final_price;
  private String qh_final_commission;
  private String activityId;

  public Long getCategory_id() {
    return category_id;
  }

  public void setCategory_id(Long category_id) {
    this.category_id = category_id;
  }

  public String getCoupon_click_url() {
    return coupon_click_url;
  }

  public void setCoupon_click_url(String coupon_click_url) {
    this.coupon_click_url = coupon_click_url;
  }

  public String getCoupon_start_time() {
    return coupon_start_time;
  }

  public void setCoupon_start_time(String coupon_start_time) {
    this.coupon_start_time = coupon_start_time;
  }

  public String getCoupon_end_time() {
    return coupon_end_time;
  }

  public void setCoupon_end_time(String coupon_end_time) {
    this.coupon_end_time = coupon_end_time;
  }

  public String getCoupon_info() {
    return coupon_info;
  }

  public void setCoupon_info(String coupon_info) {
    this.coupon_info = coupon_info;
  }

  public String getCoupon() {
    return coupon;
  }

  public void setCoupon(String coupon) {
    this.coupon = coupon;
  }

  public Long getItem_id() {
    return item_id;
  }

  public void setItem_id(Long item_id) {
    this.item_id = item_id;
  }

  public String getMax_commission_rate() {
    return max_commission_rate;
  }

  public void setMax_commission_rate(String max_commission_rate) {
    this.max_commission_rate = max_commission_rate;
  }

  public Integer getCoupon_total_count() {
    return coupon_total_count;
  }

  public void setCoupon_total_count(Integer coupon_total_count) {
    this.coupon_total_count = coupon_total_count;
  }

  public Integer getCoupon_remain_count() {
    return coupon_remain_count;
  }

  public void setCoupon_remain_count(Integer coupon_remain_count) {
    this.coupon_remain_count = coupon_remain_count;
  }

  public Integer getMm_coupon_remain_count() {
    return mm_coupon_remain_count;
  }

  public void setMm_coupon_remain_count(Integer mm_coupon_remain_count) {
    this.mm_coupon_remain_count = mm_coupon_remain_count;
  }

  public Integer getMm_coupon_total_count() {
    return mm_coupon_total_count;
  }

  public void setMm_coupon_total_count(Integer mm_coupon_total_count) {
    this.mm_coupon_total_count = mm_coupon_total_count;
  }

  public String getMm_coupon_click_url() {
    return mm_coupon_click_url;
  }

  public void setMm_coupon_click_url(String mm_coupon_click_url) {
    this.mm_coupon_click_url = mm_coupon_click_url;
  }

  public String getMm_coupon_start_time() {
    return mm_coupon_start_time;
  }

  public void setMm_coupon_start_time(String mm_coupon_start_time) {
    this.mm_coupon_start_time = mm_coupon_start_time;
  }

  public String getMm_coupon_end_time() {
    return mm_coupon_end_time;
  }

  public void setMm_coupon_end_time(String mm_coupon_end_time) {
    this.mm_coupon_end_time = mm_coupon_end_time;
  }

  public String getMm_coupon_info() {
    return mm_coupon_info;
  }

  public void setMm_coupon_info(String mm_coupon_info) {
    this.mm_coupon_info = mm_coupon_info;
  }

  public Integer getCoupon_type() {
    return coupon_type;
  }

  public void setCoupon_type(Integer coupon_type) {
    this.coupon_type = coupon_type;
  }

  public String getItem_url() {
    return item_url;
  }

  public void setItem_url(String item_url) {
    this.item_url = item_url;
  }

  public String getCoupon_tpwd() {
    return coupon_tpwd;
  }

  public void setCoupon_tpwd(String coupon_tpwd) {
    this.coupon_tpwd = coupon_tpwd;
  }

  public String getItem_tpwd() {
    return item_tpwd;
  }

  public void setItem_tpwd(String item_tpwd) {
    this.item_tpwd = item_tpwd;
  }

  public String[] getItemInfo() {
    return itemInfo;
  }

  public void setItemInfo(String[] itemInfo) {
    this.itemInfo = itemInfo;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPict_url() {
    return pict_url;
  }

  public void setPict_url(String pict_url) {
    this.pict_url = pict_url;
  }

  public String getReserve_price() {
    return reserve_price;
  }

  public void setReserve_price(String reserve_price) {
    this.reserve_price = reserve_price;
  }

  public String getZk_final_price() {
    return zk_final_price;
  }

  public void setZk_final_price(String zk_final_price) {
    this.zk_final_price = zk_final_price;
  }

  public String getQh_final_price() {
    return qh_final_price;
  }

  public void setQh_final_price(String qh_final_price) {
    this.qh_final_price = qh_final_price;
  }

  public String getQh_final_commission() {
    return qh_final_commission;
  }

  public void setQh_final_commission(String qh_final_commission) {
    this.qh_final_commission = qh_final_commission;
  }

  public String getActivityId() {
    return activityId;
  }

  public void setActivityId(String activityId) {
    this.activityId = activityId;
  }

  @Override
  public String toString() {
    return "DingdanxiaLink{" +
        "category_id=" + category_id +
        ", coupon_click_url='" + coupon_click_url + '\'' +
        ", coupon_start_time='" + coupon_start_time + '\'' +
        ", coupon_end_time='" + coupon_end_time + '\'' +
        ", coupon_info='" + coupon_info + '\'' +
        ", coupon='" + coupon + '\'' +
        ", item_id=" + item_id +
        ", max_commission_rate='" + max_commission_rate + '\'' +
        ", coupon_total_count=" + coupon_total_count +
        ", coupon_remain_count=" + coupon_remain_count +
        ", mm_coupon_remain_count=" + mm_coupon_remain_count +
        ", mm_coupon_total_count=" + mm_coupon_total_count +
        ", mm_coupon_click_url='" + mm_coupon_click_url + '\'' +
        ", mm_coupon_start_time='" + mm_coupon_start_time + '\'' +
        ", mm_coupon_end_time='" + mm_coupon_end_time + '\'' +
        ", mm_coupon_info='" + mm_coupon_info + '\'' +
        ", coupon_type=" + coupon_type +
        ", item_url='" + item_url + '\'' +
        ", coupon_tpwd='" + coupon_tpwd + '\'' +
        ", item_tpwd='" + item_tpwd + '\'' +
        ", itemInfo=" + Arrays.toString(itemInfo) +
        ", title='" + title + '\'' +
        ", pict_url='" + pict_url + '\'' +
        ", reserve_price='" + reserve_price + '\'' +
        ", zk_final_price='" + zk_final_price + '\'' +
        ", qh_final_price='" + qh_final_price + '\'' +
        ", qh_final_commission='" + qh_final_commission + '\'' +
        ", activityId='" + activityId + '\'' +
        '}';
  }
}
