package com.alipay.rebate.shop.pojo.mobilecode;

public class MobileCodeStatistical {
    private String date;
    private Integer sendSmsNum;
    private Integer registerSendSmsNum;
    private Integer sendSmsSuccessNum;
    private Integer sendSmsFailNum;
    private Integer loginSendSmsNum;
    private Integer updatePasswordSendSmsNum;
    private Integer updatePhoneSendSmsNum;
    private Integer updateAlipayAccountSendSmsNum;
    private Integer sendWithDrawSmsNum;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getSendSmsNum() {
        return sendSmsNum;
    }

    public void setSendSmsNum(Integer sendSmsNum) {
        this.sendSmsNum = sendSmsNum;
    }

    public Integer getRegisterSendSmsNum() {
        return registerSendSmsNum;
    }

    public void setRegisterSendSmsNum(Integer registerSendSmsNum) {
        this.registerSendSmsNum = registerSendSmsNum;
    }

    public Integer getSendSmsSuccessNum() {
        return sendSmsSuccessNum;
    }

    public void setSendSmsSuccessNum(Integer sendSmsSuccessNum) {
        this.sendSmsSuccessNum = sendSmsSuccessNum;
    }

    public Integer getSendSmsFailNum() {
        return sendSmsFailNum;
    }

    public void setSendSmsFailNum(Integer sendSmsFailNum) {
        this.sendSmsFailNum = sendSmsFailNum;
    }

    public Integer getLoginSendSmsNum() {
        return loginSendSmsNum;
    }

    public void setLoginSendSmsNum(Integer loginSendSmsNum) {
        this.loginSendSmsNum = loginSendSmsNum;
    }

    public Integer getUpdatePasswordSendSmsNum() {
        return updatePasswordSendSmsNum;
    }

    public void setUpdatePasswordSendSmsNum(Integer updatePasswordSendSmsNum) {
        this.updatePasswordSendSmsNum = updatePasswordSendSmsNum;
    }

    public Integer getUpdatePhoneSendSmsNum() {
        return updatePhoneSendSmsNum;
    }

    public void setUpdatePhoneSendSmsNum(Integer updatePhoneSendSmsNum) {
        this.updatePhoneSendSmsNum = updatePhoneSendSmsNum;
    }

    public Integer getUpdateAlipayAccountSendSmsNum() {
        return updateAlipayAccountSendSmsNum;
    }

    public void setUpdateAlipayAccountSendSmsNum(Integer updateAlipayAccountSendSmsNum) {
        this.updateAlipayAccountSendSmsNum = updateAlipayAccountSendSmsNum;
    }

    public Integer getSendWithDrawSmsNum() {
        return sendWithDrawSmsNum;
    }

    public void setSendWithDrawSmsNum(Integer sendWithDrawSmsNum) {
        this.sendWithDrawSmsNum = sendWithDrawSmsNum;
    }

    @Override
    public String toString() {
        return "MobileCodeStatistical{" +
                "date='" + date + '\'' +
                ", sendSmsNum=" + sendSmsNum +
                ", registerSendSmsNum=" + registerSendSmsNum +
                ", sendSmsSuccessNum=" + sendSmsSuccessNum +
                ", sendSmsFailNum=" + sendSmsFailNum +
                ", loginSendSmsNum=" + loginSendSmsNum +
                ", updatePasswordSendSmsNum=" + updatePasswordSendSmsNum +
                ", updatePhoneSendSmsNum=" + updatePhoneSendSmsNum +
                ", updateAlipayAccountSendSmsNum=" + updateAlipayAccountSendSmsNum +
                ", sendWithDrawSmsNum=" + sendWithDrawSmsNum +
                '}';
    }
}
