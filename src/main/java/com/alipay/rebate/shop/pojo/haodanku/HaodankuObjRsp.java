package com.alipay.rebate.shop.pojo.haodanku;

public class HaodankuObjRsp{

  Integer code;
  Integer min_id;
  String msg;
  Object data;

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public Integer getMin_id() {
    return min_id;
  }

  public void setMin_id(Integer min_id) {
    this.min_id = min_id;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "HaodankuObjRsp{" +
        "code=" + code +
        ", min_id=" + min_id +
        ", msg='" + msg + '\'' +
        ", data=" + data +
        '}';
  }
}
