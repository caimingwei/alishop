package com.alipay.rebate.shop.pojo.user.admin;

import com.alibaba.fastjson.annotation.JSONField;

import java.math.BigDecimal;
import java.util.Date;
public class UserListResponse {

    private Long userId;
    private String userNickName;
    private String parentUserName;
    private String phone;
    private Date userRegisterTime;
    private Date lastLoginTime;
    @JSONField(format = "￥#0.00")
    private String userAmount;
    private Long userIntgeralTreasure;
    private Long userIntegral;
    private Integer isAuth;
    private Integer userStatus;
    private Long relationId;
    private Long specialId;
    private String lastLoginIp;
    private Integer loginPlatform;
    private String deviceModel;
    private BigDecimal userTotalIncome;
    private Long parentUserId;
    private String latelyActiveTime;

    public String getLatelyActiveTime() {
        return latelyActiveTime;
    }

    public void setLatelyActiveTime(String latelyActiveTime) {
        this.latelyActiveTime = latelyActiveTime;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getSpecialId() {
        return specialId;
    }

    public void setSpecialId(Long specialId) {
        this.specialId = specialId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getParentUserName() {
        return parentUserName;
    }

    public void setParentUserName(String parentUserName) {
        this.parentUserName = parentUserName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getUserRegisterTime() {
        return userRegisterTime;
    }

    public void setUserRegisterTime(Date userRegisterTime) {
        this.userRegisterTime = userRegisterTime;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(String userAmount) {

        this.userAmount = userAmount;
    }

    public Long getUserIntgeralTreasure() {
        return userIntgeralTreasure;
    }

    public void setUserIntgeralTreasure(Long userIntgeralTreasure) {
        this.userIntgeralTreasure = userIntgeralTreasure;
    }

    public Long getUserIntegral() {
        return userIntegral;
    }

    public void setUserIntegral(Long userIntegral) {
        this.userIntegral = userIntegral;
    }

    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public Integer getLoginPlatform() {
        return loginPlatform;
    }

    public void setLoginPlatform(Integer loginPlatform) {
        this.loginPlatform = loginPlatform;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public BigDecimal getUserTotalIncome() {
        return userTotalIncome;
    }

    public void setUserTotalIncome(BigDecimal userTotalIncome) {
        this.userTotalIncome = userTotalIncome;
    }

    public Long getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Long parentUserId) {
        this.parentUserId = parentUserId;
    }

    @Override
    public String toString() {
        return "UserListResponse{" +
                "userId=" + userId +
                ", userNickName='" + userNickName + '\'' +
                ", parentUserName='" + parentUserName + '\'' +
                ", phone='" + phone + '\'' +
                ", userRegisterTime=" + userRegisterTime +
                ", lastLoginTime=" + lastLoginTime +
                ", userAmount='" + userAmount + '\'' +
                ", userIntgeralTreasure=" + userIntgeralTreasure +
                ", userIntegral=" + userIntegral +
                ", isAuth=" + isAuth +
                ", userStatus=" + userStatus +
                ", relationId=" + relationId +
                ", specialId=" + specialId +
                ", lastLoginIp='" + lastLoginIp + '\'' +
                ", loginPlatform=" + loginPlatform +
                ", deviceModel='" + deviceModel + '\'' +
                ", userTotalIncome=" + userTotalIncome +
                ", parentUserId=" + parentUserId +
                ", latelyActiveTime='" + latelyActiveTime + '\'' +
                '}';
    }
}
