package com.alipay.rebate.shop.pojo.pdtframework;

import com.alipay.rebate.shop.model.Product;
import com.github.pagehelper.PageInfo;

public class AppPdtFrameworkAndPdtRsp {

  private Object framework;

  PageInfo<Product> data;

  public Object getFramework() {
    return framework;
  }

  public void setFramework(Object framework) {
    this.framework = framework;
  }

  public PageInfo<Product> getData() {
    return data;
  }

  public void setData(PageInfo<Product> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "AppPdtFrameworkAndPdtRsp{" +
        "framework=" + framework +
        ", data=" + data +
        '}';
  }
}
