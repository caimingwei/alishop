package com.alipay.rebate.shop.pojo.dataoke;

public class DataokePrivilegeLinkData {

  private String couponClickUrl;
  private String couponEndTime;
  private String couponInfo;
  private String couponStartTime;
  private Number itemId;
  private Number couponTotalCount;
  private Number couponRemainCount;
  private String itemUrl;
  private String tpwd;
  private Number maxCommissionRate;

  public String getCouponClickUrl() {
    return couponClickUrl;
  }

  public void setCouponClickUrl(String couponClickUrl) {
    this.couponClickUrl = couponClickUrl;
  }

  public String getCouponEndTime() {
    return couponEndTime;
  }

  public void setCouponEndTime(String couponEndTime) {
    this.couponEndTime = couponEndTime;
  }

  public String getCouponInfo() {
    return couponInfo;
  }

  public void setCouponInfo(String couponInfo) {
    this.couponInfo = couponInfo;
  }

  public String getCouponStartTime() {
    return couponStartTime;
  }

  public void setCouponStartTime(String couponStartTime) {
    this.couponStartTime = couponStartTime;
  }

  public Number getItemId() {
    return itemId;
  }

  public void setItemId(Number itemId) {
    this.itemId = itemId;
  }

  public Number getCouponTotalCount() {
    return couponTotalCount;
  }

  public void setCouponTotalCount(Number couponTotalCount) {
    this.couponTotalCount = couponTotalCount;
  }

  public Number getCouponRemainCount() {
    return couponRemainCount;
  }

  public void setCouponRemainCount(Number couponRemainCount) {
    this.couponRemainCount = couponRemainCount;
  }

  public String getItemUrl() {
    return itemUrl;
  }

  public void setItemUrl(String itemUrl) {
    this.itemUrl = itemUrl;
  }

  public String getTpwd() {
    return tpwd;
  }

  public void setTpwd(String tpwd) {
    this.tpwd = tpwd;
  }

  public Number getMaxCommissionRate() {
    return maxCommissionRate;
  }

  public void setMaxCommissionRate(Number maxCommissionRate) {
    this.maxCommissionRate = maxCommissionRate;
  }

  @Override
  public String toString() {
    return "DataokeDataResponse{" +
        "couponClickUrl='" + couponClickUrl + '\'' +
        ", couponEndTime='" + couponEndTime + '\'' +
        ", couponInfo='" + couponInfo + '\'' +
        ", couponStartTime='" + couponStartTime + '\'' +
        ", itemId=" + itemId +
        ", couponTotalCount=" + couponTotalCount +
        ", couponRemainCount=" + couponRemainCount +
        ", itemUrl='" + itemUrl + '\'' +
        ", tpwd='" + tpwd + '\'' +
        ", maxCommissionRate=" + maxCommissionRate +
        '}';
  }
}
