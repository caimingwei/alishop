package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;
import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class SearchProductRequest extends CommonPageRequest {

  @Min(value = 1, message = "sort范围为1-12")
  @Max(value = 12, message = "sort范围为1-12")
  private Integer sort;
  @Min(value = 1, message = "cid范围为1-14")
  @Max(value = 14, message = "cid范围为1-14")
  private Integer cid;
  private Integer scid;
  @Min(value = 0, message = "是否聚划算可选值[0,1]")
  @Max(value = 1, message = "是否聚划算可选值[0,1]")
  private Integer juHuaSuan;
  @Min(value = 0, message = "是否淘抢购可选值[0,1]")
  @Max(value = 1, message = "是否淘抢购可选值[0,1]")
  private Integer taoQiangGou;
  @Min(value = 0, message = "是否天猫商品可选值[0,1]")
  @Max(value = 1, message = "是否天猫商品可选值[0,1]")
  private Integer tmall;
  @Min(value = 0, message = "是否天猫超市商品可选值[0,1]")
  @Max(value = 1, message = "是否天猫超市商品可选值[0,1]")
  private Integer tchaoshi;
  @DecimalMin(value = "0.00",message = "金额需为正数")
  private BigDecimal priceLowerLimit;
  @DecimalMin(value = "0.00",message = "金额需为正数")
  private BigDecimal priceUpperLimit;
  @Min(value = 0,message = "月销量需为正数")
  private Integer monthSalesMin;
  @Min(value = 0,message = "月销量需为正数")
  private Integer monthSalesMax;
  @DecimalMin(value = "0.00",message = "金额需为正数")
  private BigDecimal couponMin;
  @DecimalMin(value = "0.00",message = "金额需为正数")
  private BigDecimal couponMax;
  @DecimalMin(value = "0.00",message = "比例为正数")
  private BigDecimal commissionRateMin;
  @DecimalMin(value = "0.00",message = "比例为正数")
  private BigDecimal commissionRateMax;
  @Min(value = 1,message = "活动类型范围为 1,2,3")
  @Max(value = 3,message = "活动类型范围为 1,2,3")
  private Integer activityType;
  private String keyword;

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public Integer getCid() {
    return cid;
  }

  public void setCid(Integer cid) {
    this.cid = cid;
  }

  public Integer getScid() {
    return scid;
  }

  public void setScid(Integer scid) {
    this.scid = scid;
  }

  public Integer getJuHuaSuan() {
    return juHuaSuan;
  }

  public void setJuHuaSuan(Integer juHuaSuan) {
    this.juHuaSuan = juHuaSuan;
  }

  public Integer getTaoQiangGou() {
    return taoQiangGou;
  }

  public void setTaoQiangGou(Integer taoQiangGou) {
    this.taoQiangGou = taoQiangGou;
  }

  public Integer getTmall() {
    return tmall;
  }

  public void setTmall(Integer tmall) {
    this.tmall = tmall;
  }

  public Integer getTchaoshi() {
    return tchaoshi;
  }

  public void setTchaoshi(Integer tchaoshi) {
    this.tchaoshi = tchaoshi;
  }

  public BigDecimal getPriceLowerLimit() {
    return priceLowerLimit;
  }

  public void setPriceLowerLimit(BigDecimal priceLowerLimit) {
    this.priceLowerLimit = priceLowerLimit;
  }

  public BigDecimal getPriceUpperLimit() {
    return priceUpperLimit;
  }

  public void setPriceUpperLimit(BigDecimal priceUpperLimit) {
    this.priceUpperLimit = priceUpperLimit;
  }

  public Integer getMonthSalesMin() {
    return monthSalesMin;
  }

  public void setMonthSalesMin(Integer monthSalesMin) {
    this.monthSalesMin = monthSalesMin;
  }

  public Integer getMonthSalesMax() {
    return monthSalesMax;
  }

  public void setMonthSalesMax(Integer monthSalesMax) {
    this.monthSalesMax = monthSalesMax;
  }

  public BigDecimal getCouponMin() {
    return couponMin;
  }

  public void setCouponMin(BigDecimal couponMin) {
    this.couponMin = couponMin;
  }

  public BigDecimal getCouponMax() {
    return couponMax;
  }

  public void setCouponMax(BigDecimal couponMax) {
    this.couponMax = couponMax;
  }

  public BigDecimal getCommissionRateMin() {
    return commissionRateMin;
  }

  public void setCommissionRateMin(BigDecimal commissionRateMin) {
    this.commissionRateMin = commissionRateMin;
  }

  public BigDecimal getCommissionRateMax() {
    return commissionRateMax;
  }

  public void setCommissionRateMax(BigDecimal commissionRateMax) {
    this.commissionRateMax = commissionRateMax;
  }

  public Integer getActivityType() {
    return activityType;
  }

  public void setActivityType(Integer activityType) {
    this.activityType = activityType;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  @Override
  public String toString() {
    return "SearchProductRequest{" +
        "sort=" + sort +
        ", cid=" + cid +
        ", scid=" + scid +
        ", juHuaSuan=" + juHuaSuan +
        ", taoQiangGou=" + taoQiangGou +
        ", tmall=" + tmall +
        ", tchaoshi=" + tchaoshi +
        ", priceLowerLimit=" + priceLowerLimit +
        ", priceUpperLimit=" + priceUpperLimit +
        ", monthSalesMin=" + monthSalesMin +
        ", monthSalesMax=" + monthSalesMax +
        ", couponMin=" + couponMin +
        ", couponMax=" + couponMax +
        ", commissionRateMin=" + commissionRateMin +
        ", commissionRateMax=" + commissionRateMax +
        ", activityType=" + activityType +
        ", keyword='" + keyword + '\'' +
        "} " + super.toString();
  }
}
