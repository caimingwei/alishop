package com.alipay.rebate.shop.pojo.taobao;

public class RefundOrderRequest {

  private Long bizType;
  private Long pageNo;
  private Long pageSize;
  private Long refundType;
  private Long searchType;
  private String startTime;

  public Long getBizType() {
    return bizType;
  }

  public void setBizType(Long bizType) {
    this.bizType = bizType;
  }

  public Long getPageNo() {
    return pageNo;
  }

  public void setPageNo(Long pageNo) {
    this.pageNo = pageNo;
  }

  public Long getPageSize() {
    return pageSize;
  }

  public void setPageSize(Long pageSize) {
    this.pageSize = pageSize;
  }

  public Long getRefundType() {
    return refundType;
  }

  public void setRefundType(Long refundType) {
    this.refundType = refundType;
  }

  public Long getSearchType() {
    return searchType;
  }

  public void setSearchType(Long searchType) {
    this.searchType = searchType;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  @Override
  public String toString() {
    return "RefundOrderRequest{" +
        "bizType=" + bizType +
        ", pageNo=" + pageNo +
        ", pageSize=" + pageSize +
        ", refundType=" + refundType +
        ", searchType=" + searchType +
        ", startTime=" + startTime +
        '}';
  }
}
