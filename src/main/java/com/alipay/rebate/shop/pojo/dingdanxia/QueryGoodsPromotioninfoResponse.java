package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;

public class QueryGoodsPromotioninfoResponse {

    private Long skuId;
    private BigDecimal unitPrice;
    private String materialUrl;
    private Long endDate;
    private Integer isFreeFreightRisk;
    private Integer isFreeShipping;
    private BigDecimal commisionRatioWl;
    private BigDecimal commisionRatioPc;
    private String imgUrl;
    private Long vid;
    private String cidName;
    private BigDecimal wlUnitPrice;
    private String cid2Name;
    private Integer isSeckill;
    private Long cid2;
    private String cid3Name;
    private Long inOrderCount;
    private Long cid3;
    private Long shopId;
    private Integer isJdSale;
    private String goodsName;
    private Long startDate;
    private Long cid;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getMaterialUrl() {
        return materialUrl;
    }

    public void setMaterialUrl(String materialUrl) {
        this.materialUrl = materialUrl;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Integer getIsFreeFreightRisk() {
        return isFreeFreightRisk;
    }

    public void setIsFreeFreightRisk(Integer isFreeFreightRisk) {
        this.isFreeFreightRisk = isFreeFreightRisk;
    }

    public Integer getIsFreeShipping() {
        return isFreeShipping;
    }

    public void setIsFreeShipping(Integer isFreeShipping) {
        this.isFreeShipping = isFreeShipping;
    }

    public BigDecimal getCommisionRatioWl() {
        return commisionRatioWl;
    }

    public void setCommisionRatioWl(BigDecimal commisionRatioWl) {
        this.commisionRatioWl = commisionRatioWl;
    }

    public BigDecimal getCommisionRatioPc() {
        return commisionRatioPc;
    }

    public void setCommisionRatioPc(BigDecimal commisionRatioPc) {
        this.commisionRatioPc = commisionRatioPc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Long getVid() {
        return vid;
    }

    public void setVid(Long vid) {
        this.vid = vid;
    }

    public String getCidName() {
        return cidName;
    }

    public void setCidName(String cidName) {
        this.cidName = cidName;
    }

    public BigDecimal getWlUnitPrice() {
        return wlUnitPrice;
    }

    public void setWlUnitPrice(BigDecimal wlUnitPrice) {
        this.wlUnitPrice = wlUnitPrice;
    }

    public String getCid2Name() {
        return cid2Name;
    }

    public void setCid2Name(String cid2Name) {
        this.cid2Name = cid2Name;
    }

    public Integer getIsSeckill() {
        return isSeckill;
    }

    public void setIsSeckill(Integer isSeckill) {
        this.isSeckill = isSeckill;
    }

    public Long getCid2() {
        return cid2;
    }

    public void setCid2(Long cid2) {
        this.cid2 = cid2;
    }

    public String getCid3Name() {
        return cid3Name;
    }

    public void setCid3Name(String cid3Name) {
        this.cid3Name = cid3Name;
    }

    public Long getInOrderCount() {
        return inOrderCount;
    }

    public void setInOrderCount(Long inOrderCount) {
        this.inOrderCount = inOrderCount;
    }

    public Long getCid3() {
        return cid3;
    }

    public void setCid3(Long cid3) {
        this.cid3 = cid3;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Integer getIsJdSale() {
        return isJdSale;
    }

    public void setIsJdSale(Integer isJdSale) {
        this.isJdSale = isJdSale;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    @Override
    public String toString() {
        return "QueryGoodsPromotioninfoResponse{" +
                "skuId=" + skuId +
                ", unitPrice=" + unitPrice +
                ", materialUrl='" + materialUrl + '\'' +
                ", endDate=" + endDate +
                ", isFreeFreightRisk=" + isFreeFreightRisk +
                ", isFreeShipping=" + isFreeShipping +
                ", commisionRatioWl=" + commisionRatioWl +
                ", commisionRatioPc=" + commisionRatioPc +
                ", imgUrl='" + imgUrl + '\'' +
                ", vid=" + vid +
                ", cidName='" + cidName + '\'' +
                ", wlUnitPrice=" + wlUnitPrice +
                ", cid2Name='" + cid2Name + '\'' +
                ", isSeckill=" + isSeckill +
                ", cid2=" + cid2 +
                ", cid3Name='" + cid3Name + '\'' +
                ", inOrderCount=" + inOrderCount +
                ", cid3=" + cid3 +
                ", shopId=" + shopId +
                ", isJdSale=" + isJdSale +
                ", goodsName='" + goodsName + '\'' +
                ", startDate=" + startDate +
                ", cid=" + cid +
                '}';
    }
}
