package com.alipay.rebate.shop.pojo.user.admin;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;
import java.math.BigDecimal;

public class UserPageRequest extends CommonPageRequest {

    private String userNickName;
    private String phone;
    private Long userId;
    private Long parentUserId;
    private Long relationOrSpecialId;
    private BigDecimal userStartAmount;
    private BigDecimal userEndAmount;
    private Long userStartIntgeralTreasure;
    private Long userEndIntgeralTreasure;
    private Long userStartIntgeral;
    private Long userEndIntgeral;
    private Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Long parentUserId) {
        this.parentUserId = parentUserId;
    }

    public Long getRelationOrSpecialId() {
        return relationOrSpecialId;
    }

    public void setRelationOrSpecialId(Long relationOrSpecialId) {
        this.relationOrSpecialId = relationOrSpecialId;
    }

    public BigDecimal getUserStartAmount() {
        return userStartAmount;
    }

    public void setUserStartAmount(BigDecimal userStartAmount) {
        this.userStartAmount = userStartAmount;
    }

    public BigDecimal getUserEndAmount() {
        return userEndAmount;
    }

    public void setUserEndAmount(BigDecimal userEndAmount) {
        this.userEndAmount = userEndAmount;
    }

    public Long getUserStartIntgeralTreasure() {
        return userStartIntgeralTreasure;
    }

    public void setUserStartIntgeralTreasure(Long userStartIntgeralTreasure) {
        this.userStartIntgeralTreasure = userStartIntgeralTreasure;
    }

    public Long getUserEndIntgeralTreasure() {
        return userEndIntgeralTreasure;
    }

    public void setUserEndIntgeralTreasure(Long userEndIntgeralTreasure) {
        this.userEndIntgeralTreasure = userEndIntgeralTreasure;
    }

    public Long getUserStartIntgeral() {
        return userStartIntgeral;
    }

    public void setUserStartIntgeral(Long userStartIntgeral) {
        this.userStartIntgeral = userStartIntgeral;
    }

    public Long getUserEndIntgeral() {
        return userEndIntgeral;
    }

    public void setUserEndIntgeral(Long userEndIntgeral) {
        this.userEndIntgeral = userEndIntgeral;
    }

    @Override
    public String toString() {
        return "UserPageRequest{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", userNickName='" + userNickName + '\'' +
                ", phone='" + phone + '\'' +
                ", userId=" + userId +
                ", parentUserId=" + parentUserId +
                ", relationOrSpecialId=" + relationOrSpecialId +
                ", userStartAmount=" + userStartAmount +
                ", userEndAmount=" + userEndAmount +
                ", userStartIntgeralTreasure=" + userStartIntgeralTreasure +
                ", userEndIntgeralTreasure=" + userEndIntgeralTreasure +
                ", userStartIntgeral=" + userStartIntgeral +
                ", userEndIntgeral=" + userEndIntgeral +
                '}';
    }
}
