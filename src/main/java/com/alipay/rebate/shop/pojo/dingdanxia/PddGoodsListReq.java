package com.alipay.rebate.shop.pojo.dingdanxia;

public class PddGoodsListReq {


    private String apikey;
    private String signature;
    private String p_id;
    private Integer offset;
    private Integer sort_type;
    private Integer limit;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getSort_type() {
        return sort_type;
    }

    public void setSort_type(Integer sort_type) {
        this.sort_type = sort_type;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "PddGoodsListReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", p_id='" + p_id + '\'' +
                ", offset=" + offset +
                ", sort_type=" + sort_type +
                ", limit=" + limit +
                '}';
    }
}
