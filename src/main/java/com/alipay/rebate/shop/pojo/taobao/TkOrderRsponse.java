package com.alipay.rebate.shop.pojo.taobao;

import com.alipay.rebate.shop.model.Orders;
import java.util.List;

public class TkOrderRsponse {

  private List<Orders> data;
  private String position_index;
  private Boolean has_next;

  public List<Orders> getData() {
    return data;
  }

  public void setData(List<Orders> data) {
    this.data = data;
  }

  public String getPosition_index() {
    return position_index;
  }

  public void setPosition_index(String position_index) {
    this.position_index = position_index;
  }

  public Boolean getHas_next() {
    return has_next;
  }

  public void setHas_next(Boolean has_next) {
    this.has_next = has_next;
  }

  @Override
  public String toString() {
    return "OrderRsponse{" +
        "data=" + data +
        ", position_index='" + position_index + '\'' +
        ", has_next=" + has_next +
        '}';
  }
}
