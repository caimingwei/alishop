package com.alipay.rebate.shop.pojo.user.admin;

import javax.persistence.Column;
import java.math.BigDecimal;

public class ImportPddOrders {

    private String orderId;

    private Long goodsId;

    private String goodsName;

    private Long goodsPrice;

    private Long goodsQuantity;

    private String goodsThumbnailUrl;

    private Long orderAmount;

    private Long orderCreateTime;

    private Long orderGroupSuccessTime;

    private Long orderModifyAt;

    private Long orderPayTime;

    private String orderSn;

    private Integer orderStatus;

    private Long orderVerifyTime;

    private String orderStatusDesc;

    private String pId;

    private Long promotionAmount;

    private Long promotionRate;

    private Integer cpaNew;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Long goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Long getGoodsQuantity() {
        return goodsQuantity;
    }

    public void setGoodsQuantity(Long goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }

    public String getGoodsThumbnailUrl() {
        return goodsThumbnailUrl;
    }

    public void setGoodsThumbnailUrl(String goodsThumbnailUrl) {
        this.goodsThumbnailUrl = goodsThumbnailUrl;
    }

    public Long getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Long orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Long getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(Long orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public Long getOrderGroupSuccessTime() {
        return orderGroupSuccessTime;
    }

    public void setOrderGroupSuccessTime(Long orderGroupSuccessTime) {
        this.orderGroupSuccessTime = orderGroupSuccessTime;
    }

    public Long getOrderModifyAt() {
        return orderModifyAt;
    }

    public void setOrderModifyAt(Long orderModifyAt) {
        this.orderModifyAt = orderModifyAt;
    }

    public Long getOrderPayTime() {
        return orderPayTime;
    }

    public void setOrderPayTime(Long orderPayTime) {
        this.orderPayTime = orderPayTime;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getOrderVerifyTime() {
        return orderVerifyTime;
    }

    public void setOrderVerifyTime(Long orderVerifyTime) {
        this.orderVerifyTime = orderVerifyTime;
    }

    public String getOrderStatusDesc() {
        return orderStatusDesc;
    }

    public void setOrderStatusDesc(String orderStatusDesc) {
        this.orderStatusDesc = orderStatusDesc;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public Long getPromotionAmount() {
        return promotionAmount;
    }

    public void setPromotionAmount(Long promotionAmount) {
        this.promotionAmount = promotionAmount;
    }

    public Long getPromotionRate() {
        return promotionRate;
    }

    public void setPromotionRate(Long promotionRate) {
        this.promotionRate = promotionRate;
    }

    public Integer getCpaNew() {
        return cpaNew;
    }

    public void setCpaNew(Integer cpaNew) {
        this.cpaNew = cpaNew;
    }

    @Override
    public String toString() {
        return "ImportPddOrders{" +
                "orderId='" + orderId + '\'' +
                ", goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", goodsPrice=" + goodsPrice +
                ", goodsQuantity=" + goodsQuantity +
                ", goodsThumbnailUrl='" + goodsThumbnailUrl + '\'' +
                ", orderAmount=" + orderAmount +
                ", orderCreateTime=" + orderCreateTime +
                ", orderGroupSuccessTime=" + orderGroupSuccessTime +
                ", orderModifyAt=" + orderModifyAt +
                ", orderPayTime=" + orderPayTime +
                ", orderSn='" + orderSn + '\'' +
                ", orderStatus=" + orderStatus +
                ", orderVerifyTime=" + orderVerifyTime +
                ", orderStatusDesc='" + orderStatusDesc + '\'' +
                ", pId='" + pId + '\'' +
                ", promotionAmount=" + promotionAmount +
                ", promotionRate=" + promotionRate +
                ", cpaNew=" + cpaNew +
                '}';
    }
}
