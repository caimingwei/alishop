package com.alipay.rebate.shop.pojo.user.product;

import com.alipay.rebate.shop.model.Activity;
import com.alipay.rebate.shop.model.ActivityProduct;
import com.github.pagehelper.PageInfo;

public class ActivityProductRsp {

  Activity activity;
  PageInfo<ActivityProduct> pageInfo;

  public Activity getActivity() {
    return activity;
  }

  public void setActivity(Activity activity) {
    this.activity = activity;
  }

  public PageInfo<ActivityProduct> getPageInfo() {
    return pageInfo;
  }

  public void setPageInfo(
      PageInfo<ActivityProduct> pageInfo) {
    this.pageInfo = pageInfo;
  }

  @Override
  public String toString() {
    return "ActivityProductRsp{" +
        "activity=" + activity +
        ", pageInfo=" + pageInfo +
        '}';
  }
}
