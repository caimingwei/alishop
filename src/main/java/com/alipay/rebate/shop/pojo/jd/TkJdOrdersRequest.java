package com.alipay.rebate.shop.pojo.jd;

public class TkJdOrdersRequest {

    private String apikey;
    private String signature;
    private String time;
    private Integer pageNo;
    private Integer pageSize;
    private Integer type;
    private String key;
    private Long childUnionId;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getChildUnionId() {
        return childUnionId;
    }

    public void setChildUnionId(Long childUnionId) {
        this.childUnionId = childUnionId;
    }

    @Override
    public String toString() {
        return "TkJdOrdersRequest{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", time='" + time + '\'' +
                ", pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", type=" + type +
                ", key='" + key + '\'' +
                ", childUnionId=" + childUnionId +
                '}';
    }
}
