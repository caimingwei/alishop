package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.pojo.user.customer.UserFansDetailInfo;
import com.github.pagehelper.PageInfo;

public class UserFansListResponse {

    private Integer totalFansNum;
    PageInfo<UserFansDetailInfo> oneLevelFans;
    PageInfo<UserFansDetailInfo> secondLevelFans;

    public Integer getTotalFansNum() {
        return totalFansNum;
    }

    public void setTotalFansNum(Integer totalFansNum) {
        this.totalFansNum = totalFansNum;
    }

    public PageInfo<UserFansDetailInfo> getOneLevelFans() {
        return oneLevelFans;
    }

    public void setOneLevelFans(PageInfo<UserFansDetailInfo> oneLevelFans) {
        this.oneLevelFans = oneLevelFans;
    }

    public PageInfo<UserFansDetailInfo> getSecondLevelFans() {
        return secondLevelFans;
    }

    public void setSecondLevelFans(PageInfo<UserFansDetailInfo> secondLevelFans) {
        this.secondLevelFans = secondLevelFans;
    }

    @Override
    public String toString() {
        return "UserFansListResponse{" +
                "totalFansNum=" + totalFansNum +
                ", oneLevelFans=" + oneLevelFans +
                ", secondLevelFans=" + secondLevelFans +
                '}';
    }
}
