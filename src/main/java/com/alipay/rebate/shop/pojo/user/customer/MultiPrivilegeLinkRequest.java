package com.alipay.rebate.shop.pojo.user.customer;

import java.util.List;

public class MultiPrivilegeLinkRequest {

  private String userId;
  private String userName;
  private String accessToken;
  boolean flag;
  private Long relationId;
  private Long specialId;
  private List<PrivilegeLinkProductData> data;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public boolean isFlag() {
    return flag;
  }

  public void setFlag(boolean flag) {
    this.flag = flag;
  }

  public Long getRelationId() {
    return relationId;
  }

  public void setRelationId(Long relationId) {
    this.relationId = relationId;
  }

  public Long getSpecialId() {
    return specialId;
  }

  public void setSpecialId(Long specialId) {
    this.specialId = specialId;
  }

  public List<PrivilegeLinkProductData> getData() {
    return data;
  }

  public void setData(
      List<PrivilegeLinkProductData> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "MultiPrivilegeLinkRequest{" +
        "userId='" + userId + '\'' +
        ", userName='" + userName + '\'' +
        ", accessToken='" + accessToken + '\'' +
        ", flag=" + flag +
        ", relationId=" + relationId +
        ", specialId=" + specialId +
        ", data=" + data +
        '}';
  }
}
