package com.alipay.rebate.shop.pojo.dingdanxia;

public class DingdanxiaOrder {

  private Long trade_parent_id;
  private Long trade_id;
  private Long num_iid;
  private String item_title;
  private Long item_num;
  private String price;
  private String pay_price;
  private String seller_nick;
  private String seller_shop_title;
  private String commission;
  private String commission_rate;
  private String create_time;
  private String earning_time;
  private Long tk_status;
  private String tk3rd_type;
  private Long tk3rd_pub_id;
  private String order_type;
  private String income_rate;
  private String pub_share_pre_fee;
  private String subsidy_rate;
  private String subsidy_type;
  private String terminal_type;
  private String auction_category;
  private String site_id;
  private String site_name;
  private String adzone_id;
  private String adzone_name;
  private String alipay_total_price;
  private String total_commission_rate;
  private String total_commission_fee;
  private String subsidy_fee;
  private Long relation_id;
  private Long special_id;
  private String click_time;

  public Long getTrade_parent_id() {
    return trade_parent_id;
  }

  public void setTrade_parent_id(Long trade_parent_id) {
    this.trade_parent_id = trade_parent_id;
  }

  public Long getTrade_id() {
    return trade_id;
  }

  public void setTrade_id(Long trade_id) {
    this.trade_id = trade_id;
  }

  public Long getNum_iid() {
    return num_iid;
  }

  public void setNum_iid(Long num_iid) {
    this.num_iid = num_iid;
  }

  public String getItem_title() {
    return item_title;
  }

  public void setItem_title(String item_title) {
    this.item_title = item_title;
  }

  public Long getItem_num() {
    return item_num;
  }

  public void setItem_num(Long item_num) {
    this.item_num = item_num;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getPay_price() {
    return pay_price;
  }

  public void setPay_price(String pay_price) {
    this.pay_price = pay_price;
  }

  public String getSeller_nick() {
    return seller_nick;
  }

  public void setSeller_nick(String seller_nick) {
    this.seller_nick = seller_nick;
  }

  public String getSeller_shop_title() {
    return seller_shop_title;
  }

  public void setSeller_shop_title(String seller_shop_title) {
    this.seller_shop_title = seller_shop_title;
  }

  public String getCommission() {
    return commission;
  }

  public void setCommission(String commission) {
    this.commission = commission;
  }

  public String getCommission_rate() {
    return commission_rate;
  }

  public void setCommission_rate(String commission_rate) {
    this.commission_rate = commission_rate;
  }

  public String getCreate_time() {
    return create_time;
  }

  public void setCreate_time(String create_time) {
    this.create_time = create_time;
  }

  public String getEarning_time() {
    return earning_time;
  }

  public void setEarning_time(String earning_time) {
    this.earning_time = earning_time;
  }

  public Long getTk_status() {
    return tk_status;
  }

  public void setTk_status(Long tk_status) {
    this.tk_status = tk_status;
  }

  public String getTk3rd_type() {
    return tk3rd_type;
  }

  public void setTk3rd_type(String tk3rd_type) {
    this.tk3rd_type = tk3rd_type;
  }

  public Long getTk3rd_pub_id() {
    return tk3rd_pub_id;
  }

  public void setTk3rd_pub_id(Long tk3rd_pub_id) {
    this.tk3rd_pub_id = tk3rd_pub_id;
  }

  public String getOrder_type() {
    return order_type;
  }

  public void setOrder_type(String order_type) {
    this.order_type = order_type;
  }

  public String getIncome_rate() {
    return income_rate;
  }

  public void setIncome_rate(String income_rate) {
    this.income_rate = income_rate;
  }

  public String getPub_share_pre_fee() {
    return pub_share_pre_fee;
  }

  public void setPub_share_pre_fee(String pub_share_pre_fee) {
    this.pub_share_pre_fee = pub_share_pre_fee;
  }

  public String getSubsidy_rate() {
    return subsidy_rate;
  }

  public void setSubsidy_rate(String subsidy_rate) {
    this.subsidy_rate = subsidy_rate;
  }

  public String getSubsidy_type() {
    return subsidy_type;
  }

  public void setSubsidy_type(String subsidy_type) {
    this.subsidy_type = subsidy_type;
  }

  public String getTerminal_type() {
    return terminal_type;
  }

  public void setTerminal_type(String terminal_type) {
    this.terminal_type = terminal_type;
  }

  public String getAuction_category() {
    return auction_category;
  }

  public void setAuction_category(String auction_category) {
    this.auction_category = auction_category;
  }

  public String getSite_id() {
    return site_id;
  }

  public void setSite_id(String site_id) {
    this.site_id = site_id;
  }

  public String getSite_name() {
    return site_name;
  }

  public void setSite_name(String site_name) {
    this.site_name = site_name;
  }

  public String getAdzone_id() {
    return adzone_id;
  }

  public void setAdzone_id(String adzone_id) {
    this.adzone_id = adzone_id;
  }

  public String getAdzone_name() {
    return adzone_name;
  }

  public void setAdzone_name(String adzone_name) {
    this.adzone_name = adzone_name;
  }

  public String getAlipay_total_price() {
    return alipay_total_price;
  }

  public void setAlipay_total_price(String alipay_total_price) {
    this.alipay_total_price = alipay_total_price;
  }

  public String getTotal_commission_rate() {
    return total_commission_rate;
  }

  public void setTotal_commission_rate(String total_commission_rate) {
    this.total_commission_rate = total_commission_rate;
  }

  public String getTotal_commission_fee() {
    return total_commission_fee;
  }

  public void setTotal_commission_fee(String total_commission_fee) {
    this.total_commission_fee = total_commission_fee;
  }

  public String getSubsidy_fee() {
    return subsidy_fee;
  }

  public void setSubsidy_fee(String subsidy_fee) {
    this.subsidy_fee = subsidy_fee;
  }

  public Long getRelation_id() {
    return relation_id;
  }

  public void setRelation_id(Long relation_id) {
    this.relation_id = relation_id;
  }

  public Long getSpecial_id() {
    return special_id;
  }

  public void setSpecial_id(Long special_id) {
    this.special_id = special_id;
  }

  public String getClick_time() {
    return click_time;
  }

  public void setClick_time(String click_time) {
    this.click_time = click_time;
  }

  @Override
  public String toString() {
    return "\nDingdanxiaOrder{" +
        "\ntrade_parent_id=" + trade_parent_id +
        ", \ntrade_id=" + trade_id +
        ", \nnum_iid=" + num_iid +
        ", \nitem_title='" + item_title + '\'' +
        ", \nitem_num=" + item_num +
        ", \nprice='" + price + '\'' +
        ", \npay_price='" + pay_price + '\'' +
        ", \nseller_nick='" + seller_nick + '\'' +
        ", \nseller_shop_title='" + seller_shop_title + '\'' +
        ", \ncommission='" + commission + '\'' +
        ", \ncommission_rate='" + commission_rate + '\'' +
        ", \ncreate_time='" + create_time + '\'' +
        ", \nearning_time='" + earning_time + '\'' +
        ", \ntk_status=" + tk_status +
        ", \ntk3rd_type='" + tk3rd_type + '\'' +
        ", \ntk3rd_pub_id=" + tk3rd_pub_id +
        ", \norder_type='" + order_type + '\'' +
        ", \nincome_rate='" + income_rate + '\'' +
        ", \npub_share_pre_fee='" + pub_share_pre_fee + '\'' +
        ", \nsubsidy_rate='" + subsidy_rate + '\'' +
        ", \nsubsidy_type='" + subsidy_type + '\'' +
        ", \nterminal_type='" + terminal_type + '\'' +
        ", \nauction_category='" + auction_category + '\'' +
        ", \nsite_id='" + site_id + '\'' +
        ", \nsite_name='" + site_name + '\'' +
        ", \nadzone_id='" + adzone_id + '\'' +
        ", \nadzone_name='" + adzone_name + '\'' +
        ", \nalipay_total_price='" + alipay_total_price + '\'' +
        ", \ntotal_commission_rate='" + total_commission_rate + '\'' +
        ", \ntotal_commission_fee='" + total_commission_fee + '\'' +
        ", \nsubsidy_fee='" + subsidy_fee + '\'' +
        ", \nrelation_id=" + relation_id +
        ", \nspecial_id=" + special_id +
        ", \nclick_time='" + click_time + '\'' +
        '}';
  }
}
