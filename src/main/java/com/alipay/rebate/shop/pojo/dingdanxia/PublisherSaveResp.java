//package com.alipay.rebate.shop.pojo.dingdanxia;
//
//public class PublisherSaveResp {
//
//    private Integer relation_id;
//    private String account_name;
//    private Integer special_id;
//    private String desc;
//
//    public Integer getRelation_id() {
//        return relation_id;
//    }
//
//    public void setRelation_id(Integer relation_id) {
//        this.relation_id = relation_id;
//    }
//
//    public String getAccount_name() {
//        return account_name;
//    }
//
//    public void setAccount_name(String account_name) {
//        this.account_name = account_name;
//    }
//
//    public Integer getSpecial_id() {
//        return special_id;
//    }
//
//    public void setSpecial_id(Integer special_id) {
//        this.special_id = special_id;
//    }
//
//    public String getDesc() {
//        return desc;
//    }
//
//    public void setDesc(String desc) {
//        this.desc = desc;
//    }
//
//    @Override
//    public String toString() {
//        return "PublisherSaveResp{" +
//                "relation_id='" + relation_id + '\'' +
//                ", account_name='" + account_name + '\'' +
//                ", special_id=" + special_id +
//                ", desc='" + desc + '\'' +
//                '}';
//    }
//}
