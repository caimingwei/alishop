package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;

public class MeiTuanRefund {
    private String orderid;
    private String quantity;
    private Long refundtime;
    private BigDecimal profit;
    private BigDecimal money;

    public java.lang.String getOrderid() {
        return orderid;
    }

    public void setOrderid(java.lang.String orderid) {
        this.orderid = orderid;
    }

    public java.lang.String getQuantity() {
        return quantity;
    }

    public void setQuantity(java.lang.String quantity) {
        this.quantity = quantity;
    }

    public void setRefundtime(Long refundtime) {
        this.refundtime = refundtime;
    }

    public Long getRefundtime() {
        return refundtime;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    @Override
    public java.lang.String toString() {
        return "MeiTuanRefund{" +
                "orderid='" + orderid + '\'' +
                ", quantity='" + quantity + '\'' +
                ", refundtime='" + refundtime + '\'' +
                ", String='" + money + '\'' +
                ", profit='" + profit + '\'' +
                '}';
    }
}
