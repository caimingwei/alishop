package com.alipay.rebate.shop.pojo.historyprice;

import java.math.BigDecimal;

public class GoodInfo {

    private String goodPicture;
    private String goodPrice;
    private String goodTitle;
    private String goodUrl;
    private String mallName;

    public String getGoodPicture() {
        return goodPicture;
    }

    public void setGoodPicture(String goodPicture) {
        this.goodPicture = goodPicture;
    }

    public String getGoodPrice() {
        return goodPrice;
    }

    public void setGoodPrice(String goodPrice) {
        this.goodPrice = goodPrice;
    }

    public String getGoodTitle() {
        return goodTitle;
    }

    public void setGoodTitle(String goodTitle) {
        this.goodTitle = goodTitle;
    }

    public String getGoodUrl() {
        return goodUrl;
    }

    public void setGoodUrl(String goodUrl) {
        this.goodUrl = goodUrl;
    }

    public String getMallName() {
        return mallName;
    }

    public void setMallName(String mallName) {
        this.mallName = mallName;
    }

    @Override
    public String toString() {
        return "GoodInfo{" +
                "goodPicture='" + goodPicture + '\'' +
                ", goodPrice=" + goodPrice +
                ", goodTitle='" + goodTitle + '\'' +
                ", goodUrl='" + goodUrl + '\'' +
                ", mallName='" + mallName + '\'' +
                '}';
    }
}
