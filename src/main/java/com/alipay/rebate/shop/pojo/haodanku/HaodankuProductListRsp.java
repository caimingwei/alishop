package com.alipay.rebate.shop.pojo.haodanku;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HaodankuProductListRsp extends HaodankuCommonMsg{

  List<HaodankuProduct> data;

  List<HaodankuProduct> item_info;

  public List<HaodankuProduct> getData() {
    return data;
  }

  public void setData(List<HaodankuProduct> data) {
    this.data = data;
  }

  public List<HaodankuProduct> getItem_info() {
    return item_info;
  }

  public void setItem_info(List<HaodankuProduct> item_info) {
    this.item_info = item_info;
  }

  @Override
  public String toString() {
    return "HaodankuProductListRsp{" +
        "data=" + data +
        ", item_info=" + item_info +
        "} " + super.toString();
  }
}
