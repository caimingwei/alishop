package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.List;

public class MeiTuanRsp {
    private List<MeiTuanCoupon> coupon;
    private List<MeiTuanRefund> refund;
    private MeiTuanOrderRsp order;

    public List<MeiTuanCoupon> getCoupon() {
        return coupon;
    }

    public void setCoupon(List<MeiTuanCoupon> coupon) {
        this.coupon = coupon;
    }

    public List<MeiTuanRefund> getRefund() {
        return refund;
    }

    public void setRefund(List<MeiTuanRefund> refund) {
        this.refund = refund;
    }

    public MeiTuanOrderRsp getOrder() {
        return order;
    }

    public void setOrder(MeiTuanOrderRsp order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "MeiTuanRsp{" +
                "coupon=" + coupon +
                ", refund=" + refund +
                ", order=" + order +
                '}';
    }
}
