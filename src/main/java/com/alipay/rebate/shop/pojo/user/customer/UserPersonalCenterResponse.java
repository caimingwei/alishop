package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.pojo.usergrade.UserGradeRsp;
import java.util.Date;

public class UserPersonalCenterResponse {

    private Long userId;
    private String totalIncome;
    private String todayIncome;
    private String freezeMoney;
    private String userNickName;
    private String headImageUrl;
    private String userAmount;
    private String userPhone;
    private String realName;
    private String alipayAccount;
    private Long relationId;
    private Long specialId;
    private String taobaoUserId;
    private Date userRegisterTime;
    private UserGradeRsp userGrade;
    private Long userIntgeral;
    private Integer userStatus;
    private Integer loginPlatform;
    private Integer privacyProtection;
    private String areaCode;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Integer getPrivacyProtection() {
        return privacyProtection;
    }

    public void setPrivacyProtection(Integer privacyProtection) {
        this.privacyProtection = privacyProtection;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(String totalIncome) {
        this.totalIncome = totalIncome;
    }

    public String getTodayIncome() {
        return todayIncome;
    }

    public void setTodayIncome(String todayIncome) {
        this.todayIncome = todayIncome;
    }

    public String getFreezeMoney() {
        return freezeMoney;
    }

    public void setFreezeMoney(String freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getHeadImageUrl() {
        return headImageUrl;
    }

    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl;
    }

    public String getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(String userAmount) {
        this.userAmount = userAmount;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getSpecialId() {
        return specialId;
    }

    public void setSpecialId(Long specialId) {
        this.specialId = specialId;
    }


    public String getTaobaoUserId() {
        return taobaoUserId;
    }

    public void setTaobaoUserId(String taobaoUserId) {
        this.taobaoUserId = taobaoUserId;
    }

    public UserGradeRsp getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(UserGradeRsp userGrade) {
        this.userGrade = userGrade;
    }

    public Date getUserRegisterTime() {
        return userRegisterTime;
    }

    public void setUserRegisterTime(Date userRegisterTime) {
        this.userRegisterTime = userRegisterTime;
    }

    public Long getUserIntgeral() {
        return userIntgeral;
    }

    public void setUserIntgeral(Long userIntgeral) {
        this.userIntgeral = userIntgeral;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getLoginPlatform() {
        return loginPlatform;
    }

    public void setLoginPlatform(Integer loginPlatform) {
        this.loginPlatform = loginPlatform;
    }

    @Override
    public String toString() {
        return "UserPersonalCenterResponse{" +
                "userId=" + userId +
                ", totalIncome='" + totalIncome + '\'' +
                ", todayIncome='" + todayIncome + '\'' +
                ", freezeMoney='" + freezeMoney + '\'' +
                ", userNickName='" + userNickName + '\'' +
                ", headImageUrl='" + headImageUrl + '\'' +
                ", userAmount='" + userAmount + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", realName='" + realName + '\'' +
                ", alipayAccount='" + alipayAccount + '\'' +
                ", relationId=" + relationId +
                ", specialId=" + specialId +
                ", taobaoUserId='" + taobaoUserId + '\'' +
                ", userRegisterTime=" + userRegisterTime +
                ", userGrade=" + userGrade +
                ", userIntgeral=" + userIntgeral +
                ", userStatus=" + userStatus +
                ", loginPlatform=" + loginPlatform +
                ", privacyProtection=" + privacyProtection +
                '}';
    }
}
