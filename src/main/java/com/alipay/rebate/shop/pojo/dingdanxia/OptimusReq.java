package com.alipay.rebate.shop.pojo.dingdanxia;

public class OptimusReq {

  private String apikey;
  private String signature;
  private Number material_id;
  private Number page_size;
  private Number page_no;
  private String device_type;
  private String device_encrypt;
  private String device_value;
  private Number content_id;
  private String content_source;
  private Number item_id;

  public String getApikey() {
    return apikey;
  }

  public void setApikey(String apikey) {
    this.apikey = apikey;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public Number getMaterial_id() {
    return material_id;
  }

  public void setMaterial_id(Number material_id) {
    this.material_id = material_id;
  }

  public Number getPage_size() {
    return page_size;
  }

  public void setPage_size(Number page_size) {
    this.page_size = page_size;
  }

  public Number getPage_no() {
    return page_no;
  }

  public void setPage_no(Number page_no) {
    this.page_no = page_no;
  }

  public String getDevice_type() {
    return device_type;
  }

  public void setDevice_type(String device_type) {
    this.device_type = device_type;
  }

  public String getDevice_encrypt() {
    return device_encrypt;
  }

  public void setDevice_encrypt(String device_encrypt) {
    this.device_encrypt = device_encrypt;
  }

  public String getDevice_value() {
    return device_value;
  }

  public void setDevice_value(String device_value) {
    this.device_value = device_value;
  }

  public Number getContent_id() {
    return content_id;
  }

  public void setContent_id(Number content_id) {
    this.content_id = content_id;
  }

  public String getContent_source() {
    return content_source;
  }

  public void setContent_source(String content_source) {
    this.content_source = content_source;
  }

  public Number getItem_id() {
    return item_id;
  }

  public void setItem_id(Number item_id) {
    this.item_id = item_id;
  }

  @Override
  public String toString() {
    return "OptimusReq{" +
        "apikey='" + apikey + '\'' +
        ", signature='" + signature + '\'' +
        ", material_id=" + material_id +
        ", page_size=" + page_size +
        ", page_no=" + page_no +
        ", device_type='" + device_type + '\'' +
        ", device_encrypt='" + device_encrypt + '\'' +
        ", device_value='" + device_value + '\'' +
        ", content_id=" + content_id +
        ", content_source='" + content_source + '\'' +
        ", item_id=" + item_id +
        '}';
  }
}
