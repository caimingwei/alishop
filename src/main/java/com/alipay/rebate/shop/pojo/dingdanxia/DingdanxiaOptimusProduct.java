package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;
import java.util.Arrays;

public class DingdanxiaOptimusProduct {

  private Integer coupon_amount;
  private Object small_images;
  private String shop_title;
  private Long category_id;
  private String coupon_start_fee;
  private Long item_id;
  private String jhs_price_usp_list;
  private Integer coupon_total_count;
  private Integer user_type;
  private BigDecimal zk_final_price;
  private Integer coupon_remain_count;
  private BigDecimal commission_rate;
  private String coupon_start_time;
  private String title;
  private String item_description;
  private Long seller_id;
  private Integer volume;
  private String coupon_end_time;
  private String coupon_click_url;
  private String pict_url;
  private String click_url;
  private Integer stock;
  private Integer sell_num;
  private Integer total_stock;
  private String oetime;
  private String ostime;
  private Integer jdd_num;
  private String jdd_price;
  private String orig_price;
  private String level_one_category_name;
  private Long level_one_category_id;
  private String category_name;
  private String short_title;
  private String white_image;
  private Object word_list;
  private String word;
  private String url;
  private String tmall_play_activity_info;
  private Integer uv_sum_pre_sale;
  private String new_user_price;
  private String coupon_info;
  private String coupon_share_url;
  private String nick;
  private String sub_title;
  private String kuadian_promotion_info;

  public String getKuadian_promotion_info() {
    return kuadian_promotion_info;
  }

  public void setKuadian_promotion_info(String kuadian_promotion_info) {
    this.kuadian_promotion_info = kuadian_promotion_info;
  }

  public String getSub_title() {
    return sub_title;
  }

  public void setSub_title(String sub_title) {
    this.sub_title = sub_title;
  }

  public String getJhs_price_usp_list() {
    return jhs_price_usp_list;
  }

  public void setJhs_price_usp_list(String jhs_price_usp_list) {
    this.jhs_price_usp_list = jhs_price_usp_list;
  }

  public Integer getCoupon_amount() {
    return coupon_amount;
  }

  public void setCoupon_amount(Integer coupon_amount) {
    this.coupon_amount = coupon_amount;
  }

  public Object getSmall_images() {
    return small_images;
  }

  public void setSmall_images(Object small_images) {
    this.small_images = small_images;
  }

  public String getShop_title() {
    return shop_title;
  }

  public void setShop_title(String shop_title) {
    this.shop_title = shop_title;
  }

  public Long getCategory_id() {
    return category_id;
  }

  public void setCategory_id(Long category_id) {
    this.category_id = category_id;
  }

  public String getCoupon_start_fee() {
    return coupon_start_fee;
  }

  public void setCoupon_start_fee(String coupon_start_fee) {
    this.coupon_start_fee = coupon_start_fee;
  }

  public Long getItem_id() {
    return item_id;
  }

  public void setItem_id(Long item_id) {
    this.item_id = item_id;
  }

  public Integer getCoupon_total_count() {
    return coupon_total_count;
  }

  public void setCoupon_total_count(Integer coupon_total_count) {
    this.coupon_total_count = coupon_total_count;
  }

  public Integer getUser_type() {
    return user_type;
  }

  public void setUser_type(Integer user_type) {
    this.user_type = user_type;
  }

  public BigDecimal getZk_final_price() {
    return zk_final_price;
  }

  public void setZk_final_price(BigDecimal zk_final_price) {
    this.zk_final_price = zk_final_price;
  }

  public Integer getCoupon_remain_count() {
    return coupon_remain_count;
  }

  public void setCoupon_remain_count(Integer coupon_remain_count) {
    this.coupon_remain_count = coupon_remain_count;
  }

  public BigDecimal getCommission_rate() {
    return commission_rate;
  }

  public void setCommission_rate(BigDecimal commission_rate) {
    this.commission_rate = commission_rate;
  }

  public String getCoupon_start_time() {
    return coupon_start_time;
  }

  public void setCoupon_start_time(String coupon_start_time) {
    this.coupon_start_time = coupon_start_time;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getItem_description() {
    return item_description;
  }

  public void setItem_description(String item_description) {
    this.item_description = item_description;
  }

  public Long getSeller_id() {
    return seller_id;
  }

  public void setSeller_id(Long seller_id) {
    this.seller_id = seller_id;
  }

  public Integer getVolume() {
    return volume;
  }

  public void setVolume(Integer volume) {
    this.volume = volume;
  }

  public String getCoupon_end_time() {
    return coupon_end_time;
  }

  public void setCoupon_end_time(String coupon_end_time) {
    this.coupon_end_time = coupon_end_time;
  }

  public String getCoupon_click_url() {
    return coupon_click_url;
  }

  public void setCoupon_click_url(String coupon_click_url) {
    this.coupon_click_url = coupon_click_url;
  }

  public String getPict_url() {
    return pict_url;
  }

  public void setPict_url(String pict_url) {
    this.pict_url = pict_url;
  }

  public String getClick_url() {
    return click_url;
  }

  public void setClick_url(String click_url) {
    this.click_url = click_url;
  }

  public Integer getStock() {
    return stock;
  }

  public void setStock(Integer stock) {
    this.stock = stock;
  }

  public Integer getSell_num() {
    return sell_num;
  }

  public void setSell_num(Integer sell_num) {
    this.sell_num = sell_num;
  }

  public Integer getTotal_stock() {
    return total_stock;
  }

  public void setTotal_stock(Integer total_stock) {
    this.total_stock = total_stock;
  }

  public String getOetime() {
    return oetime;
  }

  public void setOetime(String oetime) {
    this.oetime = oetime;
  }

  public String getOstime() {
    return ostime;
  }

  public void setOstime(String ostime) {
    this.ostime = ostime;
  }

  public Integer getJdd_num() {
    return jdd_num;
  }

  public void setJdd_num(Integer jdd_num) {
    this.jdd_num = jdd_num;
  }

  public String getJdd_price() {
    return jdd_price;
  }

  public void setJdd_price(String jdd_price) {
    this.jdd_price = jdd_price;
  }

  public String getOrig_price() {
    return orig_price;
  }

  public void setOrig_price(String orig_price) {
    this.orig_price = orig_price;
  }

  public String getLevel_one_category_name() {
    return level_one_category_name;
  }

  public void setLevel_one_category_name(String level_one_category_name) {
    this.level_one_category_name = level_one_category_name;
  }

  public Long getLevel_one_category_id() {
    return level_one_category_id;
  }

  public void setLevel_one_category_id(Long level_one_category_id) {
    this.level_one_category_id = level_one_category_id;
  }

  public String getCategory_name() {
    return category_name;
  }

  public void setCategory_name(String category_name) {
    this.category_name = category_name;
  }

  public String getShort_title() {
    return short_title;
  }

  public void setShort_title(String short_title) {
    this.short_title = short_title;
  }

  public String getWhite_image() {
    return white_image;
  }

  public void setWhite_image(String white_image) {
    this.white_image = white_image;
  }

  public Object getWord_list() {
    return word_list;
  }

  public void setWord_list(Object word_list) {
    this.word_list = word_list;
  }

  public String getWord() {
    return word;
  }

  public void setWord(String word) {
    this.word = word;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTmall_play_activity_info() {
    return tmall_play_activity_info;
  }

  public void setTmall_play_activity_info(String tmall_play_activity_info) {
    this.tmall_play_activity_info = tmall_play_activity_info;
  }

  public Integer getUv_sum_pre_sale() {
    return uv_sum_pre_sale;
  }

  public void setUv_sum_pre_sale(Integer uv_sum_pre_sale) {
    this.uv_sum_pre_sale = uv_sum_pre_sale;
  }

  public String getNew_user_price() {
    return new_user_price;
  }

  public void setNew_user_price(String new_user_price) {
    this.new_user_price = new_user_price;
  }

  public String getCoupon_info() {
    return coupon_info;
  }

  public void setCoupon_info(String coupon_info) {
    this.coupon_info = coupon_info;
  }

  public String getCoupon_share_url() {
    return coupon_share_url;
  }

  public void setCoupon_share_url(String coupon_share_url) {
    this.coupon_share_url = coupon_share_url;
  }

  public String getNick() {
    return nick;
  }

  public void setNick(String nick) {
    this.nick = nick;
  }

  @Override
  public String toString() {
    return "DingdanxiaOptimusProduct{" +
            "coupon_amount=" + coupon_amount +
            ", small_images=" + small_images +
            ", shop_title='" + shop_title + '\'' +
            ", category_id=" + category_id +
            ", coupon_start_fee='" + coupon_start_fee + '\'' +
            ", item_id=" + item_id +
            ", jhs_price_usp_list='" + jhs_price_usp_list + '\'' +
            ", coupon_total_count=" + coupon_total_count +
            ", user_type=" + user_type +
            ", zk_final_price=" + zk_final_price +
            ", coupon_remain_count=" + coupon_remain_count +
            ", commission_rate=" + commission_rate +
            ", coupon_start_time='" + coupon_start_time + '\'' +
            ", title='" + title + '\'' +
            ", item_description='" + item_description + '\'' +
            ", seller_id=" + seller_id +
            ", volume=" + volume +
            ", coupon_end_time='" + coupon_end_time + '\'' +
            ", coupon_click_url='" + coupon_click_url + '\'' +
            ", pict_url='" + pict_url + '\'' +
            ", click_url='" + click_url + '\'' +
            ", stock=" + stock +
            ", sell_num=" + sell_num +
            ", total_stock=" + total_stock +
            ", oetime='" + oetime + '\'' +
            ", ostime='" + ostime + '\'' +
            ", jdd_num=" + jdd_num +
            ", jdd_price='" + jdd_price + '\'' +
            ", orig_price='" + orig_price + '\'' +
            ", level_one_category_name='" + level_one_category_name + '\'' +
            ", level_one_category_id=" + level_one_category_id +
            ", category_name='" + category_name + '\'' +
            ", short_title='" + short_title + '\'' +
            ", white_image='" + white_image + '\'' +
            ", word_list=" + word_list +
            ", word='" + word + '\'' +
            ", url='" + url + '\'' +
            ", tmall_play_activity_info='" + tmall_play_activity_info + '\'' +
            ", uv_sum_pre_sale=" + uv_sum_pre_sale +
            ", new_user_price='" + new_user_price + '\'' +
            ", coupon_info='" + coupon_info + '\'' +
            ", coupon_share_url='" + coupon_share_url + '\'' +
            ", nick='" + nick + '\'' +
            ", sub_title='" + sub_title + '\'' +
            ", kuadian_promotion_info='" + kuadian_promotion_info + '\'' +
            '}';
  }
}
