package com.alipay.rebate.shop.pojo.user.admin;

import com.alipay.rebate.shop.constants.PatternContant;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ProductPageFrameworkRequest {

  private Integer productPageId;

  @NotNull(message = "板块类别不能为空")
  private Integer pageCatagory;

  @NotBlank(message = "板块名不能为空")
  private String pageName;

  @NotNull(message = "是否列表展示不能为空")
  private Integer classifyShow;

  @NotNull(message = "列表展示类型不能为空")
  private Integer showType;

  @NotNull(message = "是否显示返利不能为空")
  private Integer showRebate;

  private String bannerPicPath;

  @NotNull(message = "是否使用不能为空")
  private Integer isUse;

  private String dataSource;

  public Integer getProductPageId() {
    return productPageId;
  }

  public void setProductPageId(Integer productPageId) {
    this.productPageId = productPageId;
  }

  public Integer getPageCatagory() {
    return pageCatagory;
  }

  public void setPageCatagory(Integer pageCatagory) {
    this.pageCatagory = pageCatagory;
  }

  public String getPageName() {
    return pageName;
  }

  public void setPageName(String pageName) {
    this.pageName = pageName;
  }

  public Integer getClassifyShow() {
    return classifyShow;
  }

  public void setClassifyShow(Integer classifyShow) {
    this.classifyShow = classifyShow;
  }

  public Integer getShowType() {
    return showType;
  }

  public void setShowType(Integer showType) {
    this.showType = showType;
  }

  public Integer getShowRebate() {
    return showRebate;
  }

  public void setShowRebate(Integer showRebate) {
    this.showRebate = showRebate;
  }

  public String getBannerPicPath() {
    return bannerPicPath;
  }

  public void setBannerPicPath(String bannerPicPath) {
    this.bannerPicPath = bannerPicPath;
  }

  public Integer getIsUse() {
    return isUse;
  }

  public void setIsUse(Integer isUse) {
    this.isUse = isUse;
  }

  public String getDataSource() {
    return dataSource;
  }

  public void setDataSource(String dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public String toString() {
    return "ProductPageFrameworkRequest{" +
        "productPageId=" + productPageId +
        ", pageCatagory=" + pageCatagory +
        ", pageName='" + pageName + '\'' +
        ", classifyShow=" + classifyShow +
        ", showType=" + showType +
        ", showRebate=" + showRebate +
        ", bannerPicPath='" + bannerPicPath + '\'' +
        ", isUse=" + isUse +
        ", dataSource='" + dataSource + '\'' +
        '}';
  }
}
