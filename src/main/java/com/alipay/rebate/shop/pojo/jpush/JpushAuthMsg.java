package com.alipay.rebate.shop.pojo.jpush;

public class JpushAuthMsg {

  private String key;
  private String secret;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  @Override
  public String toString() {
    return "JpushAuthMsg{" +
        "key='" + key + '\'' +
        ", secret='" + secret + '\'' +
        '}';
  }
}
