package com.alipay.rebate.shop.pojo.mobilecode;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;

import java.util.Date;

public class CodePageInformation extends CommonPageRequest {

    private Long id;
    private String phone;
    private String recipientName;
    private Date sendTime;
    private Long status;
    private String type;
    private String sendContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSendContent() {
        return sendContent;
    }

    public void setSendContent(String sendContent) {
        this.sendContent = sendContent;
    }

    @Override
    public String toString() {
        return "CodePageInformation{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", recipientName='" + recipientName + '\'' +
                ", sendTime=" + sendTime +
                ", status=" + status +
                ", type='" + type + '\'' +
                ", sendContent='" + sendContent + '\'' +
                ", pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                '}';
    }
}
