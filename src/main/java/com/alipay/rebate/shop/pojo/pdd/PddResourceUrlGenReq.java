package com.alipay.rebate.shop.pojo.pdd;

public class PddResourceUrlGenReq {

    private String custom_parameters;
    private Boolean generate_qq_app;
    private Boolean generate_schema_url;
    private Boolean generate_we_app;
    private String pid;
    private Integer resource_type;
    private String url;

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public Boolean getGenerate_qq_app() {
        return generate_qq_app;
    }

    public void setGenerate_qq_app(Boolean generate_qq_app) {
        this.generate_qq_app = generate_qq_app;
    }

    public Boolean getGenerate_schema_url() {
        return generate_schema_url;
    }

    public void setGenerate_schema_url(Boolean generate_schema_url) {
        this.generate_schema_url = generate_schema_url;
    }

    public Boolean getGenerate_we_app() {
        return generate_we_app;
    }

    public void setGenerate_we_app(Boolean generate_we_app) {
        this.generate_we_app = generate_we_app;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getResource_type() {
        return resource_type;
    }

    public void setResource_type(Integer resource_type) {
        this.resource_type = resource_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
