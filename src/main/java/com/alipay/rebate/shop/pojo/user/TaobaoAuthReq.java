package com.alipay.rebate.shop.pojo.user;

public class TaobaoAuthReq {

  private String userId;
  private String userName;
  private String accessToken;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  @Override
  public String toString() {
    return "TaobaoAuthReq{" +
        "userId='" + userId + '\'' +
        ", userName='" + userName + '\'' +
        ", accessToken='" + accessToken + '\'' +
        '}';
  }
}
