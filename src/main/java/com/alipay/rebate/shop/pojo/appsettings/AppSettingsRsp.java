package com.alipay.rebate.shop.pojo.appsettings;

import javax.persistence.Column;
import java.util.Date;

public class AppSettingsRsp {

    private Integer id;

    private Integer isShowRebate;

    private Date updateTime;

    private String pid;

    private Integer iosAudit;

    private String appVersion;

    private String activityPid;

    private Integer spDomainType;

    private String spDomain;

    private String csWetchat;

    private String haodankuKey;

    private Integer jdRebate;

    private Integer pddRebate;

    private Integer promoteEarn;

    private String privacyPolicy;
    private String useAgreement;
    private String taobaoKey;
    private Integer androidAudit;
    private String androidAppVersion;

    public Integer getAndroidAudit() {
        return androidAudit;
    }

    public void setAndroidAudit(Integer androidAudit) {
        this.androidAudit = androidAudit;
    }

    public String getAndroidAppVersion() {
        return androidAppVersion;
    }

    public void setAndroidAppVersion(String androidAppVersion) {
        this.androidAppVersion = androidAppVersion;
    }

    public String getTaobaoKey() {
        return taobaoKey;
    }

    public void setTaobaoKey(String taobaoKey) {
        this.taobaoKey = taobaoKey;
    }

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public String getUseAgreement() {
        return useAgreement;
    }

    public void setUseAgreement(String useAgreement) {
        this.useAgreement = useAgreement;
    }

    public Integer getPromoteEarn() {
        return promoteEarn;
    }

    public void setPromoteEarn(Integer promoteEarn) {
        this.promoteEarn = promoteEarn;
    }

    public Integer getPddRebate() {
        return pddRebate;
    }

    public void setPddRebate(Integer pddRebate) {
        this.pddRebate = pddRebate;
    }

    public Integer getJdRebate() {
        return jdRebate;
    }

    public void setJdRebate(Integer jdRebate) {
        this.jdRebate = jdRebate;
    }

    public String getHaodankuKey() {
        return haodankuKey;
    }

    public void setHaodankuKey(String haodankuKey) {
        this.haodankuKey = haodankuKey;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsShowRebate() {
        return isShowRebate;
    }

    public void setIsShowRebate(Integer isShowRebate) {
        this.isShowRebate = isShowRebate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getIosAudit() {
        return iosAudit;
    }

    public void setIosAudit(Integer iosAudit) {
        this.iosAudit = iosAudit;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }

    public Integer getSpDomainType() {
        return spDomainType;
    }

    public void setSpDomainType(Integer spDomainType) {
        this.spDomainType = spDomainType;
    }

    public String getSpDomain() {
        return spDomain;
    }

    public void setSpDomain(String spDomain) {
        this.spDomain = spDomain;
    }

    public String getCsWetchat() {
        return csWetchat;
    }

    public void setCsWetchat(String csWetchat) {
        this.csWetchat = csWetchat;
    }

    @Override
    public String toString() {
        return "AppSettingsRsp{" +
                "id=" + id +
                ", isShowRebate=" + isShowRebate +
                ", updateTime=" + updateTime +
                ", pid='" + pid + '\'' +
                ", iosAudit=" + iosAudit +
                ", appVersion='" + appVersion + '\'' +
                ", activityPid='" + activityPid + '\'' +
                ", spDomainType=" + spDomainType +
                ", spDomain='" + spDomain + '\'' +
                ", csWetchat='" + csWetchat + '\'' +
                ", haodankuKey='" + haodankuKey + '\'' +
                ", jdRebate=" + jdRebate +
                ", pddRebate=" + pddRebate +
                ", promoteEarn=" + promoteEarn +
                ", privacyPolicy='" + privacyPolicy + '\'' +
                ", useAgreement='" + useAgreement + '\'' +
                ", taobaoKey='" + taobaoKey + '\'' +
                '}';
    }
}
