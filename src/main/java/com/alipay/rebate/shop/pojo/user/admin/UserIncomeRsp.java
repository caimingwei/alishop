package com.alipay.rebate.shop.pojo.user.admin;

import java.math.BigDecimal;

public class UserIncomeRsp {

  private Long userIncomeId;
  private Long userId;
  private BigDecimal money;
  private Long intgeralNum;
  private Long relationId;
  private Long specialId;
  private String tradeId;
  private String parentTradeId;
  private Integer type;
  private Long tkStatus;
  private Integer status;
  private Integer awardType;
  private String createTime;
  private String updateTime;
  private String userNickName;
  private Integer roadType;
  private String tkPaidTime;
  private String earningTime;

  public String getTkPaidTime() {
    return tkPaidTime;
  }

  public void setTkPaidTime(String tkPaidTime) {
    this.tkPaidTime = tkPaidTime;
  }

  public String getEarningTime() {
    return earningTime;
  }

  public void setEarningTime(String earningTime) {
    this.earningTime = earningTime;
  }

  public Integer getRoadType() {
    return roadType;
  }
  public void setRoadType(Integer roadType) {
    this.roadType = roadType;
  }

  public Long getUserIncomeId() {
    return userIncomeId;
  }

  public void setUserIncomeId(Long userIncomeId) {
    this.userIncomeId = userIncomeId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public BigDecimal getMoney() {
    return money;
  }

  public void setMoney(BigDecimal money) {
    this.money = money;
  }

  public Long getIntgeralNum() {
    return intgeralNum;
  }

  public void setIntgeralNum(Long intgeralNum) {
    this.intgeralNum = intgeralNum;
  }

  public Long getRelationId() {
    return relationId;
  }

  public void setRelationId(Long relationId) {
    this.relationId = relationId;
  }

  public Long getSpecialId() {
    return specialId;
  }

  public void setSpecialId(Long specialId) {
    this.specialId = specialId;
  }

  public String getTradeId() {
    return tradeId;
  }

  public void setTradeId(String tradeId) {
    this.tradeId = tradeId;
  }

  public String getParentTradeId() {
    return parentTradeId;
  }

  public void setParentTradeId(String parentTradeId) {
    this.parentTradeId = parentTradeId;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Long getTkStatus() {
    return tkStatus;
  }

  public void setTkStatus(Long tkStatus) {
    this.tkStatus = tkStatus;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Integer getAwardType() {
    return awardType;
  }

  public void setAwardType(Integer awardType) {
    this.awardType = awardType;
  }

  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }

  public String getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(String updateTime) {
    this.updateTime = updateTime;
  }

  public String getUserNickName() {
    return userNickName;
  }

  public void setUserNickName(String userNickName) {
    this.userNickName = userNickName;
  }

  @Override
  public String toString() {
    return "UserIncomeRsp{" +
            "userIncomeId=" + userIncomeId +
            ", userId=" + userId +
            ", money=" + money +
            ", intgeralNum=" + intgeralNum +
            ", relationId=" + relationId +
            ", specialId=" + specialId +
            ", tradeId='" + tradeId + '\'' +
            ", parentTradeId='" + parentTradeId + '\'' +
            ", type=" + type +
            ", tkStatus=" + tkStatus +
            ", status=" + status +
            ", awardType=" + awardType +
            ", createTime='" + createTime + '\'' +
            ", updateTime='" + updateTime + '\'' +
            ", userNickName='" + userNickName + '\'' +
            ", roadType=" + roadType +
            ", tkPaidTime='" + tkPaidTime + '\'' +
            ", earningTime='" + earningTime + '\'' +
            '}';
  }
}
