package com.alipay.rebate.shop.pojo.taobao;

public class ItemInfoRequest {

    private String num_iids;
    private Long platform;
    private String ip;

    public String getNum_iids() {
        return num_iids;
    }

    public void setNum_iids(String num_iids) {
        this.num_iids = num_iids;
    }

    public Long getPlatform() {
        return platform;
    }

    public void setPlatform(Long platform) {
        this.platform = platform;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "ItemInfoRequest{" +
                "num_iids='" + num_iids + '\'' +
                ", platform=" + platform +
                ", ip='" + ip + '\'' +
                '}';
    }
}
