package com.alipay.rebate.shop.pojo.order;

import javax.validation.constraints.NotNull;

public class FindOrdersReq {

    @NotNull(message = "订单号不能为空")
    private String id;
    @NotNull(message = "用户id不能为空")
    private Long userId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "FindOrdersReq{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                '}';
    }
}
