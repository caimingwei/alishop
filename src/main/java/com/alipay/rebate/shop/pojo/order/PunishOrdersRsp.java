package com.alipay.rebate.shop.pojo.order;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.math.BigDecimal;

public class PunishOrdersRsp {

    private Long id;
    private String workOrderId;
    private String tradeId;
    private String type;
    private BigDecimal punishMoney;
    private Long userId;
    private Long promoterId;
    private String tkPaidTime;
    private String earningTime;

    public String getTkPaidTime() {
        return tkPaidTime;
    }

    public void setTkPaidTime(String tkPaidTime) {
        this.tkPaidTime = tkPaidTime;
    }

    public String getEarningTime() {
        return earningTime;
    }

    public void setEarningTime(String earningTime) {
        this.earningTime = earningTime;
    }

    public Long getPromoterId() {
        return promoterId;
    }

    public void setPromoterId(Long promoterId) {
        this.promoterId = promoterId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPunishMoney() {
        return punishMoney;
    }

    public void setPunishMoney(BigDecimal punishMoney) {
        this.punishMoney = punishMoney;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PunishOrdersRsp{" +
                "id=" + id +
                ", workOrderId='" + workOrderId + '\'' +
                ", tradeId='" + tradeId + '\'' +
                ", type='" + type + '\'' +
                ", punishMoney=" + punishMoney +
                ", userId=" + userId +
                ", promoterId=" + promoterId +
                ", tkPaidTime='" + tkPaidTime + '\'' +
                ", earningTime='" + earningTime + '\'' +
                '}';
    }
}
