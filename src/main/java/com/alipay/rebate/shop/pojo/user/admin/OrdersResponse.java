package com.alipay.rebate.shop.pojo.user.admin;

import java.math.BigDecimal;
import java.util.Date;

public class OrdersResponse {

  private String tradeId;

  private String tradeParentId;

  private String numIid;

  private String itemTitle;

  private Long itemNum;

  private BigDecimal price;

  private BigDecimal payPrice;

  private String sellerNick;

  private String sellerShopTitle;

  private BigDecimal commission;

  private BigDecimal commissionRate;

  private String unid;

  private Date createTime;

  private String earningTime;

  private Long tkStatus;

  private String tk3rdType;

  private Long tk3rdPubId;

  private String orderType;

  private BigDecimal incomeRate;

  private BigDecimal pubSharePreFee;

  private BigDecimal subsidyRate;

  private String subsidyType;

  private String terminalType;

  private String auctionCategory;

  private String siteId;

  private String siteName;

  private String adzoneId;

  private String adzoneName;

  private BigDecimal alipayTotalPrice;

  private BigDecimal totalCommissionRate;

  private BigDecimal totalCommissionFee;

  private BigDecimal subsidyFee;

  private Long relationId;

  private Long specialId;

  private Date clickTime;

  private BigDecimal oneLevelCommission;

  private BigDecimal twoLevelCommission;

  private BigDecimal threeLevelCommission;

  private Long userId;

  private String userName;

  private Long promoterId;

  private String promoterName;

  private String itemImg;

  private BigDecimal userCommission;

  private Long userUit;

  private Long oneLevelUserUit;

  private Long secondLevelUserUit;

  public String getTradeId() {
    return tradeId;
  }

  public void setTradeId(String tradeId) {
    this.tradeId = tradeId;
  }

  public String getTradeParentId() {
    return tradeParentId;
  }

  public void setTradeParentId(String tradeParentId) {
    this.tradeParentId = tradeParentId;
  }

  public String getNumIid() {
    return numIid;
  }

  public void setNumIid(String numIid) {
    this.numIid = numIid;
  }

  public String getItemTitle() {
    return itemTitle;
  }

  public void setItemTitle(String itemTitle) {
    this.itemTitle = itemTitle;
  }

  public Long getItemNum() {
    return itemNum;
  }

  public void setItemNum(Long itemNum) {
    this.itemNum = itemNum;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getPayPrice() {
    return payPrice;
  }

  public void setPayPrice(BigDecimal payPrice) {
    this.payPrice = payPrice;
  }

  public String getSellerNick() {
    return sellerNick;
  }

  public void setSellerNick(String sellerNick) {
    this.sellerNick = sellerNick;
  }

  public String getSellerShopTitle() {
    return sellerShopTitle;
  }

  public void setSellerShopTitle(String sellerShopTitle) {
    this.sellerShopTitle = sellerShopTitle;
  }

  public BigDecimal getCommission() {
    return commission;
  }

  public void setCommission(BigDecimal commission) {
    this.commission = commission;
  }

  public BigDecimal getCommissionRate() {
    return commissionRate;
  }

  public void setCommissionRate(BigDecimal commissionRate) {
    this.commissionRate = commissionRate;
  }

  public String getUnid() {
    return unid;
  }

  public void setUnid(String unid) {
    this.unid = unid;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getEarningTime() {
    return earningTime;
  }

  public void setEarningTime(String earningTime) {
    this.earningTime = earningTime;
  }

  public Long getTkStatus() {
    return tkStatus;
  }

  public void setTkStatus(Long tkStatus) {
    this.tkStatus = tkStatus;
  }

  public String getTk3rdType() {
    return tk3rdType;
  }

  public void setTk3rdType(String tk3rdType) {
    this.tk3rdType = tk3rdType;
  }

  public Long getTk3rdPubId() {
    return tk3rdPubId;
  }

  public void setTk3rdPubId(Long tk3rdPubId) {
    this.tk3rdPubId = tk3rdPubId;
  }

  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }

  public BigDecimal getIncomeRate() {
    return incomeRate;
  }

  public void setIncomeRate(BigDecimal incomeRate) {
    this.incomeRate = incomeRate;
  }

  public BigDecimal getPubSharePreFee() {
    return pubSharePreFee;
  }

  public void setPubSharePreFee(BigDecimal pubSharePreFee) {
    this.pubSharePreFee = pubSharePreFee;
  }

  public BigDecimal getSubsidyRate() {
    return subsidyRate;
  }

  public void setSubsidyRate(BigDecimal subsidyRate) {
    this.subsidyRate = subsidyRate;
  }

  public String getSubsidyType() {
    return subsidyType;
  }

  public void setSubsidyType(String subsidyType) {
    this.subsidyType = subsidyType;
  }

  public String getTerminalType() {
    return terminalType;
  }

  public void setTerminalType(String terminalType) {
    this.terminalType = terminalType;
  }

  public String getAuctionCategory() {
    return auctionCategory;
  }

  public void setAuctionCategory(String auctionCategory) {
    this.auctionCategory = auctionCategory;
  }

  public String getSiteId() {
    return siteId;
  }

  public void setSiteId(String siteId) {
    this.siteId = siteId;
  }

  public String getSiteName() {
    return siteName;
  }

  public void setSiteName(String siteName) {
    this.siteName = siteName;
  }

  public String getAdzoneId() {
    return adzoneId;
  }

  public void setAdzoneId(String adzoneId) {
    this.adzoneId = adzoneId;
  }

  public String getAdzoneName() {
    return adzoneName;
  }

  public void setAdzoneName(String adzoneName) {
    this.adzoneName = adzoneName;
  }

  public BigDecimal getAlipayTotalPrice() {
    return alipayTotalPrice;
  }

  public void setAlipayTotalPrice(BigDecimal alipayTotalPrice) {
    this.alipayTotalPrice = alipayTotalPrice;
  }

  public BigDecimal getTotalCommissionRate() {
    return totalCommissionRate;
  }

  public void setTotalCommissionRate(BigDecimal totalCommissionRate) {
    this.totalCommissionRate = totalCommissionRate;
  }

  public BigDecimal getTotalCommissionFee() {
    return totalCommissionFee;
  }

  public void setTotalCommissionFee(BigDecimal totalCommissionFee) {
    this.totalCommissionFee = totalCommissionFee;
  }

  public BigDecimal getSubsidyFee() {
    return subsidyFee;
  }

  public void setSubsidyFee(BigDecimal subsidyFee) {
    this.subsidyFee = subsidyFee;
  }

  public Long getRelationId() {
    return relationId;
  }

  public void setRelationId(Long relationId) {
    this.relationId = relationId;
  }

  public Long getSpecialId() {
    return specialId;
  }

  public void setSpecialId(Long specialId) {
    this.specialId = specialId;
  }

  public Date getClickTime() {
    return clickTime;
  }

  public void setClickTime(Date clickTime) {
    this.clickTime = clickTime;
  }

  public BigDecimal getOneLevelCommission() {
    return oneLevelCommission;
  }

  public void setOneLevelCommission(BigDecimal oneLevelCommission) {
    this.oneLevelCommission = oneLevelCommission;
  }

  public BigDecimal getTwoLevelCommission() {
    return twoLevelCommission;
  }

  public void setTwoLevelCommission(BigDecimal twoLevelCommission) {
    this.twoLevelCommission = twoLevelCommission;
  }

  public BigDecimal getThreeLevelCommission() {
    return threeLevelCommission;
  }

  public void setThreeLevelCommission(BigDecimal threeLevelCommission) {
    this.threeLevelCommission = threeLevelCommission;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Long getPromoterId() {
    return promoterId;
  }

  public void setPromoterId(Long promoterId) {
    this.promoterId = promoterId;
  }

  public String getPromoterName() {
    return promoterName;
  }

  public void setPromoterName(String promoterName) {
    this.promoterName = promoterName;
  }

  public String getItemImg() {
    return itemImg;
  }

  public void setItemImg(String itemImg) {
    this.itemImg = itemImg;
  }

  public BigDecimal getUserCommission() {
    return userCommission;
  }

  public void setUserCommission(BigDecimal userCommission) {
    this.userCommission = userCommission;
  }

  public Long getUserUit() {
    return userUit;
  }

  public void setUserUit(Long userUit) {
    this.userUit = userUit;
  }

  public Long getOneLevelUserUit() {
    return oneLevelUserUit;
  }

  public void setOneLevelUserUit(Long oneLevelUserUit) {
    this.oneLevelUserUit = oneLevelUserUit;
  }

  public Long getSecondLevelUserUit() {
    return secondLevelUserUit;
  }

  public void setSecondLevelUserUit(Long secondLevelUserUit) {
    this.secondLevelUserUit = secondLevelUserUit;
  }

  @Override
  public String toString() {
    return "OrdersResponse{" +
        "tradeId='" + tradeId + '\'' +
        ", tradeParentId='" + tradeParentId + '\'' +
        ", numIid=" + numIid +
        ", itemTitle='" + itemTitle + '\'' +
        ", itemNum=" + itemNum +
        ", price=" + price +
        ", payPrice=" + payPrice +
        ", sellerNick='" + sellerNick + '\'' +
        ", sellerShopTitle='" + sellerShopTitle + '\'' +
        ", commission=" + commission +
        ", commissionRate=" + commissionRate +
        ", unid='" + unid + '\'' +
        ", createTime=" + createTime +
        ", earningTime='" + earningTime + '\'' +
        ", tkStatus=" + tkStatus +
        ", tk3rdType='" + tk3rdType + '\'' +
        ", tk3rdPubId=" + tk3rdPubId +
        ", orderType='" + orderType + '\'' +
        ", incomeRate=" + incomeRate +
        ", pubSharePreFee=" + pubSharePreFee +
        ", subsidyRate=" + subsidyRate +
        ", subsidyType='" + subsidyType + '\'' +
        ", terminalType='" + terminalType + '\'' +
        ", auctionCategory='" + auctionCategory + '\'' +
        ", siteId='" + siteId + '\'' +
        ", siteName='" + siteName + '\'' +
        ", adzoneId='" + adzoneId + '\'' +
        ", adzoneName='" + adzoneName + '\'' +
        ", alipayTotalPrice=" + alipayTotalPrice +
        ", totalCommissionRate=" + totalCommissionRate +
        ", totalCommissionFee=" + totalCommissionFee +
        ", subsidyFee=" + subsidyFee +
        ", relationId=" + relationId +
        ", specialId=" + specialId +
        ", clickTime=" + clickTime +
        ", oneLevelCommission=" + oneLevelCommission +
        ", twoLevelCommission=" + twoLevelCommission +
        ", threeLevelCommission=" + threeLevelCommission +
        ", userId=" + userId +
        ", userName='" + userName + '\'' +
        ", promoterId=" + promoterId +
        ", promoterName='" + promoterName + '\'' +
        ", itemImg='" + itemImg + '\'' +
        ", userCommission=" + userCommission +
        ", userUit=" + userUit +
        ", oneLevelUserUit=" + oneLevelUserUit +
        ", secondLevelUserUit=" + secondLevelUserUit +
        '}';
  }
}
