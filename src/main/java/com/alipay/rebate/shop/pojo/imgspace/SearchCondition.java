package com.alipay.rebate.shop.pojo.imgspace;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;

public class SearchCondition extends CommonPageRequest {

  private Long groupId;
  private String keyWord;

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getKeyWord() {
    return keyWord;
  }

  public void setKeyWord(String keyWord) {
    this.keyWord = keyWord;
  }

  @Override
  public String toString() {
    return "SearchCondition{" +
        "groupId=" + groupId +
        ", keyWord='" + keyWord + '\'' +
        "} " + super.toString();
  }
}
