package com.alipay.rebate.shop.pojo.user.admin;

public class UpdateOrdersRequest {

    private String tradeId;
    private Long userId;



    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UpdateOrdersRequest{" +
                "tradeId='" + tradeId + '\'' +
                ", userId=" + userId +
                '}';
    }
}
