package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.URL;

public class UpdateHeadImageUrlRequest {

  @NotNull(message = "头像地址不能为空")
  @URL(message = "url 格式不正确")
  private String headImageUrl;

  public String getHeadImageUrl() {
    return headImageUrl;
  }

  public void setHeadImageUrl(String headImageUrl) {
    this.headImageUrl = headImageUrl;
  }

  @Override
  public String toString() {
    return "UpdateHeadImageUrlRequest{" +
        "headImageUrl='" + headImageUrl + '\'' +
        '}';
  }
}
