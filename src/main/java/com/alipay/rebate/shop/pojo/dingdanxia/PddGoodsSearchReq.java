package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.Arrays;

public class PddGoodsSearchReq {

    private String apikey;
    private String signature;
    private String keyword;
    private Long opt_id;
    private Integer page;
    private Integer page_size;
    private Integer sort_type;
    private Boolean with_coupon;
    private String range_list;
    private Long cat_id;
    private String goods_id_list;
    private Integer merchant_type;
    private String pid;
    private String custom_parameters;
    private Integer[] merchant_type_list;
    private Boolean is_brand_goods;
    private Integer[] activity_tags;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getOpt_id() {
        return opt_id;
    }

    public void setOpt_id(Long opt_id) {
        this.opt_id = opt_id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public void setPage_size(Integer page_size) {
        this.page_size = page_size;
    }

    public Integer getSort_type() {
        return sort_type;
    }

    public void setSort_type(Integer sort_type) {
        this.sort_type = sort_type;
    }

    public Boolean getWith_coupon() {
        return with_coupon;
    }

    public void setWith_coupon(Boolean with_coupon) {
        this.with_coupon = with_coupon;
    }

    public String getRange_list() {
        return range_list;
    }

    public void setRange_list(String range_list) {
        this.range_list = range_list;
    }

    public Long getCat_id() {
        return cat_id;
    }

    public void setCat_id(Long cat_id) {
        this.cat_id = cat_id;
    }

    public String getGoods_id_list() {
        return goods_id_list;
    }

    public void setGoods_id_list(String goods_id_list) {
        this.goods_id_list = goods_id_list;
    }

    public Integer getMerchant_type() {
        return merchant_type;
    }

    public void setMerchant_type(Integer merchant_type) {
        this.merchant_type = merchant_type;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public Integer[] getMerchant_type_list() {
        return merchant_type_list;
    }

    public void setMerchant_type_list(Integer[] merchant_type_list) {
        this.merchant_type_list = merchant_type_list;
    }

    public Boolean getIs_brand_goods() {
        return is_brand_goods;
    }

    public void setIs_brand_goods(Boolean is_brand_goods) {
        this.is_brand_goods = is_brand_goods;
    }

    public Integer[] getActivity_tags() {
        return activity_tags;
    }

    public void setActivity_tags(Integer[] activity_tags) {
        this.activity_tags = activity_tags;
    }

    @Override
    public String toString() {
        return "PddGoodsSearchReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", keyword='" + keyword + '\'' +
                ", opt_id=" + opt_id +
                ", page=" + page +
                ", page_size=" + page_size +
                ", sort_type=" + sort_type +
                ", with_coupon=" + with_coupon +
                ", range_list='" + range_list + '\'' +
                ", cat_id=" + cat_id +
                ", goods_id_list='" + goods_id_list + '\'' +
                ", merchant_type=" + merchant_type +
                ", pid='" + pid + '\'' +
                ", custom_parameters='" + custom_parameters + '\'' +
                ", merchant_type_list=" + Arrays.toString(merchant_type_list) +
                ", is_brand_goods=" + is_brand_goods +
                ", activity_tags=" + Arrays.toString(activity_tags) +
                '}';
    }
}
