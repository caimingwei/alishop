package com.alipay.rebate.shop.pojo.dingdanxia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DingdanxiaRsp<T> {

  private int code;
  private String msg;
  private T data;
//  private String request_id;

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

//  public String getRequest_id() {
//    return request_id;
//  }
//
//  public void setRequest_id(String request_id) {
//    this.request_id = request_id;
//  }

  @Override
  public String toString() {
    return "DingdanxiaResponse{" +
        "code=" + code +
        ", msg='" + msg + '\'' +
        ", data=" + data +
//        ", request_id='" + request_id + '\'' +
        '}';
  }
}
