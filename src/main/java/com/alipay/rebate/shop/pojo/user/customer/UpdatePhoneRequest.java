package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.constants.PatternContant;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class UpdatePhoneRequest {

  @NotEmpty(message = "手机号码不能为空")
  @Pattern(regexp= PatternContant.PHONE_PATTERN,
      message=PatternContant.PHONE_PATTERN_ERROR_MESSAGE)
  private String userPhone;
  @NotEmpty(message = "验证码不能为空")
  private String mobileCode;
  private String areaCode;

  public String getAreaCode() {
    return areaCode;
  }

  public void setAreaCode(String areaCode) {
    this.areaCode = areaCode;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public String getMobileCode() {
    return mobileCode;
  }

  public void setMobileCode(String mobileCode) {
    this.mobileCode = mobileCode;
  }

  @Override
  public String toString() {
    return "UpdatePhoneRequest{" +
            "userPhone='" + userPhone + '\'' +
            ", mobileCode='" + mobileCode + '\'' +
            ", areaCode='" + areaCode + '\'' +
            '}';
  }
}
