package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.constants.PatternContant;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

public class PrivilegeLinkProductData {

  @NotNull(message = PatternContant.GOODSID_ERROR_MESSAGE)
  private String goodsIds;

  @NotNull(message = PatternContant.TPWD_TEXT_ERROR_MESSAGE)
  @Length(min = 5,message = PatternContant.TPWD_TEXT_RANGE_ERROR_MESSAGE)
  private String text;
  @NotNull(message = PatternContant.TPWD_LOGO_ERROR_MESSAGE)
  private String logo;

  public String getGoodsIds() {
    return goodsIds;
  }

  public void setGoodsIds(String goodsIds) {
    this.goodsIds = goodsIds;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  @Override
  public String toString() {
    return "PrivilegeLinkProductData{" +
        "goodsIds='" + goodsIds + '\'' +
        ", text='" + text + '\'' +
        ", logo='" + logo + '\'' +
        '}';
  }

}
