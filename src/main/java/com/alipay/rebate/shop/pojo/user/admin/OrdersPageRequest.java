package com.alipay.rebate.shop.pojo.user.admin;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;
import javax.validation.constraints.Min;

public class OrdersPageRequest extends CommonPageRequest {
    private Long tradeId;
    private String itemTitle;
    private String startCreateTime;
    private String endCreateTime;
    private Long userId;
    private String userName;
    private String promoterId;
    private String promoterName;
    private Long orderType;
    private Integer tkStatus;
    private Long tkOrderRole;
    private String tradeParentId;

    public String getTradeParentId() {
        return tradeParentId;
    }

    public void setTradeParentId(String tradeParentId) {
        this.tradeParentId = tradeParentId;
    }

    public Integer getTkStatus() {
        return tkStatus;
    }

    public void setTkStatus(Integer tkStatus) {
        this.tkStatus = tkStatus;
    }

    public Long getTradeId() {
        return tradeId;
    }

    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPromoterId() {
        return promoterId;
    }

    public void setPromoterId(String promoterId) {
        this.promoterId = promoterId;
    }

    public String getPromoterName() {
        return promoterName;
    }

    public void setPromoterName(String promoterName) {
        this.promoterName = promoterName;
    }

    public Long getOrderType() {
        return orderType;
    }

    public void setOrderType(Long orderType) {
        this.orderType = orderType;
    }

    public Long getTkOrderRole() {
        return tkOrderRole;
    }

    public void setTkOrderRole(Long tkOrderRole) {
        this.tkOrderRole = tkOrderRole;
    }

    @Override
    public String toString() {
        return "OrdersPageRequest{" +
                "tradeId=" + tradeId +
                ", itemTitle='" + itemTitle + '\'' +
                ", startCreateTime='" + startCreateTime + '\'' +
                ", endCreateTime='" + endCreateTime + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", promoterId='" + promoterId + '\'' +
                ", promoterName='" + promoterName + '\'' +
                ", orderType=" + orderType +
                ", tkStatus=" + tkStatus +
                ", tkOrderRole=" + tkOrderRole +
                ", tradeParentId='" + tradeParentId + '\'' +
                '}';
    }
}
