package com.alipay.rebate.shop.pojo.user.customer;

import java.math.BigDecimal;

public class UserIncomeRecordResponse {

    private BigDecimal userAmount;
    private BigDecimal thisMonthSettlementEstimate;
    private BigDecimal lastMonthSettlementEstimate;
    private BigDecimal thisMonthPaidEstimate;
    private BigDecimal lastMonthPaidEstimate;
    private Integer thisDayPaidCount;
    private BigDecimal thisDayPaidCommission;
    private Integer lastDayPaidCount;
    private BigDecimal lastDayPaidCommission;

    public BigDecimal getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(BigDecimal userAmount) {
        this.userAmount = userAmount;
    }

    public BigDecimal getThisMonthSettlementEstimate() {
        return thisMonthSettlementEstimate;
    }

    public void setThisMonthSettlementEstimate(BigDecimal thisMonthSettlementEstimate) {
        this.thisMonthSettlementEstimate = thisMonthSettlementEstimate;
    }

    public BigDecimal getLastMonthSettlementEstimate() {
        return lastMonthSettlementEstimate;
    }

    public void setLastMonthSettlementEstimate(BigDecimal lastMonthSettlementEstimate) {
        this.lastMonthSettlementEstimate = lastMonthSettlementEstimate;
    }

    public BigDecimal getThisMonthPaidEstimate() {
        return thisMonthPaidEstimate;
    }

    public void setThisMonthPaidEstimate(BigDecimal thisMonthPaidEstimate) {
        this.thisMonthPaidEstimate = thisMonthPaidEstimate;
    }

    public BigDecimal getLastMonthPaidEstimate() {
        return lastMonthPaidEstimate;
    }

    public void setLastMonthPaidEstimate(BigDecimal lastMonthPaidEstimate) {
        this.lastMonthPaidEstimate = lastMonthPaidEstimate;
    }

    public Integer getThisDayPaidCount() {
        return thisDayPaidCount;
    }

    public void setThisDayPaidCount(Integer thisDayPaidCount) {
        this.thisDayPaidCount = thisDayPaidCount;
    }

    public BigDecimal getThisDayPaidCommission() {
        return thisDayPaidCommission;
    }

    public void setThisDayPaidCommission(BigDecimal thisDayPaidCommission) {
        this.thisDayPaidCommission = thisDayPaidCommission;
    }

    public Integer getLastDayPaidCount() {
        return lastDayPaidCount;
    }

    public void setLastDayPaidCount(Integer lastDayPaidCount) {
        this.lastDayPaidCount = lastDayPaidCount;
    }

    public BigDecimal getLastDayPaidCommission() {
        return lastDayPaidCommission;
    }

    public void setLastDayPaidCommission(BigDecimal lastDayPaidCommission) {
        this.lastDayPaidCommission = lastDayPaidCommission;
    }

    @Override
    public String toString() {
        return "UserIncomeRecordResponse{" +
                "userAmount=" + userAmount +
                ", thisMonthSettlementEstimate=" + thisMonthSettlementEstimate +
                ", lastMonthSettlementEstimate=" + lastMonthSettlementEstimate +
                ", thisMonthPaidEstimate=" + thisMonthPaidEstimate +
                ", lastMonthPaidEstimate=" + lastMonthPaidEstimate +
                ", thisDayPaidCount=" + thisDayPaidCount +
                ", thisDayPaidCommission=" + thisDayPaidCommission +
                ", lastDayPaidCount=" + lastDayPaidCount +
                ", lastDayPaidCommission=" + lastDayPaidCommission +
                '}';
    }
}
