package com.alipay.rebate.shop.pojo.user.admin;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class WithDrawPageRequest extends CommonPageRequest {

    private String userId;
    private String userName;
    @Min(value = 1,message = "提现状态可选值为 1,2,3")
    @Max(value = 3,message = "提现状态可选值为 1,2,3")
    private Integer status;
    @Min(value = 0,message = "提现状态可选值为 0,1")
    @Max(value = 1,message = "提现状态可选值为 0,1")
    private Integer type;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "WithDrawPageRequest{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", status=" + status +
                ", type=" + type +
                '}';
    }
}
