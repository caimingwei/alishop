package com.alipay.rebate.shop.pojo.haodanku;

public class HaodankuCommonMsg {

  protected Integer code;
  protected Integer min_id;
  protected String msg;

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public Integer getMin_id() {
    return min_id;
  }

  public void setMin_id(Integer min_id) {
    this.min_id = min_id;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  @Override
  public String toString() {
    return "HaodankuCommonMsg{" +
        "code=" + code +
        ", min_id=" + min_id +
        ", msg=" + msg +
        '}';
  }
}
