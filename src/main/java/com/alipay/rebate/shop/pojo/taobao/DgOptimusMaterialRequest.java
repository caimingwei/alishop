package com.alipay.rebate.shop.pojo.taobao;

/**
 * 通用物料
 */
public class DgOptimusMaterialRequest {

    private Long page_size;
    private Long adzone_id;
    private Long page_no;
    private Long material_id;
    private String device_value;
    private String device_encrypt;
    private String device_type;
    private Long content_id;
    private String content_source;
    private Long item_id;

    public Long getPage_size() {
        return page_size;
    }

    public void setPage_size(Long page_size) {
        this.page_size = page_size;
    }

    public Long getAdzone_id() {
        return adzone_id;
    }

    public void setAdzone_id(Long adzone_id) {
        this.adzone_id = adzone_id;
    }

    public Long getPage_no() {
        return page_no;
    }

    public void setPage_no(Long page_no) {
        this.page_no = page_no;
    }

    public Long getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Long material_id) {
        this.material_id = material_id;
    }

    public String getDevice_value() {
        return device_value;
    }

    public void setDevice_value(String device_value) {
        this.device_value = device_value;
    }

    public String getDevice_encrypt() {
        return device_encrypt;
    }

    public void setDevice_encrypt(String device_encrypt) {
        this.device_encrypt = device_encrypt;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public Long getContent_id() {
        return content_id;
    }

    public void setContent_id(Long content_id) {
        this.content_id = content_id;
    }

    public String getContent_source() {
        return content_source;
    }

    public void setContent_source(String content_source) {
        this.content_source = content_source;
    }

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    @Override
    public String toString() {
        return "DgOptimusMaterialRequest{" +
                "page_size=" + page_size +
                ", adzone_id=" + adzone_id +
                ", page_no=" + page_no +
                ", material_id=" + material_id +
                ", device_value='" + device_value + '\'' +
                ", device_encrypt='" + device_encrypt + '\'' +
                ", device_type='" + device_type + '\'' +
                ", content_id=" + content_id +
                ", content_source='" + content_source + '\'' +
                ", item_id=" + item_id +
                '}';
    }
}
