package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;

public class MeiTuanOrderRsp {
    private String smstitle;
    private String uid;
    private BigDecimal total;
    private String quantity;
    private String orderid;
    private String dealid;
    private Long modtime;
    private BigDecimal direct;
    private String sign;
    private Long paytime;
    private String sid;

    public String getSmstitle() {
        return smstitle;
    }

    public void setSmstitle(String smstitle) {
        this.smstitle = smstitle;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getDealid() {
        return dealid;
    }

    public void setDealid(String dealid) {
        this.dealid = dealid;
    }

    public Long getModtime() {
        return modtime;
    }

    public void setModtime(Long modtime) {
        this.modtime = modtime;
    }

    public BigDecimal getDirect() {
        return direct;
    }

    public void setDirect(BigDecimal direct) {
        this.direct = direct;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @Override
    public String toString() {
        return "MeiTuanOrderRsp{" +
                "smstitle='" + smstitle + '\'' +
                ", uid='" + uid + '\'' +
                ", total=" + total +
                ", quantity='" + quantity + '\'' +
                ", orderid='" + orderid + '\'' +
                ", dealid='" + dealid + '\'' +
                ", modtime=" + modtime +
                ", direct=" + direct +
                ", sign='" + sign + '\'' +
                ", paytime=" + paytime +
                ", sid='" + sid + '\'' +
                '}';
    }
}
