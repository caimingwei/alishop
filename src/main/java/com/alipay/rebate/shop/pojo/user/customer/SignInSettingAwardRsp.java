package com.alipay.rebate.shop.pojo.user.customer;

import java.math.BigDecimal;
import java.util.Date;

public class SignInSettingAwardRsp {

  private Long userId;
  private Date newlySignInTime;
  private Integer signInDays;
  private BigDecimal totalAwardMoney;
  private Integer totalIntegralMoney;
  private Integer awardType;
  private String ruleDesc;
  private Integer num;
  private boolean isSignToday;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Date getNewlySignInTime() {
    return newlySignInTime;
  }

  public void setNewlySignInTime(Date newlySignInTime) {
    this.newlySignInTime = newlySignInTime;
  }

  public Integer getSignInDays() {
    return signInDays;
  }

  public void setSignInDays(Integer signInDays) {
    this.signInDays = signInDays;
  }

  public BigDecimal getTotalAwardMoney() {
    return totalAwardMoney;
  }

  public void setTotalAwardMoney(BigDecimal totalAwardMoney) {
    this.totalAwardMoney = totalAwardMoney;
  }

  public Integer getAwardType() {
    return awardType;
  }

  public void setAwardType(Integer awardType) {
    this.awardType = awardType;
  }

  public String getRuleDesc() {
    return ruleDesc;
  }

  public void setRuleDesc(String ruleDesc) {
    this.ruleDesc = ruleDesc;
  }

  public Integer getNum() {
    return num;
  }

  public void setNum(Integer num) {
    this.num = num;
  }

  public boolean getSignToday() {
    return isSignToday;
  }

  public void setSignToday(boolean signToday) {
    isSignToday = signToday;
  }

  public Integer getTotalIntegralMoney() {
    return totalIntegralMoney;
  }

  public void setTotalIntegralMoney(Integer totalIntegralMoney) {
    this.totalIntegralMoney = totalIntegralMoney;
  }

  @Override
  public String toString() {
    return "SignInSettingAwardRsp{" +
        "userId=" + userId +
        ", newlySignInTime=" + newlySignInTime +
        ", signInDays=" + signInDays +
        ", totalAwardMoney=" + totalAwardMoney +
        ", totalIntegralMoney=" + totalIntegralMoney +
        ", awardType=" + awardType +
        ", ruleDesc='" + ruleDesc + '\'' +
        ", num=" + num +
        ", isSignToday=" + isSignToday +
        '}';
  }
}
