package com.alipay.rebate.shop.pojo.dataoke;

import java.util.List;

public class DataokeProductListData {

  Integer totalNum;
  String pageId;
  List<DataokeProduct> list;

  public Integer getTotalNum() {
    return totalNum;
  }

  public void setTotalNum(Integer totalNum) {
    this.totalNum = totalNum;
  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public List<DataokeProduct> getList() {
    return list;
  }

  public void setList(List<DataokeProduct> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "DataokeProductListData{" +
        "totalNum=" + totalNum +
        ", pageId='" + pageId + '\'' +
        ", list=" + list +
        '}';
  }
}
