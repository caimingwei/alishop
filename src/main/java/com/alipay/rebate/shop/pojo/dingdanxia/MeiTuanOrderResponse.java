package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.List;

public class MeiTuanOrderResponse {
    private int code;
    private String msg;
    private MeiTuanRsp data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public MeiTuanRsp getData() {
        return data;
    }

    public void setData(MeiTuanRsp data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MeiTuanOrderResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
