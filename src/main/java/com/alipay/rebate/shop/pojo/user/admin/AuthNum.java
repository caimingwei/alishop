package com.alipay.rebate.shop.pojo.user.admin;

public class AuthNum {

    private String date;
    private Integer authNum;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getAuthNum() {
        return authNum;
    }

    public void setAuthNum(Integer authNum) {
        this.authNum = authNum;
    }

    @Override
    public String toString() {
        return "AuthNum{" +
                "date='" + date + '\'' +
                ", authNum=" + authNum +
                '}';
    }
}
