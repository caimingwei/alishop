package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.constants.PatternContant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

public class H5RegisterRequest {

    @NotEmpty(message = PatternContant.PHONE_NULL_MESSAGE)
    @Pattern(regexp= PatternContant.PHONE_PATTERN,
            message=PatternContant.PHONE_PATTERN_ERROR_MESSAGE)
    private String userPhone;
    @NotNull(message = "推广人id不能为空")
    private Long parentUserId;
    @NotEmpty(message = "用户昵称不能为空")
    private String userNickName;
    @NotEmpty(message = "验证码不能为空")
    private String mobileCode;
    @NotEmpty(message = "密码不能为空")
    private String password;
//    @NotEmpty(message = "token不能为空")
    private String token;

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Long getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Long parentUserId) {
        this.parentUserId = parentUserId;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "H5RegisterRequest{" +
            "userPhone='" + userPhone + '\'' +
            ", parentUserId=" + parentUserId +
            ", userNickName='" + userNickName + '\'' +
            ", mobileCode='" + mobileCode + '\'' +
            ", password='" + password + '\'' +
            ", token='" + token + '\'' +
            '}';
    }
}
