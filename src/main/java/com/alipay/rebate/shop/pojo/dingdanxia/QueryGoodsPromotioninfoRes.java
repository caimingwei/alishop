package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.List;

public class QueryGoodsPromotioninfoRes {
    private Integer code;
    private String msg;
    private List<QueryGoodsPromotioninfoResponse> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<QueryGoodsPromotioninfoResponse> getData() {
        return data;
    }

    public void setData(List<QueryGoodsPromotioninfoResponse> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QueryGoodsPromotioninfoRes{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
