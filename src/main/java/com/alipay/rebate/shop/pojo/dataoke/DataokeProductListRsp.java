package com.alipay.rebate.shop.pojo.dataoke;

public class DataokeProductListRsp extends DataokeCommonMsg{

   DataokeProductListData data;

  public DataokeProductListData getData() {
    return data;
  }

  public void setData(DataokeProductListData data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "DataokeProductListRsp{" +
        "data=" + data +
        "} " + super.toString();
  }

}
