package com.alipay.rebate.shop.pojo.taobao;

public class TpwdRequest {

    private String user_id;
    private String text;
    private String url;
    private String logo;
    private String ext;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    @Override
    public String toString() {
        return "TpwdRequest{" +
                "user_id='" + user_id + '\'' +
                ", text='" + text + '\'' +
                ", url='" + url + '\'' +
                ", logo='" + logo + '\'' +
                ", ext='" + ext + '\'' +
                '}';
    }
}
