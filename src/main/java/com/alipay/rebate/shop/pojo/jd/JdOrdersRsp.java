package com.alipay.rebate.shop.pojo.jd;

import java.util.List;

public class JdOrdersRsp {

    private Integer code;
    private String msg;
    private Integer total_results;
    private Boolean hasMore;
    private List<TkJdOrdersResponse> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getTotal_results() {
        return total_results;
    }

    public void setTotal_results(Integer total_results) {
        this.total_results = total_results;
    }

    public Boolean getHasMore() {
        return hasMore;
    }

    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    public List<TkJdOrdersResponse> getData() {
        return data;
    }

    public void setData(List<TkJdOrdersResponse> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "JdOrdersRsp{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", total_results=" + total_results +
                ", hasMore=" + hasMore +
                ", data=" + data +
                '}';
    }
}
