package com.alipay.rebate.shop.pojo.pdtframework;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class OfficialChoice {

  private List<
      @Min(value = 0, message = "cid范围为0-14")
      @Max(value = 14, message = "cid范围为0-14")
      Integer> category;
  @Min(value = 0, message = "筛选值范围[0,1,2]")
  @Max(value = 2, message = "筛选值范围[0,1,2]")
  private Integer filtrate;
  @Min(value = 0, message = "cid范围为0-14")
  @Max(value = 14, message = "cid范围为0-14")
  private Integer adj_category;
  private BigDecimal minPrice;
  private BigDecimal maxPrice;
  private Integer sell;
  private Integer minBrokerage;
  private Integer maxBrokerage;
  private String keyword;
  @JsonIgnore
  @Min(value = 0, message = "sort范围为0-8")
  @Max(value = 8, message = "sort范围为0-8")
  private Integer sort;

  public List<Integer> getCategory() {
    return category;
  }

  public void setCategory(List<Integer> category) {
    this.category = category;
  }

  public Integer getFiltrate() {
    return filtrate;
  }

  public void setFiltrate(Integer filtrate) {
    this.filtrate = filtrate;
  }

  public Integer getAdj_category() {
    return adj_category;
  }

  public void setAdj_category(Integer adj_category) {
    this.adj_category = adj_category;
  }

  public BigDecimal getMinPrice() {
    return minPrice;
  }

  public void setMinPrice(BigDecimal minPrice) {
    this.minPrice = minPrice;
  }

  public BigDecimal getMaxPrice() {
    return maxPrice;
  }

  public void setMaxPrice(BigDecimal maxPrice) {
    this.maxPrice = maxPrice;
  }

  public Integer getSell() {
    return sell;
  }

  public void setSell(Integer sell) {
    this.sell = sell;
  }

  public Integer getMinBrokerage() {
    return minBrokerage;
  }

  public void setMinBrokerage(Integer minBrokerage) {
    this.minBrokerage = minBrokerage;
  }

  public Integer getMaxBrokerage() {
    return maxBrokerage;
  }

  public void setMaxBrokerage(Integer maxBrokerage) {
    this.maxBrokerage = maxBrokerage;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  @Override
  public String toString() {
    return "OfficialChoice{" +
        "category=" + category +
        ", filtrate=" + filtrate +
        ", adj_category=" + adj_category +
        ", minPrice=" + minPrice +
        ", maxPrice=" + maxPrice +
        ", sell=" + sell +
        ", minBrokerage=" + minBrokerage +
        ", maxBrokerage=" + maxBrokerage +
        ", keyword='" + keyword + '\'' +
        ", sort=" + sort +
        '}';
  }
}
