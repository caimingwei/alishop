package com.alipay.rebate.shop.pojo.dingdanxia;

public class PddGoodsDetailReq {

    private String apikey;
    private String signature;
    private Long goods_id_list;
    private String custom_parameters;
    private String pid;
    private Integer plan_type;
    private String search_id;
    private Long zs_duo_id;


    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getGoods_id_list() {
        return goods_id_list;
    }

    public void setGoods_id_list(Long goods_id_list) {
        this.goods_id_list = goods_id_list;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(Integer plan_type) {
        this.plan_type = plan_type;
    }

    public String getSearch_id() {
        return search_id;
    }

    public void setSearch_id(String search_id) {
        this.search_id = search_id;
    }

    public Long getZs_duo_id() {
        return zs_duo_id;
    }

    public void setZs_duo_id(Long zs_duo_id) {
        this.zs_duo_id = zs_duo_id;
    }

    @Override
    public String toString() {
        return "GoodsDetailReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", goods_id_list=" + goods_id_list +
                ", custom_parameters='" + custom_parameters + '\'' +
                ", pid='" + pid + '\'' +
                ", plan_type=" + plan_type +
                ", search_id='" + search_id + '\'' +
                ", zs_duo_id=" + zs_duo_id +
                '}';
    }
}
