package com.alipay.rebate.shop.pojo.pdd;

import java.util.Arrays;

public class GoodsPicture {

    private String goods_thumbnail_url;
    private String imgUrl;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getGoods_thumbnail_url() {
        return goods_thumbnail_url;
    }

    public void setGoods_thumbnail_url(String goods_thumbnail_url) {
        this.goods_thumbnail_url = goods_thumbnail_url;
    }

    @Override
    public String toString() {
        return "GoodsPicture{" +
                "goods_thumbnail_url='" + goods_thumbnail_url + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}
