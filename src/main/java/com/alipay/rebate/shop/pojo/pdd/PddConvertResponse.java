package com.alipay.rebate.shop.pojo.pdd;

public class PddConvertResponse {
    private Integer code;
    private String msg;
    private PddConvertRes data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PddConvertRes getData() {
        return data;
    }

    public void setData(PddConvertRes data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PddConvertResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
