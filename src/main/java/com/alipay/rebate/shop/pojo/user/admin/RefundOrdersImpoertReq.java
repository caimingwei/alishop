package com.alipay.rebate.shop.pojo.user.admin;

import java.math.BigDecimal;
import java.util.Date;

public class RefundOrdersImpoertReq {

    private String staterTime;
    private String finishTime;
    private String estimateTime;
    private String productName;
    private String url;
    private String tkTradeId;
    private String tkchildrenTradeId;
    private Long refundStatus;
    private BigDecimal estimateMoney;
    private BigDecimal refundMoney;
    private BigDecimal refundBusinessMoney;
    private Integer promoters;
    private String parentTradeid;
    private String childrentradeid;

    public String getStaterTime() {
        return staterTime;
    }

    public void setStaterTime(String staterTime) {
        this.staterTime = staterTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(String estimateTime) {
        this.estimateTime = estimateTime;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTkTradeId() {
        return tkTradeId;
    }

    public void setTkTradeId(String tkTradeId) {
        this.tkTradeId = tkTradeId;
    }

    public String getTkchildrenTradeId() {
        return tkchildrenTradeId;
    }

    public void setTkchildrenTradeId(String tkchildrenTradeId) {
        this.tkchildrenTradeId = tkchildrenTradeId;
    }

    public Long getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Long refundStatus) {
        this.refundStatus = refundStatus;
    }

    public BigDecimal getEstimateMoney() {
        return estimateMoney;
    }

    public void setEstimateMoney(BigDecimal estimateMoney) {
        this.estimateMoney = estimateMoney;
    }

    public BigDecimal getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(BigDecimal refundMoney) {
        this.refundMoney = refundMoney;
    }

    public BigDecimal getRefundBusinessMoney() {
        return refundBusinessMoney;
    }

    public void setRefundBusinessMoney(BigDecimal refundBusinessMoney) {
        this.refundBusinessMoney = refundBusinessMoney;
    }

    public Integer getPromoters() {
        return promoters;
    }

    public void setPromoters(Integer promoters) {
        this.promoters = promoters;
    }

    public String getParentTradeid() {
        return parentTradeid;
    }

    public void setParentTradeid(String parentTradeid) {
        this.parentTradeid = parentTradeid;
    }

    public String getChildrentradeid() {
        return childrentradeid;
    }

    public void setChildrentradeid(String childrentradeid) {
        this.childrentradeid = childrentradeid;
    }

    @Override
    public String toString() {
        return "RefundOrdersImpoertReq{" +
                "staterTime=" + staterTime +
                ", finishTime=" + finishTime +
                ", estimateTime=" + estimateTime +
                ", productName=" + productName +
                ", url='" + url + '\'' +
                ", tkTradeId='" + tkTradeId + '\'' +
                ", tkchildrenTradeId='" + tkchildrenTradeId + '\'' +
                ", refundStatus=" + refundStatus +
                ", estimateMoney=" + estimateMoney +
                ", refundMoney=" + refundMoney +
                ", refundBusinessMoney=" + refundBusinessMoney +
                ", promoters=" + promoters +
                ", parentTradeid='" + parentTradeid + '\'' +
                ", childrentradeid='" + childrentradeid + '\'' +
                '}';
    }
}
