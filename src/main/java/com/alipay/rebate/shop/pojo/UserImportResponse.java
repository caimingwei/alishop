package com.alipay.rebate.shop.pojo;

public class UserImportResponse {
    private String uid;
    private String phone;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "UserImportResponse{" +
                "uid='" + uid + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
