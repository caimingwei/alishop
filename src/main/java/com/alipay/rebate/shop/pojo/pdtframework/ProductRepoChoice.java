package com.alipay.rebate.shop.pojo.pdtframework;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class ProductRepoChoice {

  private List<Integer> category;
  @Min(value = 0, message = "筛选值范围[0,1,2]")
  @Max(value = 2, message = "筛选值范围[0,1,2]")
  private Integer filtrate;
  private Double minPrice;
  private Double maxPrice;
  private Integer sell;
  private Integer minBrokerage;
  private Integer maxBrokerage;
  private String keyword;
  @JsonIgnore
  @Min(value = 0, message = "sort范围为0-8")
  @Max(value = 8, message = "sort范围为0-8")
  private Integer sort;
  @Min(value = 0, message = "tmall范围为0-1")
  @Max(value = 1, message = "tmall范围为0-1")
  private Integer tmall;

  public List<Integer> getCategory() {
    return category;
  }

  public void setCategory(List<Integer> category) {
    this.category = category;
  }

  public Integer getFiltrate() {
    return filtrate;
  }

  public void setFiltrate(Integer filtrate) {
    this.filtrate = filtrate;
  }

  public Double getMinPrice() {
    return minPrice;
  }

  public void setMinPrice(Double minPrice) {
    this.minPrice = minPrice;
  }

  public Double getMaxPrice() {
    return maxPrice;
  }

  public void setMaxPrice(Double maxPrice) {
    this.maxPrice = maxPrice;
  }

  public Integer getSell() {
    return sell;
  }

  public void setSell(Integer sell) {
    this.sell = sell;
  }

  public Integer getMinBrokerage() {
    return minBrokerage;
  }

  public void setMinBrokerage(Integer minBrokerage) {
    this.minBrokerage = minBrokerage;
  }

  public Integer getMaxBrokerage() {
    return maxBrokerage;
  }

  public void setMaxBrokerage(Integer maxBrokerage) {
    this.maxBrokerage = maxBrokerage;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public Integer getTmall() {
    return tmall;
  }

  public void setTmall(Integer tmall) {
    this.tmall = tmall;
  }

  @Override
  public String toString() {
    return "ProductRepoChoice{" +
        "category=" + category +
        ", filtrate=" + filtrate +
        ", minPrice=" + minPrice +
        ", maxPrice=" + maxPrice +
        ", sell=" + sell +
        ", minBrokerage=" + minBrokerage +
        ", maxBrokerage=" + maxBrokerage +
        ", keyword='" + keyword + '\'' +
        ", sort=" + sort +
        ", tmall=" + tmall +
        '}';
  }
}
