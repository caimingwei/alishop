package com.alipay.rebate.shop.pojo.dingdanxia;

public class PddConvertReq {

    private String apikey;
    private String signature;
    private String p_id;
    private String goods_id_list;
    private Boolean generate_short_url;
    private Boolean multi_group;
    private String custom_parameters;
    private Boolean generate_weapp_webview;
    private String zs_duo_id;
    private Boolean generate_we_app;
    private Boolean generate_weiboapp_webview;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getGoods_id_list() {
        return goods_id_list;
    }

    public void setGoods_id_list(String goods_id_list) {
        this.goods_id_list = goods_id_list;
    }

    public Boolean getGenerate_short_url() {
        return generate_short_url;
    }

    public void setGenerate_short_url(Boolean generate_short_url) {
        this.generate_short_url = generate_short_url;
    }

    public Boolean getMulti_group() {
        return multi_group;
    }

    public void setMulti_group(Boolean multi_group) {
        this.multi_group = multi_group;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public Boolean getGenerate_weapp_webview() {
        return generate_weapp_webview;
    }

    public void setGenerate_weapp_webview(Boolean generate_weapp_webview) {
        this.generate_weapp_webview = generate_weapp_webview;
    }

    public String getZs_duo_id() {
        return zs_duo_id;
    }

    public void setZs_duo_id(String zs_duo_id) {
        this.zs_duo_id = zs_duo_id;
    }

    public Boolean getGenerate_we_app() {
        return generate_we_app;
    }

    public void setGenerate_we_app(Boolean generate_we_app) {
        this.generate_we_app = generate_we_app;
    }

    public Boolean getGenerate_weiboapp_webview() {
        return generate_weiboapp_webview;
    }

    public void setGenerate_weiboapp_webview(Boolean generate_weiboapp_webview) {
        this.generate_weiboapp_webview = generate_weiboapp_webview;
    }

    @Override
    public String toString() {
        return "PddConvertReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", p_id='" + p_id + '\'' +
                ", goods_id_list='" + goods_id_list + '\'' +
                ", generate_short_url=" + generate_short_url +
                ", multi_group=" + multi_group +
                ", custom_parameters='" + custom_parameters + '\'' +
                ", generate_weapp_webview=" + generate_weapp_webview +
                ", zs_duo_id='" + zs_duo_id + '\'' +
                ", generate_we_app=" + generate_we_app +
                ", generate_weiboapp_webview=" + generate_weiboapp_webview +
                '}';
    }
}
