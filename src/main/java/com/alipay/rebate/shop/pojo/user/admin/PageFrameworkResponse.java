package com.alipay.rebate.shop.pojo.user.admin;

import com.alibaba.fastjson.JSONObject;
import com.alipay.rebate.shop.model.PageModule;
import java.util.Date;
import java.util.List;

public class PageFrameworkResponse {

  private Long pageId;

  private String title;

  private String illustration;

  private Date createTime;

  private Date updateTime;

  private Integer isDefault;

  private Integer isCurrentUse;

  private String pageModules;

  private Integer isIndex;

  private Integer isUpdate;

  private String startColor;
  private String endColor;

  public String getStartColor() {
    return startColor;
  }

  public void setStartColor(String startColor) {
    this.startColor = startColor;
  }

  public String getEndColor() {
    return endColor;
  }

  public void setEndColor(String endColor) {
    this.endColor = endColor;
  }

  public Integer getIsIndex() {
    return isIndex;
  }

  public void setIsIndex(Integer isIndex) {
    this.isIndex = isIndex;
  }

  public Integer getIsUpdate() {
    return isUpdate;
  }

  public void setIsUpdate(Integer isUpdate) {
    this.isUpdate = isUpdate;
  }

  public Long getPageId() {
    return pageId;
  }

  public void setPageId(Long pageId) {
    this.pageId = pageId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIllustration() {
    return illustration;
  }

  public void setIllustration(String illustration) {
    this.illustration = illustration;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

  public Integer getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Integer isDefault) {
    this.isDefault = isDefault;
  }

  public Integer getIsCurrentUse() {
    return isCurrentUse;
  }

  public void setIsCurrentUse(Integer isCurrentUse) {
    this.isCurrentUse = isCurrentUse;
  }

  public String getPageModules() {
    return pageModules;
  }

  public void setPageModules(String pageModules) {
    this.pageModules = pageModules;
  }

  @Override
  public String toString() {
    return "PageFrameworkResponse{" +
            "pageId=" + pageId +
            ", title='" + title + '\'' +
            ", illustration='" + illustration + '\'' +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", isDefault=" + isDefault +
            ", isCurrentUse=" + isCurrentUse +
            ", pageModules='" + pageModules + '\'' +
            ", isIndex=" + isIndex +
            ", isUpdate=" + isUpdate +
            ", startColor='" + startColor + '\'' +
            ", endColor='" + endColor + '\'' +
            '}';
  }
}
