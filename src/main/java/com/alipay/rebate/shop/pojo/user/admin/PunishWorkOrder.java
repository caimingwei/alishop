package com.alipay.rebate.shop.pojo.user.admin;

import java.math.BigDecimal;

public class PunishWorkOrder {

    private String workOrderId;
    private String tradeId;
    private String type;
    private BigDecimal punishMoney;

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPunishMoney() {
        return punishMoney;
    }

    public void setPunishMoney(BigDecimal punishMoney) {
        this.punishMoney = punishMoney;
    }

    @Override
    public String toString() {
        return "PunishWorkOrder{" +
                "workOrderId='" + workOrderId + '\'' +
                ", tradeId='" + tradeId + '\'' +
                ", type='" + type + '\'' +
                ", punishMoney=" + punishMoney +
                '}';
    }
}
