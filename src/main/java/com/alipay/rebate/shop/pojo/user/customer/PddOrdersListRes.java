package com.alipay.rebate.shop.pojo.user.customer;

import java.math.BigDecimal;

public class PddOrdersListRes {

    private String orderId;
    private Long goodsId;
    private String goodsName;
    private BigDecimal goodsPrice;
    private Long goodsQuantity;
    private String goodsThumbnailUrl;
    private Integer orderStatus;
    private Long userUit;
    private String orderCreateTime;

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Long getGoodsQuantity() {
        return goodsQuantity;
    }

    public void setGoodsQuantity(Long goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }

    public String getGoodsThumbnailUrl() {
        return goodsThumbnailUrl;
    }

    public void setGoodsThumbnailUrl(String goodsThumbnailUrl) {
        this.goodsThumbnailUrl = goodsThumbnailUrl;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getUserUit() {
        return userUit;
    }

    public void setUserUit(Long userUit) {
        this.userUit = userUit;
    }

    @Override
    public String toString() {
        return "PddOrdersListRes{" +
                "orderId='" + orderId + '\'' +
                ", goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", goodsPrice=" + goodsPrice +
                ", goodsQuantity=" + goodsQuantity +
                ", goodsThumbnailUrl='" + goodsThumbnailUrl + '\'' +
                ", orderStatus=" + orderStatus +
                ", userUit=" + userUit +
                ", orderCreateTime='" + orderCreateTime + '\'' +
                '}';
    }
}
