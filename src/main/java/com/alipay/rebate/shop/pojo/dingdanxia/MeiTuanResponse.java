package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.List;

public class MeiTuanResponse {
    private int code;
    private String msg;
    private int total_results;
    private List<MeituanOrdersRsp> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public List<MeituanOrdersRsp> getData() {
        return data;
    }

    public void setData(List<MeituanOrdersRsp> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MeiTuanResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", total_results=" + total_results +
                ", data=" + data +
                '}';
    }
}
