package com.alipay.rebate.shop.pojo.dataoke;

public class DataokeRankingListReq {

    private Integer rankType;
    private Integer cid;

    public Integer getRankType() {
        return rankType;
    }

    public void setRankType(Integer rankType) {
        this.rankType = rankType;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    @Override
    public String toString() {
        return "DataokeRankingListReq{" +
                "rankType=" + rankType +
                ", cid=" + cid +
                '}';
    }
}
