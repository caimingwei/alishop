package com.alipay.rebate.shop.pojo.user.customer;

public class TaobaoAuthFailRsp {

  private Long userId;
  private String userNickName;
  private String phone;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserNickName() {
    return userNickName;
  }

  public void setUserNickName(String userNickName) {
    this.userNickName = userNickName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public String toString() {
    return "TaobaoAuthFailRsp{" +
        "userId=" + userId +
        ", userNickName='" + userNickName + '\'' +
        ", phone='" + phone + '\'' +
        '}';
  }
}
