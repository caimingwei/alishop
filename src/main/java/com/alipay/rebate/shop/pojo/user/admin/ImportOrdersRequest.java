package com.alipay.rebate.shop.pojo.user.admin;

import com.alipay.rebate.shop.model.Orders;

import java.util.List;

public class ImportOrdersRequest {

    private List<Orders> orders;

    public List<Orders> getOrders() {
        return orders;
    }

    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "ImportOrdersRequest{" +
                "orders=" + orders +
                '}';
    }
}
