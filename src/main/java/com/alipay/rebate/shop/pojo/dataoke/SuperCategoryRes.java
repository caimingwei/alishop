package com.alipay.rebate.shop.pojo.dataoke;

public class SuperCategoryRes {

    private Long subcid;
    private String subcname;
    private String scpic;

    public Long getSubcid() {
        return subcid;
    }

    public void setSubcid(Long subcid) {
        this.subcid = subcid;
    }

    public String getSubcname() {
        return subcname;
    }

    public void setSubcname(String subcname) {
        this.subcname = subcname;
    }

    public String getScpic() {
        return scpic;
    }

    public void setScpic(String scpic) {
        this.scpic = scpic;
    }

    @Override
    public String toString() {
        return "SuperCategoryRequest{" +
                "subcid=" + subcid +
                ", subcname='" + subcname + '\'' +
                ", scpic='" + scpic + '\'' +
                '}';
    }
}
