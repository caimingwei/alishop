package com.alipay.rebate.shop.pojo.dataoke;

public class DataokeCommonMsg {

  private Long time;
  private Integer code;
  private String msg;

  public Long getTime() {
    return time;
  }

  public void setTime(Long time) {
    this.time = time;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  @Override
  public String toString() {
    return "DataokeCommonMsg{" +
        "time=" + time +
        ", code=" + code +
        ", msg='" + msg + '\'' +
        '}';
  }
}
