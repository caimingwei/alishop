package com.alipay.rebate.shop.pojo.pdd;

public class PddConvertRes {

    private String weibo_app_web_view_short_url;
    private String mobile_url;
    private String goods_id;
    private String goods_detail;
    private String url;
    private String short_url;
    private String qq_app_info;
    private String mobile_super_short_url;
    private String we_app_info;
    private String schema_url;
    private String weibo_app_web_view_url;
    private String mobile_short_url;
    private String we_app_web_view_url;
    private String we_app_web_view_short_url;

    public String getWeibo_app_web_view_short_url() {
        return weibo_app_web_view_short_url;
    }

    public void setWeibo_app_web_view_short_url(String weibo_app_web_view_short_url) {
        this.weibo_app_web_view_short_url = weibo_app_web_view_short_url;
    }

    public String getMobile_url() {
        return mobile_url;
    }

    public void setMobile_url(String mobile_url) {
        this.mobile_url = mobile_url;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_detail() {
        return goods_detail;
    }

    public void setGoods_detail(String goods_detail) {
        this.goods_detail = goods_detail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShort_url() {
        return short_url;
    }

    public void setShort_url(String short_url) {
        this.short_url = short_url;
    }

    public String getQq_app_info() {
        return qq_app_info;
    }

    public void setQq_app_info(String qq_app_info) {
        this.qq_app_info = qq_app_info;
    }

    public String getMobile_super_short_url() {
        return mobile_super_short_url;
    }

    public void setMobile_super_short_url(String mobile_super_short_url) {
        this.mobile_super_short_url = mobile_super_short_url;
    }

    public String getWe_app_info() {
        return we_app_info;
    }

    public void setWe_app_info(String we_app_info) {
        this.we_app_info = we_app_info;
    }

    public String getSchema_url() {
        return schema_url;
    }

    public void setSchema_url(String schema_url) {
        this.schema_url = schema_url;
    }

    public String getWeibo_app_web_view_url() {
        return weibo_app_web_view_url;
    }

    public void setWeibo_app_web_view_url(String weibo_app_web_view_url) {
        this.weibo_app_web_view_url = weibo_app_web_view_url;
    }

    public String getMobile_short_url() {
        return mobile_short_url;
    }

    public void setMobile_short_url(String mobile_short_url) {
        this.mobile_short_url = mobile_short_url;
    }

    public String getWe_app_web_view_url() {
        return we_app_web_view_url;
    }

    public void setWe_app_web_view_url(String we_app_web_view_url) {
        this.we_app_web_view_url = we_app_web_view_url;
    }

    public String getWe_app_web_view_short_url() {
        return we_app_web_view_short_url;
    }

    public void setWe_app_web_view_short_url(String we_app_web_view_short_url) {
        this.we_app_web_view_short_url = we_app_web_view_short_url;
    }

    @Override
    public String toString() {
        return "PddConvertRes{" +
                "weibo_app_web_view_short_url='" + weibo_app_web_view_short_url + '\'' +
                ", mobile_url='" + mobile_url + '\'' +
                ", goods_id='" + goods_id + '\'' +
                ", goods_detail='" + goods_detail + '\'' +
                ", url='" + url + '\'' +
                ", short_url='" + short_url + '\'' +
                ", qq_app_info='" + qq_app_info + '\'' +
                ", mobile_super_short_url='" + mobile_super_short_url + '\'' +
                ", we_app_info='" + we_app_info + '\'' +
                ", schema_url='" + schema_url + '\'' +
                ", weibo_app_web_view_url='" + weibo_app_web_view_url + '\'' +
                ", mobile_short_url='" + mobile_short_url + '\'' +
                ", we_app_web_view_url='" + we_app_web_view_url + '\'' +
                ", we_app_web_view_short_url='" + we_app_web_view_short_url + '\'' +
                '}';
    }
}
