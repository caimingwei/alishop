package com.alipay.rebate.shop.pojo.pdd;

import java.util.List;

public class PddDdkWeappQrcodeUrlGenReq {

    private String custom_parameters;
    private Boolean generate_mall_collect_coupon;
    private List<Long> goods_id_list;
    private String p_id;
    private Long zs_duo_id;

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public Boolean getGenerate_mall_collect_coupon() {
        return generate_mall_collect_coupon;
    }

    public void setGenerate_mall_collect_coupon(Boolean generate_mall_collect_coupon) {
        this.generate_mall_collect_coupon = generate_mall_collect_coupon;
    }

    public List<Long> getGoods_id_list() {
        return goods_id_list;
    }

    public void setGoods_id_list(List<Long> goods_id_list) {
        this.goods_id_list = goods_id_list;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public Long getZs_duo_id() {
        return zs_duo_id;
    }

    public void setZs_duo_id(Long zs_duo_id) {
        this.zs_duo_id = zs_duo_id;
    }

    @Override
    public String toString() {
        return "PddDdkWeappQrcodeUrlGenReq{" +
                "custom_parameters='" + custom_parameters + '\'' +
                ", generate_mall_collect_coupon=" + generate_mall_collect_coupon +
                ", goods_id_list=" + goods_id_list +
                ", p_id='" + p_id + '\'' +
                ", zs_duo_id=" + zs_duo_id +
                '}';
    }
}
