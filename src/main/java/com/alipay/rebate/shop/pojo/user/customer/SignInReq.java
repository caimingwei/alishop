package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.NotNull;

public class SignInReq {

  private String signInTime;
  @NotNull(message = "奖励数量不能为空")
  private Integer num;

  public String getSignInTime() {
    return signInTime;
  }

  public void setSignInTime(String signInTime) {
    this.signInTime = signInTime;
  }

  public Integer getNum() {
    return num;
  }

  public void setNum(Integer num) {
    this.num = num;
  }

  @Override
  public String toString() {
    return "SignInReq{" +
        "signInTime='" + signInTime + '\'' +
        ", num=" + num +
        '}';
  }
}
