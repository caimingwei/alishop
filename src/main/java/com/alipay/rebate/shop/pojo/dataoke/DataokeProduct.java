package com.alipay.rebate.shop.pojo.dataoke;

import javax.persistence.Column;
import java.math.BigDecimal;

public class DataokeProduct {

  private Number id;
  private String goodsId;
  private String itemLink;
  private String title;
  private String dtitle;
  private String desc;
  private Long cid;
  private Long tbcid;
  private String mainPic;
  private String marketingMainPic;
  private BigDecimal originalPrice;
  private BigDecimal actualPrice;
  private BigDecimal discounts;
  private Integer commissionType;
  private BigDecimal commissionRate;
  private String couponLink;
  private Integer couponTotalNum;
  private Integer couponReceiveNum;
  private String couponEndTime;
  private String couponStartTime;
  private BigDecimal couponPrice;
  private String couponConditions;
  private Integer monthSales;
  private Integer twoHoursSales;
  private Integer dailySales;
  private Integer brand;
  private Long brandId;
  private String brandName;
  private String createTime;
  private Integer tchaoshi;
  private Integer activityType;
  private String activityStartTime;
  private String activityEndTime;
  private Integer shopType;
  private Integer haitao;
  private Integer goldSellers;
  private Long sellerId;
  private String shopName;
  private String shopLevel;
  private BigDecimal descScore;
  private BigDecimal dsrScore;
  private BigDecimal dsrPercent;
  private BigDecimal shipScore;
  private BigDecimal shipPercent;
  private BigDecimal serviceScore;
  private BigDecimal servicePercent;
  private Integer hotPush;
  private String teamName;
  private BigDecimal quanMLink;
  private BigDecimal hzQuanOver;
  private String circleText;
  private Integer freeshipRemoteDistrict;

  public BigDecimal getQuanMLink() {
    return quanMLink;
  }

  public void setQuanMLink(BigDecimal quanMLink) {
    this.quanMLink = quanMLink;
  }

  public BigDecimal getHzQuanOver() {
    return hzQuanOver;
  }

  public void setHzQuanOver(BigDecimal hzQuanOver) {
    this.hzQuanOver = hzQuanOver;
  }

  public String getCircleText() {
    return circleText;
  }

  public void setCircleText(String circleText) {
    this.circleText = circleText;
  }

  public Integer getFreeshipRemoteDistrict() {
    return freeshipRemoteDistrict;
  }

  public void setFreeshipRemoteDistrict(Integer freeshipRemoteDistrict) {
    this.freeshipRemoteDistrict = freeshipRemoteDistrict;
  }

  public Number getId() {
    return id;
  }

  public void setId(Number id) {
    this.id = id;
  }

  public String getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(String goodsId) {
    this.goodsId = goodsId;
  }

  public String getItemLink() {
    return itemLink;
  }

  public void setItemLink(String itemLink) {
    this.itemLink = itemLink;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDtitle() {
    return dtitle;
  }

  public void setDtitle(String dtitle) {
    this.dtitle = dtitle;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public Long getCid() {
    return cid;
  }

  public void setCid(Long cid) {
    this.cid = cid;
  }

  public Long getTbcid() {
    return tbcid;
  }

  public void setTbcid(Long tbcid) {
    this.tbcid = tbcid;
  }

  public String getMainPic() {
    return mainPic;
  }

  public void setMainPic(String mainPic) {
    this.mainPic = mainPic;
  }

  public String getMarketingMainPic() {
    return marketingMainPic;
  }

  public void setMarketingMainPic(String marketingMainPic) {
    this.marketingMainPic = marketingMainPic;
  }

  public BigDecimal getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
  }

  public BigDecimal getActualPrice() {
    return actualPrice;
  }

  public void setActualPrice(BigDecimal actualPrice) {
    this.actualPrice = actualPrice;
  }

  public BigDecimal getDiscounts() {
    return discounts;
  }

  public void setDiscounts(BigDecimal discounts) {
    this.discounts = discounts;
  }

  public Integer getCommissionType() {
    return commissionType;
  }

  public void setCommissionType(Integer commissionType) {
    this.commissionType = commissionType;
  }

  public BigDecimal getCommissionRate() {
    return commissionRate;
  }

  public void setCommissionRate(BigDecimal commissionRate) {
    this.commissionRate = commissionRate;
  }

  public String getCouponLink() {
    return couponLink;
  }

  public void setCouponLink(String couponLink) {
    this.couponLink = couponLink;
  }

  public Integer getCouponTotalNum() {
    return couponTotalNum;
  }

  public void setCouponTotalNum(Integer couponTotalNum) {
    this.couponTotalNum = couponTotalNum;
  }

  public Integer getCouponReceiveNum() {
    return couponReceiveNum;
  }

  public void setCouponReceiveNum(Integer couponReceiveNum) {
    this.couponReceiveNum = couponReceiveNum;
  }

  public String getCouponEndTime() {
    return couponEndTime;
  }

  public void setCouponEndTime(String couponEndTime) {
    this.couponEndTime = couponEndTime;
  }

  public String getCouponStartTime() {
    return couponStartTime;
  }

  public void setCouponStartTime(String couponStartTime) {
    this.couponStartTime = couponStartTime;
  }

  public BigDecimal getCouponPrice() {
    return couponPrice;
  }

  public void setCouponPrice(BigDecimal couponPrice) {
    this.couponPrice = couponPrice;
  }

  public String getCouponConditions() {
    return couponConditions;
  }

  public void setCouponConditions(String couponConditions) {
    this.couponConditions = couponConditions;
  }

  public Integer getMonthSales() {
    return monthSales;
  }

  public void setMonthSales(Integer monthSales) {
    this.monthSales = monthSales;
  }

  public Integer getTwoHoursSales() {
    return twoHoursSales;
  }

  public void setTwoHoursSales(Integer twoHoursSales) {
    this.twoHoursSales = twoHoursSales;
  }

  public Integer getDailySales() {
    return dailySales;
  }

  public void setDailySales(Integer dailySales) {
    this.dailySales = dailySales;
  }

  public Integer getBrand() {
    return brand;
  }

  public void setBrand(Integer brand) {
    this.brand = brand;
  }

  public Long getBrandId() {
    return brandId;
  }

  public void setBrandId(Long brandId) {
    this.brandId = brandId;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }

  public Integer getTchaoshi() {
    return tchaoshi;
  }

  public void setTchaoshi(Integer tchaoshi) {
    this.tchaoshi = tchaoshi;
  }

  public Integer getActivityType() {
    return activityType;
  }

  public void setActivityType(Integer activityType) {
    this.activityType = activityType;
  }

  public String getActivityStartTime() {
    return activityStartTime;
  }

  public void setActivityStartTime(String activityStartTime) {
    this.activityStartTime = activityStartTime;
  }

  public String getActivityEndTime() {
    return activityEndTime;
  }

  public void setActivityEndTime(String activityEndTime) {
    this.activityEndTime = activityEndTime;
  }

  public Integer getShopType() {
    return shopType;
  }

  public void setShopType(Integer shopType) {
    this.shopType = shopType;
  }

  public Integer getHaitao() {
    return haitao;
  }

  public void setHaitao(Integer haitao) {
    this.haitao = haitao;
  }

  public Integer getGoldSellers() {
    return goldSellers;
  }

  public void setGoldSellers(Integer goldSellers) {
    this.goldSellers = goldSellers;
  }

  public Long getSellerId() {
    return sellerId;
  }

  public void setSellerId(Long sellerId) {
    this.sellerId = sellerId;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getShopLevel() {
    return shopLevel;
  }

  public void setShopLevel(String shopLevel) {
    this.shopLevel = shopLevel;
  }

  public BigDecimal getDescScore() {
    return descScore;
  }

  public void setDescScore(BigDecimal descScore) {
    this.descScore = descScore;
  }

  public BigDecimal getDsrScore() {
    return dsrScore;
  }

  public void setDsrScore(BigDecimal dsrScore) {
    this.dsrScore = dsrScore;
  }

  public BigDecimal getDsrPercent() {
    return dsrPercent;
  }

  public void setDsrPercent(BigDecimal dsrPercent) {
    this.dsrPercent = dsrPercent;
  }

  public BigDecimal getShipScore() {
    return shipScore;
  }

  public void setShipScore(BigDecimal shipScore) {
    this.shipScore = shipScore;
  }

  public BigDecimal getShipPercent() {
    return shipPercent;
  }

  public void setShipPercent(BigDecimal shipPercent) {
    this.shipPercent = shipPercent;
  }

  public BigDecimal getServiceScore() {
    return serviceScore;
  }

  public void setServiceScore(BigDecimal serviceScore) {
    this.serviceScore = serviceScore;
  }

  public BigDecimal getServicePercent() {
    return servicePercent;
  }

  public void setServicePercent(BigDecimal servicePercent) {
    this.servicePercent = servicePercent;
  }

  public Integer getHotPush() {
    return hotPush;
  }

  public void setHotPush(Integer hotPush) {
    this.hotPush = hotPush;
  }

  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }

    @Override
    public String toString() {
        return "DataokeProduct{" +
                "id=" + id +
                ", goodsId='" + goodsId + '\'' +
                ", itemLink='" + itemLink + '\'' +
                ", title='" + title + '\'' +
                ", dtitle='" + dtitle + '\'' +
                ", desc='" + desc + '\'' +
                ", cid=" + cid +
                ", tbcid=" + tbcid +
                ", mainPic='" + mainPic + '\'' +
                ", marketingMainPic='" + marketingMainPic + '\'' +
                ", originalPrice=" + originalPrice +
                ", actualPrice=" + actualPrice +
                ", discounts=" + discounts +
                ", commissionType=" + commissionType +
                ", commissionRate=" + commissionRate +
                ", couponLink='" + couponLink + '\'' +
                ", couponTotalNum=" + couponTotalNum +
                ", couponReceiveNum=" + couponReceiveNum +
                ", couponEndTime='" + couponEndTime + '\'' +
                ", couponStartTime='" + couponStartTime + '\'' +
                ", couponPrice=" + couponPrice +
                ", couponConditions='" + couponConditions + '\'' +
                ", monthSales=" + monthSales +
                ", twoHoursSales=" + twoHoursSales +
                ", dailySales=" + dailySales +
                ", brand=" + brand +
                ", brandId=" + brandId +
                ", brandName='" + brandName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", tchaoshi=" + tchaoshi +
                ", activityType=" + activityType +
                ", activityStartTime='" + activityStartTime + '\'' +
                ", activityEndTime='" + activityEndTime + '\'' +
                ", shopType=" + shopType +
                ", haitao=" + haitao +
                ", goldSellers=" + goldSellers +
                ", sellerId=" + sellerId +
                ", shopName='" + shopName + '\'' +
                ", shopLevel='" + shopLevel + '\'' +
                ", descScore=" + descScore +
                ", dsrScore=" + dsrScore +
                ", dsrPercent=" + dsrPercent +
                ", shipScore=" + shipScore +
                ", shipPercent=" + shipPercent +
                ", serviceScore=" + serviceScore +
                ", servicePercent=" + servicePercent +
                ", hotPush=" + hotPush +
                ", teamName='" + teamName + '\'' +
                ", quanMLink=" + quanMLink +
                ", hzQuanOver=" + hzQuanOver +
                ", circleText='" + circleText + '\'' +
                ", freeshipRemoteDistrict=" + freeshipRemoteDistrict +
                '}';
    }
}
