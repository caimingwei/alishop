package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;

public class SuperSearchReq {

  private String q;
  private String cat;
  private Boolean has_coupon;
  private String sort;
  private String platform;
  private Integer page_size;
  private Integer page_no;
  private BigDecimal start_dsr;
  private BigDecimal end_tk_rate;
  private BigDecimal start_tk_rate;
  private BigDecimal end_price;
  private BigDecimal start_price;
  private Boolean is_overseas;
  private Boolean is_tmall;
  private String itemloc;
  private Long material_id;
  private String ip;
  private Boolean include_rfd_rate;
  private Boolean include_good_rate;
  private Boolean include_pay_rate_30;
  private Boolean need_prepay;
  private Integer need_free_shipment;
  private Integer npx_level;
  private Integer end_ka_tk_rate;
  private String start_ka_tk_rate;
  private String device_value;
  private String device_encrypt;
  private String device_type;

  public String getQ() {
    return q;
  }

  public void setQ(String q) {
    this.q = q;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public Boolean getHas_coupon() {
    return has_coupon;
  }

  public void setHas_coupon(Boolean has_coupon) {
    this.has_coupon = has_coupon;
  }

  public String getSort() {
    return sort;
  }

  public void setSort(String sort) {
    this.sort = sort;
  }

  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public Integer getPage_size() {
    return page_size;
  }

  public void setPage_size(Integer page_size) {
    this.page_size = page_size;
  }

  public Integer getPage_no() {
    return page_no;
  }

  public void setPage_no(Integer page_no) {
    this.page_no = page_no;
  }

  public BigDecimal getStart_dsr() {
    return start_dsr;
  }

  public void setStart_dsr(BigDecimal start_dsr) {
    this.start_dsr = start_dsr;
  }

  public BigDecimal getEnd_tk_rate() {
    return end_tk_rate;
  }

  public void setEnd_tk_rate(BigDecimal end_tk_rate) {
    this.end_tk_rate = end_tk_rate;
  }

  public BigDecimal getStart_tk_rate() {
    return start_tk_rate;
  }

  public void setStart_tk_rate(BigDecimal start_tk_rate) {
    this.start_tk_rate = start_tk_rate;
  }

  public BigDecimal getEnd_price() {
    return end_price;
  }

  public void setEnd_price(BigDecimal end_price) {
    this.end_price = end_price;
  }

  public BigDecimal getStart_price() {
    return start_price;
  }

  public void setStart_price(BigDecimal start_price) {
    this.start_price = start_price;
  }

  public Boolean getIs_overseas() {
    return is_overseas;
  }

  public void setIs_overseas(Boolean is_overseas) {
    this.is_overseas = is_overseas;
  }

  public Boolean getIs_tmall() {
    return is_tmall;
  }

  public void setIs_tmall(Boolean is_tmall) {
    this.is_tmall = is_tmall;
  }

  public String getItemloc() {
    return itemloc;
  }

  public void setItemloc(String itemloc) {
    this.itemloc = itemloc;
  }

  public Long getMaterial_id() {
    return material_id;
  }

  public void setMaterial_id(Long material_id) {
    this.material_id = material_id;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public Boolean getInclude_rfd_rate() {
    return include_rfd_rate;
  }

  public void setInclude_rfd_rate(Boolean include_rfd_rate) {
    this.include_rfd_rate = include_rfd_rate;
  }

  public Boolean getInclude_good_rate() {
    return include_good_rate;
  }

  public void setInclude_good_rate(Boolean include_good_rate) {
    this.include_good_rate = include_good_rate;
  }

  public Boolean getInclude_pay_rate_30() {
    return include_pay_rate_30;
  }

  public void setInclude_pay_rate_30(Boolean include_pay_rate_30) {
    this.include_pay_rate_30 = include_pay_rate_30;
  }

  public Boolean getNeed_prepay() {
    return need_prepay;
  }

  public void setNeed_prepay(Boolean need_prepay) {
    this.need_prepay = need_prepay;
  }

  public Integer getNeed_free_shipment() {
    return need_free_shipment;
  }

  public void setNeed_free_shipment(Integer need_free_shipment) {
    this.need_free_shipment = need_free_shipment;
  }

  public Integer getNpx_level() {
    return npx_level;
  }

  public void setNpx_level(Integer npx_level) {
    this.npx_level = npx_level;
  }

  public Integer getEnd_ka_tk_rate() {
    return end_ka_tk_rate;
  }

  public void setEnd_ka_tk_rate(Integer end_ka_tk_rate) {
    this.end_ka_tk_rate = end_ka_tk_rate;
  }

  public String getStart_ka_tk_rate() {
    return start_ka_tk_rate;
  }

  public void setStart_ka_tk_rate(String start_ka_tk_rate) {
    this.start_ka_tk_rate = start_ka_tk_rate;
  }

  public String getDevice_value() {
    return device_value;
  }

  public void setDevice_value(String device_value) {
    this.device_value = device_value;
  }

  public String getDevice_encrypt() {
    return device_encrypt;
  }

  public void setDevice_encrypt(String device_encrypt) {
    this.device_encrypt = device_encrypt;
  }

  public String getDevice_type() {
    return device_type;
  }

  public void setDevice_type(String device_type) {
    this.device_type = device_type;
  }

  @Override
  public String toString() {
    return "SuperSearchRequest{" +
        "q='" + q + '\'' +
        ", cat='" + cat + '\'' +
        ", has_coupon=" + has_coupon +
        ", sort='" + sort + '\'' +
        ", platform='" + platform + '\'' +
        ", page_size=" + page_size +
        ", page_no=" + page_no +
        ", start_dsr=" + start_dsr +
        ", end_tk_rate=" + end_tk_rate +
        ", start_tk_rate=" + start_tk_rate +
        ", end_price=" + end_price +
        ", start_price=" + start_price +
        ", is_overseas=" + is_overseas +
        ", is_tmall=" + is_tmall +
        ", itemloc='" + itemloc + '\'' +
        ", material_id=" + material_id +
        ", ip='" + ip + '\'' +
        ", include_rfd_rate=" + include_rfd_rate +
        ", include_good_rate=" + include_good_rate +
        ", include_pay_rate_30=" + include_pay_rate_30 +
        ", need_prepay=" + need_prepay +
        ", need_free_shipment=" + need_free_shipment +
        ", npx_level=" + npx_level +
        ", end_ka_tk_rate=" + end_ka_tk_rate +
        ", start_ka_tk_rate='" + start_ka_tk_rate + '\'' +
        ", device_value='" + device_value + '\'' +
        ", device_encrypt='" + device_encrypt + '\'' +
        ", device_type='" + device_type + '\'' +
        '}';
  }
}
