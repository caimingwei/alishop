package com.alipay.rebate.shop.pojo.user.admin;

import java.util.List;

public class PunishWorkOrderReq {
    private List<PunishWorkOrder> workOrder;

    public List<PunishWorkOrder> getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(List<PunishWorkOrder> workOrder) {
        this.workOrder = workOrder;
    }

    @Override
    public String toString() {
        return "PunishWorkOrderReq{" +
                "workOrder=" + workOrder +
                '}';
    }
}
