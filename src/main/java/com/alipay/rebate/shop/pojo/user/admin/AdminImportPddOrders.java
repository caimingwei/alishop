package com.alipay.rebate.shop.pojo.user.admin;

import com.alipay.rebate.shop.model.PddOrders;

import java.util.List;

public class AdminImportPddOrders {

    private List<ImportPddOrders> orders;

    public List<ImportPddOrders> getOrders() {
        return orders;
    }

    public void setOrders(List<ImportPddOrders> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "AdminImportPddOrders{" +
                "orders=" + orders +
                '}';
    }
}
