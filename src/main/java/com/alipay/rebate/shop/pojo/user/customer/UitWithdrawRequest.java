package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.NotNull;

public class UitWithdrawRequest {

    @NotNull(message = "提现集分宝数不能为空")
    private Long withDrawUitCount;

    public Long getWithDrawUitCount() {
        return withDrawUitCount;
    }

    public void setWithDrawUitCount(Long withDrawUitCount) {
        this.withDrawUitCount = withDrawUitCount;
    }

    @Override
    public String toString() {
        return "UitWithdrawRequest{" +
                "withDrawUitCount=" + withDrawUitCount +
                '}';
    }
}
