package com.alipay.rebate.shop.pojo.taobao;

public class TkOrderRequest {

    private Long query_type;
    private String position_index;
    private Long page_size;
    private Long member_type;
    private Long tk_status;
    private String end_time;
    private String start_time;
    private Long jump_type;
    private Long page_no;
    private Long order_scene;

    public Long getQuery_type() {
        return query_type;
    }

    public void setQuery_type(Long query_type) {
        this.query_type = query_type;
    }

    public String getPosition_index() {
        return position_index;
    }

    public void setPosition_index(String position_index) {
        this.position_index = position_index;
    }

    public Long getPage_size() {
        return page_size;
    }

    public void setPage_size(Long page_size) {
        this.page_size = page_size;
    }

    public Long getMember_type() {
        return member_type;
    }

    public void setMember_type(Long member_type) {
        this.member_type = member_type;
    }

    public Long getTk_status() {
        return tk_status;
    }

    public void setTk_status(Long tk_status) {
        this.tk_status = tk_status;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public Long getJump_type() {
        return jump_type;
    }

    public void setJump_type(Long jump_type) {
        this.jump_type = jump_type;
    }

    public Long getPage_no() {
        return page_no;
    }

    public void setPage_no(Long page_no) {
        this.page_no = page_no;
    }

    public Long getOrder_scene() {
        return order_scene;
    }

    public void setOrder_scene(Long order_scene) {
        this.order_scene = order_scene;
    }

    @Override
    public String toString() {
        return "OrderRequest{" +
            "query_type=" + query_type +
            ", position_index=" + position_index +
            ", page_size=" + page_size +
            ", member_type=" + member_type +
            ", tk_status=" + tk_status +
            ", end_time=" + end_time +
            ", start_time=" + start_time +
            ", jump_type='" + jump_type + '\'' +
            ", page_no=" + page_no +
            ", order_scene=" + order_scene +
            '}';
    }
}