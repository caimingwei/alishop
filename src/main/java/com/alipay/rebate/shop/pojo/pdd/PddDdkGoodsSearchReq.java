package com.alipay.rebate.shop.pojo.pdd;

import java.util.List;

public class PddDdkGoodsSearchReq {

    private List<Integer> activity_tags;
    private Long cat_id;
    private String custom_parameters;
    private List<Long> goods_id_list;
    private Boolean is_brand_goods;
    private String keyword;
    private String list_id;
    private Integer merchant_type;
    private List<Integer> merchant_type_list;
    private Long opt_id;
    private Integer page;
    private Integer page_size;
    private String pid;
    private Long range_from;
    private Integer range_id;
    private Long range_to;
    private Integer sort_type;
    private Boolean with_coupon;

    public List<Integer> getActivity_tags() {
        return activity_tags;
    }

    public void setActivity_tags(List<Integer> activity_tags) {
        this.activity_tags = activity_tags;
    }

    public Long getCat_id() {
        return cat_id;
    }

    public void setCat_id(Long cat_id) {
        this.cat_id = cat_id;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public List<Long> getGoods_id_list() {
        return goods_id_list;
    }

    public void setGoods_id_list(List<Long> goods_id_list) {
        this.goods_id_list = goods_id_list;
    }

    public Boolean getIs_brand_goods() {
        return is_brand_goods;
    }

    public void setIs_brand_goods(Boolean is_brand_goods) {
        this.is_brand_goods = is_brand_goods;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public Integer getMerchant_type() {
        return merchant_type;
    }

    public void setMerchant_type(Integer merchant_type) {
        this.merchant_type = merchant_type;
    }

    public List<Integer> getMerchant_type_list() {
        return merchant_type_list;
    }

    public void setMerchant_type_list(List<Integer> merchant_type_list) {
        this.merchant_type_list = merchant_type_list;
    }

    public Long getOpt_id() {
        return opt_id;
    }

    public void setOpt_id(Long opt_id) {
        this.opt_id = opt_id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public void setPage_size(Integer page_size) {
        this.page_size = page_size;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Long getRange_from() {
        return range_from;
    }

    public void setRange_from(Long range_from) {
        this.range_from = range_from;
    }

    public Integer getRange_id() {
        return range_id;
    }

    public void setRange_id(Integer range_id) {
        this.range_id = range_id;
    }

    public Long getRange_to() {
        return range_to;
    }

    public void setRange_to(Long range_to) {
        this.range_to = range_to;
    }

    public Integer getSort_type() {
        return sort_type;
    }

    public void setSort_type(Integer sort_type) {
        this.sort_type = sort_type;
    }

    public Boolean getWith_coupon() {
        return with_coupon;
    }

    public void setWith_coupon(Boolean with_coupon) {
        this.with_coupon = with_coupon;
    }

    @Override
    public String toString() {
        return "PddGoodsSearchReq{" +
                "activity_tags=" + activity_tags +
                ", cat_id=" + cat_id +
                ", custom_parameters='" + custom_parameters + '\'' +
                ", goods_id_list=" + goods_id_list +
                ", is_brand_goods=" + is_brand_goods +
                ", keyword='" + keyword + '\'' +
                ", list_id='" + list_id + '\'' +
                ", merchant_type=" + merchant_type +
                ", merchant_type_list=" + merchant_type_list +
                ", opt_id=" + opt_id +
                ", page=" + page +
                ", page_size=" + page_size +
                ", pid='" + pid + '\'' +
                ", range_from=" + range_from +
                ", range_id=" + range_id +
                ", range_to=" + range_to +
                ", sort_type=" + sort_type +
                ", with_coupon=" + with_coupon +
                '}';
    }
}
