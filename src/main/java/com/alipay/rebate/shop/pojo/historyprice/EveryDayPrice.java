package com.alipay.rebate.shop.pojo.historyprice;

import java.math.BigDecimal;

public class EveryDayPrice {

    private BigDecimal price;
    private Long time;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "EveryDayPrice{" +
                "price=" + price +
                ", time='" + time + '\'' +
                '}';
    }
}
