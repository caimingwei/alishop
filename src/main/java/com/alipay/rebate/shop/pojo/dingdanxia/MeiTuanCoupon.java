package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;

public class MeiTuanCoupon {

    private String orderid;
    private String sequence;
    private Long usetime;
    private BigDecimal price;
    private BigDecimal profit;
    private String newbuyer;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public void setUsetime(Long usetime) {
        this.usetime = usetime;
    }

    public Long getUsetime() {
        return usetime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public String getNewbuyer() {
        return newbuyer;
    }

    public void setNewbuyer(String newbuyer) {
        this.newbuyer = newbuyer;
    }

    @Override
    public String toString() {
        return "MeiTuanCoupon{" +
                "orderid='" + orderid + '\'' +
                ", sequence='" + sequence + '\'' +
                ", usetime='" + usetime + '\'' +
                ", price='" + price + '\'' +
                ", profit='" + profit + '\'' +
                ", newbuyer='" + newbuyer + '\'' +
                '}';
    }
}
