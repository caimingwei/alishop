package com.alipay.rebate.shop.pojo.alipay;

import java.math.BigDecimal;

public class WithdrawUserInfo {

  private Long userId;
  private BigDecimal userAmount;
  private Long userIntgeralTreasure;
  private String userNickName;
  private Integer withdrawType;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public BigDecimal getUserAmount() {
    return userAmount;
  }

  public void setUserAmount(BigDecimal userAmount) {
    this.userAmount = userAmount;
  }

  public Long getUserIntgeralTreasure() {
    return userIntgeralTreasure;
  }

  public void setUserIntgeralTreasure(Long userIntgeralTreasure) {
    this.userIntgeralTreasure = userIntgeralTreasure;
  }

  public String getUserNickName() {
    return userNickName;
  }

  public void setUserNickName(String userNickName) {
    this.userNickName = userNickName;
  }

  public Integer getWithdrawType() {
    return withdrawType;
  }

  public void setWithdrawType(Integer withdrawType) {
    this.withdrawType = withdrawType;
  }

  @Override
  public String toString() {
    return "WithdrawUserInfo{" +
        "userId=" + userId +
        ", userAmount=" + userAmount +
        ", userIntgeralTreasure=" + userIntgeralTreasure +
        ", userNickName='" + userNickName + '\'' +
        ", withdrawType=" + withdrawType +
        '}';
  }
}
