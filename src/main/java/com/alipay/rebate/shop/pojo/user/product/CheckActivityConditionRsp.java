package com.alipay.rebate.shop.pojo.user.product;


public class CheckActivityConditionRsp {

  private boolean is_allow;
  private String goodsId;
  private String couponClickUrl;
  private String itemUrl;
  private String coupon;
  private Long relationId;
  private Long specialId;
  private String reason;
  private Object taobaoAuthFailRsp;

  public boolean isIs_allow() {
    return is_allow;
  }

  public void setIs_allow(boolean is_allow) {
    this.is_allow = is_allow;
  }

  public String getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(String goodsId) {
    this.goodsId = goodsId;
  }

  public String getCouponClickUrl() {
    return couponClickUrl;
  }

  public void setCouponClickUrl(String couponClickUrl) {
    this.couponClickUrl = couponClickUrl;
  }

  public String getItemUrl() {
    return itemUrl;
  }

  public void setItemUrl(String itemUrl) {
    this.itemUrl = itemUrl;
  }

  public String getCoupon() {
    return coupon;
  }

  public void setCoupon(String coupon) {
    this.coupon = coupon;
  }

  public Long getRelationId() {
    return relationId;
  }

  public void setRelationId(Long relationId) {
    this.relationId = relationId;
  }

  public Long getSpecialId() {
    return specialId;
  }

  public void setSpecialId(Long specialId) {
    this.specialId = specialId;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }


  public Object getTaobaoAuthFailRsp() {
    return taobaoAuthFailRsp;
  }

  public void setTaobaoAuthFailRsp(Object taobaoAuthFailRsp) {
    this.taobaoAuthFailRsp = taobaoAuthFailRsp;
  }

  @Override
  public String toString() {
    return "CheckActivityConditionRsp{" +
            "is_allow=" + is_allow +
            ", goodsId='" + goodsId + '\'' +
            ", couponClickUrl='" + couponClickUrl + '\'' +
            ", itemUrl='" + itemUrl + '\'' +
            ", coupon='" + coupon + '\'' +
            ", relationId=" + relationId +
            ", specialId=" + specialId +
            ", reason='" + reason + '\'' +
            ", taobaoAuthFailRsp=" + taobaoAuthFailRsp +
            '}';
  }
}
