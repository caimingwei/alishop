package com.alipay.rebate.shop.pojo.dingdanxia;

public class QueryJingfenGoodsReq {

    private String apikey;
    private String signature;
    private Integer eliteId;
    private Integer pageIndex;
    private Integer pageSize;
    private String sortName;
    private String sort;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getEliteId() {
        return eliteId;
    }

    public void setEliteId(Integer eliteId) {
        this.eliteId = eliteId;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "QueryJingfenGoodsReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", eliteId=" + eliteId +
                ", pageIndex=" + pageIndex +
                ", pageSize=" + pageSize +
                ", sortName='" + sortName + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }
}
