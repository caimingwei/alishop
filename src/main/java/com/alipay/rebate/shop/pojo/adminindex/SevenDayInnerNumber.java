package com.alipay.rebate.shop.pojo.adminindex;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

public class SevenDayInnerNumber {

    private String date;
    private Integer registerCountOfSevenDayInner;
    private Integer paidOrdersCountOfSevenDayInner;
    private BigDecimal estimateCommissionCountOfSevenDayInner;
    private BigDecimal balanceCommissionCountOfSevenDayInner;
    private BigDecimal pddCommissionFeeCountOfSevenDayInner;

    public BigDecimal getPddCommissionFeeCountOfSevenDayInner() {
        return pddCommissionFeeCountOfSevenDayInner;
    }

    public void setPddCommissionFeeCountOfSevenDayInner(BigDecimal pddCommissionFeeCountOfSevenDayInner) {
        this.pddCommissionFeeCountOfSevenDayInner = pddCommissionFeeCountOfSevenDayInner;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getRegisterCountOfSevenDayInner() {
        return registerCountOfSevenDayInner;
    }

    public void setRegisterCountOfSevenDayInner(Integer registerCountOfSevenDayInner) {
        this.registerCountOfSevenDayInner = registerCountOfSevenDayInner;
    }

    public Integer getPaidOrdersCountOfSevenDayInner() {
        return paidOrdersCountOfSevenDayInner;
    }

    public void setPaidOrdersCountOfSevenDayInner(Integer paidOrdersCountOfSevenDayInner) {
        this.paidOrdersCountOfSevenDayInner = paidOrdersCountOfSevenDayInner;
    }

    public BigDecimal getEstimateCommissionCountOfSevenDayInner() {
        return estimateCommissionCountOfSevenDayInner;
    }

    public void setEstimateCommissionCountOfSevenDayInner(BigDecimal estimateCommissionCountOfSevenDayInner) {
        this.estimateCommissionCountOfSevenDayInner = estimateCommissionCountOfSevenDayInner;
    }

    public BigDecimal getBalanceCommissionCountOfSevenDayInner() {
        return balanceCommissionCountOfSevenDayInner;
    }

    public void setBalanceCommissionCountOfSevenDayInner(BigDecimal balanceCommissionCountOfSevenDayInner) {
        this.balanceCommissionCountOfSevenDayInner = balanceCommissionCountOfSevenDayInner;
    }

    @Override
    public String toString() {
        return "SevenDayInnerNumber{" +
                "date='" + date + '\'' +
                ", registerCountOfSevenDayInner=" + registerCountOfSevenDayInner +
                ", paidOrdersCountOfSevenDayInner=" + paidOrdersCountOfSevenDayInner +
                ", estimateCommissionCountOfSevenDayInner=" + estimateCommissionCountOfSevenDayInner +
                ", balanceCommissionCountOfSevenDayInner=" + balanceCommissionCountOfSevenDayInner +
                ", pddCommissionFeeCountOfSevenDayInner=" + pddCommissionFeeCountOfSevenDayInner +
                '}';
    }
}
