package com.alipay.rebate.shop.pojo.user.admin;

import java.util.List;

public class InviteUserSettingsDto {

  private String ruleHtml;
  private List<String> imgs;

  public String getRuleHtml() {
    return ruleHtml;
  }

  public void setRuleHtml(String ruleHtml) {
    this.ruleHtml = ruleHtml;
  }

  public List<String> getImgs() {
    return imgs;
  }

  public void setImgs(List<String> imgs) {
    this.imgs = imgs;
  }

  @Override
  public String toString() {
    return "InviteUserSettingsDto{" +
        "ruleHtml='" + ruleHtml + '\'' +
        ", imgs=" + imgs +
        '}';
  }
}
