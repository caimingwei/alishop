package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.NotEmpty;

public class UpdateUserNickNameRequest {

  @NotEmpty(message = "用户昵称不能为空")
  private String userNickName;

  public String getUserNickName() {
    return userNickName;
  }

  public void setUserNickName(String userNickName) {
    this.userNickName = userNickName;
  }

  @Override
  public String toString() {
    return "UpdateUserNickNameRequest{" +
        "userNickName='" + userNickName + '\'' +
        '}';
  }
}
