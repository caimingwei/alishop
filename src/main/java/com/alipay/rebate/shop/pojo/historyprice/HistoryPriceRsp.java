package com.alipay.rebate.shop.pojo.historyprice;

import java.math.BigDecimal;
import java.util.List;

public class HistoryPriceRsp {

    private List<EveryDayPrice> data;
    private GoodInfo goodInfo;
    private BigDecimal maxPrice;
    private BigDecimal minPrice;

    public List<EveryDayPrice> getData() {
        return data;
    }

    public void setData(List<EveryDayPrice> data) {
        this.data = data;
    }

    public GoodInfo getGoodInfo() {
        return goodInfo;
    }

    public void setGoodInfo(GoodInfo goodInfo) {
        this.goodInfo = goodInfo;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    @Override
    public String toString() {
        return "HistoryPriceRsp{" +
                "data=" + data +
                ", goodInfo=" + goodInfo +
                ", maxPrice=" + maxPrice +
                ", minPrice=" + minPrice +
                '}';
    }
}
