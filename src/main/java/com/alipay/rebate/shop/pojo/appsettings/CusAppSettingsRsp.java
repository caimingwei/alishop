package com.alipay.rebate.shop.pojo.appsettings;

public class CusAppSettingsRsp {

  private Integer spDomainType;

  private String spDomain;

  public Integer getSpDomainType() {
    return spDomainType;
  }

  public void setSpDomainType(Integer spDomainType) {
    this.spDomainType = spDomainType;
  }

  public String getSpDomain() {
    return spDomain;
  }

  public void setSpDomain(String spDomain) {
    this.spDomain = spDomain;
  }

  @Override
  public String toString() {
    return "CusAppSettingsRsp{" +
        "spDomainType=" + spDomainType +
        ", spDomain='" + spDomain + '\'' +
        '}';
  }
}
