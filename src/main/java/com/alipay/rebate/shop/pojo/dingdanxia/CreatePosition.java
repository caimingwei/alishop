package com.alipay.rebate.shop.pojo.dingdanxia;

public class CreatePosition {

    private Long spaceName;

    public Long getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(Long spaceName) {
        this.spaceName = spaceName;
    }

    @Override
    public String toString() {
        return "CreatePosition{" +
                "spaceName=" + spaceName +
                '}';
    }

}
