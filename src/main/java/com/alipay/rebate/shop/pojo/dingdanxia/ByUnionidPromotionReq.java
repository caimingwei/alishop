package com.alipay.rebate.shop.pojo.dingdanxia;

public class ByUnionidPromotionReq {

    private String apikey;
    private String signature;
    private String materialId;
    private Long unionId;
    private Long positionId;
    private String pid;
    private String couponUrl;
    private String subUnionId;
    private Integer chainType;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public Long getUnionId() {
        return unionId;
    }

    public void setUnionId(Long unionId) {
        this.unionId = unionId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getSubUnionId() {
        return subUnionId;
    }

    public void setSubUnionId(String subUnionId) {
        this.subUnionId = subUnionId;
    }

    public Integer getChainType() {
        return chainType;
    }

    public void setChainType(Integer chainType) {
        this.chainType = chainType;
    }

    @Override
    public String toString() {
        return "ByUnionidPromotionReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", materialId='" + materialId + '\'' +
                ", unionId=" + unionId +
                ", positionId=" + positionId +
                ", pid='" + pid + '\'' +
                ", couponUrl='" + couponUrl + '\'' +
                ", subUnionId='" + subUnionId + '\'' +
                ", chainType=" + chainType +
                '}';
    }
}
