package com.alipay.rebate.shop.pojo.dataoke;

public class DataokeSuperCategoryRep {


    private String appKey;
    private String version;
    private Integer cid;
    private String cname;
    private String cpic;
    private Integer subcid;
    private String scname;
    private String scpic;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCpic() {
        return cpic;
    }

    public void setCpic(String cpic) {
        this.cpic = cpic;
    }

    public Integer getSubcid() {
        return subcid;
    }

    public void setSubcid(Integer subcid) {
        this.subcid = subcid;
    }

    public String getScname() {
        return scname;
    }

    public void setScname(String scname) {
        this.scname = scname;
    }

    public String getScpic() {
        return scpic;
    }

    public void setScpic(String scpic) {
        this.scpic = scpic;
    }

    @Override
    public String toString() {
        return "DataokeSuperCategoryRep{" +
                "appKey='" + appKey + '\'' +
                ", version='" + version + '\'' +
                ", cid=" + cid +
                ", cname='" + cname + '\'' +
                ", cpic='" + cpic + '\'' +
                ", subcid=" + subcid +
                ", scname='" + scname + '\'' +
                ", scpic='" + scpic + '\'' +
                '}';
    }
}
