package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.constants.ProjectConstant;

public class UserWithdrawResponse {

    private String relationId;
    private String specialId;
    private Long userId;
    private Long withdrawUITOnGoing = 0L;
    private Long withdrawUITSettlementing=0L;
    private String withdrawAMOnGoing = ProjectConstant.DEFAULT_BIGDECIMAL_ZERO_STR;
    private String withdrawAMSettlementing = ProjectConstant.DEFAULT_BIGDECIMAL_ZERO_STR;
    private Long userIntgeralTreasure;
    private String userAmount;

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public String getSpecialId() {
        return specialId;
    }

    public void setSpecialId(String specialId) {
        this.specialId = specialId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWithdrawUITOnGoing() {
        return withdrawUITOnGoing;
    }

    public void setWithdrawUITOnGoing(Long withdrawUITOnGoing) {
        this.withdrawUITOnGoing = withdrawUITOnGoing;
    }

    public Long getWithdrawUITSettlementing() {
        return withdrawUITSettlementing;
    }

    public void setWithdrawUITSettlementing(Long withdrawUITSettlementing) {
        this.withdrawUITSettlementing = withdrawUITSettlementing;
    }

    public String getWithdrawAMOnGoing() {
        return withdrawAMOnGoing;
    }

    public void setWithdrawAMOnGoing(String withdrawAMOnGoing) {
        this.withdrawAMOnGoing = withdrawAMOnGoing;
    }

    public String getWithdrawAMSettlementing() {
        return withdrawAMSettlementing;
    }

    public void setWithdrawAMSettlementing(String withdrawAMSettlementing) {
        this.withdrawAMSettlementing = withdrawAMSettlementing;
    }

    public Long getUserIntgeralTreasure() {
        return userIntgeralTreasure;
    }

    public void setUserIntgeralTreasure(Long userIntgeralTreasure) {
        this.userIntgeralTreasure = userIntgeralTreasure;
    }

    public String getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(String userAmount) {
        this.userAmount = userAmount;
    }

    @Override
    public String toString() {
        return "UserWithdrawResponse{" +
                "relationId='" + relationId + '\'' +
                ", specialId='" + specialId + '\'' +
                ", userId=" + userId +
                ", withdrawUITOnGoing=" + withdrawUITOnGoing +
                ", withdrawUITSettlementing=" + withdrawUITSettlementing +
                ", withdrawAMOnGoing='" + withdrawAMOnGoing + '\'' +
                ", withdrawAMSettlementing='" + withdrawAMSettlementing + '\'' +
                ", userIntgeralTreasure=" + userIntgeralTreasure +
                ", userAmount='" + userAmount + '\'' +
                '}';
    }
}
