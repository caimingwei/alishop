package com.alipay.rebate.shop.pojo.entitysettings;

import java.math.BigDecimal;

public class IrUser implements Comparable<IrUser>{

  private String userName;
  private String headImage;
  private BigDecimal totalIncome;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getHeadImage() {
    return headImage;
  }

  public void setHeadImage(String headImage) {
    this.headImage = headImage;
  }

  public BigDecimal getTotalIncome() {
    return totalIncome;
  }

  public void setTotalIncome(BigDecimal totalIncome) {
    this.totalIncome = totalIncome;
  }

  @Override
  public int compareTo(IrUser o) {
    return totalIncome.compareTo(o.getTotalIncome());
  }

  @Override
  public String toString() {
    return "IrUser{" +
        "userName='" + userName + '\'' +
        ", headImage='" + headImage + '\'' +
        ", totalIncome=" + totalIncome +
        '}';
  }
}
