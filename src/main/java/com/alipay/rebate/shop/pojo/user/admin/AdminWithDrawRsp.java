package com.alipay.rebate.shop.pojo.user.admin;


import java.math.BigDecimal;

public class AdminWithDrawRsp {


    private Long withDrawId;

    private Long userId;

    private String userNickName;

    private BigDecimal intgeralNum;

    //提现状态
    private Integer type;

    //提现类型
    private Integer status;

    //提现申请时间
    private String createTime;

    //提现处理时间
    private String updateTime;

    public BigDecimal getIntgeralNum() {
        return intgeralNum;
    }

    public void setIntgeralNum(BigDecimal intgeralNum) {
        this.intgeralNum = intgeralNum;
    }

    public Long getWithDrawId() {
        return withDrawId;
    }

    public void setWithDrawId(Long withDrawId) {
        this.withDrawId = withDrawId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AdminWithDrawRsp{" +
                "withDrawId=" + withDrawId +
                ", userId=" + userId +
                ", userNickName='" + userNickName + '\'' +
                ", intgeralNum=" + intgeralNum +
                ", type=" + type +
                ", status=" + status +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
