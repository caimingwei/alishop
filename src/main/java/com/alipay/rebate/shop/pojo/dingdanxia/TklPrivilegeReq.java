package com.alipay.rebate.shop.pojo.dingdanxia;

public class TklPrivilegeReq {

  private String signature;
  private String tkl;
  private String pid;
  private Number relation_id;
  private String tpwd;
  private String activityId;
  private String logo;
  private String text;
  private Boolean itemInfo;
  private Boolean extspk;
  private String left_symbol;
  private String right_symbol;
  private Boolean shorturl;

  private String tb_auth_id;

  public String getTb_auth_id() {
    return tb_auth_id;
  }

  public void setTb_auth_id(String tb_auth_id) {
    this.tb_auth_id = tb_auth_id;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public String getTkl() {
    return tkl;
  }

  public void setTkl(String tkl) {
    this.tkl = tkl;
  }

  public String getPid() {
    return pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  public Number getRelation_id() {
    return relation_id;
  }

  public void setRelation_id(Number relation_id) {
    this.relation_id = relation_id;
  }

  public String getTpwd() {
    return tpwd;
  }

  public void setTpwd(String tpwd) {
    this.tpwd = tpwd;
  }

  public String getActivityId() {
    return activityId;
  }

  public void setActivityId(String activityId) {
    this.activityId = activityId;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Boolean getItemInfo() {
    return itemInfo;
  }

  public void setItemInfo(Boolean itemInfo) {
    this.itemInfo = itemInfo;
  }

  public Boolean getExtspk() {
    return extspk;
  }

  public void setExtspk(Boolean extspk) {
    this.extspk = extspk;
  }

  public String getLeft_symbol() {
    return left_symbol;
  }

  public void setLeft_symbol(String left_symbol) {
    this.left_symbol = left_symbol;
  }

  public String getRight_symbol() {
    return right_symbol;
  }

  public void setRight_symbol(String right_symbol) {
    this.right_symbol = right_symbol;
  }

  public Boolean getShorturl() {
    return shorturl;
  }

  public void setShorturl(Boolean shorturl) {
    this.shorturl = shorturl;
  }

  @Override
  public String toString() {
    return "TklPrivilegeReq{" +
        "signature='" + signature + '\'' +
        ", tkl='" + tkl + '\'' +
        ", pid='" + pid + '\'' +
        ", relation_id=" + relation_id +
        ", tpwd='" + tpwd + '\'' +
        ", activityId='" + activityId + '\'' +
        ", logo='" + logo + '\'' +
        ", text='" + text + '\'' +
        ", itemInfo=" + itemInfo +
        ", extspk=" + extspk +
        ", left_symbol='" + left_symbol + '\'' +
        ", right_symbol='" + right_symbol + '\'' +
        ", shorturl=" + shorturl +
        '}';
  }
}
