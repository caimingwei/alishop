package com.alipay.rebate.shop.pojo.user.customer;

import java.math.BigDecimal;
import java.util.Date;

public class UserCouponRsp {

  private Long id;
  private String name;
  private BigDecimal amountOfMoney;
  private BigDecimal conditionMoney;
  private Integer conditionType;
  private Integer equalintegral;
  private Integer type;
  private Integer isUse;
  private Date startTime;
  private Date endTime;
  private Date createTime;
  private String remark;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getAmountOfMoney() {
    return amountOfMoney;
  }

  public void setAmountOfMoney(BigDecimal amountOfMoney) {
    this.amountOfMoney = amountOfMoney;
  }

  public BigDecimal getConditionMoney() {
    return conditionMoney;
  }

  public void setConditionMoney(BigDecimal conditionMoney) {
    this.conditionMoney = conditionMoney;
  }

  public Integer getConditionType() {
    return conditionType;
  }

  public void setConditionType(Integer conditionType) {
    this.conditionType = conditionType;
  }

  public Integer getEqualintegral() {
    return equalintegral;
  }

  public void setEqualintegral(Integer equalintegral) {
    this.equalintegral = equalintegral;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Integer getIsUse() {
    return isUse;
  }

  public void setIsUse(Integer isUse) {
    this.isUse = isUse;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  @Override
  public String toString() {
    return "UserCouponRsp{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", amountOfMoney=" + amountOfMoney +
        ", conditionMoney=" + conditionMoney +
        ", conditionType=" + conditionType +
        ", equalintegral=" + equalintegral +
        ", type=" + type +
        ", isUse=" + isUse +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", createTime=" + createTime +
        ", remark='" + remark + '\'' +
        '}';
  }
}
