package com.alipay.rebate.shop.pojo.entitysettings;

import java.util.List;

public class InviteNumRankSettings {

  private String startTime;
  private String endTime;
  private Integer realUserNum;
  private List<InrUser> inrUsers;

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public Integer getRealUserNum() {
    return realUserNum;
  }

  public void setRealUserNum(Integer realUserNum) {
    this.realUserNum = realUserNum;
  }

  public List<InrUser> getInrUsers() {
    return inrUsers;
  }

  public void setInrUsers(List<InrUser> inrUsers) {
    this.inrUsers = inrUsers;
  }

  @Override
  public String toString() {
    return "InviteNumRankSettings{" +
        "startTime='" + startTime + '\'' +
        ", endTime='" + endTime + '\'' +
        ", realUserNum=" + realUserNum +
        ", inrUsers=" + inrUsers +
        '}';
  }
}
