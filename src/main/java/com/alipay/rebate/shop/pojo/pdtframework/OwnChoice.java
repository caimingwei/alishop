package com.alipay.rebate.shop.pojo.pdtframework;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class OwnChoice {

  private List<
      @Min(value = 1, message = "cid范围为1-14")
      @Max(value = 14, message = "cid范围为1-14")
      Integer> category;

  @JsonIgnore
  @Min(value = 0, message = "sort范围为0-8")
  @Max(value = 8, message = "sort范围为0-8")
  Integer sort;

  public List<Integer> getCategory() {
    return category;
  }

  public void setCategory(List<Integer> category) {
    this.category = category;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  @Override
  public String toString() {
    return "OwnChoice{" +
        "category=" + category +
        ", sort=" + sort +
        '}';
  }
}
