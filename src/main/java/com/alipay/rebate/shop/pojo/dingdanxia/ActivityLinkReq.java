package com.alipay.rebate.shop.pojo.dingdanxia;

public class ActivityLinkReq {

    private String apikey;
    private String signature;
    private String promotion_scene_id;
    private String relation_id;
    private String union_id;
    private Integer platform;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPromotion_scene_id() {
        return promotion_scene_id;
    }

    public void setPromotion_scene_id(String promotion_scene_id) {
        this.promotion_scene_id = promotion_scene_id;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public String getUnion_id() {
        return union_id;
    }

    public void setUnion_id(String union_id) {
        this.union_id = union_id;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "ActivityLinkReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", promotion_scene_id='" + promotion_scene_id + '\'' +
                ", relation_id='" + relation_id + '\'' +
                ", union_id='" + union_id + '\'' +
                ", platform=" + platform +
                '}';
    }
}
