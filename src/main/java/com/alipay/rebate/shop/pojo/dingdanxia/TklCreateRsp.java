package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.Map;

public class TklCreateRsp {

  private String request_id;
  private Map<String,String> data;

  public String getRequest_id() {
    return request_id;
  }

  public void setRequest_id(String request_id) {
    this.request_id = request_id;
  }

  public Map<String, String> getData() {
    return data;
  }

  public void setData(Map<String, String> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "TklCreateRsp{" +
        "request_id='" + request_id + '\'' +
        ", data=" + data +
        '}';
  }
}
