package com.alipay.rebate.shop.pojo.dingdanxia;

public class PddUrlConvertReq {

    private String apikey;
    private String signature;
    private String source_url;
    private String pid;
    private Boolean generate_schema_url;
    private String custom_parameters;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Boolean getGenerate_schema_url() {
        return generate_schema_url;
    }

    public void setGenerate_schema_url(Boolean generate_schema_url) {
        this.generate_schema_url = generate_schema_url;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    @Override
    public String toString() {
        return "PddUrlConvertReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", source_url='" + source_url + '\'' +
                ", pid='" + pid + '\'' +
                ", generate_schema_url=" + generate_schema_url +
                ", custom_parameters='" + custom_parameters + '\'' +
                '}';
    }
}
