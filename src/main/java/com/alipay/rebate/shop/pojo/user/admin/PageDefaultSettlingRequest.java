package com.alipay.rebate.shop.pojo.user.admin;

import javax.validation.constraints.NotNull;

public class PageDefaultSettlingRequest {

  @NotNull(message = "页面模板id不能为空")
  private Long pageId;
  @NotNull(message = "必须指定是否为默认值")
  private Integer isDefault;

  public Long getPageId() {
    return pageId;
  }

  public void setPageId(Long pageId) {
    this.pageId = pageId;
  }

  public Integer getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Integer isDefault) {
    this.isDefault = isDefault;
  }

  @Override
  public String toString() {
    return "PageDefaultSettlingRequest{" +
        "pageId=" + pageId +
        ", isDefault=" + isDefault +
        '}';
  }
}
