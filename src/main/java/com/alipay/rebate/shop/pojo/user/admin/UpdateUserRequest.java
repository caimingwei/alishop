package com.alipay.rebate.shop.pojo.user.admin;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;

//TODO(格式验证)
public class UpdateUserRequest {

  // 用户id
  @NotNull(message = "用户id不能为空")
  private Long userId;
  // 会员名
  private String userNickName;
  // 用户姓名
  private String realName;
  // 支付宝账号
  private String alipayAccount;
  // 注册时间
  private Long userRegisterTime;
  // 注册ip
  private String userRegisterIp;
  // 余额
  private BigDecimal userAmount;
  // 集分宝总额
  private Long userIntgeralTreasure;
  // 积分
  private Long userIntgeral;
  // 上次登陆时间
  private Long lastLoginTime;
  // 电话
  private String phone;
  // 密码
  private String userPassword;
  // 用户状态
  private Integer userStatus;

  private Integer userGradeId;

  //推荐人id
  private Long parentUserId;

  private String password;

  public Integer getUserGradeId() {
    return userGradeId;
  }

  public void setUserGradeId(Integer userGradeId) {
    this.userGradeId = userGradeId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserNickName() {
    return userNickName;
  }

  public void setUserNickName(String userNickName) {
    this.userNickName = userNickName;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public String getAlipayAccount() {
    return alipayAccount;
  }

  public void setAlipayAccount(String alipayAccount) {
    this.alipayAccount = alipayAccount;
  }

  public Long getUserRegisterTime() {
    return userRegisterTime;
  }

  public void setUserRegisterTime(Long userRegisterTime) {
    this.userRegisterTime = userRegisterTime;
  }

  public String getUserRegisterIp() {
    return userRegisterIp;
  }

  public void setUserRegisterIp(String userRegisterIp) {
    this.userRegisterIp = userRegisterIp;
  }

  public BigDecimal getUserAmount() {
    return userAmount;
  }

  public void setUserAmount(BigDecimal userAmount) {
    this.userAmount = userAmount;
  }

  public Long getUserIntgeralTreasure() {
    return userIntgeralTreasure;
  }

  public void setUserIntgeralTreasure(Long userIntgeralTreasure) {
    this.userIntgeralTreasure = userIntgeralTreasure;
  }

  public Long getUserIntgeral() {
    return userIntgeral;
  }

  public void setUserIntgeral(Long userIntgeral) {
    this.userIntgeral = userIntgeral;
  }

  public Long getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(Long lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getUserPassword() {
    return userPassword;
  }

  public void setUserPassword(String userPassword) {
    this.userPassword = userPassword;
  }

  public Integer getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(Integer userStatus) {
    this.userStatus = userStatus;
  }

  public Long getParentUserId() {
    return parentUserId;
  }

  public void setParentUserId(Long parentUserId) {
    this.parentUserId = parentUserId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return "UpdateUserRequest{" +
        "userId=" + userId +
        ", userNickName='" + userNickName + '\'' +
        ", realName='" + realName + '\'' +
        ", alipayAccount='" + alipayAccount + '\'' +
        ", userRegisterTime=" + userRegisterTime +
        ", userRegisterIp='" + userRegisterIp + '\'' +
        ", userAmount=" + userAmount +
        ", userIntgeralTreasure=" + userIntgeralTreasure +
        ", userIntgeral=" + userIntgeral +
        ", lastLoginTime=" + lastLoginTime +
        ", phone='" + phone + '\'' +
        ", userPassword='" + userPassword + '\'' +
        ", userStatus=" + userStatus +
        ", parentUserId=" + parentUserId +
        ", password=" + password +
        '}';
  }
}
