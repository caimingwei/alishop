package com.alipay.rebate.shop.pojo.appsettings;

public class PrivacyPolicy {

    private String privacyPolicy;
    private String useAgreement;

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public String getUseAgreement() {
        return useAgreement;
    }

    public void setUseAgreement(String useAgreement) {
        this.useAgreement = useAgreement;
    }

    @Override
    public String toString() {
        return "PtivacyPolicy{" +
                "privacyPolicy='" + privacyPolicy + '\'' +
                ", useAgreement='" + useAgreement + '\'' +
                '}';
    }
}
