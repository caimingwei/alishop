package com.alipay.rebate.shop.pojo.userincome;

import java.math.BigDecimal;

public class IncomeSchedulerRsp {

  private Long userIncomeId;

  private Long userId;

  private BigDecimal money;

  private String paidTime;

  private String tradeId;

  private Integer type;

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Long getUserIncomeId() {
    return userIncomeId;
  }

  public void setUserIncomeId(Long userIncomeId) {
    this.userIncomeId = userIncomeId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public BigDecimal getMoney() {
    return money;
  }

  public void setMoney(BigDecimal money) {
    this.money = money;
  }

  public String getPaidTime() {
    return paidTime;
  }

  public void setPaidTime(String paidTime) {
    this.paidTime = paidTime;
  }

  public String getTradeId() {
    return tradeId;
  }

  public void setTradeId(String tradeId) {
    this.tradeId = tradeId;
  }

  @Override
  public String toString() {
    return "IncomeSchedulerRsp{" +
            "userIncomeId=" + userIncomeId +
            ", userId=" + userId +
            ", money=" + money +
            ", paidTime='" + paidTime + '\'' +
            ", tradeId='" + tradeId + '\'' +
            ", type=" + type +
            '}';
  }
}
