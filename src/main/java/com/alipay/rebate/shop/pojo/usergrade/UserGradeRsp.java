package com.alipay.rebate.shop.pojo.usergrade;

import com.alipay.rebate.shop.model.CommissionLevelSettings;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;

public class UserGradeRsp {

  private Integer id;
  private String gradeName;
  private Double gradeWeight;
  private Double firstCommission;
  private Double secondCommission;
  private Double thirdCommission;
  private Integer enableCmlevelSettings;
  private Date createTime;
  private Date updateTime;
  private List<CommissionLevelSettings> levelSettings;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getGradeName() {
    return gradeName;
  }

  public void setGradeName(String gradeName) {
    this.gradeName = gradeName;
  }

  public Double getGradeWeight() {
    return gradeWeight;
  }

  public void setGradeWeight(Double gradeWeight) {
    this.gradeWeight = gradeWeight;
  }

  public Double getFirstCommission() {
    return firstCommission;
  }

  public void setFirstCommission(Double firstCommission) {
    this.firstCommission = firstCommission;
  }

  public Double getSecondCommission() {
    return secondCommission;
  }

  public void setSecondCommission(Double secondCommission) {
    this.secondCommission = secondCommission;
  }

  public Double getThirdCommission() {
    return thirdCommission;
  }

  public void setThirdCommission(Double thirdCommission) {
    this.thirdCommission = thirdCommission;
  }

  public Integer getEnableCmlevelSettings() {
    return enableCmlevelSettings;
  }

  public void setEnableCmlevelSettings(Integer enableCmlevelSettings) {
    this.enableCmlevelSettings = enableCmlevelSettings;
  }

  public List<CommissionLevelSettings> getLevelSettings() {
    return levelSettings;
  }

  public void setLevelSettings(
      List<CommissionLevelSettings> levelSettings) {
    this.levelSettings = levelSettings;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

  @Override
  public String toString() {
    return "UserGradeRsp{" +
        "id=" + id +
        ", gradeName='" + gradeName + '\'' +
        ", gradeWeight=" + gradeWeight +
        ", firstCommission=" + firstCommission +
        ", secondCommission=" + secondCommission +
        ", thirdCommission=" + thirdCommission +
        ", enableCmlevelSettings=" + enableCmlevelSettings +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", levelSettings=" + levelSettings +
        '}';
  }
}
