package com.alipay.rebate.shop.pojo.user.customer;


import javax.validation.constraints.NotEmpty;

public class UpdateAlipayAccountRequest {

  @NotEmpty(message = "验证码能为空")
  private String mobileCode;
  @NotEmpty(message = "真实姓名不能为空")
  private String realName;
  @NotEmpty(message = "支付宝账号不能为空")
  private String alipayAccount;

  public String getMobileCode() {
    return mobileCode;
  }

  public void setMobileCode(String mobileCode) {
    this.mobileCode = mobileCode;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public String getAlipayAccount() {
    return alipayAccount;
  }

  public void setAlipayAccount(String alipayAccount) {
    this.alipayAccount = alipayAccount;
  }

  @Override
  public String toString() {
    return "UpdateAlipayAccountRequest{" +
        "mobileCode='" + mobileCode + '\'' +
        ", realName='" + realName + '\'' +
        ", alipayAccount='" + alipayAccount + '\'' +
        '}';
  }
}
