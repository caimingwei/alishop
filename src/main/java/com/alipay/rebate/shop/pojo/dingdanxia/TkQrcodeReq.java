package com.alipay.rebate.shop.pojo.dingdanxia;

public class TkQrcodeReq {

  private String apikey;
  private String signature;
  private Long item_id;
  private Boolean qrcode;
  private Long relation_id;
  private String pid;

  public String getApikey() {
    return apikey;
  }

  public void setApikey(String apikey) {
    this.apikey = apikey;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public Long getItem_id() {
    return item_id;
  }

  public void setItem_id(Long item_id) {
    this.item_id = item_id;
  }

  public Boolean getQrcode() {
    return qrcode;
  }

  public void setQrcode(Boolean qrcode) {
    this.qrcode = qrcode;
  }

  public Long getRelation_id() {
    return relation_id;
  }

  public void setRelation_id(Long relation_id) {
    this.relation_id = relation_id;
  }

  public String getPid() {
    return pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  @Override
  public String toString() {
    return "TkQrcodeReq{" +
        "apikey='" + apikey + '\'' +
        ", signature='" + signature + '\'' +
        ", item_id=" + item_id +
        ", qrcode=" + qrcode +
        ", relation_id=" + relation_id +
        ", pid='" + pid + '\'' +
        '}';
  }
}
