package com.alipay.rebate.shop.pojo.dingdanxia;

import java.math.BigDecimal;

public class DingdanxiaQueryGoodsReq {

    private String apikey;
    private String signature;
    private Long cid1;
    private Long cid2;
    private Long cid3;
    private Integer pageIndex;
    private Integer pageSize;
    private String skuIds;
    private String keyword;
    private BigDecimal pricefrom;
    private BigDecimal priceto;
    private Integer commissionShareStart;
    private Integer commissionShareEnd;
    private String owner;
    private String sortName;
    private String sort;
    private Integer isCoupon;
    private Integer isPG;
    private BigDecimal pingouPriceStart;
    private BigDecimal pingouPriceEnd;
    private Integer isHot;
    private String brandCode;
    private Integer shopId;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getCid1() {
        return cid1;
    }

    public void setCid1(Long cid1) {
        this.cid1 = cid1;
    }

    public Long getCid2() {
        return cid2;
    }

    public void setCid2(Long cid2) {
        this.cid2 = cid2;
    }

    public Long getCid3() {
        return cid3;
    }

    public void setCid3(Long cid3) {
        this.cid3 = cid3;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(String skuIds) {
        this.skuIds = skuIds;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public BigDecimal getPricefrom() {
        return pricefrom;
    }

    public void setPricefrom(BigDecimal pricefrom) {
        this.pricefrom = pricefrom;
    }

    public BigDecimal getPriceto() {
        return priceto;
    }

    public void setPriceto(BigDecimal priceto) {
        this.priceto = priceto;
    }

    public Integer getCommissionShareStart() {
        return commissionShareStart;
    }

    public void setCommissionShareStart(Integer commissionShareStart) {
        this.commissionShareStart = commissionShareStart;
    }

    public Integer getCommissionShareEnd() {
        return commissionShareEnd;
    }

    public void setCommissionShareEnd(Integer commissionShareEnd) {
        this.commissionShareEnd = commissionShareEnd;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(Integer isCoupon) {
        this.isCoupon = isCoupon;
    }

    public Integer getIsPG() {
        return isPG;
    }

    public void setIsPG(Integer isPG) {
        this.isPG = isPG;
    }

    public BigDecimal getPingouPriceStart() {
        return pingouPriceStart;
    }

    public void setPingouPriceStart(BigDecimal pingouPriceStart) {
        this.pingouPriceStart = pingouPriceStart;
    }

    public BigDecimal getPingouPriceEnd() {
        return pingouPriceEnd;
    }

    public void setPingouPriceEnd(BigDecimal pingouPriceEnd) {
        this.pingouPriceEnd = pingouPriceEnd;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    public String toString() {
        return "DingdanxiaQueryGoodsReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", cid1=" + cid1 +
                ", cid2=" + cid2 +
                ", cid3=" + cid3 +
                ", pageIndex=" + pageIndex +
                ", pageSize=" + pageSize +
                ", skuIds=" + skuIds +
                ", keyword='" + keyword + '\'' +
                ", pricefrom=" + pricefrom +
                ", priceto=" + priceto +
                ", commissionShareStart=" + commissionShareStart +
                ", commissionShareEnd=" + commissionShareEnd +
                ", owner='" + owner + '\'' +
                ", sortName='" + sortName + '\'' +
                ", sort='" + sort + '\'' +
                ", isCoupon=" + isCoupon +
                ", isPG=" + isPG +
                ", pingouPriceStart=" + pingouPriceStart +
                ", pingouPriceEnd=" + pingouPriceEnd +
                ", isHot=" + isHot +
                ", brandCode='" + brandCode + '\'' +
                ", shopId=" + shopId +
                '}';
    }
}
