package com.alipay.rebate.shop.pojo.pdd;

public class PddDdkGoodsZsUnitUrlGenReq {

    private String pid;
    private String source_url;
    private String custom_parameters;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    @Override
    public String toString() {
        return "PddDdkGoodsZsUnitUrlGenReq{" +
                "pid='" + pid + '\'' +
                ", source_url='" + source_url + '\'' +
                ", custom_parameters='" + custom_parameters + '\'' +
                '}';
    }
}
