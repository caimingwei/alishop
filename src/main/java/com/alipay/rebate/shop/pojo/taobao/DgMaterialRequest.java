package com.alipay.rebate.shop.pojo.taobao;

/**
 * 通用物料
 */
public class DgMaterialRequest {

    private Long start_dsr;
    private Long page_size;
    private Long page_no;
    private Long platform;
    private Long end_tk_rate;
    private Long start_tk_rate;
    private Long end_price;
    private Long start_price;
    private Boolean is_overseas;
    private Boolean is_tmall;
    private String sort;
    private String itemloc;
    private String cat;
    private String q;
    private Long material_id;
    private Boolean has_coupon;
    private String ip;
    private Long adzone_id;
    private Boolean need_free_shipmen;
    private Boolean need_prepay;
    private Boolean include_pay_rate_30;
    private Boolean include_good_rate;
    private Boolean include_rfd_rate;
    private Long npx_level;
    private Long end_ka_tk_rate;
    private Long start_ka_tk_rate;
    private String device_encrypt;
    private String device_value;
    private String device_type;


    public Long getStart_dsr() {
        return start_dsr;
    }

    public void setStart_dsr(Long start_dsr) {
        this.start_dsr = start_dsr;
    }

    public Long getPage_size() {
        return page_size;
    }

    public void setPage_size(Long page_size) {
        this.page_size = page_size;
    }

    public Long getPage_no() {
        return page_no;
    }

    public void setPage_no(Long page_no) {
        this.page_no = page_no;
    }

    public Long getPlatform() {
        return platform;
    }

    public void setPlatform(Long platform) {
        this.platform = platform;
    }

    public Long getEnd_tk_rate() {
        return end_tk_rate;
    }

    public void setEnd_tk_rate(Long end_tk_rate) {
        this.end_tk_rate = end_tk_rate;
    }

    public Long getStart_tk_rate() {
        return start_tk_rate;
    }

    public void setStart_tk_rate(Long start_tk_rate) {
        this.start_tk_rate = start_tk_rate;
    }

    public Long getEnd_price() {
        return end_price;
    }

    public void setEnd_price(Long end_price) {
        this.end_price = end_price;
    }

    public Long getStart_price() {
        return start_price;
    }

    public void setStart_price(Long start_price) {
        this.start_price = start_price;
    }

    public Boolean getIs_overseas() {
        return is_overseas;
    }

    public void setIs_overseas(Boolean is_overseas) {
        this.is_overseas = is_overseas;
    }

    public Boolean getIs_tmall() {
        return is_tmall;
    }

    public void setIs_tmall(Boolean is_tmall) {
        this.is_tmall = is_tmall;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getItemloc() {
        return itemloc;
    }

    public void setItemloc(String itemloc) {
        this.itemloc = itemloc;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public Long getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Long material_id) {
        this.material_id = material_id;
    }

    public Boolean getHas_coupon() {
        return has_coupon;
    }

    public void setHas_coupon(Boolean has_coupon) {
        this.has_coupon = has_coupon;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getAdzone_id() {
        return adzone_id;
    }

    public void setAdzone_id(Long adzone_id) {
        this.adzone_id = adzone_id;
    }

    public Boolean getNeed_free_shipmen() {
        return need_free_shipmen;
    }

    public void setNeed_free_shipmen(Boolean need_free_shipmen) {
        this.need_free_shipmen = need_free_shipmen;
    }

    public Boolean getNeed_prepay() {
        return need_prepay;
    }

    public void setNeed_prepay(Boolean need_prepay) {
        this.need_prepay = need_prepay;
    }

    public Boolean getInclude_pay_rate_30() {
        return include_pay_rate_30;
    }

    public void setInclude_pay_rate_30(Boolean include_pay_rate_30) {
        this.include_pay_rate_30 = include_pay_rate_30;
    }

    public Boolean getInclude_good_rate() {
        return include_good_rate;
    }

    public void setInclude_good_rate(Boolean include_good_rate) {
        this.include_good_rate = include_good_rate;
    }

    public Boolean getInclude_rfd_rate() {
        return include_rfd_rate;
    }

    public void setInclude_rfd_rate(Boolean include_rfd_rate) {
        this.include_rfd_rate = include_rfd_rate;
    }

    public Long getNpx_level() {
        return npx_level;
    }

    public void setNpx_level(Long npx_level) {
        this.npx_level = npx_level;
    }

    public Long getEnd_ka_tk_rate() {
        return end_ka_tk_rate;
    }

    public void setEnd_ka_tk_rate(Long end_ka_tk_rate) {
        this.end_ka_tk_rate = end_ka_tk_rate;
    }

    public Long getStart_ka_tk_rate() {
        return start_ka_tk_rate;
    }

    public void setStart_ka_tk_rate(Long start_ka_tk_rate) {
        this.start_ka_tk_rate = start_ka_tk_rate;
    }

    public String getDevice_encrypt() {
        return device_encrypt;
    }

    public void setDevice_encrypt(String device_encrypt) {
        this.device_encrypt = device_encrypt;
    }

    public String getDevice_value() {
        return device_value;
    }

    public void setDevice_value(String device_value) {
        this.device_value = device_value;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    @Override
    public String toString() {
        return "DgMaterialRequest{" +
                "start_dsr=" + start_dsr +
                ", age_size=" + page_size +
                ", page_no=" + page_no +
                ", platform=" + platform +
                ", end_tk_rate=" + end_tk_rate +
                ", start_tk_rate=" + start_tk_rate +
                ", end_price=" + end_price +
                ", start_price=" + start_price +
                ", is_overseas=" + is_overseas +
                ", is_tmall=" + is_tmall +
                ", sort='" + sort + '\'' +
                ", itemloc='" + itemloc + '\'' +
                ", cat='" + cat + '\'' +
                ", q='" + q + '\'' +
                ", material_id=" + material_id +
                ", has_coupon=" + has_coupon +
                ", ip='" + ip + '\'' +
                ", adzone_id=" + adzone_id +
                ", need_free_shipmen=" + need_free_shipmen +
                ", need_prepay=" + need_prepay +
                ", include_pay_rate_30=" + include_pay_rate_30 +
                ", include_good_rate=" + include_good_rate +
                ", include_rfd_rate=" + include_rfd_rate +
                ", npx_level=" + npx_level +
                ", end_ka_tk_rate=" + end_ka_tk_rate +
                ", start_ka_tk_rate=" + start_ka_tk_rate +
                ", device_encrypt='" + device_encrypt + '\'' +
                ", device_value='" + device_value + '\'' +
                ", device_type='" + device_type + '\'' +
                '}';
    }
}
