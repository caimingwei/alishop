package com.alipay.rebate.shop.pojo.dataoke;

import java.math.BigDecimal;

public class DataokeProductSearchReq {

  private String appKey;
  private String version;
  private Integer pageSize;
  private String pageId;
  private String keyWords;
  private String cids;
  private Integer subcid;
  private Integer juHuaSuan;
  private Integer taoQiangGou;
  private Integer tmall;
  private Integer tchaoshi;
  private Integer goldSeller;
  private Integer haitao;
  private Integer brand;
  private String brandIds;
  private BigDecimal priceLowerLimit;
  private BigDecimal priceUpperLimit;
  private BigDecimal couponPriceLowerLimit;
  private BigDecimal commissionRateLowerLimit;
  private BigDecimal monthSalesLowerLimit;
  private String sort;

  public String getAppKey() {
    return appKey;
  }

  public void setAppKey(String appKey) {
    this.appKey = appKey;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public String getKeyWords() {
    return keyWords;
  }

  public void setKeyWords(String keyWords) {
    this.keyWords = keyWords;
  }

  public String getCids() {
    return cids;
  }

  public void setCids(String cids) {
    this.cids = cids;
  }

  public Integer getSubcid() {
    return subcid;
  }

  public void setSubcid(Integer subcid) {
    this.subcid = subcid;
  }

  public Integer getJuHuaSuan() {
    return juHuaSuan;
  }

  public void setJuHuaSuan(Integer juHuaSuan) {
    this.juHuaSuan = juHuaSuan;
  }

  public Integer getTaoQiangGou() {
    return taoQiangGou;
  }

  public void setTaoQiangGou(Integer taoQiangGou) {
    this.taoQiangGou = taoQiangGou;
  }

  public Integer getTmall() {
    return tmall;
  }

  public void setTmall(Integer tmall) {
    this.tmall = tmall;
  }

  public Integer getTchaoshi() {
    return tchaoshi;
  }

  public void setTchaoshi(Integer tchaoshi) {
    this.tchaoshi = tchaoshi;
  }

  public Integer getGoldSeller() {
    return goldSeller;
  }

  public void setGoldSeller(Integer goldSeller) {
    this.goldSeller = goldSeller;
  }

  public Integer getHaitao() {
    return haitao;
  }

  public void setHaitao(Integer haitao) {
    this.haitao = haitao;
  }

  public Integer getBrand() {
    return brand;
  }

  public void setBrand(Integer brand) {
    this.brand = brand;
  }

  public String getBrandIds() {
    return brandIds;
  }

  public void setBrandIds(String brandIds) {
    this.brandIds = brandIds;
  }

  public BigDecimal getPriceLowerLimit() {
    return priceLowerLimit;
  }

  public void setPriceLowerLimit(BigDecimal priceLowerLimit) {
    this.priceLowerLimit = priceLowerLimit;
  }

  public BigDecimal getPriceUpperLimit() {
    return priceUpperLimit;
  }

  public void setPriceUpperLimit(BigDecimal priceUpperLimit) {
    this.priceUpperLimit = priceUpperLimit;
  }

  public BigDecimal getCouponPriceLowerLimit() {
    return couponPriceLowerLimit;
  }

  public void setCouponPriceLowerLimit(BigDecimal couponPriceLowerLimit) {
    this.couponPriceLowerLimit = couponPriceLowerLimit;
  }

  public BigDecimal getCommissionRateLowerLimit() {
    return commissionRateLowerLimit;
  }

  public void setCommissionRateLowerLimit(BigDecimal commissionRateLowerLimit) {
    this.commissionRateLowerLimit = commissionRateLowerLimit;
  }

  public BigDecimal getMonthSalesLowerLimit() {
    return monthSalesLowerLimit;
  }

  public void setMonthSalesLowerLimit(BigDecimal monthSalesLowerLimit) {
    this.monthSalesLowerLimit = monthSalesLowerLimit;
  }

  public String getSort() {
    return sort;
  }

  public void setSort(String sort) {
    this.sort = sort;
  }

  @Override
  public String toString() {
    return "DataokeProductSearchReq{" +
        "appKey='" + appKey + '\'' +
        ", version='" + version + '\'' +
        ", pageSize=" + pageSize +
        ", pageId='" + pageId + '\'' +
        ", keyWords='" + keyWords + '\'' +
        ", cids='" + cids + '\'' +
        ", subcid=" + subcid +
        ", juHuaSuan=" + juHuaSuan +
        ", taoQiangGou=" + taoQiangGou +
        ", tmall=" + tmall +
        ", tchaoshi=" + tchaoshi +
        ", goldSeller=" + goldSeller +
        ", haitao=" + haitao +
        ", brand=" + brand +
        ", brandIds='" + brandIds + '\'' +
        ", priceLowerLimit=" + priceLowerLimit +
        ", priceUpperLimit=" + priceUpperLimit +
        ", couponPriceLowerLimit=" + couponPriceLowerLimit +
        ", commissionRateLowerLimit=" + commissionRateLowerLimit +
        ", monthSalesLowerLimit=" + monthSalesLowerLimit +
        ", sort='" + sort + '\'' +
        '}';
  }
}
