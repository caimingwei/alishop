package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.NotNull;

public class UserRequest {

    private String userPhone;
    private String openId;
    private String unionId;
    private String parentUserPhone;
    private Long parentUserId;
    @NotNull
    private String headImageUrl;
    private String userNickName;
    private String mobileCode;
    private String token;
    private String password;
    private String realName;
    private String alipayAccount;
    private String areaCode;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getParentUserPhone() {
        return parentUserPhone;
    }

    public void setParentUserPhone(String parentUserPhone) {
        this.parentUserPhone = parentUserPhone;
    }

    public String getHeadImageUrl() {
        return headImageUrl;
    }

    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public Long getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Long parentUserId) {
        this.parentUserId = parentUserId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }

    @Override
    public String toString() {
        return "UserRequest{" +
                "userPhone='" + userPhone + '\'' +
                ", openId='" + openId + '\'' +
                ", parentUserPhone='" + parentUserPhone + '\'' +
                ", parentUserId=" + parentUserId +
                ", headImageUrl='" + headImageUrl + '\'' +
                ", userNickName='" + userNickName + '\'' +
                ", mobileCode='" + mobileCode + '\'' +
                ", token='" + token + '\'' +
                ", password='" + password + '\'' +
                ", realName='" + realName + '\'' +
                ", alipayAccount='" + alipayAccount + '\'' +
                '}';
    }
}
