package com.alipay.rebate.shop.pojo.common;

import com.alipay.rebate.shop.constants.StatusCode;

public class ResponseResult<T> {
    private int status = StatusCode.SUCCESS.getCode();
    private T data;
    private String msg = "成功";
    private long time;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ResponseResult{" +
                "status=" + status +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                ", time=" + time +
                '}';
    }
}
