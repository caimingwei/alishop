package com.alipay.rebate.shop.pojo.dataoke;

public class FriendsCircleListReq {

    private Integer pageSize;
    private String pageId;
    private String sort;
    private String cids;
    private String subcid;
    private Integer pre;
    private Integer freeshipRemoteDistrict;
    private Integer goodsId;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCids() {
        return cids;
    }

    public void setCids(String cids) {
        this.cids = cids;
    }

    public String getSubcid() {
        return subcid;
    }

    public void setSubcid(String subcid) {
        this.subcid = subcid;
    }

    public Integer getPre() {
        return pre;
    }

    public void setPre(Integer pre) {
        this.pre = pre;
    }

    public Integer getFreeshipRemoteDistrict() {
        return freeshipRemoteDistrict;
    }

    public void setFreeshipRemoteDistrict(Integer freeshipRemoteDistrict) {
        this.freeshipRemoteDistrict = freeshipRemoteDistrict;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    @Override
    public String toString() {
        return "FriendsCircleListReq{" +
                "pageSize=" + pageSize +
                ", pageId='" + pageId + '\'' +
                ", sort='" + sort + '\'' +
                ", cids='" + cids + '\'' +
                ", subcid='" + subcid + '\'' +
                ", pre=" + pre +
                ", freeshipRemoteDistrict=" + freeshipRemoteDistrict +
                ", goodsId=" + goodsId +
                '}';
    }
}
