package com.alipay.rebate.shop.pojo.user.customer;

public class PrivilegeLinkAndTpwdResponse {

  private String goodsId;
  private String couponClickUrl;
  private String itemUrl;
  private String tpwd;
  private Long relationId;
  private Long specialId;
  private String coupon;

  public String getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(String goodsId) {
    this.goodsId = goodsId;
  }

  public String getCouponClickUrl() {
    return couponClickUrl;
  }

  public void setCouponClickUrl(String couponClickUrl) {
    this.couponClickUrl = couponClickUrl;
  }

  public String getItemUrl() {
    return itemUrl;
  }

  public void setItemUrl(String itemUrl) {
    this.itemUrl = itemUrl;
  }

  public String getTpwd() {
    return tpwd;
  }

  public void setTpwd(String tpwd) {
    this.tpwd = tpwd;
  }

  public Long getRelationId() {
    return relationId;
  }

  public void setRelationId(Long relationId) {
    this.relationId = relationId;
  }

  public Long getSpecialId() {
    return specialId;
  }

  public void setSpecialId(Long specialId) {
    this.specialId = specialId;
  }

  public String getCoupon() {
    return coupon;
  }

  public void setCoupon(String coupon) {
    this.coupon = coupon;
  }

  @Override
  public String toString() {
    return "PrivilegeLinkAndTpwdResponse{" +
        "goodsId='" + goodsId + '\'' +
        ", couponClickUrl='" + couponClickUrl + '\'' +
        ", itemUrl='" + itemUrl + '\'' +
        ", tpwd='" + tpwd + '\'' +
        ", relationId=" + relationId +
        ", specialId=" + specialId +
        ", coupon='" + coupon + '\'' +
        '}';
  }
}
