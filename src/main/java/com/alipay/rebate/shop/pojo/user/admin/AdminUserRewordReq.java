package com.alipay.rebate.shop.pojo.user.admin;

import java.math.BigDecimal;

public class AdminUserRewordReq {

    private Long userId;
    private BigDecimal reword;
    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getReword() {
        return reword;
    }

    public void setReword(BigDecimal reword) {
        this.reword = reword;
    }


    @Override
    public String toString() {
        return "AdminUserRewordReq{" +
                "userId=" + userId +
                ", reword=" + reword +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
