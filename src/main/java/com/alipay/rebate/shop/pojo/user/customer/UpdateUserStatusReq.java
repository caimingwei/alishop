package com.alipay.rebate.shop.pojo.user.customer;

public class UpdateUserStatusReq {
    private Long userId;
    private Integer status;
    private String remarks;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "UpdateUserStatusReq{" +
                "userId=" + userId +
                ", status=" + status +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
