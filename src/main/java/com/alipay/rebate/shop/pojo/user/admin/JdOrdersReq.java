package com.alipay.rebate.shop.pojo.user.admin;

public class JdOrdersReq {

    private Long orderId;
    private String skuName;
    private String startOrderTime;
    private String endOrdersTime;
    private Long userId;
    private String userName;
    private Integer validCode;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getStartOrderTime() {
        return startOrderTime;
    }

    public void setStartOrderTime(String startOrderTime) {
        this.startOrderTime = startOrderTime;
    }

    public String getEndOrdersTime() {
        return endOrdersTime;
    }

    public void setEndOrdersTime(String endOrdersTime) {
        this.endOrdersTime = endOrdersTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getValidCode() {
        return validCode;
    }

    public void setValidCode(Integer validCode) {
        this.validCode = validCode;
    }

    @Override
    public String toString() {
        return "JdOrdersReq{" +
                "orderId=" + orderId +
                ", skuName='" + skuName + '\'' +
                ", startOrderTime='" + startOrderTime + '\'' +
                ", endOrdersTime='" + endOrdersTime + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", validCode=" + validCode +
                '}';
    }
}
