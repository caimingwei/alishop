package com.alipay.rebate.shop.pojo.dingdanxia;

public class MeituanOrdersRsp {
    private String orderid;
    private Integer paytime;
    private String payprice;
    private String profit;
    private String uid;
    private String smstitle;
    private String sid;
    private String quantity;
    private String refundtime;
    private String money;
    private String refund_money;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Integer getPaytime() {
        return paytime;
    }

    public void setPaytime(Integer paytime) {
        this.paytime = paytime;
    }

    public String getPayprice() {
        return payprice;
    }

    public void setPayprice(String payprice) {
        this.payprice = payprice;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getSmstitle() {
        return smstitle;
    }

    public void setSmstitle(String smstitle) {
        this.smstitle = smstitle;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRefundtime() {
        return refundtime;
    }

    public void setRefundtime(String refundtime) {
        this.refundtime = refundtime;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getRefund_money() {
        return refund_money;
    }

    public void setRefund_money(String refund_money) {
        this.refund_money = refund_money;
    }

    @Override
    public String toString() {
        return "MeituanOrders{" +
                "orderid='" + orderid + '\'' +
                ", paytime=" + paytime +
                ", payprice='" + payprice + '\'' +
                ", profit='" + profit + '\'' +
                ", uid='" + uid + '\'' +
                ", smstitle='" + smstitle + '\'' +
                ", sid='" + sid + '\'' +
                ", quantity='" + quantity + '\'' +
                ", refundtime='" + refundtime + '\'' +
                ", money='" + money + '\'' +
                ", refund_money='" + refund_money + '\'' +
                '}';
    }
}
