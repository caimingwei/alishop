package com.alipay.rebate.shop.pojo.imgspace;

import java.util.List;

public class GroupBPReq {

  private List<Long> ids;
  private Long groupId;
  private String groupName;

  public List<Long> getIds() {
    return ids;
  }

  public void setIds(List<Long> ids) {
    this.ids = ids;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  @Override
  public String toString() {
    return "GroupBPReq{" +
        "ids=" + ids +
        ", groupId=" + groupId +
        ", groupName='" + groupName + '\'' +
        '}';
  }
}
