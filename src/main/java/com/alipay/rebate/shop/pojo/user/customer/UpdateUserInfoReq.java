package com.alipay.rebate.shop.pojo.user.customer;

public class UpdateUserInfoReq {

  private Integer loginPlatform;
  private String deviceModel;
  private Integer privacyProtection;

  public Integer getPrivacyProtection() {
    return privacyProtection;
  }

  public void setPrivacyProtection(Integer privacyProtection) {
    this.privacyProtection = privacyProtection;
  }

  public Integer getLoginPlatform() {
    return loginPlatform;
  }

  public void setLoginPlatform(Integer loginPlatform) {
    this.loginPlatform = loginPlatform;
  }

  public String getDeviceModel() {
    return deviceModel;
  }

  public void setDeviceModel(String deviceModel) {
    this.deviceModel = deviceModel;
  }

  @Override
  public String toString() {
    return "UpdateUserInfoReq{" +
            "loginPlatform=" + loginPlatform +
            ", deviceModel='" + deviceModel + '\'' +
            ", privacyProtection=" + privacyProtection +
            '}';
  }
}
