package com.alipay.rebate.shop.pojo;

import org.mybatis.generator.api.dom.java.Interface;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class UserImportRequset {
    private String uphone;
    private String user_name;
    private String avatar;
    private BigDecimal money;
    private Long jifen;
    private String wx_unionid;
    private String alipay;
    private String alipay_name;
    private String login_time;
    private String create_time;
    private String user_level;
    private String agent_level;
    private String agent_create_time;

    public String getUphone() {
        return uphone;
    }

    public void setUphone(String uphone) {
        this.uphone = uphone;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Long getJifen() {
        return jifen;
    }

    public void setJifen(Long jifen) {
        this.jifen = jifen;
    }

    public String getWx_unionid() {
        return wx_unionid;
    }

    public void setWx_unionid(String wx_unionid) {
        this.wx_unionid = wx_unionid;
    }

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }

    public String getAlipay_name() {
        return alipay_name;
    }

    public void setAlipay_name(String alipay_name) {
        this.alipay_name = alipay_name;
    }

    public String getLogin_time() {
        return login_time;
    }

    public void setLogin_time(String login_time) {
        this.login_time = login_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUser_level() {
        return user_level;
    }

    public void setUser_level(String user_level) {
        this.user_level = user_level;
    }

    public String getAgent_level() {
        return agent_level;
    }

    public void setAgent_level(String agent_level) {
        this.agent_level = agent_level;
    }

    public String getAgent_create_time() {
        return agent_create_time;
    }

    public void setAgent_create_time(String agent_create_time) {
        this.agent_create_time = agent_create_time;
    }

    @Override
    public String toString() {
        return "UserImportRespon{" +
                "uphone='" + uphone + '\'' +
                ", user_name='" + user_name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", money=" + money +
                ", jifen=" + jifen +
                ", wx_unionid='" + wx_unionid + '\'' +
                ", alipay='" + alipay + '\'' +
                ", alipay_name='" + alipay_name + '\'' +
                ", login_time='" + login_time + '\'' +
                ", create_time='" + create_time + '\'' +
                ", user_level='" + user_level + '\'' +
                ", agent_level='" + agent_level + '\'' +
                ", agent_create_time='" + agent_create_time + '\'' +
                '}';
    }
}
