package com.alipay.rebate.shop.pojo.taobao;

public class ItemRecommendRequest {

    private String fields;
    private Long num_iid;
    private Long count;
    private Long platform;

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public Long getNum_iid() {
        return num_iid;
    }

    public void setNum_iid(Long num_iid) {
        this.num_iid = num_iid;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getPlatform() {
        return platform;
    }

    public void setPlatform(Long platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "ItemRecommendRequest{" +
                "fields='" + fields + '\'' +
                ", num_iid=" + num_iid +
                ", count=" + count +
                ", platform=" + platform +
                '}';
    }
}
