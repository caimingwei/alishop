package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.List;

public class  CreatePositionRsp {

    private CreatePosition resultList;
    private Long siteId;
    private Long type;
    private Long unionId;

    public CreatePosition getResultList() {
        return resultList;
    }

    public void setResultList(CreatePosition resultList) {
        this.resultList = resultList;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId){
        this.siteId = siteId;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getUnionId() {
        return unionId;
    }

    public void setUnionId(Long unionId) {
        this.unionId = unionId;
    }

    @Override
    public String toString() {
        return "CreatePositionRsp{" +
                "resultList=" + resultList +
                ", siteId=" + siteId +
                ", type=" + type +
                ", unionId=" + unionId +
                '}';
    }
}
