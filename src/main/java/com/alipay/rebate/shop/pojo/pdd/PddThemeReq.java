package com.alipay.rebate.shop.pojo.pdd;

import java.util.Arrays;
import java.util.List;

public class PddThemeReq {

    private String custom_parameters;
    private Boolean generate_mobile;
    private Boolean generate_qq_app;
    private Boolean generate_schema_url;
    private Boolean generate_short_url;
    private Boolean generate_weapp_webview;
    private Boolean generate_we_app;
    private String pid;
    private List<Long> theme_id_list;

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public Boolean getGenerate_mobile() {
        return generate_mobile;
    }

    public void setGenerate_mobile(Boolean generate_mobile) {
        this.generate_mobile = generate_mobile;
    }

    public Boolean getGenerate_qq_app() {
        return generate_qq_app;
    }

    public void setGenerate_qq_app(Boolean generate_qq_app) {
        this.generate_qq_app = generate_qq_app;
    }

    public Boolean getGenerate_schema_url() {
        return generate_schema_url;
    }

    public void setGenerate_schema_url(Boolean generate_schema_url) {
        this.generate_schema_url = generate_schema_url;
    }

    public Boolean getGenerate_short_url() {
        return generate_short_url;
    }

    public void setGenerate_short_url(Boolean generate_short_url) {
        this.generate_short_url = generate_short_url;
    }

    public Boolean getGenerate_weapp_webview() {
        return generate_weapp_webview;
    }

    public void setGenerate_weapp_webview(Boolean generate_weapp_webview) {
        this.generate_weapp_webview = generate_weapp_webview;
    }

    public Boolean getGenerate_we_app() {
        return generate_we_app;
    }

    public void setGenerate_we_app(Boolean generate_we_app) {
        this.generate_we_app = generate_we_app;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<Long> getTheme_id_list() {
        return theme_id_list;
    }

    public void setTheme_id_list(List<Long> theme_id_list) {
        this.theme_id_list = theme_id_list;
    }

    @Override
    public String toString() {
        return "PddThemeReq{" +
                "custom_parameters='" + custom_parameters + '\'' +
                ", generate_mobile=" + generate_mobile +
                ", generate_qq_app=" + generate_qq_app +
                ", generate_schema_url=" + generate_schema_url +
                ", generate_short_url=" + generate_short_url +
                ", generate_weapp_webview=" + generate_weapp_webview +
                ", generate_we_app=" + generate_we_app +
                ", pid='" + pid + '\'' +
                ", theme_id_list=" + theme_id_list +
                '}';
    }
}
