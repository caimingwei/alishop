package com.alipay.rebate.shop.pojo.pdtframework;

import com.alipay.rebate.shop.model.Product;
import com.github.pagehelper.PageInfo;

public class ProductRepoChoiceRsp {

  private Integer activityId;
  private Integer productRepoId;
  PageInfo<Product> data;

  public Integer getActivityId() {
    return activityId;
  }

  public void setActivityId(Integer activityId) {
    this.activityId = activityId;
  }

  public Integer getProductRepoId() {
    return productRepoId;
  }

  public void setProductRepoId(Integer productRepoId) {
    this.productRepoId = productRepoId;
  }

  public PageInfo<Product> getData() {
    return data;
  }

  public void setData(PageInfo<Product> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "ProductRepoChoiceRsp{" +
        "activityId=" + activityId +
        ", productRepoId=" + productRepoId +
        ", data=" + data +
        '}';
  }
}
