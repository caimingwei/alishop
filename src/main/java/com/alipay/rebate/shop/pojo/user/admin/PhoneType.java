package com.alipay.rebate.shop.pojo.user.admin;

import java.math.BigDecimal;

public class PhoneType {

    private Integer phoneNum;
    private String deviceModel;
    private BigDecimal active;

    public BigDecimal getActive() {
        return active;
    }

    public void setActive(BigDecimal active) {
        this.active = active;
    }

    public Integer getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(Integer phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    @Override
    public String toString() {
        return "PhoneType{" +
                "phoneNum=" + phoneNum +
                ", deviceModel='" + deviceModel + '\'' +
                ", active=" + active +
                '}';
    }
}
