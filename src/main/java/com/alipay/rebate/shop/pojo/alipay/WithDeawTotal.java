package com.alipay.rebate.shop.pojo.alipay;

import java.math.BigDecimal;

public class WithDeawTotal {

    private BigDecimal total;

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "WithDeawTotal{" +
                "total=" + total +
                '}';
    }
}
