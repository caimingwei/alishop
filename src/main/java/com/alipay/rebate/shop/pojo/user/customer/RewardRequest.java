package com.alipay.rebate.shop.pojo.user.customer;

public class RewardRequest {

  Long uit;

  public Long getUit() {
    return uit;
  }

  public void setUit(Long uit) {
    this.uit = uit;
  }

  @Override
  public String toString() {
    return "RewardRequest{" +
        "uit=" + uit +
        '}';
  }
}
