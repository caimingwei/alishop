package com.alipay.rebate.shop.pojo.user.customer;

import java.math.BigDecimal;

public class InviteDetailRsp {

  private BigDecimal income;
  private Long validUserNum;
  private Long firstBuyUserNum;

  public BigDecimal getIncome() {
    return income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }

  public Long getValidUserNum() {
    return validUserNum;
  }

  public void setValidUserNum(Long validUserNum) {
    this.validUserNum = validUserNum;
  }

  public Long getFirstBuyUserNum() {
    return firstBuyUserNum;
  }

  public void setFirstBuyUserNum(Long firstBuyUserNum) {
    this.firstBuyUserNum = firstBuyUserNum;
  }

  @Override
  public String toString() {
    return "InviteDetailRsp{" +
        "income=" + income +
        ", validUserNum=" + validUserNum +
        ", firstBuyUserNum=" + firstBuyUserNum +
        '}';
  }
}
