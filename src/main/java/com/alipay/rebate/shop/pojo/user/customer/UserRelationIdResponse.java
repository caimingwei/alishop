package com.alipay.rebate.shop.pojo.user.customer;

public class UserRelationIdResponse {

    private Long relationId;
    private Long specialId;

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getSpecialId() {
        return specialId;
    }

    public void setSpecialId(Long specialId) {
        this.specialId = specialId;
    }

    @Override
    public String toString() {
        return "UserRelationIdResponse{" +
                "relationId='" + relationId + '\'' +
                ", specialId='" + specialId + '\'' +
                '}';
    }
}
