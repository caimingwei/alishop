package com.alipay.rebate.shop.pojo;

import javax.persistence.Column;
import java.util.Date;

public class InterfaceStatisticsRsp {

    private Long id;
    private Long oneDayCount;
    private Date theSameDay;
    private String name;
    private Long oneMinuteCount;
    private String minute;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOneDayCount() {
        return oneDayCount;
    }

    public void setOneDayCount(Long oneDayCount) {
        this.oneDayCount = oneDayCount;
    }

    public Date getTheSameDay() {
        return theSameDay;
    }

    public void setTheSameDay(Date theSameDay) {
        this.theSameDay = theSameDay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOneMinuteCount() {
        return oneMinuteCount;
    }

    public void setOneMinuteCount(Long oneMinuteCount) {
        this.oneMinuteCount = oneMinuteCount;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return "InterfaceStatisticsRsp{" +
                "id=" + id +
                ", oneDayCount=" + oneDayCount +
                ", theSameDay=" + theSameDay +
                ", name='" + name + '\'' +
                ", oneMinuteCount=" + oneMinuteCount +
                ", minute='" + minute + '\'' +
                '}';
    }
}
