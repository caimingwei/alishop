package com.alipay.rebate.shop.pojo.dingdanxia;

public class TklCreateReq {

  private String apikey;
  private String signature;
  private String text;
  private String url;
  private String logo;
  private String user_id;
  private String left_symbol;
  private String right_symbol;

  public String getApikey() {
    return apikey;
  }

  public void setApikey(String apikey) {
    this.apikey = apikey;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getLeft_symbol() {
    return left_symbol;
  }

  public void setLeft_symbol(String left_symbol) {
    this.left_symbol = left_symbol;
  }

  public String getRight_symbol() {
    return right_symbol;
  }

  public void setRight_symbol(String right_symbol) {
    this.right_symbol = right_symbol;
  }

  @Override
  public String toString() {
    return "TklCreateReq{" +
        "apikey='" + apikey + '\'' +
        ", signature='" + signature + '\'' +
        ", text='" + text + '\'' +
        ", url='" + url + '\'' +
        ", logo='" + logo + '\'' +
        ", user_id='" + user_id + '\'' +
        ", left_symbol='" + left_symbol + '\'' +
        ", right_symbol='" + right_symbol + '\'' +
        '}';
  }
}
