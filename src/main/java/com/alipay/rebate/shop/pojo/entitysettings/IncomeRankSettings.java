package com.alipay.rebate.shop.pojo.entitysettings;

import java.util.List;

public class IncomeRankSettings {

  private String startTime;
  private String endTime;
  private Integer realUserNum;
  private List<IrUser> irUsers;

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public Integer getRealUserNum() {
    return realUserNum;
  }

  public void setRealUserNum(Integer realUserNum) {
    this.realUserNum = realUserNum;
  }

  public List<IrUser> getIrUsers() {
    return irUsers;
  }

  public void setIrUsers(List<IrUser> irUsers) {
    this.irUsers = irUsers;
  }

  @Override
  public String toString() {
    return "IncomeRankSettings{" +
        "startTime='" + startTime + '\'' +
        ", endTime='" + endTime + '\'' +
        ", realUserNum=" + realUserNum +
        ", irUsers=" + irUsers +
        '}';
  }
}
