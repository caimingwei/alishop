package com.alipay.rebate.shop.pojo.pdtframework;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class ProductChoice {

  private List<
      @Min(value = 0, message = "cid范围为0-17")
      @Max(value = 17, message = "cid范围为0-17")
      Integer> category;
  @Min(value = 1, message = "type范围为1-12")
  @Max(value = 12, message = "type范围为1-12")
  private Integer type;
  private BigDecimal minAfterPrice;
  private BigDecimal maxAfterPrice;
  private BigDecimal minTicket;
  private BigDecimal maxTicket;
  private Integer sell;
  @JsonIgnore
  @Min(value = 0, message = "sort范围为0-14")
  @Max(value = 14, message = "sort范围为0-14")
  private Integer sort;

  public List<Integer> getCategory() {
    return category;
  }

  public void setCategory(List<Integer> category) {
    this.category = category;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public BigDecimal getMinAfterPrice() {
    return minAfterPrice;
  }

  public void setMinAfterPrice(BigDecimal minAfterPrice) {
    this.minAfterPrice = minAfterPrice;
  }

  public BigDecimal getMaxAfterPrice() {
    return maxAfterPrice;
  }

  public void setMaxAfterPrice(BigDecimal maxAfterPrice) {
    this.maxAfterPrice = maxAfterPrice;
  }

  public BigDecimal getMinTicket() {
    return minTicket;
  }

  public void setMinTicket(BigDecimal minTicket) {
    this.minTicket = minTicket;
  }

  public BigDecimal getMaxTicket() {
    return maxTicket;
  }

  public void setMaxTicket(BigDecimal maxTicket) {
    this.maxTicket = maxTicket;
  }

  public Integer getSell() {
    return sell;
  }

  public void setSell(Integer sell) {
    this.sell = sell;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  @Override
  public String toString() {
    return "ProductChoice{" +
        "category=" + category +
        ", type=" + type +
        ", minAfterPrice=" + minAfterPrice +
        ", maxAfterPrice=" + maxAfterPrice +
        ", minTicket=" + minTicket +
        ", maxTicket=" + maxTicket +
        ", sell=" + sell +
        ", sort=" + sort +
        '}';
  }
}
