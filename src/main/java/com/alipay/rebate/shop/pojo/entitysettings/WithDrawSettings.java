package com.alipay.rebate.shop.pojo.entitysettings;

import java.math.BigDecimal;

public class WithDrawSettings {

  private Integer taobaoMinWUit;
  private Integer uitMinMutiple;
  private BigDecimal minWAmount;
  private Integer amMinMutiple;
  private BigDecimal chargeRate;
  private Integer illustrationType;
  private String  illustration;
  private Integer timeStart;
  private Integer timeEnd;

  public Integer getTaobaoMinWUit() {
    return taobaoMinWUit;
  }

  public void setTaobaoMinWUit(Integer taobaoMinWUit) {
    this.taobaoMinWUit = taobaoMinWUit;
  }

  public Integer getUitMinMutiple() {
    return uitMinMutiple;
  }

  public void setUitMinMutiple(Integer uitMinMutiple) {
    this.uitMinMutiple = uitMinMutiple;
  }

  public BigDecimal getMinWAmount() {
    return minWAmount;
  }

  public void setMinWAmount(BigDecimal minWAmount) {
    this.minWAmount = minWAmount;
  }

  public Integer getAmMinMutiple() {
    return amMinMutiple;
  }

  public void setAmMinMutiple(Integer amMinMutiple) {
    this.amMinMutiple = amMinMutiple;
  }

  public BigDecimal getChargeRate() {
    return chargeRate;
  }

  public void setChargeRate(BigDecimal chargeRate) {
    this.chargeRate = chargeRate;
  }

  public Integer getIllustrationType() {
    return illustrationType;
  }

  public void setIllustrationType(Integer illustrationType) {
    this.illustrationType = illustrationType;
  }

  public String getIllustration() {
    return illustration;
  }

  public void setIllustration(String illustration) {
    this.illustration = illustration;
  }

  public Integer getTimeStart() {
    return timeStart;
  }

  public void setTimeStart(Integer timeStart) {
    this.timeStart = timeStart;
  }

  public Integer getTimeEnd() {
    return timeEnd;
  }

  public void setTimeEnd(Integer timeEnd) {
    this.timeEnd = timeEnd;
  }

  @Override
  public String toString() {
    return "WithDrawSettings{" +
        "taobaoMinWUit=" + taobaoMinWUit +
        ", uitMinMutiple=" + uitMinMutiple +
        ", minWAmount=" + minWAmount +
        ", amMinMutiple=" + amMinMutiple +
        ", chargeRate=" + chargeRate +
        ", illustrationType=" + illustrationType +
        ", illustration='" + illustration + '\'' +
        ", timeStart=" + timeStart +
        ", timeEnd=" + timeEnd +
        '}';
  }
}
