package com.alipay.rebate.shop.pojo.user;

public class TaobaoAuthRsp {

  private Long userId;
  private Long relationId;
  private Long specialId;
  private boolean authSuccess;
  private String msg;
  private Object authUserMsg;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getRelationId() {
    return relationId;
  }

  public void setRelationId(Long relationId) {
    this.relationId = relationId;
  }

  public Long getSpecialId() {
    return specialId;
  }

  public void setSpecialId(Long specialId) {
    this.specialId = specialId;
  }

  public boolean isAuthSuccess() {
    return authSuccess;
  }

  public void setAuthSuccess(boolean authSuccess) {
    this.authSuccess = authSuccess;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Object getAuthUserMsg() {
    return authUserMsg;
  }

  public void setAuthUserMsg(Object authUserMsg) {
    this.authUserMsg = authUserMsg;
  }

  @Override
  public String toString() {
    return "TaobaoAuthRsp{" +
        "userId=" + userId +
        ", relationId=" + relationId +
        ", specialId=" + specialId +
        ", authSuccess=" + authSuccess +
        ", msg='" + msg + '\'' +
        ", authUserMsg=" + authUserMsg +
        '}';
  }
}
