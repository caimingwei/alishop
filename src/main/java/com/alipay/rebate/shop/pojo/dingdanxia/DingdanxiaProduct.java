package com.alipay.rebate.shop.pojo.dingdanxia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DingdanxiaProduct {

  private String coupon_start_time;
  private String coupon_end_time;
  private String coupon_info;
  private String coupon;
  private String coupon_id;
  private String coupon_total_count;
  private String coupon_remain_count;
  private String info_dxjh;
  private String tk_total_sales;
  private String tk_total_commi;
  private String num_iid;
  private String title;
  private String short_title;
  private String category_id;
  private String category_name;
  private String level_one_category_id;
  private String level_one_category_name;
  private String pict_url;
  private Object small_images;
  private String reserve_price;
  private String zk_final_price;
  private String user_type;
  private String provcity;
  private String item_url;
  private String include_mkt;
  private String include_dxjh;
  private String commission_rate;
  private String volume;
  private String seller_id;
  private String commission_type;
  private String shop_title;
  private String url;
  private String coupon_share_url;
  private String shop_dsr;
  private String white_image;
  private String oetime;
  private String ostime;
  private String jdd_num;
  private String jdd_price;
  private String uv_sum_pre_sale;
  private Integer coupon_amount;

  public String getCoupon_start_time() {
    return coupon_start_time;
  }

  public void setCoupon_start_time(String coupon_start_time) {
    this.coupon_start_time = coupon_start_time;
  }

  public String getCoupon_end_time() {
    return coupon_end_time;
  }

  public void setCoupon_end_time(String coupon_end_time) {
    this.coupon_end_time = coupon_end_time;
  }

  public String getCoupon_info() {
    return coupon_info;
  }

  public void setCoupon_info(String coupon_info) {
    this.coupon_info = coupon_info;
  }

  public String getCoupon() {
    return coupon;
  }

  public void setCoupon(String coupon) {
    this.coupon = coupon;
  }

  public String getCoupon_id() {
    return coupon_id;
  }

  public void setCoupon_id(String coupon_id) {
    this.coupon_id = coupon_id;
  }

  public String getCoupon_total_count() {
    return coupon_total_count;
  }

  public void setCoupon_total_count(String coupon_total_count) {
    this.coupon_total_count = coupon_total_count;
  }

  public String getCoupon_remain_count() {
    return coupon_remain_count;
  }

  public void setCoupon_remain_count(String coupon_remain_count) {
    this.coupon_remain_count = coupon_remain_count;
  }

  public String getInfo_dxjh() {
    return info_dxjh;
  }

  public void setInfo_dxjh(String info_dxjh) {
    this.info_dxjh = info_dxjh;
  }

  public String getTk_total_sales() {
    return tk_total_sales;
  }

  public void setTk_total_sales(String tk_total_sales) {
    this.tk_total_sales = tk_total_sales;
  }

  public String getTk_total_commi() {
    return tk_total_commi;
  }

  public void setTk_total_commi(String tk_total_commi) {
    this.tk_total_commi = tk_total_commi;
  }

  public String getNum_iid() {
    return num_iid;
  }

  public void setNum_iid(String num_iid) {
    this.num_iid = num_iid;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getShort_title() {
    return short_title;
  }

  public void setShort_title(String short_title) {
    this.short_title = short_title;
  }

  public String getCategory_id() {
    return category_id;
  }

  public void setCategory_id(String category_id) {
    this.category_id = category_id;
  }

  public String getCategory_name() {
    return category_name;
  }

  public void setCategory_name(String category_name) {
    this.category_name = category_name;
  }

  public String getLevel_one_category_id() {
    return level_one_category_id;
  }

  public void setLevel_one_category_id(String level_one_category_id) {
    this.level_one_category_id = level_one_category_id;
  }

  public String getLevel_one_category_name() {
    return level_one_category_name;
  }

  public void setLevel_one_category_name(String level_one_category_name) {
    this.level_one_category_name = level_one_category_name;
  }

  public String getPict_url() {
    return pict_url;
  }

  public void setPict_url(String pict_url) {
    this.pict_url = pict_url;
  }

  public Object getSmall_images() {
    return small_images;
  }

  public void setSmall_images(Object small_images) {
    this.small_images = small_images;
  }

  public String getReserve_price() {
    return reserve_price;
  }

  public void setReserve_price(String reserve_price) {
    this.reserve_price = reserve_price;
  }

  public String getZk_final_price() {
    return zk_final_price;
  }

  public void setZk_final_price(String zk_final_price) {
    this.zk_final_price = zk_final_price;
  }

  public String getUser_type() {
    return user_type;
  }

  public void setUser_type(String user_type) {
    this.user_type = user_type;
  }

  public String getProvcity() {
    return provcity;
  }

  public void setProvcity(String provcity) {
    this.provcity = provcity;
  }

  public String getItem_url() {
    return item_url;
  }

  public void setItem_url(String item_url) {
    this.item_url = item_url;
  }

  public String getInclude_mkt() {
    return include_mkt;
  }

  public void setInclude_mkt(String include_mkt) {
    this.include_mkt = include_mkt;
  }

  public String getInclude_dxjh() {
    return include_dxjh;
  }

  public void setInclude_dxjh(String include_dxjh) {
    this.include_dxjh = include_dxjh;
  }

  public String getCommission_rate() {
    return commission_rate;
  }

  public void setCommission_rate(String commission_rate) {
    this.commission_rate = commission_rate;
  }

  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume = volume;
  }

  public String getSeller_id() {
    return seller_id;
  }

  public void setSeller_id(String seller_id) {
    this.seller_id = seller_id;
  }

  public String getCommission_type() {
    return commission_type;
  }

  public void setCommission_type(String commission_type) {
    this.commission_type = commission_type;
  }

  public String getShop_title() {
    return shop_title;
  }

  public void setShop_title(String shop_title) {
    this.shop_title = shop_title;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getCoupon_share_url() {
    return coupon_share_url;
  }

  public void setCoupon_share_url(String coupon_share_url) {
    this.coupon_share_url = coupon_share_url;
  }

  public String getShop_dsr() {
    return shop_dsr;
  }

  public void setShop_dsr(String shop_dsr) {
    this.shop_dsr = shop_dsr;
  }

  public String getWhite_image() {
    return white_image;
  }

  public void setWhite_image(String white_image) {
    this.white_image = white_image;
  }

  public String getOetime() {
    return oetime;
  }

  public void setOetime(String oetime) {
    this.oetime = oetime;
  }

  public String getOstime() {
    return ostime;
  }

  public void setOstime(String ostime) {
    this.ostime = ostime;
  }

  public String getJdd_num() {
    return jdd_num;
  }

  public void setJdd_num(String jdd_num) {
    this.jdd_num = jdd_num;
  }

  public String getJdd_price() {
    return jdd_price;
  }

  public void setJdd_price(String jdd_price) {
    this.jdd_price = jdd_price;
  }

  public String getUv_sum_pre_sale() {
    return uv_sum_pre_sale;
  }

  public void setUv_sum_pre_sale(String uv_sum_pre_sale) {
    this.uv_sum_pre_sale = uv_sum_pre_sale;
  }

  public Integer getCoupon_amount() {
    return coupon_amount;
  }

  public void setCoupon_amount(Integer coupon_amount) {
    this.coupon_amount = coupon_amount;
  }

  @Override
  public String toString() {
    return "DingdanxiaProduct{" +
        "coupon_start_time='" + coupon_start_time + '\'' +
        ", coupon_end_time='" + coupon_end_time + '\'' +
        ", coupon_info='" + coupon_info + '\'' +
        ", coupon='" + coupon + '\'' +
        ", coupon_id='" + coupon_id + '\'' +
        ", coupon_total_count='" + coupon_total_count + '\'' +
        ", coupon_remain_count='" + coupon_remain_count + '\'' +
        ", info_dxjh='" + info_dxjh + '\'' +
        ", tk_total_sales='" + tk_total_sales + '\'' +
        ", tk_total_commi='" + tk_total_commi + '\'' +
        ", num_iid='" + num_iid + '\'' +
        ", title='" + title + '\'' +
        ", short_title='" + short_title + '\'' +
        ", category_id='" + category_id + '\'' +
        ", category_name='" + category_name + '\'' +
        ", level_one_category_id='" + level_one_category_id + '\'' +
        ", level_one_category_name='" + level_one_category_name + '\'' +
        ", pict_url='" + pict_url + '\'' +
        ", small_images='" + small_images + '\'' +
        ", reserve_price='" + reserve_price + '\'' +
        ", zk_final_price='" + zk_final_price + '\'' +
        ", user_type='" + user_type + '\'' +
        ", provcity='" + provcity + '\'' +
        ", item_url='" + item_url + '\'' +
        ", include_mkt='" + include_mkt + '\'' +
        ", include_dxjh='" + include_dxjh + '\'' +
        ", commission_rate='" + commission_rate + '\'' +
        ", volume='" + volume + '\'' +
        ", seller_id='" + seller_id + '\'' +
        ", commission_type='" + commission_type + '\'' +
        ", shop_title='" + shop_title + '\'' +
        ", url='" + url + '\'' +
        ", coupon_share_url='" + coupon_share_url + '\'' +
        ", shop_dsr='" + shop_dsr + '\'' +
        ", white_image='" + white_image + '\'' +
        ", oetime='" + oetime + '\'' +
        ", ostime='" + ostime + '\'' +
        ", jdd_num='" + jdd_num + '\'' +
        ", jdd_price='" + jdd_price + '\'' +
        ", uv_sum_pre_sale='" + uv_sum_pre_sale + '\'' +
        '}';
  }
}
