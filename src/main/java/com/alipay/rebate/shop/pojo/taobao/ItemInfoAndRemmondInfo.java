package com.alipay.rebate.shop.pojo.taobao;

public class ItemInfoAndRemmondInfo {

    private String tbkItemInfo;
    private String tbkItemRecommendInfo;

    public String getTbkItemInfo() {
        return tbkItemInfo;
    }

    public void setTbkItemInfo(String tbkItemInfo) {
        this.tbkItemInfo = tbkItemInfo;
    }

    public String getTbkItemRecommendInfo() {
        return tbkItemRecommendInfo;
    }

    public void setTbkItemRecommendInfo(String tbkItemRecommendInfo) {
        this.tbkItemRecommendInfo = tbkItemRecommendInfo;
    }

    @Override
    public String toString() {
        return "ItemInfoAndRemmondInfo{" +
                "tbkItemInfo='" + tbkItemInfo + '\'' +
                ", tbkItemRecommendInfo='" + tbkItemRecommendInfo + '\'' +
                '}';
    }
}
