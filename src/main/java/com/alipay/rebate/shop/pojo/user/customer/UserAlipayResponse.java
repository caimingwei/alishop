package com.alipay.rebate.shop.pojo.user.customer;

public class UserAlipayResponse {

    private String alipayAccount;
    private String realName;

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String toString() {
        return "UserAlipayResponse{" +
                "alipayAccount='" + alipayAccount + '\'' +
                ", realName='" + realName + '\'' +
                '}';
    }
}
