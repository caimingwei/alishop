package com.alipay.rebate.shop.pojo.userincome;

import java.math.BigDecimal;

public class CalculateUserIncome {
    private Long userId;
    private BigDecimal uit;
    private BigDecimal withdrawMobey;
    private BigDecimal user_income;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getUit() {
        return uit;
    }

    public void setUit(BigDecimal uit) {
        this.uit = uit;
    }

    public BigDecimal getWithdrawMobey() {
        return withdrawMobey;
    }

    public void setWithdrawMobey(BigDecimal withdrawMobey) {
        this.withdrawMobey = withdrawMobey;
    }

    public BigDecimal getUser_income() {
        return user_income;
    }

    public void setUser_income(BigDecimal user_income) {
        this.user_income = user_income;
    }

    @Override
    public String toString() {
        return "IncomeSumMoneyRsp{" +
                "userId=" + userId +
                ", uit=" + uit +
                ", withdrawMobey=" + withdrawMobey +
                ", user_income=" + user_income +
                '}';
    }
}
