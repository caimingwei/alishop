package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class PlusOrReduceVirMoneyReq {

  @NotNull(message = "虚拟币数量不能为空")
  private Long num;
  @NotNull(message = "奖励类型不能为空")
  @Min(value = 1,message = "奖励类型范围为1-3")
  @Max(value = 3,message = "奖励类型范围为1-3")
  private Integer awardType;
  @NotNull(message = "类型不能为空")
  private Integer type;
  private boolean flag;

  public Long getNum() {
    return num;
  }

  public void setNum(Long num) {
    this.num = num;
  }

  public Integer getAwardType() {
    return awardType;
  }

  public void setAwardType(Integer awardType) {
    this.awardType = awardType;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public boolean isFlag() {
    return flag;
  }

  public void setFlag(boolean flag) {
    this.flag = flag;
  }

  @Override
  public String toString() {
    return "PlusOrReduceVirMoneyReq{" +
        "num=" + num +
        ", awardType=" + awardType +
        ", type=" + type +
        ", flag=" + flag +
        '}';
  }
}
