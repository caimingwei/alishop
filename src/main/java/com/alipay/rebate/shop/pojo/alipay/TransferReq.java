package com.alipay.rebate.shop.pojo.alipay;

import com.alipay.rebate.shop.model.AlipayOrder;
import java.util.List;

public class TransferReq {

  private boolean checkLeftBeforeTransfer = true;

  private boolean batchModifyOrderStatusReject = false;
  
  List<AlipayOrder> alipayOrders;

  public boolean isCheckLeftBeforeTransfer() {
    return checkLeftBeforeTransfer;
  }

  public void setCheckLeftBeforeTransfer(boolean checkLeftBeforeTransfer) {
    this.checkLeftBeforeTransfer = checkLeftBeforeTransfer;
  }

  public List<AlipayOrder> getAlipayOrders() {
    return alipayOrders;
  }

  public void setAlipayOrders(List<AlipayOrder> alipayOrders) {
    this.alipayOrders = alipayOrders;
  }

  public boolean isBatchModifyOrderStatusReject() {
    return batchModifyOrderStatusReject;
  }

  public void setBatchModifyOrderStatusReject(boolean batchModifyOrderStatusReject) {
    this.batchModifyOrderStatusReject = batchModifyOrderStatusReject;
  }

  @Override
  public String toString() {
    return "TransferReq{" +
        "checkLeftBeforeTransfer=" + checkLeftBeforeTransfer +
        ", alipayOrders=" + alipayOrders +
        ", batchModifyOrderStatusReject=" + batchModifyOrderStatusReject +
        '}';
  }
}
