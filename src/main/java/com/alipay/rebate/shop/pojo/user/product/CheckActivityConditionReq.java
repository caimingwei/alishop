package com.alipay.rebate.shop.pojo.user.product;

import javax.validation.constraints.NotNull;

public class CheckActivityConditionReq {

  @NotNull(message = "商品id不能为空")
  private String goodsId;
  private boolean flag;
  private String access_token;
  private String userId;
  private String userName;
  @NotNull(message = "活动id不能为空")
  private Integer activityId;
  private Integer productRepoId;
//  @NotNull(message = "条件json不能为空")
  private ActivityProductCondition json;
  private Integer itemType;

  public Integer getItemType() {
    return itemType;
  }
  public void setItemType(Integer itemType) {
    this.itemType = itemType;
  }

  public String getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(String goodsId) {
    this.goodsId = goodsId;
  }

  public boolean isFlag() {
    return flag;
  }

  public void setFlag(boolean flag) {
    this.flag = flag;
  }

  public String getAccess_token() {
    return access_token;
  }

  public void setAccess_token(String access_token) {
    this.access_token = access_token;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Integer getActivityId() {
    return activityId;
  }

  public void setActivityId(Integer activityId) {
    this.activityId = activityId;
  }

  public Integer getProductRepoId() {
    return productRepoId;
  }

  public void setProductRepoId(Integer productRepoId) {
    this.productRepoId = productRepoId;
  }

  public ActivityProductCondition getJson() {
    return json;
  }

  public void setJson(ActivityProductCondition json) {
    this.json = json;
  }

  @Override
  public String toString() {
    return "CheckActivityConditionReq{" +
            "goodsId='" + goodsId + '\'' +
            ", flag=" + flag +
            ", access_token='" + access_token + '\'' +
            ", userId='" + userId + '\'' +
            ", userName='" + userName + '\'' +
            ", activityId=" + activityId +
            ", productRepoId=" + productRepoId +
            ", json=" + json +
            ", itemType=" + itemType +
            '}';
  }
}
