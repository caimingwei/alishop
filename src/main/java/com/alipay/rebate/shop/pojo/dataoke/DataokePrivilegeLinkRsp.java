package com.alipay.rebate.shop.pojo.dataoke;

public class DataokePrivilegeLinkRsp {

    private Long time;
    private Integer code;
    private String msg;
    private DataokePrivilegeLinkData data;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataokePrivilegeLinkData getData() {
        return data;
    }

    public void setData(DataokePrivilegeLinkData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DataokeResponse{" +
                "time=" + time +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
