package com.alipay.rebate.shop.pojo.user.customer;

import java.math.BigDecimal;

public class UserMiddleModel {

    private BigDecimal withdrawUITOnGoing = new BigDecimal(0.00);
    private BigDecimal withdrawUITSettlementing  = new BigDecimal(0.00);;
    private String withdrawAMOnGoing = "0.00";
    private String withdrawAMSettlementing = "0.00";
    private String userTotalIncome = "0.00";
    private String userTodayIncome = "0.00";

    public BigDecimal getWithdrawUITOnGoing() {
        return withdrawUITOnGoing;
    }

    public void setWithdrawUITOnGoing(BigDecimal withdrawUITOnGoing) {
        this.withdrawUITOnGoing = withdrawUITOnGoing;
    }

    public BigDecimal getWithdrawUITSettlementing() {
        return withdrawUITSettlementing;
    }

    public void setWithdrawUITSettlementing(BigDecimal withdrawUITSettlementing) {
        this.withdrawUITSettlementing = withdrawUITSettlementing;
    }

    public String getWithdrawAMOnGoing() {
        return withdrawAMOnGoing;
    }

    public void setWithdrawAMOnGoing(String withdrawAMOnGoing) {
        this.withdrawAMOnGoing = withdrawAMOnGoing;
    }

    public String getWithdrawAMSettlementing() {
        return withdrawAMSettlementing;
    }

    public void setWithdrawAMSettlementing(String withdrawAMSettlementing) {
        this.withdrawAMSettlementing = withdrawAMSettlementing;
    }

    public String getUserTotalIncome() {
        return userTotalIncome;
    }

    public void setUserTotalIncome(String userTotalIncome) {
        this.userTotalIncome = userTotalIncome;
    }

    public String getUserTodayIncome() {
        return userTodayIncome;
    }

    public void setUserTodayIncome(String userTodayIncome) {
        this.userTodayIncome = userTodayIncome;
    }

    @Override
    public String toString() {
        return "UserMiddleModel{" +
                "withdrawUITOnGoing=" + withdrawUITOnGoing +
                ", withdrawUITSettlementing=" + withdrawUITSettlementing +
                ", withdrawAMOnGoing=" + withdrawAMOnGoing +
                ", withdrawAMSettlementing=" + withdrawAMSettlementing +
                ", userTotalIncome=" + userTotalIncome +
                ", userTodayIncome=" + userTodayIncome +
                '}';
    }
}
