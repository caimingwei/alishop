package com.alipay.rebate.shop.pojo.pdd;

import java.util.List;

public class PddGoodsRecommendReq {

    private Integer channel_type;
    private String custom_parameters;
    private Integer limit;
    private String list_id;
    private Integer offset;
    private String pid;
    private Long cat_id;
    private List<Long> goods_ids;

    public Integer getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(Integer channel_type) {
        this.channel_type = channel_type;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Long getCat_id() {
        return cat_id;
    }

    public void setCat_id(Long cat_id) {
        this.cat_id = cat_id;
    }

    public List<Long> getGoods_ids() {
        return goods_ids;
    }

    public void setGoods_ids(List<Long> goods_ids) {
        this.goods_ids = goods_ids;
    }

    @Override
    public String toString() {
        return "PddGoodsRecommendReq{" +
                "channel_type=" + channel_type +
                ", custom_parameters='" + custom_parameters + '\'' +
                ", limit=" + limit +
                ", list_id='" + list_id + '\'' +
                ", offset=" + offset +
                ", pid='" + pid + '\'' +
                ", cat_id=" + cat_id +
                ", goods_ids=" + goods_ids +
                '}';
    }
}
