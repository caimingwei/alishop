package com.alipay.rebate.shop.pojo.pdtframework;

import javax.validation.constraints.NotNull;

public class TaobaoJingPin {

//  @NotNull(message = "jdk 不能为空")
  private Integer jpk;
  @NotNull(message = "物料id 不能为空")
  private Long jpkCategory;


  public Integer getJpk() {
    return jpk;
  }

  public void setJpk(Integer jpk) {
    this.jpk = jpk;
  }

  public Long getJpkCategory() {
    return jpkCategory;
  }

  public void setJpkCategory(Long jpkCategory) {
    this.jpkCategory = jpkCategory;
  }

  @Override
  public String toString() {
    return "TaobaoJingPin{" +
        "jpk=" + jpk +
        ", jpkCategory=" + jpkCategory +
        '}';
  }
}
