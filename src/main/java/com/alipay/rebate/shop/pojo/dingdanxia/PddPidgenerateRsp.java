package com.alipay.rebate.shop.pojo.dingdanxia;

public class PddPidgenerateRsp {

    private Long create_time;
    private String p_id;
    private String pid_name;

    public Long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Long create_time) {
        this.create_time = create_time;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getPid_name() {
        return pid_name;
    }

    public void setPid_name(String pid_name) {
        this.pid_name = pid_name;
    }

    @Override
    public String toString() {
        return "PddPidgenerateRsp{" +
                "create_time=" + create_time +
                ", p_id='" + p_id + '\'' +
                ", pid_name='" + pid_name + '\'' +
                '}';
    }
}
