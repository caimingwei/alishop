package com.alipay.rebate.shop.pojo.haodanku;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HaodankuProduct {

  private Long product_id;
  private Long itemid;
  private String itemtitle;
  private String itemshorttitle;
  private String itemdesc;
  private BigDecimal itemprice;
  private Integer itemsale;
  private Integer itemsale2;
  private Integer todaysale;
  private String itempic;
  private String itempic_copy;
  private Long fqcat;
  private BigDecimal itemendprice;
  private String shoptype;
  private String couponurl;
  private BigDecimal couponmoney;
  private Integer is_brand;
  private Integer is_live;
  private String guide_article;
  private Long videoid;
  private String activity_type;
  private String planlink;
  private Long userid;
  private String sellernick;
  private String shopname;
  private String tktype;
  private BigDecimal tkrates;
  private Integer cuntao;
  private BigDecimal tkmoney;
  private Integer couponreceive2;
  private Integer couponsurplus;
  private Integer couponnum;
  private String couponexplain;
  private Long couponstarttime;
  private Long couponendtime;
  private Long start_time;
  private Long end_time;
  private Long starttime;
  private Integer report_status;
  private Integer general_index;
  private String seller_name;
  private BigDecimal discount;

  public Long getProduct_id() {
    return product_id;
  }

  public void setProduct_id(Long product_id) {
    this.product_id = product_id;
  }

  public Long getItemid() {
    return itemid;
  }

  public void setItemid(Long itemid) {
    this.itemid = itemid;
  }

  public String getItemtitle() {
    return itemtitle;
  }

  public void setItemtitle(String itemtitle) {
    this.itemtitle = itemtitle;
  }

  public String getItemshorttitle() {
    return itemshorttitle;
  }

  public void setItemshorttitle(String itemshorttitle) {
    this.itemshorttitle = itemshorttitle;
  }

  public String getItemdesc() {
    return itemdesc;
  }

  public void setItemdesc(String itemdesc) {
    this.itemdesc = itemdesc;
  }

  public BigDecimal getItemprice() {
    return itemprice;
  }

  public void setItemprice(BigDecimal itemprice) {
    this.itemprice = itemprice;
  }

  public Integer getItemsale() {
    return itemsale;
  }

  public void setItemsale(Integer itemsale) {
    this.itemsale = itemsale;
  }

  public Integer getItemsale2() {
    return itemsale2;
  }

  public void setItemsale2(Integer itemsale2) {
    this.itemsale2 = itemsale2;
  }

  public Integer getTodaysale() {
    return todaysale;
  }

  public void setTodaysale(Integer todaysale) {
    this.todaysale = todaysale;
  }

  public String getItempic() {
    return itempic;
  }

  public void setItempic(String itempic) {
    this.itempic = itempic;
  }

  public String getItempic_copy() {
    return itempic_copy;
  }

  public void setItempic_copy(String itempic_copy) {
    this.itempic_copy = itempic_copy;
  }

  public Long getFqcat() {
    return fqcat;
  }

  public void setFqcat(Long fqcat) {
    this.fqcat = fqcat;
  }

  public BigDecimal getItemendprice() {
    return itemendprice;
  }

  public void setItemendprice(BigDecimal itemendprice) {
    this.itemendprice = itemendprice;
  }

  public String getShoptype() {
    return shoptype;
  }

  public void setShoptype(String shoptype) {
    this.shoptype = shoptype;
  }

  public String getCouponurl() {
    return couponurl;
  }

  public void setCouponurl(String couponurl) {
    this.couponurl = couponurl;
  }

  public BigDecimal getCouponmoney() {
    return couponmoney;
  }

  public void setCouponmoney(BigDecimal couponmoney) {
    this.couponmoney = couponmoney;
  }

  public Integer getIs_brand() {
    return is_brand;
  }

  public void setIs_brand(Integer is_brand) {
    this.is_brand = is_brand;
  }

  public Integer getIs_live() {
    return is_live;
  }

  public void setIs_live(Integer is_live) {
    this.is_live = is_live;
  }

  public String getGuide_article() {
    return guide_article;
  }

  public void setGuide_article(String guide_article) {
    this.guide_article = guide_article;
  }

  public Long getVideoid() {
    return videoid;
  }

  public void setVideoid(Long videoid) {
    this.videoid = videoid;
  }

  public String getActivity_type() {
    return activity_type;
  }

  public void setActivity_type(String activity_type) {
    this.activity_type = activity_type;
  }

  public String getPlanlink() {
    return planlink;
  }

  public void setPlanlink(String planlink) {
    this.planlink = planlink;
  }

  public Long getUserid() {
    return userid;
  }

  public void setUserid(Long userid) {
    this.userid = userid;
  }

  public String getSellernick() {
    return sellernick;
  }

  public void setSellernick(String sellernick) {
    this.sellernick = sellernick;
  }

  public String getShopname() {
    return shopname;
  }

  public void setShopname(String shopname) {
    this.shopname = shopname;
  }

  public String getTktype() {
    return tktype;
  }

  public void setTktype(String tktype) {
    this.tktype = tktype;
  }

  public BigDecimal getTkrates() {
    return tkrates;
  }

  public void setTkrates(BigDecimal tkrates) {
    this.tkrates = tkrates;
  }

  public Integer getCuntao() {
    return cuntao;
  }

  public void setCuntao(Integer cuntao) {
    this.cuntao = cuntao;
  }

  public BigDecimal getTkmoney() {
    return tkmoney;
  }

  public void setTkmoney(BigDecimal tkmoney) {
    this.tkmoney = tkmoney;
  }

  public Integer getCouponreceive2() {
    return couponreceive2;
  }

  public void setCouponreceive2(Integer couponreceive2) {
    this.couponreceive2 = couponreceive2;
  }

  public Integer getCouponsurplus() {
    return couponsurplus;
  }

  public void setCouponsurplus(Integer couponsurplus) {
    this.couponsurplus = couponsurplus;
  }

  public Integer getCouponnum() {
    return couponnum;
  }

  public void setCouponnum(Integer couponnum) {
    this.couponnum = couponnum;
  }

  public String getCouponexplain() {
    return couponexplain;
  }

  public void setCouponexplain(String couponexplain) {
    this.couponexplain = couponexplain;
  }

  public Long getCouponstarttime() {
    return couponstarttime;
  }

  public void setCouponstarttime(Long couponstarttime) {
    this.couponstarttime = couponstarttime;
  }

  public Long getCouponendtime() {
    return couponendtime;
  }

  public void setCouponendtime(Long couponendtime) {
    this.couponendtime = couponendtime;
  }

  public Long getStart_time() {
    return start_time;
  }

  public void setStart_time(Long start_time) {
    this.start_time = start_time;
  }

  public Long getEnd_time() {
    return end_time;
  }

  public void setEnd_time(Long end_time) {
    this.end_time = end_time;
  }

  public Long getStarttime() {
    return starttime;
  }

  public void setStarttime(Long starttime) {
    this.starttime = starttime;
  }

  public Integer getReport_status() {
    return report_status;
  }

  public void setReport_status(Integer report_status) {
    this.report_status = report_status;
  }

  public Integer getGeneral_index() {
    return general_index;
  }

  public void setGeneral_index(Integer general_index) {
    this.general_index = general_index;
  }

  public String getSeller_name() {
    return seller_name;
  }

  public void setSeller_name(String seller_name) {
    this.seller_name = seller_name;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  @Override
  public String toString() {
    return "HaodankuProduct{" +
        "product_id=" + product_id +
        ", itemid=" + itemid +
        ", itemtitle='" + itemtitle + '\'' +
        ", itemshorttitle='" + itemshorttitle + '\'' +
        ", itemdesc='" + itemdesc + '\'' +
        ", itemprice=" + itemprice +
        ", itemsale=" + itemsale +
        ", itemsale2=" + itemsale2 +
        ", todaysale=" + todaysale +
        ", itempic='" + itempic + '\'' +
        ", itempic_copy='" + itempic_copy + '\'' +
        ", fqcat=" + fqcat +
        ", itemendprice=" + itemendprice +
        ", shoptype='" + shoptype + '\'' +
        ", couponurl='" + couponurl + '\'' +
        ", couponmoney=" + couponmoney +
        ", is_brand=" + is_brand +
        ", is_live=" + is_live +
        ", guide_article='" + guide_article + '\'' +
        ", videoid=" + videoid +
        ", activity_type='" + activity_type + '\'' +
        ", planlink='" + planlink + '\'' +
        ", userid=" + userid +
        ", sellernick='" + sellernick + '\'' +
        ", shopname='" + shopname + '\'' +
        ", tktype='" + tktype + '\'' +
        ", tkrates=" + tkrates +
        ", cuntao=" + cuntao +
        ", tkmoney=" + tkmoney +
        ", couponreceive2=" + couponreceive2 +
        ", couponsurplus=" + couponsurplus +
        ", couponnum=" + couponnum +
        ", couponexplain='" + couponexplain + '\'' +
        ", couponstarttime=" + couponstarttime +
        ", couponendtime=" + couponendtime +
        ", start_time=" + start_time +
        ", end_time=" + end_time +
        ", starttime=" + starttime +
        ", report_status=" + report_status +
        ", general_index=" + general_index +
        ", seller_name='" + seller_name + '\'' +
        ", discount=" + discount +
        '}';
  }
}
