package com.alipay.rebate.shop.pojo.user.admin;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PageFrameworkRequest {

  private Long pageId;
  private String title;
  private String illustration;
//  private List<PageModule> pageModules;
  @NotNull(message = "模块不能为空")
  @NotEmpty(message = "模块不能为空")
  private String pageModules;

  private Integer isIndex;
  private String startColor;
  private String endColor;

  public String getStartColor() {
    return startColor;
  }

  public void setStartColor(String startColor) {
    this.startColor = startColor;
  }

  public String getEndColor() {
    return endColor;
  }

  public void setEndColor(String endColor) {
    this.endColor = endColor;
  }

  public Integer getIsIndex() {
    return isIndex;
  }

  public void setIsIndex(Integer isIndex) {
    this.isIndex = isIndex;
  }

  public Long getPageId() {
    return pageId;
  }

  public void setPageId(Long pageId) {
    this.pageId = pageId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIllustration() {
    return illustration;
  }

  public void setIllustration(String illustration) {
    this.illustration = illustration;
  }

  public String getPageModules() {
    return pageModules;
  }

  public void setPageModules(String pageModules) {
    this.pageModules = pageModules;
  }

  @Override
  public String toString() {
    return "PageFrameworkRequest{" +
        "pageId=" + pageId +
        ", title='" + title + '\'' +
        ", illustration='" + illustration + '\'' +
        ", pageModules=" + pageModules +
        '}';
  }
}
