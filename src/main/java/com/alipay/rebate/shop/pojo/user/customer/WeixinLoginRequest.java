package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.NotNull;

public class WeixinLoginRequest {

    @NotNull(message = "openId 不能为空")
    private String openId;
    private Integer loginPlatform;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Integer getLoginPlatform() {
        return loginPlatform;
    }

    public void setLoginPlatform(Integer loginPlatform) {
        this.loginPlatform = loginPlatform;
    }

    @Override
    public String toString() {
        return "WeixinLoginRequest{" +
                "openId='" + openId + '\'' +
                '}';
    }
}
