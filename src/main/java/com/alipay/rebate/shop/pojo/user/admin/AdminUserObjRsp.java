package com.alipay.rebate.shop.pojo.user.admin;

import java.util.Date;

public class AdminUserObjRsp {

  private Long id;

  private String userAccount;

  private String userPassword;

  private Date lastLoginTime;

  private Date newlyLoginTime;

  private Integer loginTimes;

  private Date userRegisterTime;

  private String userRegisterIp;

  private String lastLoginIp;

  private String newlyLoginIp;

  private Integer groupId;

  private Date createTime;

  private Date updateTime;

  private String groupName;

  private String permission;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserAccount() {
    return userAccount;
  }

  public void setUserAccount(String userAccount) {
    this.userAccount = userAccount;
  }

  public String getUserPassword() {
    return userPassword;
  }

  public void setUserPassword(String userPassword) {
    this.userPassword = userPassword;
  }

  public Date getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(Date lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public Date getNewlyLoginTime() {
    return newlyLoginTime;
  }

  public void setNewlyLoginTime(Date newlyLoginTime) {
    this.newlyLoginTime = newlyLoginTime;
  }

  public Integer getLoginTimes() {
    return loginTimes;
  }

  public void setLoginTimes(Integer loginTimes) {
    this.loginTimes = loginTimes;
  }

  public Date getUserRegisterTime() {
    return userRegisterTime;
  }

  public void setUserRegisterTime(Date userRegisterTime) {
    this.userRegisterTime = userRegisterTime;
  }

  public String getUserRegisterIp() {
    return userRegisterIp;
  }

  public void setUserRegisterIp(String userRegisterIp) {
    this.userRegisterIp = userRegisterIp;
  }

  public String getLastLoginIp() {
    return lastLoginIp;
  }

  public void setLastLoginIp(String lastLoginIp) {
    this.lastLoginIp = lastLoginIp;
  }

  public String getNewlyLoginIp() {
    return newlyLoginIp;
  }

  public void setNewlyLoginIp(String newlyLoginIp) {
    this.newlyLoginIp = newlyLoginIp;
  }

  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getPermission() {
    return permission;
  }

  public void setPermission(String permission) {
    this.permission = permission;
  }

  @Override
  public String toString() {
    return "AdminUserObjRsp{" +
        "id=" + id +
        ", userAccount='" + userAccount + '\'' +
        ", userPassword='" + userPassword + '\'' +
        ", lastLoginTime=" + lastLoginTime +
        ", newlyLoginTime=" + newlyLoginTime +
        ", loginTimes=" + loginTimes +
        ", userRegisterTime=" + userRegisterTime +
        ", userRegisterIp='" + userRegisterIp + '\'' +
        ", lastLoginIp='" + lastLoginIp + '\'' +
        ", newlyLoginIp='" + newlyLoginIp + '\'' +
        ", groupId=" + groupId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", groupName='" + groupName + '\'' +
        ", permission='" + permission + '\'' +
        '}';
  }
}
