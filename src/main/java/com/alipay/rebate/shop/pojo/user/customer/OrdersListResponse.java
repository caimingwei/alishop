package com.alipay.rebate.shop.pojo.user.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

public class OrdersListResponse {

    private String tradeId;
    private String orderType;
    private Long tkStatus;
    private String itemImg;
    private String itemTitle;
    private Long createTime;
    private Long userUit;
    private BigDecimal price;
    private Long numIid;
    @JsonIgnore
    private BigDecimal userCommission;
    // 是否已经到账
    private Integer isPaid;
//    @JsonIgnore
    private Long userId;
    private Long relationId;
    private String unfreezingDate;
    private Long promoterId;
    private Integer privacyProtection;

    public Integer getPrivacyProtection() {
        return privacyProtection;
    }

    public void setPrivacyProtection(Integer privacyProtection) {
        this.privacyProtection = privacyProtection;
    }

    public Long getPromoterId() {
        return promoterId;
    }

    public void setPromoterId(Long promoterId) {
        this.promoterId = promoterId;
    }

    public String getUnfreezingDate() {
        return unfreezingDate;
    }

    public void setUnfreezingDate(String unfreezingDate) {
        this.unfreezingDate = unfreezingDate;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Long getTkStatus() {
        return tkStatus;
    }

    public void setTkStatus(Long tkStatus) {
        this.tkStatus = tkStatus;
    }

    public String getItemImg() {
        return itemImg;
    }

    public void setItemImg(String itemImg) {
        this.itemImg = itemImg;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUserUit() {
        return userUit;
    }

    public void setUserUit(Long userUit) {
        this.userUit = userUit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public BigDecimal getUserCommission() {
        return userCommission;
    }

    public void setUserCommission(BigDecimal userCommission) {
        this.userCommission = userCommission;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getNumIid() {
        return numIid;
    }

    public void setNumIid(Long numIid) {
        this.numIid = numIid;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    @Override
    public String toString() {
        return "OrdersListResponse{" +
                "tradeId='" + tradeId + '\'' +
                ", orderType='" + orderType + '\'' +
                ", tkStatus=" + tkStatus +
                ", itemImg='" + itemImg + '\'' +
                ", itemTitle='" + itemTitle + '\'' +
                ", createTime=" + createTime +
                ", userUit=" + userUit +
                ", price=" + price +
                ", numIid=" + numIid +
                ", userCommission=" + userCommission +
                ", isPaid=" + isPaid +
                ", userId=" + userId +
                ", relationId=" + relationId +
                ", unfreezingDate='" + unfreezingDate + '\'' +
                ", promoterId=" + promoterId +
                ", privacyProtection=" + privacyProtection +
                '}';
    }
}
