package com.alipay.rebate.shop.pojo.smsgoupsend;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;

public class SmsGroupSendSearchReq extends CommonPageRequest {

  private Integer sendObj;
  private Integer status;
  private String startTime;
  private String endTime;

  public Integer getSendObj() {
    return sendObj;
  }

  public void setSendObj(Integer sendObj) {
    this.sendObj = sendObj;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  @Override
  public String toString() {
    return "SmsGroupSendSearchReq{" +
        "sendObj=" + sendObj +
        ", status=" + status +
        ", startTime='" + startTime + '\'' +
        ", endTime='" + endTime + '\'' +
        "} " + super.toString();
  }
}
