package com.alipay.rebate.shop.pojo.user.customer;

import com.alipay.rebate.shop.constants.PatternContant;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class UpdatePasswordRequest {


  @NotEmpty(message = "手机号码不能为空")
  @Pattern(regexp= PatternContant.PHONE_PATTERN,
      message=PatternContant.PHONE_PATTERN_ERROR_MESSAGE)
  private String userPhone;
  @NotEmpty(message = "验证码不能为空")
  private String mobileCode;
  @NotEmpty(message = "密码不能为空")
  private String password;

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public String getMobileCode() {
    return mobileCode;
  }

  public void setMobileCode(String mobileCode) {
    this.mobileCode = mobileCode;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return "UpdatePasswordRequest{" +
        "userPhone='" + userPhone + '\'' +
        ", mobileCode='" + mobileCode + '\'' +
        ", password='" + password + '\'' +
        '}';
  }
}
