package com.alipay.rebate.shop.pojo;

public class UserId {
    private String uid;
    private String invId;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getInvId() {
        return invId;
    }

    public void setInvId(String invId) {
        this.invId = invId;
    }

    @Override
    public String toString() {
        return "UserId{" +
                "uid='" + uid + '\'' +
                ", invId='" + invId + '\'' +
                '}';
    }
}
