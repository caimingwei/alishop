package com.alipay.rebate.shop.pojo.user.customer;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class AmountWithdrawRequest {

    @NotNull(message = "提现金额不能为空")
    private BigDecimal withDrawAmount;

    public BigDecimal getWithDrawAmount() {
        return withDrawAmount;
    }

    public void setWithDrawAmount(BigDecimal withDrawAmount) {
        this.withDrawAmount = withDrawAmount;
    }

    @Override
    public String toString() {
        return "AmountWithdrawRequest{" +
                "withDrawAmount=" + withDrawAmount +
                '}';
    }
}
