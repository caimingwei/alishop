package com.alipay.rebate.shop.pojo.dataoke;

import java.util.List;

public class SuperCategoryResponse {

    private String time;
    private Integer code;
    private String msg;
    private List<SuperCategoryResp> data;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<SuperCategoryResp> getData() {
        return data;
    }

    public void setData(List<SuperCategoryResp> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SuperCategoryResponse{" +
                "time='" + time + '\'' +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
