package com.alipay.rebate.shop.pojo.entitysettings;

public class InrUser implements Comparable<InrUser>{

  private String userName;
  private String headImage;
  private Integer inviteNum;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getHeadImage() {
    return headImage;
  }

  public void setHeadImage(String headImage) {
    this.headImage = headImage;
  }

  public Integer getInviteNum() {
    return inviteNum;
  }

  public void setInviteNum(Integer inviteNum) {
    this.inviteNum = inviteNum;
  }

  @Override
  public int compareTo(InrUser o) {
    return inviteNum - o.inviteNum;
  }

  @Override
  public String toString() {
    return "InrUser{" +
        "userName='" + userName + '\'' +
        ", headImage='" + headImage + '\'' +
        ", inviteNum=" + inviteNum +
        '}';
  }
}
