package com.alipay.rebate.shop.pojo.dingdanxia;

import javax.validation.constraints.NotNull;

public class TklCreateTwoReq {

    private String apikey;
    private String signature;
    private String url;
    private String text;
    private String logo;
    @NotNull
    private String salt;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toString() {
        return "TklCreateTwoReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", url='" + url + '\'' +
                ", text='" + text + '\'' +
                ", logo='" + logo + '\'' +
                ", salt='" + salt + '\'' +
                '}';
    }
}
