package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.Arrays;

public class CreatePositionReq {

    private String apikey;
    private String signature;
    private Long unionId;
    private String key;
    private Integer unionType;
    private Integer type;
    private String[] spaceNameList;
    private Long siteId;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getUnionId() {
        return unionId;
    }

    public void setUnionId(Long unionId) {
        this.unionId = unionId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getUnionType() {
        return unionType;
    }

    public void setUnionType(Integer unionType) {
        this.unionType = unionType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String[] getSpaceNameList() {
        return spaceNameList;
    }

    public void setSpaceNameList(String[] spaceNameList) {
        this.spaceNameList = spaceNameList;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Override
    public String toString() {
        return "CreatePositionReq{" +
                "apikey='" + apikey + '\'' +
                ", signature='" + signature + '\'' +
                ", unionId=" + unionId +
                ", key='" + key + '\'' +
                ", unionType=" + unionType +
                ", type=" + type +
                ", spaceNameList=" + Arrays.toString(spaceNameList) +
                ", siteId=" + siteId +
                '}';
    }
}
