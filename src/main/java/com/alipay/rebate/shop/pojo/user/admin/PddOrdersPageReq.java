package com.alipay.rebate.shop.pojo.user.admin;

public class PddOrdersPageReq {

    private String orderSn;
    private String goodsName;
    private String orderCreateTime;
    private String endCreateTime;
    private Long userId;
    private String userName;
    private Long orderStatus;

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Long orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "PddOrdersPageReq{" +
                "orderSd='" + orderSn + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", orderCreateTime='" + orderCreateTime + '\'' +
                ", endCreateTime='" + endCreateTime + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", orderStatus=" + orderStatus +
                '}';
    }
}
