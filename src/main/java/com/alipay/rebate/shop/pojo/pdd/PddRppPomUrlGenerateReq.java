package com.alipay.rebate.shop.pojo.pdd;

import java.util.List;

public class PddRppPomUrlGenerateReq {

    private Integer channel_type;
    private String custom_parameters;
    private List<Long> amount_probability;
    private Boolean dis_text;
    private Boolean not_show_background;
    private Integer opt_id;
    private Long range_from;
    private Integer range_id;
    private Long range_to;
    private Boolean generate_qq_app;
    private Boolean generate_schema_url;
    private Boolean generate_short_url;
    private Boolean generate_we_app;
    private List<String> p_id_list;


    public Integer getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(Integer channel_type) {
        this.channel_type = channel_type;
    }

    public String getCustom_parameters() {
        return custom_parameters;
    }

    public void setCustom_parameters(String custom_parameters) {
        this.custom_parameters = custom_parameters;
    }

    public List<Long> getAmount_probability() {
        return amount_probability;
    }

    public void setAmount_probability(List<Long> amount_probability) {
        this.amount_probability = amount_probability;
    }

    public Boolean getDis_text() {
        return dis_text;
    }

    public void setDis_text(Boolean dis_text) {
        this.dis_text = dis_text;
    }

    public Boolean getNot_show_background() {
        return not_show_background;
    }

    public void setNot_show_background(Boolean not_show_background) {
        this.not_show_background = not_show_background;
    }

    public Integer getOpt_id() {
        return opt_id;
    }

    public void setOpt_id(Integer opt_id) {
        this.opt_id = opt_id;
    }

    public Long getRange_from() {
        return range_from;
    }

    public void setRange_from(Long range_from) {
        this.range_from = range_from;
    }

    public Integer getRange_id() {
        return range_id;
    }

    public void setRange_id(Integer range_id) {
        this.range_id = range_id;
    }

    public Long getRange_to() {
        return range_to;
    }

    public void setRange_to(Long range_to) {
        this.range_to = range_to;
    }

    public Boolean getGenerate_qq_app() {
        return generate_qq_app;
    }

    public void setGenerate_qq_app(Boolean generate_qq_app) {
        this.generate_qq_app = generate_qq_app;
    }

    public Boolean getGenerate_schema_url() {
        return generate_schema_url;
    }

    public void setGenerate_schema_url(Boolean generate_schema_url) {
        this.generate_schema_url = generate_schema_url;
    }

    public Boolean getGenerate_short_url() {
        return generate_short_url;
    }

    public void setGenerate_short_url(Boolean generate_short_url) {
        this.generate_short_url = generate_short_url;
    }

    public Boolean getGenerate_we_app() {
        return generate_we_app;
    }

    public void setGenerate_we_app(Boolean generate_we_app) {
        this.generate_we_app = generate_we_app;
    }

    public List<String> getP_id_list() {
        return p_id_list;
    }

    public void setP_id_list(List<String> p_id_list) {
        this.p_id_list = p_id_list;
    }

    @Override
    public String toString() {
        return "PddRppPomUrlGenerateReq{" +
                "channel_type=" + channel_type +
                ", custom_parameters='" + custom_parameters + '\'' +
                ", amount_probability=" + amount_probability +
                ", dis_text=" + dis_text +
                ", not_show_background=" + not_show_background +
                ", opt_id=" + opt_id +
                ", range_from=" + range_from +
                ", range_id=" + range_id +
                ", range_to=" + range_to +
                ", generate_qq_app=" + generate_qq_app +
                ", generate_schema_url=" + generate_schema_url +
                ", generate_short_url=" + generate_short_url +
                ", generate_we_app=" + generate_we_app +
                ", p_id_list=" + p_id_list +
                '}';
    }
}
