package com.alipay.rebate.shop.pojo.jd;

import java.util.Arrays;
import java.util.List;

public class TkJdOrdersResponse {

    private String ext1;
    private Long finishTime;
    private Integer orderEmt;
    private Long orderId;
    private Long orderTime;
    private Long parentId;
    private String payMonth;
    private Integer plus;
    private Long popId;
    private List<SkuInfo> skuList;
    private Long unionId;
    private Integer validCode;


    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    public Long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Long finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getOrderEmt() {
        return orderEmt;
    }

    public void setOrderEmt(Integer orderEmt) {
        this.orderEmt = orderEmt;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPayMonth() {
        return payMonth;
    }

    public void setPayMonth(String payMonth) {
        this.payMonth = payMonth;
    }

    public Integer getPlus() {
        return plus;
    }

    public void setPlus(Integer plus) {
        this.plus = plus;
    }

    public Long getPopId() {
        return popId;
    }

    public void setPopId(Long popId) {
        this.popId = popId;
    }

    public List<SkuInfo> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<SkuInfo> skuList) {
        this.skuList = skuList;
    }

    public Long getUnionId() {
        return unionId;
    }

    public void setUnionId(Long unionId) {
        this.unionId = unionId;
    }

    public Integer getValidCode() {
        return validCode;
    }

    public void setValidCode(Integer validCode) {
        this.validCode = validCode;
    }

    @Override
    public String toString() {
        return "TkJdOrdersResponse{" +
                "ext1='" + ext1 + '\'' +
                ", finishTime=" + finishTime +
                ", orderEmt=" + orderEmt +
                ", orderId=" + orderId +
                ", orderTime=" + orderTime +
                ", parentId=" + parentId +
                ", payMonth='" + payMonth + '\'' +
                ", plus=" + plus +
                ", popId=" + popId +
                ", skuList=" + skuList +
                ", unionId=" + unionId +
                ", validCode=" + validCode +
                '}';
    }
}
