package com.alipay.rebate.shop.pojo;

import com.alipay.rebate.shop.pojo.adminindex.SevenDayInnerNumber;

import java.math.BigDecimal;
import java.util.List;

public class AdminIndexRsp {

  private Integer totalUserNumber;
  private Integer todayRegisterUserNumber;
  private Integer yesterdayRegisterUserNumber;
  private Integer totalOrderNumber;
  private Integer todayPaidOrderNumber;
  private Integer todayACOrderNumber;
  private BigDecimal totalCm;
  private BigDecimal todayPaidCm;
  private BigDecimal todayACCm;
  private Integer todayWithDrawNumber;
  private Integer yesterDayPaidCount;
  private Integer yesterDayACCount;
  private Integer currentNotHanleWithdrawNumber;
  private List<SevenDayInnerNumber> sevenDayInnerNumber;
  private Integer totalPddOrdersNum;
  private Integer todayPaidPddOrderNum;
  private Integer yesterdayPaidPddOrdersNmu;
  private BigDecimal pddOrdersTotalCommission;
  private BigDecimal todayPaidPddOrdersCommissionFee;
  private BigDecimal yesterdayPaidPddOrdersCommissionFee;
  private Integer totalJdOrdersNum;
  private Integer todayPaidjdOrderNum;
  private Integer yesterdayPaidJdOrdersNmu;
  private BigDecimal jdOrdersTotalCommission;
  private BigDecimal todayPaidJdOrdersCommissionFee;
  private BigDecimal yesterdayPaidJdOrdersCommissionFee;

  public Integer getTotalPddOrdersNum() {
    return totalPddOrdersNum;
  }

  public void setTotalPddOrdersNum(Integer totalPddOrdersNum) {
    this.totalPddOrdersNum = totalPddOrdersNum;
  }

  public Integer getTodayPaidPddOrderNum() {
    return todayPaidPddOrderNum;
  }

  public void setTodayPaidPddOrderNum(Integer todayPaidPddOrderNum) {
    this.todayPaidPddOrderNum = todayPaidPddOrderNum;
  }

  public Integer getYesterdayPaidPddOrdersNmu() {
    return yesterdayPaidPddOrdersNmu;
  }

  public void setYesterdayPaidPddOrdersNmu(Integer yesterdayPaidPddOrdersNmu) {
    this.yesterdayPaidPddOrdersNmu = yesterdayPaidPddOrdersNmu;
  }

  public BigDecimal getPddOrdersTotalCommission() {
    return pddOrdersTotalCommission;
  }

  public void setPddOrdersTotalCommission(BigDecimal pddOrdersTotalCommission) {
    this.pddOrdersTotalCommission = pddOrdersTotalCommission;
  }

  public BigDecimal getTodayPaidPddOrdersCommissionFee() {
    return todayPaidPddOrdersCommissionFee;
  }

  public void setTodayPaidPddOrdersCommissionFee(BigDecimal todayPaidPddOrdersCommissionFee) {
    this.todayPaidPddOrdersCommissionFee = todayPaidPddOrdersCommissionFee;
  }

  public BigDecimal getYesterdayPaidPddOrdersCommissionFee() {
    return yesterdayPaidPddOrdersCommissionFee;
  }

  public void setYesterdayPaidPddOrdersCommissionFee(BigDecimal yesterdayPaidPddOrdersCommissionFee) {
    this.yesterdayPaidPddOrdersCommissionFee = yesterdayPaidPddOrdersCommissionFee;
  }

  public Integer getTotalJdOrdersNum() {
    return totalJdOrdersNum;
  }

  public void setTotalJdOrdersNum(Integer totalJdOrdersNum) {
    this.totalJdOrdersNum = totalJdOrdersNum;
  }

  public Integer getTodayPaidjdOrderNum() {
    return todayPaidjdOrderNum;
  }

  public void setTodayPaidjdOrderNum(Integer todayPaidjdOrderNum) {
    this.todayPaidjdOrderNum = todayPaidjdOrderNum;
  }

  public Integer getYesterdayPaidJdOrdersNmu() {
    return yesterdayPaidJdOrdersNmu;
  }

  public void setYesterdayPaidJdOrdersNmu(Integer yesterdayPaidJdOrdersNmu) {
    this.yesterdayPaidJdOrdersNmu = yesterdayPaidJdOrdersNmu;
  }

  public BigDecimal getJdOrdersTotalCommission() {
    return jdOrdersTotalCommission;
  }

  public void setJdOrdersTotalCommission(BigDecimal jdOrdersTotalCommission) {
    this.jdOrdersTotalCommission = jdOrdersTotalCommission;
  }

  public BigDecimal getTodayPaidJdOrdersCommissionFee() {
    return todayPaidJdOrdersCommissionFee;
  }

  public void setTodayPaidJdOrdersCommissionFee(BigDecimal todayPaidJdOrdersCommissionFee) {
    this.todayPaidJdOrdersCommissionFee = todayPaidJdOrdersCommissionFee;
  }

  public BigDecimal getYesterdayPaidJdOrdersCommissionFee() {
    return yesterdayPaidJdOrdersCommissionFee;
  }

  public void setYesterdayPaidJdOrdersCommissionFee(BigDecimal yesterdayPaidJdOrdersCommissionFee) {
    this.yesterdayPaidJdOrdersCommissionFee = yesterdayPaidJdOrdersCommissionFee;
  }

  public Integer getTotalUserNumber() {
    return totalUserNumber;
  }

  public void setTotalUserNumber(Integer totalUserNumber) {
    this.totalUserNumber = totalUserNumber;
  }

  public Integer getTodayRegisterUserNumber() {
    return todayRegisterUserNumber;
  }

  public void setTodayRegisterUserNumber(Integer todayRegisterUserNumber) {
    this.todayRegisterUserNumber = todayRegisterUserNumber;
  }

  public Integer getYesterdayRegisterUserNumber() {
    return yesterdayRegisterUserNumber;
  }

  public void setYesterdayRegisterUserNumber(Integer yesterdayRegisterUserNumber) {
    this.yesterdayRegisterUserNumber = yesterdayRegisterUserNumber;
  }

  public Integer getTotalOrderNumber() {
    return totalOrderNumber;
  }

  public void setTotalOrderNumber(Integer totalOrderNumber) {
    this.totalOrderNumber = totalOrderNumber;
  }

  public Integer getTodayPaidOrderNumber() {
    return todayPaidOrderNumber;
  }

  public void setTodayPaidOrderNumber(Integer todayPaidOrderNumber) {
    this.todayPaidOrderNumber = todayPaidOrderNumber;
  }

  public Integer getTodayACOrderNumber() {
    return todayACOrderNumber;
  }

  public void setTodayACOrderNumber(Integer todayACOrderNumber) {
    this.todayACOrderNumber = todayACOrderNumber;
  }

  public BigDecimal getTotalCm() {
    return totalCm;
  }

  public void setTotalCm(BigDecimal totalCm) {
    this.totalCm = totalCm;
  }

  public BigDecimal getTodayPaidCm() {
    return todayPaidCm;
  }

  public void setTodayPaidCm(BigDecimal todayPaidCm) {
    this.todayPaidCm = todayPaidCm;
  }

  public BigDecimal getTodayACCm() {
    return todayACCm;
  }

  public void setTodayACCm(BigDecimal todayACCm) {
    this.todayACCm = todayACCm;
  }

  public Integer getTodayWithDrawNumber() {
    return todayWithDrawNumber;
  }

  public void setTodayWithDrawNumber(Integer todayWithDrawNumber) {
    this.todayWithDrawNumber = todayWithDrawNumber;
  }


  public Integer getYesterDayPaidCount() {
    return yesterDayPaidCount;
  }

  public void setYesterDayPaidCount(Integer yesTerDayPaidCount) {
    this.yesterDayPaidCount = yesTerDayPaidCount;
  }

  public Integer getYesterDayACCount() {
    return yesterDayACCount;
  }

  public void setYesterDayACCount(Integer yesTerdayACCount) {
    this.yesterDayACCount = yesTerdayACCount;
  }

  public Integer getCurrentNotHanleWithdrawNumber() {
    return currentNotHanleWithdrawNumber;
  }

  public void setCurrentNotHanleWithdrawNumber(Integer currentNotHanleWithdrawNumber) {
    this.currentNotHanleWithdrawNumber = currentNotHanleWithdrawNumber;
  }

  public List<SevenDayInnerNumber> getSevenDayInnerNumber() {
    return sevenDayInnerNumber;
  }

  public void setSevenDayInnerNumber(List<SevenDayInnerNumber> sevenDayInnerNumber) {
    this.sevenDayInnerNumber = sevenDayInnerNumber;
  }

  @Override
  public String toString() {
    return "AdminIndexRsp{" +
            "totalUserNumber=" + totalUserNumber +
            ", todayRegisterUserNumber=" + todayRegisterUserNumber +
            ", yesterdayRegisterUserNumber=" + yesterdayRegisterUserNumber +
            ", totalOrderNumber=" + totalOrderNumber +
            ", todayPaidOrderNumber=" + todayPaidOrderNumber +
            ", todayACOrderNumber=" + todayACOrderNumber +
            ", totalCm=" + totalCm +
            ", todayPaidCm=" + todayPaidCm +
            ", todayACCm=" + todayACCm +
            ", todayWithDrawNumber=" + todayWithDrawNumber +
            ", yesterDayPaidCount=" + yesterDayPaidCount +
            ", yesterDayACCount=" + yesterDayACCount +
            ", currentNotHanleWithdrawNumber=" + currentNotHanleWithdrawNumber +
            ", sevenDayInnerNumber=" + sevenDayInnerNumber +
            ", totalPddOrdersNum=" + totalPddOrdersNum +
            ", todayPaidPddOrderNum=" + todayPaidPddOrderNum +
            ", yesterdayPaidPddOrdersNmu=" + yesterdayPaidPddOrdersNmu +
            ", pddOrdersTotalCommission=" + pddOrdersTotalCommission +
            ", todayPaidPddOrdersCommissionFee=" + todayPaidPddOrdersCommissionFee +
            ", yesterdayPaidPddOrdersCommissionFee=" + yesterdayPaidPddOrdersCommissionFee +
            ", totalJdOrdersNum=" + totalJdOrdersNum +
            ", todayPaidjdOrderNum=" + todayPaidjdOrderNum +
            ", yesterdayPaidJdOrdersNmu=" + yesterdayPaidJdOrdersNmu +
            ", jdOrdersTotalCommission=" + jdOrdersTotalCommission +
            ", todayPaidJdOrdersCommissionFee=" + todayPaidJdOrdersCommissionFee +
            ", yesterdayPaidJdOrdersCommissionFee=" + yesterdayPaidJdOrdersCommissionFee +
            '}';
  }
}
