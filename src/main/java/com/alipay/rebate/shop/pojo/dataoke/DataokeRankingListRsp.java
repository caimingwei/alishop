package com.alipay.rebate.shop.pojo.dataoke;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DataokeRankingListRsp {

    private String appKey;
    private String version;
    private Integer rankType;
    private Integer cid;
    private Integer top;
    private String keyWord;
    private Integer hotVal;
    private Integer upVal;
    private Integer id;
    private Integer goodsId;
    private Integer ranking;
    private Integer newRankingGoods;
    private String dtitle;
    private BigDecimal actualPrice;
    private BigDecimal commissionRate;
    private BigDecimal couponPrice;
    private Integer couponReceiveNum;
    private Integer couponTotalNum;
    private Integer monthSales;
    private Integer twoHoursSales;
    private Integer dailySales;
    private Integer hotPush;
    private String mainPic;
    private String title;
    private String desc;
    private BigDecimal originalPrice;
    private String couponLink;
    private Date couponStartTime;
    private Date couponEndTime;
    private Integer commissionType;
    private Date createTime;
    private Integer activityType;
    private List picList;
    private String guideName;
    private Integer shopType;
    private Integer couponConditions;
    private Integer avgSales;
    private Date entryTime;
    private String sellerId;
    private BigDecimal quanMLink;
    private BigDecimal hzQuanOver;
    private Integer yunfeixian;
    private BigDecimal estimateAmount;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getRankType() {
        return rankType;
    }

    public void setRankType(Integer rankType) {
        this.rankType = rankType;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Integer getHotVal() {
        return hotVal;
    }

    public void setHotVal(Integer hotVal) {
        this.hotVal = hotVal;
    }

    public Integer getUpVal() {
        return upVal;
    }

    public void setUpVal(Integer upVal) {
        this.upVal = upVal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Integer getNewRankingGoods() {
        return newRankingGoods;
    }

    public void setNewRankingGoods(Integer newRankingGoods) {
        this.newRankingGoods = newRankingGoods;
    }

    public String getDtitle() {
        return dtitle;
    }

    public void setDtitle(String dtitle) {
        this.dtitle = dtitle;
    }

    public BigDecimal getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Integer getCouponReceiveNum() {
        return couponReceiveNum;
    }

    public void setCouponReceiveNum(Integer couponReceiveNum) {
        this.couponReceiveNum = couponReceiveNum;
    }

    public Integer getCouponTotalNum() {
        return couponTotalNum;
    }

    public void setCouponTotalNum(Integer couponTotalNum) {
        this.couponTotalNum = couponTotalNum;
    }

    public Integer getMonthSales() {
        return monthSales;
    }

    public void setMonthSales(Integer monthSales) {
        this.monthSales = monthSales;
    }

    public Integer getTwoHoursSales() {
        return twoHoursSales;
    }

    public void setTwoHoursSales(Integer twoHoursSales) {
        this.twoHoursSales = twoHoursSales;
    }

    public Integer getDailySales() {
        return dailySales;
    }

    public void setDailySales(Integer dailySales) {
        this.dailySales = dailySales;
    }

    public Integer getHotPush() {
        return hotPush;
    }

    public void setHotPush(Integer hotPush) {
        this.hotPush = hotPush;
    }

    public String getMainPic() {
        return mainPic;
    }

    public void setMainPic(String mainPic) {
        this.mainPic = mainPic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getCouponLink() {
        return couponLink;
    }

    public void setCouponLink(String couponLink) {
        this.couponLink = couponLink;
    }

    public Date getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(Date couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public Date getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(Date couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public Integer getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(Integer commissionType) {
        this.commissionType = commissionType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public List getPicList() {
        return picList;
    }

    public void setPicList(List picList) {
        this.picList = picList;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    public Integer getCouponConditions() {
        return couponConditions;
    }

    public void setCouponConditions(Integer couponConditions) {
        this.couponConditions = couponConditions;
    }

    public Integer getAvgSales() {
        return avgSales;
    }

    public void setAvgSales(Integer avgSales) {
        this.avgSales = avgSales;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public BigDecimal getQuanMLink() {
        return quanMLink;
    }

    public void setQuanMLink(BigDecimal quanMLink) {
        this.quanMLink = quanMLink;
    }

    public BigDecimal getHzQuanOver() {
        return hzQuanOver;
    }

    public void setHzQuanOver(BigDecimal hzQuanOver) {
        this.hzQuanOver = hzQuanOver;
    }

    public Integer getYunfeixian() {
        return yunfeixian;
    }

    public void setYunfeixian(Integer yunfeixian) {
        this.yunfeixian = yunfeixian;
    }

    public BigDecimal getEstimateAmount() {
        return estimateAmount;
    }

    public void setEstimateAmount(BigDecimal estimateAmount) {
        this.estimateAmount = estimateAmount;
    }

    @Override
    public String toString() {
        return "DataokeRankingListRsp{" +
                "appKey='" + appKey + '\'' +
                ", version='" + version + '\'' +
                ", rankType=" + rankType +
                ", cid=" + cid +
                ", top=" + top +
                ", keyWord='" + keyWord + '\'' +
                ", hotVal=" + hotVal +
                ", upVal=" + upVal +
                ", id=" + id +
                ", goodsId=" + goodsId +
                ", ranking=" + ranking +
                ", newRankingGoods=" + newRankingGoods +
                ", dtitle='" + dtitle + '\'' +
                ", actualPrice=" + actualPrice +
                ", commissionRate=" + commissionRate +
                ", couponPrice=" + couponPrice +
                ", couponReceiveNum=" + couponReceiveNum +
                ", couponTotalNum=" + couponTotalNum +
                ", monthSales=" + monthSales +
                ", twoHoursSales=" + twoHoursSales +
                ", dailySales=" + dailySales +
                ", hotPush=" + hotPush +
                ", mainPic='" + mainPic + '\'' +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", originalPrice=" + originalPrice +
                ", couponLink='" + couponLink + '\'' +
                ", couponStartTime=" + couponStartTime +
                ", couponEndTime=" + couponEndTime +
                ", commissionType=" + commissionType +
                ", createTime=" + createTime +
                ", activityType=" + activityType +
                ", picList=" + picList +
                ", guideName='" + guideName + '\'' +
                ", shopType=" + shopType +
                ", couponConditions=" + couponConditions +
                ", avgSales=" + avgSales +
                ", entryTime=" + entryTime +
                ", sellerId='" + sellerId + '\'' +
                ", quanMLink=" + quanMLink +
                ", hzQuanOver=" + hzQuanOver +
                ", yunfeixian=" + yunfeixian +
                ", estimateAmount=" + estimateAmount +
                '}';
    }
}
