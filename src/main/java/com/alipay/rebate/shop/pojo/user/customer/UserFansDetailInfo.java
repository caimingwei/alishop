package com.alipay.rebate.shop.pojo.user.customer;

public class UserFansDetailInfo {

    private Long userId;
    private String userNickName;
    private String phone;
    private Long totalFansNum;
    private Long ordersNum;
    private Long userRegisterTime;
    private String userHeadImage;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getTotalFansNum() {
        return totalFansNum;
    }

    public void setTotalFansNum(Long totalFansNum) {
        this.totalFansNum = totalFansNum;
    }

    public Long getOrdersNum() {
        return ordersNum;
    }

    public void setOrdersNum(Long ordersNum) {
        this.ordersNum = ordersNum;
    }

    public Long getUserRegisterTime() {
        return userRegisterTime;
    }

    public void setUserRegisterTime(Long userRegisterTime) {
        this.userRegisterTime = userRegisterTime;
    }

    public String getUserHeadImage() {
        return userHeadImage;
    }

    public void setUserHeadImage(String userHeadImage) {
        this.userHeadImage = userHeadImage;
    }

    @Override
    public String toString() {
        return "UserFansDetailInfo{" +
            "userId=" + userId +
            ", userNickName='" + userNickName + '\'' +
            ", phone='" + phone + '\'' +
            ", totalFansNum=" + totalFansNum +
            ", ordersNum=" + ordersNum +
            ", userRegisterTime=" + userRegisterTime +
            ", userHeadImage='" + userHeadImage + '\'' +
            '}';
    }
}
