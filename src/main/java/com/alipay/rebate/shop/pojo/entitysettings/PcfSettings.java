package com.alipay.rebate.shop.pojo.entitysettings;

public class PcfSettings {

  private Integer frameworkType;
  private Integer isShowActivityFramework;
  private String afBackgroud;
  private String afRedirect;
  private Integer isShowLevel;
  private Integer isShowId;
  private Integer isShowParterCenter;
  private Object customSettings;
  private Object csSettings;
  private String startColor;
  private String endColor;
  private String incomeStartColor;
  private String incomeEndColor;

  public String getIncomeStartColor() {
    return incomeStartColor;
  }

  public void setIncomeStartColor(String incomeStartColor) {
    this.incomeStartColor = incomeStartColor;
  }

  public String getIncomeEndColor() {
    return incomeEndColor;
  }

  public void setIncomeEndColor(String incomeEndColor) {
    this.incomeEndColor = incomeEndColor;
  }

  public String getStartColor() {
    return startColor;
  }

  public void setStartColor(String startColor) {
    this.startColor = startColor;
  }

  public String getEndColor() {
    return endColor;
  }

  public void setEndColor(String endColor) {
    this.endColor = endColor;
  }

  public Integer getFrameworkType() {
    return frameworkType;
  }

  public void setFrameworkType(Integer frameworkType) {
    this.frameworkType = frameworkType;
  }

  public Integer getIsShowActivityFramework() {
    return isShowActivityFramework;
  }

  public void setIsShowActivityFramework(Integer isShowActivityFramework) {
    this.isShowActivityFramework = isShowActivityFramework;
  }

  public String getAfBackgroud() {
    return afBackgroud;
  }

  public void setAfBackgroud(String afBackgroud) {
    this.afBackgroud = afBackgroud;
  }

  public String getAfRedirect() {
    return afRedirect;
  }

  public void setAfRedirect(String afRedirect) {
    this.afRedirect = afRedirect;
  }

  public Integer getIsShowLevel() {
    return isShowLevel;
  }

  public void setIsShowLevel(Integer isShowLevel) {
    this.isShowLevel = isShowLevel;
  }

  public Integer getIsShowId() {
    return isShowId;
  }

  public void setIsShowId(Integer isShowId) {
    this.isShowId = isShowId;
  }

  public Integer getIsShowParterCenter() {
    return isShowParterCenter;
  }

  public void setIsShowParterCenter(Integer isShowParterCenter) {
    this.isShowParterCenter = isShowParterCenter;
  }

  public Object getCustomSettings() {
    return customSettings;
  }

  public void setCustomSettings(Object customSettings) {
    this.customSettings = customSettings;
  }

  public Object getCsSettings() {
    return csSettings;
  }

  public void setCsSettings(Object csSettings) {
    this.csSettings = csSettings;
  }

  @Override
  public String toString() {
    return "PcfSettings{" +
            "frameworkType=" + frameworkType +
            ", isShowActivityFramework=" + isShowActivityFramework +
            ", afBackgroud='" + afBackgroud + '\'' +
            ", afRedirect='" + afRedirect + '\'' +
            ", isShowLevel=" + isShowLevel +
            ", isShowId=" + isShowId +
            ", isShowParterCenter=" + isShowParterCenter +
            ", customSettings=" + customSettings +
            ", csSettings=" + csSettings +
            ", startColor='" + startColor + '\'' +
            ", endColor='" + endColor + '\'' +
            ", incomeStartColor='" + incomeStartColor + '\'' +
            ", incomeEndColor='" + incomeEndColor + '\'' +
            '}';
  }
}
