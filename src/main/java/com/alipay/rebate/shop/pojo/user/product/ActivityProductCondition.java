package com.alipay.rebate.shop.pojo.user.product;

public class ActivityProductCondition {

  private String userLevelName;
  private Integer userLevel;
  private Integer setTime;
  private Integer setNum;
  private Integer scene;
  private Integer joinTime;
  private Integer inviteNum;

  public String getUserLevelName() {
    return userLevelName;
  }

  public void setUserLevelName(String userLevelName) {
    this.userLevelName = userLevelName;
  }

  public Integer getUserLevel() {
    return userLevel;
  }

  public void setUserLevel(Integer userLevel) {
    this.userLevel = userLevel;
  }

  public Integer getSetTime() {
    return setTime;
  }

  public void setSetTime(Integer setTime) {
    this.setTime = setTime;
  }

  public Integer getSetNum() {
    return setNum;
  }

  public void setSetNum(Integer setNum) {
    this.setNum = setNum;
  }

  public Integer getScene() {
    return scene;
  }

  public void setScene(Integer scene) {
    this.scene = scene;
  }

  public Integer getJoinTime() {
    return joinTime;
  }

  public void setJoinTime(Integer joinTime) {
    this.joinTime = joinTime;
  }

  public Integer getInviteNum() {
    return inviteNum;
  }

  public void setInviteNum(Integer inviteNum) {
    this.inviteNum = inviteNum;
  }

  @Override
  public String toString() {
    return "ActivityProductCondition{" +
        "userLevelName='" + userLevelName + '\'' +
        ", userLevel=" + userLevel +
        ", setTime=" + setTime +
        ", setNum=" + setNum +
        ", scene=" + scene +
        ", joinTime=" + joinTime +
        ", inviteNum=" + inviteNum +
        '}';
  }
}
