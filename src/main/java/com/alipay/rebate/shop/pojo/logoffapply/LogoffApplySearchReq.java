package com.alipay.rebate.shop.pojo.logoffapply;

import com.alipay.rebate.shop.pojo.common.CommonPageRequest;

public class LogoffApplySearchReq extends CommonPageRequest {

  private Long userId;
  private String userName;
  private String phone;
  private Integer status;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "LogoffApplySearchReq{" +
        "userId=" + userId +
        ", userName='" + userName + '\'' +
        ", phone='" + phone + '\'' +
        ", status=" + status +
        '}';
  }
}
