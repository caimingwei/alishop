package com.alipay.rebate.shop.pojo.dataoke;

import java.util.List;

public class SuperCategoryResp {

    private Integer cid;
    private String cname;
    private String cpic;
    private List<SuperCategoryRes> subcategories;

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCpic() {
        return cpic;
    }

    public void setCpic(String cpic) {
        this.cpic = cpic;
    }

    public List<SuperCategoryRes> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<SuperCategoryRes> subcategories) {
        this.subcategories = subcategories;
    }

    @Override
    public String toString() {
        return "SuperCategoryReq{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", cpic='" + cpic + '\'' +
                ", subcategories=" + subcategories +
                '}';
    }
}
