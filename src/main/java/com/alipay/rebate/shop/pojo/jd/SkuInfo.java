package com.alipay.rebate.shop.pojo.jd;

import java.math.BigDecimal;

public class SkuInfo {

    private BigDecimal actualCosPrice;
    private BigDecimal actualFee;
    private Long cid1;
    private Long cid2;
    private Long cid3;
    private BigDecimal commissionRate;
    private Long cpActId;
    private BigDecimal estimateCosPrice;
    private BigDecimal estimateFee;
    private String ext1;
    private BigDecimal finalRate;
    private Long frozenSkuNum;
    private String giftCouponKey;
    private Long giftCouponOcsAmount;
    private Integer payMonth;
    private String pid;
    private Long popId;
    private Long positionId;
    private BigDecimal price;
    private BigDecimal proPriceAmount;
    private Long siteId;
    private Long skuId;
    private String skuName;
    private Long skuNum;
    private Long skuReturnNum;
    private BigDecimal subSideRate;
    private String subUnionId;
    private BigDecimal subsidyRate;
    private Integer traceType;
    private String unionAlias;
    private Integer unionRole;
    private String unionTag;
    private Integer unionTrafficGroup;
    private Integer validCode;

    public BigDecimal getActualCosPrice() {
        return actualCosPrice;
    }

    public void setActualCosPrice(BigDecimal actualCosPrice) {
        this.actualCosPrice = actualCosPrice;
    }

    public BigDecimal getActualFee() {
        return actualFee;
    }

    public void setActualFee(BigDecimal actualFee) {
        this.actualFee = actualFee;
    }

    public Long getCid1() {
        return cid1;
    }

    public void setCid1(Long cid1) {
        this.cid1 = cid1;
    }

    public Long getCid2() {
        return cid2;
    }

    public void setCid2(Long cid2) {
        this.cid2 = cid2;
    }

    public Long getCid3() {
        return cid3;
    }

    public void setCid3(Long cid3) {
        this.cid3 = cid3;
    }

    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Long getCpActId() {
        return cpActId;
    }

    public void setCpActId(Long cpActId) {
        this.cpActId = cpActId;
    }

    public BigDecimal getEstimateCosPrice() {
        return estimateCosPrice;
    }

    public void setEstimateCosPrice(BigDecimal estimateCosPrice) {
        this.estimateCosPrice = estimateCosPrice;
    }

    public BigDecimal getEstimateFee() {
        return estimateFee;
    }

    public void setEstimateFee(BigDecimal estimateFee) {
        this.estimateFee = estimateFee;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    public BigDecimal getFinalRate() {
        return finalRate;
    }

    public void setFinalRate(BigDecimal finalRate) {
        this.finalRate = finalRate;
    }

    public Long getFrozenSkuNum() {
        return frozenSkuNum;
    }

    public void setFrozenSkuNum(Long frozenSkuNum) {
        this.frozenSkuNum = frozenSkuNum;
    }

    public String getGiftCouponKey() {
        return giftCouponKey;
    }

    public void setGiftCouponKey(String giftCouponKey) {
        this.giftCouponKey = giftCouponKey;
    }

    public Long getGiftCouponOcsAmount() {
        return giftCouponOcsAmount;
    }

    public void setGiftCouponOcsAmount(Long giftCouponOcsAmount) {
        this.giftCouponOcsAmount = giftCouponOcsAmount;
    }

    public Integer getPayMonth() {
        return payMonth;
    }

    public void setPayMonth(Integer payMonth) {
        this.payMonth = payMonth;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Long getPopId() {
        return popId;
    }

    public void setPopId(Long popId) {
        this.popId = popId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Long getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(Long skuNum) {
        this.skuNum = skuNum;
    }

    public Long getSkuReturnNum() {
        return skuReturnNum;
    }

    public void setSkuReturnNum(Long skuReturnNum) {
        this.skuReturnNum = skuReturnNum;
    }

    public BigDecimal getSubSideRate() {
        return subSideRate;
    }

    public void setSubSideRate(BigDecimal subSideRate) {
        this.subSideRate = subSideRate;
    }

    public String getSubUnionId() {
        return subUnionId;
    }

    public void setSubUnionId(String subUnionId) {
        this.subUnionId = subUnionId;
    }

    public BigDecimal getSubsidyRate() {
        return subsidyRate;
    }

    public void setSubsidyRate(BigDecimal subsidyRate) {
        this.subsidyRate = subsidyRate;
    }

    public Integer getTraceType() {
        return traceType;
    }

    public void setTraceType(Integer traceType) {
        this.traceType = traceType;
    }

    public String getUnionAlias() {
        return unionAlias;
    }

    public void setUnionAlias(String unionAlias) {
        this.unionAlias = unionAlias;
    }

    public Integer getUnionRole() {
        return unionRole;
    }

    public void setUnionRole(Integer unionRole) {
        this.unionRole = unionRole;
    }

    public String getUnionTag() {
        return unionTag;
    }

    public void setUnionTag(String unionTag) {
        this.unionTag = unionTag;
    }

    public Integer getUnionTrafficGroup() {
        return unionTrafficGroup;
    }

    public void setUnionTrafficGroup(Integer unionTrafficGroup) {
        this.unionTrafficGroup = unionTrafficGroup;
    }

    public Integer getValidCode() {
        return validCode;
    }

    public void setValidCode(Integer validCode) {
        this.validCode = validCode;
    }

    public BigDecimal getProPriceAmount() {
        return proPriceAmount;
    }

    public void setProPriceAmount(BigDecimal proPriceAmount) {
        this.proPriceAmount = proPriceAmount;
    }

    @Override
    public String toString() {
        return "SkuInfo{" +
                "actualCosPrice=" + actualCosPrice +
                ", actualFee=" + actualFee +
                ", cid1=" + cid1 +
                ", cid2=" + cid2 +
                ", cid3=" + cid3 +
                ", commissionRate=" + commissionRate +
                ", cpActId=" + cpActId +
                ", estimateCosPrice=" + estimateCosPrice +
                ", estimateFee=" + estimateFee +
                ", ext1='" + ext1 + '\'' +
                ", finalRate=" + finalRate +
                ", frozenSkuNum=" + frozenSkuNum +
                ", giftCouponKey='" + giftCouponKey + '\'' +
                ", giftCouponOcsAmount=" + giftCouponOcsAmount +
                ", payMonth=" + payMonth +
                ", pid='" + pid + '\'' +
                ", popId=" + popId +
                ", positionId=" + positionId +
                ", price=" + price +
                ", proPriceAmount=" + proPriceAmount +
                ", siteId=" + siteId +
                ", skuId=" + skuId +
                ", skuName='" + skuName + '\'' +
                ", skuNum=" + skuNum +
                ", skuReturnNum=" + skuReturnNum +
                ", subSideRate=" + subSideRate +
                ", subUnionId='" + subUnionId + '\'' +
                ", subsidyRate=" + subsidyRate +
                ", traceType=" + traceType +
                ", unionAlias='" + unionAlias + '\'' +
                ", unionRole=" + unionRole +
                ", unionTag='" + unionTag + '\'' +
                ", unionTrafficGroup=" + unionTrafficGroup +
                ", validCode=" + validCode +
                '}';
    }
}
