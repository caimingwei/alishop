package com.alipay.rebate.shop.pojo.dingdanxia;

import java.util.List;

public class SpkOptimusResponse {

    private Integer code;
    private String msg;
    private Boolean is_default;
    private List<DingdanxiaOptimusProduct> data;

    public Boolean getIs_default() {
        return is_default;
    }

    public void setIs_default(Boolean is_default) {
        this.is_default = is_default;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DingdanxiaOptimusProduct> getData() {
        return data;
    }

    public void setData(List<DingdanxiaOptimusProduct> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SpkOptimusResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", is_default=" + is_default +
                ", data=" + data +
                '}';
    }
}
