package com.alipay.rebate.shop.pojo.user.customer;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * 用户对象
 */
public class UserDto implements Serializable {
    private static final long serialVersionUID = -9077975168976887742L;

    private String account;
    private String openId;
    private String nickName;
    private int sex;
    private String headImgUrl;
    private String unionId;
    private char[] password;
    private String phone;
    private String mobileCode;
    private String encryptPwd;
    private Long userId;
    private String salt;
    private List<String> roles;
    private String adminAccount;
    private Integer userStatus;
    private String currentRegistrationId;
    private Integer loginPlatform;

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getEncryptPwd() {
		return encryptPwd;
	}

	public void setEncryptPwd(String encryptPwd) {
		this.encryptPwd = encryptPwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getAdminAccount() {
		return adminAccount;
	}

	public void setAdminAccount(String adminAccount) {
		this.adminAccount = adminAccount;
	}

	public Integer getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	public String getCurrentRegistrationId() {
		return currentRegistrationId;
	}

	public void setCurrentRegistrationId(String currentRegistrationId) {
		this.currentRegistrationId = currentRegistrationId;
	}

	public Integer getLoginPlatform() {
		return loginPlatform;
	}

	public void setLoginPlatform(Integer loginPlatform) {
		this.loginPlatform = loginPlatform;
	}

	@Override
	public String toString() {
		return "UserDto{" +
				"account='" + account + '\'' +
				", openId='" + openId + '\'' +
				", nickName='" + nickName + '\'' +
				", sex=" + sex +
				", headImgUrl='" + headImgUrl + '\'' +
				", unionId='" + unionId + '\'' +
				", password=" + Arrays.toString(password) +
				", phone='" + phone + '\'' +
				", mobileCode='" + mobileCode + '\'' +
				", encryptPwd='" + encryptPwd + '\'' +
				", userId=" + userId +
				", salt='" + salt + '\'' +
				", roles=" + roles +
				", adminAccount='" + adminAccount + '\'' +
				", userStatus=" + userStatus +
				", currentRegistrationId='" + currentRegistrationId + '\'' +
				", loginPlatform=" + loginPlatform +
				'}';
	}
}
