package com.alipay.rebate.shop.scheduler;

public enum TypeTkStatus {

  RELATION_PAID,RELATION_ACCOUNTING,RELATION_COMPLETE,RELATION_OVERDUE,
  NORMAL_PAID,NORMAL_ACCOUNTING,NORMAL_COMPLETE,NORMAL_OVERDUE,
  SPECIAL_PAID,SPECIAL_ACCOUNTING,SPECIAL_COMPLETE,SPECIAL_OVERDUE;
}
