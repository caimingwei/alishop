package com.alipay.rebate.shop.scheduler;

import java.util.HashMap;
import java.util.Map;

public class TaobaoOrdersTkStatusMapper {

    public static Map<Long,TypeTkStatus> relationMap = new HashMap<>();
    public static Map<Long,TypeTkStatus> specialMap = new HashMap<>();
    static{
        relationMap.put(12L,TypeTkStatus.RELATION_PAID);
        relationMap.put(13L,TypeTkStatus.RELATION_OVERDUE);
        relationMap.put(3L,TypeTkStatus.RELATION_ACCOUNTING);
        relationMap.put(14L,TypeTkStatus.RELATION_COMPLETE);
    }

    static{
        specialMap.put(12L,TypeTkStatus.SPECIAL_PAID);
        specialMap.put(13L,TypeTkStatus.SPECIAL_OVERDUE);
        specialMap.put(3L,TypeTkStatus.SPECIAL_ACCOUNTING);
        specialMap.put(14L,TypeTkStatus.SPECIAL_COMPLETE);
    }

    public static TypeTkStatus getRelationTkStatus(Long tkStatus){
        return relationMap.get(tkStatus);
    }

    public static TypeTkStatus getSpecialTkStatus(Long tkStatus){
        return specialMap.get(tkStatus);
    }

}
