package com.alipay.rebate.shop.scheduler;

import java.math.BigDecimal;

public class ConditionJson {

  private BigDecimal consumePrice;

  public BigDecimal getConsumePrice() {
    return consumePrice;
  }

  public void setConsumePrice(BigDecimal consumePrice) {
    this.consumePrice = consumePrice;
  }

  @Override
  public String toString() {
    return "ConditionJson{" +
        "consumePrice=" + consumePrice +
        '}';
  }
}
