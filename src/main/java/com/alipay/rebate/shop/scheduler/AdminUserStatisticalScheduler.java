package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.helper.UserStatisticalHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AdminUserStatisticalScheduler {

    @Resource
    private UserStatisticalHelper userStatisticalHelper;

    private Logger logger = LoggerFactory.getLogger(AdminUserStatisticalScheduler.class);

    @Scheduled(fixedRate = 1000 * 60 * 5)
    public void getTodayRegisterUserNum(){
        logger.debug("getTodayRegisterUserNum");
        userStatisticalHelper.addOrUpdateUserStatistical();
    }

}
