package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.dao.mapper.SmsGroupSendMapper;
import com.alipay.rebate.shop.helper.SmsMessageSender;
import com.alipay.rebate.shop.model.SmsGroupSend;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

//@Component
public class SmsSendScheduler {

  @Resource
  private SmsGroupSendMapper smsGroupSendMapper;
  @Resource
  private SmsMessageSender smsMessageSender;
  @Resource
  @Qualifier("sharePool")
  private ExecutorService executorService;

  private Logger logger = LoggerFactory.getLogger(SmsSendScheduler.class);

  @Scheduled(fixedRate = 1000 * 60 * 1)
  public void importPaidOrders() {
    List<SmsGroupSend> list = smsGroupSendMapper.selectOnTimeRecord();
    logger.debug("list is : {}",list);
//    if(list.size() >0){
//      for(int i=0; i<list.size(); i++){
//        SmsGroupSend smsGroupSend = list.get(i);
//        CompletableFuture.runAsync(()->{
//          smsMessageSender.sendSms(smsGroupSend);
//        },executorService);
//      }
//    }
  }
}
