package com.alipay.rebate.shop.scheduler.orderimport;

import com.alipay.rebate.shop.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

//@Component
public class RefundOrdersScheduler {

  private Logger logger = LoggerFactory.getLogger(RefundOrdersScheduler.class);

  @Autowired
  private RefundOrdersImporter refundOrdersImporter;

  @Scheduled(cron = "0 0 02 ? * *")
  public void importOverDueOrders() {
    for (int i = 15;i >= 0 ; i--) {
      String daysStartTime = DateUtil.getDaysStartTime(i);
      logger.debug("daysStartTime is : {}",daysStartTime);
      refundOrdersImporter.importRelationRefundOrders(daysStartTime);
      refundOrdersImporter.importSpeialnRefundOrders(daysStartTime);
    }
  }

  @Scheduled(fixedRate = 1000 * 60 * 30)
  public void importOverDueOrdersByHour(){
    String date = DateUtil.getTodayStartTime();
    logger.debug("date is : {}",date);
    refundOrdersImporter.importRelationRefundOrders(date);
    refundOrdersImporter.importSpeialnRefundOrders(date);
  }

}
