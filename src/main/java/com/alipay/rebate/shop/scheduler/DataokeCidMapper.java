package com.alipay.rebate.shop.scheduler;

import java.util.HashMap;
import java.util.Map;

public class DataokeCidMapper {

  public static Map<Long,Long> map = new HashMap<>();

  static {
    map.put(0L,0L); // 全部
    map.put(1L,1L); // 女装
    map.put(2L,9L); // 母婴
    map.put(3L,4L); // 美装
    map.put(4L,10L); // 居家
    map.put(5L,6L); // 鞋品
    map.put(6L,11L); // 美食
    map.put(7L,13L); // 文娱车品
    map.put(8L,12L); // 数码家电
    map.put(9L,2L); // 男装
    map.put(10L,3L); // 内衣
    map.put(11L,7L); // 箱包
    map.put(12L,5L); // 配饰
    map.put(13L,14L); // 户外运动 算其他
    map.put(14L,14L); // 家装家纺 算其他
  }

  public static Long getCid(Long originCid){
    return map.get(originCid);
  }
}
