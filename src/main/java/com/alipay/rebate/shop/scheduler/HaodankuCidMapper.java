package com.alipay.rebate.shop.scheduler;

import java.util.HashMap;
import java.util.Map;

public class HaodankuCidMapper {

  public static Map<Long,Long> map = new HashMap<>();

  static {
    map.put(0L,0L); // 全部
    map.put(1L,1L); // 女装
    map.put(2L,2L); // 男装
    map.put(3L,3L); // 内衣
    map.put(4L,4L); // 美妆
    map.put(5L,5L); // 配饰
    map.put(6L,6L); // 鞋品
    map.put(7L,7L); // 箱包
    map.put(8L,8L); // 儿童
    map.put(9L,9L); // 母婴
    map.put(10L,10L); // 居家
    map.put(11L,11L); // 美食
    map.put(12L,12L); // 数码家电
    map.put(13L,12L); // 数码加点
    map.put(14L,14L); // 其他
    map.put(15L,13L); // 文娱车品
    map.put(16L,13L); // 文娱车品
    map.put(17L,14L); // 宠物 算其他
  }

  public static Long getCid(Long originCid){
    return map.get(originCid);
  }
}
