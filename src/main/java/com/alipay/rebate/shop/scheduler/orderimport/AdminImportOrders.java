package com.alipay.rebate.shop.scheduler.orderimport;

import com.alipay.rebate.shop.constants.LuckyMonCouponConstant;
import com.alipay.rebate.shop.constants.OrdersConstant;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.model.CommissionFreezeLevelSettings;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.admin.ImportOrdersRequest;
import com.alipay.rebate.shop.pojo.user.customer.UserCouponRsp;
import com.alipay.rebate.shop.utils.DateUtil;
import com.alipay.rebate.shop.utils.DingdanxiaUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Component
public class AdminImportOrders extends AbstractOrdersImporter {

    private Logger logger = LoggerFactory.getLogger(AdminImportOrders.class);

    Gson gson = new GsonBuilder().create();

    @Override
    void doDealWithOrder(Orders orders, Orders previousOrder, Long parentUserId, Long parentPUserId, User user, User oneLevelUser) {

    }

    @Override
    protected void createIncomeRecord(Orders orders, Long userId, Long parentUserId,
                                      Long parentPuserId, Orders previousOrders, CommissionFreezeLevelSettings freezeCommissionSettings) {

    }

    @Transactional
    public void addOrUpdateOrders(ImportOrdersRequest ordersList){

        logger.debug("ordersList status import");

        //获取后台传进来的订单
        List<Orders> orders = ordersList.getOrders();

        for (Orders order:orders){

            //根据订单信息进行处理
            checkOrders(order);

            //查询订单是否已经存在
            Orders previousOrder = ordersMapper.selectOrdersByTradeId(order.getTradeId());

            //保存或更新订单
            addOrUpdateOrders(order,previousOrder);

        }

    }

    //判断订单是否已经存在
    private Orders addOrUpdateOrders(Orders orders,Orders ordersByTradeId) {

        logger.debug("ordersByTradeId is : {}",ordersByTradeId);

        // 如果订单记录在数据库已经存在，那么更新订单信息，否则直接插入
        if(ordersByTradeId != null && !orders.getTkStatus().equals(ordersByTradeId.getTkStatus())){
            logger.debug("update orders");
            ordersMapper.updateByPrimaryKeySelective(orders);
        }
        if (ordersByTradeId == null){
            logger.debug("insert orders");
            ordersMapper.insertSelective(orders);
        }

        return ordersByTradeId;

    }


    //根据订单信息判断是哪种订单
    public void checkOrders(Orders orders){

        logger.debug("checkOrders stater");

        //查询订单是否已经存在
        Orders previousOrder = ordersMapper.selectOrdersByTradeId(orders.getTradeId());

        User user = null;
        User parentUser = null;
        User parentPUser = null;

        //若渠道id与会员运营id都为空，则看作常规订单处理
        if (orders.getRelationId() == null && orders.getSpecialId() == null){

            // 如果订单状态和数据库订单状态一样，那么不处理
            if(existsInDbAndStatusTheSame(orders,previousOrder)){
                logger.debug("orders have inserted");
                return;
            }

            // 如果根据淘宝id查询出来的用户超过两个，直接入库
            if(checkIfUserFindbyTbidMoreThanTwoAndDoLogic(orders)){
                logger.debug("Relation End <--------------------> End");
                return;
            }

            //根据订单号后6位查询用户信息
            user = getUserByTbId(orders);
            logger.debug("checkOrders user is : {}",user);

            //上级id不为空，查询出上级用户
            if (user.getParentUserId() != null){
                parentUser = userMapper.selectById(user.getParentUserId());

                //上上级用户
                if (parentUser.getParentUserId() != null){
                    parentPUser = userMapper.selectById(parentUser.getParentUserId());
                }

            }
            logger.debug("checkOrders parentUser and parentPUser is : {},{}",parentUser,parentPUser);

            // 先前已经导入状态不同订单
            if(previousOrder != null){
                handleWhenPreviousOrderExists(orders,previousOrder);
                logger.debug("Relation End <--------------------> End");
                return;
            }

            if(orders == null) return ;
            // 如果订单adzone id 和 淘宝客联盟zoneid不一致，那么忽略
            if(!DingdanxiaUtil.ADZONE_ID.equals(String.valueOf(orders.getAdzoneId())) &&
                    !DingdanxiaUtil.ADZONE_ID_ACTIVITY.equals(String.valueOf(orders.getAdzoneId()))
            ){
                return;
            }

            // 如果用户不是我们平台的， 直接返回
            if(user == null){
                orders.setIsRecord(0);
                ordersMapper.insertSelective(orders);
                return;
            }

            //处理订单
            handleOrdersByStatus(orders,previousOrder,user,parentUser);

            orders.setUserId(user.getUserId());
            orders.setUserName(user.getUserNickName());
            doDealWithNMOrder(orders,user,previousOrder);

        }


        //渠道id不为空
        if (orders.getRelationId() != null && orders.getSpecialId() == null){

            // 如果订单状态和数据库订单状态一样，那么不处理
            if(existsInDbAndStatusTheSame(orders,previousOrder)){
                logger.debug("orders have inserted");
                return;
            }

            // 如果根据淘宝id查询出来的用户超过两个，直接入库
            if(checkIfUserFindbyTbidMoreThanTwoAndDoLogic(orders)){
                logger.debug("Relation End <--------------------> End");
                return;
            }

            //根据订单号后6位查询用户信息
            user = getUserByTbId(orders);
            logger.debug("checkOrders user is : {}",user);

            //上级id不为空，查询出上级用户
            if (user.getParentUserId() != null){
                parentUser = userMapper.selectById(user.getParentUserId());

                //上上级用户
                if (parentUser.getParentUserId() != null){
                    parentPUser = userMapper.selectById(parentUser.getParentUserId());
                }

            }
            logger.debug("checkOrders parentUser and parentPUser is : {},{}",parentUser,parentPUser);

            // 先前已经导入状态不同订单
            if(previousOrder != null){
                handleWhenPreviousOrderExists(orders,previousOrder);
                logger.debug("Relation End <--------------------> End");
                return;
            }


            // 根据relationId 获取用户信息
            User relationIdUser = userMapper.selectUserByRelationId(orders.getRelationId());
            logger.debug("relationId user is : {}",relationIdUser);
            Long relationUserId = relationIdUser == null ? null : relationIdUser.getUserId();
            if(user == null){
                logger.debug("Not our platform user");
                // 购买商品的不是我们平台的用户，则查看推广人是否是我们平台的用户
                // 如果relationId 为空，说明推广人也不是我们平台的，丢弃这个订单
                if(relationUserId == null){
                    logger.debug("RelationId user not our platform user");
                    return;
                }
                // 如果推广人是我们平台的，那么保存订单
                setOrdersInfoWhenRelationNewImport(orders,
                        null, null,
                        relationUserId,
                        relationIdUser.getUserNickName());
                // 进入订单处理逻辑
                //处理订单
                handleOrdersByStatus(orders,previousOrder,user,parentUser);
            }else{
                logger.debug("our platform user");
                // 如果relationId 为空，说明推广人不是我们平台的，丢弃这个订单
                if(relationUserId == null){
                    logger.debug("relationIdUser is not our platform user");
                    return;
                }
                // 购买商品的是我们平台的用户
                if(user.getUserId().equals(relationUserId)){
                    // 如果商品就是推广人自己购买的，那么保存订单
                    logger.debug("user and relationIdUser is the same");
                    setOrdersInfoWhenRelationNewImport(orders,
                            user.getUserId(),
                            user.getUserNickName(),
                            relationUserId,
                            relationIdUser.getUserNickName());
                    //处理订单
                    handleOrdersByStatus(orders,previousOrder,user,parentUser);
                    return;
                }
                // 如果推广人和购买人都是我们平台的，那么保存订单
                logger.debug("relationIdUser and user is not the same");
                // 订单数加 1
                setOrdersInfoWhenRelationNewImport(orders,
                        user.getUserId(),
                        user.getUserNickName(),
                        relationUserId,
                        relationIdUser.getUserNickName());
                //处理订单
                handleOrdersByStatus(orders,previousOrder,user,parentUser);
                doDealWithNMOrder(orders,user,previousOrder);
            }




        }


        //会员运营id不为空
        if (orders.getSpecialId() != null){

            // 获取先前的订单
            logger.debug("Special previousOrder is : {}",previousOrder);
            // 如果订单状态和数据库订单状态一样，那么不处理
            if(existsInDbAndStatusTheSame(orders,previousOrder)){
                logger.debug("orders have inserted");
                return;
            }

            // 先前已经导入状态不同订单
            if(previousOrder != null){
                handleWhenPreviousOrderExists(orders,previousOrder);
                logger.debug("Special End <--------------------> End");
                return;
            }

            // 如果根据淘宝id查询出来的用户超过两个，直接入库
            if(checkIfUserFindbyTbidMoreThanTwoAndDoLogic(orders)){
                logger.debug("Relation End <--------------------> End");
                return;
            }

            //根据会员运营id查询用户信息
            user = userMapper.selectUserBySpecialId(orders.getSpecialId());
            logger.debug("checkOrders user is : {}",user);

            //上级id不为空，查询出上级用户
            if (user.getParentUserId() != null){
                parentUser = userMapper.selectById(user.getParentUserId());

                //上上级用户
                if (parentUser.getParentUserId() != null){
                    parentPUser = userMapper.selectById(parentUser.getParentUserId());
                }

            }
            logger.debug("checkOrders parentUser and parentPUser is : {},{}",parentUser,parentPUser);

            // 如果用户不是我们平台的， 直接返回
            if(user == null){
                orders.setIsRecord(0);
                ordersMapper.insertSelective(orders);
                return;
            }
            orders.setUserId(user.getUserId());
            orders.setUserName(user.getUserNickName());

            //处理订单
            handleOrdersByStatus(orders,previousOrder,user,parentUser);
            doDealWithSCOrder(orders,user,previousOrder);

        }

    }

    //根据不同的订单状态处理订单
    @Transactional
    public void handleOrdersByStatus(Orders orders,Orders previousOrder,User user,User parentUser){
        //结算或完成
        if (orders.getTkStatus() == 3 || orders.getTkStatus() == 14){
            // 查看是否是第一单完成订单,如果是,进行新人任务奖励
            // 以前没有完成的订单, 这笔订单已经到了结算/完成状态
            checkNewUserTask(user.getUserId(),orders);
            // 根据状态获取到实际能拿到的所有佣金
            BigDecimal totalGetMoney = getTotalGetMoney(orders);
            logger.debug("Done totalGetMoney is : {}", totalGetMoney);
            // 如果是活动订单
            dealwithActivityOrdersLogicIfis(previousOrder,orders,user.getUserId());

            // 获取冻结配置，根据冻结配置决定是否冻结
            CommissionFreezeLevelSettings freezeCommissionSettings = getFreezeSettingsByMoney(orders.getUserCommission());
            logger.debug("CommissionFreezeLevelSettings is : {}",freezeCommissionSettings);

            //根据用户是否有上级，处理佣金
            checkParentUser(orders,user,previousOrder,totalGetMoney,parentUser,freezeCommissionSettings);
            // 不冻结的话直接增加用户集分宝
            if(freezeCommissionSettings == null){
                userHelper.plusUser(orders,user,parentUser);
            }
            // 如果数据库有记录，那么更新订单信息，没有则直接插入
            saveOrUpdateOrders(orders,previousOrder);

            // 拉新奖励
            if(previousOrder == null || OrdersConstant.PAID_STATUS.equals(previousOrder.getTkStatus())){
                checkAndAwardToUser(orders);
            }
            giveLuckyMoney(user);

        }

        //付款
        if (orders.getTkStatus() == 12){
            // 查看是否是第一单完成订单,如果是,进行新人任务奖励
            // 以前没有完成的订单, 这笔订单已经到了结算/完成状态
            checkNewUserTask(user.getUserId(),orders);
            // 根据状态获取到实际能拿到的所有佣金
            BigDecimal totalGetMoney = getTotalGetMoney(orders);
            logger.debug("Done totalGetMoney is : {}", totalGetMoney);
            // 如果是活动订单
            dealwithActivityOrdersLogicIfis(previousOrder,orders,user.getUserId());

            // 获取冻结配置，根据冻结配置决定是否冻结
            CommissionFreezeLevelSettings freezeCommissionSettings = getFreezeSettingsByMoney(orders.getUserCommission());
            logger.debug("CommissionFreezeLevelSettings is : {}",freezeCommissionSettings);

            //根据用户是否有上级，处理佣金
            checkParentUser(orders,user,previousOrder,totalGetMoney,parentUser,freezeCommissionSettings);

            // 如果数据库有记录，那么更新订单信息，没有则直接插入
            saveOrUpdateOrders(orders,previousOrder);
        }

        //失效
        if (orders.getTkStatus() == 13){
            // 删除收益记录
            deleteUserIncomeWhenOverDue(orders);
        }

    }

    //根据上级用户是否为空进行佣金的计算
    private void checkParentUser(Orders orders,User user,Orders previousOrder,
                                 BigDecimal totalGetMoney,User parentUser,CommissionFreezeLevelSettings freezeCommissionSettings){
        //没有上级用户
        if (parentUser == null){
            // 订单付款状态
            // 计算用户佣金，用户上级佣金，上上级佣金，上上上级佣金
            countCommission(totalGetMoney,orders,null, null,user,null);
            insertOrUpdateUserIncome(orders,user,null,null,freezeCommissionSettings);
        }

        //有上级
        if (parentUser != null){

            //获取上上级
            User parentPUser = userMapper.selectById(parentUser.getParentUserId());

            //上上级不为空
            if (parentPUser != null){
                // 订单付款状态
                // 计算用户佣金，用户上级佣金，上上级佣金，上上上级佣金
                countCommission(totalGetMoney,orders,parentUser.getUserId(), parentPUser.getUserId(),user,parentUser);
                insertOrUpdateUserIncome(orders,user,parentUser.getUserId(), parentPUser.getUserId(),freezeCommissionSettings);
            }else {

                logger.debug("parentUser.getUserId() and parentPUser.getUserId() and parentUser and user is :" +
                        "{},{},{},{}",parentUser.getUserId(), null,user,parentUser);
                // 订单付款状态
                // 计算用户佣金，用户上级佣金，上上级佣金，上上上级佣金
                countCommission(totalGetMoney,orders,parentUser.getUserId(), null,user,parentUser);
                insertOrUpdateUserIncome(orders,user,parentUser.getUserId(), null,freezeCommissionSettings);
            }
        }
    }

    private void insertOrUpdateUserIncome(Orders orders,User user,Long parenUserId,Long parenPUserId,
                                          CommissionFreezeLevelSettings freezeCommissionSettings){
        // 创建收益记录
        // 查询数据库是否已经有用户记录
        List<UserIncome> userIncomeList = userIncomeMapper
                .selectUserIncomeByOrdersId(orders.getTradeId());
        if (userIncomeList.size() > 0){
            // 如果数据库有记录，那么就更新住居
            logger.debug("update userIncome record");
            updateUserIncome(orders,user.getUserId(),parenUserId,parenPUserId,
                    userIncomeList,orders.getTkStatus(),freezeCommissionSettings);
        }else{
            insertUserIncome(orders,user.getUserId(),parenUserId,parenPUserId,freezeCommissionSettings);
        }
    }

    //处理同一订单不同状态的订单
    private void handleWhenPreviousOrderExists(Orders orders, Orders previousOrder){

        // 如果先前导入的是入库非确定订单，那么只需要简单更新
        if(Integer.valueOf(0).equals(previousOrder.getIsRecord())){
            ordersMapper.updateByPrimaryKeySelective(orders);
            return;
        }

        // 从数据库订单拷贝一些数据
        orders.setUserId(previousOrder.getUserId());
        orders.setUserUit(previousOrder.getUserUit());
        orders.setUserCommission(previousOrder.getUserCommission());
        orders.setOneLevelUserUit(previousOrder.getOneLevelUserUit());
        orders.setOneLevelCommission(previousOrder.getOneLevelCommission());
        orders.setTwoLevelUserUit(previousOrder.getTwoLevelUserUit());
        orders.setTwoLevelCommission(previousOrder.getTwoLevelCommission());
        orders.setPromoterId(previousOrder.getPromoterId());
        orders.setPromoterName(previousOrder.getPromoterName());
        orders.setActivityId(previousOrder.getActivityId());

        // 获取用户信息
        Long parentUserId , parentPUserId = null;
        User user, oneLevelUser = null;

        // 如果是渠道订单，那么奖励是按照推广人，推广人上级，推广人上上级进行奖励的
        if(orders.getRelationId() != null){
            user = userMapper.selectUserByRelationId(orders.getRelationId());
        }else{ // 常规订单或者会员运营订单
            user = userMapper.selectById(orders.getUserId());
        }

        parentUserId = user.getParentUserId();
        if(parentUserId != null){
            oneLevelUser = userMapper.selectById(parentUserId);
            parentPUserId = oneLevelUser.getParentUserId();
        }

        logger.debug("parentUserId, parentPuserId is : {},{}",parentUserId,parentPUserId);
        logger.debug("user, oneLevelUser is : {}",user,oneLevelUser);
    }

    // 如果根据淘宝id查询出来的用户超过两个，直接入库
    private boolean checkIfUserFindbyTbidMoreThanTwoAndDoLogic(Orders orders){
        String tbId = getTaobaoUserIdFinalSixNumber(orders.getTradeId());
        int uCount = userMapper.selectCountByTbid(tbId);
        if(uCount >=2){
            orders.setIsRecord(0);
            saveOrUpdateOrders(orders,null);
            logger.debug("End <--------------------> End");
            return true;
        }
        return false;
    }

    //判断之前的订单是否一致
    private boolean existsInDbAndStatusTheSame(Orders orders, Orders previousOrder){
        if(previousOrder != null && orders.getTkStatus().equals(previousOrder.getTkStatus())){
            logger.debug("orders have inserted");
            return true;
        }
        return false;
    }

    private void setOrdersInfoWhenRelationNewImport(
            Orders orders,
            Long userId,
            String userName,
            Long relationUserId,
            String relationUserName
    ){
        orders.setUserId(userId);
        orders.setUserName(userName);
        orders.setPromoterId(relationUserId);
        orders.setPromoterName(relationUserName);
    }


    //获取淘宝后六位数字进行拼接
    private User getUserByTbId(Orders orders){
        String tradeIdStr = String.valueOf(orders.getTradeId());
        String taobaoUserIdFinalSixNumber
                = getTaobaoUserIdFinalSixNumber(tradeIdStr);
        logger.debug("taobaoUserId finalSixNumber is : {}",taobaoUserIdFinalSixNumber);
        // 通过淘宝用户id到数据库查询用户，如果用户不为空，那么就是我们平台的用户
        return userMapper.selectUserInfoByTaobaoUserIdFinalSixNumber(taobaoUserIdFinalSixNumber);
    }

    private String getTaobaoUserIdFinalSixNumber(String tradeIdStr){
        Integer length = tradeIdStr.length();
        StringBuilder builder = new StringBuilder();
        builder.append(tradeIdStr.substring(length -6,length - 4));
        builder.append(tradeIdStr.substring(length -2,length));
        builder.append(tradeIdStr.substring(length -4,length - 2));
        return builder.toString();
    }

    private void giveLuckyMoney(User user){
        List<UserCouponRsp> userCouponRsps = couponGetRecordMapper.selectCouponDetailByUserId(user.getUserId());
        luckyMoneyHelper.openLuckyMoneyCouponIfNeed(user.getUserId(),
                LuckyMonCouponConstant.CONDITION_TYPE_ORDER_REWARD,
                userCouponRsps,true);
        luckyMoneyHelper.openLuckyMoneyCouponIfNeed(user.getUserId(),
                LuckyMonCouponConstant.CONDITION_TYPE_PAID_MONEY,
                userCouponRsps,true);
    }


    protected void insertUserIncome(
            Orders orders, Long userId, Long parentUserId, Long parentPuserId,
            CommissionFreezeLevelSettings freezeCommissionSettings
    ){
        BigDecimal userCommission = orders.getUserCommission();
        BigDecimal oneLevelCommission = orders.getOneLevelCommission();
        BigDecimal twoLevelCommission = orders.getTwoLevelCommission();
        // 往用户收益表插入记录
        // 1 购买用户本人
        Integer roadType = 0;
        insertUserIncome(orders,userCommission,userId,roadType,freezeCommissionSettings);
        // 如果属于活动订单,那么上级, 上上级佣金不算
        if(orders.getActivityId() != null){
            return;
        }
        // 2 推广人，即上级用户
        if(parentUserId != null){
            roadType = 1;
            insertUserIncome(orders,oneLevelCommission,parentUserId,roadType,freezeCommissionSettings);
            if(parentPuserId != null){
                roadType = 2;
                insertUserIncome(orders,twoLevelCommission,parentPuserId,roadType,freezeCommissionSettings);
            }
        }
    }

    protected void insertUserIncome(
            Orders orders,
            BigDecimal money,
            Long userId,
            Integer roadType,
            CommissionFreezeLevelSettings freezeCommissionSettings
    ){
        boolean freezeCommission = freezeCommissionSettings != null;
        UserIncome userIncome = userIncomeHelper.inserOrderCommissionIncome(orders,money,userId,roadType,freezeCommission);
        if(freezeCommissionSettings != null){
            incomeCommissionFreezeLevelHelper.insert(userIncome.getUserIncomeId(),
                    freezeCommissionSettings.getId());
        }
    }

    protected void updateUserIncome(
            Orders orders,
            Long userId,
            Long parentUserId,
            Long parentPuserId,
            List<UserIncome> userIncomeList,
            Long tkStatus,
            CommissionFreezeLevelSettings freezeCommissionSettings
    ){
        BigDecimal userCommission = orders.getUserCommission();
        BigDecimal oneLevelCommission = orders.getOneLevelCommission();
        BigDecimal twoLevelCommission = orders.getTwoLevelCommission();
        // 往用户收益表插入记录
        // 1 购买用户本人
        UserIncome userIncome = getUserIncomeIdFromList(userIncomeList,userId);
        updateUserIncome(userIncome,userCommission,tkStatus,freezeCommissionSettings);
        // 如果属于活动订单,那么上级, 上上级佣金不算
        if(orders.getActivityId() != null){
            return;
        }
        // 2 推广人，即上级用户
        if(parentUserId != null){
            UserIncome parentIncome = getUserIncomeIdFromList(userIncomeList,parentUserId);
            updateUserIncome(parentIncome,oneLevelCommission,tkStatus,freezeCommissionSettings);
            if(parentPuserId != null){
                UserIncome parentPIncome = getUserIncomeIdFromList(userIncomeList,parentPuserId);
                updateUserIncome(parentPIncome,twoLevelCommission,tkStatus,freezeCommissionSettings);
            }
        }
    }

    private void updateUserIncome(
            UserIncome preIncome, BigDecimal money, Long tkStatus,
            CommissionFreezeLevelSettings freezeCommissionSettings
    ){
        UserIncome userIncome = new UserIncome();
        userIncome.setUserIncomeId(preIncome.getUserIncomeId());
        userIncome.setMoney(money);
        userIncome.setTkStatus(tkStatus);
        if(freezeCommissionSettings == null ){
            userIncome.setStatus(UserIncomeConstant.IS_PAID);
        } else {
            userIncome.setStatus(getIncomeStatusWhenUpdate(preIncome));
        }
        userIncome.setUpdateTime(DateUtil.getNowStr());
        logger.debug("update UserIncome is: {}",userIncome);
        userIncomeMapper.updateByPrimaryKeySelective(userIncome);
    }

    private UserIncome getUserIncomeIdFromList(List<UserIncome> userIncomeList,Long userId){

        logger.debug("userIncomeList is : {}",userIncomeList);
        for(int i = 0 ; i<userIncomeList.size() ; i++){
            Long incomeUserId = userIncomeList.get(i).getUserId();
            if(incomeUserId.equals(userId)){
                return userIncomeList.get(i);
            }
        }
        return null;
    }

    private Integer getIncomeStatusWhenUpdate(
            UserIncome preIncome){
        // 从付款进入到结算/完成，应该为冻结状态
        if (OrdersConstant.PAID_STATUS.equals(preIncome.getTkStatus())){
            return UserIncomeConstant.FREEZE;
        }
        // 如果先前是冻结状态或者付款状态，那么无需改变状态
        if (UserIncomeConstant.FREEZE == preIncome.getStatus() ||
                UserIncomeConstant.IS_PAID == preIncome.getStatus()
        ){
            return null;
        }
        return null;
    }

}
