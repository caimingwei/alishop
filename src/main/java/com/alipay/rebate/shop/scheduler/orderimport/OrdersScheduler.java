package com.alipay.rebate.shop.scheduler.orderimport;

import com.alipay.rebate.shop.helper.MeiTuanOrderHelper;
import com.alipay.rebate.shop.scheduler.TypeTkStatus;
import com.alipay.rebate.shop.service.admin.ActivityProductService;
import com.alipay.rebate.shop.utils.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

//@Component
public class OrdersScheduler {

  private Logger logger = LoggerFactory.getLogger(OrdersScheduler.class);
  @Autowired
  private PaidOrdersImporter paidOrdersImporter;
  @Autowired
  private AcOrdersImporter acOrdersImporter;
  @Autowired
  private OverdueOrdersImporter overdueOrdersImporter;
  @Autowired
  private CrOrdersImporter crOrdersImporter;
  @Autowired
  private PddOrdersImport pddOrdersImport;
  @Autowired
  JdOrdersImport jdOrdersImport;
  @Resource
  private ActivityProductService activityProductService;
  @Resource
  private MeiTuanOrderHelper meiTuanOrderHelper;

  /**
   * 每分钟导入付款的订单
   * 导入20 分钟前到现在的订单
   */
  @Scheduled(fixedRate = 1000 * 60 * 3)
  public void importPaidOrders() {
    String now = DateUtil.getNowStr();
    String tmb = DateUtil.getTwentyMinutesBeforeFromNow();
    logger.debug("now is and tmb is :{},{}",now,tmb);
    paidOrdersImporter.importOrders(tmb,now, TypeTkStatus.RELATION_PAID);
    paidOrdersImporter.importOrders(tmb,now, TypeTkStatus.SPECIAL_PAID);
    paidOrdersImporter.importOrders(tmb,now, TypeTkStatus.NORMAL_PAID);
  }

  /**
   * 每2分钟导入结算的订单
   * 导入20分钟前到现在的订单
   */
  @Scheduled(fixedRate = 1000 * 60 * 6)
  public void importAccountingOrders() {
    String now = DateUtil.getNowStr();
    String tmb = DateUtil.getTwentyMinutesBeforeFromNow();
    logger.debug("now is and tmb is :{},{}",now,tmb);

    acOrdersImporter.importOrders(tmb,now, TypeTkStatus.RELATION_ACCOUNTING);
    acOrdersImporter.importOrders(tmb,now, TypeTkStatus.SPECIAL_ACCOUNTING);
    acOrdersImporter.importOrders(tmb,now, TypeTkStatus.NORMAL_ACCOUNTING);

    crOrdersImporter.importOrders(tmb,now, TypeTkStatus.RELATION_COMPLETE);
    crOrdersImporter.importOrders(tmb,now, TypeTkStatus.SPECIAL_COMPLETE);
    crOrdersImporter.importOrders(tmb,now, TypeTkStatus.NORMAL_COMPLETE);
  }

  /**
   * 每5分钟导入失效的订单
   * 导入20分钟前到现在的订单
   */
  @Scheduled(fixedRate = 1000 * 60 * 10)
  public void importOverDueOrdersByMinute() throws InterruptedException {
    String now = DateUtil.getNowStr();
    String tmb = DateUtil.getTwentyMinutesBeforeFromNow();
    logger.debug("now is and tmb is :{},{}",now,tmb);
    overdueOrdersImporter.importOrders(tmb,now, TypeTkStatus.RELATION_OVERDUE);
    overdueOrdersImporter.importOrders(tmb,now, TypeTkStatus.SPECIAL_OVERDUE);
    overdueOrdersImporter.importOrders(tmb,now, TypeTkStatus.NORMAL_OVERDUE);
  }

  /**
   * 每天三点导入失效的订单
   * 从15天前的3点开始导入，时间间隔为20分钟
   */
  @Scheduled(cron = "0 0 02 ? * *")
//  @Scheduled(fixedRate = 1000 * 60 * 2)
  public void importOverDueOrders() throws InterruptedException {
    LocalDateTime localDateTime = LocalDateTime.now();
    int year = localDateTime.getYear();
    int month = localDateTime.getMonthValue();
    int day = localDateTime.getDayOfMonth();
    LocalDateTime dateTime = LocalDateTime.of(year,month,day,3,0,0);
    String date = DateUtil.getFifteenDayBefore(dateTime);
    overdueOrdersImporter.importOverDueOrders(date,dateTime, TypeTkStatus.RELATION_OVERDUE);
    overdueOrdersImporter.importOverDueOrders(date,dateTime, TypeTkStatus.SPECIAL_OVERDUE);
    overdueOrdersImporter.importOverDueOrders(date,dateTime, TypeTkStatus.NORMAL_OVERDUE);

  }

  /**
   * 每天5点导入付款和结算的订单
   * 从昨天5点开始导入，时间间隔为20分钟
   */
  @Scheduled(cron = "0 0 05 ? * *")
  public void importPaidAndAccountingOrders() throws InterruptedException {
    LocalDateTime localDateTime = LocalDateTime.now();
    int year = localDateTime.getYear();
    int month = localDateTime.getMonthValue();
    int day = localDateTime.getDayOfMonth();
    LocalDateTime dateTime = LocalDateTime.of(year,month,day,5,0,0);
    String date = DateUtil.getFifteenDayBefore(dateTime);
    paidOrdersImporter.importPaidAndAccountingOrders(date,dateTime,TypeTkStatus.RELATION_PAID);
    paidOrdersImporter.importPaidAndAccountingOrders(date,dateTime,TypeTkStatus.SPECIAL_PAID);
    paidOrdersImporter.importPaidAndAccountingOrders(date,dateTime,TypeTkStatus.NORMAL_PAID);

    acOrdersImporter.importPaidAndAccountingOrders(date,dateTime,TypeTkStatus.RELATION_ACCOUNTING);
    acOrdersImporter.importPaidAndAccountingOrders(date,dateTime,TypeTkStatus.SPECIAL_ACCOUNTING);
    acOrdersImporter.importPaidAndAccountingOrders(date,dateTime,TypeTkStatus.NORMAL_ACCOUNTING);
  }

  /**
   * 每分钟导入订单
   */
  @Scheduled(fixedRate = 1000 * 60 * 1)
  public void importPddOrdersByMinute() throws IOException, ParseException {
    logger.debug("import PddOrders start");
    long end = System.currentTimeMillis();
    long time = end/1000;
    pddOrdersImport.importOrders(time-1200,time,50,null);
  }

  /**
   * 每小时导入
   */
  @Scheduled(cron = "0 0 * * * ?")
  public void importOOrdersByHour() throws IOException, ParseException {
    logger.debug("import PddOrders start");
    long end = System.currentTimeMillis();
    long time = end/1000;
    pddOrdersImport.importOrders(time-7200,time,null,null);
  }

  /**
   * 每天五点导入pdd订单
   * 从昨天5点开始导入前1天的
   */
  @Scheduled(cron = "0 0 04 ? * *")
  public void importOrdersByDay() throws IOException, ParseException {
    logger.debug("import PddOrders start");
    long end = System.currentTimeMillis();
    long time = end/1000;
    //昨天
    pddOrdersImport.importOrders(time-86400,time,null,null);
  }

  /**
   * 京东订单
   * 根据下单时间每20分钟导入
   */
  @Scheduled(fixedRate = 1000 * 60 * 20)
  public void importJdOrdersByHour() throws IOException {
    logger.debug("import jdOrders by hour");
    LocalDateTime localDateTime = LocalDateTime.now();
    String lastHour = DateUtil.getLastHour(localDateTime);
    jdOrdersImport.importJdOrders(lastHour,1,200,1,null);

  }
  /**
   * 京东订单
   * 根据完成时间每20分钟导入
   */
  @Scheduled(fixedRate = 1000 * 60 * 20)
  public void importJdOrdersByHourTwo() throws IOException {
    logger.debug("import jdOrders by hour");
    LocalDateTime localDateTime = LocalDateTime.now();
    String lastHour = DateUtil.getLastHour(localDateTime);
    jdOrdersImport.importJdOrders(lastHour,1,200,2,null);

  }
  /**
   * 京东订单
   * 根据更新时间每20分钟导入
   */
  @Scheduled(fixedRate = 1000 * 60 * 20)
  public void importJdOrdersByHourThree() throws IOException {
    logger.debug("import jdOrders by hour");
    LocalDateTime localDateTime = LocalDateTime.now();
    String lastHour = DateUtil.getLastHour(localDateTime);
    jdOrdersImport.importJdOrders(lastHour,1,200,3,null);

  }

  /**
   * 下单时间
   * 每分钟导入订单
   */
  @Scheduled(fixedRate = 1000 * 60 * 1)
  public void importJdOrdersByMinute() throws IOException, ParseException {
    logger.debug("import jdOrders by minute");
    String beforeMinute = DateUtil.getBeforeMinute();
    jdOrdersImport.importJdOrders(beforeMinute,1,200,1,null);
  }
  /**
   * 完成时间
   * 每分钟导入订单
   */
  @Scheduled(fixedRate = 1000 * 60 * 1)
  public void importJdOrdersByMinuteTwo() throws IOException, ParseException {
    logger.debug("import jdOrders by minute");
    String beforeMinute = DateUtil.getBeforeMinute();
    jdOrdersImport.importJdOrders(beforeMinute,1,200,2,null);
  }
  /**
   * 更新时间
   * 每分钟导入订单
   */
  @Scheduled(fixedRate = 1000 * 60 * 1)
  public void importJdOrdersByMinuteThree() throws IOException, ParseException {
    logger.debug("import jdOrders by minute");
    String beforeMinute = DateUtil.getBeforeMinute();
    jdOrdersImport.importJdOrders(beforeMinute,1,200,3,null);
  }

  /**
   * 每天0点修改已失效的活动商品
   */
  @Scheduled(cron = "0 0 * * * ?")
  public void updateActivityProduct(){
    activityProductService.updateActivityProduct();
  }

  /**
   * 每月21号结算上个月已完成状态的京东订单
   */
  @Scheduled(cron = "0 0 1 21 * ?")
  public void handleLastMonthCompletionStatusOrders(){
    logger.debug("handleLastMonthCompletionStatusOrders");
    jdOrdersImport.handleLastMonthCompletionStatusOrders();
  }
//  /**
//   * 每月11号结算上个月之前的美团订单
//   */
//  @Scheduled(cron = "0 0 1 11 * ?")
//  public void handleLastMonthCompletionMeituanOrder(){
//    logger.debug("handleLastMonthCompletionMeituanOrder");
//    meiTuanOrderHelper.handleLastMonthCompletionMeituanOrder();
//  }

}
