package com.alipay.rebate.shop.scheduler;

public class AwardGradValue {

  private Integer begin;
  private Integer end;
  private Long award;

  public Integer getBegin() {
    return begin;
  }

  public void setBegin(Integer begin) {
    this.begin = begin;
  }

  public Integer getEnd() {
    return end;
  }

  public void setEnd(Integer end) {
    this.end = end;
  }

  public Long getAward() {
    return award;
  }

  public void setAward(Long award) {
    this.award = award;
  }

  @Override
  public String toString() {
    return "AwardGradValue{" +
        "begin=" + begin +
        ", end=" + end +
        ", award=" + award +
        '}';
  }
}
