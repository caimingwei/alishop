package com.alipay.rebate.shop.scheduler;


@FunctionalInterface
public interface CustomFunction {

  void doBusiness(
      String startTime,String endTime,
      String startHour, String endHour
  );
}
