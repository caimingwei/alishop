package com.alipay.rebate.shop.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

//@Component
public class UserIncomeScheduler {

  private Logger logger = LoggerFactory.getLogger(UserIncomeScheduler.class);
  @Autowired
  private UserIncomeScanner userIncomeScanner;

  /**
   * 每天5点导入付款和结算的订单
   * 从昨天5点开始导入，时间间隔为20分钟
   */
  @Scheduled(fixedRate = 1000 * 60 * 15)
//  @Scheduled(cron = "0 0 04 ? * *")
  public void scanFreezeUserIncome() throws InterruptedException {
    logger.debug("scanFreezeUserIncome");
    userIncomeScanner.scanFreezeUserIncome();
  }
}
