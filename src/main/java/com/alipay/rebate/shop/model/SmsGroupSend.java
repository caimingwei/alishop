package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "sms_group_send")
public class SmsGroupSend {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "send_obj")
    private Integer sendObj;

    @Column(name = "condition_day")
    private Integer conditionDay;

    @Column(name = "send_type")
    private Integer sendType;

    @Column(name = "send_time")
    private Date sendTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "phone_num")
    private String phoneNum;

    private String content;

    private Integer status;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return send_obj
     */
    public Integer getSendObj() {
        return sendObj;
    }

    /**
     * @param sendObj
     */
    public void setSendObj(Integer sendObj) {
        this.sendObj = sendObj;
    }

    /**
     * @return condition_day
     */
    public Integer getConditionDay() {
        return conditionDay;
    }

    /**
     * @param conditionDay
     */
    public void setConditionDay(Integer conditionDay) {
        this.conditionDay = conditionDay;
    }

    /**
     * @return send_type
     */
    public Integer getSendType() {
        return sendType;
    }

    /**
     * @param sendType
     */
    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    /**
     * @return send_time
     */
    public Date getSendTime() {
        return sendTime;
    }

    /**
     * @param sendTime
     */
    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return phone_num
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * @param phoneNum
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    /**
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SmsGroupSend{" +
            "id=" + id +
            ", sendObj=" + sendObj +
            ", conditionDay=" + conditionDay +
            ", sendType=" + sendType +
            ", sendTime=" + sendTime +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", phoneNum='" + phoneNum + '\'' +
            ", content='" + content + '\'' +
            ", status=" + status +
            '}';
    }
}