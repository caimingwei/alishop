package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "refund_orders")
public class RefundOrders {
    @Column(name = "tb_trade_id")
    @Id
    private String tbTradeId;

    @Column(name = "tb_trade_parent_id")
    private String tbTradeParentId;

    @Column(name = "relation_id")
    private Long relationId;

    @Column(name = "tk3rd_pub_id")
    private Long tk3rdPubId;

    @Column(name = "tk_pub_id")
    private Long tkPubId;

    @Column(name = "tk_subsidy_fee_refund3rd_pub")
    private BigDecimal tkSubsidyFeeRefund3rdPub;

    @Column(name = "tk_commission_fee_refund3rd_pub")
    private BigDecimal tkCommissionFeeRefund3rdPub;

    @Column(name = "tk_subsidy_fee_refund_pub")
    private BigDecimal tkSubsidyFeeRefundPub;

    @Column(name = "tk_commission_fee_refund_pub")
    private BigDecimal tkCommissionFeeRefundPub;

    @Column(name = "tk_refund_suit_time")
    private Date tkRefundSuitTime;

    @Column(name = "tk_refund_time")
    private Date tkRefundTime;

    @Column(name = "earning_time")
    private Date earningTime;

    @Column(name = "tb_trade_create_time")
    private Date tbTradeCreateTime;

    @Column(name = "refund_status")
    private Long refundStatus;

    @Column(name = "special_id")
    private Long specialId;

    @Column(name = "refund_fee")
    private BigDecimal refundFee;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "tb_auction_title")
    private String tbAuctionTitle;

    /**
     * @return tb_trade_id
     */
    public String getTbTradeId() {
        return tbTradeId;
    }

    /**
     * @param tbTradeId
     */
    public void setTbTradeId(String tbTradeId) {
        this.tbTradeId = tbTradeId;
    }

    /**
     * @return tb_trade_parent_id
     */
    public String getTbTradeParentId() {
        return tbTradeParentId;
    }

    /**
     * @param tbTradeParentId
     */
    public void setTbTradeParentId(String tbTradeParentId) {
        this.tbTradeParentId = tbTradeParentId;
    }

    /**
     * @return relation_id
     */
    public Long getRelationId() {
        return relationId;
    }

    /**
     * @param relationId
     */
    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    /**
     * @return tk3rd_pub_id
     */
    public Long getTk3rdPubId() {
        return tk3rdPubId;
    }

    /**
     * @param tk3rdPubId
     */
    public void setTk3rdPubId(Long tk3rdPubId) {
        this.tk3rdPubId = tk3rdPubId;
    }

    /**
     * @return tk_pub_id
     */
    public Long getTkPubId() {
        return tkPubId;
    }

    /**
     * @param tkPubId
     */
    public void setTkPubId(Long tkPubId) {
        this.tkPubId = tkPubId;
    }

    /**
     * @return tk_subsidy_fee_refund3rd_pub
     */
    public BigDecimal getTkSubsidyFeeRefund3rdPub() {
        return tkSubsidyFeeRefund3rdPub;
    }

    /**
     * @param tkSubsidyFeeRefund3rdPub
     */
    public void setTkSubsidyFeeRefund3rdPub(BigDecimal tkSubsidyFeeRefund3rdPub) {
        this.tkSubsidyFeeRefund3rdPub = tkSubsidyFeeRefund3rdPub;
    }

    /**
     * @return tk_commission_fee_refund3rd_pub
     */
    public BigDecimal getTkCommissionFeeRefund3rdPub() {
        return tkCommissionFeeRefund3rdPub;
    }

    /**
     * @param tkCommissionFeeRefund3rdPub
     */
    public void setTkCommissionFeeRefund3rdPub(BigDecimal tkCommissionFeeRefund3rdPub) {
        this.tkCommissionFeeRefund3rdPub = tkCommissionFeeRefund3rdPub;
    }

    /**
     * @return tk_subsidy_fee_refund_pub
     */
    public BigDecimal getTkSubsidyFeeRefundPub() {
        return tkSubsidyFeeRefundPub;
    }

    /**
     * @param tkSubsidyFeeRefundPub
     */
    public void setTkSubsidyFeeRefundPub(BigDecimal tkSubsidyFeeRefundPub) {
        this.tkSubsidyFeeRefundPub = tkSubsidyFeeRefundPub;
    }

    /**
     * @return tk_commission_fee_refund_pub
     */
    public BigDecimal getTkCommissionFeeRefundPub() {
        return tkCommissionFeeRefundPub;
    }

    /**
     * @param tkCommissionFeeRefundPub
     */
    public void setTkCommissionFeeRefundPub(BigDecimal tkCommissionFeeRefundPub) {
        this.tkCommissionFeeRefundPub = tkCommissionFeeRefundPub;
    }

    /**
     * @return tk_refund_suit_time
     */
    public Date getTkRefundSuitTime() {
        return tkRefundSuitTime;
    }

    /**
     * @param tkRefundSuitTime
     */
    public void setTkRefundSuitTime(Date tkRefundSuitTime) {
        this.tkRefundSuitTime = tkRefundSuitTime;
    }

    /**
     * @return tk_refund_time
     */
    public Date getTkRefundTime() {
        return tkRefundTime;
    }

    /**
     * @param tkRefundTime
     */
    public void setTkRefundTime(Date tkRefundTime) {
        this.tkRefundTime = tkRefundTime;
    }

    /**
     * @return earning_time
     */
    public Date getEarningTime() {
        return earningTime;
    }

    /**
     * @param earningTime
     */
    public void setEarningTime(Date earningTime) {
        this.earningTime = earningTime;
    }

    /**
     * @return tb_trade_create_time
     */
    public Date getTbTradeCreateTime() {
        return tbTradeCreateTime;
    }

    /**
     * @param tbTradeCreateTime
     */
    public void setTbTradeCreateTime(Date tbTradeCreateTime) {
        this.tbTradeCreateTime = tbTradeCreateTime;
    }

    /**
     * @return refund_status
     */
    public Long getRefundStatus() {
        return refundStatus;
    }

    /**
     * @param refundStatus
     */
    public void setRefundStatus(Long refundStatus) {
        this.refundStatus = refundStatus;
    }

    /**
     * @return special_id
     */
    public Long getSpecialId() {
        return specialId;
    }

    /**
     * @param specialId
     */
    public void setSpecialId(Long specialId) {
        this.specialId = specialId;
    }

    /**
     * @return refund_fee
     */
    public BigDecimal getRefundFee() {
        return refundFee;
    }

    /**
     * @param refundFee
     */
    public void setRefundFee(BigDecimal refundFee) {
        this.refundFee = refundFee;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return tb_auction_title
     */
    public String getTbAuctionTitle() {
        return tbAuctionTitle;
    }

    /**
     * @param tbAuctionTitle
     */
    public void setTbAuctionTitle(String tbAuctionTitle) {
        this.tbAuctionTitle = tbAuctionTitle;
    }
}