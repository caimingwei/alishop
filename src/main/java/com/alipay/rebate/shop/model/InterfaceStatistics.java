package com.alipay.rebate.shop.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "interface_statistics")
public class InterfaceStatistics {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 接口每小时的调用次数
     */
    @Column(name = "one_hour_count")
    private Long oneHourCount;

    /**
     * 接口每天的调用次数
     */
    @Column(name = "one_day_count")
    private Long oneDayCount;


    /**
     * 当天的日期
     */
    @Column(name = "the_same_day")
    private Date theSameDay;

    private String name;

    private Long oneMinuteCount;

    private String minute;

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public Long getOneMinuteCount() {
        return oneMinuteCount;
    }

    public void setOneMinuteCount(Long oneMinuteCount) {
        this.oneMinuteCount = oneMinuteCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取接口每小时的调用次数
     *
     * @return one_hour_count - 接口每小时的调用次数
     */
    public Long getOneHourCount() {
        return oneHourCount;
    }

    /**
     * 设置接口每小时的调用次数
     *
     * @param oneHourCount 接口每小时的调用次数
     */
    public void setOneHourCount(Long oneHourCount) {
        this.oneHourCount = oneHourCount;
    }

    /**
     * 获取接口每天的调用次数
     *
     * @return one_day_count - 接口每天的调用次数
     */
    public Long getOneDayCount() {
        return oneDayCount;
    }

    /**
     * 设置接口每天的调用次数
     *
     * @param oneDayCount 接口每天的调用次数
     */
    public void setOneDayCount(Long oneDayCount) {
        this.oneDayCount = oneDayCount;
    }


    /**
     * 获取当天的日期
     *
     * @return the_same_day - 当天的日期
     */
    public Date getTheSameDay() {
        return theSameDay;
    }

    /**
     * 设置当天的日期
     *
     * @param theSameDay 当天的日期
     */
    public void setTheSameDay(Date theSameDay) {
        this.theSameDay = theSameDay;
    }

    @Override
    public String toString() {
        return "InterfaceStatistics{" +
                "id=" + id +
                ", oneHourCount=" + oneHourCount +
                ", oneDayCount=" + oneDayCount +
                ", theSameDay=" + theSameDay +
                ", name='" + name + '\'' +
                ", oneMinuteCount=" + oneMinuteCount +
                ", minute='" + minute + '\'' +
                '}';
    }
}