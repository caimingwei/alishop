package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "user_grade")
public class UserGrade {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "grade_name")
    private String gradeName;

    @Column(name = "grade_weight")
    private Double gradeWeight;

    @Column(name = "first_commission")
    private Double firstCommission;

    @Column(name = "second_commission")
    private Double secondCommission;

    @Column(name = "third_commission")
    private Double thirdCommission;

    @Column(name = "enable_cmlevel_settings")
    private Integer enableCmlevelSettings;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return grade_name
     */
    public String getGradeName() {
        return gradeName;
    }

    /**
     * @param gradeName
     */
    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    /**
     * @return grade_weight
     */
    public Double getGradeWeight() {
        return gradeWeight;
    }

    /**
     * @param gradeWeight
     */
    public void setGradeWeight(Double gradeWeight) {
        this.gradeWeight = gradeWeight;
    }

    /**
     * @return first_commission
     */
    public Double getFirstCommission() {
        return firstCommission;
    }

    /**
     * @param firstCommission
     */
    public void setFirstCommission(Double firstCommission) {
        this.firstCommission = firstCommission;
    }

    /**
     * @return second_commission
     */
    public Double getSecondCommission() {
        return secondCommission;
    }

    /**
     * @param secondCommission
     */
    public void setSecondCommission(Double secondCommission) {
        this.secondCommission = secondCommission;
    }

    public Double getThirdCommission() {
        return thirdCommission;
    }

    public void setThirdCommission(Double thirdCommission) {
        this.thirdCommission = thirdCommission;
    }

    public Integer getEnableCmlevelSettings() {
        return enableCmlevelSettings;
    }

    public void setEnableCmlevelSettings(Integer enableCmlevelSettings) {
        this.enableCmlevelSettings = enableCmlevelSettings;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}