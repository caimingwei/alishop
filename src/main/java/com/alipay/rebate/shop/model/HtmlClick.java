package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "html_click")
public class HtmlClick {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "html_name")
    private String htmlName;

    @Column(name = "html_chinese_name")
    private String htmlChineseName;

    @Column(name = "click_count")
    private Long clickCount;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "the_same_day")
    private Date theSameDay;

    private String residenceTime;

    private Long accessIpCount;

    public String getResidenceTime() {
        return residenceTime;
    }

    public void setResidenceTime(String residenceTime) {
        this.residenceTime = residenceTime;
    }

    public Long getAccessIpCount() {
        return accessIpCount;
    }

    public void setAccessIpCount(Long accessIpCount) {
        this.accessIpCount = accessIpCount;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return html_name
     */
    public String getHtmlName() {
        return htmlName;
    }

    /**
     * @param htmlName
     */
    public void setHtmlName(String htmlName) {
        this.htmlName = htmlName;
    }

    /**
     * @return html_chinese_name
     */
    public String getHtmlChineseName() {
        return htmlChineseName;
    }

    /**
     * @param htmlChineseName
     */
    public void setHtmlChineseName(String htmlChineseName) {
        this.htmlChineseName = htmlChineseName;
    }

    /**
     * @return click_count
     */
    public Long getClickCount() {
        return clickCount;
    }

    /**
     * @param clickCount
     */
    public void setClickCount(Long clickCount) {
        this.clickCount = clickCount;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return the_same_day
     */
    public Date getTheSameDay() {
        return theSameDay;
    }

    /**
     * @param theSameDay
     */
    public void setTheSameDay(Date theSameDay) {
        this.theSameDay = theSameDay;
    }

    @Override
    public String toString() {
        return "HtmlClick{" +
                "id=" + id +
                ", htmlName='" + htmlName + '\'' +
                ", htmlChineseName='" + htmlChineseName + '\'' +
                ", clickCount=" + clickCount +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", theSameDay=" + theSameDay +
                ", residenceTime='" + residenceTime + '\'' +
                ", accessIpCount=" + accessIpCount +
                '}';
    }
}