package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "admin_user")
public class AdminUser {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "user_account")
    private String userAccount;

    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "last_login_time")
    private Date lastLoginTime;

    @Column(name = "newly_login_time")
    private Date newlyLoginTime;

    @Column(name = "login_times")
    private Integer loginTimes;

    @Column(name = "user_register_time")
    private Date userRegisterTime;

    @Column(name = "user_register_ip")
    private String userRegisterIp;

    @Column(name = "last_login_ip")
    private String lastLoginIp;

    @Column(name = "newly_login_ip")
    private String newlyLoginIp;

    @Column(name = "group_id")
    private Integer groupId;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return user_account
     */
    public String getUserAccount() {
        return userAccount;
    }

    /**
     * @param userAccount
     */
    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    /**
     * @return user_password
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return last_login_time
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * @param lastLoginTime
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * @return newly_login_time
     */
    public Date getNewlyLoginTime() {
        return newlyLoginTime;
    }

    /**
     * @param newlyLoginTime
     */
    public void setNewlyLoginTime(Date newlyLoginTime) {
        this.newlyLoginTime = newlyLoginTime;
    }

    /**
     * @return login_times
     */
    public Integer getLoginTimes() {
        return loginTimes;
    }

    /**
     * @param loginTimes
     */
    public void setLoginTimes(Integer loginTimes) {
        this.loginTimes = loginTimes;
    }

    /**
     * @return user_register_time
     */
    public Date getUserRegisterTime() {
        return userRegisterTime;
    }

    /**
     * @param userRegisterTime
     */
    public void setUserRegisterTime(Date userRegisterTime) {
        this.userRegisterTime = userRegisterTime;
    }

    /**
     * @return user_register_ip
     */
    public String getUserRegisterIp() {
        return userRegisterIp;
    }

    /**
     * @param userRegisterIp
     */
    public void setUserRegisterIp(String userRegisterIp) {
        this.userRegisterIp = userRegisterIp;
    }

    /**
     * @return last_login_ip
     */
    public String getLastLoginIp() {
        return lastLoginIp;
    }

    /**
     * @param lastLoginIp
     */
    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    /**
     * @return newly_login_ip
     */
    public String getNewlyLoginIp() {
        return newlyLoginIp;
    }

    /**
     * @param newlyLoginIp
     */
    public void setNewlyLoginIp(String newlyLoginIp) {
        this.newlyLoginIp = newlyLoginIp;
    }

    /**
     * @return group_id
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}