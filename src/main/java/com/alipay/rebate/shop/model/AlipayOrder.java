package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "alipay_order")
public class AlipayOrder {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    /**
     * 商户转账唯一订单号：发起转账来源方定义的转账单据号。请求时对应的参数，原样返回。
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 支付宝转账单据号，成功一定返回，失败可能不返回也可能返回。
     */
    @Column(name = "order_id")
    private String orderId;

    /**
     * 收款方账户 支付宝账号（手机或邮箱）
     */
    @Column(name = "payee_account")
    private String payeeAccount;

    /**
     * 转账金额 只支持2位小数，小数点前最大支持13位，金额必须大于等于0.1元
     */
    private String amount;

    /**
     * 付款方姓名（最长支持100个英文/50个汉字）。显示在收款方的账单详情页。如果该字段不传，则默认显示付款方的支付宝认证姓名或单位名称。
     */
    @Column(name = "payee_show_name")
    private String payeeShowName;

    /**
     * 收款方真实姓名（最长支持100个英文/50个汉字）。 
     */
    @Column(name = "payee_real_name")
    private String payeeRealName;

    /**
     * 转账备注（支持200个英文/100个汉字）。 
     */
    private String remark;

    /**
     * 接口响应code
     */
    @Column(name = "response_code")
    private String responseCode;

    /**
     * 接口响应信息
     */
    @Column(name = "response_msg")
    private String responseMsg;

    /**
     * 支付时间：格式为yyyy-MM-dd HH:mm:ss，仅转账成功返回。
     */
    @Column(name = "pay_date")
    private String payDate;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "withdraw_id")
    private Long withdrawId;

    @Column(name = "body")
    private String body;

    @Column(name = "sub_code")
    private String subCode;

    public Long getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(Long withdrawId) {
        this.withdrawId = withdrawId;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商户转账唯一订单号：发起转账来源方定义的转账单据号。请求时对应的参数，原样返回。
     *
     * @return order_no - 商户转账唯一订单号：发起转账来源方定义的转账单据号。请求时对应的参数，原样返回。
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置商户转账唯一订单号：发起转账来源方定义的转账单据号。请求时对应的参数，原样返回。
     *
     * @param orderNo 商户转账唯一订单号：发起转账来源方定义的转账单据号。请求时对应的参数，原样返回。
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取支付宝转账单据号，成功一定返回，失败可能不返回也可能返回。
     *
     * @return order_id - 支付宝转账单据号，成功一定返回，失败可能不返回也可能返回。
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置支付宝转账单据号，成功一定返回，失败可能不返回也可能返回。
     *
     * @param orderId 支付宝转账单据号，成功一定返回，失败可能不返回也可能返回。
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取收款方账户 支付宝账号（手机或邮箱）
     *
     * @return payee_account - 收款方账户 支付宝账号（手机或邮箱）
     */
    public String getPayeeAccount() {
        return payeeAccount;
    }

    /**
     * 设置收款方账户 支付宝账号（手机或邮箱）
     *
     * @param payeeAccount 收款方账户 支付宝账号（手机或邮箱）
     */
    public void setPayeeAccount(String payeeAccount) {
        this.payeeAccount = payeeAccount;
    }

    /**
     * 获取转账金额 只支持2位小数，小数点前最大支持13位，金额必须大于等于0.1元
     *
     * @return amount - 转账金额 只支持2位小数，小数点前最大支持13位，金额必须大于等于0.1元
     */
    public String getAmount() {
        return amount;
    }

    /**
     * 设置转账金额 只支持2位小数，小数点前最大支持13位，金额必须大于等于0.1元
     *
     * @param amount 转账金额 只支持2位小数，小数点前最大支持13位，金额必须大于等于0.1元
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * 获取付款方姓名（最长支持100个英文/50个汉字）。显示在收款方的账单详情页。如果该字段不传，则默认显示付款方的支付宝认证姓名或单位名称。
     *
     * @return payee_show_name - 付款方姓名（最长支持100个英文/50个汉字）。显示在收款方的账单详情页。如果该字段不传，则默认显示付款方的支付宝认证姓名或单位名称。
     */
    public String getPayeeShowName() {
        return payeeShowName;
    }

    /**
     * 设置付款方姓名（最长支持100个英文/50个汉字）。显示在收款方的账单详情页。如果该字段不传，则默认显示付款方的支付宝认证姓名或单位名称。
     *
     * @param payeeShowName 付款方姓名（最长支持100个英文/50个汉字）。显示在收款方的账单详情页。如果该字段不传，则默认显示付款方的支付宝认证姓名或单位名称。
     */
    public void setPayeeShowName(String payeeShowName) {
        this.payeeShowName = payeeShowName;
    }

    /**
     * 获取收款方真实姓名（最长支持100个英文/50个汉字）。 
     *
     * @return payee_real_name - 收款方真实姓名（最长支持100个英文/50个汉字）。 
     */
    public String getPayeeRealName() {
        return payeeRealName;
    }

    /**
     * 设置收款方真实姓名（最长支持100个英文/50个汉字）。 
     *
     * @param payeeRealName 收款方真实姓名（最长支持100个英文/50个汉字）。 
     */
    public void setPayeeRealName(String payeeRealName) {
        this.payeeRealName = payeeRealName;
    }

    /**
     * 获取转账备注（支持200个英文/100个汉字）。 
     *
     * @return remark - 转账备注（支持200个英文/100个汉字）。 
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置转账备注（支持200个英文/100个汉字）。 
     *
     * @param remark 转账备注（支持200个英文/100个汉字）。 
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取接口响应code
     *
     * @return response_code - 接口响应code
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * 设置接口响应code
     *
     * @param responseCode 接口响应code
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * 获取接口响应信息
     *
     * @return response_msg - 接口响应信息
     */
    public String getResponseMsg() {
        return responseMsg;
    }

    /**
     * 设置接口响应信息
     *
     * @param responseMsg 接口响应信息
     */
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    /**
     * 获取支付时间：格式为yyyy-MM-dd HH:mm:ss，仅转账成功返回。
     *
     * @return pay_date - 支付时间：格式为yyyy-MM-dd HH:mm:ss，仅转账成功返回。
     */
    public String getPayDate() {
        return payDate;
    }

    /**
     * 设置支付时间：格式为yyyy-MM-dd HH:mm:ss，仅转账成功返回。
     *
     * @param payDate 支付时间：格式为yyyy-MM-dd HH:mm:ss，仅转账成功返回。
     */
    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    /**
     *
     * @return body
     */
    public String getBody() {
        return body;
    }

    /**
     *
     * @param body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     *
     * @return subCode
     */
    public String getSubCode() {
        return subCode;
    }

    /**
     *
     * @param subCode
     */
    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }
}