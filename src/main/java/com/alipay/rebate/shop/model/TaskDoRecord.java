package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "task_do_record")
public class TaskDoRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    private Integer type;

    @Column(name = "sub_type")
    private Integer subType;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return sub_type
     */
    public Integer getSubType() {
        return subType;
    }

    /**
     * @param subType
     */
    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TaskDoRecord{" +
                "id=" + id +
                ", userId=" + userId +
                ", type=" + type +
                ", subType=" + subType +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}