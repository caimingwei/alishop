package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "meituan_orders")
public class MeituanOrders {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 订单号
     */
    @Column(name = "order_id")
    private String orderId;

    private String sequence;

    /**
     * 使用时间
     */
    @Column(name = "use_time")
    private String useTime;

    /**
     * 用户支付金额
     */
    private BigDecimal price;

    /**
     * 返佣金额
     */
    private BigDecimal profit;

    /**
     * 是否新客 0 否 1 是
     */
    @Column(name = "new_buyer")
    private String newBuyer;

    /**
     * 订单标题
     */
    @Column(name = "sms_title")
    private String smsTitle;

    /**
     * 卖家昵称
     */
    private String uid;

    /**
     * 订单总价
     */
    private BigDecimal total;

    /**
     * 可忽略(订单数量)
     */
    private String quantity;

    /**
     * 商品 id
     */
    @Column(name = "deal_id")
    private String dealId;

    /**
     * 更新时间
     */
    private String modtime;

    /**
     * 订单付款金额
     */
    private BigDecimal direct;

    /**
     * 第三方推广者ID
     */
    private String sign;

    /**
     * 付款时间
     */
    @Column(name = "pay_time")
    private String payTime;

    /**
     * 渠道方用户唯一标识
     */
    private String sid;

    /**
     * 用户所得佣金
     */
    @Column(name = "user_commission")
    private BigDecimal userCommission;

    /**
     * 上级所得佣金
     */
    @Column(name = "one_level_commission")
    private BigDecimal oneLevelCommission;

    /**
     * 上上级所得佣金
     */
    @Column(name = "two_level_commission")
    private BigDecimal twoLevelCommission;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_name")
    private String userName;

    /**
     * 略
     */
    @Column(name = "promoter_id")
    private Long promoterId;

    @Column(name = "promoter_name")
    private String promoterName;

    /**
     * 用户所得集分宝
     */
    @Column(name = "user_uit")
    private Long userUit;

    /**
     * 上级所得集分宝
     */
    @Column(name = "one_level_user_uit")
    private Long oneLevelUserUit;

    /**
     * 上上级所得集分宝
     */
    @Column(name = "two_level_user_uit")
    private Long twoLevelUserUit;

    @Column(name = "is_record")
    private Integer isRecord;

    public BigDecimal getProfit() {
        return profit;
    }

    /**
     * 获取订单号
     *
     * @return order_id - 订单号
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置订单号
     *
     * @param orderId 订单号
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return sequence
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * @param sequence
     */
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    /**
     * 获取使用时间
     *
     * @return use_time - 使用时间
     */
    public String getUseTime() {
        return useTime;
    }

    /**
     * 设置使用时间
     *
     * @param useTime 使用时间
     */
    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    /**
     * 获取用户支付金额
     *
     * @return price - 用户支付金额
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 设置用户支付金额
     *
     * @param price 用户支付金额
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    /**
     * 获取是否新客 0 否 1 是
     *
     * @return new_buyer - 是否新客 0 否 1 是
     */
    public String getNewBuyer() {
        return newBuyer;
    }

    /**
     * 设置是否新客 0 否 1 是
     *
     * @param newBuyer 是否新客 0 否 1 是
     */
    public void setNewBuyer(String newBuyer) {
        this.newBuyer = newBuyer;
    }

    /**
     * 获取订单标题
     *
     * @return sms_title - 订单标题
     */
    public String getSmsTitle() {
        return smsTitle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 设置订单标题
     *
     * @param smsTitle 订单标题
     */
    public void setSmsTitle(String smsTitle) {
        this.smsTitle = smsTitle;
    }

    /**
     * 获取卖家昵称
     *
     * @return uid - 卖家昵称
     */
    public String getUid() {
        return uid;
    }

    /**
     * 设置卖家昵称
     *
     * @param uid 卖家昵称
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * 获取订单总价
     *
     * @return total - 订单总价
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * 设置订单总价
     *
     * @param total 订单总价
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * 获取可忽略(订单数量)
     *
     * @return quantity - 可忽略(订单数量)
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * 设置可忽略(订单数量)
     *
     * @param quantity 可忽略(订单数量)
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * 获取商品 id
     *
     * @return deal_id - 商品 id
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 设置商品 id
     *
     * @param dealId 商品 id
     */
    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    /**
     * 获取更新时间
     *
     * @return modtime - 更新时间
     */
    public String getModtime() {
        return modtime;
    }

    /**
     * 设置更新时间
     *
     * @param modtime 更新时间
     */
    public void setModtime(String modtime) {
        this.modtime = modtime;
    }

    /**
     * 获取订单付款金额
     *
     * @return direct - 订单付款金额
     */
    public BigDecimal getDirect() {
        return direct;
    }

    /**
     * 设置订单付款金额
     *
     * @param direct 订单付款金额
     */
    public void setDirect(BigDecimal direct) {
        this.direct = direct;
    }

    /**
     * 获取第三方推广者ID
     *
     * @return sign - 第三方推广者ID
     */
    public String getSign() {
        return sign;
    }

    /**
     * 设置第三方推广者ID
     *
     * @param sign 第三方推广者ID
     */
    public void setSign(String sign) {
        this.sign = sign;
    }

    /**
     * 获取付款时间
     *
     * @return pay_time - 付款时间
     */
    public String getPayTime() {
        return payTime;
    }

    /**
     * 设置付款时间
     *
     * @param payTime 付款时间
     */
    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    /**
     * 获取渠道方用户唯一标识
     *
     * @return sid - 渠道方用户唯一标识
     */
    public String getSid() {
        return sid;
    }

    /**
     * 设置渠道方用户唯一标识
     *
     * @param sid 渠道方用户唯一标识
     */
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     * 获取用户所得佣金
     *
     * @return user_commission - 用户所得佣金
     */
    public BigDecimal getUserCommission() {
        return userCommission;
    }

    /**
     * 设置用户所得佣金
     *
     * @param userCommission 用户所得佣金
     */
    public void setUserCommission(BigDecimal userCommission) {
        this.userCommission = userCommission;
    }

    /**
     * 获取上级所得佣金
     *
     * @return one_level_commission - 上级所得佣金
     */
    public BigDecimal getOneLevelCommission() {
        return oneLevelCommission;
    }

    /**
     * 设置上级所得佣金
     *
     * @param oneLevelCommission 上级所得佣金
     */
    public void setOneLevelCommission(BigDecimal oneLevelCommission) {
        this.oneLevelCommission = oneLevelCommission;
    }

    /**
     * 获取上上级所得佣金
     *
     * @return two_level_commission - 上上级所得佣金
     */
    public BigDecimal getTwoLevelCommission() {
        return twoLevelCommission;
    }

    /**
     * 设置上上级所得佣金
     *
     * @param twoLevelCommission 上上级所得佣金
     */
    public void setTwoLevelCommission(BigDecimal twoLevelCommission) {
        this.twoLevelCommission = twoLevelCommission;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return user_name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取略
     *
     * @return promoter_id - 略
     */
    public Long getPromoterId() {
        return promoterId;
    }

    /**
     * 设置略
     *
     * @param promoterId 略
     */
    public void setPromoterId(Long promoterId) {
        this.promoterId = promoterId;
    }

    /**
     * @return promoter_name
     */
    public String getPromoterName() {
        return promoterName;
    }

    /**
     * @param promoterName
     */
    public void setPromoterName(String promoterName) {
        this.promoterName = promoterName;
    }

    /**
     * 获取用户所得集分宝
     *
     * @return user_uit - 用户所得集分宝
     */
    public Long getUserUit() {
        return userUit;
    }

    /**
     * 设置用户所得集分宝
     *
     * @param userUit 用户所得集分宝
     */
    public void setUserUit(Long userUit) {
        this.userUit = userUit;
    }

    /**
     * 获取上级所得集分宝
     *
     * @return one_level_user_uit - 上级所得集分宝
     */
    public Long getOneLevelUserUit() {
        return oneLevelUserUit;
    }

    /**
     * 设置上级所得集分宝
     *
     * @param oneLevelUserUit 上级所得集分宝
     */
    public void setOneLevelUserUit(Long oneLevelUserUit) {
        this.oneLevelUserUit = oneLevelUserUit;
    }

    /**
     * 获取上上级所得集分宝
     *
     * @return two_level_user_uit - 上上级所得集分宝
     */
    public Long getTwoLevelUserUit() {
        return twoLevelUserUit;
    }

    /**
     * 设置上上级所得集分宝
     *
     * @param twoLevelUserUit 上上级所得集分宝
     */
    public void setTwoLevelUserUit(Long twoLevelUserUit) {
        this.twoLevelUserUit = twoLevelUserUit;
    }

    /**
     * @return is_record
     */
    public Integer getIsRecord() {
        return isRecord;
    }

    /**
     * @param isRecord
     */
    public void setIsRecord(Integer isRecord) {
        this.isRecord = isRecord;
    }

    @Override
    public String toString() {
        return "MeituanOrders{" +
                "id=" + id +
                ", orderId='" + orderId + '\'' +
                ", sequence='" + sequence + '\'' +
                ", useTime='" + useTime + '\'' +
                ", price=" + price +
                ", profit=" + profit +
                ", newBuyer='" + newBuyer + '\'' +
                ", smsTitle='" + smsTitle + '\'' +
                ", uid='" + uid + '\'' +
                ", total=" + total +
                ", quantity='" + quantity + '\'' +
                ", dealId='" + dealId + '\'' +
                ", modtime='" + modtime + '\'' +
                ", direct=" + direct +
                ", sign='" + sign + '\'' +
                ", payTime='" + payTime + '\'' +
                ", sid='" + sid + '\'' +
                ", userCommission=" + userCommission +
                ", oneLevelCommission=" + oneLevelCommission +
                ", twoLevelCommission=" + twoLevelCommission +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", promoterId=" + promoterId +
                ", promoterName='" + promoterName + '\'' +
                ", userUit=" + userUit +
                ", oneLevelUserUit=" + oneLevelUserUit +
                ", twoLevelUserUit=" + twoLevelUserUit +
                ", isRecord=" + isRecord +
                '}';
    }
}