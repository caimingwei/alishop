package com.alipay.rebate.shop.model;

import javax.persistence.*;

@Table(name = "invite_share_url_settings")
public class InviteShareUrlSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String url;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "InviteShareUrlSettings{" +
            "id=" + id +
            ", url='" + url + '\'' +
            '}';
    }
}