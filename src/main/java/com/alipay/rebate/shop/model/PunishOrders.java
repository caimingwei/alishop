package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "punish_orders")
public class PunishOrders {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "work_order_id")
    private String workOrderId;

    @Column(name = "trade_id")
    private String tradeId;

    private String type;

    @Column(name = "punish_money")
    private BigDecimal punishMoney;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return work_order_id
     */
    public String getWorkOrderId() {
        return workOrderId;
    }

    /**
     * @param workOrderId
     */
    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    /**
     * @return trade_id
     */
    public String getTradeId() {
        return tradeId;
    }

    /**
     * @param tradeId
     */
    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return punish_money
     */
    public BigDecimal getPunishMoney() {
        return punishMoney;
    }

    /**
     * @param punishMoney
     */
    public void setPunishMoney(BigDecimal punishMoney) {
        this.punishMoney = punishMoney;
    }
}