package com.alipay.rebate.shop.model;

import javax.persistence.*;

@Table(name = "super_category")
public class SuperCategory {
    private Integer cid;

    private String cname;

    private String cpic;

    private String subcategories;

    /**
     * @return cid
     */
    public Integer getCid() {
        return cid;
    }

    /**
     * @param cid
     */
    public void setCid(Integer cid) {
        this.cid = cid;
    }

    /**
     * @return cname
     */
    public String getCname() {
        return cname;
    }

    /**
     * @param cname
     */
    public void setCname(String cname) {
        this.cname = cname;
    }

    /**
     * @return cpic
     */
    public String getCpic() {
        return cpic;
    }

    /**
     * @param cpic
     */
    public void setCpic(String cpic) {
        this.cpic = cpic;
    }

    /**
     * @return subcategories
     */
    public String getSubcategories() {
        return subcategories;
    }

    /**
     * @param subcategories
     */
    public void setSubcategories(String subcategories) {
        this.subcategories = subcategories;
    }
}