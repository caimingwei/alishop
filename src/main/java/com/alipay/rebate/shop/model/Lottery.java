package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

public class Lottery {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    private String title;

    @Column(name = "max_time")
    private Integer maxTime;

    @Column(name = "free_time")
    private Integer freeTime;

    @Column(name = "consume_jf")
    private Integer consumeJf;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "lottery_describe")
    private String lotteryDescribe;

    @Column(name = "json_data")
    private String jsonData;

    @Column(name = "is_current_use")
    private Integer isCurrentUse;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return max_time
     */
    public Integer getMaxTime() {
        return maxTime;
    }

    /**
     * @param maxTime
     */
    public void setMaxTime(Integer maxTime) {
        this.maxTime = maxTime;
    }

    /**
     * @return free_time
     */
    public Integer getFreeTime() {
        return freeTime;
    }

    /**
     * @param freeTime
     */
    public void setFreeTime(Integer freeTime) {
        this.freeTime = freeTime;
    }

    /**
     * @return consume_jf
     */
    public Integer getConsumeJf() {
        return consumeJf;
    }

    /**
     * @param consumeJf
     */
    public void setConsumeJf(Integer consumeJf) {
        this.consumeJf = consumeJf;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getLotteryDescribe() {
        return lotteryDescribe;
    }

    public void setLotteryDescribe(String lotteryDescribe) {
        this.lotteryDescribe = lotteryDescribe;
    }

    /**
     * @return json_data
     */
    public String getJsonData() {
        return jsonData;
    }

    /**
     * @param jsonData
     */
    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public Integer getIsCurrentUse() {
        return isCurrentUse;
    }

    public void setIsCurrentUse(Integer isCurrentUse) {
        this.isCurrentUse = isCurrentUse;
    }
}