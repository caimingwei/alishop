package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "commission_level_settings")
public class CommissionLevelSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_grade_id")
    private Integer userGradeId;

    private BigDecimal begin;

    private BigDecimal end;

    private BigDecimal rate;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserGradeId() {
        return userGradeId;
    }

    public void setUserGradeId(Integer userGradeId) {
        this.userGradeId = userGradeId;
    }

    /**
     * @return begin
     */
    public BigDecimal getBegin() {
        return begin;
    }

    /**
     * @param begin
     */
    public void setBegin(BigDecimal begin) {
        this.begin = begin;
    }

    /**
     * @return end
     */
    public BigDecimal getEnd() {
        return end;
    }

    /**
     * @param end
     */
    public void setEnd(BigDecimal end) {
        this.end = end;
    }

    /**
     * @return rate
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * @param rate
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CommissionLevelSettings{" +
            "id=" + id +
            ", userGradeId=" + userGradeId +
            ", begin=" + begin +
            ", end=" + end +
            ", rate=" + rate +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            '}';
    }
}