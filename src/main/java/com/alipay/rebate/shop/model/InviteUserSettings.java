package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "invite_user_settings")
public class InviteUserSettings {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    private String imgs;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "rule_html")
    private String ruleHtml;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return imgs
     */
    public String getImgs() {
        return imgs;
    }

    /**
     * @param imgs
     */
    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return rule_html
     */
    public String getRuleHtml() {
        return ruleHtml;
    }

    /**
     * @param ruleHtml
     */
    public void setRuleHtml(String ruleHtml) {
        this.ruleHtml = ruleHtml;
    }
}