package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "jpush_settings")
public class JpushSettings {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    private String name;

    private Integer type;

    @Column(name = "wechat_enable")
    private Integer wechatEnable;

    @Column(name = "sms_enable")
    private Integer smsEnable;

    @Column(name = "app_enable")
    private Integer appEnable;

    @Column(name = "wechat_title")
    private String wechatTitle;

    @Column(name = "wechat_content")
    private String wechatContent;

    @Column(name = "sms_content")
    private String smsContent;

    @Column(name = "sms_sign_name")
    private String smsSignName;

    @Column(name = "sms_template_code")
    private String smsTemplateCode;

    @Column(name = "app_title")
    private String appTitle;

    @Column(name = "app_content")
    private String appContent;

    @Column(name = "app_intent")
    private String appIntent;

    @Column(name = "app_uri_action")
    private String appUriAction;

    @Column(name = "app_uri_activity")
    private String appUriActivity;

    @Column(name = "app_extras_json")
    private String appExtrasJson;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return wechat_enable
     */
    public Integer getWechatEnable() {
        return wechatEnable;
    }

    /**
     * @param wechatEnable
     */
    public void setWechatEnable(Integer wechatEnable) {
        this.wechatEnable = wechatEnable;
    }

    /**
     * @return sms_enable
     */
    public Integer getSmsEnable() {
        return smsEnable;
    }

    /**
     * @param smsEnable
     */
    public void setSmsEnable(Integer smsEnable) {
        this.smsEnable = smsEnable;
    }

    /**
     * @return app_enable
     */
    public Integer getAppEnable() {
        return appEnable;
    }

    /**
     * @param appEnable
     */
    public void setAppEnable(Integer appEnable) {
        this.appEnable = appEnable;
    }

    /**
     * @return wechat_title
     */
    public String getWechatTitle() {
        return wechatTitle;
    }

    /**
     * @param wechatTitle
     */
    public void setWechatTitle(String wechatTitle) {
        this.wechatTitle = wechatTitle;
    }

    /**
     * @return wechat_content
     */
    public String getWechatContent() {
        return wechatContent;
    }

    /**
     * @param wechatContent
     */
    public void setWechatContent(String wechatContent) {
        this.wechatContent = wechatContent;
    }

    /**
     * @return sms_content
     */
    public String getSmsContent() {
        return smsContent;
    }

    /**
     * @param smsContent
     */
    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public String getSmsSignName() {
        return smsSignName;
    }

    public void setSmsSignName(String smsSignName) {
        this.smsSignName = smsSignName;
    }

    /**
     * @return sms_template_code
     */
    public String getSmsTemplateCode() {
        return smsTemplateCode;
    }

    /**
     * @param smsTemplateCode
     */
    public void setSmsTemplateCode(String smsTemplateCode) {
        this.smsTemplateCode = smsTemplateCode;
    }

    /**
     * @return app_title
     */
    public String getAppTitle() {
        return appTitle;
    }

    /**
     * @param appTitle
     */
    public void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    /**
     * @return app_content
     */
    public String getAppContent() {
        return appContent;
    }

    /**
     * @param appContent
     */
    public void setAppContent(String appContent) {
        this.appContent = appContent;
    }

    /**
     * @return app_intent
     */
    public String getAppIntent() {
        return appIntent;
    }

    /**
     * @param appIntent
     */
    public void setAppIntent(String appIntent) {
        this.appIntent = appIntent;
    }

    public String getAppUriAction() {
        return appUriAction;
    }

    public void setAppUriAction(String appUriAction) {
        this.appUriAction = appUriAction;
    }

    public String getAppUriActivity() {
        return appUriActivity;
    }

    public void setAppUriActivity(String appUriActivity) {
        this.appUriActivity = appUriActivity;
    }

    public String getAppExtrasJson() {
        return appExtrasJson;
    }

    public void setAppExtrasJson(String appExtrasJson) {
        this.appExtrasJson = appExtrasJson;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "JpushSettings{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", type=" + type +
            ", wechatEnable=" + wechatEnable +
            ", smsEnable=" + smsEnable +
            ", appEnable=" + appEnable +
            ", wechatTitle='" + wechatTitle + '\'' +
            ", wechatContent='" + wechatContent + '\'' +
            ", smsContent='" + smsContent + '\'' +
            ", smsSignName='" + smsSignName + '\'' +
            ", smsTemplateCode='" + smsTemplateCode + '\'' +
            ", appTitle='" + appTitle + '\'' +
            ", appContent='" + appContent + '\'' +
            ", appIntent='" + appIntent + '\'' +
            ", appUriAction='" + appUriAction + '\'' +
            ", appUriActivity='" + appUriActivity + '\'' +
            ", appExtrasJson='" + appExtrasJson + '\'' +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            '}';
    }
}