package com.alipay.rebate.shop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;

public class PageModule {
    private Long moduleId;

    private String title;

    private String illustration;

    private Date createTime;

    @JsonIgnore
    private String json;

    private Integer moduleOrder;

    private Long pageId;

    private Integer type;

    private List<ModuleJson> moduleJson;


    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getIllustration() {
        return illustration;
    }

    public void setIllustration(String illustration) {
        this.illustration = illustration == null ? null : illustration.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json == null ? null : json.trim();
    }

    public Integer getModuleOrder() {
        return moduleOrder;
    }

    public void setModuleOrder(Integer moduleOrder) {
        this.moduleOrder = moduleOrder;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<ModuleJson> getModuleJson() {
        return moduleJson;
    }

    public void setModuleJson(List<ModuleJson> moduleJson) {
        this.moduleJson = moduleJson;
    }

    @Override
    public String toString() {
        return "PageModule{" +
            "moduleId=" + moduleId +
            ", title='" + title + '\'' +
            ", illustration='" + illustration + '\'' +
            ", createTime=" + createTime +
            ", code='" + json + '\'' +
            ", moduleOrder=" + moduleOrder +
            ", pageId=" + pageId +
            ", type=" + type +
            '}';
    }
}