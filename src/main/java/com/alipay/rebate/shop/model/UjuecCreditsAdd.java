package com.alipay.rebate.shop.model;

import javax.persistence.*;

@Table(name = "ujuec_credits_add")
public class UjuecCreditsAdd {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String phone;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
}