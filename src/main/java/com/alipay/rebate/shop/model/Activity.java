package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

public class Activity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    private String title;

    @Column(name = "backgroud_pic")
    private String backgroudPic;

    @Column(name = "top_pic")
    private String topPic;

    @Column(name = "bottom_pic")
    private String bottomPic;

    @Column(name = "begig_time")
    private Date begigTime;

    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "create_time")
    private Date createTime;

    private String remark;

    @Column(name = "desc_html")
    private String descHtml;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return backgroud_pic
     */
    public String getBackgroudPic() {
        return backgroudPic;
    }

    /**
     * @param backgroudPic
     */
    public void setBackgroudPic(String backgroudPic) {
        this.backgroudPic = backgroudPic;
    }

    /**
     * @return top_pic
     */
    public String getTopPic() {
        return topPic;
    }

    /**
     * @param topPic
     */
    public void setTopPic(String topPic) {
        this.topPic = topPic;
    }

    /**
     * @return bottom_pic
     */
    public String getBottomPic() {
        return bottomPic;
    }

    /**
     * @param bottomPic
     */
    public void setBottomPic(String bottomPic) {
        this.bottomPic = bottomPic;
    }

    /**
     * @return begig_time
     */
    public Date getBegigTime() {
        return begigTime;
    }

    /**
     * @param begigTime
     */
    public void setBegigTime(Date begigTime) {
        this.begigTime = begigTime;
    }

    /**
     * @return end_time
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return desc_html
     */
    public String getDescHtml() {
        return descHtml;
    }

    /**
     * @param descHtml
     */
    public void setDescHtml(String descHtml) {
        this.descHtml = descHtml;
    }
}