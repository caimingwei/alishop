package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

public class Extra {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String comefromparent;

    @Column(name = "the_same_day")
    private Date theSameDay;

    @Column(name = "one_day_count")
    private Long oneDayCount;

    private String remark;

    private String comefromchlidren;

    private String pri;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "udpate_time")
    private Date udpateTime;

    private String version;

    private String htmlName;

    public String getHtmlName() {
        return htmlName;
    }

    public void setHtmlName(String htmlName) {
        this.htmlName = htmlName;
    }

    public String getComefromchlidren() {
        return comefromchlidren;
    }

    public void setComefromchlidren(String comefromchlidren) {
        this.comefromchlidren = comefromchlidren;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComefromparent() {
        return comefromparent;
    }

    public void setComefromparent(String comefromparent) {
        this.comefromparent = comefromparent;
    }

    public Date getTheSameDay() {
        return theSameDay;
    }

    public void setTheSameDay(Date theSameDay) {
        this.theSameDay = theSameDay;
    }

    public Long getOneDayCount() {
        return oneDayCount;
    }

    public void setOneDayCount(Long oneDayCount) {
        this.oneDayCount = oneDayCount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPri() {
        return pri;
    }

    public void setPri(String pri) {
        this.pri = pri;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUdpateTime() {
        return udpateTime;
    }

    public void setUdpateTime(Date udpateTime) {
        this.udpateTime = udpateTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Extra{" +
                "id=" + id +
                ", comefromparent='" + comefromparent + '\'' +
                ", theSameDay=" + theSameDay +
                ", oneDayCount=" + oneDayCount +
                ", remark='" + remark + '\'' +
                ", comefromchlidren='" + comefromchlidren + '\'' +
                ", pri='" + pri + '\'' +
                ", createTime=" + createTime +
                ", udpateTime=" + udpateTime +
                ", version='" + version + '\'' +
                ", htmlName='" + htmlName + '\'' +
                '}';
    }
}