package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "cust_product")
public class CustProduct {
    /**
     * 商品库名称
     */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    /**
     * 分类
     */
    private String name;

    /**
     * 分类
     */
    private String classify;

    /**
     * 筛选，商品库来源 0：无限 1：好单库 2：大淘客
     */
    @Column(name = "product_from")
    private Integer productFrom;

    /**
     * 价格开始区间
     */
    @Column(name = "price_begin")
    private Double priceBegin;

    /**
     * 价格结束区间
     */
    @Column(name = "price_end")
    private Double priceEnd;

    /**
     * 月销量
     */
    @Column(name = "month_sale")
    private Integer monthSale;

    /**
     * 佣金比例开始区间
     */
    @Column(name = "commission_begin")
    private Integer commissionBegin;

    /**
     * 佣金比例结束区间
     */
    @Column(name = "commission_end")
    private Integer commissionEnd;

    /**
     * 关键字过滤
     */
    @Column(name = "key_word")
    private String keyWord;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取商品库名称
     *
     * @return id - 商品库名称
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置商品库名称
     *
     * @param id 商品库名称
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取分类
     *
     * @return name - 分类
     */
    public String getName() {
        return name;
    }

    /**
     * 设置分类
     *
     * @param name 分类
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取分类
     *
     * @return classify - 分类
     */
    public String getClassify() {
        return classify;
    }

    /**
     * 设置分类
     *
     * @param classify 分类
     */
    public void setClassify(String classify) {
        this.classify = classify;
    }

    /**
     * 获取筛选，商品库来源 0：无限 1：好单库 2：大淘客
     *
     * @return product_from - 筛选，商品库来源 0：无限 1：好单库 2：大淘客
     */
    public Integer getProductFrom() {
        return productFrom;
    }

    /**
     * 设置筛选，商品库来源 0：无限 1：好单库 2：大淘客
     *
     * @param productFrom 筛选，商品库来源 0：无限 1：好单库 2：大淘客
     */
    public void setProductFrom(Integer productFrom) {
        this.productFrom = productFrom;
    }

    /**
     * 获取价格开始区间
     *
     * @return price_begin - 价格开始区间
     */
    public Double getPriceBegin() {
        return priceBegin;
    }

    /**
     * 设置价格开始区间
     *
     * @param priceBegin 价格开始区间
     */
    public void setPriceBegin(Double priceBegin) {
        this.priceBegin = priceBegin;
    }

    /**
     * 获取价格结束区间
     *
     * @return price_end - 价格结束区间
     */
    public Double getPriceEnd() {
        return priceEnd;
    }

    /**
     * 设置价格结束区间
     *
     * @param priceEnd 价格结束区间
     */
    public void setPriceEnd(Double priceEnd) {
        this.priceEnd = priceEnd;
    }

    /**
     * 获取月销量
     *
     * @return month_sale - 月销量
     */
    public Integer getMonthSale() {
        return monthSale;
    }

    /**
     * 设置月销量
     *
     * @param monthSale 月销量
     */
    public void setMonthSale(Integer monthSale) {
        this.monthSale = monthSale;
    }

    /**
     * 获取佣金比例开始区间
     *
     * @return commission_begin - 佣金比例开始区间
     */
    public Integer getCommissionBegin() {
        return commissionBegin;
    }

    /**
     * 设置佣金比例开始区间
     *
     * @param commissionBegin 佣金比例开始区间
     */
    public void setCommissionBegin(Integer commissionBegin) {
        this.commissionBegin = commissionBegin;
    }

    /**
     * 获取佣金比例结束区间
     *
     * @return commission_end - 佣金比例结束区间
     */
    public Integer getCommissionEnd() {
        return commissionEnd;
    }

    /**
     * 设置佣金比例结束区间
     *
     * @param commissionEnd 佣金比例结束区间
     */
    public void setCommissionEnd(Integer commissionEnd) {
        this.commissionEnd = commissionEnd;
    }

    /**
     * 获取关键字过滤
     *
     * @return key_word - 关键字过滤
     */
    public String getKeyWord() {
        return keyWord;
    }

    /**
     * 设置关键字过滤
     *
     * @param keyWord 关键字过滤
     */
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}