package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "invite_settings")
public class InviteSettings {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    /**
     * 拉新海报图片地址
     */
    @Column(name = "bill_pic_url")
    private String billPicUrl;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 奖励梯度的json设置
     */
    @Column(name = "award_grad_json")
    private String awardGradJson;

    /**
     * 规则富文本
     */
    @Column(name = "rule_html")
    private String ruleHtml;

    /**
     * 条件设置
     */
    @Column(name = "condition_json")
    private String conditionJson;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取拉新海报图片地址
     *
     * @return bill_pic_url - 拉新海报图片地址
     */
    public String getBillPicUrl() {
        return billPicUrl;
    }

    /**
     * 设置拉新海报图片地址
     *
     * @param billPicUrl 拉新海报图片地址
     */
    public void setBillPicUrl(String billPicUrl) {
        this.billPicUrl = billPicUrl;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取奖励梯度的json设置
     *
     * @return award_grad_json - 奖励梯度的json设置
     */
    public String getAwardGradJson() {
        return awardGradJson;
    }

    /**
     * 设置奖励梯度的json设置
     *
     * @param awardGradJson 奖励梯度的json设置
     */
    public void setAwardGradJson(String awardGradJson) {
        this.awardGradJson = awardGradJson;
    }

    /**
     * 获取规则富文本
     *
     * @return rule_html - 规则富文本
     */
    public String getRuleHtml() {
        return ruleHtml;
    }

    /**
     * 设置规则富文本
     *
     * @param ruleHtml 规则富文本
     */
    public void setRuleHtml(String ruleHtml) {
        this.ruleHtml = ruleHtml;
    }

    /**
     * 获取条件设置
     *
     * @return condition_json - 条件设置
     */
    public String getConditionJson() {
        return conditionJson;
    }

    /**
     * 设置条件设置
     *
     * @param conditionJson 条件设置
     */
    public void setConditionJson(String conditionJson) {
        this.conditionJson = conditionJson;
    }
}