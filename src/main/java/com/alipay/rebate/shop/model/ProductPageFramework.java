package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "product_page_framework")
public class ProductPageFramework {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_page_id")
    @Id
    private Integer productPageId;

    /**
     * 板块类别 1：普通板块
     */
    @Column(name = "page_catagory")
    private Integer pageCatagory;

    /**
     * 板块名称
     */
    @Column(name = "page_name")
    private String pageName;

    /**
     * 分类展示 0：否  1：是
     */
    @Column(name = "classify_show")
    private Integer classifyShow;

    /**
     * 列表展示
     */
    @Column(name = "show_type")
    private Integer showType;

    /**
     * 显示返利 0：否  1：是
     */
    @Column(name = "show_rebate")
    private Integer showRebate;

    /**
     * 板块banner的图片地址
     */
    @Column(name = "banner_pic_path")
    private String bannerPicPath;

    /**
     * 板块是否启用
     */
    @Column(name = "is_use")
    private Integer isUse;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 数据来源，此处存放数据来源的json字符串设置
     */
    @Column(name = "data_source")
    private String dataSource;

    /**
     * @return product_page_id
     */
    public Integer getProductPageId() {
        return productPageId;
    }

    /**
     * @param productPageId
     */
    public void setProductPageId(Integer productPageId) {
        this.productPageId = productPageId;
    }

    /**
     * 获取板块类别 1：普通板块
     *
     * @return page_catagory - 板块类别 1：普通板块
     */
    public Integer getPageCatagory() {
        return pageCatagory;
    }

    /**
     * 设置板块类别 1：普通板块
     *
     * @param pageCatagory 板块类别 1：普通板块
     */
    public void setPageCatagory(Integer pageCatagory) {
        this.pageCatagory = pageCatagory;
    }

    /**
     * 获取板块名称
     *
     * @return page_name - 板块名称
     */
    public String getPageName() {
        return pageName;
    }

    /**
     * 设置板块名称
     *
     * @param pageName 板块名称
     */
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    /**
     * 获取分类展示 0：否  1：是
     *
     * @return classify_show - 分类展示 0：否  1：是
     */
    public Integer getClassifyShow() {
        return classifyShow;
    }

    /**
     * 设置分类展示 0：否  1：是
     *
     * @param classifyShow 分类展示 0：否  1：是
     */
    public void setClassifyShow(Integer classifyShow) {
        this.classifyShow = classifyShow;
    }

    /**
     * 获取列表展示
     *
     * @return show_type - 列表展示
     */
    public Integer getShowType() {
        return showType;
    }

    /**
     * 设置列表展示
     *
     * @param showType 列表展示
     */
    public void setShowType(Integer showType) {
        this.showType = showType;
    }

    /**
     * 获取显示返利 0：否  1：是
     *
     * @return show_rebate - 显示返利 0：否  1：是
     */
    public Integer getShowRebate() {
        return showRebate;
    }

    /**
     * 设置显示返利 0：否  1：是
     *
     * @param showRebate 显示返利 0：否  1：是
     */
    public void setShowRebate(Integer showRebate) {
        this.showRebate = showRebate;
    }

    /**
     * 获取板块banner的图片地址
     *
     * @return banner_pic_path - 板块banner的图片地址
     */
    public String getBannerPicPath() {
        return bannerPicPath;
    }

    /**
     * 设置板块banner的图片地址
     *
     * @param bannerPicPath 板块banner的图片地址
     */
    public void setBannerPicPath(String bannerPicPath) {
        this.bannerPicPath = bannerPicPath;
    }

    /**
     * 获取板块是否启用
     *
     * @return is_use - 板块是否启用
     */
    public Integer getIsUse() {
        return isUse;
    }

    /**
     * 设置板块是否启用
     *
     * @param isUse 板块是否启用
     */
    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取数据来源，此处存放数据来源的json字符串设置
     *
     * @return data_source - 数据来源，此处存放数据来源的json字符串设置
     */
    public String getDataSource() {
        return dataSource;
    }

    /**
     * 设置数据来源，此处存放数据来源的json字符串设置
     *
     * @param dataSource 数据来源，此处存放数据来源的json字符串设置
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public String toString() {
        return "ProductPageFramework{" +
            "productPageId=" + productPageId +
            ", pageCatagory=" + pageCatagory +
            ", pageName='" + pageName + '\'' +
            ", classifyShow=" + classifyShow +
            ", showType=" + showType +
            ", showRebate=" + showRebate +
            ", bannerPicPath='" + bannerPicPath + '\'' +
            ", isUse=" + isUse +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", dataSource='" + dataSource + '\'' +
            '}';
    }
}