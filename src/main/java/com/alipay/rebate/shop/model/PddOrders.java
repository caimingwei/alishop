package com.alipay.rebate.shop.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "pdd_orders")
public class PddOrders {

    private String orderId;

    /**
     * 商品ID
     */
    @Column(name = "goods_id")
    private Long goodsId;

    /**
     * 商品标题
     */
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 订单中sku的单件价格，单位为分
     */
    @Column(name = "goods_price")
    private BigDecimal goodsPrice;

    /**
     * 购买商品的数量
     */
    @Column(name = "goods_quantity")
    private Long goodsQuantity;

    /**
     * 商品缩略图
     */
    @Column(name = "goods_thumbnail_url")
    private String goodsThumbnailUrl;

    /**
     * 实际支付金额，单位为分
     */
    @Column(name = "order_amount")
    private BigDecimal orderAmount;

    /**
     * 订单生成时间
     */
    @Column(name = "order_create_time")
    private String orderCreateTime;

    /**
     * 成团时间
     */
    @Column(name = "order_group_success_time")
    private String orderGroupSuccessTime;

    /**
     * 最后更新时间
     */
    @Column(name = "order_modify_at")
    private String orderModifyAt;

    /**
     * 支付时间
     */
    @Column(name = "order_pay_time")
    private String orderPayTime;

    private String orderSn;

    /**
     * 订单状态： -1 未支付; 0-已支付；1-已成团；2-确认收货；3-审核成功；4-审核失败（不可提现）；5-已经结算；8-非多多进宝商品（无佣金订单）
     */
    @Column(name = "order_status")
    private Integer orderStatus;

    /**
     * 审核时间
     */
    @Column(name = "order_verify_time")
    private String orderVerifyTime;

    /**
     * 订单状态描述
     */
    @Column(name = "order_status_desc")
    private String orderStatusDesc;

    /**
     * 推广位ID
     */
    @Column(name = "p_id")
    private String pId;

    /**
     * 佣金金额，单位为分
     */
    @Column(name = "promotion_amount")
    private BigDecimal promotionAmount;

    /**
     * 佣金比例，千分比
     */
    @Column(name = "promotion_rate")
    private Long promotionRate;

    /**
     * 是否是 cpa 新用户，1表示是，0表示否
     */
    @Column(name = "cpa_new")
    private Integer cpaNew;

    private Long userUit;

    private Long oneLevelUserUit;

    private Long twoLevelUserUit;

    private BigDecimal userCommission;

    private BigDecimal oneLevelCommission;

    private BigDecimal twoLevelCommission;

    private Long userId;

    private String userName;

    private Integer isRecord;

    private String customParameters;

    private Integer activityId;

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getCustomParameters() {
        return customParameters;
    }

    public void setCustomParameters(String customParameters) {
        this.customParameters = customParameters;
    }

    public Integer getIsRecord() {
        return isRecord;
    }

    public void setIsRecord(Integer isRecord) {
        this.isRecord = isRecord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getUserCommission() {
        return userCommission;
    }

    public void setUserCommission(BigDecimal userCommission) {
        this.userCommission = userCommission;
    }

    public BigDecimal getOneLevelCommission() {
        return oneLevelCommission;
    }

    public void setOneLevelCommission(BigDecimal oneLevelCommission) {
        this.oneLevelCommission = oneLevelCommission;
    }

    public BigDecimal getTwoLevelCommission() {
        return twoLevelCommission;
    }

    public void setTwoLevelCommission(BigDecimal twoLevelCommission) {
        this.twoLevelCommission = twoLevelCommission;
    }

    public Long getUserUit() {
        return userUit;
    }

    public void setUserUit(Long userUit) {
        this.userUit = userUit;
    }

    public Long getOneLevelUserUit() {
        return oneLevelUserUit;
    }

    public void setOneLevelUserUit(Long oneLevelUserUit) {
        this.oneLevelUserUit = oneLevelUserUit;
    }

    public Long getTwoLevelUserUit() {
        return twoLevelUserUit;
    }

    public void setTwoLevelUserUit(Long twoLevelUserUit) {
        this.twoLevelUserUit = twoLevelUserUit;
    }

    /**
     * 获取商品ID
     *
     * @return goods_id - 商品ID
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 设置商品ID
     *
     * @param goodsId 商品ID
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取商品标题
     *
     * @return goods_name - 商品标题
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * 设置商品标题
     *
     * @param goodsName 商品标题
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 获取订单中sku的单件价格，单位为分
     *
     * @return goods_price - 订单中sku的单件价格，单位为分
     */
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 设置订单中sku的单件价格，单位为分
     *
     * @param goodsPrice 订单中sku的单件价格，单位为分
     */
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 获取购买商品的数量
     *
     * @return goods_quantity - 购买商品的数量
     */
    public Long getGoodsQuantity() {
        return goodsQuantity;
    }

    /**
     * 设置购买商品的数量
     *
     * @param goodsQuantity 购买商品的数量
     */
    public void setGoodsQuantity(Long goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }

    /**
     * 获取商品缩略图
     *
     * @return goods_thumbnail_url - 商品缩略图
     */
    public String getGoodsThumbnailUrl() {
        return goodsThumbnailUrl;
    }

    /**
     * 设置商品缩略图
     *
     * @param goodsThumbnailUrl 商品缩略图
     */
    public void setGoodsThumbnailUrl(String goodsThumbnailUrl) {
        this.goodsThumbnailUrl = goodsThumbnailUrl;
    }

    /**
     * 获取实际支付金额，单位为分
     *
     * @return order_amount - 实际支付金额，单位为分
     */
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    /**
     * 设置实际支付金额，单位为分
     *
     * @param orderAmount 实际支付金额，单位为分
     */
    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    /**
     * 获取订单生成时间
     *
     * @return order_create_time - 订单生成时间
     */
    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    /**
     * 设置订单生成时间
     *
     * @param orderCreateTime 订单生成时间
     */
    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    /**
     * 获取成团时间
     *
     * @return order_group_success_time - 成团时间
     */
    public String getOrderGroupSuccessTime() {
        return orderGroupSuccessTime;
    }

    /**
     * 设置成团时间
     *
     * @param orderGroupSuccessTime 成团时间
     */
    public void setOrderGroupSuccessTime(String orderGroupSuccessTime) {
        this.orderGroupSuccessTime = orderGroupSuccessTime;
    }

    /**
     * 获取最后更新时间
     *
     * @return order_modify_at - 最后更新时间
     */
    public String getOrderModifyAt() {
        return orderModifyAt;
    }

    /**
     * 设置最后更新时间
     *
     * @param orderModifyAt 最后更新时间
     */
    public void setOrderModifyAt(String orderModifyAt) {
        this.orderModifyAt = orderModifyAt;
    }

    /**
     * 获取支付时间
     *
     * @return order_pay_time - 支付时间
     */
    public String getOrderPayTime() {
        return orderPayTime;
    }

    /**
     * 设置支付时间
     *
     * @param orderPayTime 支付时间
     */
    public void setOrderPayTime(String orderPayTime) {
        this.orderPayTime = orderPayTime;
    }

    /**
     * @return order_sn
     */
    public String getOrderSn() {
        return orderSn;
    }

    /**
     * @param orderSn
     */
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    /**
     * 获取订单状态： -1 未支付; 0-已支付；1-已成团；2-确认收货；3-审核成功；4-审核失败（不可提现）；5-已经结算；8-非多多进宝商品（无佣金订单）
     *
     * @return order_status - 订单状态： -1 未支付; 0-已支付；1-已成团；2-确认收货；3-审核成功；4-审核失败（不可提现）；5-已经结算；8-非多多进宝商品（无佣金订单）
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     * 设置订单状态： -1 未支付; 0-已支付；1-已成团；2-确认收货；3-审核成功；4-审核失败（不可提现）；5-已经结算；8-非多多进宝商品（无佣金订单）
     *
     * @param orderStatus 订单状态： -1 未支付; 0-已支付；1-已成团；2-确认收货；3-审核成功；4-审核失败（不可提现）；5-已经结算；8-非多多进宝商品（无佣金订单）
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 获取审核时间
     *
     * @return order_verify_time - 审核时间
     */
    public String getOrderVerifyTime() {
        return orderVerifyTime;
    }

    /**
     * 设置审核时间
     *
     * @param orderVerifyTime 审核时间
     */
    public void setOrderVerifyTime(String orderVerifyTime) {
        this.orderVerifyTime = orderVerifyTime;
    }

    /**
     * 获取订单状态描述
     *
     * @return order_status_desc - 订单状态描述
     */
    public String getOrderStatusDesc() {
        return orderStatusDesc;
    }

    /**
     * 设置订单状态描述
     *
     * @param orderStatusDesc 订单状态描述
     */
    public void setOrderStatusDesc(String orderStatusDesc) {
        this.orderStatusDesc = orderStatusDesc;
    }

    /**
     * 获取推广位ID
     *
     * @return p_id - 推广位ID
     */
    public String getpId() {
        return pId;
    }

    /**
     * 设置推广位ID
     *
     * @param pId 推广位ID
     */
    public void setpId(String pId) {
        this.pId = pId;
    }

    /**
     * 获取佣金金额，单位为分
     *
     * @return promotion_amount - 佣金金额，单位为分
     */
    public BigDecimal getPromotionAmount() {
        return promotionAmount;
    }

    /**
     * 设置佣金金额，单位为分
     *
     * @param promotionAmount 佣金金额，单位为分
     */
    public void setPromotionAmount(BigDecimal promotionAmount) {
        this.promotionAmount = promotionAmount;
    }

    /**
     * 获取佣金比例，千分比
     *
     * @return promotion_rate - 佣金比例，千分比
     */
    public Long getPromotionRate() {
        return promotionRate;
    }

    /**
     * 设置佣金比例，千分比
     *
     * @param promotionRate 佣金比例，千分比
     */
    public void setPromotionRate(Long promotionRate) {
        this.promotionRate = promotionRate;
    }

    /**
     * 获取是否是 cpa 新用户，1表示是，0表示否
     *
     * @return cpa_new - 是否是 cpa 新用户，1表示是，0表示否
     */
    public Integer getCpaNew() {
        return cpaNew;
    }

    /**
     * 设置是否是 cpa 新用户，1表示是，0表示否
     *
     * @param cpaNew 是否是 cpa 新用户，1表示是，0表示否
     */
    public void setCpaNew(Integer cpaNew) {
        this.cpaNew = cpaNew;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "PddOrders{" +
                "orderId='" + orderId + '\'' +
                ", goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", goodsPrice=" + goodsPrice +
                ", goodsQuantity=" + goodsQuantity +
                ", goodsThumbnailUrl='" + goodsThumbnailUrl + '\'' +
                ", orderAmount=" + orderAmount +
                ", orderCreateTime='" + orderCreateTime + '\'' +
                ", orderGroupSuccessTime='" + orderGroupSuccessTime + '\'' +
                ", orderModifyAt='" + orderModifyAt + '\'' +
                ", orderPayTime='" + orderPayTime + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", orderStatus=" + orderStatus +
                ", orderVerifyTime='" + orderVerifyTime + '\'' +
                ", orderStatusDesc='" + orderStatusDesc + '\'' +
                ", pId='" + pId + '\'' +
                ", promotionAmount=" + promotionAmount +
                ", promotionRate=" + promotionRate +
                ", cpaNew=" + cpaNew +
                ", userUit=" + userUit +
                ", oneLevelUserUit=" + oneLevelUserUit +
                ", twoLevelUserUit=" + twoLevelUserUit +
                ", userCommission=" + userCommission +
                ", oneLevelCommission=" + oneLevelCommission +
                ", twoLevelCommission=" + twoLevelCommission +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", isRecord=" + isRecord +
                ", customParameters='" + customParameters + '\'' +
                ", activityId=" + activityId +
                '}';
    }
}