package com.alipay.rebate.shop.model;

import javax.persistence.*;
import java.util.Date;

@Table(name = "entity_settings")
public class EntitySettings {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    private Integer type;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    private String json;

//    private String startColor;
//    private String endColor;
//
//    private String incomeStartColor;
//    private String incomeEndColor;

//    public String getIncomeStartColor() {
//        return incomeStartColor;
//    }
//
//    public void setIncomeStartColor(String incomeStartColor) {
//        this.incomeStartColor = incomeStartColor;
//    }
//
//    public String getIncomeEndColor() {
//        return incomeEndColor;
//    }
//
//    public void setIncomeEndColor(String incomeEndColor) {
//        this.incomeEndColor = incomeEndColor;
//    }
//
//    public String getStartColor() {
//        return startColor;
//    }
//
//    public void setStartColor(String startColor) {
//        this.startColor = startColor;
//    }
//
//    public String getEndColor() {
//        return endColor;
//    }
//
//    public void setEndColor(String endColor) {
//        this.endColor = endColor;
//    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return json
     */
    public String getJson() {
        return json;
    }

    /**
     * @param json
     */
    public void setJson(String json) {
        this.json = json;
    }

    @Override
    public String toString() {
        return "EntitySettings{" +
                "id=" + id +
                ", type=" + type +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", json='" + json + '\'' +
//                ", startColor='" + startColor + '\'' +
//                ", endColor='" + endColor + '\'' +
//                ", incomeStartColor='" + incomeStartColor + '\'' +
//                ", incomeEndColor='" + incomeEndColor + '\'' +
                '}';
    }
}