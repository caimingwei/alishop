package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "app_settings")
public class AppSettings {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "is_show_rebate")
    private Integer isShowRebate;

    @Column(name = "update_time")
    private Date updateTime;

    private String pid;

    private Integer iosAudit;

    private String appVersion;

    private String activityPid;

    private String taobaoAppKey;

    private String taobaoAppSecret;

    private String jpushKey;

    private String jpushSecret;

    private Integer spDomainType;

    private String spDomain;

    private String csWetchat;

    private String password;

    private String smsAccesskeyId;

    private String smsSecret;

    private Long unionId;

    private String jdKey;

    private Long siteId;

    private String haodankuKey;

    private Integer jdRebate;

    private Integer PddRebate;

    private String sparePid;

    private String dataokeAppSecret;

    private String dataokeAppKey;

    private Integer promoteEarn;

    private String appAlias;

    private String privacyPolicy;
    private String useAgreement;

    private String tbAuthId;
    private Integer androidAudit;
    private String androidAppVersion;

    private int taobaoAmImportTimeArea;

    public String getAndroidAppVersion() {
        return androidAppVersion;
    }

    public void setAndroidAppVersion(String androidAppVersion) {
        this.androidAppVersion = androidAppVersion;
    }

    public String getTbAuthId() {
        return tbAuthId;
    }

    public void setTbAuthId(String tbAuthId) {
        this.tbAuthId = tbAuthId;
    }

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public String getUseAgreement() {
        return useAgreement;
    }

    public void setUseAgreement(String useAgreement) {
        this.useAgreement = useAgreement;
    }

    public String getAppAlias() {
        return appAlias;
    }

    public void setAppAlias(String appAlias) {
        this.appAlias = appAlias;
    }

    public Integer getPromoteEarn() {
        return promoteEarn;
    }

    public void setPromoteEarn(Integer promoteEarn) {
        this.promoteEarn = promoteEarn;
    }

    public String getDataokeAppSecret() {
        return dataokeAppSecret;
    }

    public void setDataokeAppSecret(String dataokeAppSecret) {
        this.dataokeAppSecret = dataokeAppSecret;
    }

    public String getDataokeAppKey() {
        return dataokeAppKey;
    }

    public void setDataokeAppKey(String dataokeAppKey) {
        this.dataokeAppKey = dataokeAppKey;
    }

    public String getSparePid() {
        return sparePid;
    }

    public void setSparePid(String sparePid) {
        this.sparePid = sparePid;
    }

    public Integer getPddRebate() {
        return PddRebate;
    }

    public void setPddRebate(Integer pddRebate) {
        PddRebate = pddRebate;
    }

    public Integer getJdRebate() {
        return jdRebate;
    }

    public void setJdRebate(Integer jdRebate) {
        this.jdRebate = jdRebate;
    }

    public String getHaodankuKey() {
        return haodankuKey;
    }

    public void setHaodankuKey(String haodankuKey) {
        this.haodankuKey = haodankuKey;
    }

    public Long getUnionId() {
        return unionId;
    }

    public void setUnionId(Long unionId) {
        this.unionId = unionId;
    }

    public String getJdKey() {
        return jdKey;
    }

    public void setJdKey(String jdKey) {
        this.jdKey = jdKey;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getSmsAccesskeyId() {
        return smsAccesskeyId;
    }

    public void setSmsAccesskeyId(String smsAccesskeyId) {
        this.smsAccesskeyId = smsAccesskeyId;
    }

    public String getSmsSecret() {
        return smsSecret;
    }

    public void setSmsSecret(String smsSecret) {
        this.smsSecret = smsSecret;
    }

    public Integer getIosAudit() {
        return iosAudit;
    }

    public void setIosAudit(Integer iosAudit) {
        this.iosAudit = iosAudit;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return is_show_rebate
     */
    public Integer getIsShowRebate() {
        return isShowRebate;
    }

    /**
     * @param isShowRebate
     */
    public void setIsShowRebate(Integer isShowRebate) {
        this.isShowRebate = isShowRebate;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }

    public String getTaobaoAppKey() {
        return taobaoAppKey;
    }

    public void setTaobaoAppKey(String taobaoAppKey) {
        this.taobaoAppKey = taobaoAppKey;
    }

    public String getTaobaoAppSecret() {
        return taobaoAppSecret;
    }

    public void setTaobaoAppSecret(String taobaoAppSecret) {
        this.taobaoAppSecret = taobaoAppSecret;
    }

    public String getJpushKey() {
        return jpushKey;
    }

    public void setJpushKey(String jpushKey) {
        this.jpushKey = jpushKey;
    }

    public String getJpushSecret() {
        return jpushSecret;
    }

    public void setJpushSecret(String jpushSecret) {
        this.jpushSecret = jpushSecret;
    }

    public Integer getSpDomainType() {
        return spDomainType;
    }

    public void setSpDomainType(Integer spDomainType) {
        this.spDomainType = spDomainType;
    }

    public String getSpDomain() {
        return spDomain;
    }

    public void setSpDomain(String spDomain) {
        this.spDomain = spDomain;
    }

    public String getCsWetchat() {
        return csWetchat;
    }

    public void setCsWetchat(String csWetchat) {
        this.csWetchat = csWetchat;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAndroidAudit() {
        return androidAudit;
    }

    public void setAndroidAudit(Integer androidAudit) {
        this.androidAudit = androidAudit;
    }

    public int getTaobaoAmImportTimeArea() {
        return taobaoAmImportTimeArea;
    }

    public void setTaobaoAmImportTimeArea(int taobaoAmImportTimeArea) {
        this.taobaoAmImportTimeArea = taobaoAmImportTimeArea;
    }

    @Override
    public String toString() {
        return "AppSettings{" +
                "id=" + id +
                ", isShowRebate=" + isShowRebate +
                ", updateTime=" + updateTime +
                ", pid='" + pid + '\'' +
                ", iosAudit=" + iosAudit +
                ", appVersion='" + appVersion + '\'' +
                ", activityPid='" + activityPid + '\'' +
                ", taobaoAppKey='" + taobaoAppKey + '\'' +
                ", taobaoAppSecret='" + taobaoAppSecret + '\'' +
                ", jpushKey='" + jpushKey + '\'' +
                ", jpushSecret='" + jpushSecret + '\'' +
                ", spDomainType=" + spDomainType +
                ", spDomain='" + spDomain + '\'' +
                ", csWetchat='" + csWetchat + '\'' +
                ", password='" + password + '\'' +
                ", smsAccesskeyId='" + smsAccesskeyId + '\'' +
                ", smsSecret='" + smsSecret + '\'' +
                ", unionId=" + unionId +
                ", jdKey='" + jdKey + '\'' +
                ", siteId=" + siteId +
                ", haodankuKey='" + haodankuKey + '\'' +
                ", jdRebate=" + jdRebate +
                ", PddRebate=" + PddRebate +
                ", sparePid='" + sparePid + '\'' +
                ", dataokeAppSecret='" + dataokeAppSecret + '\'' +
                ", dataokeAppKey='" + dataokeAppKey + '\'' +
                ", promoteEarn=" + promoteEarn +
                ", appAlias='" + appAlias + '\'' +
                ", privacyPolicy='" + privacyPolicy + '\'' +
                ", useAgreement='" + useAgreement + '\'' +
                ", tbAuthId='" + tbAuthId + '\'' +
                ", androidAudit=" + androidAudit +
                ", androidAppVersion='" + androidAppVersion + '\'' +
                '}';
    }

}