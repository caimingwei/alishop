package com.alipay.rebate.shop.model;

import javax.persistence.*;

@Table(name = "income_commission_freeze_level")
public class IncomeCommissionFreezeLevel {
    @Column(name = "user_income_id")
    @Id
    private Long userIncomeId;

    @Column(name = "freeze_level_id")
    private Integer freezeLevelId;

    /**
     * @return user_income_id
     */
    public Long getUserIncomeId() {
        return userIncomeId;
    }

    /**
     * @param userIncomeId
     */
    public void setUserIncomeId(Long userIncomeId) {
        this.userIncomeId = userIncomeId;
    }

    /**
     * @return freeze_level_id
     */
    public Integer getFreezeLevelId() {
        return freezeLevelId;
    }

    /**
     * @param freezeLevelId
     */
    public void setFreezeLevelId(Integer freezeLevelId) {
        this.freezeLevelId = freezeLevelId;
    }
}