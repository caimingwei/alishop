package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ActivityBrowseProduct {
    private Long id;

    private Long userId;

    @NotNull(message = "商品id不能为空")
    private Long goodsId;

    @NotNull(message = "活动id不能为空")
    private Integer activityId;

    private Integer productRepoId;

    private Date clickTime;

    @NotNull(message = "条件json不能为空")
    @NotEmpty(message = "条件json不能为空")
    private String json;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getProductRepoId() {
        return productRepoId;
    }

    public void setProductRepoId(Integer productRepoId) {
        this.productRepoId = productRepoId;
    }

    public Date getClickTime() {
        return clickTime;
    }

    public void setClickTime(Date clickTime) {
        this.clickTime = clickTime;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json == null ? null : json.trim();
    }
}