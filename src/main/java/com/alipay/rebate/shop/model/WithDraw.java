package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;

public class WithDraw {
    private Long withdrawId;

    private Long userId;

    private String userName;

    private Date applyTime;

    private Integer platform;

    private String account;

    private BigDecimal money;

    private Date commitTime;

    private Date handleTime;

    private String realName;

    private String phone;

    private Integer status;

    private Integer type;

    private Integer isFirst;

    private String refuseReason;

    public Long getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(Long withdrawId) {
        this.withdrawId = withdrawId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(Date commitTime) {
        this.commitTime = commitTime;
    }

    public Date getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(Integer isFirst) {
        this.isFirst = isFirst;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason;
    }

    @Override
    public String toString() {
        return "WithDraw{" +
            "withdrawId=" + withdrawId +
            ", userId=" + userId +
            ", userName='" + userName + '\'' +
            ", applyTime=" + applyTime +
            ", platform=" + platform +
            ", account='" + account + '\'' +
            ", money=" + money +
            ", commitTime=" + commitTime +
            ", handleTime=" + handleTime +
            ", realName='" + realName + '\'' +
            ", phone='" + phone + '\'' +
            ", status=" + status +
            ", type=" + type +
            ", isFirst=" + isFirst +
            ", refuseReason='" + refuseReason + '\'' +
            '}';
    }
}