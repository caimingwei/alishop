package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "sign_in_settings")
public class SignInSettings {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    /**
     * 签到玩法 1:七天累积玩法
     */
    @Column(name = "si_rule")
    private Integer siRule;

    /**
     * 签到奖励形式 1：固定式 2：随机式
     */
    @Column(name = "award_form")
    private Integer awardForm;

    /**
     * 固定式签到获得奖励
     */
    @Column(name = "fix_award_num")
    private Integer fixAwardNum;

    /**
     * 随机式获得最低奖励
     */
    @Column(name = "min_award_num")
    private Integer minAwardNum;

    /**
     * 随机式获得最高奖励
     */
    @Column(name = "max_award_num")
    private Integer maxAwardNum;

    /**
     * 奖励类型 1：积分 2：集分宝 3：积分
     */
    @Column(name = "award_type")
    private Integer awardType;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "rule_desc")
    private String ruleDesc;

    public String getRuleDesc() {
        return ruleDesc;
    }

    public void setRuleDesc(String ruleDesc) {
        this.ruleDesc = ruleDesc;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取签到玩法 1:七天累积玩法
     *
     * @return si_rule - 签到玩法 1:七天累积玩法
     */
    public Integer getSiRule() {
        return siRule;
    }

    /**
     * 设置签到玩法 1:七天累积玩法
     *
     * @param siRule 签到玩法 1:七天累积玩法
     */
    public void setSiRule(Integer siRule) {
        this.siRule = siRule;
    }

    /**
     * 获取签到奖励形式 1：固定式 2：随机式
     *
     * @return award_form - 签到奖励形式 1：固定式 2：随机式
     */
    public Integer getAwardForm() {
        return awardForm;
    }

    /**
     * 设置签到奖励形式 1：固定式 2：随机式
     *
     * @param awardForm 签到奖励形式 1：固定式 2：随机式
     */
    public void setAwardForm(Integer awardForm) {
        this.awardForm = awardForm;
    }

    /**
     * 获取固定式签到获得奖励
     *
     * @return fix_award_num - 固定式签到获得奖励
     */
    public Integer getFixAwardNum() {
        return fixAwardNum;
    }

    /**
     * 设置固定式签到获得奖励
     *
     * @param fixAwardNum 固定式签到获得奖励
     */
    public void setFixAwardNum(Integer fixAwardNum) {
        this.fixAwardNum = fixAwardNum;
    }

    /**
     * 获取随机式获得最低奖励
     *
     * @return min_award_num - 随机式获得最低奖励
     */
    public Integer getMinAwardNum() {
        return minAwardNum;
    }

    /**
     * 设置随机式获得最低奖励
     *
     * @param minAwardNum 随机式获得最低奖励
     */
    public void setMinAwardNum(Integer minAwardNum) {
        this.minAwardNum = minAwardNum;
    }

    /**
     * 获取随机式获得最高奖励
     *
     * @return max_award_num - 随机式获得最高奖励
     */
    public Integer getMaxAwardNum() {
        return maxAwardNum;
    }

    /**
     * 设置随机式获得最高奖励
     *
     * @param maxAwardNum 随机式获得最高奖励
     */
    public void setMaxAwardNum(Integer maxAwardNum) {
        this.maxAwardNum = maxAwardNum;
    }

    /**
     * 获取奖励类型 1：积分 2：集分宝
     *
     * @return award_type - 奖励类型 1：积分 2：集分宝
     */
    public Integer getAwardType() {
        return awardType;
    }

    /**
     * 设置奖励类型 1：积分 2：集分宝
     *
     * @param awardType 奖励类型 1：积分 2：集分宝
     */
    public void setAwardType(Integer awardType) {
        this.awardType = awardType;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}