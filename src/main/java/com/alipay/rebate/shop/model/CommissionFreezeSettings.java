package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "commission_freeze_settings")
public class CommissionFreezeSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private BigDecimal money;

    @Column(name = "freeze_day")
    private Integer freezeDay;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return money
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * @param money
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * @return freeze_day
     */
    public Integer getFreezeDay() {
        return freezeDay;
    }

    /**
     * @param freezeDay
     */
    public void setFreezeDay(Integer freezeDay) {
        this.freezeDay = freezeDay;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}