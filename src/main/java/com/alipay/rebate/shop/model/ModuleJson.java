package com.alipay.rebate.shop.model;

public class ModuleJson {

  private Long moduleId;
  private String title;
  private String illustration;
  private Integer order;
  private String imgUrl;
  private String redirectPage;

  public Long getModuleId() {
    return moduleId;
  }

  public void setModuleId(Long moduleId) {
    this.moduleId = moduleId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIllustration() {
    return illustration;
  }

  public void setIllustration(String illustration) {
    this.illustration = illustration;
  }

  public Integer getOrder() {
    return order;
  }

  public void setOrder(Integer order) {
    this.order = order;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getRedirectPage() {
    return redirectPage;
  }

  public void setRedirectPage(String redirectPage) {
    this.redirectPage = redirectPage;
  }

  @Override
  public String toString() {
    return "ModuleJson{" +
        "moduleId=" + moduleId +
        ", title='" + title + '\'' +
        ", illustration='" + illustration + '\'' +
        ", order=" + order +
        ", imgUrl='" + imgUrl + '\'' +
        ", redirectPage='" + redirectPage + '\'' +
        '}';
  }
}
