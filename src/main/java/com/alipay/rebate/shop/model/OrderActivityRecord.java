package com.alipay.rebate.shop.model;

public class OrderActivityRecord {
    private Integer activityId;

    private Long userId;

    private String tradeId;

    private Integer activityProductId;

    private Integer qualType;

    private String createTime;

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId == null ? null : tradeId.trim();
    }

    public Integer getActivityProductId() {
        return activityProductId;
    }

    public void setActivityProductId(Integer activityProductId) {
        this.activityProductId = activityProductId;
    }

    public Integer getQualType() {
        return qualType;
    }

    public void setQualType(Integer qualType) {
        this.qualType = qualType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }
}