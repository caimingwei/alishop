package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "commission_freeze_level_settings")
public class CommissionFreezeLevelSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private BigDecimal begin;

    private BigDecimal end;

    @Column(name = "freeze_day")
    private Integer freezeDay;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return begin
     */
    public BigDecimal getBegin() {
        return begin;
    }

    /**
     * @param begin
     */
    public void setBegin(BigDecimal begin) {
        this.begin = begin;
    }

    /**
     * @return end
     */
    public BigDecimal getEnd() {
        return end;
    }

    /**
     * @param end
     */
    public void setEnd(BigDecimal end) {
        this.end = end;
    }

    /**
     * @return freeze_day
     */
    public Integer getFreezeDay() {
        return freezeDay;
    }

    /**
     * @param freezeDay
     */
    public void setFreezeDay(Integer freezeDay) {
        this.freezeDay = freezeDay;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CommissionFreezeLevelSettings{" +
                "id=" + id +
                ", begin=" + begin +
                ", end=" + end +
                ", freezeDay=" + freezeDay +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", type=" + type +
                '}';
    }
}