package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "activity_pop")
public class ActivityPop {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    /**
     * 是否开启
     */
    @Column(name = "is_use")
    private Integer isUse;

    /**
     * 背景图
     */
    @Column(name = "backgroud_pic_path")
    private String backgroudPicPath;

    /**
     * 跳转url的类型
     */
    @Column(name = "skip_url_type")
    private String skipUrlType;

    /**
     * 跳转url
     */
    @Column(name = "skip_url")
    private String skipUrl;

    /**
     * 跳转url名称
     */
    @Column(name = "skip_name")
    private String skipName;

    /**
     * 开始时间
     */
    @Column(name = "begin_time")
    private Date beginTime;

    @Column(name = "open_type")
    private String openType;

    /**
     * 结束时间
     */
    @Column(name = "`end _time`")
    private Date endTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取是否开启
     *
     * @return is_use - 是否开启
     */
    public Integer getIsUse() {
        return isUse;
    }

    /**
     * 设置是否开启
     *
     * @param isUse 是否开启
     */
    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    /**
     * 获取背景图
     *
     * @return backgroud_pic_path - 背景图
     */
    public String getBackgroudPicPath() {
        return backgroudPicPath;
    }

    /**
     * 设置背景图
     *
     * @param backgroudPicPath 背景图
     */
    public void setBackgroudPicPath(String backgroudPicPath) {
        this.backgroudPicPath = backgroudPicPath;
    }

    public String getSkipName() {
        return skipName;
    }

    public void setSkipName(String skipName) {
        this.skipName = skipName;
    }

    /**
     * 获取跳转url的类型
     *
     * @return skip_url_type - 跳转url的类型
     */
    public String getSkipUrlType() {
        return skipUrlType;
    }

    /**
     * 设置跳转url的类型
     *
     * @param skipUrlType 跳转url的类型
     */
    public void setSkipUrlType(String skipUrlType) {
        this.skipUrlType = skipUrlType;
    }

    /**
     * 获取跳转url
     *
     * @return skip_url - 跳转url
     */
    public String getSkipUrl() {
        return skipUrl;
    }

    /**
     * 设置跳转url
     *
     * @param skipUrl 跳转url
     */
    public void setSkipUrl(String skipUrl) {
        this.skipUrl = skipUrl;
    }

    /**
     * 获取开始时间
     *
     * @return begin_time - 开始时间
     */
    public Date getBeginTime() {
        return beginTime;
    }

    /**
     * 设置开始时间
     *
     * @param beginTime 开始时间
     */
    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    /**
     * 获取结束时间
     *
     * @return end _time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }
}