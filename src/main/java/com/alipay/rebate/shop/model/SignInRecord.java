package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "sign_in_record")
public class SignInRecord {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "sign_in_time")
    private Date signInTime;

    @Column(name = "award_type")
    private Integer awardType;

    private BigDecimal money;

    @Column(name = "estimate_money")
    private BigDecimal estimateMoney;

    private Integer integral;

    @Column(name = "estimate_integral")
    private Integer estimateIntegral;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return sign_in_time
     */
    public Date getSignInTime() {
        return signInTime;
    }

    /**
     * @param signInTime
     */
    public void setSignInTime(Date signInTime) {
        this.signInTime = signInTime;
    }

    /**
     * @return award_type
     */
    public Integer getAwardType() {
        return awardType;
    }

    /**
     * @param awardType
     */
    public void setAwardType(Integer awardType) {
        this.awardType = awardType;
    }

    /**
     * @return money
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * @param money
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * @return estimate_money
     */
    public BigDecimal getEstimateMoney() {
        return estimateMoney;
    }

    /**
     * @param estimateMoney
     */
    public void setEstimateMoney(BigDecimal estimateMoney) {
        this.estimateMoney = estimateMoney;
    }

    /**
     * @return integral
     */
    public Integer getIntegral() {
        return integral;
    }

    /**
     * @param integral
     */
    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    /**
     * @return estimate_integral
     */
    public Integer getEstimateIntegral() {
        return estimateIntegral;
    }

    /**
     * @param estimateIntegral
     */
    public void setEstimateIntegral(Integer estimateIntegral) {
        this.estimateIntegral = estimateIntegral;
    }
}