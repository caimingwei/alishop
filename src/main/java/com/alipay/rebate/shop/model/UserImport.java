package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;

public class UserImport {
    private Long id;
    private Long userId;
    private Long invId;
    private String phone;
    private Integer userLevel;
    private String userName;
    private String avatar;
    private String agentLevel;
    private String wxUnionid;
    private String alipay;
    private String alipayName;
    private BigDecimal money;
    private Long jifen;
    private Date agentCreateTime;
    private Date loginTime;
    private Date createTime;
    private Long uit;

    public Long getUit() {
        return uit;
    }

    public void setUit(Long uit) {
        this.uit = uit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getInvId() {
        return invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(String agentLevel) {
        this.agentLevel = agentLevel;
    }

    public String getWxUnionid() {
        return wxUnionid;
    }

    public void setWxUnionid(String wxUnionid) {
        this.wxUnionid = wxUnionid;
    }

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }

    public String getAlipayName() {
        return alipayName;
    }

    public void setAlipayName(String alipayName) {
        this.alipayName = alipayName;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Long getJifen() {
        return jifen;
    }

    public void setJifen(Long jifen) {
        this.jifen = jifen;
    }

    public Date getAgentCreateTime() {
        return agentCreateTime;
    }

    public void setAgentCreateTime(Date agentCreateTime) {
        this.agentCreateTime = agentCreateTime;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "UserImport{" +
                "id=" + id +
                ", userId=" + userId +
                ", invId=" + invId +
                ", phone='" + phone + '\'' +
                ", userLevel=" + userLevel +
                ", userName='" + userName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", agentLevel='" + agentLevel + '\'' +
                ", wxUnionid='" + wxUnionid + '\'' +
                ", alipay='" + alipay + '\'' +
                ", alipayName='" + alipayName + '\'' +
                ", money=" + money +
                ", jifen=" + jifen +
                ", agentCreateTime=" + agentCreateTime +
                ", loginTime=" + loginTime +
                ", createTime=" + createTime +
                ", uit=" + uit +
                '}';
    }
}
