package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "activity_product")
public class ActivityProduct {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "activity_id")
    private Integer activityId;

    /**
     * 商品链接
     */
    @Column(name = "product_id")
    private String productId;

    private Integer productType;

    /**
     * 商品短链接
     */
    @Column(name = "short_product_url")
    private String shortProductUrl;

    /**
     * 短标题
     */
    @Column(name = "short_title")
    private String shortTitle;

    /**
     * 图片地址
     */
    @Column(name = "pic_url")
    private String picUrl;

    /**
     * 优惠券链接
     */
    @Column(name = "voucher_url")
    private String voucherUrl;

    /**
     * 返款额
     */
    @Column(name = "refunds_price")
    private BigDecimal refundsPrice;

    /**
     * 券后价
     */
    @Column(name = "after_price")
    private BigDecimal afterPrice;

    /**
     * 佣金比
     */
    @Column(name = "commission_ratio")
    private Integer commissionRatio;

    /**
     * 发放单数
     */
    @Column(name = "limit_num")
    private Integer limitNum;

    private Integer weight;

    @Column(name = "virtual_sale_num")
    private Integer virtualSaleNum;

    /**
     * 资格类型
     */
    @Column(name = "qual_type")
    private Integer qualType;

    /**
     * 开始时间
     */
    @Column(name = "begin_time")
    private Date beginTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 资格限制的json字符串设置
     */
    @Column(name = "qual_json")
    private String qualJson;

    private Integer isDeleted;

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return activity_id
     */
    public Integer getActivityId() {
        return activityId;
    }

    /**
     * @param activityId
     */
    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * 获取商品短链接
     *
     * @return short_product_url - 商品短链接
     */
    public String getShortProductUrl() {
        return shortProductUrl;
    }

    /**
     * 设置商品短链接
     *
     * @param shortProductUrl 商品短链接
     */
    public void setShortProductUrl(String shortProductUrl) {
        this.shortProductUrl = shortProductUrl;
    }

    /**
     * 获取短标题
     *
     * @return short_title - 短标题
     */
    public String getShortTitle() {
        return shortTitle;
    }

    /**
     * 设置短标题
     *
     * @param shortTitle 短标题
     */
    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    /**
     * 获取图片地址
     *
     * @return pic_url - 图片地址
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 设置图片地址
     *
     * @param picUrl 图片地址
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 获取优惠券链接
     *
     * @return voucher_url - 优惠券链接
     */
    public String getVoucherUrl() {
        return voucherUrl;
    }

    /**
     * 设置优惠券链接
     *
     * @param voucherUrl 优惠券链接
     */
    public void setVoucherUrl(String voucherUrl) {
        this.voucherUrl = voucherUrl;
    }

    /**
     * 获取返款额
     *
     * @return refunds_price - 返款额
     */
    public BigDecimal getRefundsPrice() {
        return refundsPrice;
    }

    /**
     * 设置返款额
     *
     * @param refundsPrice 返款额
     */
    public void setRefundsPrice(BigDecimal refundsPrice) {
        this.refundsPrice = refundsPrice;
    }

    /**
     * 获取券后价
     *
     * @return after_price - 券后价
     */
    public BigDecimal getAfterPrice() {
        return afterPrice;
    }

    /**
     * 设置券后价
     *
     * @param afterPrice 券后价
     */
    public void setAfterPrice(BigDecimal afterPrice) {
        this.afterPrice = afterPrice;
    }

    /**
     * 获取佣金比
     *
     * @return commission_ratio - 佣金比
     */
    public Integer getCommissionRatio() {
        return commissionRatio;
    }

    /**
     * 设置佣金比
     *
     * @param commissionRatio 佣金比
     */
    public void setCommissionRatio(Integer commissionRatio) {
        this.commissionRatio = commissionRatio;
    }

    /**
     * 获取发放单数
     *
     * @return limit_num - 发放单数
     */
    public Integer getLimitNum() {
        return limitNum;
    }

    /**
     * 设置发放单数
     *
     * @param limitNum 发放单数
     */
    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    /**
     * @return weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @param weight
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * @return virtual_sale_num
     */
    public Integer getVirtualSaleNum() {
        return virtualSaleNum;
    }

    /**
     * @param virtualSaleNum
     */
    public void setVirtualSaleNum(Integer virtualSaleNum) {
        this.virtualSaleNum = virtualSaleNum;
    }

    /**
     * 获取资格类型
     *
     * @return qual_type - 资格类型
     */
    public Integer getQualType() {
        return qualType;
    }

    /**
     * 设置资格类型
     *
     * @param qualType 资格类型
     */
    public void setQualType(Integer qualType) {
        this.qualType = qualType;
    }

    /**
     * 获取开始时间
     *
     * @return begin_time - 开始时间
     */
    public Date getBeginTime() {
        return beginTime;
    }

    /**
     * 设置开始时间
     *
     * @param beginTime 开始时间
     */
    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取资格限制的json字符串设置
     *
     * @return qual_json - 资格限制的json字符串设置
     */
    public String getQualJson() {
        return qualJson;
    }

    /**
     * 设置资格限制的json字符串设置
     *
     * @param qualJson 资格限制的json字符串设置
     */
    public void setQualJson(String qualJson) {
        this.qualJson = qualJson;
    }

    @Override
    public String toString() {
        return "ActivityProduct{" +
                "id=" + id +
                ", activityId=" + activityId +
                ", productId='" + productId + '\'' +
                ", productType=" + productType +
                ", shortProductUrl='" + shortProductUrl + '\'' +
                ", shortTitle='" + shortTitle + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", voucherUrl='" + voucherUrl + '\'' +
                ", refundsPrice=" + refundsPrice +
                ", afterPrice=" + afterPrice +
                ", commissionRatio=" + commissionRatio +
                ", limitNum=" + limitNum +
                ", weight=" + weight +
                ", virtualSaleNum=" + virtualSaleNum +
                ", qualType=" + qualType +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", qualJson='" + qualJson + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}