package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "app_sk")
public class AppSk {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    /**
     * 密令字符串
     */
    @Column(name = "secret_key")
    private String secretKey;

    /**
     * 密令数量
     */
    @Column(name = "key_number")
    private Integer keyNumber;

    /**
     * 推广人id
     */
    @Column(name = "promoter_id")
    private String promoterId;

    /**
     * 商品库id
     */
    @Column(name = "cust_product_id")
    private Integer custProductId;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取密令字符串
     *
     * @return secret_key - 密令字符串
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * 设置密令字符串
     *
     * @param secretKey 密令字符串
     */
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    /**
     * 获取密令数量
     *
     * @return key_number - 密令数量
     */
    public Integer getKeyNumber() {
        return keyNumber;
    }

    /**
     * 设置密令数量
     *
     * @param keyNumber 密令数量
     */
    public void setKeyNumber(Integer keyNumber) {
        this.keyNumber = keyNumber;
    }

    /**
     * 获取推广人id
     *
     * @return promoter_id - 推广人id
     */
    public String getPromoterId() {
        return promoterId;
    }

    /**
     * 设置推广人id
     *
     * @param promoterId 推广人id
     */
    public void setPromoterId(String promoterId) {
        this.promoterId = promoterId;
    }

    /**
     * 获取商品库id
     *
     * @return cust_product_id - 商品库id
     */
    public Integer getCustProductId() {
        return custProductId;
    }

    /**
     * 设置商品库id
     *
     * @param custProductId 商品库id
     */
    public void setCustProductId(Integer custProductId) {
        this.custProductId = custProductId;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}