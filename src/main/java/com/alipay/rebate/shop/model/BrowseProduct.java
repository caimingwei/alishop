package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "browse_product")
public class BrowseProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "goods_id")
    private Long goodsId;

    private String title;

    @Column(name = "main_pic")
    private String mainPic;

    @Column(name = "actual_price")
    private BigDecimal actualPrice;

    @Column(name = "coupon_price")
    private BigDecimal couponPrice;

    @Column(name = "browse_date")
    private Date browseDate;

    @Column(name = "browse_times")
    private Integer browseTimes;

    @Column(name = "last_browse_time")
    private Date lastBrowseTime;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return goods_id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * @param goodsId
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return main_pic
     */
    public String getMainPic() {
        return mainPic;
    }

    /**
     * @param mainPic
     */
    public void setMainPic(String mainPic) {
        this.mainPic = mainPic;
    }

    /**
     * @return actual_price
     */
    public BigDecimal getActualPrice() {
        return actualPrice;
    }

    /**
     * @param actualPrice
     */
    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    /**
     * @return coupon_price
     */
    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    /**
     * @param couponPrice
     */
    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    /**
     * @return browse_date
     */
    public Date getBrowseDate() {
        return browseDate;
    }

    /**
     * @param browseDate
     */
    public void setBrowseDate(Date browseDate) {
        this.browseDate = browseDate;
    }

    /**
     * @return browse_times
     */
    public Integer getBrowseTimes() {
        return browseTimes;
    }

    /**
     * @param browseTimes
     */
    public void setBrowseTimes(Integer browseTimes) {
        this.browseTimes = browseTimes;
    }

    /**
     * @return last_browse_time
     */
    public Date getLastBrowseTime() {
        return lastBrowseTime;
    }

    /**
     * @param lastBrowseTime
     */
    public void setLastBrowseTime(Date lastBrowseTime) {
        this.lastBrowseTime = lastBrowseTime;
    }
}