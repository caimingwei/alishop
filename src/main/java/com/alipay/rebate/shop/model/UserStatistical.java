package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "user_statistical")
public class UserStatistical {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "register_num")
    private Integer registerNum;

    @Column(name = "auth_num")
    private Integer authNum;

    @Column(name = "has_parent_auth_num")
    private Integer hasParentAuthNum;

    @Column(name = "has_parent_and_orders_num")
    private Integer hasParentAndOrdersNum;

    @Column(name = "no_parent_auth_num")
    private Integer noParentAuthNum;

    @Column(name = "no_parent_but_has_orders_num")
    private Integer noParentButHasOrdersNum;

    @Column(name = "has_parent_user_num")
    private Integer hasParentUserNum;

    @Column(name = "no_parent_user_num")
    private Integer noParentUserNum;

    @Column(name = "has_parent_user_scale")
    private BigDecimal hasParentUserScale;

    @Column(name = "no_parent_user_scale")
    private BigDecimal noParentUserScale;

    @Column(name = "has_parent_and_orders_scale")
    private BigDecimal hasParentAndOrdersScale;

    @Column(name = "`no_parent_but_has_orders_scale`")
    private BigDecimal noParentButHasOrdersScale;

    @Column(name = "has_parent_active")
    private BigDecimal hasParentActive;

    @Column(name = "no_parent_active")
    private BigDecimal noParentActive;

    @Column(name = "into_serven_day_has_parent_active")
    private BigDecimal intoServenDayHasParentActive;

    @Column(name = "into_serven_day_no_parent_active")
    private BigDecimal intoServenDayNoParentActive;

    @Column(name = "into_thirty_day_has_parent_active")
    private BigDecimal intoThirtyDayHasParentActive;

    @Column(name = "into_thirty_day_no_parent_active")
    private BigDecimal intoThirtyDayNoParentActive;

    @Column(name = "different_phone_user_scale")
    private String differentPhoneUserScale;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    private String authRatio;

    public String getAuthRatio() {
        return authRatio;
    }

    public void setAuthRatio(String authRatio) {
        this.authRatio = authRatio;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return register_num
     */
    public Integer getRegisterNum() {
        return registerNum;
    }

    /**
     * @param registerNum
     */
    public void setRegisterNum(Integer registerNum) {
        this.registerNum = registerNum;
    }

    /**
     * @return auth_num
     */
    public Integer getAuthNum() {
        return authNum;
    }

    /**
     * @param authNum
     */
    public void setAuthNum(Integer authNum) {
        this.authNum = authNum;
    }

    /**
     * @return has_parent_auth_num
     */
    public Integer getHasParentAuthNum() {
        return hasParentAuthNum;
    }

    /**
     * @param hasParentAuthNum
     */
    public void setHasParentAuthNum(Integer hasParentAuthNum) {
        this.hasParentAuthNum = hasParentAuthNum;
    }

    /**
     * @return has_parent_and_orders_num
     */
    public Integer getHasParentAndOrdersNum() {
        return hasParentAndOrdersNum;
    }

    /**
     * @param hasParentAndOrdersNum
     */
    public void setHasParentAndOrdersNum(Integer hasParentAndOrdersNum) {
        this.hasParentAndOrdersNum = hasParentAndOrdersNum;
    }

    /**
     * @return no_parent_auth_num
     */
    public Integer getNoParentAuthNum() {
        return noParentAuthNum;
    }

    /**
     * @param noParentAuthNum
     */
    public void setNoParentAuthNum(Integer noParentAuthNum) {
        this.noParentAuthNum = noParentAuthNum;
    }

    /**
     * @return no_parent_but_has_orders_num
     */
    public Integer getNoParentButHasOrdersNum() {
        return noParentButHasOrdersNum;
    }

    /**
     * @param noParentButHasOrdersNum
     */
    public void setNoParentButHasOrdersNum(Integer noParentButHasOrdersNum) {
        this.noParentButHasOrdersNum = noParentButHasOrdersNum;
    }

    /**
     * @return has_parent_user_num
     */
    public Integer getHasParentUserNum() {
        return hasParentUserNum;
    }

    /**
     * @param hasParentUserNum
     */
    public void setHasParentUserNum(Integer hasParentUserNum) {
        this.hasParentUserNum = hasParentUserNum;
    }

    /**
     * @return no_parent_user_num
     */
    public Integer getNoParentUserNum() {
        return noParentUserNum;
    }

    /**
     * @param noParentUserNum
     */
    public void setNoParentUserNum(Integer noParentUserNum) {
        this.noParentUserNum = noParentUserNum;
    }

    /**
     * @return has_parent_user_scale
     */
    public BigDecimal getHasParentUserScale() {
        return hasParentUserScale;
    }

    /**
     * @param hasParentUserScale
     */
    public void setHasParentUserScale(BigDecimal hasParentUserScale) {
        this.hasParentUserScale = hasParentUserScale;
    }

    /**
     * @return no_parent_user_scale
     */
    public BigDecimal getNoParentUserScale() {
        return noParentUserScale;
    }

    /**
     * @param noParentUserScale
     */
    public void setNoParentUserScale(BigDecimal noParentUserScale) {
        this.noParentUserScale = noParentUserScale;
    }

    /**
     * @return has_parent_and_orders_scale
     */
    public BigDecimal getHasParentAndOrdersScale() {
        return hasParentAndOrdersScale;
    }

    /**
     * @param hasParentAndOrdersScale
     */
    public void setHasParentAndOrdersScale(BigDecimal hasParentAndOrdersScale) {
        this.hasParentAndOrdersScale = hasParentAndOrdersScale;
    }

    /**
     * @return no_parent_but_has orders_scale
     */
    public BigDecimal getNoParentButHasOrdersScale() {
        return noParentButHasOrdersScale;
    }

    /**
     * @param noParentButHasOrdersScale
     */
    public void setNoParentButHasOrdersScale(BigDecimal noParentButHasOrdersScale) {
        this.noParentButHasOrdersScale = noParentButHasOrdersScale;
    }

    /**
     * @return has_parent_active
     */
    public BigDecimal getHasParentActive() {
        return hasParentActive;
    }

    /**
     * @param hasParentActive
     */
    public void setHasParentActive(BigDecimal hasParentActive) {
        this.hasParentActive = hasParentActive;
    }

    /**
     * @return no_parent_active
     */
    public BigDecimal getNoParentActive() {
        return noParentActive;
    }

    /**
     * @param noParentActive
     */
    public void setNoParentActive(BigDecimal noParentActive) {
        this.noParentActive = noParentActive;
    }

    /**
     * @return into_serven_day_has_parent_active
     */
    public BigDecimal getIntoServenDayHasParentActive() {
        return intoServenDayHasParentActive;
    }

    /**
     * @param intoServenDayHasParentActive
     */
    public void setIntoServenDayHasParentActive(BigDecimal intoServenDayHasParentActive) {
        this.intoServenDayHasParentActive = intoServenDayHasParentActive;
    }

    /**
     * @return into_serven_day_no_parent_active
     */
    public BigDecimal getIntoServenDayNoParentActive() {
        return intoServenDayNoParentActive;
    }

    /**
     * @param intoServenDayNoParentActive
     */
    public void setIntoServenDayNoParentActive(BigDecimal intoServenDayNoParentActive) {
        this.intoServenDayNoParentActive = intoServenDayNoParentActive;
    }

    /**
     * @return into_thirty_day_has_parent_active
     */
    public BigDecimal getIntoThirtyDayHasParentActive() {
        return intoThirtyDayHasParentActive;
    }

    /**
     * @param intoThirtyDayHasParentActive
     */
    public void setIntoThirtyDayHasParentActive(BigDecimal intoThirtyDayHasParentActive) {
        this.intoThirtyDayHasParentActive = intoThirtyDayHasParentActive;
    }

    /**
     * @return into_thirty_day_no_parent_active
     */
    public BigDecimal getIntoThirtyDayNoParentActive() {
        return intoThirtyDayNoParentActive;
    }

    /**
     * @param intoThirtyDayNoParentActive
     */
    public void setIntoThirtyDayNoParentActive(BigDecimal intoThirtyDayNoParentActive) {
        this.intoThirtyDayNoParentActive = intoThirtyDayNoParentActive;
    }

    /**
     * @return different_phone_user_scale
     */

    public String getDifferentPhoneUserScale() {
        return differentPhoneUserScale;
    }

    /**
     * @param differentPhoneUserScale
     */


    public void setDifferentPhoneUserScale(String differentPhoneUserScale) {
        this.differentPhoneUserScale = differentPhoneUserScale;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}