package com.alipay.rebate.shop.model;

import javax.persistence.*;

@Table(name = "yj_user_id")
public class YjUserId {
    private String uid;

    private String phone;

    /**
     * @return uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
}