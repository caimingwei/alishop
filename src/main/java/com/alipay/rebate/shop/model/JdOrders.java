package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "jd_orders")
public class JdOrders {
    /**
     * 订单完成时间
     */
    @Column(name = "finish_time")
    private String finishTime;

    /**
     * 下单设备(1:PC,2:无线)
     */
    @Column(name = "order_emt")
    private Integer orderEmt;

    /**
     * 订单ID
     */
    @Column(name = "order_id")
    private Long orderId;

    /**
     * 下单时间
     */
    @Column(name = "order_time")
    private String orderTime;

    /**
     * 父单的订单ID
     */
    @Column(name = "parent_id")
    private Long parentId;

    /**
     * 订单维度预估结算时间
     */
    @Column(name = "pay_month")
    private String payMonth;

    /**
     * 下单用户是否为PLUS会员 0：否，1：是
     */
    private Integer plus;

    /**
     * 商家ID
     */
    @Column(name = "pop_id")
    private Long popId;

    /**
     * 实际计算佣金的金额。订单完成后，会将误扣除的运费券金额更正。如订单完成后发生退款，此金额会更新
     */
    @Column(name = "actual_cos_price")
    private BigDecimal actualCosPrice;

    /**
     * 推客获得的实际佣金（实际计佣金额*佣金比例*最终比例）。如订单完成后发生退款，此金额会更新
     */
    @Column(name = "actual_fee")
    private BigDecimal actualFee;

    /**
     * 佣金比例
     */
    @Column(name = "commission_rate")
    private BigDecimal commissionRate;

    /**
     * 预估计佣金额
     */
    @Column(name = "estimate_cos_price")
    private BigDecimal estimateCosPrice;

    /**
     * 推客的预估佣金
     */
    @Column(name = "estimate_fee")
    private BigDecimal estimateFee;

    /**
     * 最终比例（分成比例+补贴比例）
     */
    @Column(name = "final_rate")
    private BigDecimal finalRate;

    /**
     * 一级类目ID
     */
    private Long cid1;

    /**
     * 商品售后中数量
     */
    @Column(name = "frozen_sku_num")
    private Long frozenSkuNum;

    /**
     * 联盟子站长身份标识
     */
    private String pid;

    /**
     * 推广位ID,0代表无推广位
     */
    @Column(name = "position_id")
    private Long positionId;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 二级类目ID
     */
    private Long cid2;

    /**
     * 网站ID，0：无网站
     */
    @Column(name = "site_id")
    private Long siteId;

    /**
     * 商品ID
     */
    @Column(name = "sku_id")
    private Long skuId;

    /**
     * 商品名称
     */
    @Column(name = "sku_name")
    private String skuName;

    /**
     * 商品数量
     */
    @Column(name = "sku_num")
    private Long skuNum;

    /**
     * 商品已退货数量
     */
    @Column(name = "sku_return_num")
    private Long skuReturnNum;

    /**
     * 分成比例
     */
    @Column(name = "sub_side_rate")
    private BigDecimal subSideRate;

    /**
     * 补贴比例
     */
    @Column(name = "subsidy_rate")
    private BigDecimal subsidyRate;

    /**
     * 三级类目ID
     */
    private Long cid3;

    /**
     * PID所属母账号平台名称
     */
    @Column(name = "union_alias")
    private String unionAlias;

    /**
     * 联盟标签数据
     */
    @Column(name = "union_tag")
    private String unionTag;

    /**
     * 渠道组 1：1号店，其他：京东
     */
    @Column(name = "union_traffic_group")
    private Integer unionTrafficGroup;

    /**
     * sku维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     */
    @Column(name = "skuinfo_valid_code")
    private Integer skuinfoValidCode;

    /**
     * 子联盟ID
     */
    @Column(name = "sub_union_id")
    private String subUnionId;

    /**
     * 2：同店；3：跨店
     */
    @Column(name = "trace_type")
    private Integer traceType;

    /**
     * 订单行维度预估结算时间0：未结算
     */
    @Column(name = "skuinfo_pay_month")
    private Integer skuinfoPayMonth;

    /**
     * 商家ID
     */
    @Column(name = "skuinfo_pop_id")
    private Long skuinfoPopId;

    /**
     * 推客生成推广链接时传入的扩展字段
     */
    @Column(name = "skuinfo_ext1")
    private String skuinfoExt1;

    /**
     * 招商团活动id，正整数，为0时表示无活动
     */
    @Column(name = "cp_actId")
    private Long cpActid;

    /**
     * 站长角色，1： 推客、 2： 团长
     */
    @Column(name = "union_role")
    private Integer unionRole;

    /**
     * 推客的联盟ID
     */
    @Column(name = "union_id")
    private Long unionId;

    /**
     * 推客生成推广链接时传入的扩展字段，订单维度
     */
    private String ext1;

    /**
     * 订单维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     */
    @Column(name = "valid_code")
    private Integer validCode;


    private Long userUit;

    private Long oneLevelUserUit;

    private Long twoLevelUserUit;

    private BigDecimal userCommission;

    private BigDecimal oneLevelCommission;

    private BigDecimal twoLevelCommission;

    private Long userId;

    private String userName;

    private Integer isRecord;

    private Integer activityId;

    private String imgUrl;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getIsRecord() {
        return isRecord;
    }

    public void setIsRecord(Integer isRecord) {
        this.isRecord = isRecord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserUit() {
        return userUit;
    }

    public void setUserUit(Long userUit) {
        this.userUit = userUit;
    }

    public Long getOneLevelUserUit() {
        return oneLevelUserUit;
    }

    public void setOneLevelUserUit(Long oneLevelUserUit) {
        this.oneLevelUserUit = oneLevelUserUit;
    }

    public Long getTwoLevelUserUit() {
        return twoLevelUserUit;
    }

    public void setTwoLevelUserUit(Long twoLevelUserUit) {
        this.twoLevelUserUit = twoLevelUserUit;
    }

    public BigDecimal getUserCommission() {
        return userCommission;
    }

    public void setUserCommission(BigDecimal userCommission) {
        this.userCommission = userCommission;
    }

    public BigDecimal getOneLevelCommission() {
        return oneLevelCommission;
    }

    public void setOneLevelCommission(BigDecimal oneLevelCommission) {
        this.oneLevelCommission = oneLevelCommission;
    }

    public BigDecimal getTwoLevelCommission() {
        return twoLevelCommission;
    }

    public void setTwoLevelCommission(BigDecimal twoLevelCommission) {
        this.twoLevelCommission = twoLevelCommission;
    }

    /**
     * 获取订单完成时间
     *
     * @return finish_time - 订单完成时间
     */
    public String getFinishTime() {
        return finishTime;
    }

    /**
     * 设置订单完成时间
     *
     * @param finishTime 订单完成时间
     */
    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    /**
     * 获取下单设备(1:PC,2:无线)
     *
     * @return order_emt - 下单设备(1:PC,2:无线)
     */
    public Integer getOrderEmt() {
        return orderEmt;
    }

    /**
     * 设置下单设备(1:PC,2:无线)
     *
     * @param orderEmt 下单设备(1:PC,2:无线)
     */
    public void setOrderEmt(Integer orderEmt) {
        this.orderEmt = orderEmt;
    }

    /**
     * 获取订单ID
     *
     * @return order_id - 订单ID
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * 设置订单ID
     *
     * @param orderId 订单ID
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取下单时间
     *
     * @return order_time - 下单时间
     */
    public String getOrderTime() {
        return orderTime;
    }

    /**
     * 设置下单时间
     *
     * @param orderTime 下单时间
     */
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    /**
     * 获取父单的订单ID
     *
     * @return parent_id - 父单的订单ID
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 设置父单的订单ID
     *
     * @param parentId 父单的订单ID
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取订单维度预估结算时间
     *
     * @return pay_month - 订单维度预估结算时间
     */
    public String getPayMonth() {
        return payMonth;
    }

    /**
     * 设置订单维度预估结算时间
     *
     * @param payMonth 订单维度预估结算时间
     */
    public void setPayMonth(String payMonth) {
        this.payMonth = payMonth;
    }

    /**
     * 获取下单用户是否为PLUS会员 0：否，1：是
     *
     * @return plus - 下单用户是否为PLUS会员 0：否，1：是
     */
    public Integer getPlus() {
        return plus;
    }

    /**
     * 设置下单用户是否为PLUS会员 0：否，1：是
     *
     * @param plus 下单用户是否为PLUS会员 0：否，1：是
     */
    public void setPlus(Integer plus) {
        this.plus = plus;
    }

    /**
     * 获取商家ID
     *
     * @return pop_id - 商家ID
     */
    public Long getPopId() {
        return popId;
    }

    /**
     * 设置商家ID
     *
     * @param popId 商家ID
     */
    public void setPopId(Long popId) {
        this.popId = popId;
    }

    /**
     * 获取实际计算佣金的金额。订单完成后，会将误扣除的运费券金额更正。如订单完成后发生退款，此金额会更新
     *
     * @return actual_cos_price - 实际计算佣金的金额。订单完成后，会将误扣除的运费券金额更正。如订单完成后发生退款，此金额会更新
     */
    public BigDecimal getActualCosPrice() {
        return actualCosPrice;
    }

    /**
     * 设置实际计算佣金的金额。订单完成后，会将误扣除的运费券金额更正。如订单完成后发生退款，此金额会更新
     *
     * @param actualCosPrice 实际计算佣金的金额。订单完成后，会将误扣除的运费券金额更正。如订单完成后发生退款，此金额会更新
     */
    public void setActualCosPrice(BigDecimal actualCosPrice) {
        this.actualCosPrice = actualCosPrice;
    }

    /**
     * 获取推客获得的实际佣金（实际计佣金额*佣金比例*最终比例）。如订单完成后发生退款，此金额会更新
     *
     * @return actual_fee - 推客获得的实际佣金（实际计佣金额*佣金比例*最终比例）。如订单完成后发生退款，此金额会更新
     */
    public BigDecimal getActualFee() {
        return actualFee;
    }

    /**
     * 设置推客获得的实际佣金（实际计佣金额*佣金比例*最终比例）。如订单完成后发生退款，此金额会更新
     *
     * @param actualFee 推客获得的实际佣金（实际计佣金额*佣金比例*最终比例）。如订单完成后发生退款，此金额会更新
     */
    public void setActualFee(BigDecimal actualFee) {
        this.actualFee = actualFee;
    }

    /**
     * 获取佣金比例
     *
     * @return commission_rate - 佣金比例
     */
    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    /**
     * 设置佣金比例
     *
     * @param commissionRate 佣金比例
     */
    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    /**
     * 获取预估计佣金额
     *
     * @return estimate_cos_price - 预估计佣金额
     */
    public BigDecimal getEstimateCosPrice() {
        return estimateCosPrice;
    }

    /**
     * 设置预估计佣金额
     *
     * @param estimateCosPrice 预估计佣金额
     */
    public void setEstimateCosPrice(BigDecimal estimateCosPrice) {
        this.estimateCosPrice = estimateCosPrice;
    }

    /**
     * 获取推客的预估佣金
     *
     * @return estimate_fee - 推客的预估佣金
     */
    public BigDecimal getEstimateFee() {
        return estimateFee;
    }

    /**
     * 设置推客的预估佣金
     *
     * @param estimateFee 推客的预估佣金
     */
    public void setEstimateFee(BigDecimal estimateFee) {
        this.estimateFee = estimateFee;
    }

    /**
     * 获取最终比例（分成比例+补贴比例）
     *
     * @return final_rate - 最终比例（分成比例+补贴比例）
     */
    public BigDecimal getFinalRate() {
        return finalRate;
    }

    /**
     * 设置最终比例（分成比例+补贴比例）
     *
     * @param finalRate 最终比例（分成比例+补贴比例）
     */
    public void setFinalRate(BigDecimal finalRate) {
        this.finalRate = finalRate;
    }

    /**
     * 获取一级类目ID
     *
     * @return cid1 - 一级类目ID
     */
    public Long getCid1() {
        return cid1;
    }

    /**
     * 设置一级类目ID
     *
     * @param cid1 一级类目ID
     */
    public void setCid1(Long cid1) {
        this.cid1 = cid1;
    }

    /**
     * 获取商品售后中数量
     *
     * @return frozen_sku_num - 商品售后中数量
     */
    public Long getFrozenSkuNum() {
        return frozenSkuNum;
    }

    /**
     * 设置商品售后中数量
     *
     * @param frozenSkuNum 商品售后中数量
     */
    public void setFrozenSkuNum(Long frozenSkuNum) {
        this.frozenSkuNum = frozenSkuNum;
    }

    /**
     * 获取联盟子站长身份标识
     *
     * @return pid - 联盟子站长身份标识
     */
    public String getPid() {
        return pid;
    }

    /**
     * 设置联盟子站长身份标识
     *
     * @param pid 联盟子站长身份标识
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * 获取推广位ID,0代表无推广位
     *
     * @return position_id - 推广位ID,0代表无推广位
     */
    public Long getPositionId() {
        return positionId;
    }

    /**
     * 设置推广位ID,0代表无推广位
     *
     * @param positionId 推广位ID,0代表无推广位
     */
    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    /**
     * 获取商品单价
     *
     * @return price - 商品单价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 设置商品单价
     *
     * @param price 商品单价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 获取二级类目ID
     *
     * @return cid2 - 二级类目ID
     */
    public Long getCid2() {
        return cid2;
    }

    /**
     * 设置二级类目ID
     *
     * @param cid2 二级类目ID
     */
    public void setCid2(Long cid2) {
        this.cid2 = cid2;
    }

    /**
     * 获取网站ID，0：无网站
     *
     * @return site_id - 网站ID，0：无网站
     */
    public Long getSiteId() {
        return siteId;
    }

    /**
     * 设置网站ID，0：无网站
     *
     * @param siteId 网站ID，0：无网站
     */
    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    /**
     * 获取商品ID
     *
     * @return sku_id - 商品ID
     */
    public Long getSkuId() {
        return skuId;
    }

    /**
     * 设置商品ID
     *
     * @param skuId 商品ID
     */
    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    /**
     * 获取商品名称
     *
     * @return sku_name - 商品名称
     */
    public String getSkuName() {
        return skuName;
    }

    /**
     * 设置商品名称
     *
     * @param skuName 商品名称
     */
    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    /**
     * 获取商品数量
     *
     * @return sku_num - 商品数量
     */
    public Long getSkuNum() {
        return skuNum;
    }

    /**
     * 设置商品数量
     *
     * @param skuNum 商品数量
     */
    public void setSkuNum(Long skuNum) {
        this.skuNum = skuNum;
    }

    /**
     * 获取商品已退货数量
     *
     * @return sku_return_num - 商品已退货数量
     */
    public Long getSkuReturnNum() {
        return skuReturnNum;
    }

    /**
     * 设置商品已退货数量
     *
     * @param skuReturnNum 商品已退货数量
     */
    public void setSkuReturnNum(Long skuReturnNum) {
        this.skuReturnNum = skuReturnNum;
    }

    /**
     * 获取分成比例
     *
     * @return sub_side_rate - 分成比例
     */
    public BigDecimal getSubSideRate() {
        return subSideRate;
    }

    /**
     * 设置分成比例
     *
     * @param subSideRate 分成比例
     */
    public void setSubSideRate(BigDecimal subSideRate) {
        this.subSideRate = subSideRate;
    }

    /**
     * 获取补贴比例
     *
     * @return subsidy_rate - 补贴比例
     */
    public BigDecimal getSubsidyRate() {
        return subsidyRate;
    }

    /**
     * 设置补贴比例
     *
     * @param subsidyRate 补贴比例
     */
    public void setSubsidyRate(BigDecimal subsidyRate) {
        this.subsidyRate = subsidyRate;
    }

    /**
     * 获取三级类目ID
     *
     * @return cid3 - 三级类目ID
     */
    public Long getCid3() {
        return cid3;
    }

    /**
     * 设置三级类目ID
     *
     * @param cid3 三级类目ID
     */
    public void setCid3(Long cid3) {
        this.cid3 = cid3;
    }

    /**
     * 获取PID所属母账号平台名称
     *
     * @return union_alias - PID所属母账号平台名称
     */
    public String getUnionAlias() {
        return unionAlias;
    }

    /**
     * 设置PID所属母账号平台名称
     *
     * @param unionAlias PID所属母账号平台名称
     */
    public void setUnionAlias(String unionAlias) {
        this.unionAlias = unionAlias;
    }

    /**
     * 获取联盟标签数据
     *
     * @return union_tag - 联盟标签数据
     */
    public String getUnionTag() {
        return unionTag;
    }

    /**
     * 设置联盟标签数据
     *
     * @param unionTag 联盟标签数据
     */
    public void setUnionTag(String unionTag) {
        this.unionTag = unionTag;
    }

    /**
     * 获取渠道组 1：1号店，其他：京东
     *
     * @return union_traffic_group - 渠道组 1：1号店，其他：京东
     */
    public Integer getUnionTrafficGroup() {
        return unionTrafficGroup;
    }

    /**
     * 设置渠道组 1：1号店，其他：京东
     *
     * @param unionTrafficGroup 渠道组 1：1号店，其他：京东
     */
    public void setUnionTrafficGroup(Integer unionTrafficGroup) {
        this.unionTrafficGroup = unionTrafficGroup;
    }

    /**
     * 获取sku维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     *
     * @return skuinfo_valid_code - sku维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     */
    public Integer getSkuinfoValidCode() {
        return skuinfoValidCode;
    }

    /**
     * 设置sku维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     *
     * @param skuinfoValidCode sku维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     */
    public void setSkuinfoValidCode(Integer skuinfoValidCode) {
        this.skuinfoValidCode = skuinfoValidCode;
    }

    /**
     * 获取子联盟ID
     *
     * @return sub_union_id - 子联盟ID
     */
    public String getSubUnionId() {
        return subUnionId;
    }

    /**
     * 设置子联盟ID
     *
     * @param subUnionId 子联盟ID
     */
    public void setSubUnionId(String subUnionId) {
        this.subUnionId = subUnionId;
    }

    /**
     * 获取2：同店；3：跨店
     *
     * @return trace_type - 2：同店；3：跨店
     */
    public Integer getTraceType() {
        return traceType;
    }

    /**
     * 设置2：同店；3：跨店
     *
     * @param traceType 2：同店；3：跨店
     */
    public void setTraceType(Integer traceType) {
        this.traceType = traceType;
    }

    /**
     * 获取订单行维度预估结算时间0：未结算
     *
     * @return skuinfo_pay_month - 订单行维度预估结算时间0：未结算
     */
    public Integer getSkuinfoPayMonth() {
        return skuinfoPayMonth;
    }

    /**
     * 设置订单行维度预估结算时间0：未结算
     *
     * @param skuinfoPayMonth 订单行维度预估结算时间0：未结算
     */
    public void setSkuinfoPayMonth(Integer skuinfoPayMonth) {
        this.skuinfoPayMonth = skuinfoPayMonth;
    }

    /**
     * 获取商家ID
     *
     * @return skuinfo_pop_id - 商家ID
     */
    public Long getSkuinfoPopId() {
        return skuinfoPopId;
    }

    /**
     * 设置商家ID
     *
     * @param skuinfoPopId 商家ID
     */
    public void setSkuinfoPopId(Long skuinfoPopId) {
        this.skuinfoPopId = skuinfoPopId;
    }

    /**
     * 获取推客生成推广链接时传入的扩展字段
     *
     * @return skuinfo_ext1 - 推客生成推广链接时传入的扩展字段
     */
    public String getSkuinfoExt1() {
        return skuinfoExt1;
    }

    /**
     * 设置推客生成推广链接时传入的扩展字段
     *
     * @param skuinfoExt1 推客生成推广链接时传入的扩展字段
     */
    public void setSkuinfoExt1(String skuinfoExt1) {
        this.skuinfoExt1 = skuinfoExt1;
    }

    /**
     * 获取招商团活动id，正整数，为0时表示无活动
     *
     * @return cp_actId - 招商团活动id，正整数，为0时表示无活动
     */
    public Long getCpActid() {
        return cpActid;
    }

    /**
     * 设置招商团活动id，正整数，为0时表示无活动
     *
     * @param cpActid 招商团活动id，正整数，为0时表示无活动
     */
    public void setCpActid(Long cpActid) {
        this.cpActid = cpActid;
    }

    /**
     * 获取站长角色，1： 推客、 2： 团长
     *
     * @return union_role - 站长角色，1： 推客、 2： 团长
     */
    public Integer getUnionRole() {
        return unionRole;
    }

    /**
     * 设置站长角色，1： 推客、 2： 团长
     *
     * @param unionRole 站长角色，1： 推客、 2： 团长
     */
    public void setUnionRole(Integer unionRole) {
        this.unionRole = unionRole;
    }

    /**
     * 获取推客的联盟ID
     *
     * @return union_id - 推客的联盟ID
     */
    public Long getUnionId() {
        return unionId;
    }

    /**
     * 设置推客的联盟ID
     *
     * @param unionId 推客的联盟ID
     */
    public void setUnionId(Long unionId) {
        this.unionId = unionId;
    }

    /**
     * 获取推客生成推广链接时传入的扩展字段，订单维度
     *
     * @return ext1 - 推客生成推广链接时传入的扩展字段，订单维度
     */
    public String getExt1() {
        return ext1;
    }

    /**
     * 设置推客生成推广链接时传入的扩展字段，订单维度
     *
     * @param ext1 推客生成推广链接时传入的扩展字段，订单维度
     */
    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    /**
     * 获取订单维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     *
     * @return valid_code - 订单维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     */
    public Integer getValidCode() {
        return validCode;
    }

    /**
     * 设置订单维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     *
     * @param validCode 订单维度的有效码（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示）
     */
    public void setValidCode(Integer validCode) {
        this.validCode = validCode;
    }

    @Override
    public String toString() {
        return "JdOrders{" +
                "finishTime='" + finishTime + '\'' +
                ", orderEmt=" + orderEmt +
                ", orderId=" + orderId +
                ", orderTime='" + orderTime + '\'' +
                ", parentId=" + parentId +
                ", payMonth='" + payMonth + '\'' +
                ", plus=" + plus +
                ", popId=" + popId +
                ", actualCosPrice=" + actualCosPrice +
                ", actualFee=" + actualFee +
                ", commissionRate=" + commissionRate +
                ", estimateCosPrice=" + estimateCosPrice +
                ", estimateFee=" + estimateFee +
                ", finalRate=" + finalRate +
                ", cid1=" + cid1 +
                ", frozenSkuNum=" + frozenSkuNum +
                ", pid='" + pid + '\'' +
                ", positionId=" + positionId +
                ", price=" + price +
                ", cid2=" + cid2 +
                ", siteId=" + siteId +
                ", skuId=" + skuId +
                ", skuName='" + skuName + '\'' +
                ", skuNum=" + skuNum +
                ", skuReturnNum=" + skuReturnNum +
                ", subSideRate=" + subSideRate +
                ", subsidyRate=" + subsidyRate +
                ", cid3=" + cid3 +
                ", unionAlias='" + unionAlias + '\'' +
                ", unionTag='" + unionTag + '\'' +
                ", unionTrafficGroup=" + unionTrafficGroup +
                ", skuinfoValidCode=" + skuinfoValidCode +
                ", subUnionId='" + subUnionId + '\'' +
                ", traceType=" + traceType +
                ", skuinfoPayMonth=" + skuinfoPayMonth +
                ", skuinfoPopId=" + skuinfoPopId +
                ", skuinfoExt1='" + skuinfoExt1 + '\'' +
                ", cpActid=" + cpActid +
                ", unionRole=" + unionRole +
                ", unionId=" + unionId +
                ", ext1='" + ext1 + '\'' +
                ", validCode=" + validCode +
                ", userUit=" + userUit +
                ", oneLevelUserUit=" + oneLevelUserUit +
                ", twoLevelUserUit=" + twoLevelUserUit +
                ", userCommission=" + userCommission +
                ", oneLevelCommission=" + oneLevelCommission +
                ", twoLevelCommission=" + twoLevelCommission +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", isRecord=" + isRecord +
                ", activityId=" + activityId +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}