package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Product {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "goods_id")
    private Long goodsId;

    @Column(name = "item_link")
    private String itemLink;

    private String title;

    private String dtitle;

    private String description;

    private Long cid;

    private Long tbcid;

    @Column(name = "main_pic")
    private String mainPic;

    @Column(name = "marketing_main_pic")
    private String marketingMainPic;

    @Column(name = "original_price")
    private BigDecimal originalPrice;

    @Column(name = "actual_price")
    private BigDecimal actualPrice;

    private BigDecimal discounts;

    @Column(name = "commission_type")
    private Integer commissionType;

    @Column(name = "commission_rate")
    private BigDecimal commissionRate;

    @Column(name = "coupon_link")
    private String couponLink;

    @Column(name = "coupon_total_num")
    private Integer couponTotalNum;

    @Column(name = "coupon_receive_num")
    private Integer couponReceiveNum;

    @Column(name = "coupon_end_time")
    private String couponEndTime;

    @Column(name = "coupon_start_time")
    private String couponStartTime;

    @Column(name = "coupon_price")
    private BigDecimal couponPrice;

    @Column(name = "coupon_conditions")
    private String couponConditions;

    @Column(name = "month_sales")
    private Integer monthSales;

    @Column(name = "two_hours_sales")
    private Integer twoHoursSales;

    @Column(name = "daily_sales")
    private Integer dailySales;

    private Integer brand;

    @Column(name = "brand_id")
    private Long brandId;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name = "create_time")
    private String createTime;

    private Integer tchaoshi;

    @Column(name = "activity_type")
    private Integer activityType;

    @Column(name = "activity_start_time")
    private String activityStartTime;

    @Column(name = "activity_end_time")
    private String activityEndTime;

    @Column(name = "shop_type")
    private Integer shopType;

    private Integer haitao;

    @Column(name = "gold_sellers")
    private Integer goldSellers;

    @Column(name = "seller_id")
    private Long sellerId;

    @Column(name = "shop_name")
    private String shopName;

    @Column(name = "shop_level")
    private String shopLevel;

    @Column(name = "desc_score")
    private BigDecimal descScore;

    @Column(name = "dsr_score")
    private BigDecimal dsrScore;

    @Column(name = "dsr_percent")
    private BigDecimal dsrPercent;

    @Column(name = "ship_score")
    private BigDecimal shipScore;

    @Column(name = "ship_percent")
    private BigDecimal shipPercent;

    @Column(name = "service_score")
    private BigDecimal serviceScore;

    @Column(name = "service_percent")
    private BigDecimal servicePercent;

    @Column(name = "hot_push")
    private Integer hotPush;

    @Column(name = "team_name")
    private String teamName;

    @Column(name = "product_type")
    private Integer productType;
    @Column(name = "quan_m_link")
    private BigDecimal quanMLink;
    @Column(name = "hz_quan_over")
    private BigDecimal hzQuanOver;
    @Column(name = "circle_text")
    private String circleText;
    @Column(name = "freeship_remote_district")
    private Integer freeshipRemoteDistrict;
    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return goods_id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * @param goodsId
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * @return item_link
     */
    public String getItemLink() {
        return itemLink;
    }

    /**
     * @param itemLink
     */
    public void setItemLink(String itemLink) {
        this.itemLink = itemLink;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return dtitle
     */
    public String getDtitle() {
        return dtitle;
    }

    /**
     * @param dtitle
     */
    public void setDtitle(String dtitle) {
        this.dtitle = dtitle;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return cid
     */
    public Long getCid() {
        return cid;
    }

    /**
     * @param cid
     */
    public void setCid(Long cid) {
        this.cid = cid;
    }

    /**
     * @return tbcid
     */
    public Long getTbcid() {
        return tbcid;
    }

    /**
     * @param tbcid
     */
    public void setTbcid(Long tbcid) {
        this.tbcid = tbcid;
    }

    /**
     * @return main_pic
     */
    public String getMainPic() {
        return mainPic;
    }

    /**
     * @param mainPic
     */
    public void setMainPic(String mainPic) {
        this.mainPic = mainPic;
    }

    /**
     * @return marketing_main_pic
     */
    public String getMarketingMainPic() {
        return marketingMainPic;
    }

    /**
     * @param marketingMainPic
     */
    public void setMarketingMainPic(String marketingMainPic) {
        this.marketingMainPic = marketingMainPic;
    }

    /**
     * @return original_price
     */
    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    /**
     * @param originalPrice
     */
    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    /**
     * @return actual_price
     */
    public BigDecimal getActualPrice() {
        return actualPrice;
    }

    /**
     * @param actualPrice
     */
    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    /**
     * @return discounts
     */
    public BigDecimal getDiscounts() {
        return discounts;
    }

    /**
     * @param discounts
     */
    public void setDiscounts(BigDecimal discounts) {
        this.discounts = discounts;
    }

    /**
     * @return commission_type
     */
    public Integer getCommissionType() {
        return commissionType;
    }

    /**
     * @param commissionType
     */
    public void setCommissionType(Integer commissionType) {
        this.commissionType = commissionType;
    }

    /**
     * @return commission_rate
     */
    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    /**
     * @param commissionRate
     */
    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    /**
     * @return coupon_link
     */
    public String getCouponLink() {
        return couponLink;
    }

    /**
     * @param couponLink
     */
    public void setCouponLink(String couponLink) {
        this.couponLink = couponLink;
    }

    /**
     * @return coupon_total_num
     */
    public Integer getCouponTotalNum() {
        return couponTotalNum;
    }

    /**
     * @param couponTotalNum
     */
    public void setCouponTotalNum(Integer couponTotalNum) {
        this.couponTotalNum = couponTotalNum;
    }

    /**
     * @return coupon_receive_num
     */
    public Integer getCouponReceiveNum() {
        return couponReceiveNum;
    }

    /**
     * @param couponReceiveNum
     */
    public void setCouponReceiveNum(Integer couponReceiveNum) {
        this.couponReceiveNum = couponReceiveNum;
    }

    /**
     * @return coupon_end_time
     */
    public String getCouponEndTime() {
        return couponEndTime;
    }

    /**
     * @param couponEndTime
     */
    public void setCouponEndTime(String couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    /**
     * @return coupon_start_time
     */
    public String getCouponStartTime() {
        return couponStartTime;
    }

    /**
     * @param couponStartTime
     */
    public void setCouponStartTime(String couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    /**
     * @return coupon_price
     */
    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    /**
     * @param couponPrice
     */
    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    /**
     * @return coupon_conditions
     */
    public String getCouponConditions() {
        return couponConditions;
    }

    /**
     * @param couponConditions
     */
    public void setCouponConditions(String couponConditions) {
        this.couponConditions = couponConditions;
    }

    /**
     * @return month_sales
     */
    public Integer getMonthSales() {
        return monthSales;
    }

    /**
     * @param monthSales
     */
    public void setMonthSales(Integer monthSales) {
        this.monthSales = monthSales;
    }

    /**
     * @return two_hours_sales
     */
    public Integer getTwoHoursSales() {
        return twoHoursSales;
    }

    /**
     * @param twoHoursSales
     */
    public void setTwoHoursSales(Integer twoHoursSales) {
        this.twoHoursSales = twoHoursSales;
    }

    /**
     * @return daily_sales
     */
    public Integer getDailySales() {
        return dailySales;
    }

    /**
     * @param dailySales
     */
    public void setDailySales(Integer dailySales) {
        this.dailySales = dailySales;
    }

    /**
     * @return brand
     */
    public Integer getBrand() {
        return brand;
    }

    /**
     * @param brand
     */
    public void setBrand(Integer brand) {
        this.brand = brand;
    }

    /**
     * @return brand_id
     */
    public Long getBrandId() {
        return brandId;
    }

    /**
     * @param brandId
     */
    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    /**
     * @return brand_name
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * @param brandName
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     * @return create_time
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return tchaoshi
     */
    public Integer getTchaoshi() {
        return tchaoshi;
    }

    /**
     * @param tchaoshi
     */
    public void setTchaoshi(Integer tchaoshi) {
        this.tchaoshi = tchaoshi;
    }

    /**
     * @return activity_type
     */
    public Integer getActivityType() {
        return activityType;
    }

    /**
     * @param activityType
     */
    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    /**
     * @return activity_start_time
     */
    public String getActivityStartTime() {
        return activityStartTime;
    }

    /**
     * @param activityStartTime
     */
    public void setActivityStartTime(String activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    /**
     * @return activity_end_time
     */
    public String getActivityEndTime() {
        return activityEndTime;
    }

    /**
     * @param activityEndTime
     */
    public void setActivityEndTime(String activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    /**
     * @return shop_type
     */
    public Integer getShopType() {
        return shopType;
    }

    /**
     * @param shopType
     */
    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    /**
     * @return haitao
     */
    public Integer getHaitao() {
        return haitao;
    }

    /**
     * @param haitao
     */
    public void setHaitao(Integer haitao) {
        this.haitao = haitao;
    }

    /**
     * @return gold_sellers
     */
    public Integer getGoldSellers() {
        return goldSellers;
    }

    /**
     * @param goldSellers
     */
    public void setGoldSellers(Integer goldSellers) {
        this.goldSellers = goldSellers;
    }

    /**
     * @return seller_id
     */
    public Long getSellerId() {
        return sellerId;
    }

    /**
     * @param sellerId
     */
    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * @return shop_name
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * @param shopName
     */
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    /**
     * @return shop_level
     */
    public String getShopLevel() {
        return shopLevel;
    }

    /**
     * @param shopLevel
     */
    public void setShopLevel(String shopLevel) {
        this.shopLevel = shopLevel;
    }

    /**
     * @return desc_score
     */
    public BigDecimal getDescScore() {
        return descScore;
    }

    /**
     * @param descScore
     */
    public void setDescScore(BigDecimal descScore) {
        this.descScore = descScore;
    }

    /**
     * @return dsr_score
     */
    public BigDecimal getDsrScore() {
        return dsrScore;
    }

    /**
     * @param dsrScore
     */
    public void setDsrScore(BigDecimal dsrScore) {
        this.dsrScore = dsrScore;
    }

    /**
     * @return dsr_percent
     */
    public BigDecimal getDsrPercent() {
        return dsrPercent;
    }

    /**
     * @param dsrPercent
     */
    public void setDsrPercent(BigDecimal dsrPercent) {
        this.dsrPercent = dsrPercent;
    }

    /**
     * @return ship_score
     */
    public BigDecimal getShipScore() {
        return shipScore;
    }

    /**
     * @param shipScore
     */
    public void setShipScore(BigDecimal shipScore) {
        this.shipScore = shipScore;
    }

    /**
     * @return ship_percent
     */
    public BigDecimal getShipPercent() {
        return shipPercent;
    }

    /**
     * @param shipPercent
     */
    public void setShipPercent(BigDecimal shipPercent) {
        this.shipPercent = shipPercent;
    }

    /**
     * @return service_score
     */
    public BigDecimal getServiceScore() {
        return serviceScore;
    }

    /**
     * @param serviceScore
     */
    public void setServiceScore(BigDecimal serviceScore) {
        this.serviceScore = serviceScore;
    }

    /**
     * @return service_percent
     */
    public BigDecimal getServicePercent() {
        return servicePercent;
    }

    /**
     * @param servicePercent
     */
    public void setServicePercent(BigDecimal servicePercent) {
        this.servicePercent = servicePercent;
    }

    /**
     * @return hot_push
     */
    public Integer getHotPush() {
        return hotPush;
    }

    /**
     * @param hotPush
     */
    public void setHotPush(Integer hotPush) {
        this.hotPush = hotPush;
    }

    /**
     * @return team_name
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param teamName
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * @return product_type
     */
    public Integer getProductType() {
        return productType;
    }

    /**
     * @param productType
     */
    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public BigDecimal getQuanMLink() {
        return quanMLink;
    }

    public void setQuanMLink(BigDecimal quanMLink) {
        this.quanMLink = quanMLink;
    }

    public BigDecimal getHzQuanOver() {
        return hzQuanOver;
    }

    public void setHzQuanOver(BigDecimal hzQuanOver) {
        this.hzQuanOver = hzQuanOver;
    }

    public String getCircleText() {
        return circleText;
    }

    public void setCircleText(String circleText) {
        this.circleText = circleText;
    }

    public Integer getFreeshipRemoteDistrict() {
        return freeshipRemoteDistrict;
    }

    public void setFreeshipRemoteDistrict(Integer freeshipRemoteDistrict) {
        this.freeshipRemoteDistrict = freeshipRemoteDistrict;
    }
}