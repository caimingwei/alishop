package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "task_settings")
public class TaskSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Column(name = "is_use")
    private Integer isUse;

    @Column(name = "max_times")
    private Integer maxTimes;

    private Integer type;

    private Integer subType;

    @Column(name = "award_type")
    private Integer awardType;

    @Column(name = "award_num")
    private Integer awardNum;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return is_use
     */
    public Integer getIsUse() {
        return isUse;
    }

    /**
     * @param isUse
     */
    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    /**
     * @return max_times
     */
    public Integer getMaxTimes() {
        return maxTimes;
    }

    /**
     * @param maxTimes
     */
    public void setMaxTimes(Integer maxTimes) {
        this.maxTimes = maxTimes;
    }

    /**
     * @return type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    /**
     * @return award_type
     */
    public Integer getAwardType() {
        return awardType;
    }

    /**
     * @param awardType
     */
    public void setAwardType(Integer awardType) {
        this.awardType = awardType;
    }

    /**
     * @return award_num
     */
    public Integer getAwardNum() {
        return awardNum;
    }

    /**
     * @param awardNum
     */
    public void setAwardNum(Integer awardNum) {
        this.awardNum = awardNum;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}