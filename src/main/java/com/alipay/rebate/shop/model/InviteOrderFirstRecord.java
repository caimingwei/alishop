package com.alipay.rebate.shop.model;

import java.math.BigDecimal;

public class InviteOrderFirstRecord {
    private Long id;

    private Long userId;

    private Long parentUserId;

    private String tradeId;

    private String tbCreateTime;

    private String tbEarningTime;

    private BigDecimal alipayTotalPrice;

    private Integer awardType;

    private BigDecimal awardMoney;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Long parentUserId) {
        this.parentUserId = parentUserId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId == null ? null : tradeId.trim();
    }

    public String getTbCreateTime() {
        return tbCreateTime;
    }

    public void setTbCreateTime(String tbCreateTime) {
        this.tbCreateTime = tbCreateTime == null ? null : tbCreateTime.trim();
    }

    public String getTbEarningTime() {
        return tbEarningTime;
    }

    public void setTbEarningTime(String tbEarningTime) {
        this.tbEarningTime = tbEarningTime == null ? null : tbEarningTime.trim();
    }

    public BigDecimal getAlipayTotalPrice() {
        return alipayTotalPrice;
    }

    public void setAlipayTotalPrice(BigDecimal alipayTotalPrice) {
        this.alipayTotalPrice = alipayTotalPrice;
    }

    public Integer getAwardType() {
        return awardType;
    }

    public void setAwardType(Integer awardType) {
        this.awardType = awardType;
    }

    public BigDecimal getAwardMoney() {
        return awardMoney;
    }

    public void setAwardMoney(BigDecimal awardMoney) {
        this.awardMoney = awardMoney;
    }
}