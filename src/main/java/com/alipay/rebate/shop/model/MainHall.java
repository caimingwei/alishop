package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "main_hall")
public class MainHall {
    /**
     * 活动模板id
     */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "mos_url")
    private String mosUrl;

    /**
     * 联盟活动id
     */
    @Column(name = "leagues_activity_id")
    private String leaguesActivityId;

    private String pid;

    /**
     * 1-联盟官方活动  2-mos开头的链接活动
     */
    private Integer type;

    /**
     * 附图
     */
    @Column(name = "figure_url")
    private String figureUrl;

    /**
     * 主图
     */
    @Column(name = "main_diagram_url")
    private String mainDiagramUrl;

    @Column(name = "add_time")
    private Date addTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取活动模板id
     *
     * @return id - 活动模板id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置活动模板id
     *
     * @param id 活动模板id
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return mos_url
     */
    public String getMosUrl() {
        return mosUrl;
    }

    /**
     * @param mosUrl
     */
    public void setMosUrl(String mosUrl) {
        this.mosUrl = mosUrl;
    }

    /**
     * 获取联盟活动id
     *
     * @return leagues_activity_id - 联盟活动id
     */
    public String getLeaguesActivityId() {
        return leaguesActivityId;
    }

    /**
     * 设置联盟活动id
     *
     * @param leaguesActivityId 联盟活动id
     */
    public void setLeaguesActivityId(String leaguesActivityId) {
        this.leaguesActivityId = leaguesActivityId;
    }

    /**
     * @return pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * @param pid
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * 获取1-联盟官方活动  2-mos开头的链接活动
     *
     * @return type - 1-联盟官方活动  2-mos开头的链接活动
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置1-联盟官方活动  2-mos开头的链接活动
     *
     * @param type 1-联盟官方活动  2-mos开头的链接活动
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取附图
     *
     * @return figure_url - 附图
     */
    public String getFigureUrl() {
        return figureUrl;
    }

    /**
     * 设置附图
     *
     * @param figureUrl 附图
     */
    public void setFigureUrl(String figureUrl) {
        this.figureUrl = figureUrl;
    }

    /**
     * 获取主图
     *
     * @return main_diagram_url - 主图
     */
    public String getMainDiagramUrl() {
        return mainDiagramUrl;
    }

    /**
     * 设置主图
     *
     * @param mainDiagramUrl 主图
     */
    public void setMainDiagramUrl(String mainDiagramUrl) {
        this.mainDiagramUrl = mainDiagramUrl;
    }

    /**
     * @return add_time
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * @param addTime
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "MainHall{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mosUrl='" + mosUrl + '\'' +
                ", leaguesActivityId=" + leaguesActivityId +
                ", pid='" + pid + '\'' +
                ", type=" + type +
                ", figureUrl='" + figureUrl + '\'' +
                ", mainDiagramUrl='" + mainDiagramUrl + '\'' +
                ", addTime=" + addTime +
                ", updateTime=" + updateTime +
                '}';
    }
}