package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "sign_in_award")
public class SignInAward {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "newly_sign_in_time")
    private Date newlySignInTime;

    @Column(name = "sign_in_days")
    private Integer signInDays;

    @Column(name = "total_award_money")
    private BigDecimal totalAwardMoney;

    @Column(name = "total_award_integral")
    private Integer totalAwardIntegral;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return newly_sign_in_time
     */
    public Date getNewlySignInTime() {
        return newlySignInTime;
    }

    /**
     * @param newlySignInTime
     */
    public void setNewlySignInTime(Date newlySignInTime) {
        this.newlySignInTime = newlySignInTime;
    }

    /**
     * @return sign_in_days
     */
    public Integer getSignInDays() {
        return signInDays;
    }

    /**
     * @param signInDays
     */
    public void setSignInDays(Integer signInDays) {
        this.signInDays = signInDays;
    }

    /**
     * @return total_award_money
     */
    public BigDecimal getTotalAwardMoney() {
        return totalAwardMoney;
    }

    /**
     * @param totalAwardMoney
     */
    public void setTotalAwardMoney(BigDecimal totalAwardMoney) {
        this.totalAwardMoney = totalAwardMoney;
    }

    /**
     * @return total_award_integral
     */
    public Integer getTotalAwardIntegral() {
        return totalAwardIntegral;
    }

    /**
     * @param totalAwardIntegral
     */
    public void setTotalAwardIntegral(Integer totalAwardIntegral) {
        this.totalAwardIntegral = totalAwardIntegral;
    }

    @Override
    public String toString() {
        return "SignInAward{" +
            "id=" + id +
            ", userId=" + userId +
            ", newlySignInTime=" + newlySignInTime +
            ", signInDays=" + signInDays +
            ", totalAwardMoney=" + totalAwardMoney +
            ", totalAwardIntegral=" + totalAwardIntegral +
            '}';
    }
}