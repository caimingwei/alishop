package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "page_framework")
public class PageFramework {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "page_id")
    private Long pageId;

    private String title;

    private String illustration;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "is_default")
    private Integer isDefault;

    @Column(name = "is_current_use")
    private Integer isCurrentUse;

    @Column(name = "is_index")
    private Integer isIndex;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "is_update")
    private Integer isUpdate;

    @Column(name = "page_modules")
    private String pageModules;

    private String startColor;
    private String endColor;

    public String getStartColor() {
        return startColor;
    }

    public void setStartColor(String startColor) {
        this.startColor = startColor;
    }

    public String getEndColor() {
        return endColor;
    }

    public void setEndColor(String endColor) {
        this.endColor = endColor;
    }

    /**
     * @return page_id
     */
    public Long getPageId() {
        return pageId;
    }

    /**
     * @param pageId
     */
    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return illustration
     */
    public String getIllustration() {
        return illustration;
    }

    /**
     * @param illustration
     */
    public void setIllustration(String illustration) {
        this.illustration = illustration;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return is_default
     */
    public Integer getIsDefault() {
        return isDefault;
    }

    /**
     * @param isDefault
     */
    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * @return is_current_use
     */
    public Integer getIsCurrentUse() {
        return isCurrentUse;
    }

    /**
     * @param isCurrentUse
     */
    public void setIsCurrentUse(Integer isCurrentUse) {
        this.isCurrentUse = isCurrentUse;
    }

    /**
     * @return is_index
     */
    public Integer getIsIndex() {
        return isIndex;
    }

    /**
     * @param isIndex
     */
    public void setIsIndex(Integer isIndex) {
        this.isIndex = isIndex;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return is_update
     */
    public Integer getIsUpdate() {
        return isUpdate;
    }

    /**
     * @param isUpdate
     */
    public void setIsUpdate(Integer isUpdate) {
        this.isUpdate = isUpdate;
    }

    /**
     * @return page_modules
     */
    public String getPageModules() {
        return pageModules;
    }

    /**
     * @param pageModules
     */
    public void setPageModules(String pageModules) {
        this.pageModules = pageModules;
    }

    @Override
    public String toString() {
        return "PageFramework{" +
                "pageId=" + pageId +
                ", title='" + title + '\'' +
                ", illustration='" + illustration + '\'' +
                ", createTime=" + createTime +
                ", isDefault=" + isDefault +
                ", isCurrentUse=" + isCurrentUse +
                ", isIndex=" + isIndex +
                ", updateTime=" + updateTime +
                ", isUpdate=" + isUpdate +
                ", pageModules='" + pageModules + '\'' +
                ", startColor='" + startColor + '\'' +
                ", endColor='" + endColor + '\'' +
                '}';
    }
}