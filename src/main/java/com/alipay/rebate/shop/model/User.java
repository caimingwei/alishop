package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;

public class User {
    private Long userId;

    private String inviterCode;

    private Integer vipLevel;

    private Integer userStatus;

    private Date lastLoginTime;

    private Long relationId;

    private Long specialId;

    private Long parentUserId;

    private BigDecimal userAmount;

    private Long userIntgeralTreasure;

    private Integer straightFansNum;

    private Integer totalFansNum;

    private Integer totalFansOrdersNum;

    private Date createTime;

    private Date updateTime;

    private Integer userType;

    private BigDecimal userCommisionRate;

    private BigDecimal userCommision;

    private String userNickName;

    private String userAccount;

    private String userPassword;

    private String userHeadImage;

    private Date userRegisterTime;

    private String userRegisterIp;

    private String lastLoginIp;

    private String phone;

    private String alipayAccount;

    private String realName;

    private String openId;

    private BigDecimal userIncome;

    private String parentUserName;

    private Long userIntgeral;

    private Integer isAuth;

    private Long ordersNum;

    private String taobaoUserId;

    private String taobaoUserName;

    private Integer userGradeId;

    private String currentRegistrationId;

    private Integer loginPlatform;

    private String deviceModel;

    private UserGrade userGrade;

    private String remarks;

    private Date latelyActiveTime;

    private Integer privacyProtection;

    private String areaCode;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Integer getPrivacyProtection() {
        return privacyProtection;
    }

    public void setPrivacyProtection(Integer privacyProtection) {
        this.privacyProtection = privacyProtection;
    }

    public Date getLatelyActiveTime() {
        return latelyActiveTime;
    }

    public void setLatelyActiveTime(Date latelyActiveTime) {
        this.latelyActiveTime = latelyActiveTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getInviterCode() {
        return inviterCode;
    }

    public void setInviterCode(String inviterCode) {
        this.inviterCode = inviterCode == null ? null : inviterCode.trim();
    }

    public Integer getVipLevel() {
        return vipLevel;
    }

    public void setVipLevel(Integer vipLevel) {
        this.vipLevel = vipLevel;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getSpecialId() {
        return specialId;
    }

    public void setSpecialId(Long specialId) {
        this.specialId = specialId;
    }

    public Long getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Long parentUserId) {
        this.parentUserId = parentUserId;
    }

    public BigDecimal getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(BigDecimal userAmount) {
        this.userAmount = userAmount;
    }

    public Long getUserIntgeralTreasure() {
        return userIntgeralTreasure;
    }

    public void setUserIntgeralTreasure(Long userIntgeralTreasure) {
        this.userIntgeralTreasure = userIntgeralTreasure;
    }

    public Integer getStraightFansNum() {
        return straightFansNum;
    }

    public void setStraightFansNum(Integer straightFansNum) {
        this.straightFansNum = straightFansNum;
    }

    public Integer getTotalFansNum() {
        return totalFansNum;
    }

    public void setTotalFansNum(Integer totalFansNum) {
        this.totalFansNum = totalFansNum;
    }

    public Integer getTotalFansOrdersNum() {
        return totalFansOrdersNum;
    }

    public void setTotalFansOrdersNum(Integer totalFansOrdersNum) {
        this.totalFansOrdersNum = totalFansOrdersNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public BigDecimal getUserCommisionRate() {
        return userCommisionRate;
    }

    public void setUserCommisionRate(BigDecimal userCommisionRate) {
        this.userCommisionRate = userCommisionRate;
    }

    public BigDecimal getUserCommision() {
        return userCommision;
    }

    public void setUserCommision(BigDecimal userCommision) {
        this.userCommision = userCommision;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName == null ? null : userNickName.trim();
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount == null ? null : userAccount.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public String getUserHeadImage() {
        return userHeadImage;
    }

    public void setUserHeadImage(String userHeadImage) {
        this.userHeadImage = userHeadImage == null ? null : userHeadImage.trim();
    }

    public Date getUserRegisterTime() {
        return userRegisterTime;
    }

    public void setUserRegisterTime(Date userRegisterTime) {
        this.userRegisterTime = userRegisterTime;
    }

    public String getUserRegisterIp() {
        return userRegisterIp;
    }

    public void setUserRegisterIp(String userRegisterIp) {
        this.userRegisterIp = userRegisterIp == null ? null : userRegisterIp.trim();
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount == null ? null : alipayAccount.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    public BigDecimal getUserIncome() {
        return userIncome;
    }

    public void setUserIncome(BigDecimal userIncome) {
        this.userIncome = userIncome;
    }

    public String getParentUserName() {
        return parentUserName;
    }

    public void setParentUserName(String parentUserName) {
        this.parentUserName = parentUserName == null ? null : parentUserName.trim();
    }

    public Long getUserIntgeral() {
        return userIntgeral;
    }

    public void setUserIntgeral(Long userIntgeral) {
        this.userIntgeral = userIntgeral;
    }

    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    public Long getOrdersNum() {
        return ordersNum;
    }

    public void setOrdersNum(Long ordersNum) {
        this.ordersNum = ordersNum;
    }

    public String getTaobaoUserId() {
        return taobaoUserId;
    }

    public void setTaobaoUserId(String taobaoUserId) {
        this.taobaoUserId = taobaoUserId;
    }

    public String getTaobaoUserName() {
        return taobaoUserName;
    }

    public void setTaobaoUserName(String taobaoUserName) {
        this.taobaoUserName = taobaoUserName;
    }

    public Integer getUserGradeId() {
        return userGradeId;
    }

    public void setUserGradeId(Integer userGradeId) {
        this.userGradeId = userGradeId;
    }

    public String getCurrentRegistrationId() {
        return currentRegistrationId;
    }

    public void setCurrentRegistrationId(String currentRegistrationId) {
        this.currentRegistrationId = currentRegistrationId;
    }

    public Integer getLoginPlatform() {
        return loginPlatform;
    }

    public void setLoginPlatform(Integer loginPlatform) {
        this.loginPlatform = loginPlatform;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public UserGrade getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(UserGrade userGrade) {
        this.userGrade = userGrade;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", inviterCode='" + inviterCode + '\'' +
                ", vipLevel=" + vipLevel +
                ", userStatus=" + userStatus +
                ", lastLoginTime=" + lastLoginTime +
                ", relationId=" + relationId +
                ", specialId=" + specialId +
                ", parentUserId=" + parentUserId +
                ", userAmount=" + userAmount +
                ", userIntgeralTreasure=" + userIntgeralTreasure +
                ", straightFansNum=" + straightFansNum +
                ", totalFansNum=" + totalFansNum +
                ", totalFansOrdersNum=" + totalFansOrdersNum +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", userType=" + userType +
                ", userCommisionRate=" + userCommisionRate +
                ", userCommision=" + userCommision +
                ", userNickName='" + userNickName + '\'' +
                ", userAccount='" + userAccount + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userHeadImage='" + userHeadImage + '\'' +
                ", userRegisterTime=" + userRegisterTime +
                ", userRegisterIp='" + userRegisterIp + '\'' +
                ", lastLoginIp='" + lastLoginIp + '\'' +
                ", phone='" + phone + '\'' +
                ", alipayAccount='" + alipayAccount + '\'' +
                ", realName='" + realName + '\'' +
                ", openId='" + openId + '\'' +
                ", userIncome=" + userIncome +
                ", parentUserName='" + parentUserName + '\'' +
                ", userIntgeral=" + userIntgeral +
                ", isAuth=" + isAuth +
                ", ordersNum=" + ordersNum +
                ", taobaoUserId='" + taobaoUserId + '\'' +
                ", taobaoUserName='" + taobaoUserName + '\'' +
                ", userGradeId=" + userGradeId +
                ", currentRegistrationId='" + currentRegistrationId + '\'' +
                ", loginPlatform=" + loginPlatform +
                ", deviceModel='" + deviceModel + '\'' +
                ", userGrade=" + userGrade +
                ", remarks='" + remarks + '\'' +
                ", latelyActiveTime=" + latelyActiveTime +
                ", privacyProtection=" + privacyProtection +
                '}';
    }
}