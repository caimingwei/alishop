package com.alipay.rebate.shop.model;

import java.math.BigDecimal;

public class Orders {
    private String tradeId;

    private String tradeParentId;

    private Long numIid;

    private String itemTitle;

    private Long itemNum;

    private BigDecimal price;

    private BigDecimal payPrice;

    private String sellerNick;

    private String sellerShopTitle;

    private BigDecimal commission;

    private BigDecimal commissionRate;

    private String unid;

    private String createTime;

    private String earningTime;

    private Long tkStatus;

    private String tk3rdType;

    private Long tk3rdPubId;

    private String orderType;

    private BigDecimal incomeRate;

    private BigDecimal pubSharePreFee;

    private BigDecimal subsidyRate;

    private String subsidyType;

    private String terminalType;

    private String auctionCategory;

    private Long siteId;

    private String siteName;

    private Long adzoneId;

    private String adzoneName;

    private BigDecimal alipayTotalPrice;

    private BigDecimal totalCommissionRate;

    private BigDecimal totalCommissionFee;

    private BigDecimal subsidyFee;

    private Long relationId;

    private Long specialId;

    private String clickTime;

    private String tkPaidTime;

    private String tbPaidTime;

    private BigDecimal pubShareFee;

    private Long tkOrderRole;

    private BigDecimal pubShareRate;

    private Long refundTag;

    private BigDecimal tkTotalRate;

    private BigDecimal alimamaRate;

    private BigDecimal alimamaShareFee;

    private String flowSource;

    private String itemLink;

    private BigDecimal userCommission;

    private BigDecimal oneLevelCommission;

    private BigDecimal twoLevelCommission;

    private Long userId;

    private String userName;

    private Long promoterId;

    private String promoterName;

    private String itemImg;

    private Long userUit;

    private Long oneLevelUserUit;

    private Long twoLevelUserUit;

    private Integer activityId;

    private Integer isRecord;

    private Long customStatus;

    private Integer refundStatus;

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Long getCustomStatus() {
        return customStatus;
    }

    public void setCustomStatus(Long customStatus) {
        this.customStatus = customStatus;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId == null ? null : tradeId.trim();
    }

    public String getTradeParentId() {
        return tradeParentId;
    }

    public void setTradeParentId(String tradeParentId) {
        this.tradeParentId = tradeParentId == null ? null : tradeParentId.trim();
    }

    public Long getNumIid() {
        return numIid;
    }

    public void setNumIid(Long numIid) {
        this.numIid = numIid;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle == null ? null : itemTitle.trim();
    }

    public Long getItemNum() {
        return itemNum;
    }

    public void setItemNum(Long itemNum) {
        this.itemNum = itemNum;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    public String getSellerNick() {
        return sellerNick;
    }

    public void setSellerNick(String sellerNick) {
        this.sellerNick = sellerNick == null ? null : sellerNick.trim();
    }

    public String getSellerShopTitle() {
        return sellerShopTitle;
    }

    public void setSellerShopTitle(String sellerShopTitle) {
        this.sellerShopTitle = sellerShopTitle == null ? null : sellerShopTitle.trim();
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    public String getUnid() {
        return unid;
    }

    public void setUnid(String unid) {
        this.unid = unid == null ? null : unid.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getEarningTime() {
        return earningTime;
    }

    public void setEarningTime(String earningTime) {
        this.earningTime = earningTime == null ? null : earningTime.trim();
    }

    public Long getTkStatus() {
        return tkStatus;
    }

    public void setTkStatus(Long tkStatus) {
        this.tkStatus = tkStatus;
    }

    public String getTk3rdType() {
        return tk3rdType;
    }

    public void setTk3rdType(String tk3rdType) {
        this.tk3rdType = tk3rdType == null ? null : tk3rdType.trim();
    }

    public Long getTk3rdPubId() {
        return tk3rdPubId;
    }

    public void setTk3rdPubId(Long tk3rdPubId) {
        this.tk3rdPubId = tk3rdPubId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType == null ? null : orderType.trim();
    }

    public BigDecimal getIncomeRate() {
        return incomeRate;
    }

    public void setIncomeRate(BigDecimal incomeRate) {
        this.incomeRate = incomeRate;
    }

    public BigDecimal getPubSharePreFee() {
        return pubSharePreFee;
    }

    public void setPubSharePreFee(BigDecimal pubSharePreFee) {
        this.pubSharePreFee = pubSharePreFee;
    }

    public BigDecimal getSubsidyRate() {
        return subsidyRate;
    }

    public void setSubsidyRate(BigDecimal subsidyRate) {
        this.subsidyRate = subsidyRate;
    }

    public String getSubsidyType() {
        return subsidyType;
    }

    public void setSubsidyType(String subsidyType) {
        this.subsidyType = subsidyType == null ? null : subsidyType.trim();
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType == null ? null : terminalType.trim();
    }

    public String getAuctionCategory() {
        return auctionCategory;
    }

    public void setAuctionCategory(String auctionCategory) {
        this.auctionCategory = auctionCategory == null ? null : auctionCategory.trim();
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName == null ? null : siteName.trim();
    }

    public Long getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(Long adzoneId) {
        this.adzoneId = adzoneId;
    }

    public String getAdzoneName() {
        return adzoneName;
    }

    public void setAdzoneName(String adzoneName) {
        this.adzoneName = adzoneName == null ? null : adzoneName.trim();
    }

    public BigDecimal getAlipayTotalPrice() {
        return alipayTotalPrice;
    }

    public void setAlipayTotalPrice(BigDecimal alipayTotalPrice) {
        this.alipayTotalPrice = alipayTotalPrice;
    }

    public BigDecimal getTotalCommissionRate() {
        return totalCommissionRate;
    }

    public void setTotalCommissionRate(BigDecimal totalCommissionRate) {
        this.totalCommissionRate = totalCommissionRate;
    }

    public BigDecimal getTotalCommissionFee() {
        return totalCommissionFee;
    }

    public void setTotalCommissionFee(BigDecimal totalCommissionFee) {
        this.totalCommissionFee = totalCommissionFee;
    }

    public BigDecimal getSubsidyFee() {
        return subsidyFee;
    }

    public void setSubsidyFee(BigDecimal subsidyFee) {
        this.subsidyFee = subsidyFee;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getSpecialId() {
        return specialId;
    }

    public void setSpecialId(Long specialId) {
        this.specialId = specialId;
    }

    public String getClickTime() {
        return clickTime;
    }

    public void setClickTime(String clickTime) {
        this.clickTime = clickTime == null ? null : clickTime.trim();
    }

    public String getTkPaidTime() {
        return tkPaidTime;
    }

    public void setTkPaidTime(String tkPaidTime) {
        this.tkPaidTime = tkPaidTime == null ? null : tkPaidTime.trim();
    }

    public String getTbPaidTime() {
        return tbPaidTime;
    }

    public void setTbPaidTime(String tbPaidTime) {
        this.tbPaidTime = tbPaidTime == null ? null : tbPaidTime.trim();
    }

    public BigDecimal getPubShareFee() {
        return pubShareFee;
    }

    public void setPubShareFee(BigDecimal pubShareFee) {
        this.pubShareFee = pubShareFee;
    }

    public Long getTkOrderRole() {
        return tkOrderRole;
    }

    public void setTkOrderRole(Long tkOrderRole) {
        this.tkOrderRole = tkOrderRole;
    }

    public BigDecimal getPubShareRate() {
        return pubShareRate;
    }

    public void setPubShareRate(BigDecimal pubShareRate) {
        this.pubShareRate = pubShareRate;
    }

    public Long getRefundTag() {
        return refundTag;
    }

    public void setRefundTag(Long refundTag) {
        this.refundTag = refundTag;
    }

    public BigDecimal getTkTotalRate() {
        return tkTotalRate;
    }

    public void setTkTotalRate(BigDecimal tkTotalRate) {
        this.tkTotalRate = tkTotalRate;
    }

    public BigDecimal getAlimamaRate() {
        return alimamaRate;
    }

    public void setAlimamaRate(BigDecimal alimamaRate) {
        this.alimamaRate = alimamaRate;
    }

    public BigDecimal getAlimamaShareFee() {
        return alimamaShareFee;
    }

    public void setAlimamaShareFee(BigDecimal alimamaShareFee) {
        this.alimamaShareFee = alimamaShareFee;
    }

    public String getFlowSource() {
        return flowSource;
    }

    public void setFlowSource(String flowSource) {
        this.flowSource = flowSource == null ? null : flowSource.trim();
    }

    public String getItemLink() {
        return itemLink;
    }

    public void setItemLink(String itemLink) {
        this.itemLink = itemLink == null ? null : itemLink.trim();
    }

    public BigDecimal getUserCommission() {
        return userCommission;
    }

    public void setUserCommission(BigDecimal userCommission) {
        this.userCommission = userCommission;
    }

    public BigDecimal getOneLevelCommission() {
        return oneLevelCommission;
    }

    public void setOneLevelCommission(BigDecimal oneLevelCommission) {
        this.oneLevelCommission = oneLevelCommission;
    }

    public BigDecimal getTwoLevelCommission() {
        return twoLevelCommission;
    }

    public void setTwoLevelCommission(BigDecimal twoLevelCommission) {
        this.twoLevelCommission = twoLevelCommission;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Long getPromoterId() {
        return promoterId;
    }

    public void setPromoterId(Long promoterId) {
        this.promoterId = promoterId;
    }

    public String getPromoterName() {
        return promoterName;
    }

    public void setPromoterName(String promoterName) {
        this.promoterName = promoterName == null ? null : promoterName.trim();
    }

    public String getItemImg() {
        return itemImg;
    }

    public void setItemImg(String itemImg) {
        this.itemImg = itemImg == null ? null : itemImg.trim();
    }

    public Long getUserUit() {
        return userUit;
    }

    public void setUserUit(Long userUit) {
        this.userUit = userUit;
    }

    public Long getOneLevelUserUit() {
        return oneLevelUserUit;
    }

    public void setOneLevelUserUit(Long oneLevelUserUit) {
        this.oneLevelUserUit = oneLevelUserUit;
    }

    public Long getTwoLevelUserUit() {
        return twoLevelUserUit;
    }

    public void setTwoLevelUserUit(Long twoLevelUserUit) {
        this.twoLevelUserUit = twoLevelUserUit;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getIsRecord() {
        return isRecord;
    }

    public void setIsRecord(Integer isRecord) {
        this.isRecord = isRecord;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "tradeId='" + tradeId + '\'' +
                ", tradeParentId='" + tradeParentId + '\'' +
                ", numIid=" + numIid +
                ", itemTitle='" + itemTitle + '\'' +
                ", itemNum=" + itemNum +
                ", price=" + price +
                ", payPrice=" + payPrice +
                ", sellerNick='" + sellerNick + '\'' +
                ", sellerShopTitle='" + sellerShopTitle + '\'' +
                ", commission=" + commission +
                ", commissionRate=" + commissionRate +
                ", unid='" + unid + '\'' +
                ", createTime='" + createTime + '\'' +
                ", earningTime='" + earningTime + '\'' +
                ", tkStatus=" + tkStatus +
                ", tk3rdType='" + tk3rdType + '\'' +
                ", tk3rdPubId=" + tk3rdPubId +
                ", orderType='" + orderType + '\'' +
                ", incomeRate=" + incomeRate +
                ", pubSharePreFee=" + pubSharePreFee +
                ", subsidyRate=" + subsidyRate +
                ", subsidyType='" + subsidyType + '\'' +
                ", terminalType='" + terminalType + '\'' +
                ", auctionCategory='" + auctionCategory + '\'' +
                ", siteId=" + siteId +
                ", siteName='" + siteName + '\'' +
                ", adzoneId=" + adzoneId +
                ", adzoneName='" + adzoneName + '\'' +
                ", alipayTotalPrice=" + alipayTotalPrice +
                ", totalCommissionRate=" + totalCommissionRate +
                ", totalCommissionFee=" + totalCommissionFee +
                ", subsidyFee=" + subsidyFee +
                ", relationId=" + relationId +
                ", specialId=" + specialId +
                ", clickTime='" + clickTime + '\'' +
                ", tkPaidTime='" + tkPaidTime + '\'' +
                ", tbPaidTime='" + tbPaidTime + '\'' +
                ", pubShareFee=" + pubShareFee +
                ", tkOrderRole=" + tkOrderRole +
                ", pubShareRate=" + pubShareRate +
                ", refundTag=" + refundTag +
                ", tkTotalRate=" + tkTotalRate +
                ", alimamaRate=" + alimamaRate +
                ", alimamaShareFee=" + alimamaShareFee +
                ", flowSource='" + flowSource + '\'' +
                ", itemLink='" + itemLink + '\'' +
                ", userCommission=" + userCommission +
                ", oneLevelCommission=" + oneLevelCommission +
                ", twoLevelCommission=" + twoLevelCommission +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", promoterId=" + promoterId +
                ", promoterName='" + promoterName + '\'' +
                ", itemImg='" + itemImg + '\'' +
                ", userUit=" + userUit +
                ", oneLevelUserUit=" + oneLevelUserUit +
                ", twoLevelUserUit=" + twoLevelUserUit +
                ", activityId=" + activityId +
                ", isRecord=" + isRecord +
                ", customStatus=" + customStatus +
                ", refundStatus=" + refundStatus +
                '}';
    }
}