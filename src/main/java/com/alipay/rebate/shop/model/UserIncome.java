package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "user_income")
public class UserIncome {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_income_id")
    @Id
    private Long userIncomeId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    private BigDecimal money;

    @Column(name = "intgeral_num")
    private Long intgeralNum;

    /**
     * 渠道id
     */
    @Column(name = "relation_id")
    private Long relationId;

    /**
     * 会员运营id
     */
    @Column(name = "special_id")
    private Long specialId;

    /**
     * 订单号
     */
    @Column(name = "trade_id")
    private String tradeId;

    /**
     * 父订单号
     */
    @Column(name = "parent_trade_id")
    private String parentTradeId;

    /**
     * 收益类型 1:订单返利 2：阅读奖励 3：拉新 4：签到
     */
    private Integer type;

    /**
     * 订单状态
     */
    @Column(name = "tk_status")
    private Long tkStatus;

    private Integer status;

    /**
     * 奖励类型 1:金额 2:集分宝 3:积分
     */
    @Column(name = "award_type")
    private Integer awardType;

    @Column(name = "create_time")
    private String createTime;

    @Column(name = "update_time")
    private String updateTime;

    private Integer roadType;

    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getRoadType() {
        return roadType;
    }

    public void setRoadType(Integer roadType) {
        this.roadType = roadType;
    }

    /**
     * @return user_income_id
     */
    public Long getUserIncomeId() {
        return userIncomeId;
    }

    /**
     * @param userIncomeId
     */
    public void setUserIncomeId(Long userIncomeId) {
        this.userIncomeId = userIncomeId;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return money
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * @param money
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * @return intgeral_num
     */
    public Long getIntgeralNum() {
        return intgeralNum;
    }

    /**
     * @param intgeralNum
     */
    public void setIntgeralNum(Long intgeralNum) {
        this.intgeralNum = intgeralNum;
    }

    /**
     * 获取渠道id
     *
     * @return relation_id - 渠道id
     */
    public Long getRelationId() {
        return relationId;
    }

    /**
     * 设置渠道id
     *
     * @param relationId 渠道id
     */
    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    /**
     * 获取会员运营id
     *
     * @return special_id - 会员运营id
     */
    public Long getSpecialId() {
        return specialId;
    }

    /**
     * 设置会员运营id
     *
     * @param specialId 会员运营id
     */
    public void setSpecialId(Long specialId) {
        this.specialId = specialId;
    }

    /**
     * 获取订单号
     *
     * @return trade_id - 订单号
     */
    public String getTradeId() {
        return tradeId;
    }

    /**
     * 设置订单号
     *
     * @param tradeId 订单号
     */
    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    /**
     * 获取父订单号
     *
     * @return parent_trade_id - 父订单号
     */
    public String getParentTradeId() {
        return parentTradeId;
    }

    /**
     * 设置父订单号
     *
     * @param parentTradeId 父订单号
     */
    public void setParentTradeId(String parentTradeId) {
        this.parentTradeId = parentTradeId;
    }

    /**
     * 获取收益类型 1:订单返利 2：阅读奖励 3：拉新 4：签到
     *
     * @return type - 收益类型 1:订单返利 2：阅读奖励 3：拉新 4：签到
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置收益类型 1:订单返利 2：阅读奖励 3：拉新 4：签到
     *
     * @param type 收益类型 1:订单返利 2：阅读奖励 3：拉新 4：签到
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取订单状态
     *
     * @return tk_status - 订单状态
     */
    public Long getTkStatus() {
        return tkStatus;
    }

    /**
     * 设置订单状态
     *
     * @param tkStatus 订单状态
     */
    public void setTkStatus(Long tkStatus) {
        this.tkStatus = tkStatus;
    }

    /**
     * @return status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取奖励类型 1:金额 2:集分宝 3:积分
     *
     * @return award_type - 奖励类型 1:金额 2:集分宝 3:积分
     */
    public Integer getAwardType() {
        return awardType;
    }

    /**
     * 设置奖励类型 1:金额 2:集分宝 3:积分
     *
     * @param awardType 奖励类型 1:金额 2:集分宝 3:积分
     */
    public void setAwardType(Integer awardType) {
        this.awardType = awardType;
    }

    /**
     * @return create_time
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "UserIncome{" +
                "userIncomeId=" + userIncomeId +
                ", userId=" + userId +
                ", money=" + money +
                ", intgeralNum=" + intgeralNum +
                ", relationId=" + relationId +
                ", specialId=" + specialId +
                ", tradeId='" + tradeId + '\'' +
                ", parentTradeId='" + parentTradeId + '\'' +
                ", type=" + type +
                ", tkStatus=" + tkStatus +
                ", status=" + status +
                ", awardType=" + awardType +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", roadType=" + roadType +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}