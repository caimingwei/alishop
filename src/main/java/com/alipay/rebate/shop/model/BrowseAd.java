package com.alipay.rebate.shop.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "browse_ad")
public class BrowseAd {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "min_setting")
    private Integer minSetting;

    @Column(name = "award_type")
    private Integer awardType;

    @Column(name = "award_num")
    private Integer awardNum;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return min_setting
     */
    public Integer getMinSetting() {
        return minSetting;
    }

    /**
     * @param minSetting
     */
    public void setMinSetting(Integer minSetting) {
        this.minSetting = minSetting;
    }

    /**
     * @return award_type
     */
    public Integer getAwardType() {
        return awardType;
    }

    /**
     * @param awardType
     */
    public void setAwardType(Integer awardType) {
        this.awardType = awardType;
    }

    /**
     * @return award_num
     */
    public Integer getAwardNum() {
        return awardNum;
    }

    /**
     * @param awardNum
     */
    public void setAwardNum(Integer awardNum) {
        this.awardNum = awardNum;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}