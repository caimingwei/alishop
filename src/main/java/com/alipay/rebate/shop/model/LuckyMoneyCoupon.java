package com.alipay.rebate.shop.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "lucky_money_coupon")
public class LuckyMoneyCoupon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Column(name = "amount_of_money")
    private BigDecimal amountOfMoney;

    @Column(name = "condition_money")
    private BigDecimal conditionMoney;

    @Column(name = "condition_type")
    private Integer conditionType;

    @Column(name = "equalIntegral")
    private Integer equalintegral;

    private Integer stock;

    @Column(name = "limit_num")
    private Integer limitNum;

    private Integer type;

    @Column(name = "is_current_use")
    private Integer isCurrentUse;

    @Column(name = "can_edit")
    private Integer canEdit;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    private String remark;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return amount_of_money
     */
    public BigDecimal getAmountOfMoney() {
        return amountOfMoney;
    }

    /**
     * @param amountOfMoney
     */
    public void setAmountOfMoney(BigDecimal amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    /**
     * @return condition_money
     */
    public BigDecimal getConditionMoney() {
        return conditionMoney;
    }

    /**
     * @param conditionMoney
     */
    public void setConditionMoney(BigDecimal conditionMoney) {
        this.conditionMoney = conditionMoney;
    }

    /**
     * @return condition_type
     */
    public Integer getConditionType() {
        return conditionType;
    }

    /**
     * @param conditionType
     */
    public void setConditionType(Integer conditionType) {
        this.conditionType = conditionType;
    }

    /**
     * @return equalIntegral
     */
    public Integer getEqualintegral() {
        return equalintegral;
    }

    /**
     * @param equalintegral
     */
    public void setEqualintegral(Integer equalintegral) {
        this.equalintegral = equalintegral;
    }

    /**
     * @return stock
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * @param stock
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * @return limit_num
     */
    public Integer getLimitNum() {
        return limitNum;
    }

    /**
     * @param limitNum
     */
    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    /**
     * @return type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return is_current_use
     */
    public Integer getIsCurrentUse() {
        return isCurrentUse;
    }

    /**
     * @param isCurrentUse
     */
    public void setIsCurrentUse(Integer isCurrentUse) {
        this.isCurrentUse = isCurrentUse;
    }

    /**
     * @return can_edit
     */
    public Integer getCanEdit() {
        return canEdit;
    }

    /**
     * @param canEdit
     */
    public void setCanEdit(Integer canEdit) {
        this.canEdit = canEdit;
    }

    /**
     * @return start_time
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return end_time
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}