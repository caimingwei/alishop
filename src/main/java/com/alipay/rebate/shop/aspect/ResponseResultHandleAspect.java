package com.alipay.rebate.shop.aspect;

import com.alipay.rebate.shop.constants.StatusCodeMapper;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

@Aspect
@Component
public class ResponseResultHandleAspect {

    private Logger logger = LoggerFactory.getLogger(ResponseResultHandleAspect.class);

    @Pointcut("execution(public * com.alipay.rebate.shop.controller.*.*.*(..))")
    public void responseResultTime(){

    }

    @AfterReturning(returning = "res", pointcut = "responseResultTime()")
    public void doAfterReturning(Object res) throws Throwable {
        // 处理完请求，返回内容
        logger.trace("returning value : {} ", res);
        if(res instanceof ResponseResult){
            logger.debug("handle time : {} ", res);
            ResponseResult resp = (ResponseResult) res;
            resp.setTime(new Date().getTime());
            int code = resp.getStatus();
            String msg = StatusCodeMapper.getMsg(code);
            if(msg != null && !StringUtils.isEmpty(resp.getMsg())){
                resp.setMsg(msg);
            }
        }
    }
}
