package com.alipay.rebate.shop.filter.shiro;

import com.alipay.rebate.shop.configuration.shiro.token.JWTToken;
import com.alipay.rebate.shop.constants.StatusCodeMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.utils.JwtUtils;
import com.alipay.rebate.shop.constants.CommonConstant;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import com.alipay.rebate.shop.service.common.UserService;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public abstract class AbstractJwtAuthFilter extends AuthenticatingFilter {
	private final Logger logger = LoggerFactory.getLogger(AbstractJwtAuthFilter.class);
	
    private static final int tokenRefreshInterval = 30000000;
    private UserService userService;

    public AbstractJwtAuthFilter(UserService userService){
        this.userService = userService;
    }

    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        logger.debug("JwtAuthFilter preHandle method");
        HttpServletRequest httpServletRequest = WebUtils.toHttp(request);
        logger.debug("request method is : {}",httpServletRequest.getMethod());
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) //对于OPTION请求做拦截，不做token校验
            return false;

        return super.preHandle(request, response);
    }

    @Override
    protected void postHandle(ServletRequest request, ServletResponse response){
        logger.debug("JwtAuthFilter postHandle method");
        this.fillCorsHeader(WebUtils.toHttp(request), WebUtils.toHttp(response));
        request.setAttribute("jwtShiroFilter.FILTERED", true);
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if(this.isLoginRequest(request, response))
            return true;
        logger.debug("JwtAuthFilter isAccessAllowed method");
        Boolean afterFiltered = (Boolean)(request.getAttribute("jwtShiroFilter.FILTERED"));
        logger.debug("Get into isAccessAllowed method");
        if( BooleanUtils.isTrue(afterFiltered))
        	return true;

        boolean allowed = false;
        try {
            logger.debug("executeLogin method");
            allowed = executeLogin(request, response);
        } catch(IllegalStateException e){ //not found any token
            logger.error("Not found any token");
        }catch (Exception e) {
            logger.error("Error occurs when login", e);
        }
        return allowed || super.isPermissive(mappedValue);
    }

    @Override
    protected AuthenticationToken createToken(ServletRequest servletRequest, ServletResponse servletResponse) {
        String jwtToken = getAuthzHeader(servletRequest);
        logger.debug("JwtAuthFilter createToken method");
        return new JWTToken(jwtToken);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        return false;
    }

    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response) throws Exception {
        logger.debug("JwtAuthFilter onLoginSuccess method");
        if(token instanceof JWTToken){
            JWTToken jwtToken = (JWTToken)token;
            logger.debug("JWTToken is: {}",jwtToken);
            UserDto user = (UserDto) subject.getPrincipal();
            logger.debug("UserDto is: {}",user);
            boolean shouldRefresh = shouldTokenRefresh(JwtUtils.getIssuedAt(jwtToken.getToken()));
            logger.debug("if token should Refresh is: {}",shouldRefresh);
        }
        return true;
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request, ServletResponse response) {
        logger.error("Validate token fail, token:{}, error:{}", token.toString(), e.getCause().getMessage());
        if(e.getCause() != null && e.getCause() instanceof BusinessException){
            try {
                BusinessException ex = (BusinessException) e.getCause();
                HttpServletResponse httpResponse = WebUtils.toHttp(response);
                logger.debug("JwtAuthFilter onAccessDenied method");
                httpResponse.setCharacterEncoding("UTF-8");
                httpResponse.setContentType("application/json;charset=UTF-8");
                ResponseResult responseResult = new ResponseResult();
                responseResult.setStatus(ex.getStatusCode().getCode());
                responseResult.setMsg(StatusCodeMapper.getMsg(responseResult.getStatus()));
                responseResult.setTime(System.currentTimeMillis());
                httpResponse.getWriter().print(new GsonBuilder().create().toJson(responseResult));
                fillCorsHeader(WebUtils.toHttp(request), httpResponse);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return false;
    }

    protected String getAuthzHeader(ServletRequest request) {
        HttpServletRequest httpRequest = WebUtils.toHttp(request);
        String header = httpRequest.getHeader(CommonConstant.ACCESS_HEADER_KEY);
        if(StringUtils.isEmpty(header) || header.contains("undefined")){
            header = httpRequest.getHeader(CommonConstant.AUTH_HEADER_KEY);
        }
        logger.debug("token is : {}",header);
        return StringUtils.removeStart(header, "Bearer ");
    }

    protected boolean shouldTokenRefresh(Date issueAt){
        LocalDateTime issueTime = LocalDateTime.ofInstant(issueAt.toInstant(), ZoneId.systemDefault());
        return LocalDateTime.now().minusSeconds(tokenRefreshInterval).isAfter(issueTime);
    }

    protected void fillCorsHeader(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,HEAD,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
    }
}
