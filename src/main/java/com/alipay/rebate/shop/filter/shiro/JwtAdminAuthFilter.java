package com.alipay.rebate.shop.filter.shiro;

import com.alipay.rebate.shop.service.common.UserService;

public class JwtAdminAuthFilter extends AbstractJwtAuthFilter {
    public JwtAdminAuthFilter(UserService userService) {
        super(userService);
        this.setLoginUrl("/adminLogin");
    }
}
