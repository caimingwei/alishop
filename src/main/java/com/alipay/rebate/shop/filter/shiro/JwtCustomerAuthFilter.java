package com.alipay.rebate.shop.filter.shiro;

import com.alipay.rebate.shop.service.common.UserService;

public class JwtCustomerAuthFilter extends AbstractJwtAuthFilter {
    public JwtCustomerAuthFilter(UserService userService) {
        super(userService);
        this.setLoginUrl("/login");
    }
}
