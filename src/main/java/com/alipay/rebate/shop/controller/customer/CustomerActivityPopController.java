package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.ActivityPop;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerActivityPopService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/")
public class CustomerActivityPopController {

  @Autowired
  CustomerActivityPopService activityPopService;

  @GetMapping("/activityPopList")
  public ResponseResult<List<ActivityPop>> activityPopList() {
    List<ActivityPop> list = activityPopService.findAll();
    return ResultGenerator.genSuccessResponseResult(list);
  }

}
