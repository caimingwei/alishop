package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.UserStatisticalMapper;
import com.alipay.rebate.shop.model.UserStatistical;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.UserStatisticalService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/05/27.
*/
@RestController
@RequestMapping("/admin/userStatistical")
public class UserStatisticalController {
    @Resource
    private UserStatisticalService userStatisticalService;

//    @PostMapping("/addOrUpdateUserStatistical")
//    @ResponseBody
//    public ResponseResult<Integer> addOrUpdateUserStatistical(){
//        int result = userStatisticalService.addOrUpdateUserStatistical();
//        return ResultGenerator.genSuccessResponseResult(result);
//    }
    @Autowired
    private UserStatisticalMapper userStatisticalMapper;

    @GetMapping("/list")
    public ResponseResult<List<UserStatistical>> list() {
        List<UserStatistical> userStatistical = userStatisticalMapper.getUserStatistical();
        return ResultGenerator.genSuccessResponseResult(userStatistical);
    }
}
