package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.PddOrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserJdPidMapper;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.model.UserJdPid;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import com.alipay.rebate.shop.pojo.user.customer.PddOrdersListRes;
import com.alipay.rebate.shop.service.admin.PddOrdersService;
import com.alipay.rebate.shop.service.customer.CustomerPddOrdersService;
import com.alipay.rebate.shop.service.customer.impl.CustomerPddOrdersServiceImpl;
import com.alipay.rebate.shop.utils.JwtUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* Created by CodeGenerator on 2020/05/12.
*/
@RestController
@RequestMapping("/customer/pddOrders")
public class CustomerPddOrdersController {

    private Logger logger = LoggerFactory.getLogger(CustomerPddOrdersController.class);

    @Resource
    private CustomerPddOrdersService pddOrdersService;


    @Autowired
    private UserJdPidMapper userJdPidMapper;

    @GetMapping("/selectUserOrdersByTkStatus")
    @ResponseBody
    public ResponseResult<PageInfo<OrdersListResponse>> selectUserOrdersByType(
            Integer orderStatus,
            Integer pageNo,
            Integer pageSize,
            HttpServletRequest request
    ){
        logger.debug("tkStatus , pageNo, pageSize : {}, {}, {}", orderStatus,pageNo,pageSize);
        long userId = JwtUtils.getUserIdFromToken(request);



        PageInfo<OrdersListResponse> pageInfo = pddOrdersService.selectUserOrdersByType(orderStatus, pageNo, pageSize, userId);

        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

}
