package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.JdOrdersMapper;
import com.alipay.rebate.shop.model.JdOrders;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.JdOrdersReq;
import com.alipay.rebate.shop.service.admin.JdOrdersService;
import com.alipay.rebate.shop.utils.DateUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/05/05.
*/
@RestController
@RequestMapping("/admin/jdOrders")
public class JdOrdersController {
    @Resource
    private JdOrdersService jdOrdersService;
    @Resource
    private JdOrdersMapper jdOrdersMapper;

    @PostMapping("/add")
    public Result add(JdOrders jdOrders) {
        jdOrdersService.save(jdOrders);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        jdOrdersService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping("/update")
    public Result update(JdOrders jdOrders) {
        jdOrdersService.update(jdOrders);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        JdOrders jdOrders = jdOrdersService.findById(id);
        return ResultGenerator.genSuccessResult(jdOrders);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<JdOrders>> list(JdOrdersReq req, @RequestParam(defaultValue = "1") Integer pageNo,
                                                   @RequestParam(defaultValue = "20") Integer pageSize) {

        PageInfo<JdOrders> pageInfo = jdOrdersService.selectOrdersByCondition(req, pageNo, pageSize);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/earningOrderList")
    public ResponseResult earningOrderList(String date, @RequestParam(defaultValue = "1") Integer page,
                                                   @RequestParam(defaultValue = "20") Integer size) {
        PageHelper.startPage(page,size);
        List<JdOrders> jdOrders = jdOrdersMapper.selectJdOrdersByDate(date);
        PageInfo<JdOrders> pageInfo = new PageInfo<>(jdOrders);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
