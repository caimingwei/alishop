package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.helper.HtmlClickHelper;
import com.alipay.rebate.shop.model.Extra;
import com.alipay.rebate.shop.model.HtmlClick;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.HtmlClickService;
import com.alipay.rebate.shop.utils.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
* Created by CodeGenerator on 2020/05/04.
*/
@RestController
@RequestMapping("/admin/htmlClick")
public class HtmlClickController {
    @Resource
    private HtmlClickService htmlClickService;

    @Autowired
    HtmlClickHelper htmlClickHelper;

    @DeleteMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        htmlClickService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping("/update")
    public Result update(HtmlClick htmlClick) {
        htmlClickService.update(htmlClick);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        HtmlClick htmlClick = htmlClickService.findById(id);
        return ResultGenerator.genSuccessResult(htmlClick);
    }

    @GetMapping("/list")
    public ResponseResult<List<HtmlClick>> list() {
        Date nowDate = DateUtil.getStartTimeOfToday();
        List<HtmlClick> htmlClick = htmlClickHelper.getHtmlClick(nowDate);
        return ResultGenerator.genSuccessResponseResult(htmlClick);
    }







}
