package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.AppSettings;
import com.alipay.rebate.shop.pojo.appsettings.CusAppSettingsRsp;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerAppSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/appSettings")
public class CustomerAppSettingsController {

  @Autowired
  private CustomerAppSettingsService appSettingsService;

  @GetMapping("/detail")
  public ResponseResult<CusAppSettingsRsp> detail() {
    AppSettings appSettings = appSettingsService.findById(1);
    CusAppSettingsRsp rsp = convert2CusAppSettingsRsp(appSettings);
    return ResultGenerator.genSuccessResponseResult(rsp);
  }

  private CusAppSettingsRsp convert2CusAppSettingsRsp(AppSettings appSettings){
    CusAppSettingsRsp rsp = new CusAppSettingsRsp();
    rsp.setSpDomainType(appSettings.getSpDomainType());
    rsp.setSpDomain(appSettings.getSpDomain());
    return rsp;
  }
}
