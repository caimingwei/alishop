package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.SignInSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.entitysettings.IncomeRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.InviteNumRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.PcfSettings;
import com.alipay.rebate.shop.pojo.entitysettings.WithDrawSettings;
import com.alipay.rebate.shop.service.admin.EntitySettingsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;

/**
* Created by CodeGenerator on 2019/12/16.
*/
@RestController
@RequestMapping("/admin/entity/settings")
public class EntitySettingsController {
    @Resource
    private EntitySettingsService entitySettingsService;

    @PutMapping("/updateCustomerSignInSettings")
    public ResponseResult updateCustomerSignInSettings(@RequestBody SignInSettings signInSettings)
            throws JsonProcessingException {
        entitySettingsService.updateCustomerSignInSetting(signInSettings);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/updateWithDrawSettings")
    public ResponseResult addWithDrawSettings(@RequestBody WithDrawSettings withDrawSettings)
        throws JsonProcessingException {
        entitySettingsService.updateWithDrawSettings(withDrawSettings);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/withDrawSettingsDetail")
    public ResponseResult withDrawSettingsDetail() throws IOException {
        WithDrawSettings withDrawSettings = entitySettingsService.withDrawSettingsDetail();
        return ResultGenerator.genSuccessResponseResult(withDrawSettings);
    }

    @PutMapping("/updateInviteNumRankSettings")
    public ResponseResult updateInviteNumRankSettings(@RequestBody InviteNumRankSettings inviteNumRankSettings)
        throws JsonProcessingException {
        entitySettingsService.updateInviteNumRankSettings(inviteNumRankSettings);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/inviteNumRankSettingsDetail")
    public ResponseResult inviteNumRankSettingsDetail() throws IOException {
        InviteNumRankSettings inviteNumRankSettings = entitySettingsService.inviteNumRankSettingsDetail();
        return ResultGenerator.genSuccessResponseResult(inviteNumRankSettings);
    }


    @PutMapping("/updateIncomeRankSettings")
    public ResponseResult updateIncomeRankSettings(@RequestBody IncomeRankSettings incomeRankSettings)
        throws JsonProcessingException {
        entitySettingsService.updateIncomeRankSettings(incomeRankSettings);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/incomeRankSettingsDetail")
    public ResponseResult incomeRankSettingsDetail() throws IOException {
        IncomeRankSettings incomeRankSettings = entitySettingsService.incomeRankSettingsDetail();
        return ResultGenerator.genSuccessResponseResult(incomeRankSettings);
    }

    @PutMapping("/updatePcfSettings")
    public ResponseResult updateIncomeRankSettings(@RequestBody PcfSettings pcfSettings)
        throws JsonProcessingException {
        entitySettingsService.updatePcfSettings(pcfSettings);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/pcfSettingsDetail")
    public ResponseResult pcfSettingsDetail() throws IOException {
        PcfSettings pcfSettings = entitySettingsService.pcfSettingsDetail();
        return ResultGenerator.genSuccessResponseResult(pcfSettings);
    }
}
