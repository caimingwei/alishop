package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.CustProduct;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.CustProductService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/08/28.
*/
@RestController
@RequestMapping("/admin/custProduct")
@ResponseBody
public class CustProductController {
    @Resource
    private CustProductService custProductService;
    private final Logger logger = LoggerFactory.getLogger(CustProductController.class);

    @PostMapping("/add")
    public ResponseResult add(@RequestBody CustProduct custProduct) {
        custProduct.setCreateTime(new Date());
        custProduct.setUpdateTime(new Date());
        custProductService.save(custProduct);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id) {
        custProductService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody CustProduct custProduct) {
        custProduct.setUpdateTime(new Date());
        custProductService.update(custProduct);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail/{id}")
    public ResponseResult detail(@PathVariable("id") Integer id) {
        CustProduct custProduct = custProductService.findById(id);
        return ResultGenerator.genSuccessResponseResult(custProduct);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<CustProduct>> list( @RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<CustProduct> list = custProductService.findAll();
        PageInfo<CustProduct> pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
