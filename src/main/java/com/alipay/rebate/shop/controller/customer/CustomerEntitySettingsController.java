package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.entitysettings.IncomeRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.InviteNumRankSettings;
import com.alipay.rebate.shop.pojo.entitysettings.PcfSettings;
import com.alipay.rebate.shop.pojo.entitysettings.WithDrawSettings;
import com.alipay.rebate.shop.service.admin.EntitySettingsService;
import com.alipay.rebate.shop.service.customer.CustomerEntitySettingsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/12/16.
*/
@RestController
@RequestMapping("/customer/entity/settings")
public class CustomerEntitySettingsController {
    @Resource
    private CustomerEntitySettingsService entitySettingsService;

    @GetMapping("/pcfSettingsDetail")
    public ResponseResult pcfSettingsDetail() throws IOException {
        PcfSettings pcfSettings = entitySettingsService.pcfSettingsDetail();
        return ResultGenerator.genSuccessResponseResult(pcfSettings);
    }
}
