package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.InviteShareUrlSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.InviteShareUrlSettingsService;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/10/31.
*/
@RestController
@RequestMapping("/admin/invite/share/url/settings")
public class InviteShareUrlSettingsController {
    @Resource
    private InviteShareUrlSettingsService inviteShareUrlSettingsService;

//    @PostMapping("/add")
//    public ResponseResult add(@RequestBody InviteShareUrlSettings inviteShareUrlSettings) {
//        inviteShareUrlSettings.setId(1);
//        inviteShareUrlSettingsService.save(inviteShareUrlSettings);
//        return ResultGenerator.genSuccessResponseResult(null);
//    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody InviteShareUrlSettings inviteShareUrlSettings) {
        inviteShareUrlSettings.setId(1);
        inviteShareUrlSettingsService.update(inviteShareUrlSettings);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail")
    public ResponseResult<InviteShareUrlSettings> detail() {
        InviteShareUrlSettings inviteShareUrlSettings = inviteShareUrlSettingsService.findById(1);
        return ResultGenerator.genSuccessResponseResult(inviteShareUrlSettings);
    }

}
