package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.UserJdPid;
import com.alipay.rebate.shop.service.admin.UserJdPidService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/04/28.
*/
@RestController
@RequestMapping("/user/jd/pid")
public class UserJdPidController {
    @Resource
    private UserJdPidService userJdPidService;

    @PostMapping("/add")
    public Result add(UserJdPid userJdPid) {
        userJdPidService.save(userJdPid);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        userJdPidService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping("/update")
    public Result update(UserJdPid userJdPid) {
        userJdPidService.update(userJdPid);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        UserJdPid userJdPid = userJdPidService.findById(id);
        return ResultGenerator.genSuccessResult(userJdPid);
    }

    @GetMapping("/list")
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<UserJdPid> list = userJdPidService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
