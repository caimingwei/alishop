package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.ProductMapper;
import com.alipay.rebate.shop.dao.mapper.UserJdPidMapper;
import com.alipay.rebate.shop.helper.HistoryPriceHelper;
import com.alipay.rebate.shop.helper.HtmlClickHelper;
import com.alipay.rebate.shop.helper.InterfaceStatisticsHelper;
import com.alipay.rebate.shop.helper.UserJdPidHelper;
import com.alipay.rebate.shop.model.Product;
import com.alipay.rebate.shop.model.UserJdPid;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.dataoke.*;
import com.alipay.rebate.shop.pojo.dingdanxia.*;
import com.alipay.rebate.shop.pojo.historyprice.HistoryPriceRsp;
import com.alipay.rebate.shop.pojo.historyprice.ResultResp;
import com.alipay.rebate.shop.pojo.jd.JdOrdersRsp;
import com.alipay.rebate.shop.pojo.pdd.*;
import com.alipay.rebate.shop.pojo.pdtframework.AppPdtFrameworkAndPdtRsp;
import com.alipay.rebate.shop.pojo.pdtframework.OfficialChoice;
import com.alipay.rebate.shop.pojo.pdtframework.OwnChoice;
import com.alipay.rebate.shop.pojo.pdtframework.ProductChoice;
import com.alipay.rebate.shop.pojo.pdtframework.ProductRepoChoice;
import com.alipay.rebate.shop.pojo.pdtframework.ProductRepoChoiceRsp;
import com.alipay.rebate.shop.pojo.pdtframework.TaobaoJingPin;
import com.alipay.rebate.shop.pojo.user.customer.SearchProductRequest;
import com.alipay.rebate.shop.pojo.user.product.ActivityProductRsp;
import com.alipay.rebate.shop.pojo.weixin.WeiXinAccessTokenRsp;
import com.alipay.rebate.shop.service.customer.CustomerProductService;
import com.alipay.rebate.shop.utils.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;

import gherkin.lexer.Da;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@Controller
public class CustomerProductController {

    private Logger logger = LoggerFactory.getLogger(CustomerProductController.class);

    @Autowired
    CustomerProductService productService;
    @Autowired
    ParameterChecker parameterChecker;
    @Autowired
    InterfaceStatisticsHelper interfaceStatisticsHelper;
    @Autowired
    UserJdPidHelper userJdPidHelper;
    @Autowired
    UserJdPidMapper userJdPidMapper;
    @Autowired
    HistoryPriceHelper historyPriceHelper;
    @Autowired
    HtmlClickHelper htmlClickHelper;
    @Resource
    ProductMapper productMapper;

//    @GetMapping("/common/getProductById/{id}")
//    @ResponseBody
//    public ResponseResult<Product> getProductById(@PathVariable("id") Long id){
//
//        logger.debug("product id is : {}",id);
//        Product product = productService.get(id);
//        ResponseResult<Product> responseResult = new ResponseResult<>();
//        responseResult.setData(product);
//
//        return responseResult;
//    }

    @GetMapping("/common/selectProductsByCondition")
    @ResponseBody
    public ResponseResult<PageInfo<Product>> selectProductsByCondition(
         SearchProductRequest searchProductRequest
    ){

        logger.debug("searchProductRequest is : {}",searchProductRequest);
        parameterChecker.checkParams(searchProductRequest);
        PageInfo<Product> products = productService.searchByCondition(searchProductRequest);

        return ResultGenerator.genSuccessResponseResult(products);
    }

    @GetMapping("/common/selectProductFrameworkAndProducts")
    @ResponseBody
    public ResponseResult<AppPdtFrameworkAndPdtRsp> selectProductFrameworkAndProducts(
        @RequestParam("productPageId")
        Integer productPageId
    ){

        AppPdtFrameworkAndPdtRsp rsp = productService.selectProductFrameworkAndProducts(productPageId);
        return ResultGenerator.genSuccessResponseResult(rsp);
    }

    @GetMapping("/common/selectOwnChoiceProduct")
    @ResponseBody
    public ResponseResult<PageInfo<Product>> selectOwnChoiceProduct(
        Integer pageNo,
        Integer pageSize,
        Integer[] category,
        Integer sort
    ){
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        OwnChoice ownChoice = new OwnChoice();
        if(category != null){
            ownChoice.setCategory(Arrays.asList(category));
        }
        ownChoice.setSort(sort);
        logger.debug("OwnChoice is : {}",ownChoice);
        parameterChecker.checkParams(ownChoice);
        PageInfo<Product> pageInfo = productService.selectOwnChoiceProduct(
            ownChoice,
            pageNo,
            pageSize
        );
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/common/selectOfficeChoiceProduct")
    @ResponseBody
    public ResponseResult<PageInfo<Product>> selectOfficeChoiceProduct(
        Integer pageNo,
        Integer pageSize,
        Integer[] category,
        Integer filtrate,
        Integer adj_category,
        BigDecimal minPrice,
        BigDecimal maxPrice,
        Integer sell,
        Integer minBrokerage,
        Integer maxBrokerage,
        String keyword,
        Integer sort
    ){
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);

        OfficialChoice officialChoice = new OfficialChoice();
        if(category != null){
            officialChoice.setCategory(Arrays.asList(category));
        }
        officialChoice.setFiltrate(filtrate);
        officialChoice.setAdj_category(adj_category);
        officialChoice.setMinPrice(minPrice);
        officialChoice.setMaxPrice(maxPrice);
        officialChoice.setSell(sell);
        officialChoice.setMinBrokerage(minBrokerage);
        officialChoice.setMaxBrokerage(maxBrokerage);
        officialChoice.setKeyword(keyword);
        officialChoice.setSort(sort);
        logger.debug("OfficialChoice is : {}",officialChoice);
        parameterChecker.checkParams(officialChoice);
        PageInfo<Product> pageInfo = productService.selectOfficialChoiceProduct(
            officialChoice,
            pageNo,
            pageSize
        );
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/common/selectTaobaoJinpinProduct")
    @ResponseBody
    public ResponseResult<PageInfo<Product>> selectTaobaoJinpinProduct(
        Integer pageNo,
        Integer pageSize,
        Integer jpk,
        Long jpkCategory
    ) throws Exception {

        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        logger.debug("pageNo and pageSize is : {},{}",pageNo,pageSize);
        TaobaoJingPin taobaoJingPin = new TaobaoJingPin();
        taobaoJingPin.setJpk(jpk);
        taobaoJingPin.setJpkCategory(jpkCategory);
        logger.debug("TaobaoJingPin is : {}",taobaoJingPin);
        parameterChecker.checkParams(taobaoJingPin);
        PageInfo<Product> pageInfo = productService.selectTaobaoJinpinProduct(
            taobaoJingPin,
            pageNo,
            pageSize
        );

        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/common/selectProductChoiceProduct")
    @ResponseBody
    public ResponseResult<PageInfo<Product>> selectProductChoiceProduct(
        Integer pageNo,
        Integer pageSize,
        Integer[] category,
        Integer type,
        BigDecimal minAfterPrice,
        BigDecimal maxAfterPrice,
        BigDecimal minTicket,
        BigDecimal maxTicket,
        Integer sell,
        Integer sort
    ){

        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);

        ProductChoice productChoice = new ProductChoice();
        if(category != null){
            productChoice.setCategory(Arrays.asList(category));
        }
        productChoice.setSort(sort);
        productChoice.setType(type);
        productChoice.setMinAfterPrice(minAfterPrice);
        productChoice.setMaxAfterPrice(maxAfterPrice);
        productChoice.setMinTicket(minTicket);
        productChoice.setMaxTicket(maxTicket);
        productChoice.setSell(sell);
        logger.debug("ProductChoice is : {}",productChoice);
        parameterChecker.checkParams(productChoice);
        PageInfo<Product> pageInfo = productService.selectProductChoiceProduct(
            productChoice,
            pageNo,
            pageSize
        );

        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/common/selectProductRepoChoiceProduct")
    @ResponseBody
    public ResponseResult<ProductRepoChoiceRsp> selectProductRepoChoiceProduct(
        Integer pageNo,
        Integer pageSize,
        @RequestParam("activityId")
        Integer activityId,
        @RequestParam("productRepoId")
        Integer productRepoId,
        Double minPrice,
        Double maxPrice,
        Integer isTmall,
        Integer sort,
        String keyWords
    ){
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        ProductRepoChoice productRepoChoice = new ProductRepoChoice();
        productRepoChoice.setMinPrice(minPrice);
        productRepoChoice.setMaxPrice(maxPrice);
        productRepoChoice.setTmall(isTmall);
        productRepoChoice.setSort(sort);
        productRepoChoice.setKeyword(keyWords);
        parameterChecker.checkParams(productRepoChoice);
        PageInfo<Product>  pageInfo = productService
            .selectProductRepoChoiceProduct(productRepoChoice,productRepoId,pageNo,pageSize);
        ProductRepoChoiceRsp data = new ProductRepoChoiceRsp();
        data.setActivityId(activityId);
        data.setProductRepoId(productRepoId);
        data.setData(pageInfo);
        return ResultGenerator.genSuccessResponseResult(data);
    }

    @GetMapping("/common/selectDailyFreeActivityProduct")
    @ResponseBody
    public ResponseResult<ActivityProductRsp> selectDailyFreeActivityProduct(
        Integer pageNo,
        Integer pageSize
    ){

        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);

        ActivityProductRsp activityProductRsp =
            productService.selectDailyFreeActivityProduct(pageNo,pageSize);

        return ResultGenerator.genSuccessResponseResult(activityProductRsp);
    }

    @PostMapping("/common/dingdanxiaSuperSearch")
    @ResponseBody
    public Object dingdanxiaSuperSearch(
        @RequestBody SuperSearchReq request
    ) throws Exception {
        return DingdanxiaUtil.superSearch(request);
    }

    @GetMapping("/common/haodankuGetDeserveItem")
    @ResponseBody
    public Object haodankuGetDeserveItem() throws IOException {
        return HaodankuUtil.getDeserveItem();
    }

    @GetMapping("/common/selectTalentInfo")
    @ResponseBody
    public Object selectTalentInfo(@RequestParam("talentcat") Integer talentcat)
        throws IOException {
        return HaodankuUtil.selectTalentInfo(talentcat);
    }

    @GetMapping("/common/selectTalentArticle")
    @ResponseBody
    public Object selectTalentArticle(@RequestParam("id") Integer id) throws IOException {
        return HaodankuUtil.selectTalentArticle(id);
    }

    @GetMapping("/common/selectSalesList")
    @ResponseBody
    public Object selectSalesList(
        @RequestParam("sale_type")
        Integer sale_type,
        Integer cid,
        Integer min_id,
        Integer back,
        String item_type
    ) throws IOException {
        return HaodankuUtil.selectSalesList(sale_type,cid,min_id,back,item_type);
    }

    @GetMapping("/common/tklQuery")
    @ResponseBody
    public Object tklQuery(
        @RequestParam("tkl")
        String tkl,
        String signature
    ) throws IOException {
        return DingdanxiaUtil.tklQuery(tkl,signature);
    }

    @RequestMapping("/common/idPrivilege")
    @ResponseBody
    public Object idPrivilege(
        IdPrivilegeReq request
    ) throws Exception {
        return DingdanxiaUtil.idPrivilege(request);
    }

    @RequestMapping("/common/itemInfo")
    @ResponseBody
    public Object itemInfo(
        String signature,
        String num_iids,
        Integer platform
    ) throws Exception {
        return DingdanxiaUtil.itemInfo(signature,num_iids,platform);
    }

    @GetMapping("/common/getDtkSearchGoods")
    @ResponseBody
    public Object getDtkSearchGoods(
        DataokeProductSearchReq productSearchReq
    ) {
        interfaceStatisticsHelper.getCount("getDtkSearchGoods");
        return DataokeUtil.getDtkSearchGoods(productSearchReq);
    }

    @GetMapping("/common/searchSuggestion")
    @ResponseBody
    public Object searchSuggestion(
        String keyWords,
        Integer type,
        String version
    ) {
        return DataokeUtil.searchSuggestion(keyWords,type,version);
    }

    @RequestMapping("/common/tkQrcode")
    @ResponseBody
    public Object tkQrcode(
        TkQrcodeReq req
    ) throws Exception {
        return DingdanxiaUtil.tkQrcode(req);
    }

    @PostMapping("/common/tklPrivilege")
    @ResponseBody
    public Object tklPrivilege(
        @RequestBody TklPrivilegeReq req
    ) throws Exception {
        return DingdanxiaUtil.tklPrivilege(req);
    }

    @GetMapping("/common/getdesc")
    @ResponseBody
    public Object getdesc(
        @RequestParam("id") String id
    ) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String,String> map = new HashMap<>();
        map.put("id",id);

        StringBuilder builder = new StringBuilder();
        builder.append("https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdesc/6.0/?");
        builder.append(URLEncoder.encode("data","UTF-8"));
        builder.append("=");
        builder.append(URLEncoder.encode(objectMapper.writeValueAsString(map),"UTF-8"));
        logger.debug("url is : {}",builder.toString());
        String body = HttpUtils.doGet(builder.toString());
        Object obj = objectMapper.readValue(body,Object.class);
        return obj;
    }

    @RequestMapping("/common/spkOptimus")
    @ResponseBody
    public Object spkOptimus(
        OptimusReq req
    ) throws Exception {
        return DingdanxiaUtil.spkOptimus(req);
    }

//    @RequestMapping("/common/spkOptimusTwo")
//    @ResponseBody
//    public Object spkOptimusTwo(
//            OptimusReq req
//    ) throws Exception {
//        return DingdanxiaUtil.spkOptimusTwo(req);
//    }

    @RequestMapping("/common/couponSearch")
    @ResponseBody
    public Object couponSearch(
        String signature,
        String id
    ) throws Exception {
        return DingdanxiaUtil.couponSearch(signature,id);
    }

    @RequestMapping("/common/getAllProducts")
    @ResponseBody
    public Object getAllProducts(DataokeGoodsList dataokeGoodsList) {
        logger.debug(" this dataokeGoodsList is : {}",dataokeGoodsList);
        interfaceStatisticsHelper.getCount("getAllProducts");
        return DataokeUtil.getGoodsList(dataokeGoodsList);
    }


    @RequestMapping("/common/getSuperCategory")
    @ResponseBody
    public SuperCategoryResponse getSuperCategory()
            {
                interfaceStatisticsHelper.getCount("getSuperCategory");
        return DataokeUtil.getSuperCategory();
    }

    @RequestMapping("/common/getRankingList")
    @ResponseBody
    public Object getRankingList(DataokeRankingListReq dataokeRankingListReq)
            {
        logger.debug("dataokeRankingListReq is : {}",dataokeRankingListReq);
        interfaceStatisticsHelper.getCount("getRankingList");
        return DataokeUtil.getRankingList(dataokeRankingListReq);
    }

    @RequestMapping("/common/getQueryGoods")
    @ResponseBody
    public Object getQueryGoods(DingdanxiaQueryGoodsReq req) throws IOException {
        return DingdanxiaUtil.getQueryGoods(req);

    }

    @RequestMapping("/common/getQueryGoodsPromotioninfo")
    @ResponseBody
    public Object getQueryGoodsPromotioninfo(String signature,String skuIds) throws IOException {
        return DingdanxiaUtil.getQueryGoodsPromotioninfo(signature,skuIds);

    }

    @RequestMapping("/common/getByUnionidPromotion")
    @ResponseBody
    public Object getByUnionidPromotion(ByUnionidPromotionReq req) throws IOException {

        return DingdanxiaUtil.getByUnionidPromotion(req);

    }


    @RequestMapping("/common/getQueryJingfenGoods")
    @ResponseBody
    public Object getQueryJingfenGoods(QueryJingfenGoodsReq req) throws IOException {
        return DingdanxiaUtil.getQueryJingfenGoods(req);
    }

    @RequestMapping("/common/getTklCreateTwo")
    @ResponseBody
    public Object getTklCreateTwo(TklCreateTwoReq req) throws Exception {
        return DingdanxiaUtil.getTklCreateTwo(req);
    }

    @RequestMapping("/common/getActivityLink")
    @ResponseBody
    public Object getActivityLink(ActivityLinkReq req) throws Exception {
        return DingdanxiaUtil.getActivityLink(req);
    }


    @RequestMapping("/admin/getApitj")
    @ResponseBody
    public Object getApitj(String signature) throws Exception {
        return DingdanxiaUtil.getApitj(signature);
    }

    @RequestMapping("/customer/getByUnionidPromotionTwo")
    @ResponseBody
    public Object getByUnionidPromotionTwo(ByUnionidPromotionTwo req, HttpServletRequest request) throws IOException {
        long userId = JwtUtils.getUserIdFromToken(request);
        logger.debug("getByUnionidPromotionTwo userId is : {}",userId);
        Object byUnionidPromotionTwo = userJdPidHelper.getByUnionidPromotionTwo(req, userId);
        return byUnionidPromotionTwo;
    }


    @RequestMapping("/common/getPddGoodesDetail")
    @ResponseBody
    public Object getPddGoodesDetail(PddGoodsDetailReq req) throws Exception {
        return DingdanxiaUtil.getPddGoodesDetail(req);
    }

    @RequestMapping("/common/getPddGoodsSearch")
    @ResponseBody
    public Object getPddGoodsSearch(PddGoodsSearchReq req) throws Exception {
        return DingdanxiaUtil.getPddGoodsSearch(req);
    }


    @RequestMapping("/common/getPddGoodsListReq")
    @ResponseBody
    public Object getPddGoodsListReq(PddGoodsListReq req) throws Exception {
        return DingdanxiaUtil.getPddGoodsListReq(req);
    }

    @RequestMapping("/customer/getPddConvert")
    @ResponseBody
    public Object getPddConvert(PddConvertReq req, HttpServletRequest request) throws IOException {
        long userId = JwtUtils.getUserIdFromToken(request);
        logger.debug("getByUnionidPromotionTwo userId is : {}",userId);
        Object pddConvert = userJdPidHelper.getPddConvert(req, userId);
        return pddConvert;
    }

    @RequestMapping("/common/getPddOrders")
    @ResponseBody
    public PddOrdersRsp getPddOrders(Long startUpdateTime,Long endUpdateTime,Integer pageSize,Integer page) throws Exception {
        return DingdanxiaUtil.getPddOrders(startUpdateTime,endUpdateTime,pageSize,page);
    }


    @RequestMapping("/common/getJdOrders")
    @ResponseBody
    public JdOrdersRsp getJdOrders(String time, Integer pageNo, Integer pageSize, Integer type, Long childUnionId) throws Exception {
        return DingdanxiaUtil.getJdOrders(time,pageNo,pageSize,type,childUnionId);
    }


    @RequestMapping("/common/getJdGoodsCategory")
    @ResponseBody
    public Object getJdGoodsCategory(Integer parentId,Integer grade) throws Exception {
        return DingdanxiaUtil.getJdGoodsCategory(parentId,grade);
    }

    @RequestMapping("/common/getPddThemeList")
    @ResponseBody
    public Object getPddThemeList(Integer page,Integer size) throws Exception {
        return PddUtil.getPddThemeList(page,size);
    }

    @RequestMapping("/common/getThemeGoodsSearch")
    @ResponseBody
    public Object getThemeGoodsSearch(Long theme_id) throws Exception {
        return PddUtil.getThemeGoodsSearch(theme_id);
    }

    @RequestMapping("/common/getPddGoodsRecommend")
    @ResponseBody
    public Object getPddGoodsRecommend(PddGoodsRecommendReq req) throws Exception {
        return PddUtil.getPddGoodsRecommend(req);
    }

    @RequestMapping("/customer/getPddRppPomUrlGenerate")
    @ResponseBody
    public Object getPddRppPomUrlGenerate(PddRppPomUrlGenerateReq req,HttpServletRequest request) throws Exception {
        long userId = JwtUtils.getUserIdFromToken(request);
        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(userId);
        String pid = userJdPidHelper.getPddPid(userJdPid, userId);
        List<String> pidList = new ArrayList<>();
        pidList.add(pid);
        req.setP_id_list(pidList);
        return PddUtil.getPddRppPomUrlGenerate(req);
    }

    @RequestMapping("/customer/getPddResourceUrlGen")
    @ResponseBody
    public Object getPddResourceUrlGen(PddResourceUrlGenReq req, HttpServletRequest request) throws Exception {
        long userId = JwtUtils.getUserIdFromToken(request);
        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(userId);
        String pid = userJdPidHelper.getPddPid(userJdPid, userId);
        req.setPid(pid);
        return PddUtil.getPddResourceUrlGen(req);
    }

    @RequestMapping("/common/getPddDdkGoodsSearch")
    @ResponseBody
    public Object getPddDdkGoodsSearch(PddDdkGoodsSearchReq req) throws Exception {
        return PddUtil.getPddDdkGoodsSearch(req);
    }

    @RequestMapping("/customer/getPddDdkWeappQrcodeUrlGen")
    @ResponseBody
    public Object getPddDdkWeappQrcodeUrlGen(PddDdkWeappQrcodeUrlGenReq req, HttpServletRequest request) throws Exception {
        long userId = JwtUtils.getUserIdFromToken(request);
        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(userId);
        String pid = userJdPidHelper.getPddPid(userJdPid, userId);
        req.setP_id(pid);
        return PddUtil.getPddDdkWeappQrcodeUrlGen(req);
    }

    @RequestMapping("/customer/getPddDdkThemePromUrlGenerate")
    @ResponseBody
    public Object getPddDdkThemePromUrlGenerate(PddDdkThemePromUrlGenerateReq req, HttpServletRequest request) throws Exception {
        long userId = JwtUtils.getUserIdFromToken(request);
        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(userId);
        String pid = userJdPidHelper.getPddPid(userJdPid, userId);
        req.setPid(pid);
        return PddUtil.getPddDdkThemePromUrlGenerate(req);
    }

    @RequestMapping("/customer/getPddDdkGoodsZsUnitUrlGen")
    @ResponseBody
    public Object getPddDdkGoodsZsUnitUrlGen(PddDdkGoodsZsUnitUrlGenReq req, HttpServletRequest request) throws Exception {
        long userId = JwtUtils.getUserIdFromToken(request);
        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(userId);
        logger.debug("userjdPid is : {}",userJdPid);
        String pid = userJdPidHelper.getPddPid(userJdPid, userId);
        logger.debug("pid is : {}",pid);
        req.setPid(pid);
        return PddUtil.getPddDdkGoodsZsUnitUrlGen(req);

    }

    @RequestMapping("/customer/getPddUrlGenerate")
    @ResponseBody
    public Object getPddUrlGenerate(HttpServletRequest request) throws Exception {
        long userId = JwtUtils.getUserIdFromToken(request);
        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(userId);
        logger.debug("userjdPid is : {}",userJdPid);
        String pid = userJdPidHelper.getPddPid(userJdPid, userId);
        logger.debug("pid is : {}",pid);
        return DingdanxiaUtil.getPddUrlGenerate(pid);

    }

    @GetMapping("/common/testtttttt")
    @ResponseBody
    public Object test() throws Exception {
        Object test = DingdanxiaUtil.test();
        return test;
    }

    @GetMapping("/common/getFriendsCircleListDb")
    @ResponseBody
    public ResponseResult getFriendsCircleListDb(Integer pageNo,Integer pageSize) throws Exception {
        int page = pageNo == null ? 1 : pageNo;
        int size = pageSize == null ? 2 : pageSize;
        PageHelper.startPage(page,size);
        List<Product> products = productMapper.selectDataokeGoodsFriendsCircleList();
        PageInfo<Product> pageInfo = new PageInfo<>(products);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @PostMapping("/customer/getMeituanPrivilege")
    @ResponseBody
    public Object getMeituanPrivilege(HttpServletRequest request,@RequestBody String qrcode) throws IOException {
        long userId = JwtUtils.getUserIdFromToken(request);
        return DingdanxiaUtil.getMeituanPrivilege(String.valueOf(userId),qrcode);
    }

    @GetMapping("/common/getMeituanOrders")
    @ResponseBody
    public Object getMeituanOrders(HttpServletRequest request) throws IOException {
        Long currentTimeMillis = System.currentTimeMillis()/1000;
        Long startTime = currentTimeMillis - 86400;
        return DingdanxiaUtil.getMeituanOrders(startTime.intValue(),currentTimeMillis.intValue(),null,null,null);
    }

    @GetMapping("/common/getMeituanOrderId")
    @ResponseBody
    public Object getMeituanOrderId() throws IOException {
        return DingdanxiaUtil.getMeituanOrderId("57442292440073196");
    }

    @GetMapping("/common/getAccessToken")
    @ResponseBody
    public ResponseResult getAccessToken() throws IOException {
        WeiXinAccessTokenRsp accessToken = WeiXinUtil.getAccessToken();
        return ResultGenerator.genSuccessResponseResult(accessToken);
    }

    @GetMapping("/common/getWeiXinUserInfo")
    @ResponseBody
    public Object getWeiXinUserInfo(String openId) throws IOException {
        return WeiXinUtil.getWeiXinUserInfo(openId);
    }
}
