package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.SignInSettingAwardRsp;
import com.alipay.rebate.shop.service.customer.CustomerSignInAwardService;
import com.alipay.rebate.shop.utils.JwtUtils;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/09/21.
*/
@RestController
@RequestMapping("/customer/sign/in/award")
public class CustomerSignInAwardController {
    @Resource
    private CustomerSignInAwardService signInAwardService;

    @RequestMapping("/getSignInSettingAndAward")
    public ResponseResult<SignInSettingAwardRsp> getSignInSettingAndAward(
        HttpServletRequest request
    ){
        long userId = JwtUtils.getUserIdFromToken(request);
        SignInSettingAwardRsp rsp = signInAwardService.getSignInSettingAwardRsp(userId);
        return ResultGenerator.genSuccessResponseResult(rsp);
    }
}
