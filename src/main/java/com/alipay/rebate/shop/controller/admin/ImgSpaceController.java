package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.ImgSpaceMapper;
import com.alipay.rebate.shop.model.ImgSpace;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.imgspace.GroupBPReq;
import com.alipay.rebate.shop.pojo.imgspace.SearchCondition;
import com.alipay.rebate.shop.service.admin.ImgSpaceService;
import com.alipay.rebate.shop.utils.QiniuUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2020/02/02.
*/
@RestController
@RequestMapping("/admin/img/space")
public class ImgSpaceController {
    @Resource
    private ImgSpaceService imgSpaceService;
    @Autowired
    ImgSpaceMapper imgSpaceMapper;
    private Logger logger = LoggerFactory.getLogger(ImgSpaceController.class);

    @PostMapping("/add")
    public ResponseResult add(@RequestBody ImgSpace imgSpace) {
        Date date = new Date();
        imgSpace.setCreateTime(date);
        imgSpace.setUpdateTime(date);
        imgSpaceService.save(imgSpace);
        return ResultGenerator.genSuccessResponseResult(imgSpace.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Long id,@RequestParam Integer type) {

        ImgSpace imgSpace = imgSpaceMapper.selectImgSpaceById(id);
        String url = imgSpace.getUrl();
        String key = url.substring(url.lastIndexOf("/")+1);
        logger.debug("key is : {}",key);
        imgSpaceService.deleteById(id);
        if (type == 0){
            logger.debug("delete qi nui");
            QiniuUtil.deleteQiNuiPicture(key);
        }
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody ImgSpace imgSpace) {
        Date date = new Date();
        imgSpace.setUpdateTime(date);
        imgSpaceService.update(imgSpace);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Long id) {
        ImgSpace imgSpace = imgSpaceService.findById(id);
        return ResultGenerator.genSuccessResponseResult(imgSpace);
    }

    @DeleteMapping("/batchDelete")
    public ResponseResult batchDelete(@RequestParam List<Long> ids) {
        imgSpaceService.batchDelete(ids);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/batchUpdate")
    public ResponseResult batchUpdate(@RequestBody GroupBPReq groupBPReq) {
        imgSpaceService.batchUpdateGroup(groupBPReq);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/list")
    public ResponseResult list(SearchCondition searchCondition) {
        String orderBy = "create_time " + "desc";
        PageHelper.startPage(searchCondition.getPageNo(),searchCondition.getPageSize(),orderBy);
        List<ImgSpace> list = imgSpaceService.selectISByCondition(searchCondition);
        PageInfo<ImgSpace> pageInfo = new PageInfo<>(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
