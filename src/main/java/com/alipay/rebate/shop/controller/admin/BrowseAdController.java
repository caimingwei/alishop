package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.BrowseAd;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.BrowseAdService;
import java.util.Date;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/09/08.
*/
@RestController
@RequestMapping("/admin/browseAd")
public class BrowseAdController {
    @Resource
    private BrowseAdService browseAdService;

    private final Logger logger = LoggerFactory.getLogger(BrowseAdController.class);
//
//    @PostMapping("/add")
//    public ResponseResult add(@RequestBody BrowseAd browseAd) {
//        ResponseResult<BrowseAd> responseResult = new ResponseResult<>();
//        try{
//            browseAd.setCreateTime(new Date());
//            browseAd.setUpdateTime(new Date());
//            Long sk = new Date().getTime();
//            browseAdService.save(browseAd);
//            responseResult.setMsg("添加成功");
//        }catch (Exception e){
//            logger.error("browseAd add error : {}",e.toString());
//            responseResult.setMsg("服务器异常");
//            responseResult.setStatus(5000);
//        }
//        return responseResult;
//    }
//
//    @DeleteMapping("/delete/{id}")
//    public ResponseResult delete(@PathVariable("id") Integer id) {
//        ResponseResult<BrowseAd> responseResult = new ResponseResult<>();
//        try {
//            responseResult.setMsg("删除成功");
//            browseAdService.deleteById(id);
//        }catch (Exception e){
//            logger.error("browseAd delete error : {}",e.toString());
//            responseResult.setMsg("服务器异常");
//            responseResult.setStatus(5000);
//        }
//        return responseResult;
//    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody BrowseAd browseAd) {
        browseAd.setId(1);
        browseAd.setUpdateTime(new Date());
        browseAdService.update(browseAd);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail")
    public ResponseResult detail() {
        BrowseAd browseAd = browseAdService.findById(1);
        return ResultGenerator.genSuccessResponseResult(browseAd);
    }

//    @GetMapping("/list")
//    public ResponseResult<PageInfo<BrowseAd>> list( @RequestParam(defaultValue = "0") Integer page,
//                                                 @RequestParam(defaultValue = "0") Integer size) {
//        ResponseResult<PageInfo<BrowseAd>> responseResult = new ResponseResult<>();
//        PageHelper.startPage(page, size);
//        List<BrowseAd> list = browseAdService.findAll();
//        PageInfo pageInfo = new PageInfo(list);
//        responseResult.setData(pageInfo);
//        return responseResult;
//    }
}
