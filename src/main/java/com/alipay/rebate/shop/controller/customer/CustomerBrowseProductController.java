package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.BrowseProduct;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerBrowseProductService;
import com.alipay.rebate.shop.utils.JwtUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerBrowseProductController {

  private Logger logger = LoggerFactory.getLogger(CustomerBrowseProductController.class);

  @Autowired
  ParameterChecker parameterChecker;

  @Autowired
  CustomerBrowseProductService browseProductService;

  @PostMapping("/customer/addBrowseProduct")
  @ResponseBody
  public ResponseResult<Integer> addBrowseProduct(
      @RequestBody  BrowseProduct browseProduct,
      HttpServletRequest request){

    long userId = JwtUtils.getUserIdFromToken(request);
    logger.debug("userId is : {}",userId);
    browseProduct.setUserId(userId);
    Integer result = browseProductService.insertWhenNotExists(browseProduct);

    return ResultGenerator.genSuccessResponseResult(result);
  }

  @GetMapping("/customer/selectUserBrowseProduct")
  @ResponseBody
  public ResponseResult<PageInfo<BrowseProduct>> selectUserBrowseProduct(
      HttpServletRequest request,
      Integer pageNo,
      Integer pageSize
      ){
    pageNo = parameterChecker.checkAndGetPageNo(pageNo);
    pageSize = parameterChecker.checkAndGetPageSize(pageSize);
    long userId = JwtUtils.getUserIdFromToken(request);
    PageHelper.startPage(pageNo,pageSize);
    PageInfo<BrowseProduct> result = browseProductService.selectUserBrowseProduct(userId,pageNo,pageSize);
    return ResultGenerator.genSuccessResponseResult(result);
  }

}
