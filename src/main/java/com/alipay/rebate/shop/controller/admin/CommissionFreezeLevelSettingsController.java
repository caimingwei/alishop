package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.CommissionFreezeLevelSettingsMapper;
import com.alipay.rebate.shop.dao.mapper.CommissionLevelSettingsMapper;
import com.alipay.rebate.shop.model.CommissionFreezeLevelSettings;
import com.alipay.rebate.shop.pojo.common.CommonPageRequest;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.CommissionFreezeLevelSettingsService;
import com.github.pagehelper.PageHelper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/11/08.
*/
@RestController
@RequestMapping("/admin/commission/freeze/level/settings")
public class CommissionFreezeLevelSettingsController {

    private Logger logger = LoggerFactory.getLogger(CommissionFreezeLevelSettingsController.class);
    @Resource
    private CommissionFreezeLevelSettingsService commissionFreezeLevelSettingsService;

    @Autowired
    CommissionFreezeLevelSettingsMapper commissionLevelSettingsMapper;


    @PostMapping("/add")
    public ResponseResult add(@RequestBody CommissionFreezeLevelSettings commissionFreezeLevelSettings) {
        Date date = new Date();
        commissionFreezeLevelSettings.setCreateTime(date);
        commissionFreezeLevelSettings.setUpdateTime(date);
        commissionFreezeLevelSettingsService.save(commissionFreezeLevelSettings);
        return ResultGenerator.genSuccessResponseResult(commissionFreezeLevelSettings.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Integer id) {
        commissionFreezeLevelSettingsService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody CommissionFreezeLevelSettings commissionFreezeLevelSettings) {
        Date date = new Date();
        commissionFreezeLevelSettings.setUpdateTime(date);
        commissionFreezeLevelSettingsService.update(commissionFreezeLevelSettings);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/detail")
    public ResponseResult<CommissionFreezeLevelSettings> detail(@RequestParam Integer id) {
        CommissionFreezeLevelSettings commissionFreezeLevelSettings = commissionFreezeLevelSettingsService.findById(id);
        return ResultGenerator.genSuccessResponseResult(commissionFreezeLevelSettings);
    }

    @GetMapping("/list")
    public ResponseResult<List<CommissionFreezeLevelSettings>> list(
        CommonPageRequest request
    ) {
        PageHelper.startPage(request.getPageNo(), request.getPageSize());
        logger.debug("pageNo and pageSize is : {},{}",request.getPageNo(),request.getPageSize());
        List<CommissionFreezeLevelSettings> list = commissionFreezeLevelSettingsService.findAllOrderByBegin();
        return ResultGenerator.genSuccessResponseResult(list);
    }

    @GetMapping("/get")
    public ResponseResult<List<CommissionFreezeLevelSettings>> get(BigDecimal money,Integer type){
        List<CommissionFreezeLevelSettings> commissionFreezeLevelSettings = commissionLevelSettingsMapper.selectByBeginAndEnd(money, type);
        logger.debug("commissionFreezeLevelSettings is : {}",commissionFreezeLevelSettings);
        return ResultGenerator.genSuccessResponseResult(commissionFreezeLevelSettings);
    }
}
