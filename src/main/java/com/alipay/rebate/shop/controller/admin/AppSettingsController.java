package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.AppSettingsMapper;
import com.alipay.rebate.shop.helper.AppSettingsHelper;
import com.alipay.rebate.shop.model.AppSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.AppSettingsService;
import java.util.Date;
import javax.annotation.Resource;

import com.alipay.rebate.shop.utils.EncryptUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/08/22.
*/
@RestController
@RequestMapping("/admin/appSettings")
@ResponseBody
public class AppSettingsController {
    private final Logger logger = LoggerFactory.getLogger(AppSettingsController.class);
    @Resource
    private AppSettingsService appSettingsService;
    @Resource
    private AppSettingsMapper appSettingsMapper;
    @Resource
    private AppSettingsHelper appSettingsHelper;

    @GetMapping("/get")
    public ResponseResult<String> get(){
//        String s = appSettingsHelper.getSmsAccesskeyId();
        String smsSecret = appSettingsMapper.selectSmsSecret();
        logger.debug("this is : {}",smsSecret);
        return ResultGenerator.genSuccessResponseResult(smsSecret);
    }


    @PutMapping("/update")
    public ResponseResult update(@RequestBody AppSettings appSettings) {
        logger.debug("appSettings is : {}",appSettings);
        appSettings.setId(1);
        appSettings.setUpdateTime(new Date());

        appSettingsService.update(appSettings);
        if (!StringUtils.isEmpty(appSettings.getPassword())){
            String password = EncryptUtil.encryptPassword(appSettings.getPassword());
            appSettings.setPassword(password);
        }
        if (appSettings.getCsWetchat() != null) {
            if (StringUtils.isEmpty(appSettings.getCsWetchat())) {
                appSettingsMapper.updateCsWetchat();
            }
        }
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail")
    public ResponseResult<AppSettings> detail() {
        AppSettings appSettings = appSettingsService.findById(1);
        return ResultGenerator.genSuccessResponseResult(appSettings);
    }

    @PutMapping("/updatePassword")
    public ResponseResult<Integer> updatePassword(String password){
        logger.debug("password is : {}",password);
        String pwd = EncryptUtil.encryptPassword(password);
        int i = appSettingsMapper.updatePassword(pwd);
        return ResultGenerator.genSuccessResponseResult(i);
    }

}
