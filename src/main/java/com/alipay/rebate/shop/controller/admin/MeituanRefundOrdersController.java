package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.MeituanRefundOrdersMapper;
import com.alipay.rebate.shop.model.MeituanRefundOrders;
import com.alipay.rebate.shop.service.admin.MeituanRefundOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/08/25.
*/
@RestController
@RequestMapping("/admin/meituan/refund/orders")
public class MeituanRefundOrdersController {
    @Resource
    private MeituanRefundOrdersService meituanRefundOrdersService;
    @Resource
    private MeituanRefundOrdersMapper meituanRefundOrdersMapper;

    @GetMapping("/list")
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "20") Integer size,Long userId) {
        PageHelper.startPage(page, size);
        List<MeituanRefundOrders> list = meituanRefundOrdersMapper.selectAllMeituanRefundOrder(userId);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
