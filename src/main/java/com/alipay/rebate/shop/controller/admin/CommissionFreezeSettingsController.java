package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.CommissionFreezeSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.CommissionFreezeSettingsService;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/11/02.
*/
@RestController
@RequestMapping("/admin/commission/freeze/settings")
public class CommissionFreezeSettingsController {
    @Resource
    private CommissionFreezeSettingsService commissionFreezeSettingsService;

//    @PostMapping("/add")
//    public Result add(CommissionFreezeSettings commissionFreezeSettings) {
//        commissionFreezeSettingsService.save(commissionFreezeSettings);
//        return ResultGenerator.genSuccessResult();
//    }

//    @PostMapping("/delete")
//    public Result delete(@RequestParam Integer id) {
//        commissionFreezeSettingsService.deleteById(id);
//        return ResultGenerator.genSuccessResult();
//    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody CommissionFreezeSettings commissionFreezeSettings) {
        commissionFreezeSettings.setId(1);
        commissionFreezeSettingsService.update(commissionFreezeSettings);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail")
    public ResponseResult<CommissionFreezeSettings> detail() {
        Integer id  = 1 ;
        CommissionFreezeSettings commissionFreezeSettings = commissionFreezeSettingsService.findById(id);
        return ResultGenerator.genSuccessResponseResult(commissionFreezeSettings);
    }

//    @PostMapping("/list")
//    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
//        PageHelper.startPage(page, size);
//        List<CommissionFreezeSettings> list = commissionFreezeSettingsService.findAll();
//        PageInfo pageInfo = new PageInfo(list);
//        return ResultGenerator.genSuccessResult(pageInfo);
//    }
}
