package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerTaskSettingsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/task/settings")
public class CustomerTaskSettingsController {

  @Autowired
  private CustomerTaskSettingsService taskSettingsService;

  @GetMapping("/isUseSettings")
  public ResponseResult isUseSettings() {
    List<TaskSettings> data = taskSettingsService.selectUseSettings();
    return ResultGenerator.genSuccessResponseResult(data);
  }
}
