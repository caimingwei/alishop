package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.CommissionLevelSettingsMapper;
import com.alipay.rebate.shop.model.CommissionLevelSettings;
import com.alipay.rebate.shop.pojo.common.CommonPageRequest;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.CommissionLevelSettingsService;
import com.github.pagehelper.PageHelper;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/11/08.
*/
@RestController
@RequestMapping("/common/commission/level/settings")
public class CustomerCommissionLevelSettingsController {
    @Resource
    private CommissionLevelSettingsMapper commissionLevelSettingsMapper;

    @GetMapping("/list")
    public ResponseResult<List<CommissionLevelSettings>> list(
        CommonPageRequest request
    ) {
        PageHelper.startPage(request.getPageNo(), request.getPageSize());
        List<CommissionLevelSettings> list = commissionLevelSettingsMapper.selectAllOrderByBegin();
        return ResultGenerator.genSuccessResponseResult(list);
    }
}
