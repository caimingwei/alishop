package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.MeituanOrdersMapper;
import com.alipay.rebate.shop.model.MeituanOrders;
import com.alipay.rebate.shop.service.admin.MeituanOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/08/25.
*/
@RestController
@RequestMapping("/admin/meituan/orders")
public class MeituanOrdersController {
    @Resource
    private MeituanOrdersService meituanOrdersService;
    @Resource
    private MeituanOrdersMapper meituanOrdersMapper;

    @GetMapping("/list")
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "20") Integer size,
                       String orderId, String userName, String promoterName, Long userId,
                       Long promoterId, String startTime, String endTime) {
        PageHelper.startPage(page, size);
        List<MeituanOrders> meituanOrders =
                meituanOrdersMapper.selectAllMeiTuanOrders(orderId, userName, promoterName, userId, promoterId, startTime, endTime);
        PageInfo pageInfo = new PageInfo(meituanOrders);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
