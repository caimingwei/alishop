package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.InterfaceStatistics;
import com.alipay.rebate.shop.model.MainHall;
import com.alipay.rebate.shop.pojo.InterfaceStatisticsRsp;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.InterfaceStatisticsService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/04/27.
*/
@RestController
@RequestMapping("/admin/interfaceStatistics")
public class InterfaceStatisticsController {
    @Resource
    private InterfaceStatisticsService interfaceStatisticsService;

//    @GetMapping("/list")
//    public ResponseResult<PageInfo<InterfaceStatistics>>  list(@RequestParam(defaultValue = "0") Integer pageNum, @RequestParam(defaultValue = "0") Integer pageSize) {
//        PageHelper.startPage(pageNum, pageSize);
//        List<InterfaceStatistics> list = interfaceStatisticsService.findAll();
//        PageInfo pageInfo = new PageInfo(list);
//        return ResultGenerator.genSuccessResponseResult(pageInfo);
//    }
    @GetMapping("/list")
    public ResponseResult<List<InterfaceStatisticsRsp>>  list() {

        List<InterfaceStatisticsRsp> list = interfaceStatisticsService.selectInterfaceStatistics();

        return ResultGenerator.genSuccessResponseResult(list);
    }
}
