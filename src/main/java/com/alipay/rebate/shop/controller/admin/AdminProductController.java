package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.checker.ProductPageFrameworkConverter;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.helper.HistoryPriceHelper;
import com.alipay.rebate.shop.model.ProductPageFramework;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.historyprice.HistoryPriceRsp;
import com.alipay.rebate.shop.pojo.user.admin.ProductPageFrameworkRequest;
import com.alipay.rebate.shop.service.admin.AdminProductService;

import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/admin")
@RestController
public class AdminProductController {

    private final Logger logger = LoggerFactory.getLogger(AdminProductController.class);
    @Autowired
    AdminProductService adminProductService;

    @Autowired
    ParameterChecker parameterChecker;

    @PostMapping("/createOrUpdateProductPageFramework")
    @ResponseBody
    public ResponseResult<Long> createOrUpdatePage(@RequestBody ProductPageFrameworkRequest request){
        logger.debug("ProductPageFrameworkRequest is : {}",request);
        parameterChecker.checkParams(request);
        ProductPageFramework productPageFramework = ProductPageFrameworkConverter
            .convert2ProductPageFramework(request);
        logger.debug("ProductPageFramework is : {}",productPageFramework);
        return adminProductService.createOrUpdatePage(productPageFramework);
    }

    @GetMapping("/selectAllProductPageFramework")
    @ResponseBody
    public ResponseResult<PageInfo<ProductPageFramework>> selectAllPage(Integer page,Integer size,Integer pageCatagory){
        logger.debug("Admin select all page framework ");
//        return adminProductService.selectAllPageFramework(pageCatagory);
        PageInfo<ProductPageFramework> pageInfo = adminProductService.selectAllPageFramework(page, size, pageCatagory);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/selectProductPageFramework/{id}")
    @ResponseBody
    public ResponseResult<ProductPageFramework> selectPageFrameworkByPageId(@PathVariable("id") Integer id){
        logger.debug("Page id is : {}",id);
        return adminProductService.selectPageFrameworkByPageId(id);
    }

    @DeleteMapping("/deleteProductPageFramework/{id}")
    @ResponseBody
    public ResponseResult<Integer> deletePageFramework(@PathVariable("id") Integer id){
        logger.debug("delete page id is : {}",id);
        ResponseResult<Integer> responseResult = new ResponseResult<>();
        adminProductService.deletePageById(id);

        return responseResult;
    }
}
