package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.entitysettings.InrUser;
import com.alipay.rebate.shop.pojo.entitysettings.IrUser;
import com.alipay.rebate.shop.service.customer.CustomerAppService;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/")
public class CustomerAppServiceController {

  @Autowired
  private CustomerAppService customerAppService;

  @RequestMapping("/inrUsersRank")
  public ResponseResult<List<InrUser>> inrUsersRank() throws IOException {
    List<InrUser> inrUsers = customerAppService.inviteNumRank();
    return ResultGenerator.genSuccessResponseResult(inrUsers);
  }

  @RequestMapping("/irUsersRank")
  public ResponseResult<List<IrUser>> irUsersRank() throws IOException {
    List<IrUser> irUsers = customerAppService.incomeRank();
    return ResultGenerator.genSuccessResponseResult(irUsers);
  }
}
