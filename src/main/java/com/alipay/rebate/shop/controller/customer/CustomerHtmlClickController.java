package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.helper.HtmlClickHelper;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Date;

@RestController
@RequestMapping("/common/htmlClick")
public class CustomerHtmlClickController {

    private final Logger logger = LoggerFactory.getLogger(CustomerHtmlClickController.class);

    @Autowired
    HtmlClickHelper htmlClickHelper;

    @GetMapping("/addOrUpdate")
    public ResponseResult addOrUpdate(@NotNull String htmlName,@NotNull String htmlChineseName, HttpServletRequest request) {

        String ip = request.getRemoteAddr();
        logger.debug("user ip is : {}",ip);

        Date nowDate = DateUtil.getStartTimeOfToday();
        htmlClickHelper.checkInterfaceClick(htmlName,htmlChineseName);
        return ResultGenerator.genSuccessResponseResult();
    }

}
