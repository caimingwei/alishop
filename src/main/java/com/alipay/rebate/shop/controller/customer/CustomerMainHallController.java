package com.alipay.rebate.shop.controller.customer;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.MainHallMapper;
import com.alipay.rebate.shop.model.MainHall;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.mainhall.MainHallReq;
import com.alipay.rebate.shop.pojo.mainhall.UpdateMainHallReq;
import com.alipay.rebate.shop.service.admin.MainHallService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/04/24.
*/
@RestController
@RequestMapping("/common/mainHall")
public class CustomerMainHallController {
    @Resource
    private MainHallMapper mainHallMapper;

    @GetMapping("/detail")
    public ResponseResult<MainHall> detail(@RequestParam Long id) {
        MainHall mainHall = mainHallMapper.selectMainHallById(id);
        return ResultGenerator.genSuccessResponseResult(mainHall);
    }


}
