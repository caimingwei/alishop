package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.ActivityBrowseProduct;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerActivityBrowseProductService;
import com.alipay.rebate.shop.utils.JwtUtils;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/")
public class CustomerActivityBrowseProductController {

  private Logger logger = LoggerFactory.getLogger(Logger.class);

  @Autowired
  CustomerActivityBrowseProductService activityBrowseProductService;
  @Autowired
  ParameterChecker parameterChecker;


  @PostMapping("addActivityBrowseProduct")
  public ResponseResult<Integer> addActivityBrowseProduct(
      @RequestBody ActivityBrowseProduct browseProduct,
      HttpServletRequest request
  ){
    long userId = JwtUtils.getUserIdFromToken(request);
    browseProduct.setUserId(userId);
    browseProduct.setClickTime(new Date());
    logger.debug("ActivityBrowseProduct is : {}",browseProduct);
    parameterChecker.checkParams(browseProduct);
    activityBrowseProductService.save(browseProduct);
    return ResultGenerator.genSuccessResponseResult(browseProduct.getActivityId());
  }

}
