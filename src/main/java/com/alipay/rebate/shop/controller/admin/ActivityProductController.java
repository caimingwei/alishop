package com.alipay.rebate.shop.controller.admin;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.ActivityProductMapper;
import com.alipay.rebate.shop.model.ActivityProduct;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.ActivityProductService;
import com.alipay.rebate.shop.service.admin.AdminOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/08/25.
*/
@RestController
@RequestMapping("/admin/activity/product")
@ResponseBody
public class ActivityProductController {
    private final static Logger logger = LoggerFactory.getLogger(ActivityProductController.class);
    @Resource
    private ActivityProductService activityProductService;
    @Autowired
    AdminOrdersService ordersService;

    @Autowired
    ActivityProductMapper activityProductMapper;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody ActivityProduct activityProduct) {
        activityProduct.setCreateTime(new Date());
        activityProduct.setUpdateTime(new Date());
        activityProductService.save(activityProduct);
        return ResultGenerator.genSuccessResponseResult(null);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id) {
//        activityProductService.deleteById(id);
        activityProductMapper.updateActivityProductById(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }



    @PutMapping("/update")
    public ResponseResult update(@RequestBody ActivityProduct activityProduct) {
        activityProduct.setUpdateTime(new Date());
        activityProductService.update(activityProduct);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail/{id}")
    public ResponseResult detail(@PathVariable("id") Integer id) {
//        ActivityProduct activity = activityProductService.findById(id);
        ActivityProduct activity = activityProductMapper.selectActivityProductById(id);
        return ResultGenerator.genSuccessResponseResult(activity);
    }


    @GetMapping("/list")
    public ResponseResult<PageInfo<ActivityProduct>> list(@RequestBody JSONObject requestJson) {
        Integer page = requestJson.getInteger("page")==null?0:requestJson.getInteger("page");
        Integer size = requestJson.getInteger("size")==null?0:requestJson.getInteger("size");
        PageHelper.startPage(page, size);
        List<ActivityProduct> list = activityProductService.findAll();
        PageInfo<ActivityProduct> pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/getActivityProductList")
    public ResponseResult<PageInfo<JSONArray>> getActivityProductList(@RequestParam("activityId")Integer activityId,
                                                                            @RequestParam(defaultValue = "0") Integer page,
                                                                            @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<ActivityProduct> list = activityProductService.getProductsByActivityId(activityId);
        String orderType = "";
        List<Orders> orders = null;
        JSONArray arr = new JSONArray();
        JSONObject obj = null;
        System.out.println(list.size());
        for(ActivityProduct activityProduct:list){
//            orderType = activityProduct.getProductType()=0?"淘宝":"京东";
            logger.debug("activityProduct.getProductType() is : {}",activityProduct);
            if (activityProduct.getProductType() != null) {
                logger.debug("orderType");
                if (activityProduct.getProductType() == 0) {
                    orderType = "淘宝";
                }
                if (activityProduct.getProductType() == 1) {
                    orderType = "京东";
                }
                if (activityProduct.getProductType() == 2) {
                    orderType = "拼多多";
                }
            }
            orders = ordersService.selectOrderByGoodIdAndOrderType(String.valueOf(activityProduct.getProductId()),orderType);
            obj = JSONObject.parseObject(JSONObject.toJSONString(activityProduct));
            obj.put("surpNum",activityProduct.getLimitNum()-orders.size());
            arr.add(obj);
        }
        PageInfo<JSONArray> pageInfo = new PageInfo(arr);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

}
