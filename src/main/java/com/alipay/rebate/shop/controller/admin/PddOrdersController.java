package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.PddOrdersMapper;
import com.alipay.rebate.shop.model.PddOrders;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.PddOrdersPageReq;
import com.alipay.rebate.shop.service.admin.PddOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/05/12.
*/
@RestController
@RequestMapping("/admin/pddOrders")
public class PddOrdersController {
    @Resource
    private PddOrdersService pddOrdersService;
    @Resource
    private PddOrdersMapper pddOrdersMapper;

    @PostMapping("/add")
    public Result add(PddOrders pddOrders) {
        pddOrdersService.save(pddOrders);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        pddOrdersService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping("/update")
    public Result update(PddOrders pddOrders) {
        pddOrdersService.update(pddOrders);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        PddOrders pddOrders = pddOrdersService.findById(id);
        return ResultGenerator.genSuccessResult(pddOrders);
    }


    @GetMapping("/list")
    public ResponseResult<PageInfo<PddOrders>> list(PddOrdersPageReq req, @RequestParam(defaultValue = "1") Integer page,
                                                    @RequestParam(defaultValue = "20") Integer size) {
        PageInfo<PddOrders> pageInfo = pddOrdersService.selectOrdersByCondition(req, page, size);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/earningOrdersList")
    public ResponseResult<PageInfo<PddOrders>> pddOrdersList(String date, @RequestParam(defaultValue = "1") Integer page,
                                                    @RequestParam(defaultValue = "20") Integer size) {
        PageHelper.startPage(page,size);
        List<PddOrders> pddOrders = pddOrdersMapper.selectOrdersByDate(date);
        PageInfo<PddOrders> pageInfo = new PageInfo<>(pddOrders);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
