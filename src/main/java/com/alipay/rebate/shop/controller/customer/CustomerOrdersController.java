package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.order.FindOrdersReq;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import com.alipay.rebate.shop.service.customer.CustomerOrdersService;
import com.alipay.rebate.shop.utils.JwtUtils;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class CustomerOrdersController {

    private Logger logger = LoggerFactory.getLogger(CustomerOrdersController.class);

    @Autowired
    CustomerOrdersService ordersService;
    @Resource
    OrdersMapper ordersMapper;
    @Resource
    UserMapper userMapper;

    @GetMapping("/customer/getOrdersFromApi")
    public String getOrdersFromApi(){
        return "success";
    }

    @GetMapping("/customer/getOrdersById")
    @ResponseBody
    public Orders getOrdersById(String id){
       return ordersService.get(id);
    }

    @GetMapping("/customer/selectUserOrdersByTkStatus")
    @ResponseBody
    public ResponseResult<PageInfo<OrdersListResponse>> selectUserOrdersByType(
            Long tkStatus,
            Integer pageNo,
            Integer pageSize,
            HttpServletRequest request
    ){
        logger.debug("tkStatus , pageNo, pageSize : {}, {}, {}", tkStatus,pageNo,pageSize);
        long userId = JwtUtils.getUserIdFromToken(request);
        PageInfo<OrdersListResponse> data =  ordersService.selectUserOrdersByType(tkStatus,pageNo,pageSize,userId);
        return ResultGenerator.genSuccessResponseResult(data);
    }

    @GetMapping("/customer/selectFansOrdersByTkStatus")
    @ResponseBody
    public ResponseResult<PageInfo<OrdersListResponse>> selectFansOrdersByType(
            Long tkStatus,
            Integer pageNo,
            Integer pageSize,
            HttpServletRequest request){
        logger.debug("tkStatus , pageNo, pageSize: {}, {}, {}, {}", tkStatus,pageNo,pageSize);
        long userId = JwtUtils.getUserIdFromToken(request);
        logger.debug("userId is : {}",userId);
        PageInfo<OrdersListResponse> data = ordersService.selectFansOrdersByType(tkStatus,pageNo,pageSize,userId);
        return ResultGenerator.genSuccessResponseResult(data);
    }

    @DeleteMapping("/customer/deleteOrder/{id}")
    @ResponseBody
    public ResponseResult<Integer> deleteOrder(@PathVariable("id") String id){

        logger.debug("received id from request : {}",id);
        int result = ordersService.delete(id);
        return ResultGenerator.genSuccessResponseResult(result);

    }

    @GetMapping("/customer/getOrder")
    @ResponseBody
    public ResponseResult<List<Orders>> getOrder(FindOrdersReq req){
        logger.debug("received id from request : {}",req);
        List<Orders> order = ordersService.findOrder(req);
        return ResultGenerator.genSuccessResponseResult(order);
    }


    @GetMapping("/customer/selectUserOrdersByTkStatusTwo")
    @ResponseBody
    public ResponseResult<PageInfo<OrdersListResponse>> selectUserOrdersByTypeTwo(
            Long tkStatus,
            Integer type,
            Integer pageNo,
            Integer pageSize,
            HttpServletRequest request
    ) throws ParseException {
        logger.debug("tkStatus , pageNo, pageSize : {}, {}, {}", tkStatus,pageNo,pageSize);
        long userId = JwtUtils.getUserIdFromToken(request);
        PageInfo<OrdersListResponse> data =  ordersService.selectUserOrdersByTypeTwo(tkStatus,type,pageNo,pageSize,userId);
        return ResultGenerator.genSuccessResponseResult(data);
    }


    @GetMapping("/customer/selectFansOrdersByTypeTwo")
    @ResponseBody
    public ResponseResult<PageInfo<OrdersListResponse>> selectFansOrdersByTypeTwo(
            Long tkStatus,
            Integer type,
            Integer pageNo,
            Integer pageSize,
            HttpServletRequest request
    )  {
        logger.debug("tkStatus , pageNo, pageSize : {}, {}, {}", tkStatus,pageNo,pageSize);
        long userId = JwtUtils.getUserIdFromToken(request);
        PageInfo<OrdersListResponse> data =  ordersService.selectFansOrdersByTypeTwo(tkStatus,type,pageNo,pageSize,userId);
        return ResultGenerator.genSuccessResponseResult(data);
    }


    @GetMapping("/common/selectUserOrdersByTkStatus")
    @ResponseBody
    public ResponseResult<PageInfo<OrdersListResponse>> selectUserOrdersByTypeTwo(
            Long tkStatus,
            Integer type,
            Integer pageNo,
            Integer pageSize,
            Integer orderType,
            String phone
    ) throws ParseException {
        logger.debug("tkStatus , pageNo, pageSize : {}, {}, {}", tkStatus,pageNo,pageSize);
        User user = userMapper.selectUserByPhone(phone);
        if (user == null){
            throw new BusinessException("手机号对应用户不存在，请重新输入", StatusCode.PARAMETER_CHECK_ERROR);
        }
        if (type == 1) {
            PageInfo<OrdersListResponse> data = ordersService.selectUserOrdersByTypeTwo(tkStatus, orderType, pageNo, pageSize, user.getUserId());
            return ResultGenerator.genSuccessResponseResult(data);
        }
        if (type == 2){
            PageInfo<OrdersListResponse> data =  ordersService.selectFansOrdersByTypeTwo(tkStatus,orderType,pageNo,pageSize,user.getUserId());
            return ResultGenerator.genSuccessResponseResult(data);
        }
        return null;
    }
}
