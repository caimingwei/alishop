package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.AdminUser;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserObjRsp;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserRewordReq;
import com.alipay.rebate.shop.service.admin.AdminUserService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
* Created by CodeGenerator on 2019/12/08.
*/
@RestController
@RequestMapping("/admin/user")
public class AdminUserController {
    @Resource
    private AdminUserService adminUserService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody AdminUser adminUser) {

        Date date = new Date();
        if(adminUser.getCreateTime() == null){
            adminUser.setCreateTime(date);
        }
        adminUser.setUpdateTime(date);
        adminUser.setUserPassword(EncryptUtil.encryptPassword(adminUser.getUserPassword()));
        adminUserService.addNewAdminUser(adminUser);
        return ResultGenerator.genSuccessResponseResult(adminUser.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Long id) {
        adminUserService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody AdminUser adminUser) {
        adminUser.setUpdateTime(new Date());
        if(!StringUtils.isEmpty(adminUser.getUserPassword())){
            adminUser.setUserPassword(EncryptUtil.encryptPassword(adminUser.getUserPassword()));
        }
        adminUserService.updateAdminUser(adminUser);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Long id) {
        AdminUser adminUser = adminUserService.findById(id);
        return ResultGenerator.genSuccessResponseResult(adminUser);
    }

    @GetMapping("/list")
    public ResponseResult list(String account) {
        List<AdminUserObjRsp> list = adminUserService.selectAllWithGroupMsg(account);
        return ResultGenerator.genSuccessResponseResult(list);
    }

    @GetMapping("/giveUserReword")
    @ResponseBody
    public ResponseResult reword( AdminUserRewordReq req,int type){
        int result = adminUserService.rewordUser(req, type);
        return ResultGenerator.genSuccessResponseResult(result);
    }
}
