package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.LotteryMapper;
import com.alipay.rebate.shop.model.Lottery;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/")
public class CustomerLotteryController {

  @Autowired
  private LotteryMapper lotteryMapper;

  @RequestMapping("/selectCurrentUserLottery")
  public ResponseResult<Lottery> selectCurrentUserLottery(){
    Lottery lottery = lotteryMapper.selectCurrentUseLottery();
    return ResultGenerator.genSuccessResponseResult(lottery);
  }

}
