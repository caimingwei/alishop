package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.CommissionLevelSettings;
import com.alipay.rebate.shop.pojo.common.CommonPageRequest;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.CommissionLevelSettingsService;
import com.github.pagehelper.PageHelper;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/11/08.
*/
@RestController
@RequestMapping("/admin/commission/level/settings")
public class CommissionLevelSettingsController {
    @Resource
    private CommissionLevelSettingsService commissionLevelSettingsService;
    @PostMapping("/add")
    public ResponseResult add(@RequestBody CommissionLevelSettings commissionLevelSettings) {
        Date date = new Date();
        commissionLevelSettings.setCreateTime(date);
        commissionLevelSettings.setUpdateTime(date);
        commissionLevelSettingsService.save(commissionLevelSettings);
        return ResultGenerator.genSuccessResponseResult(commissionLevelSettings.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Integer id) {
        commissionLevelSettingsService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody CommissionLevelSettings commissionLevelSettings) {
        Date date = new Date();
        commissionLevelSettings.setUpdateTime(date);
        commissionLevelSettingsService.update(commissionLevelSettings);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/detail")
    public ResponseResult<CommissionLevelSettings> detail(@RequestParam Integer id) {
        CommissionLevelSettings commissionLevelSettings = commissionLevelSettingsService.findById(id);
        return ResultGenerator.genSuccessResponseResult(commissionLevelSettings);
    }

    @GetMapping("/list")
    public ResponseResult<List<CommissionLevelSettings>> list(
        CommonPageRequest request
    ) {
        PageHelper.startPage(request.getPageNo(), request.getPageSize());
        List<CommissionLevelSettings> list = commissionLevelSettingsService.findAllOrderByBegin();
        return ResultGenerator.genSuccessResponseResult(list);
    }
}
