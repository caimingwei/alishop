package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.BrowseAd;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerBrowseAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/")
public class CustomerBrowseAdController {

  @Autowired
  CustomerBrowseAdService browseAdService;

  @RequestMapping("getBrowseAdSettings")
  public ResponseResult<BrowseAd> getBrowseAdSettings(){
    BrowseAd browseAd = browseAdService.findById(1);
    return ResultGenerator.genSuccessResponseResult(browseAd);
  }

}
