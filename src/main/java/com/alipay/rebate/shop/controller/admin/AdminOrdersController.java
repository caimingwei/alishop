package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.helper.DataAnalysisHelper;
import com.alipay.rebate.shop.helper.PunishWorkOrderHelper;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.dataanalysis.DataAnalysisRsp;
import com.alipay.rebate.shop.pojo.user.admin.*;
import com.alipay.rebate.shop.scheduler.orderimport.AdminImportOrders;
import com.alipay.rebate.shop.scheduler.orderimport.AdminImportRefundOrders;
import com.alipay.rebate.shop.scheduler.orderimport.PddOrdersImport;
import com.alipay.rebate.shop.service.admin.AdminOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;

@RestController
public class AdminOrdersController {

    private final Logger logger = LoggerFactory.getLogger(AdminOrdersController.class);

    @Autowired
    AdminImportRefundOrders adminImportRefundOrders;
    @Autowired
    AdminOrdersService ordersService;
    @Autowired
    DataAnalysisHelper dataAnalysisHelper;
    @Autowired
    PunishWorkOrderHelper punishWorkOrderHelper;

//    @Autowired
//    AdminImportOrders adminImportOrders;

    @Autowired
    AdminImportOrders adminImportOrders;

    @Autowired
    PddOrdersImport pddOrdersImport;
    @Resource
    OrdersMapper ordersMapper;

    @RequestMapping("/admin/getOrdersFromApi")
    public String getOrdersFromApi(){
        return "success";
    }

    @RequestMapping("/admin/getOrdersById/{id}")
    @ResponseBody
    public ResponseResult<Orders> getOrdersById(@PathVariable("id") String id){
        logger.debug("Get Orders id from request: {}",id);
        Orders orders = ordersService.get(id);
        return ResultGenerator.genSuccessResponseResult(orders);
    }

    @GetMapping("/admin/selectOrdersByCondition")
    @ResponseBody
    public ResponseResult<PageInfo<Orders>> selectOrdersByCondition(OrdersPageRequest ordersPageRequest){
        logger.debug("Get pOrdersPageRequest from request is: {}",ordersPageRequest);
        PageInfo<Orders> pageInfo = ordersService.selectOrdersByCondition(ordersPageRequest);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/admin/selectOrdersByDate")
    @ResponseBody
    public ResponseResult<PageInfo<Orders>> selectOrdersByDate(String date,Integer page,Integer size){
        int pageSize = size == null ? 20 : size;
        int pageNo = page == null ? 1 : page;
        PageHelper.startPage(pageNo,pageSize);
        List<Orders> ordersList = ordersMapper.selectOrdersByDate(date);
        PageInfo<Orders> pageInfo = new PageInfo<>(ordersList);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/admin/getDataAnalysis")
    @ResponseBody
    public ResponseResult<DataAnalysisRsp> getDataAnalysis(){
        DataAnalysisRsp allDataAnalysis = dataAnalysisHelper.getAllDataAnalysis();
        logger.debug("allDataAnalysis is : {}",allDataAnalysis);
        return ResultGenerator.genSuccessResponseResult(allDataAnalysis);
    }


    @PutMapping("/admin/addOrUpdateOrders")
    public ResponseResult addOrUpdateOrders(@RequestBody ImportOrdersRequest orders){
        logger.debug("orders is : {}",orders);
        adminImportOrders.addOrUpdateOrders(orders);
        return ResultGenerator.genSuccessResponseResult();
    }



    @PutMapping("/admin/updateOrdersById")
    @ResponseBody
    public ResponseResult<Integer> updateOrdersById(@RequestBody UpdateOrdersRequest updateOrdersRequest){
        int result = ordersService.updateOrdersById(updateOrdersRequest);
        return ResultGenerator.genSuccessResponseResult(result);
    }


    @PutMapping("/admin/importRefundOrders")
    public ResponseResult importRefundOrders(@RequestBody AdminImportRefundOrdersReq refundOrdersReq)
            throws ParseException {
        logger.debug("orders is : {}",refundOrdersReq);
        adminImportRefundOrders.importRefundOrders(refundOrdersReq);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/admin/punishWorkOrder")
    public ResponseResult punishWorkOrder(@RequestBody PunishWorkOrderReq req){
        logger.debug("PunishWorkOrderReq is : {}",req);
        punishWorkOrderHelper.handlePunishWorkOrder(req);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/admin/selectOrdersByIsRecord")
    @ResponseBody
    public ResponseResult<PageInfo<Orders>> selectOrdersByIsRecord(OrdersPageRequest ordersPageRequest){
        PageInfo<Orders> pageInfo = ordersService.selectOrdersByIsRecord(ordersPageRequest);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }


//    @PutMapping("/admin/importPddOrders")
//    public ResponseResult importPddOrders(@RequestBody AdminImportPddOrders pddOrders){
//        logger.debug("pddOrders is : {}",pddOrders);
//        pddOrdersImport.importPddOrders(pddOrders);
//        return ResultGenerator.genSuccessResponseResult();
//    }

}
