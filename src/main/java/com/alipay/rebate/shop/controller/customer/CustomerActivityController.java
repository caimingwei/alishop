package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.ActivityMapper;
import com.alipay.rebate.shop.model.Activity;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/")
public class CustomerActivityController {

  @Autowired
  ActivityMapper activityMapper;

  @RequestMapping("getCustActivity")
  public ResponseResult<Activity> getCustActivity(){
    Activity activity = activityMapper.selectByPrimaryKey(2);
    return ResultGenerator.genSuccessResponseResult(activity);
  }

}
