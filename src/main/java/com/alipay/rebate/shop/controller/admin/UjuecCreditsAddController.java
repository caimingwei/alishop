package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.UjuecCreditsAdd;
import com.alipay.rebate.shop.service.admin.UjuecCreditsAddService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/09/25.
*/
@RestController
@RequestMapping("/ujuec/credits/add")
public class UjuecCreditsAddController {
    @Resource
    private UjuecCreditsAddService ujuecCreditsAddService;

    @PostMapping("/add")
    public Result add(UjuecCreditsAdd ujuecCreditsAdd) {
        ujuecCreditsAddService.save(ujuecCreditsAdd);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        ujuecCreditsAddService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping("/update")
    public Result update(UjuecCreditsAdd ujuecCreditsAdd) {
        ujuecCreditsAddService.update(ujuecCreditsAdd);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        UjuecCreditsAdd ujuecCreditsAdd = ujuecCreditsAddService.findById(id);
        return ResultGenerator.genSuccessResult(ujuecCreditsAdd);
    }

    @GetMapping("/list")
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<UjuecCreditsAdd> list = ujuecCreditsAddService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
