package com.alipay.rebate.shop.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.PageFramework;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.PageDefaultSettlingRequest;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkRequest;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkResponse;
import com.alipay.rebate.shop.service.admin.AdminPageFrameworkService;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminPageFrameworkController {

  private final Logger logger = LoggerFactory.getLogger(AdminPageFrameworkController.class);

  @Autowired
  AdminPageFrameworkService pageFrameworkService;

  @Autowired
  ParameterChecker parameterChecker;

  @PostMapping("/admin/createOrUpdatePageFramework")
  @ResponseBody
  public ResponseResult<Long> createOrUpdatePage(@RequestBody PageFrameworkRequest pageFrameworkRequest){
    logger.debug("PageFrameworkRequest is : {}",pageFrameworkRequest);
    return pageFrameworkService.createOrUpdatePage(pageFrameworkRequest);
  }

  //查询所有模板
  @GetMapping("/admin/selectAllPageFramework")
  @ResponseBody
  public ResponseResult<List<PageFrameworkResponse>> selectAllPage(){
    logger.debug("Admin select all page framework ");
    return pageFrameworkService.selectAllPageFramework();
  }

  //根据isIndex查询所有模板
  @GetMapping("/admin/selectAllPageFrameworkByType/{isIndex}")
  @ResponseBody
  public ResponseResult<List<PageFramework>> selectAllPageByType(@PathVariable("isIndex") Integer isIndex){
    logger.debug("Admin select all page framework ");
    ResponseResult<List<PageFramework>> responseResult = new ResponseResult<>();
    responseResult.setData(pageFrameworkService.selectAllByType(isIndex));
    return responseResult;
  }

  @GetMapping("/admin/selectPageFramework/{id}")
  @ResponseBody
  public ResponseResult<PageFrameworkResponse> selectPageFrameworkByPageId(@PathVariable("id") Long id){
    logger.debug("Page id is : {}",id);
    return pageFrameworkService.selectPageFrameworkByPageId(id);
  }

  //设置首页模板是否默认模板
  @PutMapping("/admin/setDefulatFramework")
  @ResponseBody
  public ResponseResult<Integer> setDefulatFramework(
      @RequestBody @Valid PageDefaultSettlingRequest pageDefaultSettlingRequest
  ){
    logger.debug("PageDefaultSettlingRequest is : {}",pageDefaultSettlingRequest);
    PageFramework pageFramework = new PageFramework();
    pageFramework.setPageId(pageDefaultSettlingRequest.getPageId());
    pageFramework.setIsDefault(pageDefaultSettlingRequest.getIsDefault());
    return pageFrameworkService.updatePageFrameWork(pageFramework);
  }

  //设置模板是否更新
  @PutMapping("/admin/setIsUpdateFramework")
  @ResponseBody
  public ResponseResult<Integer> setIsUpdateFramework(
          @RequestBody JSONObject requestJson
  ){
    logger.debug("PageDefaultSettlingRequest is : {}",requestJson.toJSONString());
    PageFramework pageFramework = new PageFramework();
    pageFramework.setPageId(requestJson.getLong("pageId"));
    pageFramework.setIsUpdate(requestJson.getInteger("isUpdate"));
    return pageFrameworkService.updatePageFrameWork(pageFramework);
  }

  //设置模板是否可用
  @PutMapping("/admin/setIsUseFramework")
  @ResponseBody
  public ResponseResult<Integer> setIsUseFramework(
          @RequestBody JSONObject requestJson
          ){
    logger.debug("PageIsUseSettlingRequest is : {}",requestJson);
    PageFramework pageFramework = new PageFramework();
    pageFramework.setPageId(requestJson.getLong("pageId"));
    pageFramework.setIsCurrentUse(requestJson.getInteger("isUse"));
    return pageFrameworkService.updatePageFrameWork(pageFramework);
  }

  //查询可用所有模板
  @GetMapping("/admin/getIsUseFramework")
  @ResponseBody
  public ResponseResult getIsUseFramework(){
    List<PageFramework> pageFrameworks = pageFrameworkService.selectUsePageFramework(1);
    ResponseResult<List<PageFramework>> responseResult = new ResponseResult<>();
    responseResult.setData(pageFrameworks);
    return responseResult;
  }

  @DeleteMapping("/admin/deletePageFramework/{id}")
  @ResponseBody
  public ResponseResult<Integer> deletePageFramework(@PathVariable("id") Long id){
    logger.debug("delete page id is : {}",id);
    ResponseResult<Integer> responseResult = new ResponseResult<>();
    pageFrameworkService.deleteById(id);
    return responseResult;
  }

  @PostMapping("/admin/createPageIndex")
  @ResponseBody
  public ResponseResult<Integer> createPageIndex(@RequestBody PageFrameworkRequest pageFrameworkRequest){
    logger.debug("pageFrameworkRequest is : {}",pageFrameworkRequest);
    int pageIndex = pageFrameworkService.createPageIndex(pageFrameworkRequest);
    return ResultGenerator.genSuccessResponseResult(pageIndex);
  }
  @PutMapping("/admin/updatePageIndex")
  @ResponseBody
  public ResponseResult<Integer> updatePageIndex(@RequestBody PageFrameworkRequest pageFrameworkRequest){
    int result = pageFrameworkService.updatePageIndex(pageFrameworkRequest);
    return ResultGenerator.genSuccessResponseResult(result);
  }

  @GetMapping("/admin/selectPageIndex")
  @ResponseBody
  public ResponseResult selectPageIndex(){
    List<PageFramework> pageFrameworks = pageFrameworkService.selectPageIndex();
    return ResultGenerator.genSuccessResponseResult(pageFrameworks);
  }
}
