package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.ImgGroup;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.ImgGroupService;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2020/02/02.
*/
@RestController
@RequestMapping("/admin/img/group")
public class ImgGroupController {
    @Resource
    private ImgGroupService imgGroupService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody ImgGroup imgGroup) {
        Date date = new Date();
        imgGroup.setCreateTime(date);
        imgGroup.setUpdateTime(date);
        imgGroupService.save(imgGroup);
        return ResultGenerator.genSuccessResponseResult(imgGroup.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Long id) {
        imgGroupService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody ImgGroup imgGroup) {
        Date date = new Date();
        imgGroup.setUpdateTime(date);
        imgGroupService.updateImgGroup(imgGroup);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Long id) {
        ImgGroup imgGroup = imgGroupService.findById(id);
        return ResultGenerator.genSuccessResponseResult(imgGroup);
    }

    @GetMapping("/list")
    public ResponseResult list() {
        List<ImgGroup> list = imgGroupService.findAll();
        return ResultGenerator.genSuccessResponseResult(list);
    }
}
