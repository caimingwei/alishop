package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.TaskSettingsService;
import com.github.pagehelper.PageInfo;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/10/09.
*/
@RestController
@RequestMapping("/admin/task/settings")
public class TaskSettingsController {
    @Resource
    private TaskSettingsService taskSettingsService;

    @Autowired
    private ParameterChecker parameterChecker;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody TaskSettings taskSettings) {
        taskSettingsService.addTaskSettings(taskSettings);
        return ResultGenerator.genSuccessResponseResult(taskSettings.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Integer id) {
        taskSettingsService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody TaskSettings taskSettings) {
        taskSettingsService.updateTaskSettings(taskSettings);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Integer id) {
        TaskSettings taskSettings = taskSettingsService.findById(id);
        return ResultGenerator.genSuccessResponseResult(taskSettings);
    }


    @GetMapping("/allSettings")
    public ResponseResult allSettings(Integer pageNo, Integer pageSize,
        @RequestParam("type") Integer type
        ) {
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        PageInfo<TaskSettings> data = taskSettingsService.selectAllSettings(pageNo,pageSize,type);
        return ResultGenerator.genSuccessResponseResult(data);
    }
}
