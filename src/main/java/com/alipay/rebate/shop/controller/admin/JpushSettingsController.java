package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.JpushSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.JpushSettingsService;
import com.alipay.rebate.shop.utils.JpushUtils;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/11/30.
*/
@RestController
@RequestMapping("/admin/jpush/settings")
public class JpushSettingsController {

    private Logger logger = LoggerFactory.getLogger(JpushSettingsController.class);

    @Resource
    private JpushSettingsService jpushSettingsService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody JpushSettings jpushSettings) {
        Date date = new Date();
        jpushSettings.setCreateTime(date);
        jpushSettings.setUpdateTime(date);
        jpushSettingsService.save(jpushSettings);
        return ResultGenerator.genSuccessResponseResult(jpushSettings.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Integer id) {
        jpushSettingsService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody JpushSettings jpushSettings) {
        Date date = new Date();
        jpushSettings.setUpdateTime(date);
        jpushSettingsService.update(jpushSettings);
        return ResultGenerator.genSuccessResponseResult(jpushSettings.getId());
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Integer id) {
        JpushSettings jpushSettings = jpushSettingsService.findById(id);
        return ResultGenerator.genSuccessResponseResult(jpushSettings);
    }

    @GetMapping("/list")
    public ResponseResult list() {
        List<JpushSettings> list = jpushSettingsService.findAll();
        return ResultGenerator.genSuccessResponseResult(list);
    }

    @PostMapping("/pushAppMessageToAllUser")
    public ResponseResult pushAppMessageToAllUser(@RequestBody JpushSettings jpushSettings){
        logger.debug("jpushSettings is : {}",jpushSettings);
        if(StringUtils.isEmpty(jpushSettings.getAppContent())){
            throw new BusinessException("推送内容不能为空", StatusCode.PARAMETER_CHECK_ERROR);
        }

        jpushSettingsService.pushToAllUser(jpushSettings);

        return ResultGenerator.genSuccessResponseResult();
    }
}
