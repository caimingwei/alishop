package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.PlusOrReduceVirMoneyReq;
import com.alipay.rebate.shop.service.customer.CustomerLotteryTimesRecordService;
import com.alipay.rebate.shop.service.customer.CustomerTaskSettingsService;
import com.alipay.rebate.shop.utils.JwtUtils;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/lottery/times/record")
public class CustomerLotteryTimesRecordController {

  private Logger logger = LoggerFactory.getLogger(CustomerLotteryTimesRecordController.class);
  @Autowired
  private CustomerLotteryTimesRecordService lotteryTimesRecordService;
  @Autowired
  private ParameterChecker parameterChecker;

  @GetMapping("/curdayTimes")
  public ResponseResult<Integer> curdayTimes(
      HttpServletRequest request
  ) {
   long userId = JwtUtils.getUserIdFromToken(request);
   Integer data = lotteryTimesRecordService.selectCurdayTimes(userId);
   return ResultGenerator.genSuccessResponseResult(data);
  }

  @PostMapping("/doLottery")
  public ResponseResult<Integer> addOrUpdateCurdayTimes(
      HttpServletRequest request,
      @RequestBody PlusOrReduceVirMoneyReq req
  ) {
    long userId = JwtUtils.getUserIdFromToken(request);
    if(req != null && req.isFlag()){
      parameterChecker.checkParams(req);
      parameterChecker.checkPlusOrReduceVirMoneyReq(req);
    }
    logger.debug("PlusOrReduceVirMoneyReq is : {}",req);
    Integer data = lotteryTimesRecordService.addOrUpdateCurDayTimes(userId,req);
    return ResultGenerator.genSuccessResponseResult(data);
  }
}
