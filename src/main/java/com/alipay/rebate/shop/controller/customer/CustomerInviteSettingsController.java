package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.InviteSettingsMapper;
import com.alipay.rebate.shop.model.InviteSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/")
public class CustomerInviteSettingsController {

  @Autowired
  private InviteSettingsMapper inviteSettingsMapper;

  @RequestMapping("getInviteSettings")
  public ResponseResult<InviteSettings> getInviteSettings(){
    InviteSettings inviteSettings = inviteSettingsMapper.selectByPrimaryKey(1);
    return ResultGenerator.genSuccessResponseResult(inviteSettings);
  }
}
