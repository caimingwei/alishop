package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.TaskDoRecord;
import com.alipay.rebate.shop.service.admin.TaskDoRecordService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/10/17.
*/
@RestController
@RequestMapping("/task/do/record")
public class TaskDoRecordController {
    @Resource
    private TaskDoRecordService taskDoRecordService;

    @PostMapping("/add")
    public Result add(TaskDoRecord taskDoRecord) {
        taskDoRecordService.save(taskDoRecord);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        taskDoRecordService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/update")
    public Result update(TaskDoRecord taskDoRecord) {
        taskDoRecordService.update(taskDoRecord);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        TaskDoRecord taskDoRecord = taskDoRecordService.findById(id);
        return ResultGenerator.genSuccessResult(taskDoRecord);
    }

    @PostMapping("/list")
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<TaskDoRecord> list = taskDoRecordService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
