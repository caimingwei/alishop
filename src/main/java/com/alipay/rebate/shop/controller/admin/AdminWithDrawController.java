package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.WithDrawMapper;
import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.pojo.alipay.WithDeawTotal;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.WithDrawPageRequest;
import com.alipay.rebate.shop.pojo.user.admin.WithDrawUto;
import com.alipay.rebate.shop.service.admin.AdminCustomerUserService;
import com.alipay.rebate.shop.service.admin.AdminWithdrawService;
import com.github.pagehelper.PageInfo;

import java.math.BigDecimal;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminWithDrawController {

    private final Logger logger = LoggerFactory.getLogger(AdminWithDrawController.class);

    @Autowired
    AdminWithdrawService withDrawService;
    @Autowired
    ParameterChecker parameterChecker;
    @Autowired
    AdminCustomerUserService userService;

    @GetMapping(name = "/admin/selectWithDrawRecordByUserId",consumes = "application/json")
    @ResponseBody
    public List<WithDraw> selectWithDrawRecordByUserId(Long userId){
        logger.debug("Receive user id from request is: {}",userId);
        return withDrawService.selectWithDrawRecordByUserId(userId);
    }

    @GetMapping("/admin/selectWithDrawById/{id}")
    @ResponseBody
    public ResponseResult<WithDraw> selectWithDrawById(@PathVariable("id") Long id){
        logger.debug("Receive id from request is: {}",id);
        WithDraw withDraw =  withDrawService.get(id);
        return ResultGenerator.genSuccessResponseResult(withDraw);
    }

    @PutMapping("/admin/updateWithDrawRecord")
    @ResponseBody
    public ResponseResult<Integer> updateWithDrawRecord(@RequestBody WithDraw withDraw){
        logger.debug("Receive withDraw from request is: {}",withDraw);
        int result = withDrawService.updateWithDrawRecord(withDraw);
        return ResultGenerator.genSuccessResponseResult(result);
    }


//    @PutMapping("/admin/updateWithDrawRecord")
//    @ResponseBody
//    public ResponseResult<Integer> updateWithDrawRecord(@RequestBody WithDraw withDraw){
//        logger.debug("Receive withDraw from request is: {}",withDraw);
//        ResponseResult<Integer> responseResult = new ResponseResult<>();
//        if(withDraw.getStatus()!=1){
//            withDraw.setHandleTime(new Date());
//            if(withDraw.getStatus()==2){
//                WithDraw withDraw1 = withDrawService.get(withDraw.getWithdrawId());
//                if(withDraw1!=null){
//                    User user = userService.get(withDraw1.getUserId());
//                    if(withDraw1.getType()==0&&user.getUserAmount().compareTo(withDraw1.getMoney())==1){
//                        user.setUserAmount(user.getUserAmount().subtract(withDraw1.getMoney()));
//                        userService.update(user);
//                    }else if(withDraw1.getType()==1&&BigDecimal.valueOf(user.getUserIntgeralTreasure()).compareTo(withDraw1.getMoney().multiply(BigDecimal.valueOf(Long.valueOf("10000"))))==1){
//                        user.setUserIntgeralTreasure((BigDecimal.valueOf(user.getUserIntgeralTreasure()).subtract(withDraw1.getMoney().multiply(BigDecimal.valueOf(Long.valueOf("10000"))))).longValue());
//                        userService.update(user);
//                    }else{
//                        responseResult.setStatus(6000);
//                        responseResult.setMsg("提现失败");
//                        return responseResult;
//                    }
//                }else{
//                    responseResult.setStatus(6000);
//                    responseResult.setMsg("提现失败");
//                    return responseResult;
//                }
//            }
//        }
//        int result = withDrawService.update(withDraw);
//        responseResult.setData(result);
//        return responseResult;
//    }

    @DeleteMapping("/admin/deleteWithDrawRecord")
    @ResponseBody
    public Integer deleteWithDrawRecord(Long id){
        logger.debug("Receive id from request is: {}",id);
        return withDrawService.delete(id);
    }

    @GetMapping("/admin/searchWithDrawByCondition")
    @ResponseBody
    public ResponseResult<PageInfo<WithDrawUto>> searchWithDrawByCondition(
        WithDrawPageRequest withDrawPageRequest
    ){
        logger.debug("Receive WithDrawPageRequest from request is: {}",withDrawPageRequest);
        parameterChecker.checkParams(withDrawPageRequest);
        PageInfo<WithDrawUto> pageInfo =  withDrawService.selectWithDrawByCondition(withDrawPageRequest);
//        PageInfo<WithDrawUto> pageInfo = withDrawService.selectWithDrawRecordByType(withDrawPageRequest);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/admin/selectWithDrawSuccessMoneyNum")
    public ResponseResult<WithDeawTotal> selectWithDrawSuccessMoneyNum(){
        WithDeawTotal withDeawTotal = withDrawService.selectWithDrawSuccessMoneyNum();
        return ResultGenerator.genSuccessResponseResult(withDeawTotal);
    }

    @GetMapping("/admin/selectNotHandleWithDraw")
    @ResponseBody
    public ResponseResult<PageInfo<WithDrawUto>> selectNotHandleWithDraw(WithDrawPageRequest withDrawPageRequest){
        PageInfo<WithDrawUto> pageInfo = withDrawService.selectNotHandleWithDraw(withDrawPageRequest);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }


        @GetMapping("/admin/selectHandleWithDraw")
    @ResponseBody
    public ResponseResult<PageInfo<WithDrawUto>> selectHandleWithDraw(WithDrawPageRequest withDrawPageRequest){
        PageInfo<WithDrawUto> pageInfo = withDrawService.selectHandleWithDraw(withDrawPageRequest);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }


}
