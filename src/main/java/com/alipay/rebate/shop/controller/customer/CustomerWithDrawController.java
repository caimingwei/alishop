package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.pojo.entitysettings.WithDrawSettings;
import com.alipay.rebate.shop.service.admin.EntitySettingsService;
import com.alipay.rebate.shop.service.customer.CustomerUserService;
import com.alipay.rebate.shop.service.customer.CustomerWithdrawService;
import com.alipay.rebate.shop.utils.JwtUtils;
import com.alipay.rebate.shop.constants.CommonConstant;
import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.AmountWithdrawRequest;
import com.alipay.rebate.shop.pojo.user.customer.UitWithdrawRequest;
import com.alipay.rebate.shop.pojo.user.customer.UserWithdrawResponse;
import com.github.pagehelper.PageInfo;
import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

@Controller
public class CustomerWithDrawController {

    private Logger logger = LoggerFactory.getLogger(CustomerWithDrawController.class);

    @Autowired
    CustomerUserService userService;

    @Autowired
    CustomerWithdrawService withdrawService;

    @Autowired
    ParameterChecker parameterChecker;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private EntitySettingsService entitySettingsService;

    @PostMapping("/customer/amountWithDraw")
    @ResponseBody
    public ResponseResult<UserWithdrawResponse> amountWithDraw(
            @RequestBody @Valid AmountWithdrawRequest amountWithdrawRequest,
            HttpServletRequest request
    ){
//        long userId = JwtUtils.getUserIdFromToken(request);
//        checkRepeatRequest(userId);
//        logger.debug("AmountWithdrawRequest is: {}",amountWithdrawRequest);
//        WithDraw withDraw = new WithDraw();
//        withDraw.setMoney(amountWithdrawRequest.getWithDrawAmount());
//        withDraw.setUserId(userId);
//        UserWithdrawResponse data =  withdrawService.withdrawAmount(withDraw);
//        return ResultGenerator.genSuccessResponseResult(data);
        throw new BusinessException("请更新最新版本提现",StatusCode.INTERNAL_ERROR);
    }

    @PostMapping("/customer/uitWithDraw")
    @ResponseBody
    public ResponseResult<UserWithdrawResponse> uitWithDraw(
            @RequestBody @Valid UitWithdrawRequest uitWithdrawRequest,
            HttpServletRequest request
    ){
//        logger.debug("UitWithdrawRequest is : {}",uitWithdrawRequest);
//        long userId = JwtUtils.getUserIdFromToken(request);
//        checkRepeatRequest(userId);
//        WithDraw withDraw = new WithDraw();
//        Long uitCount = uitWithdrawRequest.getWithDrawUitCount();
//        BigDecimal radix = new BigDecimal(100);
//        BigDecimal money = new BigDecimal(uitCount).divide(radix);
//        withDraw.setMoney(money);
//        withDraw.setUserId(userId);
//        UserWithdrawResponse data =  withdrawService.withdrawUit(withDraw);
//        return ResultGenerator.genSuccessResponseResult(data);
        throw new BusinessException("请更新最新版本提现",StatusCode.INTERNAL_ERROR);
    }

    private void checkRepeatRequest(Long userId){
        String key = String.valueOf(userId);
        String values = stringRedisTemplate.opsForValue().get(key);
        logger.debug("values is : {}",values);
        if (values != null) {
            throw new BusinessException("请求过于频繁", StatusCode.INTERNAL_ERROR);
        }
        stringRedisTemplate.opsForValue().set(key,key,5, TimeUnit.SECONDS);
    }

    @GetMapping("/customer/getUserUitWithDraw")
    @ResponseBody
    public ResponseResult<PageInfo<WithDraw>> getUserUitWithDraw(
        HttpServletRequest request,
        Integer pageNo,
        Integer pageSize
    ){
        long userId = JwtUtils.getUserIdFromToken(request);
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        PageInfo<WithDraw> withDraw = withdrawService.selectUserUitWithDraw(userId,pageNo, pageSize);
        return ResultGenerator.genSuccessResponseResult(withDraw);
    }

    @GetMapping("/customer/getUserAmWithDraw")
    @ResponseBody
    public ResponseResult<PageInfo<WithDraw>> getUserAmWithDraw(
        HttpServletRequest request,
        Integer pageNo,
        Integer pageSize
    ){
        long userId = JwtUtils.getUserIdFromToken(request);
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        PageInfo<WithDraw> withDraw = withdrawService.selectUserAmWithDraw(userId,pageNo,pageSize);
        return ResultGenerator.genSuccessResponseResult(withDraw);
    }

    @GetMapping("/customer/getUserWithDraw")
    @ResponseBody
    public ResponseResult<PageInfo<WithDraw>> getUserWithDraw(
        HttpServletRequest request,
        Integer pageNo,
        Integer pageSize
    ){
        long userId = JwtUtils.getUserIdFromToken(request);
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        PageInfo<WithDraw> withDraw = withdrawService.selectUserWithDraw(userId,pageNo,pageSize);
        return ResultGenerator.genSuccessResponseResult(withDraw);
    }

    @GetMapping("/customer/getWithDrawMessage")
    @ResponseBody
    public ResponseResult<UserWithdrawResponse> getWithDrawMessage(HttpServletRequest request){
        long userId = JwtUtils.getUserIdFromToken(request);
        UserWithdrawResponse data =  withdrawService.selectWithDrawDetailForCustomer(userId);
        return ResultGenerator.genSuccessResponseResult(data);
    }

    @GetMapping("/common/withDrawSettingsDetail")
    @ResponseBody
    public ResponseResult withDrawSettingsDetail() throws IOException {
        WithDrawSettings withDrawSettings = entitySettingsService.withDrawSettingsDetail();
        return ResultGenerator.genSuccessResponseResult(withDrawSettings);
    }
}
