package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.constants.TaskSettingsConstant;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.TaskDoRecord;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerTaskDoRecordService;
import com.alipay.rebate.shop.utils.JwtUtils;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/task/do/record")
public class CustomerTaskDoRecordController {

  private Logger logger = LoggerFactory.getLogger(CustomerTaskDoRecordController.class);

  @Autowired
  private CustomerTaskDoRecordService taskDoRecordService;

  @RequestMapping("/selectNewUserAndDailyTask")
  public ResponseResult<List<TaskDoRecord>> selectNewUserAndDailyTask(HttpServletRequest request) {
    long userId = JwtUtils.getUserIdFromToken(request);
    List<TaskDoRecord> data =  taskDoRecordService.selectNewUserAndDailyTask(userId);
    return ResultGenerator.genSuccessResponseResult(data);
  }

  @PostMapping("/addDailyTask")
  public ResponseResult<Long> addDailyTask(@RequestBody TaskDoRecord taskDoRecord, HttpServletRequest request) {
    logger.debug("TaskDoRecord receive is: {}",taskDoRecord);
    long userId = JwtUtils.getUserIdFromToken(request);
    taskDoRecord.setUserId(userId);
    taskDoRecord.setType(TaskSettingsConstant.DAILY_TASK);
    Date date = new Date();
    taskDoRecord.setCreateTime(date);
    taskDoRecord.setUpdateTime(date);
    taskDoRecordService.addTaskDoRecord(taskDoRecord);
    return ResultGenerator.genSuccessResponseResult(taskDoRecord.getId());
  }
}
