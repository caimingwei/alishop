package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.JpushHistory;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.JpushHistoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/12/05.
*/
@RestController
@RequestMapping("/admin/jpush/history")
public class JpushHistoryController {

    @Resource
    private JpushHistoryService jpushHistoryService;
    @Autowired
    private ParameterChecker parameterChecker;

    @PostMapping("/add")
    public Result add(JpushHistory jpushHistory) {
        jpushHistoryService.save(jpushHistory);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        jpushHistoryService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/update")
    public Result update(JpushHistory jpushHistory) {
        jpushHistoryService.update(jpushHistory);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        JpushHistory jpushHistory = jpushHistoryService.findById(id);
        return ResultGenerator.genSuccessResult(jpushHistory);
    }

    @GetMapping("/list")
    public ResponseResult list(
        Integer pageNo,
        Integer pageSize
    ) {
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        PageHelper.startPage(pageNo,pageSize);
        List<JpushHistory> list = jpushHistoryService.selectAllOrderByIdDesc();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
