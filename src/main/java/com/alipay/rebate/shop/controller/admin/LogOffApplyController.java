package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.LogOffApply;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.logoffapply.LogoffApplySearchReq;
import com.alipay.rebate.shop.service.admin.LogOffApplyService;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2020/01/19.
*/
@RestController
@RequestMapping("/admin/log/off/apply")
public class LogOffApplyController {
    @Resource
    private LogOffApplyService logOffApplyService;

//    @PostMapping("/add")
//    public ResponseResult add(@RequestBody LogOffApply logOffApply) {
//        logOffApplyService.save(logOffApply);
//        return ResultGenerator.genSuccessResponseResult();
//    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Long id) {
        logOffApplyService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody LogOffApply logOffApply) {
        if(logOffApply.getId() == null){
            throw new BusinessException("id 不能为空", StatusCode.PARAMETER_CHECK_ERROR);
        }
        logOffApplyService.updateRecord(logOffApply);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Long id) {
        LogOffApply logOffApply = logOffApplyService.findById(id);
        return ResultGenerator.genSuccessResponseResult(logOffApply);
    }

    @GetMapping("/list")
    public ResponseResult list(LogoffApplySearchReq req) {
        List<LogOffApply> list = logOffApplyService.selectRecordByCondition(req);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
