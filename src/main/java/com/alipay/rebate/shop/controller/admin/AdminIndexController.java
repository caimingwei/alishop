package com.alipay.rebate.shop.controller.admin;


import com.alibaba.fastjson.JSONObject;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.WithDrawMapper;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.pojo.AdminIndexRsp;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.AdminOrdersService;
import com.alipay.rebate.shop.service.admin.AdminCustomerUserService;
import com.alipay.rebate.shop.service.admin.AdminUserService;
import com.alipay.rebate.shop.service.admin.AdminWithdrawService;
import com.alipay.rebate.shop.service.admin.impl.AdminOrdersServiceImpl;
import com.alipay.rebate.shop.utils.DateUtil;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/index")
@ResponseBody
public class AdminIndexController {
    private final static Logger logger = LoggerFactory.getLogger(AdminIndexController.class);
    @Autowired
    AdminCustomerUserService userService;
    @Autowired
    AdminOrdersService ordersService;
    @Autowired
    AdminWithdrawService adminWithdrawService;
    @Autowired
    AdminUserService adminUserService;

    @GetMapping("/userInfo")
    public ResponseResult<JSONObject> getUserInfo(){
        JSONObject json = new JSONObject();
        Map<String,Object> map = new HashMap();
        map.put("userNickName","");
        int totalUserNumber = userService.selectTotalCountByCondition(map);
        map = new HashMap();

        map.put("registerBeginTime", DateUtil.initZoreDateByDay(new Date()));
        map.put("registerEndTime",new Date());
        int todayRegisterUserNumber = userService.selectTotalCountByCondition(map);
        map = new HashMap();
        Date d = new Date();
        d.setTime(new Date().getTime()-24*60*60*1000);
        map.put("registerBeginTime", DateUtil.initZoreDateByDay(d));
        map.put("registerEndTime",DateUtil.initZoreDateByDay(new Date()));
        int yesterdayRegisterUserNumber = userService.selectTotalCountByCondition(map);
        json.put("totalUserNumber",totalUserNumber);
        json.put("todayRegisterUserNumber",todayRegisterUserNumber);
        json.put("yesterdayRegisterUserNumber",yesterdayRegisterUserNumber);
        return ResultGenerator.genSuccessResponseResult(json);
    }



    @GetMapping("/orderInfo")
    public ResponseResult<JSONObject> orderInfo(){
        JSONObject json = new JSONObject();
        Map<String,Object> map = new HashMap();
        Map payedMap = ordersService.selectPayedOrderPriceByTime(DateUtil.initZoreDateByDay(new Date()),new Date());
        Map earnedMap = ordersService.selectEarnedOrderByTime(DateUtil.initZoreDateByDay(new Date()),new Date());
        Map withdrawMap = adminWithdrawService.selectApplyNumberByTime(DateUtil.initZoreDateByDay(new Date()),new Date());
        json.put("payedPrice",payedMap.get("payedPrice"));
        json.put("payedNumber",payedMap.get("payedNumber"));
        json.put("earnedNumber",earnedMap.get("earnedNumber"));
        json.put("applyNumber",withdrawMap.get("applyNumber"));
        return ResultGenerator.genSuccessResponseResult(json);
    }

    @GetMapping("/indexInfo")
    public ResponseResult<AdminIndexRsp> indexInfo(){
        AdminIndexRsp rsp = adminUserService.selectIndexInfo();
        return ResultGenerator.genSuccessResponseResult(rsp);
    }
}
