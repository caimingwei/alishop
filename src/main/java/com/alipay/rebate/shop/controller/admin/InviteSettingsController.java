package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.InviteSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.InviteSettingsService;
import java.util.Date;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/09/21.
*/
@RestController
@RequestMapping("/admin/inviteSettings")
public class InviteSettingsController {
    @Resource
    private InviteSettingsService inviteSettingsService;

    private final Logger logger = LoggerFactory.getLogger(InviteSettingsController.class);

    @PutMapping("/update")
    public ResponseResult update(@RequestBody InviteSettings inviteSettings) {
        inviteSettings.setUpdateTime(new Date());
        inviteSettings.setId(1);
        inviteSettingsService.update(inviteSettings);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail")
    public ResponseResult detail() {
        InviteSettings inviteSettings = inviteSettingsService.findById(1);
        return ResultGenerator.genSuccessResponseResult(inviteSettings);
    }

}
