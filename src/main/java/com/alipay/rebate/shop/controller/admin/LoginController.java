package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.AdminUserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.AccountLoginRequest;
import com.alipay.rebate.shop.pojo.user.TokenRsp;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserObjRsp;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import com.alipay.rebate.shop.service.admin.AdminUserService;
import com.alipay.rebate.shop.utils.JwtUtils;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
public class LoginController {

  private Logger logger = LoggerFactory.getLogger(LoginController.class);

  @Autowired
  private AdminUserService adminUserService;
  @Autowired
  private AdminUserMapper adminUserMapper;

  /**
   * 后台用户登陆
   * @return
   */
  @PostMapping("/common/adminLogin")
  @ResponseBody
  public ResponseResult adminLogin(
      @RequestBody @Valid
          AccountLoginRequest accountLoginRequest,
      HttpServletRequest request,
      HttpServletResponse response){
    // 获取主题信息
    Subject subject = SecurityUtils.getSubject();
    logger.debug("Login subject is : {}",subject);
    logger.debug("Login AccountLoginRequest is : {}",accountLoginRequest);
    UserDto userDto = new UserDto();
    userDto.setAccount(accountLoginRequest.getAccount());
    userDto.setPassword(accountLoginRequest.getPassword().toCharArray());
    // 后台账号密码登陆
    AdminUserObjRsp adminUserObjRsp =  adminUserService.adminLogin(userDto,request,response,subject);
    return ResultGenerator.genSuccessResponseResult(adminUserObjRsp);
  }

  /**
   * 后台用户登陆
   * @return
   */
  @GetMapping("/admin/getNewToken")
  @ResponseBody
  public ResponseResult getNewToken(
      HttpServletRequest request){
    long userId = JwtUtils.getUserIdFromToken(request);
    TokenRsp tokenRsp = adminUserService.getNewToken(userId);
    return ResultGenerator.genSuccessResponseResult(tokenRsp);
  }

  @GetMapping("/common/getNewTokenTwo")
  @ResponseBody
  public ResponseResult getNewTokenTwo(String account){
    UserDto userDto = adminUserMapper.selectUserDtoInfoForAdminLogin(account);
    if (userDto == null){
      throw new BusinessException("账号不存在", StatusCode.USER_NOT_EXISTS);
    }
    TokenRsp newToken = adminUserService.getNewToken(userDto.getUserId());
    return ResultGenerator.genSuccessResponseResult(newToken);
  }
}
