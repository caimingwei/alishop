package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.SuperCategory;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.dataoke.SuperCategoryRes;
import com.alipay.rebate.shop.pojo.dataoke.SuperCategoryResp;
import com.alipay.rebate.shop.service.customer.impl.CustomerSuperCategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
* Created by CodeGenerator on 2020/05/25.
*/
@RestController
@RequestMapping("/common/super/category")
public class CustomerSuperCategoryController {
    @Autowired
    private CustomerSuperCategoryServiceImpl superCategoryService;

    @PutMapping("/insertOrUpdateSuperCategory")
    public ResponseResult<Integer> insertOrUpdateSuperCategory(){
        int result = superCategoryService.insertOrUpdateSuperCategory();
        return ResultGenerator.genSuccessResponseResult(result);
    }

    @GetMapping("/getAdminSuperCategory")
    public ResponseResult<List<SuperCategory>> selectAllSuperCategory(){
        List<SuperCategory> respList = superCategoryService.selectAllSuperCategory();
        return ResultGenerator.genSuccessResponseResult(respList);
    }
}
