package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.PunishOrders;
import com.alipay.rebate.shop.pojo.order.PunishOrdersRsp;
import com.alipay.rebate.shop.service.admin.PunishOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2020/06/04.
*/
@RestController
@RequestMapping("/admin/punish/orders")
public class PunishOrdersController {
    @Resource
    private PunishOrdersService punishOrdersService;

    @PostMapping("/add")
    public Result add(PunishOrders punishOrders) {
        punishOrdersService.save(punishOrders);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        punishOrdersService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping("/update")
    public Result update(PunishOrders punishOrders) {
        punishOrdersService.update(punishOrders);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        PunishOrders punishOrders = punishOrdersService.findById(id);
        return ResultGenerator.genSuccessResult(punishOrders);
    }

    @GetMapping("/list")
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "20") Integer size,String tradeId,Long userId) {
        PageHelper.startPage(page, size);
        List<PunishOrdersRsp> punishOrdersRsps = punishOrdersService.selectAllPunishOrders(tradeId, userId);
        PageInfo pageInfo = new PageInfo(punishOrdersRsps);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
