package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.ActivityPop;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.ActivityPopService;
import java.util.Date;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/09/05.
*/
@RestController
@RequestMapping("/admin/activityPop")
public class ActivityPopController {
    private final static Logger logger = LoggerFactory.getLogger(ActivityController.class);
    @Resource
    private ActivityPopService activityPopService;

//    @PostMapping("/add")
//    public ResponseResult add(@RequestBody ActivityPop activityPop) {
//        ResponseResult<ActivityPop> responseResult = new ResponseResult<>();
//        try{
//            activityPop.setCreateTime(new Date());
//            activityPop.setUpdateTime(new Date());
//            activityPopService.save(activityPop);
//            responseResult.setMsg("添加成功");
//        }catch (Exception e){
//            logger.error("activityPop add error : {}",e.toString());
//            responseResult.setMsg("服务器异常");
//            responseResult.setStatus(5000);
//        }
//        return responseResult;
//    }
//
//    @DeleteMapping("/delete/{id}")
//    public ResponseResult delete(@PathVariable("id") Integer id) {
//        ResponseResult<ActivityPop> responseResult = new ResponseResult<>();
//        try{
//            activityPopService.deleteById(id);
//            responseResult.setMsg("删除成功");
//        }catch (Exception e){
//            logger.error("activityPop delete error : {}",e.toString());
//            responseResult.setMsg("服务器异常");
//            responseResult.setStatus(5000);
//        }
//        return responseResult;
//    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody ActivityPop activityPop) {
        activityPop.setUpdateTime(new Date());
        activityPop.setId(1);
        activityPopService.update(activityPop);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail")
    public ResponseResult detail() {
        ActivityPop activityPop = activityPopService.findById(1);
        return ResultGenerator.genSuccessResponseResult(activityPop);
    }
//
//    @GetMapping("/list")
//    public ResponseResult<PageInfo<ActivityPop>> list( @RequestParam(defaultValue = "0") Integer page,
//                                                    @RequestParam(defaultValue = "0") Integer size) {
//        PageHelper.startPage(page, size);
//        List<ActivityPop> list = activityPopService.findAll();
//        ResponseResult<PageInfo<ActivityPop>> responseResult = new ResponseResult<>();
//        PageInfo pageInfo = new PageInfo(list);
//        responseResult.setData(pageInfo);
//        return responseResult;
//    }
}
