package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerUserIncomeService;
import com.alipay.rebate.shop.utils.JwtUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/income/")
public class CustomerUserIncomeController {

  @Autowired
  private CustomerUserIncomeService userIncomeService;
  @Autowired
  private ParameterChecker parameterChecker;

  @RequestMapping("selectUserAccountDetailIncome")
  public ResponseResult<PageInfo<UserIncome>> selectUserAccountDetailIncome(
      Integer pageNo, Integer pageSize,
      HttpServletRequest request
      ){
    pageNo = parameterChecker.checkAndGetPageNo(pageNo);
    pageSize = parameterChecker.checkAndGetPageSize(pageSize);
    Long userId = JwtUtils.getUserIdFromToken(request);
    PageInfo<UserIncome> pageInfo = userIncomeService.selectUserAccountDetailIncome(userId,pageNo,pageSize);
    return ResultGenerator.genSuccessResponseResult(pageInfo);
  }

  @RequestMapping("selectUserOrdersDetailIncome")
  public ResponseResult<PageInfo<UserIncome>> selectUserOrdersDetailIncome(
      Integer pageNo, Integer pageSize,
      HttpServletRequest request
  ){
    pageNo = parameterChecker.checkAndGetPageNo(pageNo);
    pageSize = parameterChecker.checkAndGetPageSize(pageSize);
    Long userId = JwtUtils.getUserIdFromToken(request);
    PageInfo<UserIncome> pageInfo = userIncomeService.selectUserOrdersDetailIncome(userId,pageNo,pageSize);
    return ResultGenerator.genSuccessResponseResult(pageInfo);
  }


  @RequestMapping("selectPddOrdersDetailIncome")
  public ResponseResult<PageInfo<UserIncome>> selectPddOrdersDetailIncome(
          Integer pageNo, Integer pageSize,
          HttpServletRequest request
  ){
    pageNo = parameterChecker.checkAndGetPageNo(pageNo);
    pageSize = parameterChecker.checkAndGetPageSize(pageSize);
    Long userId = JwtUtils.getUserIdFromToken(request);
    PageInfo<UserIncome> pageInfo = userIncomeService.selectPddOrdersDetailIncome(userId,pageNo,pageSize);
    return ResultGenerator.genSuccessResponseResult(pageInfo);
  }

  @RequestMapping("selectJdOrdersDetailIncome")
  public ResponseResult<PageInfo<UserIncome>> selectJdOrdersDetailIncome(Integer pageNo, Integer pageSize,
                                                                          HttpServletRequest request){
    pageNo = parameterChecker.checkAndGetPageNo(pageNo);
    pageSize = parameterChecker.checkAndGetPageSize(pageSize);
    Long userId = JwtUtils.getUserIdFromToken(request);
    PageInfo<UserIncome> userIncomePageInfo = userIncomeService.selectJdOrdersDetailIncome(userId, pageNo, pageSize);
    return ResultGenerator.genSuccessResponseResult(userIncomePageInfo);
  }

  @RequestMapping("selectMeituanOrdersDetailIncome")
  public ResponseResult<PageInfo<UserIncome>> selectMeituanOrdersDetailIncome(Integer pageNo, Integer pageSize,
                                                                         HttpServletRequest request){
    pageNo = parameterChecker.checkAndGetPageNo(pageNo);
    pageSize = parameterChecker.checkAndGetPageSize(pageSize);
    Long userId = JwtUtils.getUserIdFromToken(request);
    PageInfo<UserIncome> userIncomePageInfo = userIncomeService.selectMeituanOrdersDetailIncome(userId, pageNo, pageSize);
    return ResultGenerator.genSuccessResponseResult(userIncomePageInfo);
  }

}
