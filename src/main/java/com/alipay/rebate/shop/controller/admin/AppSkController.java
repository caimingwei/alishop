package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.AppSk;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.AppSkService;
import com.alipay.rebate.shop.utils.AliPayUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/08/28.
*/
@RestController
@RequestMapping("/admin/appSk")
@ResponseBody
public class AppSkController {
    @Resource
    private AppSkService appSkService;

    private final Logger logger = LoggerFactory.getLogger(AppSkController.class);

    @PostMapping("/add")
    public ResponseResult add(@RequestBody AppSk appSk) {
        appSk.setCreateTime(new Date());
        appSk.setUpdateTime(new Date());
        Long sk = new Date().getTime();
        appSk.setSecretKey(AliPayUtil.getRandomChar(6));
        appSkService.save(appSk);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id) {
        appSkService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody AppSk appSk) {
        appSk.setUpdateTime(new Date());
        appSkService.update(appSk);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail/{id}")
    public ResponseResult detail(@PathVariable("id") Integer id) {
        AppSk appSk = appSkService.findById(id);
        return ResultGenerator.genSuccessResponseResult(appSk);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<AppSk>> list( @RequestParam(defaultValue = "0") Integer page,
                                                     @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<AppSk> list = appSkService.findAll();
        PageInfo<AppSk> pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
