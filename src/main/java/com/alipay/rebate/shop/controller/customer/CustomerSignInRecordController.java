package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.SignInReq;
import com.alipay.rebate.shop.service.customer.CustomerSignInRecordService;
import com.alipay.rebate.shop.utils.JwtUtils;
import java.text.ParseException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/09/21.
*/
@RestController
@RequestMapping("/customer/sign/in/record")
public class CustomerSignInRecordController {

    private Logger logger = LoggerFactory.getLogger(CustomerSignInRecordController.class);
    @Resource
    private CustomerSignInRecordService signInRecordService;
    @Resource
    private ParameterChecker parameterChecker;

    @PostMapping("/signIn")
    public ResponseResult<Integer> signIn(@RequestBody SignInReq signInReq,
        HttpServletRequest request) throws ParseException {
        logger.debug("SignInReq is : {}",signInReq);
        long userId = JwtUtils.getUserIdFromToken(request);
        parameterChecker.checkParams(signInReq);
        signInRecordService.signIn(userId,signInReq);
        return ResultGenerator.genSuccessResponseResult(null);
    }
}
