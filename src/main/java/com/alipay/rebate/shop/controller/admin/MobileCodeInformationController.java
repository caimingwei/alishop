package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.MobileCodeInformationMapper;
import com.alipay.rebate.shop.helper.MobileCodeInformationHelper;
import com.alipay.rebate.shop.model.MobileCodeInformation;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.mobilecode.CodePageInformation;
import com.alipay.rebate.shop.pojo.mobilecode.MobileCodeStatistical;
import com.alipay.rebate.shop.service.admin.MobileCodeInformationService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class MobileCodeInformationController {

    @Autowired
    private MobileCodeInformationService informationService;
    @Resource
    private MobileCodeInformationHelper mobileCodeInformationHelper;

    @GetMapping("/selectAllMobileCodeInforMation")
    @ResponseBody
    public ResponseResult<PageInfo<MobileCodeInformation>> selectAllMobileCodeInforMation(CodePageInformation codePageInformation){
        PageInfo<MobileCodeInformation> pageInfo = informationService.selectAllMobileCodeInforMation(codePageInformation);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @GetMapping("/getMobileCodeRecord")
    @ResponseBody
    public ResponseResult getMobileCodeRecord(Integer start){
        List<MobileCodeStatistical> mobileCodeStatistical = mobileCodeInformationHelper.getMobileCodeRecord(start);
        return ResultGenerator.genSuccessResponseResult(mobileCodeStatistical);
    }


}
