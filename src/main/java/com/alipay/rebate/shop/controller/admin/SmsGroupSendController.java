package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.constants.SmsGroupSendConstant;
import com.alipay.rebate.shop.constants.StatusCode;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.SmsGroupSend;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.smsgoupsend.SmsGroupSendSearchReq;
import com.alipay.rebate.shop.service.admin.SmsGroupSendService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2020/01/04.
*/
@RestController
@RequestMapping("/admin/sms/group/send")
public class SmsGroupSendController {
    @Resource
    private SmsGroupSendService smsGroupSendService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody SmsGroupSend smsGroupSend) {
        checkForAdd(smsGroupSend);
        smsGroupSendService.addSmsGroupSend(smsGroupSend);
        return ResultGenerator.genSuccessResponseResult();
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Integer id) {
        smsGroupSendService.deleteSmsGroupSend(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody SmsGroupSend smsGroupSend) {
        smsGroupSendService.updateSmsGroupSend(smsGroupSend);
        return ResultGenerator.genSuccessResponseResult();
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Integer id) {
        SmsGroupSend smsGroupSend = smsGroupSendService.findById(id);
        return ResultGenerator.genSuccessResponseResult(smsGroupSend);
    }

    @GetMapping("/list")
    public ResponseResult list(SmsGroupSendSearchReq req) {
        List<SmsGroupSend> list = smsGroupSendService.searchByCondition(req);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    private void checkForAdd(SmsGroupSend smsGroupSend){
        if(smsGroupSend.getSendObj() == null){
            throw new BusinessException("发送对象不能为空", StatusCode.PARAMETER_CHECK_ERROR);
        }
        if(smsGroupSend.getSendType() == null){
            throw new BusinessException("发送类型不能为空", StatusCode.PARAMETER_CHECK_ERROR);
        }
        if(StringUtils.isEmpty(smsGroupSend.getContent())){
            throw new BusinessException("发送内容不能为空", StatusCode.PARAMETER_CHECK_ERROR);
        }
        // 如果是手机号发送
        if(smsGroupSend.getSendObj() == SmsGroupSendConstant.PHONE_OBJ){
            if(StringUtils.isEmpty(smsGroupSend.getPhoneNum())){
                throw new BusinessException("发送号码不能为空", StatusCode.PARAMETER_CHECK_ERROR);
            }
        }
        // 如果不是手机号发送，或者全员发送
        if(smsGroupSend.getSendObj() != SmsGroupSendConstant.ALL_USER_OBJ
            || smsGroupSend.getSendObj() != SmsGroupSendConstant.PHONE_OBJ
        ){
            if(smsGroupSend.getConditionDay() == null){
                throw new BusinessException("条件天数不能为空", StatusCode.PARAMETER_CHECK_ERROR);
            }
        }
        if(smsGroupSend.getSendType() == SmsGroupSendConstant.TIMING_SEND_TYPE){
            if(smsGroupSend.getSendTime() == null){
                throw new BusinessException("发送时间不能为空", StatusCode.PARAMETER_CHECK_ERROR);
            }
        }
    }

}
