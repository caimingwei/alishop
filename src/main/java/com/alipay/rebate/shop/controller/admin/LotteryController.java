package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.Lottery;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.LotteryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/08/13.
*/
@RestController
@RequestMapping("/admin/lottery")
@ResponseBody
public class LotteryController {
    private final Logger logger = LoggerFactory.getLogger(LotteryController.class);
    @Resource
    private LotteryService lotteryService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody Lottery lottery) {
        lotteryService.create(lottery);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id) {
        lotteryService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody Lottery lottery) {
        lotteryService.updateLottery(lottery);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail/{id}")
    public ResponseResult detail(@PathVariable("id") Integer id) {
        Lottery lottery = lotteryService.findById(id);
        return ResultGenerator.genSuccessResponseResult(lottery);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<Lottery>> list( @RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Lottery> list = lotteryService.findAll();
        PageInfo<Lottery> pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
