package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.helper.ExtraHelper;
import com.alipay.rebate.shop.model.Extra;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.ExtraService;
import com.alipay.rebate.shop.utils.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
* Created by CodeGenerator on 2020/04/28.
*/
@RestController
@RequestMapping("/admin/extra")
public class ExtraController {
    @Resource
    private ExtraService extraService;

    @Autowired
    private ExtraHelper extraHelper;

    @PostMapping("/add")
    public Result add(Extra extra) {
        extraService.save(extra);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        extraService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping("/update")
    public Result update(Extra extra) {
        extraService.update(extra);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        Extra extra = extraService.findById(id);
        return ResultGenerator.genSuccessResult(extra);
    }

    @GetMapping("/list")
    public ResponseResult<List<Extra>> list() {
        Date nowDate = DateUtil.getStartTimeOfToday();
        List<Extra> extras = extraHelper.selectExtra(nowDate);
        return ResultGenerator.genSuccessResponseResult(extras);
    }
}
