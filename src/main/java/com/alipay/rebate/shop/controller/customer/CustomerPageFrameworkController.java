package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.PageFrameworkMapper;
import com.alipay.rebate.shop.model.PageFramework;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkRequest;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkResponse;
import com.alipay.rebate.shop.service.customer.CustomerPageFrameworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
public class CustomerPageFrameworkController {

  private Logger logger = LoggerFactory.getLogger(CustomerPageFrameworkController.class);

  @Autowired
  CustomerPageFrameworkService pageFrameworkService;
  @Resource
  PageFrameworkMapper pageFrameworkMapper;

  @GetMapping("/customer/selectIndexPageFrameWork")
  @ResponseBody
  public ResponseResult<PageFrameworkResponse> selectIndexPageFrameWork(){
    logger.debug("selectIndexPageFrameWork");
    PageFrameworkResponse data =  pageFrameworkService.selectDefaultPageFramework();
    return ResultGenerator.genSuccessResponseResult(data);
  }

  @GetMapping("/common/selectPageFrameWork")
  @ResponseBody
  public ResponseResult<PageFramework> selectPageFrameWork(@RequestParam("id") Long id ){
    PageFramework pageFramework =  pageFrameworkService.findById(id);
    return ResultGenerator.genSuccessResponseResult(pageFramework);
  }

  @GetMapping("/common/selectIOSPageFramework")
  @ResponseBody
  public ResponseResult<PageFrameworkResponse> selectIOSPageFramework(){
    logger.debug("selectIndexPageFrameWork");
    PageFrameworkResponse data =  pageFrameworkService.selectIOSPageFramework();
    return ResultGenerator.genSuccessResponseResult(data);
  }

  @GetMapping("/common/selectPageIndex")
  @ResponseBody
  public ResponseResult<PageFramework> selectPageIndex(){
    PageFramework pageFramework = pageFrameworkMapper.selectPageIndex();
    return ResultGenerator.genSuccessResponseResult(pageFramework);
  }

}
