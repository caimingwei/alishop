package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.UserGrade;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.usergrade.UserGradeReq;
import com.alipay.rebate.shop.pojo.usergrade.UserGradeRsp;
import com.alipay.rebate.shop.service.admin.UserGradeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/08/16.
*/
@RestController
@RequestMapping("/admin/userGrade")
public class UserGradeController {
    private final Logger logger = LoggerFactory.getLogger(UserGradeController.class);
    @Resource
    private UserGradeService userGradeService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody UserGradeReq userGrade) {
        userGradeService.addUserGrade(userGrade);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id) {
        userGradeService.deleteUserGrade(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody UserGradeReq userGrade) {
        userGradeService.updateUserGrade(userGrade);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail/{id}")
    public ResponseResult detail(@PathVariable("id") Integer id) {
        UserGradeRsp userGrade = userGradeService.userGradeDetail(id);
        return ResultGenerator.genSuccessResponseResult(userGrade);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<UserGrade>> list( @RequestParam(defaultValue = "0") Integer page,
                                                     @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<UserGrade> list = userGradeService.getAllSortByWeight();
        PageInfo<UserGrade> pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
