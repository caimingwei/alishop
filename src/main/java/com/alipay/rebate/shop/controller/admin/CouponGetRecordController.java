package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.service.admin.CouponGetRecordService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/10/26.
*/
@RestController
@RequestMapping("/coupon/get/record")
public class CouponGetRecordController {
    @Resource
    private CouponGetRecordService couponGetRecordService;

    @PostMapping("/add")
    public Result add(CouponGetRecord couponGetRecord) {
        couponGetRecordService.save(couponGetRecord);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        couponGetRecordService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/update")
    public Result update(CouponGetRecord couponGetRecord) {
        couponGetRecordService.update(couponGetRecord);
        return ResultGenerator.genSuccessResult();
    }

    @PostMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        CouponGetRecord couponGetRecord = couponGetRecordService.findById(id);
        return ResultGenerator.genSuccessResult(couponGetRecord);
    }

    @PostMapping("/list")
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<CouponGetRecord> list = couponGetRecordService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
