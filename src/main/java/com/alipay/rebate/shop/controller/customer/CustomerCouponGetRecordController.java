package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.UserCouponRsp;
import com.alipay.rebate.shop.service.admin.CouponGetRecordService;
import com.alipay.rebate.shop.service.customer.CustomerCouponGetRecordService;
import com.alipay.rebate.shop.utils.JwtUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/10/26.
*/
@RestController
@RequestMapping("/customer/coupon/get/record")
public class CustomerCouponGetRecordController {

    @Autowired
    private ParameterChecker parameterChecker;
    @Autowired
    private CustomerCouponGetRecordService couponGetRecordService;

    @GetMapping("/list")
    public ResponseResult list(
        Integer pageNo,
        Integer pageSize,
        HttpServletRequest request
    ) {
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        PageHelper.startPage(pageNo, pageSize);
        Long userId = JwtUtils.getUserIdFromToken(request);
        List<UserCouponRsp> list = couponGetRecordService.selectUserCouponList(userId);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

    @RequestMapping("/openCoupon")
    public ResponseResult openCoupon(
        @RequestParam("id") Long id
    ) {
        couponGetRecordService.openCoupon(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }
}
