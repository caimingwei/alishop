package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.LogOffApply;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.customer.CustomerLogOffApplyService;
import com.alipay.rebate.shop.utils.JwtUtils;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2020/01/19.
*/
@RestController
@RequestMapping("/customer/log/off/apply")
public class CustomerLogOffApplyController {
    @Resource
    private CustomerLogOffApplyService logOffApplyService;

    @PostMapping("/add")
    public ResponseResult add(HttpServletRequest request) {
        long userId = JwtUtils.getUserIdFromToken(request);
        int result = logOffApplyService.add(userId);
        return ResultGenerator.genSuccessResponseResult(result);
    }

    @GetMapping("/detail")
    public ResponseResult detail(HttpServletRequest request) {
        long userId = JwtUtils.getUserIdFromToken(request);
        LogOffApply result = logOffApplyService.latestApply(userId);
        return ResultGenerator.genSuccessResponseResult(result);
    }
}
