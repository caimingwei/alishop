package com.alipay.rebate.shop.controller.customer;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.InviteUserSettings;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.InviteUserSettingsDto;
import com.alipay.rebate.shop.service.admin.InviteUserSettingsService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common/invite/user/settings")
public class CustomerInviteUserSettingsController {

  @Resource
  private InviteUserSettingsService inviteUserSettingsService;
  private ObjectMapper objectMapper = new ObjectMapper();
  @GetMapping("/detail")
  public ResponseResult detail() throws Exception{
    InviteUserSettings inviteUserSettings = inviteUserSettingsService.findById(1);
    InviteUserSettingsDto dto = new InviteUserSettingsDto();
    dto.setRuleHtml(inviteUserSettings.getRuleHtml());
    List<String> img = objectMapper.readValue(inviteUserSettings.getImgs(),
        new TypeReference<List<String>>(){});
    dto.setImgs(img);
    return ResultGenerator.genSuccessResponseResult(dto);
  }

}
