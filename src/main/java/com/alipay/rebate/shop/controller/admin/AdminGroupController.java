package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.AdminGroup;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.AdminGroupService;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/12/08.
*/
@RestController
@RequestMapping("/admin/group")
public class AdminGroupController {

    private Logger logger = LoggerFactory.getLogger(AdminGroupController.class);
    @Resource
    private AdminGroupService adminGroupService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody AdminGroup adminGroup) {
        Date date = new Date();
        adminGroup.setCreateTime(date);
        adminGroup.setUpdateTime(date);
        logger.debug("adminGroup is : {}",adminGroup);
        adminGroupService.save(adminGroup);
        return ResultGenerator.genSuccessResponseResult(adminGroup.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult delete(@RequestParam Integer id) {
        adminGroupService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult();
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody AdminGroup adminGroup) {
        adminGroup.setUpdateTime(new Date());
        adminGroupService.update(adminGroup);
        return ResultGenerator.genSuccessResponseResult(adminGroup.getId());
    }

    @GetMapping("/detail")
    public ResponseResult detail(@RequestParam Integer id) {
        AdminGroup adminGroup = adminGroupService.findById(id);
        return ResultGenerator.genSuccessResponseResult(adminGroup);
    }

    @GetMapping("/list")
    public ResponseResult list() {
        List<AdminGroup> list = adminGroupService.findAll();
        return ResultGenerator.genSuccessResponseResult(list);
    }
}
