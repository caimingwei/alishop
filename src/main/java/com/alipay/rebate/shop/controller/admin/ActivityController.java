package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.Activity;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.ActivityProductService;
import com.alipay.rebate.shop.service.admin.ActivityService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/08/25.
*/
@RestController
@RequestMapping("/admin/activity")
@ResponseBody
public class ActivityController {
    private final static Logger logger = LoggerFactory.getLogger(ActivityController.class);
    @Resource
    private ActivityService activityService;
    @Resource
    private ActivityProductService activityProductService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody Activity activity) {
        activity.setCreateTime(new Date());
        activity.setUpdateTime(new Date());
        activityService.save(activity);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id) {
        activityProductService.deleteByActivityId(id);
        activityService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @PutMapping("/update")
    public ResponseResult update(@RequestBody Activity activity) {
        activity.setUpdateTime(new Date());
        activityService.update(activity);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @GetMapping("/detail/{id}")
    public ResponseResult detail(@PathVariable("id") Integer id) {
        Activity activity = activityService.findById(id);
        return ResultGenerator.genSuccessResponseResult(activity);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<Activity>> list( @RequestParam(defaultValue = "0") Integer page,
                                                    @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Activity> list = activityService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
