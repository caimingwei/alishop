package com.alipay.rebate.shop.controller.admin;
import com.alipay.rebate.shop.core.Result;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.MainHallMapper;
import com.alipay.rebate.shop.model.MainHall;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.mainhall.MainHallReq;
import com.alipay.rebate.shop.pojo.mainhall.UpdateMainHallReq;
import com.alipay.rebate.shop.service.admin.MainHallService;
import com.alipay.rebate.shop.service.admin.impl.MainHallServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
* Created by CodeGenerator on 2020/04/24.
*/
@RestController
@RequestMapping("/admin/mainHall")
public class MainHallController {

    private final Logger logger = LoggerFactory.getLogger(MainHallServiceImpl.class);

    @Resource
    private MainHallMapper mainHallMapper;

    @Autowired
    MainHallService mainHallService;

    @PostMapping("/add")
    public ResponseResult<Integer> add(@RequestBody MainHallReq req) {
        logger.debug("req is : {}",req);
        int result = mainHallService.addMainHall(req);
        return ResultGenerator.genSuccessResponseResult(result);
    }


    @GetMapping("/detail")
    public ResponseResult<MainHall> detail(@RequestParam Long id) {
        logger.debug("id is : {}",id);
        MainHall mainHall = mainHallMapper.selectMainHallById(id);
        return ResultGenerator.genSuccessResponseResult(mainHall);
    }


    @GetMapping("/list")
    public ResponseResult<PageInfo<MainHall>>  list(@RequestParam(defaultValue = "0") Integer pageNum, @RequestParam(defaultValue = "0") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<MainHall> list = mainHallService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }

        @PutMapping("/updateMainHallById")
    public ResponseResult<Integer> updateMainHallById(@RequestBody @Valid UpdateMainHallReq req){
        logger.debug("req is : {}",req);
        int result = mainHallService.updateMainHallById(req);
        return ResultGenerator.genSuccessResponseResult(result);
    }

    @DeleteMapping("/deleteMainHallById")
    public ResponseResult<Integer> deleteMainHallById(@RequestParam Long id){
        int result = mainHallMapper.deleteMainHallById(id);
        return ResultGenerator.genSuccessResponseResult(result);
    }

}
