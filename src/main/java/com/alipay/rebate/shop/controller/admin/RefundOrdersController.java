package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.RefundOrders;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.RefundOrdersService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/09/19.
*/
@RestController
@RequestMapping("/admin/refundOrders")
public class RefundOrdersController {
    private final Logger logger = LoggerFactory.getLogger(RefundOrdersController.class);
    @Resource
    private RefundOrdersService refundOrdersService;


    @GetMapping("/detail/{tbTradeId}")
    public ResponseResult detail(@PathVariable("tbTradeId") String tbTradeId) {
        RefundOrders refundOrders = refundOrdersService.selectById(tbTradeId);
        return ResultGenerator.genSuccessResponseResult(refundOrders);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<RefundOrders>> list( @RequestParam(defaultValue = "1") Integer page,
                                                   @RequestParam(defaultValue = "20") Integer size,Long userId,String tradeId) {
        PageHelper.startPage(page, size);
        List<RefundOrders> list = refundOrdersService.selectAllRefundOrders(userId,tradeId);
        PageInfo<RefundOrders> pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
