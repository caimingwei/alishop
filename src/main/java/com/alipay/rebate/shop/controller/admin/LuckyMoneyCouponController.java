package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.checker.ParameterChecker;
import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.model.LuckyMoneyCoupon;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.LuckyMoneyCouponService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by CodeGenerator on 2019/10/26.
*/
@RestController
@RequestMapping("/admin/lucky/money/coupon")
public class LuckyMoneyCouponController {
    @Resource
    private LuckyMoneyCouponService luckyMoneyCouponService;

    @Autowired
    private ParameterChecker parameterChecker;

    @PostMapping("/add")
    public ResponseResult<Integer> add(@RequestBody LuckyMoneyCoupon luckyMoneyCoupon) {
        luckyMoneyCouponService.addNewLuckyMoneyCoupon(luckyMoneyCoupon);
        return ResultGenerator.genSuccessResponseResult(luckyMoneyCoupon.getId());
    }

    @DeleteMapping("/delete")
    public ResponseResult<Void> delete(@RequestParam Integer id) {
        luckyMoneyCouponService.deleteById(id);
        return ResultGenerator.genSuccessResponseResult(null);
    }

    @PutMapping("/update")
    public ResponseResult<Integer> update(@RequestBody LuckyMoneyCoupon luckyMoneyCoupon) {
        Integer data = luckyMoneyCouponService.updateNewLuckyMoneyCoupon(luckyMoneyCoupon);
        return ResultGenerator.genSuccessResponseResult(data);
    }

    @GetMapping("/detail")
    public ResponseResult<LuckyMoneyCoupon> detail(@RequestParam Integer id) {
        LuckyMoneyCoupon luckyMoneyCoupon = luckyMoneyCouponService.findById(id);
        return ResultGenerator.genSuccessResponseResult(luckyMoneyCoupon);
    }

    @GetMapping("/list")
    public ResponseResult list(Integer pageNo,Integer pageSize) {
        pageNo = parameterChecker.checkAndGetPageNo(pageNo);
        pageSize = parameterChecker.checkAndGetPageSize(pageSize);
        PageHelper.startPage(pageNo, pageSize);
        List<LuckyMoneyCoupon> list = luckyMoneyCouponService.selectAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
