package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.core.ResultGenerator;
import com.alipay.rebate.shop.dao.mapper.WithDrawMapper;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.admin.AdminWithDrawRsp;
import com.alipay.rebate.shop.pojo.user.admin.UserIncomeRsp;
import com.alipay.rebate.shop.service.admin.AdminUserIncomeService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import gherkin.lexer.He;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
@RequestMapping("/admin/userIncome")
public class AdminUserIncomController {
    private final Logger logger = LoggerFactory.getLogger(AdminUserIncomController.class);
    @Autowired
    private AdminUserIncomeService adminUserIncomeService;



    @GetMapping("/detail/{id}")
    public ResponseResult detail(@PathVariable("id") Long id) {
        UserIncome userIncome = adminUserIncomeService.findById(id);
        return ResultGenerator.genSuccessResponseResult(userIncome);
    }

    @GetMapping("/list")
    public ResponseResult<PageInfo<UserIncomeRsp>> list(UserIncomeRsp userIncome, @RequestParam(defaultValue = "1") Integer page,
                                                     @RequestParam(defaultValue = "20") Integer size) {

        PageHelper.startPage(page,size);
        List<UserIncomeRsp> list = adminUserIncomeService.selectAllByCondition(userIncome);
        PageInfo<UserIncomeRsp> pageInfo = new PageInfo(list);

        return ResultGenerator.genSuccessResponseResult(pageInfo);
    }
}
