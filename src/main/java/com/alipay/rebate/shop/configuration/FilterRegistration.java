package com.alipay.rebate.shop.configuration;

import com.alipay.rebate.shop.filter.LogFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterRegistration {

    private static final String AUTH_URL_PATTERN = "/*";
    private static final String ADMIN_LOG_URL_PATTERN = "/admin/*";
    private static final int AUTH_FILTER_ORDER = Integer.MAX_VALUE - 2;
    private static final int LOG_FILTER_ORDER = Integer.MAX_VALUE - 1;
    private static final String AUTH_FILTER_NAME = "authFilter";
    private static final String LOG_FILTER_NAME = "logFilter";

    @Bean
    public FilterRegistrationBean logFilter(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new LogFilter());
        filterRegistrationBean.addUrlPatterns(ADMIN_LOG_URL_PATTERN);
        filterRegistrationBean.setOrder(LOG_FILTER_ORDER);
        filterRegistrationBean.setName(LOG_FILTER_NAME);
        return filterRegistrationBean;
    }
}
