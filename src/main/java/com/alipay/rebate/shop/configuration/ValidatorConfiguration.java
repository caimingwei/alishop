package com.alipay.rebate.shop.configuration;

import com.alipay.rebate.shop.pojo.user.customer.UpdatePasswordRequest;
import org.hibernate.validator.HibernateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

@Configuration
public class ValidatorConfiguration {

    private Logger logger = LoggerFactory.getLogger(ValidatorConfiguration.class);

    @Bean
    public Validator validator(){
        logger.debug("-----------------------------------");
        logger.debug("initializing ValidatorConfiguration");
        logger.debug("-----------------------------------");
        ValidatorFactory validatorFactory = Validation.byProvider( HibernateValidator.class )
                .configure()
                .failFast(true)
                .buildValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        return validator;
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        logger.debug("-----------------------------------");
        logger.debug("initializing MethodValidationPostProcessor");
        logger.debug("-----------------------------------");
        MethodValidationPostProcessor postProcessor = new MethodValidationPostProcessor();
        /**设置validator模式为快速失败返回*/
        postProcessor.setValidator(validator());
        return postProcessor;
    }

}
