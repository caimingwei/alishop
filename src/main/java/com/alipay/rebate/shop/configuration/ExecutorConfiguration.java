package com.alipay.rebate.shop.configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExecutorConfiguration {

  @Bean("sharePool")
  public ExecutorService sharePool(){
    return Executors.newFixedThreadPool(20);
  }

}
