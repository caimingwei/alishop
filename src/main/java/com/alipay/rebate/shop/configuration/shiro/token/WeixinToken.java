package com.alipay.rebate.shop.configuration.shiro.token;

import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

import java.io.Serializable;

public class WeixinToken implements HostAuthenticationToken, RememberMeAuthenticationToken, Serializable {

    private String userName;
    private boolean rememberMe;
    private String host;
    private String openId;

    public Object getPrincipal() {
        return userName;
    }

    public Object getCredentials() {
        return openId;
    }

    public WeixinToken() { this.rememberMe = false; }

    public WeixinToken(String userName) { this(userName, false, null); }

    public WeixinToken(String userName, boolean rememberMe) { this(userName, rememberMe, null); }

    public WeixinToken(String userName, boolean rememberMe, String host) {
        this.userName = userName;
        this.rememberMe = rememberMe;
        this.host = host;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public boolean isRememberMe() {
        return rememberMe;
    }

}
