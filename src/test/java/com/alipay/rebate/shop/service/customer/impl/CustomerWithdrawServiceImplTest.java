package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.EntitySettingsMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.dao.mapper.WithDrawMapper;
import com.alipay.rebate.shop.exceptoin.AmountNotEnougthException;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.exceptoin.UitNotEnougthException;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.pojo.user.customer.UserWithdrawResponse;
import com.alipay.rebate.shop.service.customer.CustomerWithdrawService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerWithdrawServiceImplTest {

    @Resource
    UserMapper userMapper;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    WithDrawMapper withDrawMapper;
    @Resource
    EntitySettingsMapper entitySettingsMapper;
    @Resource
    CustomerWithdrawService customerWithdrawService;
    Logger logger = LoggerFactory.getLogger(CustomerWithdrawServiceImplTest.class);

    @Before
    public void before(){
        userMapper.deleteUserById(10L);
        userMapper.deleteUserById(11L);
        userIncomeMapper.deleteUserIncomeByUserId(10L);
        withDrawMapper.deleteWithDrawByUserId(10L);

        User user = buildUser(10L, "mm1", "15674228888",
                "24465768918", new BigDecimal("20.00"), 5000L);
        userMapper.insertSelective(user);
    }

    @After
    public void after(){
        userMapper.deleteUserById(10L);
        userMapper.deleteUserById(11L);
        userIncomeMapper.deleteUserIncomeByUserId(10L);
        withDrawMapper.deleteWithDrawByUserId(10L);
    }

    private User buildUser(Long userId,String userName,String phone,String alipayAccount,BigDecimal userAmount,
                           Long userIntgeralTreasure){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setRealName("mm1");
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setAlipayAccount(alipayAccount);
        user.setUserAmount(userAmount);
        user.setUserIntgeralTreasure(userIntgeralTreasure);
        return user;
    }

    private WithDraw buildWithdraw(Long id,Integer status,BigDecimal money,Long userId,String account,
                                   String phone,Integer type,String userName,Date date){
        WithDraw withDraw = new WithDraw();
        withDraw.setWithdrawId(id);
        withDraw.setStatus(status);
        withDraw.setMoney(money);
        withDraw.setUserId(userId);
        withDraw.setAccount(account);
        withDraw.setApplyTime(date);
        withDraw.setCommitTime(date);
        withDraw.setHandleTime(null);
        withDraw.setIsFirst(0);
        withDraw.setPhone(phone);
        withDraw.setRealName("mm1");
        withDraw.setType(type);
        withDraw.setRefuseReason(null);
        withDraw.setPlatform(null);
        withDraw.setUserName(userName);
        return withDraw;
    }


    /**
     * 金额提现
     */
    @Test
    public void processWithdrawAmount_1(){
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 10L, null,
                null, null, null, null);
        UserWithdrawResponse userWithdrawResponse = customerWithdrawService.withdrawAmount(withDraw);
        // 校验收益记录
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserId(10L);
        Assert.assertEquals(Long.valueOf(10l),userIncome.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(userIncome.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(13),userIncome.getType());
        List<WithDraw> withDraws = withDrawMapper.selectWithDrawByUserId(10L);

        // 校验提现记录
        for (WithDraw withDraw1 : withDraws){
            Assert.assertEquals(Long.valueOf(10),withDraw1.getUserId());
            Assert.assertTrue(new BigDecimal("10.00").compareTo(withDraw1.getMoney()) == 0);
            Assert.assertEquals("15674228888",withDraw1.getPhone());
            Assert.assertEquals("mm1",withDraw1.getRealName());
            Assert.assertEquals(Integer.valueOf(0),withDraw1.getType());
            Assert.assertEquals(Integer.valueOf(1),withDraw1.getStatus());
            Assert.assertEquals(Integer.valueOf(1),withDraw1.getIsFirst());
        }
    }

    /**
     * 集分宝提现
     */
    @Test
    public void processWithdrawUit_1(){
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 10L, null,
                null, null, null, null);
        UserWithdrawResponse userWithdrawResponse = customerWithdrawService.withdrawUit(withDraw);
        // 校验收益记录
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserId(10L);
        Assert.assertEquals(Long.valueOf(10l),userIncome.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(userIncome.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(13),userIncome.getType());
        List<WithDraw> withDraws = withDrawMapper.selectWithDrawByUserId(10L);

        // 校验提现记录
        for (WithDraw withDraw1 : withDraws){
            Assert.assertEquals(Long.valueOf(10),withDraw1.getUserId());
            Assert.assertTrue(new BigDecimal("10.00").compareTo(withDraw1.getMoney()) == 0);
            Assert.assertEquals("15674228888",withDraw1.getPhone());
            Assert.assertEquals("mm1",withDraw1.getRealName());
            Assert.assertEquals(Integer.valueOf(1),withDraw1.getType());
            Assert.assertEquals(Integer.valueOf(1),withDraw1.getStatus());
            Assert.assertEquals(Integer.valueOf(1),withDraw1.getIsFirst());
        }
    }

    /**
     * 金额提现 -- 有一笔处理中的提现记录
     */
    @Test
    public void processWithdrawAmount_2(){
        WithDraw withDraw1 = buildWithdraw(10L, 1, new BigDecimal("10.00"), 10L, "mm1",
                "15674228888", 0, null, null);
        withDrawMapper.insertWithDraw(withDraw1);
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 10L, null,
                null, null, null, null);
        try {
            customerWithdrawService.withdrawAmount(withDraw);
        }catch (BusinessException exception){
            Assert.assertEquals("您有1笔提现正在处理",exception.getMessage());
        }

    }

    /**
     * 集分宝提现 -- 有一笔处理中的提现记录
     */
    @Test
    public void processWithdrawUit_2(){
        WithDraw withDraw1 = buildWithdraw(10L, 1, new BigDecimal("10.00"), 10L, "mm1",
                "15674228888", 0, null, null);
        withDrawMapper.insertWithDraw(withDraw1);
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 10L, null,
                null, null, null, null);
        try {
            customerWithdrawService.withdrawUit(withDraw);
        }catch (BusinessException exception){
            Assert.assertEquals("您有1笔提现正在处理",exception.getMessage());
        }
    }

    /**
     * 金额提现
     * 第二次提现
     */
    @Test
    public void processWithdrawAmount_3(){
        // 准备一条提现完成的提现记录
        WithDraw withDraw1 = buildWithdraw(10L, 2, new BigDecimal("10.00"), 10L, "mm1",
                "15674228888", 0, null, null);
        withDrawMapper.insertWithDraw(withDraw1);

        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 10L, null,
                null, null, null, null);
        customerWithdrawService.withdrawAmount(withDraw);
        // 校验收益记录
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserId(10L);
        Assert.assertEquals(Long.valueOf(10l),userIncome.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(userIncome.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(13),userIncome.getType());

        // 获取提现中的记录
        WithDraw withDraw2 = withDrawMapper.selectWithdrawByStatusAndUserId(1, 10L);
        Assert.assertEquals(Long.valueOf(10),withDraw2.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(withDraw2.getMoney()) == 0);
        Assert.assertEquals("15674228888",withDraw2.getPhone());
        Assert.assertEquals("mm1",withDraw2.getRealName());
        Assert.assertEquals(Integer.valueOf(0),withDraw2.getType());
        Assert.assertEquals(Integer.valueOf(1),withDraw2.getStatus());
        Assert.assertEquals(Integer.valueOf(0),withDraw2.getIsFirst());
    }

    /**
     * 金额提现
     */
    @Test
    public void processWithdrawAmount_4(){

        // 插入一条驳回的提现记录
        WithDraw withDraw1 = buildWithdraw(10L, 3, new BigDecimal("10.00"), 10L, "mm1",
                "15674228888", 0, null, null);
        withDrawMapper.insertWithDraw(withDraw1);

        // 提现为驳回，后面的提现记录还是判断为第一次提现
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 10L, null,
                null, null, null, null);
        customerWithdrawService.withdrawAmount(withDraw);
        // 校验收益记录
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserId(10L);
        Assert.assertEquals(Long.valueOf(10l),userIncome.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(userIncome.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(13),userIncome.getType());

        // 获取提现中的记录
        WithDraw withDraw2 = withDrawMapper.selectWithdrawByStatusAndUserId(1, 10L);
        Assert.assertEquals(Long.valueOf(10),withDraw2.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(withDraw2.getMoney()) == 0);
        Assert.assertEquals("15674228888",withDraw2.getPhone());
        Assert.assertEquals("mm1",withDraw2.getRealName());
        Assert.assertEquals(Integer.valueOf(0),withDraw2.getType());
        Assert.assertEquals(Integer.valueOf(1),withDraw2.getStatus());
        Assert.assertEquals(Integer.valueOf(1),withDraw2.getIsFirst());
    }

    /**
     *余额不足
     */
    @Test
    public void processWithdrawAmount_5(){
        User user = buildUser(11L, "mm1", "15674228888",
                "24465768918", new BigDecimal("2.00"), 100L);
        userMapper.insertSelective(user);
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 11L, null,
                null, null, null, null);
        try {
            customerWithdrawService.withdrawAmount(withDraw);
        }catch (AmountNotEnougthException exception){
            Assert.assertEquals("余额不足",exception.getMessage());
        }

    }

    /**
     *集分宝不足
     */
    @Test
    public void processWithdrawUit_3(){
        User user = buildUser(11L, "mm1", "15674228888",
                "24465768918", new BigDecimal("2.00"), 100L);
        userMapper.insertSelective(user);
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 11L, null,
                null, null, null, null);
        try {
            customerWithdrawService.withdrawUit(withDraw);
        }catch (UitNotEnougthException exception){
            Assert.assertEquals("集分宝不足",exception.getMessage());
        }
    }

    /**
     * 支付宝账号未绑定
     */
    @Test
    public void processWithdrawAmount_6(){

        User user = buildUser(11L, "mm1", "15674228888",
                null, new BigDecimal("2.00"), 100L);
        userMapper.insertSelective(user);
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("10.00"), 11L, null,
                null, null, null, null);
        try {
            customerWithdrawService.withdrawUit(withDraw);
        }catch (BusinessException exception){
            Assert.assertEquals("支付宝账号未绑定",exception.getMessage());
        }
    }

    /**
     *
     */
    @Test
    public void processWithdrawUit_4(){
        User user = buildUser(11L, "mm1", "15674228888",
                "24465768918", new BigDecimal("2.00"), 10000L);
        userMapper.insertSelective(user);
        WithDraw withDraw = buildWithdraw(null, null, new BigDecimal("5.00"), 11L, null,
                null, null, null, null);
        try {
            customerWithdrawService.withdrawUit(withDraw);
        }catch (BusinessException exception){
            Assert.assertEquals("提现的集分宝数目不能小于1000",exception.getMessage());
        }
    }

}
