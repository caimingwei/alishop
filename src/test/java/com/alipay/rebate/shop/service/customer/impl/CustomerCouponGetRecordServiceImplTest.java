package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.*;
import com.alipay.rebate.shop.service.customer.CustomerCouponGetRecordService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerCouponGetRecordServiceImplTest {

    private Logger logger = LoggerFactory.getLogger(CustomerCouponGetRecordServiceImplTest.class);
    @Resource
    private CouponGetRecordMapper couponGetRecordMapper;
    @Resource
    private LuckyMoneyCouponMapper luckyMoneyCouponMapper;
    @Resource
    private UserIncomeMapper userIncomeMapper;
    @Resource
    private WithDrawMapper withDrawMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    CustomerCouponGetRecordService recordService;
    @Resource
    OrdersMapper ordersMapper;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        luckyMoneyCouponMapper.deleteById(1);
        couponGetRecordMapper.deleteByPrimaryKey(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        ordersMapper.deleteByPrimaryKey("510202563513111111");

        User user = buildUser(1L, "mm1", null, null, "13467286354");
        userMapper.insertSelective(user);

    }

    @After
    public void after(){
        userMapper.deleteUserById(1L);
        luckyMoneyCouponMapper.deleteById(1);
        couponGetRecordMapper.deleteByPrimaryKey(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        ordersMapper.deleteByPrimaryKey("510202563513111111");
    }

    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private CouponGetRecord buildCouponGetRecord(Long userId,Integer couponId,Integer isUse,Integer type){
        CouponGetRecord couponGetRecord = new CouponGetRecord();
        couponGetRecord.setId(1L);
        couponGetRecord.setUserId(userId);
        couponGetRecord.setCouponId(couponId);
        couponGetRecord.setType(type);
        couponGetRecord.setIsUse(isUse);
        couponGetRecord.setCreateTime(getDate(-2));
        couponGetRecord.setUpdateTime(getDate(-1));
        return couponGetRecord;
    }


    private Date getDate(int type){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, type); //得到前一du天
        Date date = calendar.getTime();
        return date;
    }

    private LuckyMoneyCoupon buildLuckyMoneyCoupon(Date createTime,Date updateTime,Date endTime,Date startTime,int type){
        LuckyMoneyCoupon luckyMoneyCoupon = new LuckyMoneyCoupon();
        luckyMoneyCoupon.setId(1);
        luckyMoneyCoupon.setCanEdit(null);
        luckyMoneyCoupon.setCreateTime(createTime);
        luckyMoneyCoupon.setUpdateTime(updateTime);
        luckyMoneyCoupon.setAmountOfMoney(new BigDecimal("5.00"));
        luckyMoneyCoupon.setConditionMoney(new BigDecimal("12.00"));
        luckyMoneyCoupon.setConditionType(type);
        luckyMoneyCoupon.setEndTime(endTime);
        luckyMoneyCoupon.setEqualintegral(500);
        luckyMoneyCoupon.setIsCurrentUse(1);
        luckyMoneyCoupon.setLimitNum(1);
        luckyMoneyCoupon.setName("红包");
        luckyMoneyCoupon.setStartTime(startTime);
        luckyMoneyCoupon.setType(null);
        luckyMoneyCoupon.setStock(1000);
        return luckyMoneyCoupon;
    }

    private UserIncome buildUserInCome(Long id,Integer type,BigDecimal money,Long userId){
        UserIncome userIncome = new UserIncome();
        userIncome.setUserIncomeId(id);
        userIncome.setType(type);
        userIncome.setMoney(money);
        userIncome.setStatus(1);
        userIncome.setAwardType(2);
        userIncome.setCreateTime("2020-05-30 18:34:45");
        userIncome.setUpdateTime("2020-06-03 18:34:45");
        userIncome.setUserId(userId);
        userIncome.setSpecialId(null);
        userIncome.setRelationId(null);
        userIncome.setTradeId(null);
        userIncome.setTkStatus(null);
        userIncome.setIntgeralNum(null);
        userIncome.setRoadType(null);
        userIncome.setParentTradeId(null);
        userIncome.setRemarks(null);
        return userIncome;
    }

    private Orders buildInitialOrders(){
        Orders orders = new Orders();
        orders.setTradeId("510202563513111111");
        orders.setAdzoneId(109205150467L);
        orders.setAdzoneName(" 渠道ID专属");
        orders.setClickTime("2020-4-27 00:50:0");
        orders.setTkPaidTime("2020-4-27 00:50:10");
        orders.setTbPaidTime("2020-4-27 00:50:10");
        orders.setCreateTime("2020-4-27 00:50:10");
        orders.setTkStatus(3L);
        orders.setRelationId(1L);
        orders.setPubSharePreFee(new BigDecimal("100"));
        orders.setTotalCommissionRate(new BigDecimal("10"));
        orders.setTotalCommissionFee(new BigDecimal("100"));
        orders.setRefundTag(0L);
        orders.setPayPrice(new BigDecimal("1000"));
        orders.setAlipayTotalPrice(new BigDecimal("1000"));
        orders.setItemNum(1L);
        orders.setItemImg("111");
        orders.setItemTitle("111");
        orders.setItemLink("111");
        orders.setOrderType("天猫");
        orders.setSiteId(620000087L);
        return orders;
    }

    private WithDraw buildWithDraw(Long id,BigDecimal money,Long userId){
        WithDraw withDraw = new WithDraw();
        withDraw.setWithdrawId(id);
        withDraw.setUserName("mm1");
        withDraw.setPlatform(null);
        withDraw.setRefuseReason(null);
        withDraw.setType(2);
        withDraw.setRealName("mm1");
        withDraw.setPhone("13467286354");
        withDraw.setIsFirst(0);
        withDraw.setHandleTime(getDate(0));
        withDraw.setCommitTime(getDate(-3));
        withDraw.setApplyTime(getDate(-2));
        withDraw.setStatus(2);
        withDraw.setMoney(money);
        withDraw.setUserId(userId);
        withDraw.setAccount(null);
        return withDraw;
    }

    /**
     * 使用红包卷
     * 订单付款金额 > 红包卷条件金额
     */
    @Test
    public void processOpenCoupon_1(){

        // 准备订单付款红包卷
        CouponGetRecord couponGetRecord = buildCouponGetRecord(1L, 1, 0, 3);
        couponGetRecordMapper.insertSelective(couponGetRecord);
        LuckyMoneyCoupon luckyMoneyCoupon = buildLuckyMoneyCoupon(getDate(-3), getDate(-1), getDate(2), getDate(-3),3);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);

        // 准备一笔结算订单
        Orders orders = buildInitialOrders();
        orders.setUserId(1L);
        orders.setPayPrice(new BigDecimal("15.00"));
        orders.setAlipayTotalPrice(new BigDecimal("15.00"));
        ordersMapper.insert(orders);

        recordService.openCoupon(1L);
        // 校验收益记录
        UserIncome userIncome1 = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 11);
        Assert.assertEquals(Long.valueOf(1),userIncome1.getUserId());
        Assert.assertEquals(Integer.valueOf(11),userIncome1.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncome1.getStatus());
        Assert.assertTrue(new BigDecimal("5.00").compareTo(userIncome1.getMoney()) == 0);

        User user = userMapper.selectById(1L);
        Assert.assertTrue(new BigDecimal("5.00").compareTo(user.getUserAmount()) == 0);

    }

    /**
     * 使用红包卷
     * 订单付款金额 < 红包卷条件金额
     * 不能使用红宝卷
     */
    @Test
    public void processOpenCoupon_2(){

        // 准备订单付款红包卷
        CouponGetRecord couponGetRecord = buildCouponGetRecord(1L, 1, 0, 3);
        couponGetRecordMapper.insertSelective(couponGetRecord);
        LuckyMoneyCoupon luckyMoneyCoupon = buildLuckyMoneyCoupon(getDate(-3), getDate(-1), getDate(2), getDate(-3),3);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);

        // 准备一笔结算订单
        Orders orders = buildInitialOrders();
        orders.setUserId(1L);
        orders.setPayPrice(new BigDecimal("10.00"));
        orders.setAlipayTotalPrice(new BigDecimal("10.00"));
        ordersMapper.insert(orders);

        try {
            recordService.openCoupon(1L);
        }catch (BusinessException exception){
            Assert.assertEquals("付款金额不符合要求",exception.getMessage());
        }
    }

    /**
     * 红包卷已经使用
     */
    @Test
    public void processOpenCoupon_3(){
        // 使用红包卷
        processOpenCoupon_1();

        try {
            // 再次使用
            recordService.openCoupon(1L);
        }catch (BusinessException exception){
            Assert.assertEquals("红包已使用",exception.getMessage());
        }
    }

    /**
     * 红包卷已过期
     */
    @Test
    public void processOpenCoupon_4(){
        // 准备过期红包卷
        CouponGetRecord couponGetRecord = buildCouponGetRecord(1L, 1, 0, 3);
        couponGetRecordMapper.insertSelective(couponGetRecord);
        LuckyMoneyCoupon luckyMoneyCoupon = buildLuckyMoneyCoupon(getDate(-3), getDate(-1), getDate(-1), getDate(-3),3);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);

        try {
            recordService.openCoupon(1L);
        }catch (BusinessException exception){
            Assert.assertEquals("红包卷过期",exception.getMessage());
        }

    }

    /**
     * 红包卷
     * 订单返利 > 红包卷条件金额
     */
    @Test
    public void processOpenCoupon_5(){
        UserIncome userIncome = buildUserInCome(1L, 1, new BigDecimal("20.00"), 1L);
        userIncomeMapper.insertSelective(userIncome);

        // 准备返利满 -- 红包卷
        CouponGetRecord couponGetRecord = buildCouponGetRecord(1L, 1, 0, 1);
        couponGetRecordMapper.insertSelective(couponGetRecord);
        LuckyMoneyCoupon luckyMoneyCoupon = buildLuckyMoneyCoupon(getDate(-3), getDate(-1),
                getDate(2), getDate(-3),1);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);

        recordService.openCoupon(1L);
        // 校验收益记录
        UserIncome userIncome1 = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 11);
        Assert.assertEquals(Long.valueOf(1),userIncome1.getUserId());
        Assert.assertEquals(Integer.valueOf(11),userIncome1.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncome1.getStatus());
        Assert.assertTrue(new BigDecimal("5.00").compareTo(userIncome1.getMoney()) == 0);

        User user = userMapper.selectById(1L);
        Assert.assertTrue(new BigDecimal("5.00").compareTo(user.getUserAmount()) == 0);
    }

    /**
     * 红包卷
     * 订单返利 < 红包卷条件金额
     */
    @Test
    public void processOpenCoupon_6(){
        UserIncome userIncome = buildUserInCome(1L, 1, new BigDecimal("10.00"), 1L);
        userIncomeMapper.insertSelective(userIncome);

        // 准备返利满 -- 红包卷
        CouponGetRecord couponGetRecord = buildCouponGetRecord(1L, 1, 0, 1);
        couponGetRecordMapper.insertSelective(couponGetRecord);
        LuckyMoneyCoupon luckyMoneyCoupon = buildLuckyMoneyCoupon(getDate(-3), getDate(-1),
                getDate(2), getDate(-3),1);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);

        try {
            recordService.openCoupon(1L);
        }catch (BusinessException exception){
            Assert.assertEquals("订单返利金额不符合要求",exception.getMessage());
        }
    }
    /**
     * 红包卷
     * 提现满 > 红包卷条件金额
     */
    @Test
    public void processOpenCoupon_7(){
        // 准备提现记录
        WithDraw withDraw = buildWithDraw(1L, new BigDecimal("20"), 1L);
        withDrawMapper.insertWithDraw(withDraw);
        // 准备返利满 -- 红包卷
        CouponGetRecord couponGetRecord = buildCouponGetRecord(1L, 1, 0, 2);
        couponGetRecordMapper.insertSelective(couponGetRecord);
        LuckyMoneyCoupon luckyMoneyCoupon = buildLuckyMoneyCoupon(getDate(-3), getDate(-1),
                getDate(2), getDate(-3),2);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);

        recordService.openCoupon(1L);
        // 校验收益记录
        UserIncome userIncome1 = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 11);
        Assert.assertEquals(Long.valueOf(1),userIncome1.getUserId());
        Assert.assertEquals(Integer.valueOf(11),userIncome1.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncome1.getStatus());
        Assert.assertTrue(new BigDecimal("5.00").compareTo(userIncome1.getMoney()) == 0);

        User user = userMapper.selectById(1L);
        Assert.assertTrue(new BigDecimal("5.00").compareTo(user.getUserAmount()) == 0);
    }

    /**
     * 红包卷
     * 提现满 < 红包卷条件金额
     */
    @Test
    public void processOpenCoupon_8(){
        // 准备提现记录
        WithDraw withDraw = buildWithDraw(1L, new BigDecimal("10"), 1L);
        withDrawMapper.insertWithDraw(withDraw);
        // 准备返利满 -- 红包卷
        CouponGetRecord couponGetRecord = buildCouponGetRecord(1L, 1, 0, 2);
        couponGetRecordMapper.insertSelective(couponGetRecord);
        LuckyMoneyCoupon luckyMoneyCoupon = buildLuckyMoneyCoupon(getDate(-3), getDate(-1),
                getDate(2), getDate(-3),2);
        luckyMoneyCouponMapper.insertSelective(luckyMoneyCoupon);

        try {
            recordService.openCoupon(1L);
        }catch (BusinessException exception){
            Assert.assertEquals("提现金额不符合要求",exception.getMessage());
        }

    }

}
