package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.dao.mapper.ActivityProductMapper;
import com.alipay.rebate.shop.dao.mapper.CommissionLevelSettingsMapper;
import com.alipay.rebate.shop.dao.mapper.UserGradeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.CommissionLevelSettings;
import com.alipay.rebate.shop.model.UserGrade;
import com.alipay.rebate.shop.pojo.usergrade.UserGradeReq;
import com.alipay.rebate.shop.pojo.usergrade.UserGradeRsp;
import com.alipay.rebate.shop.service.admin.UserGradeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class UserGradeServiceImplTest {

    private Logger logger = LoggerFactory.getLogger(UserGradeServiceImplTest.class);

    @Resource
    UserGradeService userGradeService;
    @Resource
    private UserGradeMapper userGradeMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private ActivityProductMapper activityProductMapper;
    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Resource
    private CommissionLevelSettingsMapper commissionLevelSettingsMapper;

    @Before
    public void before(){
        userGradeMapper.deleteByGradeName("mmmm");
    }

    @After
    public void After(){
        userGradeMapper.deleteByGradeName("mmmm");
    }

    private UserGradeReq buildUserGradeReq(List<CommissionLevelSettings> levelSettings,String gradeName, Double gradeWeight, Double firstCommission,
                                           Double secondCommission, Double   thirdCommission,Integer enableCmlevelSettings){
        UserGradeReq userGradeReq = new UserGradeReq();
        userGradeReq.setLevelSettings(levelSettings);
        userGradeReq.setEnableCmlevelSettings(enableCmlevelSettings);
        userGradeReq.setFirstCommission(firstCommission);
        userGradeReq.setGradeName(gradeName);
        userGradeReq.setGradeWeight(gradeWeight);
        userGradeReq.setSecondCommission(secondCommission);
        userGradeReq.setThirdCommission(thirdCommission);
        return userGradeReq;
    }

    private UserGrade getUserGrade(List<UserGrade> userGrades ,String gradeName){
        for (UserGrade userGrade : userGrades){
            if (userGrade.getGradeName().equals(gradeName)){
                return userGrade;
            }
        }
        return null;
    }

    private CommissionLevelSettings buildCommissionLevelSettings(){
        CommissionLevelSettings commissionLevelSettings = new CommissionLevelSettings();
        commissionLevelSettings.setBegin(new BigDecimal("0.00"));
        commissionLevelSettings.setEnd(new BigDecimal("20.00"));
        commissionLevelSettings.setRate(new BigDecimal("50.00"));
        return commissionLevelSettings;
    }

    /**
     * 添加用户等级
     */
    @Test
    public void processAddUserGrade(){

        List<CommissionLevelSettings> commissionLevelSettings = new ArrayList<>();
        commissionLevelSettings.add(buildCommissionLevelSettings());

        UserGradeReq userGradeReq = buildUserGradeReq(commissionLevelSettings, "mmmm",
                3d, 85d, 12d, 3d, 0);
        userGradeService.addUserGrade(userGradeReq);
        List<UserGrade> userGrades = userGradeMapper.selectAll();
        UserGrade userGrade = getUserGrade(userGrades, "mmmm");
        Assert.assertEquals(Double.valueOf(3),userGrade.getGradeWeight());
        Assert.assertEquals(Double.valueOf(85),userGrade.getFirstCommission());
        Assert.assertEquals(Double.valueOf(12),userGrade.getSecondCommission());
        Assert.assertEquals(Double.valueOf(3),userGrade.getThirdCommission());
        Assert.assertEquals(Integer.valueOf(0),userGrade.getEnableCmlevelSettings());
        Assert.assertEquals("mmmm",userGrade.getGradeName());
    }

    /**
     * 更新
     */
    @Test
    public void processUpdateUserGrade(){
        processAddUserGrade();

        List<UserGrade> userGrades = userGradeMapper.selectAll();
        UserGrade userGrade = getUserGrade(userGrades, "mmmm");

        List<CommissionLevelSettings> commissionLevelSettings = new ArrayList<>();
        commissionLevelSettings.add(buildCommissionLevelSettings());
        UserGradeReq userGradeReq = buildUserGradeReq(commissionLevelSettings, "mmmm",
                8d, 80d, 10d, 2d, 1);
        userGradeReq.setId(userGrade.getId());
        userGradeService.updateUserGrade(userGradeReq);
        List<UserGrade> userGrades1 = userGradeMapper.selectAll();
        UserGrade userGrade1 = getUserGrade(userGrades1, "mmmm");
        Assert.assertEquals(Double.valueOf(8),userGrade1.getGradeWeight());
        Assert.assertEquals(Double.valueOf(80),userGrade1.getFirstCommission());
        Assert.assertEquals(Double.valueOf(10),userGrade1.getSecondCommission());
        Assert.assertEquals(Double.valueOf(2),userGrade1.getThirdCommission());
        Assert.assertEquals(Integer.valueOf(1),userGrade1.getEnableCmlevelSettings());
        Assert.assertEquals("mmmm",userGrade1.getGradeName());

    }

    /**
     * 用户等级详情
     */
    @Test
    public void processUserGradeDetail_1(){
        // 准备数据
        processAddUserGrade();
        List<UserGrade> userGrades = userGradeMapper.selectAll();
        UserGrade userGrade = getUserGrade(userGrades, "mmmm");

        UserGradeRsp userGradeRsp = userGradeService.userGradeDetail(userGrade.getId());
        Assert.assertEquals(Double.valueOf(3),userGradeRsp.getGradeWeight());
        Assert.assertEquals(Double.valueOf(85),userGradeRsp.getFirstCommission());
        Assert.assertEquals(Double.valueOf(12),userGradeRsp.getSecondCommission());
        Assert.assertEquals(Double.valueOf(3),userGradeRsp.getThirdCommission());
        Assert.assertEquals(Integer.valueOf(0),userGradeRsp.getEnableCmlevelSettings());
        Assert.assertEquals("mmmm",userGradeRsp.getGradeName());
    }

    /**
     * 记录不存在
     */
    @Test
    public void processUserGradeDetail_2(){

        try{
            userGradeService.userGradeDetail(888);
        }catch (BusinessException ex){
            Assert.assertEquals("记录不存在",ex.getMessage());
        }
    }

    /**
     * 删除用户等级
     * 成功
     */
    @Test
    public void processDeleteUserGrade_1(){
        // 准备数据
        processAddUserGrade();
        List<UserGrade> userGrades = userGradeMapper.selectAll();
        UserGrade userGrade = getUserGrade(userGrades, "mmmm");

        userGradeService.deleteUserGrade(userGrade.getId());
    }

}
