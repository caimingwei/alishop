package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.constants.WithDrawConstant;
import com.alipay.rebate.shop.dao.mapper.ActivityBrowseProductMapper;
import com.alipay.rebate.shop.dao.mapper.ActivityProductMapper;
import com.alipay.rebate.shop.dao.mapper.CouponGetRecordMapper;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.dao.mapper.WithDrawMapper;
import com.alipay.rebate.shop.model.CouponGetRecord;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.WithDraw;
import com.alipay.rebate.shop.service.admin.AdminWithdrawService;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class AdminWithdrawServiceImplTest {

  @Resource
  private UserMapper userMapper;
  @Resource
  private UserIncomeMapper userIncomeMapper;
  @Resource
  private WithDrawMapper withDrawMapper;
  @Resource
  private CouponGetRecordMapper couponGetRecordMapper;
  @Resource
  private AdminWithdrawService adminWithdrawService;


  @Before
  public void before() {

    userMapper.deleteUserById(1L);
    withDrawMapper.deleteByPrimaryKey(1L);
    withDrawMapper.deleteByPrimaryKey(2L);
    withDrawMapper.deleteByPrimaryKey(3L);
    userIncomeMapper.deleteUserIncomeByUserId(1L);
    couponGetRecordMapper.deleteByPrimaryKey(1L);

  }

    private User buildUser(
      Long userId,
      Long relationId , Long specialId,
      Long parentUserId, String phone,
      String taobaoUserId
  ){
    User user = new User();
    user.setUserId(userId);
    user.setUserNickName("mmm1");
    user.setUserStatus(UserContant.USER_STATUS_NORMAL);
    user.setIsAuth(UserContant.IS_AUTH);
    user.setRelationId(relationId);
    user.setSpecialId(specialId);
    user.setParentUserId(parentUserId);
    user.setTotalFansOrdersNum(0);
    user.setUserIntgeralTreasure(0L);
    user.setPhone(phone);
    user.setOrdersNum(0L);
    user.setUserGradeId(1);
    user.setUserIntgeral(0L);
    user.setUserAmount(new BigDecimal("0.00"));
    user.setUserIncome(new BigDecimal("0.00"));
    user.setUserCommision(new BigDecimal("0.00"));
    user.setTaobaoUserId(taobaoUserId);
    user.setCreateTime(new Date());
    user.setUpdateTime(new Date());
    return user;
  }

  private WithDraw buildWithDraw(){
    WithDraw withDraw = new WithDraw();
    withDraw.setIsFirst(0);
    withDraw.setCommitTime(new Date());
    withDraw.setApplyTime(new Date());
    withDraw.setType(WithDrawConstant.WITHDRAW_UIT_TYPE);
    withDraw.setAccount("897459376");
    withDraw.setUserId(1L);
    withDraw.setMoney(new BigDecimal(40));
    withDraw.setWithdrawId(1L);
    withDraw.setPhone("18819258225");
    withDraw.setRealName("xxx");
    withDraw.setStatus(WithDrawConstant.WITHDRAW_ONGOING);
    withDraw.setUserName("mmm1");
    return withDraw;
  }

  private CouponGetRecord buildCouponGetRecord(){
    CouponGetRecord couponGetRecord = new CouponGetRecord();
    couponGetRecord.setIsUse(0);
    couponGetRecord.setCreateTime(new Date());
    couponGetRecord.setUpdateTime(new Date());
    couponGetRecord.setId(1L);
    couponGetRecord.setType(1);
    couponGetRecord.setUserId(1L);
    couponGetRecord.setCouponId(12);
    return couponGetRecord;
  }

  @Test
  public void updateWithDrawRecord() {

    // 插入用户1
    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
    userMapper.insertSelective(user1);

    // 红包卷
    CouponGetRecord couponGetRecord = buildCouponGetRecord();
    couponGetRecordMapper.insertSelective(couponGetRecord);

    // 第一次提交成功 40 元
    WithDraw withDraw = buildWithDraw();
    withDrawMapper.insertWithDrawSelective(withDraw);
    WithDraw w1 = new WithDraw();
    w1.setWithdrawId(1L);
    w1.setStatus(WithDrawConstant.WITHDRAW_SUCCESS);
    adminWithdrawService.updateWithDrawRecord(w1);

    // 第二次提现成功40 元
    withDraw = buildWithDraw();
    withDraw.setWithdrawId(2L);
    withDrawMapper.insertWithDrawSelective(withDraw);
    w1 = new WithDraw();
    w1.setWithdrawId(2L);
    w1.setStatus(WithDrawConstant.WITHDRAW_SUCCESS);
    adminWithdrawService.updateWithDrawRecord(w1);

    // 第三次提现成功40 元
    withDraw = buildWithDraw();
    withDraw.setWithdrawId(3L);
    withDrawMapper.insertWithDrawSelective(withDraw);
    w1 = new WithDraw();
    w1.setWithdrawId(3L);
    w1.setStatus(WithDrawConstant.WITHDRAW_SUCCESS);
    adminWithdrawService.updateWithDrawRecord(w1);

    user1 = userMapper.selectById(1L);
    Assert.assertTrue(user1.getUserAmount().compareTo(new BigDecimal("10")) == 0);
  }


//  @Test
//  public void updateWithDrawRecord_1() {
//
////    // 插入用户1
////    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
////    userMapper.insertSelective(user1);
//
////    // 红包卷
////    CouponGetRecord couponGetRecord = buildCouponGetRecord();
////    couponGetRecordMapper.insertSelective(couponGetRecord);
//
//    WithDraw withDraw = buildWithDraw();
//    withDraw.setUserId(1002384L);
//    withDrawMapper.insertWithDrawSelective(withDraw);
//    WithDraw w1 = new WithDraw();
//    w1.setWithdrawId(1L);
//    w1.setStatus(WithDrawConstant.WITHDRAW_SUCCESS);
//    adminWithdrawService.updateWithDrawRecord(w1);
//
//
//    User user1 = userMapper.selectById(1002384L);
//    Assert.assertTrue(user1.getUserAmount().compareTo(new BigDecimal("10")) == 0);
//  }
}