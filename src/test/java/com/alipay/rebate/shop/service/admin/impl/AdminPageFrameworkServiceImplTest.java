package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.dao.mapper.PageFrameworkMapper;
import com.alipay.rebate.shop.model.PageFramework;
import com.alipay.rebate.shop.pojo.user.admin.PageFrameworkRequest;
import com.alipay.rebate.shop.service.admin.AdminPageFrameworkService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class AdminPageFrameworkServiceImplTest {

    private Logger logger = LoggerFactory.getLogger(AdminPageFrameworkServiceImplTest.class);
    @Resource
    private PageFrameworkMapper pageFrameworkMapper;
    @Resource
    AdminPageFrameworkService adminPageFrameworkService;

    @Before
    public void before(){
        pageFrameworkMapper.deletePageFramewordByTitle("mm1");
    }

    @Before
    public void After(){
        pageFrameworkMapper.deletePageFramewordByTitle("mm1");
    }

    private PageFrameworkRequest buildPageFrameworkRequest(Long pageId, String title, String illustration, String pageModules, Integer isIndex){
        PageFrameworkRequest pageFrameworkRequest = new PageFrameworkRequest();
        pageFrameworkRequest.setPageId(pageId);
        pageFrameworkRequest.setIllustration(illustration);
        pageFrameworkRequest.setIsIndex(isIndex);
        pageFrameworkRequest.setPageModules(pageModules);
        pageFrameworkRequest.setTitle(title);
        return pageFrameworkRequest;
    }

    private PageFramework buildPageFramework(Long pageId, String title, String illustration, String pageModules, Integer isIndex,Integer isDefault){
        PageFramework pageFramework = new PageFramework();
        pageFramework.setPageId(pageId);
        pageFramework.setIsDefault(isDefault);
        pageFramework.setIllustration(illustration);
        pageFramework.setTitle(title);
        pageFramework.setCreateTime(new Date());
        pageFramework.setUpdateTime(new Date());
        pageFramework.setIsUpdate(null);
        pageFramework.setPageModules(pageModules);
        pageFramework.setIsCurrentUse(1);
        pageFramework.setIsIndex(isIndex);
        return pageFramework;

    }

    private PageFramework getPageFramework(List<PageFramework> pageFrameworks,String title){
        for (PageFramework pageFramework : pageFrameworks){
            if (title.equals(pageFramework.getTitle())){
                return pageFramework;
            }
        }
        return null;
    }

    /**
     * 新建页面模板
     */
    @Test
    public void processCreateOrUpdatePage_1(){
        PageFrameworkRequest pageFrameworkRequest = buildPageFrameworkRequest(null, "mm1", "1",
                "lskdjfoisjg", 1);
        adminPageFrameworkService.createOrUpdatePage(pageFrameworkRequest);

        List<PageFramework> pageFrameworks = pageFrameworkMapper.selectAll();
        PageFramework pageFramework = getPageFramework(pageFrameworks, "mm1");
        Assert.assertEquals("mm1",pageFramework.getTitle());
        Assert.assertEquals("lskdjfoisjg",pageFramework.getPageModules());
        Assert.assertEquals(Integer.valueOf(1),pageFramework.getIsIndex());
        Assert.assertEquals("1",pageFramework.getIllustration());
    }

    /**
     * 更新页面模板
     */
    @Test
    public void processCreateOrUpdatePage_2(){
        processCreateOrUpdatePage_1();
        List<PageFramework> pageFrameworks = pageFrameworkMapper.selectAll();
        PageFramework pageFramework = getPageFramework(pageFrameworks, "mm1");
        PageFrameworkRequest pageFrameworkRequest = buildPageFrameworkRequest(pageFramework.getPageId(), "mm1", "0",
                "xiaoxiao", 0);
        adminPageFrameworkService.createOrUpdatePage(pageFrameworkRequest);
        List<PageFramework> pageFrameworks1 = pageFrameworkMapper.selectAll();
        PageFramework pageFramework2 = getPageFramework(pageFrameworks1, "mm1");
        Assert.assertEquals("mm1",pageFramework2.getTitle());
        Assert.assertEquals("xiaoxiao",pageFramework2.getPageModules());
        Assert.assertEquals(Integer.valueOf(0),pageFramework2.getIsIndex());
        Assert.assertEquals("0",pageFramework2.getIllustration());

    }

    /**
     * 设置为首页模板
     */
    @Test
    public void processCreateOrUpdatePage_3(){
        // 准备非首页模板
        PageFramework pageFramework = buildPageFramework(1L, "mm1", "1", "xiaojaksfla", 0, 0);
        pageFrameworkMapper.insertSelective(pageFramework);

        PageFramework pageFramework1 = buildPageFramework(1L, "mm1", "1", "xiaojaksfla", 0, 1);
        adminPageFrameworkService.updatePageFrameWork(pageFramework1);

        List<PageFramework> pageFrameworks1 = pageFrameworkMapper.selectAll();
        PageFramework pageFramework2 = getPageFramework(pageFrameworks1, "mm1");
        Assert.assertEquals("mm1",pageFramework2.getTitle());
        Assert.assertEquals("xiaojaksfla",pageFramework2.getPageModules());
        Assert.assertEquals(Integer.valueOf(0),pageFramework2.getIsIndex());
        Assert.assertEquals("1",pageFramework2.getIllustration());
        Assert.assertEquals(Integer.valueOf(1),pageFramework2.getIsDefault());

    }

}
