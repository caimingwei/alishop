package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.dao.mapper.BrowseProductMapper;
import com.alipay.rebate.shop.model.BrowseProduct;
import com.alipay.rebate.shop.service.customer.CustomerBrowseProductService;
import com.github.pagehelper.PageInfo;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerBrowseProductServiceImplTest {

    @Resource
    BrowseProductMapper browseProductMapper;
    @Resource
    CustomerBrowseProductService customerBrowseProductService;
    @Before
    public void before(){
        browseProductMapper.deleteByUserId(1L);

    }
    @After
    public void after(){
        browseProductMapper.deleteByUserId(1L);
    }

    private BrowseProduct buildBrowseProduct(){
        BrowseProduct browseProduct = new BrowseProduct();
        browseProduct.setUserId(1L);
        browseProduct.setGoodsId(983475L);
        browseProduct.setTitle("xiaovjaljd");
        browseProduct.setMainPic("http://lskjdfljlkjgsdgs:/");
        browseProduct.setActualPrice(new BigDecimal("3.06"));
        browseProduct.setCouponPrice(new BigDecimal("1.02"));
        return browseProduct;
    }

    /**
     * 添加商品浏览记录
     */
    @Test
    public void processAddBrowseProduct(){
        BrowseProduct browseProduct = buildBrowseProduct();
        int i = customerBrowseProductService.insertWhenNotExists(browseProduct);

        List<BrowseProduct> browseProducts = browseProductMapper.selectBrowseProductByUserId(1L);
        Assert.assertEquals(1,browseProducts.size());
        for (BrowseProduct browseProduct1 :browseProducts){
            Assert.assertEquals(Long.valueOf(1),browseProduct1.getUserId());
            Assert.assertEquals(Long.valueOf(983475),browseProduct1.getGoodsId());
            Assert.assertEquals("xiaovjaljd",browseProduct1.getTitle());
            Assert.assertEquals("http://lskjdfljlkjgsdgs:/",browseProduct1.getMainPic());
            Assert.assertTrue(new BigDecimal("3.06").compareTo(browseProduct1.getActualPrice()) == 0);
            Assert.assertTrue(new BigDecimal("1.02").compareTo(browseProduct1.getCouponPrice()) == 0);
        }
    }

    /**
     * 分页获取用户商品浏览记录
     */
    @Test
    public void processSelectUserBrowseProduct(){

        // 添加一条用户商品浏览记录
        processAddBrowseProduct();

        PageInfo<BrowseProduct> browseProductPageInfo = customerBrowseProductService.selectUserBrowseProduct(1L, 1, 20);
        List<BrowseProduct> list = browseProductPageInfo.getList();
        List<BrowseProduct> browseProducts = browseProductMapper.selectBrowseProductByUserId(1L);
        for (BrowseProduct browseProduct1 :browseProducts) {
            for (BrowseProduct browseProduct2 : browseProducts) {
                if (browseProduct1.getGoodsId() == browseProduct2.getGoodsId()) {
                    Assert.assertEquals(browseProduct2.getUserId(), browseProduct1.getUserId());
                    Assert.assertEquals(browseProduct2.getGoodsId(), browseProduct1.getGoodsId());
                    Assert.assertEquals( browseProduct2.getTitle(), browseProduct1.getTitle());
                    Assert.assertEquals( browseProduct2.getMainPic(), browseProduct1.getMainPic());
                    Assert.assertTrue(browseProduct2.getActualPrice().compareTo(browseProduct1.getActualPrice()) == 0);
                    Assert.assertTrue(browseProduct2.getCouponPrice().compareTo(browseProduct1.getCouponPrice()) == 0);
                }
            }
        }

    }

}
