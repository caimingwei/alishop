package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.model.SignInSettings;
import com.alipay.rebate.shop.service.admin.EntitySettingsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class EnititySettingsServiceImplTest {

    @Resource
    EntitySettingsService entitySettingsService;

    @Test
    public void test() throws JsonProcessingException {

        SignInSettings signInSettings = new SignInSettings();
        signInSettings.setId(1);
        signInSettings.setSiRule(1);
        signInSettings.setAwardForm(2);
        signInSettings.setFixAwardNum(0);
        signInSettings.setMinAwardNum(10);
        signInSettings.setMaxAwardNum(100);
        signInSettings.setRuleDesc("七天签到");
        signInSettings.setAwardType(1);
        signInSettings.setCreateTime(new Date(System.currentTimeMillis()));
        signInSettings.setUpdateTime(new Date(System.currentTimeMillis()));

        entitySettingsService.updateCustomerSignInSetting(signInSettings);

    }

}
