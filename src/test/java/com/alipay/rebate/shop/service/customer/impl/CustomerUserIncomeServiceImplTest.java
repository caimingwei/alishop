package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.service.customer.CustomerUserIncomeService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import com.github.pagehelper.PageInfo;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerUserIncomeServiceImplTest {

    @Resource
    CustomerUserIncomeService customerUserIncomeService;
    @Resource
    UserMapper userMapper;
    @Resource
    UserIncomeMapper userIncomeMapper;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        User user = buildUser(1L, "mm1", null, null, "13451334526");
        userMapper.insertSelective(user);

    }

    @After
    public void after(){
        userMapper.deleteUserById(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
    }


    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone) {
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private UserIncome buildUserIncome(Long id,Long userId,Integer type,Integer status,Integer awardType,
                                       BigDecimal money,Long tkStatus,String tradeId){
        UserIncome userIncome = new UserIncome();
        userIncome.setUserIncomeId(id);
        userIncome.setType(type);
        userIncome.setMoney(money);
        userIncome.setStatus(status);
        userIncome.setUserId(userId);
        userIncome.setParentTradeId(null);
        userIncome.setRoadType(0);
        userIncome.setTkStatus(tkStatus);
        userIncome.setRemarks(null);
        userIncome.setIntgeralNum(null);
        userIncome.setTradeId(tradeId);
        userIncome.setRelationId(null);
        userIncome.setSpecialId(null);
        userIncome.setUpdateTime("2020-06-17 00:34:35");
        userIncome.setCreateTime("2020-06-17 00:34:35");
        userIncome.setAwardType(awardType);
        return userIncome;
    }

    private UserIncome getUserIncomeByType(List<UserIncome> list,Integer type){
        for (UserIncome userIncome : list) {
            if (userIncome.getType() == type){
                return userIncome;
            }
        }
        return null;
    }

    /**
     * 查询用户收益详情 -》 没有收益记录
     */
    @Test
    public void selectUserAccountDetailIncome() {
        PageInfo<UserIncome> userIncomePageInfo = customerUserIncomeService.selectUserOrdersDetailIncome(1L, 1, 10);
        Assert.assertEquals(0,userIncomePageInfo.getTotal());
    }

    /**
     * 查询用户淘宝收益详情 -》 没有收益记录
     */
    @Test
    public void selectUserOrdersDetailIncome() {
        PageInfo<UserIncome> userIncomePageInfo = customerUserIncomeService.selectUserOrdersDetailIncome(1L, 1, 10);
        Assert.assertEquals(0,userIncomePageInfo.getTotal());
    }

    /**
     * 查询用户拼多多收益详情 -》 没有收益记录
     */
    @Test
    public void selectPddOrdersDetailIncome() {
        PageInfo<UserIncome> userIncomePageInfo = customerUserIncomeService.selectPddOrdersDetailIncome(1L, 1, 10);
        Assert.assertEquals(0,userIncomePageInfo.getTotal());
    }

    /**
     * 查询用户京东收益详情 -》 没有收益记录
     */
    @Test
    public void selectJdOrdersDetailIncome() {
        PageInfo<UserIncome> userIncomePageInfo = customerUserIncomeService.selectJdOrdersDetailIncome(1L, 1, 10);
        Assert.assertEquals(0,userIncomePageInfo.getTotal());
    }

    /**
     * 查询用户收益详情
     */
    @Test
    public void selectUserDetailIncome(){
        // 准备一条收益记录在数据库
        UserIncome userIncome = buildUserIncome(1L, 1L, 2, 1, 2, new BigDecimal("0.01"), null, null);
        userIncomeMapper.insertSelective(userIncome);

        // 账户收益
        PageInfo<UserIncome> userIncomePageInfo = customerUserIncomeService.selectUserAccountDetailIncome(1L, 1, 10);
        Assert.assertEquals(1,userIncomePageInfo.getTotal());
        List<UserIncome> list = userIncomePageInfo.getList();
        UserIncome userIncomeByType = getUserIncomeByType(list, 2);
        Assert.assertEquals(Long.valueOf(1),userIncomeByType.getUserId());
        Assert.assertEquals(Integer.valueOf(2),userIncomeByType.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncomeByType.getStatus());
        Assert.assertEquals(Integer.valueOf(2),userIncomeByType.getAwardType());
        Assert.assertTrue(new BigDecimal("0.01").compareTo(userIncomeByType.getMoney()) == 0);

        // 准备淘宝订单收益
        UserIncome userIncome2 = buildUserIncome(2L, 1L, 1, 1, 2, new BigDecimal("0.01"), null, null);
        userIncomeMapper.insertSelective(userIncome2);
        PageInfo<UserIncome> userIncomePageInfo2 = customerUserIncomeService.selectUserOrdersDetailIncome(1L, 1, 10);
        Assert.assertEquals(1,userIncomePageInfo2.getTotal());
        List<UserIncome> list2 = userIncomePageInfo2.getList();
        UserIncome userIncomeByType2 = getUserIncomeByType(list2, 1);
        Assert.assertEquals(Long.valueOf(1),userIncomeByType2.getUserId());
        Assert.assertEquals(Integer.valueOf(1),userIncomeByType2.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncomeByType2.getStatus());
        Assert.assertEquals(Integer.valueOf(2),userIncomeByType2.getAwardType());
        Assert.assertTrue(new BigDecimal("0.01").compareTo(userIncomeByType2.getMoney()) == 0);

        // 准备拼多多订单收益
        UserIncome userIncome3 = buildUserIncome(5L, 1L, 16, 1, 2, new BigDecimal("0.01"), null, null);
        userIncomeMapper.insertSelective(userIncome3);
        PageInfo<UserIncome> userIncomePageInfo3 = customerUserIncomeService.selectPddOrdersDetailIncome(1L, 1, 10);
        Assert.assertEquals(1,userIncomePageInfo3.getTotal());
        List<UserIncome> list3 = userIncomePageInfo3.getList();
        UserIncome userIncomeByType3 = getUserIncomeByType(list3, 16);
        Assert.assertEquals(Long.valueOf(1),userIncomeByType3.getUserId());
        Assert.assertEquals(Integer.valueOf(16),userIncomeByType3.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncomeByType3.getStatus());
        Assert.assertEquals(Integer.valueOf(2),userIncomeByType3.getAwardType());
        Assert.assertTrue(new BigDecimal("0.01").compareTo(userIncomeByType3.getMoney()) == 0);

        // 准备京东订单收益
        UserIncome userIncome4 = buildUserIncome(4L, 1L, 17, 1, 2, new BigDecimal("0.01"), null, null);
        userIncomeMapper.insertSelective(userIncome4);
        PageInfo<UserIncome> userIncomePageInfo4 = customerUserIncomeService.selectJdOrdersDetailIncome(1L, 1, 10);
        Assert.assertEquals(1,userIncomePageInfo4.getTotal());
        List<UserIncome> list4 = userIncomePageInfo4.getList();
        UserIncome userIncomeByType4 = getUserIncomeByType(list4, 17);
        Assert.assertEquals(Long.valueOf(1),userIncomeByType4.getUserId());
        Assert.assertEquals(Integer.valueOf(17),userIncomeByType4.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncomeByType4.getStatus());
        Assert.assertEquals(Integer.valueOf(2),userIncomeByType4.getAwardType());
        Assert.assertTrue(new BigDecimal("0.01").compareTo(userIncomeByType4.getMoney()) == 0);
    }

}