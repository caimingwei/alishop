package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.user.admin.UpdateUserRequest;
import com.alipay.rebate.shop.pojo.user.customer.UpdateUserStatusReq;
import com.alipay.rebate.shop.service.admin.AdminCustomerUserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class AdminCustomerUserServiceImplTest {

    @Resource
    UserMapper userMapper;
    @Resource
    AdminCustomerUserService adminCustomerUserService;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        User user = buildUser(1L, null, null, null, "156723583456", null);
        userMapper.insertSelective(user);
    }
    @After
    public void after(){
        userMapper.deleteUserById(1L);

    }

    private User buildUser(
            Long userId,
            Long relationId , Long specialId,
            Long parentUserId, String phone,
            String taobaoUserId
    ){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName("mmm1");
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(relationId);
        user.setSpecialId(specialId);
        user.setParentUserId(parentUserId);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(taobaoUserId);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserStatus(1);
        return user;
    }

    private UpdateUserRequest buildUpdateUserRequest(Long userId,String userName,String realName,String alipayAccount,
                                                     BigDecimal userAmount,Long userIntgeralTreasure,Long userIntgeral,String phone,
                                                     String userPassword,Integer userGradeId,String password){
        UpdateUserRequest request = new UpdateUserRequest();
        request.setUserId(userId);
        request.setAlipayAccount(alipayAccount);
        request.setUserNickName(userName);
        request.setPhone(phone);
        request.setRealName(realName);
        request.setUserAmount(userAmount);
        request.setUserIntgeralTreasure(userIntgeralTreasure);
        request.setUserIntgeral(userIntgeral);
        request.setUserGradeId(userGradeId);
        request.setUserPassword(userPassword);
        request.setPassword(password);
        return request;
    }

    private UpdateUserStatusReq buildUpdateUserStatusReq(Long userId,Integer status,String remarks){
        UpdateUserStatusReq userStatusReq = new UpdateUserStatusReq();
        userStatusReq.setUserId(userId);
        userStatusReq.setRemarks(remarks);
        userStatusReq.setStatus(status);
        return userStatusReq;
    }

    /**
     * 修改用户信息
     */
    @Test
    public void updateUserById() {
        UpdateUserRequest request = buildUpdateUserRequest(1L, "mm1", "mm", "2446576918",
                null, null, null, "15674223569", null, null, null);
        adminCustomerUserService.updateUserById(request);

        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals("mm1",user.getUserNickName());
        Assert.assertEquals("mm",user.getRealName());
        Assert.assertEquals("2446576918",user.getAlipayAccount());
        Assert.assertEquals("15674223569",user.getPhone());

    }

    /**
     * 修改用户信息 -> 校验密码出错
     */
    @Test
    public void updateUserById_1() {
        try {
            UpdateUserRequest request = buildUpdateUserRequest(1L, "mm1", "mm", "2446576918",
                    null, 100L, null, "15674223569", null, null, "12345678");
            adminCustomerUserService.updateUserById(request);
        }catch (RuntimeException e){
            Assert.assertEquals("校验密码输入错误",e.getMessage());
        }
    }

    /**
     * 修改用户状态
     */
    @Test
    public void updateUserStatus_1(){
        try {
            UpdateUserStatusReq userStatusReq = buildUpdateUserStatusReq(1L, 0, null);
            adminCustomerUserService.updateUserStatus(userStatusReq);
        }catch (BusinessException e){
            Assert.assertEquals("拉黑备注信息不能为空",e.getMessage());
        }
    }

    @Test
    public void updateUserStatus_2(){
        UpdateUserStatusReq userStatusReq = buildUpdateUserStatusReq(1L, 0, "mmmm");
        adminCustomerUserService.updateUserStatus(userStatusReq);
        User user = userMapper.selectById(1L);
        Assert.assertEquals(Integer.valueOf(0),user.getUserStatus());
    }
}