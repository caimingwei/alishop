package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.EntitySettingsMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.entitysettings.PcfSettings;
import com.alipay.rebate.shop.service.customer.CustomerEntitySettingsService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerEntitySettingsServiceImplTest {

    @Resource
    private EntitySettingsMapper entitySettingsMapper;
    @Resource
    UserMapper userMapper;
    @Resource
    CustomerEntitySettingsService customerEntitySettingsService;
    private Logger logger = LoggerFactory.getLogger(CustomerEntitySettingsServiceImplTest.class);

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        User user = buildUser(1L, "mm1", null, null, "1347625983");
        userMapper.insertSelective(user);
    }
    @After
    public void after(){
        userMapper.deleteUserById(1L);
    }

    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }


    /**
     * 个人中心板块配置详情
     * @throws IOException
     */
    @Test
    public void processPcfSettingsDetail() throws IOException {
        PcfSettings pcfSettings = customerEntitySettingsService.pcfSettingsDetail();
    }

}
