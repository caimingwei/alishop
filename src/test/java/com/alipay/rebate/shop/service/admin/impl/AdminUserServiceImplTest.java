package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.AdminUserMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.AdminUser;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserObjRsp;
import com.alipay.rebate.shop.pojo.user.admin.AdminUserRewordReq;
import com.alipay.rebate.shop.pojo.user.customer.UserDto;
import com.alipay.rebate.shop.service.admin.AdminUserService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class AdminUserServiceImplTest {

    @Resource
    UserMapper userMapper;
    @Resource
    AdminUserService adminUserService;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    AdminUserMapper adminUserMapper;
    @Resource
    HttpServletResponse response;
    @Resource
    HttpServletRequest request;

    @Before
    public void before(){
        adminUserMapper.deleteAdminUserByAccount("666666");
        adminUserMapper.deleteAdminUserByAccount("888888");
        userMapper.deleteUserById(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        User user = buildUser(1L, null, null,
                null, "13487234234", null);
        userMapper.insertSelective(user);
    }

    @After
    public void after(){
        adminUserMapper.deleteAdminUserByAccount("666666");
        adminUserMapper.deleteAdminUserByAccount("888888");
        userMapper.deleteUserById(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
    }

    private User buildUser(
            Long userId,
            Long relationId , Long specialId,
            Long parentUserId, String phone,
            String taobaoUserId
    ){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName("mmm1");
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(relationId);
        user.setSpecialId(specialId);
        user.setParentUserId(parentUserId);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(taobaoUserId);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return user;
    }

    private AdminUserRewordReq buildAdminUserRewordReq(Long userId,BigDecimal rewordNum,String remark){
        AdminUserRewordReq adminUserRewordReq = new AdminUserRewordReq();
        adminUserRewordReq.setUserId(userId);
        adminUserRewordReq.setRemarks(remark);
        adminUserRewordReq.setReword(rewordNum);
        return adminUserRewordReq;
    }

    private AdminUser buildAdminUser(String userAccount,String password,String groupId,String createTime){
        AdminUser adminUser = new AdminUser();
        adminUser.setUserAccount(userAccount);
        adminUser.setUserPassword(password);
        adminUser.setCreateTime(null);
        adminUser.setGroupId(Integer.valueOf(groupId));
        return adminUser;
    }

    private UserDto buildUserDto(String account,String password){
        UserDto userDto = new UserDto();
        userDto.setAccount(account);
        userDto.setPassword(password.toCharArray());
        return userDto;
    }

    /**
     * 添加管理员
     */
    @Test
    public void processAddAdminUser_1(){
        AdminUser adminUser = buildAdminUser("666666", "123456", "1", null);
        adminUserService.addNewAdminUser(adminUser);
        AdminUser adminUser1 = adminUserMapper.selectUserByAccount("666666");
        Assert.assertEquals("666666",adminUser1.getUserAccount());
        Assert.assertEquals("123456",adminUser1.getUserPassword());
        Assert.assertEquals(Integer.valueOf(1),adminUser1.getGroupId());
    }

    /**
     * 管理员已存在
     */
    @Test
    public void processAddAdminUser_2(){
        processAddAdminUser_1();
        AdminUser adminUser = buildAdminUser("666666", "123456", "1", null);
        try {
            adminUserService.addNewAdminUser(adminUser);
        }catch(BusinessException ex){
            Assert.assertEquals("账号已存在",ex.getMessage());
        }
    }

    /**
     * 更新管理员信息
     */
    @Test
    public void processUpdateAdminUser_1(){

        AdminUser adminUser = buildAdminUser("666666", "123456", "1", null);
        adminUser.setId(1L);
        adminUserMapper.insertSelective(adminUser);

        AdminUser adminUser1 = buildAdminUser("888888", "123456", "1", null);
        adminUser1.setId(1L);
        adminUserService.updateAdminUser(adminUser1);
        AdminUser adminUser2 = adminUserMapper.selectUserByAccount("888888");
        Assert.assertEquals("888888",adminUser2.getUserAccount());
        Assert.assertEquals("123456",adminUser2.getUserPassword());
        Assert.assertEquals(Integer.valueOf(1),adminUser2.getGroupId());

    }

    /**
     * 更新管理员信息
     */
    @Test
    public void processUpdateAdminUser_2(){

        AdminUser adminUser = buildAdminUser("666666", "123456", "1", null);
        adminUser.setId(1L);
        adminUserMapper.insertSelective(adminUser);

        AdminUser adminUser1 = buildAdminUser("666666", "123456", "1", null);
        adminUser1.setId(1L);
        try {
            adminUserService.addNewAdminUser(adminUser);
        }catch(BusinessException ex){
            Assert.assertEquals("账号已存在",ex.getMessage());
        }
    }

    /**
     * 系统奖励用户集分宝
     */
    @Test
    public void processRewordUser_1(){
        AdminUserRewordReq adminUserRewordReq = buildAdminUserRewordReq(1L, new BigDecimal("10.00"), "系统奖励");
        int i = adminUserService.rewordUser(adminUserRewordReq, 1);

        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals(Long.valueOf(1000),user.getUserIntgeralTreasure());

        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 12);
        Assert.assertEquals(Long.valueOf(1L),userIncome.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(userIncome.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(12),userIncome.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
    }

    /**
     * 系统奖励用户金额
     */
    @Test
    public void processRewordUser_2(){
        AdminUserRewordReq adminUserRewordReq = buildAdminUserRewordReq(1L, new BigDecimal("10.00"), "系统奖励");
        int i = adminUserService.rewordUser(adminUserRewordReq, 0);

        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(user.getUserAmount()) == 0);

        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 12);
        Assert.assertEquals(Long.valueOf(1L),userIncome.getUserId());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(userIncome.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(12),userIncome.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
    }

    /**
     * 系统奖励用户金额
     */
    @Test
    public void processRewordUser_3(){
        AdminUserRewordReq adminUserRewordReq = buildAdminUserRewordReq(1L, new BigDecimal("10.00"), "系统奖励");
        int i = adminUserService.rewordUser(adminUserRewordReq, 2);

        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals(Long.valueOf(10),user.getUserIntgeral());

        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 12);
        Assert.assertEquals(Long.valueOf(1L),userIncome.getUserId());
        Assert.assertEquals(Long.valueOf(10),userIncome.getIntgeralNum());
        Assert.assertEquals(Integer.valueOf(12),userIncome.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
    }
//
//    @Test
//    public void processAdminLogin(){
//        Subject subject = SecurityUtils.getSubject();
//        UserDto userDto = new UserDto();
//        userDto.setAccount("13487234234");
//        userDto.setPassword("123456".toCharArray());
//        AdminUserObjRsp adminUserObjRsp = adminUserService.adminLogin(userDto, request, response, subject);
//
//        Assert.assertEquals("13487234234",adminUserObjRsp.getUserAccount());
//    }

}
