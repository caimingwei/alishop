package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.pojo.entitysettings.InrUser;
import com.alipay.rebate.shop.pojo.entitysettings.IrUser;
import com.alipay.rebate.shop.service.customer.CustomerAppService;
import com.alipay.rebate.shop.service.customer.CustomerWithdrawService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerAppServiceImplTest {

    @Resource
    CustomerAppService customerAppService;

    @Test
    public void inviteNumRank() throws IOException {
        List<InrUser> inrUsers = customerAppService.inviteNumRank();
    }

    @Test
    public void incomeRank() throws IOException {
        List<IrUser> irUsers = customerAppService.incomeRank();
    }
}