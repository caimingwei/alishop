package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.SignInAwardMapper;
import com.alipay.rebate.shop.dao.mapper.SignInRecordMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.SignInAward;
import com.alipay.rebate.shop.model.SignInRecord;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.customer.SignInReq;
import com.alipay.rebate.shop.service.customer.CustomerSignInRecordService;
import com.alipay.rebate.shop.utils.DecimalUtil;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerSignInRecordServiceImplTest {

    @Resource
    UserMapper userMapper;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    SignInAwardMapper signInAwardMapper;
    @Resource
    CustomerSignInRecordService signInRecordService;
    @Resource
    SignInRecordMapper signInRecordMapper;
    Logger logger = LoggerFactory.getLogger(CustomerSignInRecordServiceImplTest.class);

    @Before
    public void before(){
        userMapper.deleteUserById(10L);
        userIncomeMapper.deleteUserIncomeByUserId(10L);
        signInRecordMapper.deleteByUserId(10L);
        signInAwardMapper.deleteByUserId(10L);

        User user = buildUser(10L, "mm1", null, null, "13417689637");
        userMapper.insertSelective(user);
    }

    @After
    public void after(){
        userMapper.deleteUserById(10L);
        userIncomeMapper.deleteUserIncomeByUserId(10L);
        signInRecordMapper.deleteByUserId(10L);
        signInAwardMapper.deleteByUserId(10L);
    }

    private SignInReq buildSignInReq(Integer num,String time){
        SignInReq signInReq = new SignInReq();
        signInReq.setNum(num);
        signInReq.setSignInTime(time);
        return signInReq;
    }

    private SignInRecord buildSignInRecord(Long userId,Date date){
        SignInRecord signInRecord = new SignInRecord();
        signInRecord.setAwardType(2);
        signInRecord.setSignInTime(date);
        signInRecord.setUserId(userId);
        signInRecord.setEstimateMoney(new BigDecimal("0.02"));
        signInRecord.setEstimateIntegral(0);
        signInRecord.setMoney(new BigDecimal(0));
        signInRecord.setIntegral(0);
        return signInRecord;
    }

    private Date getDate(int type){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, type); //得到前一du天
        Date date = calendar.getTime();
        return date;
    }


    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private SignInAward buildSignInAward(Integer signInDays,Date date,BigDecimal money){
        SignInAward signInAward = new SignInAward();
        signInAward.setSignInDays(signInDays);
        signInAward.setTotalAwardIntegral(0);
        signInAward.setTotalAwardMoney(money);
        signInAward.setUserId(10L);
        signInAward.setNewlySignInTime(date);
        return  signInAward;
    }

    /**
     * 第一天签到
     * @throws ParseException
     */
    @Test
    public void processSignIn() throws ParseException {
        SignInReq signInReq = buildSignInReq(10, null);
        signInRecordService.signIn(10L, signInReq);

        SignInAward signInAward = signInAwardMapper.selectByUserId(10L);
        logger.debug("signInAward is : {}",signInAward);
        Assert.assertEquals(Long.valueOf(10L),signInAward.getUserId());
        Assert.assertEquals(Integer.valueOf(0),signInAward.getTotalAwardIntegral());
        Assert.assertEquals(Integer.valueOf(1),signInAward.getSignInDays());
        logger.debug("money is : {}",signInAward.getTotalAwardMoney());
        Assert.assertTrue(new BigDecimal("0.02").compareTo(signInAward.getTotalAwardMoney()) == 0);

        User user = userMapper.selectById(10L);
        Assert.assertEquals(Long.valueOf(0),user.getUserIntgeralTreasure());
    }


    /**
     * 当天已签到
     * @throws ParseException
     */
    @Test
    public void processSignIn_1() throws ParseException {
        SignInRecord signInRecord = buildSignInRecord(10l,new Date());
        signInRecordMapper.insertSelective(signInRecord);
        SignInReq signInReq = buildSignInReq(10, null);
        try {
            signInRecordService.signIn(10L, signInReq);
        }catch (BusinessException exception){
            Assert.assertEquals("当天已签到",exception.getMessage());
        }
    }

    /**
     * 第二天签到
     * @throws ParseException
     */
    @Test
    public void processSignIn_2() throws ParseException {
        SignInRecord signInRecord = buildSignInRecord(10L,getDate(-1));
        signInRecordMapper.insertSelective(signInRecord);
        // 插入签到奖励记录
        SignInAward signInAward1 = buildSignInAward(1, getDate(-1), new BigDecimal("0.02"));
        signInAwardMapper.insertSelective(signInAward1);

        SignInReq signInReq = buildSignInReq(10, null);
        signInRecordService.signIn(10L, signInReq);

        SignInAward signInAward = signInAwardMapper.selectByUserId(10L);
        logger.debug("signInAward is : {}",signInAward);
        Assert.assertEquals(Long.valueOf(10L),signInAward.getUserId());
        Assert.assertEquals(Integer.valueOf(0),signInAward.getTotalAwardIntegral());
        Assert.assertEquals(Integer.valueOf(2),signInAward.getSignInDays());
        logger.debug("money is : {}",signInAward.getTotalAwardMoney());
        Assert.assertTrue(new BigDecimal("0.04").compareTo(signInAward.getTotalAwardMoney()) == 0);
    }

    /**
     * 第三天签到
     * @throws ParseException
     */
    @Test
    public void processSignIn_3() throws ParseException {
        SignInRecord signInRecord = buildSignInRecord(10L,getDate(-1));
        signInRecordMapper.insertSelective(signInRecord);
        // 插入签到奖励记录
        SignInAward signInAward1 = buildSignInAward(2, getDate(-1), new BigDecimal("0.02"));
        signInAwardMapper.insertSelective(signInAward1);

        SignInReq signInReq = buildSignInReq(10, null);
        signInRecordService.signIn(10L, signInReq);

        SignInAward signInAward = signInAwardMapper.selectByUserId(10L);
        logger.debug("signInAward is : {}",signInAward);
        Assert.assertEquals(Long.valueOf(10L),signInAward.getUserId());
        Assert.assertEquals(Integer.valueOf(0),signInAward.getTotalAwardIntegral());
        Assert.assertEquals(Integer.valueOf(3),signInAward.getSignInDays());
        logger.debug("money is : {}",signInAward.getTotalAwardMoney());
        Assert.assertTrue(new BigDecimal("0.06").compareTo(signInAward.getTotalAwardMoney()) == 0);
    }

    /**
     * 第四天签到
     * @throws ParseException
     */
    @Test
    public void processSignIn_4() throws ParseException {
        SignInRecord signInRecord = buildSignInRecord(10L,getDate(-1));
        signInRecordMapper.insertSelective(signInRecord);
        // 插入签到奖励记录
        SignInAward signInAward1 = buildSignInAward(3, getDate(-1), new BigDecimal("0.06"));
        signInAwardMapper.insertSelective(signInAward1);

        SignInReq signInReq = buildSignInReq(10, null);
        signInRecordService.signIn(10L, signInReq);

        SignInAward signInAward = signInAwardMapper.selectByUserId(10L);
        logger.debug("signInAward is : {}",signInAward);
        Assert.assertEquals(Long.valueOf(10L),signInAward.getUserId());
        Assert.assertEquals(Integer.valueOf(0),signInAward.getTotalAwardIntegral());
        Assert.assertEquals(Integer.valueOf(4),signInAward.getSignInDays());
        logger.debug("money is : {}",signInAward.getTotalAwardMoney());
        Assert.assertTrue(new BigDecimal("0.08").compareTo(signInAward.getTotalAwardMoney()) == 0);
    }

    /**
     * 第五天签到
     * @throws ParseException
     */
    @Test
    public void processSignIn_5() throws ParseException {
        SignInRecord signInRecord = buildSignInRecord(10L,getDate(-1));
        signInRecordMapper.insertSelective(signInRecord);
        // 插入签到奖励记录
        SignInAward signInAward1 = buildSignInAward(4, getDate(-1), new BigDecimal("0.08"));
        signInAwardMapper.insertSelective(signInAward1);

        SignInReq signInReq = buildSignInReq(10, null);
        signInRecordService.signIn(10L, signInReq);

        SignInAward signInAward = signInAwardMapper.selectByUserId(10L);
        logger.debug("signInAward is : {}",signInAward);
        Assert.assertEquals(Long.valueOf(10L),signInAward.getUserId());
        Assert.assertEquals(Integer.valueOf(0),signInAward.getTotalAwardIntegral());
        Assert.assertEquals(Integer.valueOf(5),signInAward.getSignInDays());
        logger.debug("money is : {}",signInAward.getTotalAwardMoney());
        Assert.assertTrue(new BigDecimal("0.10").compareTo(signInAward.getTotalAwardMoney()) == 0);
    }

    /**
     * 第六天签到
     * @throws ParseException
     */
    @Test
    public void processSignIn_6() throws ParseException {
        SignInRecord signInRecord = buildSignInRecord(10L,getDate(-1));
        signInRecordMapper.insertSelective(signInRecord);
        // 插入签到奖励记录
        SignInAward signInAward1 = buildSignInAward(5, getDate(-1), new BigDecimal("0.10"));
        signInAwardMapper.insertSelective(signInAward1);

        SignInReq signInReq = buildSignInReq(10, null);
        signInRecordService.signIn(10L, signInReq);

        SignInAward signInAward = signInAwardMapper.selectByUserId(10L);
        logger.debug("signInAward is : {}",signInAward);
        Assert.assertEquals(Long.valueOf(10L),signInAward.getUserId());
        Assert.assertEquals(Integer.valueOf(0),signInAward.getTotalAwardIntegral());
        Assert.assertEquals(Integer.valueOf(6),signInAward.getSignInDays());
        logger.debug("money is : {}",signInAward.getTotalAwardMoney());
        Assert.assertTrue(new BigDecimal("0.12").compareTo(signInAward.getTotalAwardMoney()) == 0);
    }

    /**
     * 第7天签到
     * @throws ParseException
     */
    @Test
    public void processSignIn_7() throws ParseException {
        SignInRecord signInRecord = buildSignInRecord(10L,getDate(-1));
        signInRecordMapper.insertSelective(signInRecord);
        // 插入签到奖励记录
        SignInAward signInAward1 = buildSignInAward(6, getDate(-1), new BigDecimal("0.12"));
        signInAwardMapper.insertSelective(signInAward1);

        SignInReq signInReq = buildSignInReq(10, null);
        signInRecordService.signIn(10L, signInReq);

        SignInAward signInAward = signInAwardMapper.selectByUserId(10L);
        logger.debug("signInAward is : {}",signInAward);
        Assert.assertEquals(Long.valueOf(10L),signInAward.getUserId());
        Assert.assertEquals(Integer.valueOf(0),signInAward.getTotalAwardIntegral());
        Assert.assertEquals(Integer.valueOf(7),signInAward.getSignInDays());
        logger.debug("money is : {}",signInAward.getTotalAwardMoney());
        Assert.assertTrue(new BigDecimal("0.14").compareTo(signInAward.getTotalAwardMoney()) == 0);

//        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(10L, 4);
//        logger.debug("userIncome is : {}",userIncome);
//        Assert.assertEquals(Long.valueOf(10L),userIncome.getUserId());
//        Assert.assertEquals(Integer.valueOf(4),userIncome.getType());
//        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
//        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
//        Assert.assertTrue(new BigDecimal("0.14").compareTo(userIncome.getMoney()) == 0);
//        User user = userMapper.selectById(10L);
//        Assert.assertEquals(Long.valueOf(14),user.getUserIntgeralTreasure());
    }
}
