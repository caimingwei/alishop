package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.dao.mapper.SmsGroupSendMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.helper.SmsMessageSender;
import com.alipay.rebate.shop.model.SmsGroupSend;
import com.alipay.rebate.shop.service.admin.SmsGroupSendService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class SmsGroupSendServiceImplTest {

    private Logger logger = LoggerFactory.getLogger(SmsGroupSendServiceImplTest.class);
    @Resource
    private SmsGroupSendMapper smsGroupSendMapper;
    @Resource
    private SmsMessageSender smsMessageSender;
    @Resource
    @Qualifier("sharePool")
    private ExecutorService executorService;
    @Resource
    SmsGroupSendService smsGroupSendService;

    @Before
    public void before(){
        smsGroupSendMapper.deleteByPhone("134133037645");
    }
    @After
    public void after(){
        smsGroupSendMapper.deleteByPhone("134133037645");
    }

    private SmsGroupSend buildSmsGroupSend(Integer sendObj, Integer conditionDay, Integer sendType,
                                           Date sendTime, String phoneNum, String content){
        SmsGroupSend smsGroupSend = new SmsGroupSend();
        smsGroupSend.setConditionDay(conditionDay);
        smsGroupSend.setContent(content);
        smsGroupSend.setSendType(sendType);
        smsGroupSend.setSendObj(sendObj);
        smsGroupSend.setSendTime(sendTime);
        smsGroupSend.setPhoneNum(phoneNum);
        return smsGroupSend;
    }

    private SmsGroupSend getSmsGroupSend( List<SmsGroupSend> smsGroupSends ,String phone){
        for (SmsGroupSend smsGroupSend : smsGroupSends){

            if (smsGroupSend.getPhoneNum() != null) {
                if (smsGroupSend.getPhoneNum().equals(phone)) {
                    return smsGroupSend;
                }
            }
        }
        return null;
    }

    /**
     * 新建短信群发
     * 手机号码发送
     */
    @Test
    public void processAddSmsGroupSend_1(){

        SmsGroupSend smsGroupSend = buildSmsGroupSend(0, null,
                1, null, "134133037645", "lskdjf");
        smsGroupSendService.addSmsGroupSend(smsGroupSend);
        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");
        Assert.assertEquals("134133037645",smsGroupSend1.getPhoneNum());
        Assert.assertEquals("lskdjf",smsGroupSend1.getContent());
        Assert.assertEquals(Integer.valueOf(0),smsGroupSend1.getSendObj());
        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getSendType());
        Assert.assertEquals(Integer.valueOf(0),smsGroupSend1.getStatus());
    }

//    /**
//     * 新建短信群发,测试此方法会发送短信到所有用户手机上
//     * 所有人
//     */
//    @Test
//    public void processAddSmsGroupSend_2(){
//
//        SmsGroupSend smsGroupSend = buildSmsGroupSend(1, null,
//                1, new Date(), "134133037645", "lskdjf");
//        smsGroupSendService.addSmsGroupSend(smsGroupSend);
//        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
//        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");
//        Assert.assertEquals("lskdjf",smsGroupSend1.getContent());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getSendObj());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getSendType());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getStatus());
//        Assert.assertEquals("134133037645",smsGroupSend1.getPhoneNum());
//    }
//    /**
//     * 新建短信群发，给活跃用户群发短信
//     */
//    @Test
//    public void processAddSmsGroupSend_3(){
//
//        SmsGroupSend smsGroupSend = buildSmsGroupSend(2, 2,
//                1, null, "134133037645", "lskdjf");
//        smsGroupSendService.addSmsGroupSend(smsGroupSend);
//        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
//        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");
//        Assert.assertEquals("134133037645",smsGroupSend1.getPhoneNum());
//        Assert.assertEquals("lskdjf",smsGroupSend1.getContent());
//        Assert.assertEquals(Integer.valueOf(2),smsGroupSend1.getSendObj());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getSendType());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getStatus());
//        Assert.assertEquals(Integer.valueOf(2),smsGroupSend1.getConditionDay());
//    }
//    /**
//     * 新建短信群发，给休眠用户群发短信
//     */
//    @Test
//    public void processAddSmsGroupSend_4(){
//
//        SmsGroupSend smsGroupSend = buildSmsGroupSend(3, 2,
//                1, null, "134133037645", "lskdjf");
//        smsGroupSendService.addSmsGroupSend(smsGroupSend);
//        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
//        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");
//        Assert.assertEquals("134133037645",smsGroupSend1.getPhoneNum());
//        Assert.assertEquals("lskdjf",smsGroupSend1.getContent());
//        Assert.assertEquals(Integer.valueOf(3),smsGroupSend1.getSendObj());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getSendType());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getStatus());
//        Assert.assertEquals(Integer.valueOf(2),smsGroupSend1.getConditionDay());
//    }
//    /**
//     * 新建短信群发，给新注册用户群发短信
//     */
//    @Test
//    public void processAddSmsGroupSend_5(){
//
//        SmsGroupSend smsGroupSend = buildSmsGroupSend(4, 2,
//                1, null, "134133037645", "lskdjf");
//        smsGroupSendService.addSmsGroupSend(smsGroupSend);
//        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
//        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");
//        Assert.assertEquals("134133037645",smsGroupSend1.getPhoneNum());
//        Assert.assertEquals("lskdjf",smsGroupSend1.getContent());
//        Assert.assertEquals(Integer.valueOf(4),smsGroupSend1.getSendObj());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getSendType());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getStatus());
//        Assert.assertEquals(Integer.valueOf(2),smsGroupSend1.getConditionDay());
//    }
//    /**
//     * 新建短信群发，给最近下单用户群发短信
//     */
//    @Test
//    public void processAddSmsGroupSend_6(){
//
//        SmsGroupSend smsGroupSend = buildSmsGroupSend(5, 2,
//                1, null, "134133037645", "lskdjf");
//        smsGroupSendService.addSmsGroupSend(smsGroupSend);
//        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
//        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");
//        Assert.assertEquals("134133037645",smsGroupSend1.getPhoneNum());
//        Assert.assertEquals("lskdjf",smsGroupSend1.getContent());
//        Assert.assertEquals(Integer.valueOf(5),smsGroupSend1.getSendObj());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getSendType());
//        Assert.assertEquals(Integer.valueOf(1),smsGroupSend1.getStatus());
//        Assert.assertEquals(Integer.valueOf(2),smsGroupSend1.getConditionDay());
//    }


    /**
     * 新建短信群发
     * 手机号码发送
     */
    @Test
    public void processUpdateSmsGroupSend_1(){

        processAddSmsGroupSend_1();
        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");

        SmsGroupSend smsGroupSend = buildSmsGroupSend(4, null,
                1, null, "134133037645", "测试");
        smsGroupSend.setId(smsGroupSend1.getId());
        smsGroupSend.setSendType(1);
        logger.debug("smsGroupSend is : {}",smsGroupSend);
        smsGroupSendService.updateSmsGroupSend(smsGroupSend);

        List<SmsGroupSend> smsGroupSends1 = smsGroupSendMapper.selectAll();
        SmsGroupSend smsGroupSend2 = getSmsGroupSend(smsGroupSends1, "134133037645");
        Assert.assertEquals("134133037645",smsGroupSend2.getPhoneNum());
        Assert.assertEquals("测试",smsGroupSend2.getContent());
        Assert.assertEquals(Integer.valueOf(4),smsGroupSend2.getSendObj());
        Assert.assertEquals(Integer.valueOf(1),smsGroupSend2.getSendType());
        Assert.assertEquals(Integer.valueOf(0),smsGroupSend2.getStatus());
    }

    /**
     * 记录不存在
     */
    @Test
    public void processUpdateSmsGroupSend_2(){
        SmsGroupSend smsGroupSend = buildSmsGroupSend(0, null,
                1, null, "134133037645", "测试");
        try {
            smsGroupSendService.updateSmsGroupSend(smsGroupSend);
        }catch (BusinessException e){
            Assert.assertEquals("记录不存在",e.getMessage());
        }

    }

    /**
     * 更新短信群发
     * 立即发送类型不能更新
     */
    @Test
    public void processUpdateSmsGroupSend_3(){
        SmsGroupSend smsGroupSend = buildSmsGroupSend(0, null,
                0, null, "134133037645", "lskdjf");
        smsGroupSendService.addSmsGroupSend(smsGroupSend);
        List<SmsGroupSend> smsGroupSends = smsGroupSendMapper.selectAll();
        SmsGroupSend smsGroupSend1 = getSmsGroupSend(smsGroupSends, "134133037645");
        SmsGroupSend smsGroupSend2 = buildSmsGroupSend(0, null,
                0, null, "134133037645", "lskdjf");
        smsGroupSend2.setId(smsGroupSend1.getId());
        try {
            smsGroupSendService.updateSmsGroupSend(smsGroupSend);
        }catch (BusinessException e){
            Assert.assertEquals("立即发送类型不能修改",e.getMessage());
        }

    }

    /**
     * 更新短信群发
     * 发送中
     */
    @Test
    public void processUpdateSmsGroupSend_4(){
        SmsGroupSend smsGroupSend = buildSmsGroupSend(0, null,
                1, null, "134133037645", "lskdjf");
        smsGroupSend.setStatus(1);
        smsGroupSend.setId(80);
        smsGroupSend.setCreateTime(new Date());
        smsGroupSend.setUpdateTime(new Date());
        smsGroupSend.setSendTime(new Date());
        smsGroupSendMapper.insertSelective(smsGroupSend);

        SmsGroupSend smsGroupSend2 = buildSmsGroupSend(0, null,
                1, null, "134133037645", "lskdjf");
        smsGroupSend2.setId(80);

        try {
            smsGroupSendService.updateSmsGroupSend(smsGroupSend);
        }catch (BusinessException e){
            Assert.assertEquals("发送中",e.getMessage());
        }
    }

    /**
     * 更新短信群发
     * 发送完成
     */
    @Test
    public void processUpdateSmsGroupSend_5(){
        SmsGroupSend smsGroupSend = buildSmsGroupSend(0, null,
                1, null, "134133037645", "lskdjf");
        smsGroupSend.setStatus(2);
        smsGroupSend.setId(80);
        smsGroupSend.setCreateTime(new Date());
        smsGroupSend.setUpdateTime(new Date());
        smsGroupSend.setSendTime(new Date());
        smsGroupSendMapper.insertSelective(smsGroupSend);

        SmsGroupSend smsGroupSend2 = buildSmsGroupSend(0, null,
                1, null, "134133037645", "lskdjf");
        smsGroupSend2.setId(80);

        try {
            smsGroupSendService.updateSmsGroupSend(smsGroupSend);
        }catch (BusinessException e){
            Assert.assertEquals("发送完成",e.getMessage());
        }
    }

    /**
     * 删除短信群发
     * 发送中
     */
    @Test
    public void processDeleteSmsGroupSend_(){
        SmsGroupSend smsGroupSend = buildSmsGroupSend(0, null,
                1, null, "134133037645", "lskdjf");
        smsGroupSend.setStatus(1);
        smsGroupSend.setId(80);
        smsGroupSend.setCreateTime(new Date());
        smsGroupSend.setUpdateTime(new Date());
        smsGroupSend.setSendTime(new Date());
        smsGroupSendMapper.insertSelective(smsGroupSend);

        try {
            smsGroupSendService.deleteSmsGroupSend(80);
        }catch (BusinessException e){
            Assert.assertEquals("发送中",e.getMessage());
        }
    }
}
