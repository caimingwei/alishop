package com.alipay.rebate.shop.service.admin.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.dao.mapper.ProductPageFrameworkMapper;
import com.alipay.rebate.shop.model.ProductPageFramework;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.service.admin.AdminProductService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class AdminProductServiceImplTest {

    @Resource
    ProductPageFrameworkMapper productPageFrameworkMapper;
    @Resource
    AdminProductService adminProductService;

    @Before
    public void before(){
        productPageFrameworkMapper.deletePageByPageName("mm1");
    }

    @After
    public void after(){
        productPageFrameworkMapper.deletePageByPageName("mm1");
    }

    private ProductPageFramework buildProductPageFramework(Long productPageId, Integer pageCatagory,String pageName,
                                                           Integer classifyShow,Integer showType,Integer showRebate,String bannerPicPath,Integer isUse,String dataSource){
        ProductPageFramework productPageFramework = new ProductPageFramework();
        productPageFramework.setPageCatagory(pageCatagory);
        productPageFramework.setBannerPicPath(bannerPicPath);
        productPageFramework.setClassifyShow(classifyShow);
        productPageFramework.setDataSource(dataSource);
        productPageFramework.setIsUse(isUse);
        productPageFramework.setShowType(showType);
        productPageFramework.setPageName(pageName);
        productPageFramework.setShowRebate(showRebate);
        productPageFramework.setUpdateTime(new Date());
        productPageFramework.setCreateTime(new Date());
        if (productPageId!=null) {
            productPageFramework.setProductPageId(productPageId.intValue());
        }
        return productPageFramework;
    }

    private ProductPageFramework getProductPageFramework(List<ProductPageFramework> productPageFrameworks,String pageName){
        for (ProductPageFramework productPageFramework : productPageFrameworks ){
            if (productPageFramework.getPageName().equals(pageName)){
                return productPageFramework;
            }
        }
        return null;
    }

    /**
     * 新建商品页面模板
     */
    @Test
    public void processCreateOrUpdatePage(){

        ProductPageFramework productPageFramework = buildProductPageFramework( null,1, "mm1", 1,
                1, 0, "qwertyuiop", 0, "slkdjfksajg");
        ResponseResult<Long> orUpdatePage = adminProductService.createOrUpdatePage(productPageFramework);
        List<ProductPageFramework> productPageFrameworks = productPageFrameworkMapper.selectAll();
        ProductPageFramework mm1 = getProductPageFramework(productPageFrameworks, "mm1");
        Assert.assertEquals(Integer.valueOf(1),mm1.getPageCatagory());
        Assert.assertEquals(Integer.valueOf(1),mm1.getShowType());
        Assert.assertEquals(Integer.valueOf(1),mm1.getClassifyShow());
        Assert.assertEquals(Integer.valueOf(0),mm1.getShowRebate());
        Assert.assertEquals(Integer.valueOf(0),mm1.getIsUse());
        Assert.assertEquals("qwertyuiop",mm1.getBannerPicPath());
        Assert.assertEquals("slkdjfksajg",mm1.getDataSource());

    }

    /**
     * 更新商品页面模板
     */
    @Test
    public void processCreateOrUpdatePage_1(){
        processCreateOrUpdatePage();
        List<ProductPageFramework> productPageFrameworks = productPageFrameworkMapper.selectAll();
        ProductPageFramework mm1 = getProductPageFramework(productPageFrameworks, "mm1");

        ProductPageFramework productPageFramework = buildProductPageFramework( mm1.getProductPageId().longValue(),0, "mmm", 0,
                1, 0, "asdqwe123456", 0, "asdqwe123456");
        ResponseResult<Long> orUpdatePage = adminProductService.createOrUpdatePage(productPageFramework);
        List<ProductPageFramework> productPageFrameworkList = productPageFrameworkMapper.selectAll();
        ProductPageFramework productPageFramework1 = getProductPageFramework(productPageFrameworkList, "mmm");
        Assert.assertEquals(Integer.valueOf(0),productPageFramework1.getPageCatagory());
        Assert.assertEquals(Integer.valueOf(1),productPageFramework1.getShowType());
        Assert.assertEquals(Integer.valueOf(0),productPageFramework1.getClassifyShow());
        Assert.assertEquals(Integer.valueOf(0),productPageFramework1.getShowRebate());
        Assert.assertEquals(Integer.valueOf(0),productPageFramework1.getIsUse());
        Assert.assertEquals("asdqwe123456",productPageFramework1.getBannerPicPath());
        Assert.assertEquals("asdqwe123456",productPageFramework1.getDataSource());

    }

    /**
     * 删除商品模板信息
     */
    @Test
    public void processDeletePageById(){
        // 准备一条模板数据在数据库
        processCreateOrUpdatePage();
        List<ProductPageFramework> productPageFrameworks = productPageFrameworkMapper.selectAll();
        ProductPageFramework mm1 = getProductPageFramework(productPageFrameworks, "mm1");
        adminProductService.deletePageById(mm1.getProductPageId());
    }

}
