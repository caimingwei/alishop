package com.alipay.rebate.shop.service.customer.impl;

import ch.qos.logback.core.rolling.helper.IntegerTokenConverter;
import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.model.*;
import com.alipay.rebate.shop.pojo.order.FindOrdersReq;
import com.alipay.rebate.shop.pojo.user.customer.OrdersListResponse;
import com.alipay.rebate.shop.service.customer.CustomerOrdersService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import com.github.pagehelper.PageInfo;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerOrdersServiceImplTest {

    private Logger logger = LoggerFactory.getLogger(CustomerOrdersServiceImplTest.class);

    @Resource
    private UserMapper userMapper;
    @Resource
    private OrdersMapper ordersMapper;
    @Resource
    private PddOrdersMapper pddOrdersMapper;
    @Resource
    private UserJdPidMapper userJdPidMapper;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    CommissionFreezeLevelSettingsMapper freezeLevelSettingsMapper;
    @Resource
    private JdOrdersMapper jdOrdersMapper;
    @Resource
    CustomerOrdersService customerOrdersService;
    @Resource
    BrowseProductMapper browseProductMapper;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        userIncomeMapper.deleteUserIncomeByUserId(2L);
        ordersMapper.deleteByPrimaryKey("510202563513111111");
        ordersMapper.deleteByPrimaryKey("510202563513222222");
        ordersMapper.deleteByPrimaryKey("510202563513333333");
        ordersMapper.deleteByPrimaryKey("510202563513444444");
        ordersMapper.deleteByPrimaryKey("9822734982751111111");
        ordersMapper.deleteByPrimaryKey("982273498275222222");
        browseProductMapper.deleteByUserId(1L);

        User user1 = buildUser(1L, "mm1", null, null, "18723649827");
        user1.setTaobaoUserId("983450111111");
        userMapper.insertSelective(user1);
        User user2 = buildUser(2L, "mm1", 1L, "mm1", "1872364983456");
        user2.setTaobaoUserId("983450222222");
        userMapper.insertSelective(user2);
    }
    @After
    public void after(){
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        userIncomeMapper.deleteUserIncomeByUserId(2L);
        ordersMapper.deleteByPrimaryKey("510202563513111111");
        ordersMapper.deleteByPrimaryKey("510202563513222222");
        ordersMapper.deleteByPrimaryKey("510202563513333333");
        ordersMapper.deleteByPrimaryKey("510202563513444444");
        ordersMapper.deleteByPrimaryKey("9822734982751111111");
        ordersMapper.deleteByPrimaryKey("982273498275222222");
        browseProductMapper.deleteByUserId(1L);
    }

    private FindOrdersReq buildFindOrdersReq(String tradeId,Long userId){
        FindOrdersReq req = new FindOrdersReq();
        req.setId(tradeId);
        req.setUserId(userId);
        return req;
    }

    private BrowseProduct buildBrowseProduct(Long userId,Long goodsId){
        BrowseProduct browseProduct = new BrowseProduct();
        Date date = new Date();
        browseProduct.setUserId(userId);
        browseProduct.setGoodsId(goodsId);
        browseProduct.setLastBrowseTime(date);
        browseProduct.setBrowseTimes(2);
        browseProduct.setBrowseDate(date);
        return browseProduct;
    }

    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone) {
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private Orders buildInitialOrders(Long tkStatus,String tradeId,Long userId){
        Orders orders = new Orders();
        orders.setTradeId(tradeId);
        orders.setAdzoneId(109205150467L);
        orders.setAdzoneName(" 渠道ID专属");
        orders.setClickTime("2020-4-27 00:50:0");
        orders.setTkPaidTime("2020-4-27 00:50:10");
        orders.setTbPaidTime("2020-4-27 00:50:10");
        orders.setCreateTime("2020-4-27 00:50:10");
        orders.setTkStatus(tkStatus);
        orders.setRelationId(1L);
        orders.setPubSharePreFee(new BigDecimal("100"));
        orders.setTotalCommissionRate(new BigDecimal("10"));
        orders.setTotalCommissionFee(new BigDecimal("100"));
        orders.setRefundTag(0L);
        orders.setPayPrice(new BigDecimal("1000"));
        orders.setAlipayTotalPrice(new BigDecimal("1000"));
        orders.setItemNum(1L);
        orders.setItemImg("111");
        orders.setItemTitle("111");
        orders.setItemLink("111");
        orders.setOrderType("天猫");
        orders.setSiteId(620000087L);
        orders.setUserId(userId);
        orders.setNumIid(666666L);
        return orders;
    }

    private OrdersListResponse getOrdersListResponse(List<OrdersListResponse> list,String tradeId){
        for (OrdersListResponse response : list) {
            if (response.getTradeId().equals(tradeId)){
                return response;
            }
        }
        return null;
    }

    private UserIncome getUserIncome(List<UserIncome> userIncomeList,Long userId){
        for (UserIncome userIncome : userIncomeList) {
            if (userIncome.getUserId() == userId){
                return userIncome;
            }
        }
        return null;
    }

    /**
     * 查询我的订单
     * 用户没有订单
     */
    @Test
    public void processSelectUserOrdersByType_1(){

        PageInfo<OrdersListResponse> ordersListResponsePageInfo1 = customerOrdersService.selectUserOrdersByType(null,
                1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo1.getTotal());
    }

    /**
     * 查询我的订单
     * 淘宝订单
     */
    @Test
    public void processSelectUserOrdersByType_2(){

        Orders orders1 = buildInitialOrders(12L, "510202563513111111", 1L);
        Orders orders2 = buildInitialOrders(3L, "510202563513222222", 1L);
        Orders orders3 = buildInitialOrders(13L, "510202563513333333", 1L);
        Orders orders4 = buildInitialOrders(14L, "510202563513444444", 1L);
        ordersMapper.insertSelective(orders1);
        ordersMapper.insertSelective(orders2);
        ordersMapper.insertSelective(orders3);
        ordersMapper.insertSelective(orders4);

        // 查询全部订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo1 = customerOrdersService.selectUserOrdersByType(null,
                1, 10, 1L);
        Assert.assertEquals(4,ordersListResponsePageInfo1.getTotal());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo2 = customerOrdersService.selectUserOrdersByType(12L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo2.getTotal());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo3 = customerOrdersService.selectUserOrdersByType(3L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo3.getTotal());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo4 = customerOrdersService.selectUserOrdersByType(13L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo4.getTotal());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo5 = customerOrdersService.selectUserOrdersByType(14L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo5.getTotal());
    }

    /**
     * 查询粉丝订单 -> 没有粉丝订单
     *
     */
    @Test
    public void processSelectFansOrdersByType_1(){
        PageInfo<OrdersListResponse> ordersListResponsePageInfo = customerOrdersService.selectFansOrdersByType(null, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo.getTotal());
    }

    /**
     * 有粉丝订单
     */
    @Test
    public void processSelectFansOrdersByType_2(){
        Orders orders1 = buildInitialOrders(12L, "510202563513111111", 2L);
        Orders orders2 = buildInitialOrders(3L, "510202563513222222", 2L);
        Orders orders3 = buildInitialOrders(13L, "510202563513333333", 2L);
        Orders orders4 = buildInitialOrders(14L, "510202563513444444", 2L);
        ordersMapper.insertSelective(orders1);
        ordersMapper.insertSelective(orders2);
        ordersMapper.insertSelective(orders3);
        ordersMapper.insertSelective(orders4);

        // 查询全部订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo1 = customerOrdersService.selectFansOrdersByType(null,
                1, 10, 1L);
        Assert.assertEquals(4,ordersListResponsePageInfo1.getTotal());


        PageInfo<OrdersListResponse> ordersListResponsePageInfo2 = customerOrdersService.selectFansOrdersByType(12L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo2.getTotal());
        List<OrdersListResponse> list = ordersListResponsePageInfo2.getList();
        OrdersListResponse response1 = getOrdersListResponse(list, "510202563513111111");
        Assert.assertEquals("510202563513111111",response1.getTradeId());

        // 查付款状态订单时，获取其他订单状态的数据出现空指针
        try {
            OrdersListResponse response2 = getOrdersListResponse(list, "510202563513222222");
        }catch (NullPointerException nullPointerException){
        }


        PageInfo<OrdersListResponse> ordersListResponsePageInfo3 = customerOrdersService.selectFansOrdersByType(3L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo3.getTotal());
        List<OrdersListResponse> list2 = ordersListResponsePageInfo3.getList();
        OrdersListResponse response3 = getOrdersListResponse(list2, "510202563513222222");
        Assert.assertEquals("510202563513222222",response3.getTradeId());

        // 获取其他订单状态的数据出现空指针
        try {
            OrdersListResponse response4 = getOrdersListResponse(list, "510202563513111111");
        }catch (NullPointerException nullPointerException){
        }

        PageInfo<OrdersListResponse> ordersListResponsePageInfo4 = customerOrdersService.selectFansOrdersByType(13L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo4.getTotal());
        List<OrdersListResponse> list3 = ordersListResponsePageInfo4.getList();
        OrdersListResponse response5 = getOrdersListResponse(list3, "510202563513333333");
        Assert.assertEquals("510202563513333333",response5.getTradeId());

        // 获取其他订单状态的数据出现空指针
        try {
            OrdersListResponse response6 = getOrdersListResponse(list, "510202563513222222");
        }catch (NullPointerException nullPointerException){
        }


        PageInfo<OrdersListResponse> ordersListResponsePageInfo5 = customerOrdersService.selectFansOrdersByType(14L,
                1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo5.getTotal());
        List<OrdersListResponse> list4 = ordersListResponsePageInfo5.getList();
        OrdersListResponse response7 = getOrdersListResponse(list4, "510202563513444444");
        Assert.assertEquals("510202563513444444",response7.getTradeId());

        // 获取其他订单状态的数据出现空指针
        try {
            OrdersListResponse response8 = getOrdersListResponse(list, "510202563513222222");
        }catch (NullPointerException nullPointerException){
        }
    }

    /**
     * 没有订单
     */
    @Test
    public void processSelectUserOrdersByTypeTwo_1() throws ParseException {

        PageInfo<OrdersListResponse> ordersListResponsePageInfo = customerOrdersService.selectUserOrdersByTypeTwo(null,
                1, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo.getTotal());
    }

    /**
     * 有订单
     */
    @Test
    public void processSelectUserOrdersByTypeTwo_2() throws ParseException {

        Orders orders1 = buildInitialOrders(12L, "510202563513111111", 1L);
        Orders orders2 = buildInitialOrders(3L, "510202563513222222", 1L);
        Orders orders3 = buildInitialOrders(13L, "510202563513333333", 1L);
        Orders orders4 = buildInitialOrders(14L, "510202563513444444", 1L);
        ordersMapper.insertSelective(orders1);
        ordersMapper.insertSelective(orders2);
        ordersMapper.insertSelective(orders3);
        ordersMapper.insertSelective(orders4);

        // 淘宝订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo1 = customerOrdersService.selectUserOrdersByTypeTwo(null,
                null, 1, 10, 1L);
        // 总订单数为4
        Assert.assertEquals(4,ordersListResponsePageInfo1.getTotal());

        // 拼多多订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo2 = customerOrdersService.selectUserOrdersByTypeTwo(null,
                2, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo2.getTotal());

        // 京东订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo3 = customerOrdersService.selectUserOrdersByTypeTwo(null,
                3, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo3.getTotal());

        // 根据订单状态查询
        // 淘宝订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo4 = customerOrdersService.selectUserOrdersByTypeTwo(12L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo4.getTotal());
        List<OrdersListResponse> list1 = ordersListResponsePageInfo4.getList();
        OrdersListResponse resp1 = getOrdersListResponse(list1, "510202563513111111");
        Assert.assertEquals("510202563513111111",resp1.getTradeId());
        Assert.assertEquals(Long.valueOf(1),resp1.getUserId());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo5 = customerOrdersService.selectUserOrdersByTypeTwo(3L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo5.getTotal());
        List<OrdersListResponse> list2 = ordersListResponsePageInfo5.getList();
        OrdersListResponse resp2 = getOrdersListResponse(list2, "510202563513222222");
        Assert.assertEquals("510202563513222222",resp2.getTradeId());
        Assert.assertEquals(Long.valueOf(1),resp2.getUserId());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo6 = customerOrdersService.selectUserOrdersByTypeTwo(13L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo6.getTotal());
        List<OrdersListResponse> list3 = ordersListResponsePageInfo6.getList();
        OrdersListResponse resp3 = getOrdersListResponse(list3, "510202563513333333");
        Assert.assertEquals("510202563513333333",resp3.getTradeId());
        Assert.assertEquals(Long.valueOf(1),resp3.getUserId());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo7 = customerOrdersService.selectUserOrdersByTypeTwo(14L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo7.getTotal());
        List<OrdersListResponse> list4 = ordersListResponsePageInfo7.getList();
        OrdersListResponse resp4 = getOrdersListResponse(list4, "510202563513444444");
        Assert.assertEquals("510202563513444444",resp4.getTradeId());
        Assert.assertEquals(Long.valueOf(1),resp4.getUserId());
    }

    /**
     * 查询粉丝订单 -》 没有
     */
    @Test
    public void processSelectFansOrdersByTypeTwo_1(){
        // 淘宝订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo1 = customerOrdersService.selectFansOrdersByTypeTwo(null,
                null, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo1.getTotal());

        // 拼多多订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo2 = customerOrdersService.selectFansOrdersByTypeTwo(null,
                2, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo2.getTotal());

        // 京东订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo3 = customerOrdersService.selectFansOrdersByTypeTwo(null,
                3, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo3.getTotal());
    }

    /**
     * 查询粉丝订单 -》 存在淘宝粉丝订单
     */
    @Test
    public void processSelectFansOrdersByTypeTwo_2(){

        Orders orders1 = buildInitialOrders(12L, "510202563513111111", 2L);
        Orders orders2 = buildInitialOrders(3L, "510202563513222222", 2L);
        Orders orders3 = buildInitialOrders(13L, "510202563513333333", 2L);
        Orders orders4 = buildInitialOrders(14L, "510202563513444444", 2L);
        ordersMapper.insertSelective(orders1);
        ordersMapper.insertSelective(orders2);
        ordersMapper.insertSelective(orders3);
        ordersMapper.insertSelective(orders4);

        // 淘宝订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo1 = customerOrdersService.selectFansOrdersByTypeTwo(null,
                null, 1, 10, 1L);
        // 总订单数为4
        Assert.assertEquals(4,ordersListResponsePageInfo1.getTotal());

        // 拼多多订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo2 = customerOrdersService.selectFansOrdersByTypeTwo(null,
                2, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo2.getTotal());

        // 京东订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo3 = customerOrdersService.selectFansOrdersByTypeTwo(null,
                3, 1, 10, 1L);
        // 总订单数为0
        Assert.assertEquals(0,ordersListResponsePageInfo3.getTotal());

        // 根据订单状态查询
        // 淘宝订单
        PageInfo<OrdersListResponse> ordersListResponsePageInfo4 = customerOrdersService.selectFansOrdersByTypeTwo(12L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo4.getTotal());
        List<OrdersListResponse> list1 = ordersListResponsePageInfo4.getList();
        OrdersListResponse resp1 = getOrdersListResponse(list1, "510202563513111111");
        Assert.assertEquals("510202563513111111",resp1.getTradeId());
        Assert.assertEquals(Long.valueOf(2),resp1.getUserId());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo5 = customerOrdersService.selectFansOrdersByTypeTwo(3L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo5.getTotal());
        List<OrdersListResponse> list2 = ordersListResponsePageInfo5.getList();
        OrdersListResponse resp2 = getOrdersListResponse(list2, "510202563513222222");
        Assert.assertEquals("510202563513222222",resp2.getTradeId());
        Assert.assertEquals(Long.valueOf(2),resp2.getUserId());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo6 = customerOrdersService.selectFansOrdersByTypeTwo(13L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo6.getTotal());
        List<OrdersListResponse> list3 = ordersListResponsePageInfo6.getList();
        OrdersListResponse resp3 = getOrdersListResponse(list3, "510202563513333333");
        Assert.assertEquals("510202563513333333",resp3.getTradeId());
        Assert.assertEquals(Long.valueOf(2),resp3.getUserId());

        PageInfo<OrdersListResponse> ordersListResponsePageInfo7 = customerOrdersService.selectFansOrdersByTypeTwo(14L,
                null, 1, 10, 1L);
        Assert.assertEquals(1,ordersListResponsePageInfo7.getTotal());
        List<OrdersListResponse> list4 = ordersListResponsePageInfo7.getList();
        OrdersListResponse resp4 = getOrdersListResponse(list4, "510202563513444444");
        Assert.assertEquals("510202563513444444",resp4.getTradeId());
        Assert.assertEquals(Long.valueOf(2),resp4.getUserId());

    }

    /**
     * 找回订单 -- 为用户绑定订单  -- 没有上级
     * 根据淘宝id查询到用户信息
     */
    @Test
    public void processFindOrders_1(){
        Orders orders = buildInitialOrders(3L, "9822734982751111111", 1L);
        orders.setIsRecord(0);
        orders.setTradeParentId("9822734982751111111");
        int i = ordersMapper.insertSelective(orders);
        FindOrdersReq req = buildFindOrdersReq("9822734982751111111", 1L);
        List<Orders> order = customerOrdersService.findOrder(req);

        Orders orders1 = ordersMapper.selectOrdersByTradeId("9822734982751111111");
        logger.debug("ordersList is : {}",orders1);
        Assert.assertEquals(Integer.valueOf(1),orders1.getIsRecord());
        Assert.assertEquals(Long.valueOf(7500),orders1.getUserUit());
        Assert.assertEquals(Long.valueOf(1),orders1.getPromoterId());
        Assert.assertTrue(new BigDecimal("75.00").compareTo(orders1.getUserCommission()) == 0);
        Assert.assertEquals(null,orders1.getTwoLevelUserUit());
        Assert.assertEquals(null,orders1.getOneLevelUserUit());

        List<UserIncome> userIncomes = userIncomeMapper.selectAll();
        UserIncome userIncome = getUserIncome(userIncomes, 1L);
        logger.debug("userIncome is : {}",userIncome);
        Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getType());
        Assert.assertTrue(new BigDecimal("75.00").compareTo(userIncome.getMoney()) == 0);
    }

    /**
     * 找回订单 -- 为用户绑定订单  -- 有上级
     * 根据淘宝id查询到用户信息
     */
    @Test
    public void processFindOrders_2(){
        User user = userMapper.selectById(2L);
        logger.debug("user is : {}",user);
        Orders orders = buildInitialOrders(3L, "982273498275222222", 2L);
        orders.setIsRecord(0);
        orders.setTradeParentId("982273498275222222");
        int i = ordersMapper.insertSelective(orders);
        FindOrdersReq req = buildFindOrdersReq("982273498275222222", 2L);
        List<Orders> order = customerOrdersService.findOrder(req);

        Orders orders1 = ordersMapper.selectOrdersByTradeId("982273498275222222");
        logger.debug("ordersList is : {}",orders1);
        Assert.assertEquals(Integer.valueOf(1),orders1.getIsRecord());
        Assert.assertEquals(Long.valueOf(7500),orders1.getUserUit());
        Assert.assertEquals(Long.valueOf(2),orders1.getPromoterId());
        Assert.assertTrue(new BigDecimal("75.00").compareTo(orders1.getUserCommission()) == 0);
        Assert.assertEquals(null,orders1.getTwoLevelUserUit());
        Assert.assertEquals(Long.valueOf(400),orders1.getOneLevelUserUit());

        List<UserIncome> userIncomes = userIncomeMapper.selectAll();
        UserIncome userIncome = getUserIncome(userIncomes, 2L);
        logger.debug("userIncome is : {}",userIncome);
        Assert.assertEquals(Long.valueOf(2),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getType());
        Assert.assertTrue(new BigDecimal("75.00").compareTo(userIncome.getMoney()) == 0);

        UserIncome userIncome1 = getUserIncome(userIncomes, 1L);
        logger.debug("userIncome is : {}",userIncome1);
        Assert.assertEquals(Long.valueOf(1),userIncome1.getUserId());
        Assert.assertEquals(Integer.valueOf(2),userIncome1.getAwardType());
        Assert.assertEquals(Integer.valueOf(1),userIncome1.getType());
        Assert.assertTrue(new BigDecimal("4.00").compareTo(userIncome1.getMoney()) == 0);

        User user1 = userMapper.selectById(2L);
        Assert.assertEquals(Long.valueOf(7500),user1.getUserIntgeralTreasure());
        User user2 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(400),user2.getUserIntgeralTreasure());
    }

    /**
     * 找回订单 -- 没有上级，
     * 没有通过淘宝id查询到用户信息 -- 通过商品浏览记录
     */
    @Test
    public void processFindOrders_3(){

        BrowseProduct browseProduct = buildBrowseProduct(1L, 666666L);
        browseProductMapper.insertSelective(browseProduct);
        Orders orders = buildInitialOrders(3L, "982273498275333333", 1L);
        orders.setIsRecord(0);
        orders.setTradeParentId("982273498275333333");
        int i = ordersMapper.insertSelective(orders);
        FindOrdersReq req = buildFindOrdersReq("982273498275333333", 1L);
        List<Orders> order = customerOrdersService.findOrder(req);

        Orders orders1 = ordersMapper.selectOrdersByTradeId("982273498275333333");
        logger.debug("ordersList is : {}",orders1);
        Assert.assertEquals(Integer.valueOf(1),orders1.getIsRecord());
        Assert.assertEquals(Long.valueOf(7500),orders1.getUserUit());
        Assert.assertEquals(Long.valueOf(1),orders1.getPromoterId());
        Assert.assertTrue(new BigDecimal("75.00").compareTo(orders1.getUserCommission()) == 0);
        Assert.assertEquals(null,orders1.getTwoLevelUserUit());
        Assert.assertEquals(null,orders1.getOneLevelUserUit());

        List<UserIncome> userIncomes = userIncomeMapper.selectAll();
        UserIncome userIncome = getUserIncome(userIncomes, 1L);
        logger.debug("userIncome is : {}",userIncome);
        Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getType());
        Assert.assertTrue(new BigDecimal("75.00").compareTo(userIncome.getMoney()) == 0);
    }
}