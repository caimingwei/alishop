package com.alipay.rebate.shop.service.admin.impl;

import static org.junit.Assert.*;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.JpushSettingsConstant;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.constants.WithDrawConstant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.model.*;
import com.alipay.rebate.shop.pojo.alipay.TransferReq;
import com.alipay.rebate.shop.service.admin.AdminWithdrawService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import com.alipay.rebate.shop.utils.AliUtil;
import com.alipay.rebate.shop.utils.DecimalUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class AlipayOrderServiceImplTest {

  @Resource
  private UserMapper userMapper;
  @Resource
  private UserIncomeMapper userIncomeMapper;
  @Resource
  private WithDrawMapper withDrawMapper;
  @Resource
  private CouponGetRecordMapper couponGetRecordMapper;
  @Resource
  private AlipayOrderServiceImpl alipayOrderService;
  @Resource
  private AdminWithdrawServiceImpl adminWithdrawService;
  @Resource
  private JpushSettingsMapper jpushSettingsMapper;

  @Before
  public void before() {

    userMapper.deleteUserById(1L);
    withDrawMapper.deleteByPrimaryKey(1L);
    withDrawMapper.deleteByPrimaryKey(2L);
    withDrawMapper.deleteByPrimaryKey(3L);
    userIncomeMapper.deleteUserIncomeByUserId(1L);
    couponGetRecordMapper.deleteByPrimaryKey(1L);

  }

  private User buildUser(
      Long userId,
      Long relationId , Long specialId,
      Long parentUserId, String phone,
      String taobaoUserId
  ){
    User user = new User();
    user.setUserId(userId);
    user.setUserNickName("mmm1");
    user.setUserStatus(UserContant.USER_STATUS_NORMAL);
    user.setIsAuth(UserContant.IS_AUTH);
    user.setRelationId(relationId);
    user.setSpecialId(specialId);
    user.setParentUserId(parentUserId);
    user.setTotalFansOrdersNum(0);
    user.setUserIntgeralTreasure(0L);
    user.setPhone(phone);
    user.setOrdersNum(0L);
    user.setUserGradeId(1);
    user.setUserIntgeral(0L);
    user.setUserAmount(new BigDecimal("0.00"));
    user.setUserIncome(new BigDecimal("0.00"));
    user.setUserCommision(new BigDecimal("0.00"));
    user.setTaobaoUserId(taobaoUserId);
    user.setCreateTime(new Date());
    user.setUpdateTime(new Date());
    return user;
  }

  private WithDraw buildWithDraw(){
    WithDraw withDraw = new WithDraw();
    withDraw.setIsFirst(0);
    withDraw.setCommitTime(new Date());
    withDraw.setApplyTime(new Date());
    withDraw.setType(WithDrawConstant.WITHDRAW_UIT_TYPE);
    withDraw.setAccount("897459376");
    withDraw.setUserId(1L);
    withDraw.setMoney(new BigDecimal(40));
    withDraw.setWithdrawId(1L);
    withDraw.setPhone("18819258225");
    withDraw.setRealName("xxx");
    withDraw.setStatus(WithDrawConstant.WITHDRAW_ONGOING);
    withDraw.setUserName("mmm1");
    return withDraw;
  }

  private CouponGetRecord buildCouponGetRecord(){
    CouponGetRecord couponGetRecord = new CouponGetRecord();
    couponGetRecord.setIsUse(0);
    couponGetRecord.setCreateTime(new Date());
    couponGetRecord.setUpdateTime(new Date());
    couponGetRecord.setId(1L);
    couponGetRecord.setType(1);
    couponGetRecord.setUserId(1L);
    couponGetRecord.setCouponId(12);
    return couponGetRecord;
  }

  /**
   * 测试此方法需要注意，在develop分支进行测试，不能在master分支进行测试
   * 测试的时候把 transfer方法中 if(response.getCode().equals("10000")) 这个判断去掉
   * 在develop分支转账是不可能成功的
   * 这个方法目前只用来测试红包卷开启功能，不测试转账功能
   * 由于powermock集成spring功能略复杂，以后再加
   *
   * 连续三次直接转账 40 元，总计120元，最后一次会开启红包卷
   */
  @Test
  public void transfer(){
    // 插入用户1
    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
    userMapper.insertSelective(user1);

    // 红包卷
    CouponGetRecord couponGetRecord = buildCouponGetRecord();
    couponGetRecordMapper.insertSelective(couponGetRecord);

    // 提现40元
    WithDraw withDraw1 = buildWithDraw();
    withDraw1.setWithdrawId(1L);
    withDrawMapper.insertWithDrawSelective(withDraw1);

    // 提现40元
    WithDraw withDraw2 = buildWithDraw();
    withDraw2.setWithdrawId(2L);
    withDrawMapper.insertWithDrawSelective(withDraw2);

    // 提现40元
    WithDraw withDraw3 = buildWithDraw();
    withDraw3.setWithdrawId(3L);
    withDrawMapper.insertWithDrawSelective(withDraw3);

    // 转账
    TransferReq transferReq = new TransferReq();
    AlipayOrder alipayOrder = new AlipayOrder();
    alipayOrder.setWithdrawId(1L);
    List<AlipayOrder> list = new ArrayList<>();
    list.add(alipayOrder);
    transferReq.setAlipayOrders(list);

    alipayOrderService.transfer(transferReq);

    alipayOrder = new AlipayOrder();
    alipayOrder.setWithdrawId(2L);
    list = new ArrayList<>();
    list.add(alipayOrder);
    transferReq.setAlipayOrders(list);

    alipayOrderService.transfer(transferReq);

    alipayOrder = new AlipayOrder();
    alipayOrder.setWithdrawId(3L);
    list = new ArrayList<>();
    list.add(alipayOrder);
    transferReq.setAlipayOrders(list);

    alipayOrderService.transfer(transferReq);

    couponGetRecord = couponGetRecordMapper.selectByPrimaryKey(1L);
    Assert.assertEquals(couponGetRecord.getIsUse(),Integer.valueOf(1));
    System.out.println(user1.getUserAmount());
    user1 = userMapper.selectById(1L);
    Assert.assertTrue(user1.getUserAmount().compareTo(new BigDecimal("10")) == 0);

  }

  /**
   * 测试此方法需要注意，在develop分支进行测试，不能在master分支进行测试
   * 测试的时候把 transfer方法中 if(response.getCode().equals("10000")) 这个判断去掉
   * 在develop分支转账是不可能成功的
   * 这个方法目前只用来测试红包卷开启功能，不测试转账功能
   * 由于powermock集成spring功能略复杂，以后再加
   *
   * 连续三次把提现状态修改成功共也能促发红包
   */
  @Test
  public void transfer1(){
    // 插入用户1
    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
    userMapper.insertSelective(user1);

    // 红包卷
    CouponGetRecord couponGetRecord = buildCouponGetRecord();
    couponGetRecordMapper.insertSelective(couponGetRecord);

    // 提现40元
    WithDraw withDraw1 = buildWithDraw();
    withDraw1.setWithdrawId(1L);
    withDrawMapper.insertWithDrawSelective(withDraw1);

    // 提现40元
    WithDraw withDraw2 = buildWithDraw();
    withDraw2.setWithdrawId(2L);
    withDrawMapper.insertWithDrawSelective(withDraw2);

    // 提现40元
    WithDraw withDraw3 = buildWithDraw();
    withDraw3.setWithdrawId(3L);
    withDrawMapper.insertWithDrawSelective(withDraw3);

    WithDraw withDraw = new WithDraw();
    withDraw.setWithdrawId(1L);
    withDraw.setStatus(WithDrawConstant.WITHDRAW_SUCCESS);
    adminWithdrawService.updateWithDrawRecord(withDraw);
    withDraw.setWithdrawId(2L);
    adminWithdrawService.updateWithDrawRecord(withDraw);
    withDraw.setWithdrawId(3L);
    adminWithdrawService.updateWithDrawRecord(withDraw);

    couponGetRecord = couponGetRecordMapper.selectByPrimaryKey(1L);
    Assert.assertEquals(couponGetRecord.getIsUse(),Integer.valueOf(1));
    System.out.println(user1.getUserAmount());
    user1 = userMapper.selectById(1L);
    Assert.assertTrue(user1.getUserAmount().compareTo(new BigDecimal("10")) == 0);

  }

  /**
   * 测试此方法需要注意，在develop分支进行测试，不能在master分支进行测试
   * 测试的时候把 transfer方法中 if(response.getCode().equals("10000")) 这个判断去掉
   * 在develop分支转账是不可能成功的
   * 这个方法目前只用来测试红包卷开启功能，不测试转账功能
   * 由于powermock集成spring功能略复杂，以后再加
   *
   * 连续三次把提现状态修改成功,一次转账也可以促发红包
   */
  @Test
  public void transfer2(){
    // 插入用户1
    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
    userMapper.insertSelective(user1);

    // 红包卷
    CouponGetRecord couponGetRecord = buildCouponGetRecord();
    couponGetRecordMapper.insertSelective(couponGetRecord);

    // 提现40元
    WithDraw withDraw1 = buildWithDraw();
    withDraw1.setWithdrawId(1L);
    withDrawMapper.insertWithDrawSelective(withDraw1);

    // 提现40元
    WithDraw withDraw2 = buildWithDraw();
    withDraw2.setWithdrawId(2L);
    withDrawMapper.insertWithDrawSelective(withDraw2);

    // 提现40元
    WithDraw withDraw3 = buildWithDraw();
    withDraw3.setWithdrawId(3L);
    withDrawMapper.insertWithDrawSelective(withDraw3);

    WithDraw withDraw = new WithDraw();
    withDraw.setWithdrawId(1L);
    withDraw.setStatus(WithDrawConstant.WITHDRAW_SUCCESS);
    adminWithdrawService.updateWithDrawRecord(withDraw);
    withDraw.setWithdrawId(2L);
    adminWithdrawService.updateWithDrawRecord(withDraw);

    // 转账
    TransferReq transferReq = new TransferReq();
    AlipayOrder alipayOrder = new AlipayOrder();
    alipayOrder.setWithdrawId(3L);
    List<AlipayOrder> list = new ArrayList<>();
    list.add(alipayOrder);
    transferReq.setAlipayOrders(list);

    alipayOrderService.transfer(transferReq);


    couponGetRecord = couponGetRecordMapper.selectByPrimaryKey(1L);
    Assert.assertEquals(couponGetRecord.getIsUse(),Integer.valueOf(1));
    System.out.println(user1.getUserAmount());
    user1 = userMapper.selectById(1L);
    Assert.assertTrue(user1.getUserAmount().compareTo(new BigDecimal("10")) == 0);

  }

  @Test
  public void test(){
    List<WithDraw> withDraws = withDrawMapper.selectNoHandle();

    String reason = "系统维护中，提现驳回";
    for (WithDraw withDraw : withDraws) {
      withDrawMapper.updateOrderStatus(withDraw.getWithdrawId());
      if(withDraw.getType().equals(WithDrawConstant.WITHDRAW_AMOUNT_TYPE)){
        userMapper.plusUserAM(withDraw.getMoney(),withDraw.getUserId());
      }
      if(withDraw.getType().equals(WithDrawConstant.WITHDRAW_UIT_TYPE)){
        userMapper.plusUserUit(DecimalUtil.convertMoneyToUit(withDraw.getMoney()),
                withDraw.getUserId());
      }
      JpushSettings jpushSettings = jpushSettingsMapper.selectByType(JpushSettingsConstant.WITH_FAILED);
      AliUtil.sendWithDrawSms(withDraw.getPhone(),withDraw.getUserName(),reason,
              jpushSettings.getSmsSignName(),
              jpushSettings.getSmsTemplateCode());

      updateUserIncome(withDraw.getUserId());
    }

  }
  private void updateUserIncome(Long userId) {
    //查询用户收益表是否存在收益记录
    UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(userId, UserIncomeConstant.WITHDRAW_EXAMINE_TYPE);
    //存在则更新用户收益记录
    if (userIncome != null) {
      userIncomeMapper.updateUserIncome(userId, UserIncomeConstant.WITHDRAW_EXAMINE_TYPE);
    }
  }
}