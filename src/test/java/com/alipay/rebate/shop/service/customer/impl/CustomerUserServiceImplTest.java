package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.MobileCodeMapper;
import com.alipay.rebate.shop.constants.RedisConstant;
import com.alipay.rebate.shop.constants.TaskSettingsConstant;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.exceptoin.MobileCodeNotRightException;
import com.alipay.rebate.shop.exceptoin.PhoneAlreadyRegistedException;
import com.alipay.rebate.shop.exceptoin.PhoneNotExistsException;
import com.alipay.rebate.shop.helper.MobileCodeInformationHelper;
import com.alipay.rebate.shop.helper.TaskDoRecordBuilder;
import com.alipay.rebate.shop.model.*;
import com.alipay.rebate.shop.pojo.common.ResponseResult;
import com.alipay.rebate.shop.pojo.user.customer.*;
import com.alipay.rebate.shop.service.customer.CustomerUserService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.apache.shiro.SecurityUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RestController;
import sun.rmi.runtime.Log;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerUserServiceImplTest {

    @Resource
    CustomerUserService customerUserService;
    @Resource
    UserMapper userMapper;
    @Resource
    CouponGetRecordMapper couponGetRecordMapper;
    @Resource
    HttpServletRequest request;
    @Resource
    TaskSettingsMapper taskSettingsMapper;
    @Resource
    HttpServletResponse response;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    TaskDoRecordMapper taskDoRecordMapper;
    @Resource
    ActivityProductMapper activityProductMapper;
    @Resource
    ActivityBrowseProductMapper activityBrowseProductMapper;
    @Resource
    StringRedisTemplate stringRedisTemplate;

    Logger logger = LoggerFactory.getLogger(CustomerUserServiceImplTest.class);

    @Before
    public void before() {
        userMapper.deleteUserByPhone("15673908888");
        userMapper.deleteUserByPhone("15673909999");
        userMapper.deleteUserByPhone("15673900000");
        userMapper.deleteUserByPhone("15673901111");
        userMapper.deleteUserByPhone("15673902222");
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        taskDoRecordMapper.deleteTaskDoRecordByUserId(1L);
        activityBrowseProductMapper.deleteByUserId(1L);
        activityProductMapper.deleteByPrimaryKey(888);

    }

    @After
    public void after() {
        userMapper.deleteUserByPhone("15673908888");
        userMapper.deleteUserByPhone("15673909999");
        userMapper.deleteUserByPhone("15673900000");
        userMapper.deleteUserByPhone("15673901111");
        userMapper.deleteUserByPhone("15673902222");
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        taskDoRecordMapper.deleteTaskDoRecordByUserId(1L);
        activityBrowseProductMapper.deleteByUserId(1L);
        activityProductMapper.deleteByPrimaryKey(888);
    }

    private UserRequest buildUserRequest(String phone, String mobileCode, String password, Long parentUserId,
                                         String headImageUrl, String token, String userNickName,
                                         String openId, String parentUserName,String alipayAccount,String realName) {
        UserRequest userRequest = new UserRequest();
        userRequest.setUserPhone(phone);
        userRequest.setMobileCode(mobileCode);
        userRequest.setPassword(password);
        userRequest.setHeadImageUrl(headImageUrl);
        userRequest.setOpenId(openId);
        userRequest.setParentUserId(parentUserId);
        userRequest.setToken(token);
        userRequest.setRealName(realName);
        userRequest.setUserNickName(userNickName);
        userRequest.setParentUserPhone(parentUserName);
        userRequest.setAlipayAccount(alipayAccount);
        return userRequest;
    }

    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone) {
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private UserDto buildUserDto(String account, String openId, String password, String currentRegistrationId, Integer loginPlatform) {
        UserDto userDto = new UserDto();
        userDto.setAccount(account);
        userDto.setOpenId(openId);
        char[] pass = password.toCharArray();
        userDto.setPassword(pass);
        userDto.setCurrentRegistrationId(currentRegistrationId);
        userDto.setLoginPlatform(loginPlatform);
        return null;
    }

    private ActivityProduct buildActivityProduct(Integer id, String goodsId, Integer activityId) {
        ActivityProduct activityProduct = new ActivityProduct();
        activityProduct.setId(id);
        activityProduct.setActivityId(activityId);
        activityProduct.setProductId(goodsId);
        activityProduct.setShortProductUrl("https://detail.tmall.com/item.htm?id=" + goodsId);
        activityProduct.setShortTitle("新用户0元购芦荟胶100g");
        activityProduct.setPicUrl("www.baidu.com");
        activityProduct.setVoucherUrl("https://uland.taobao.com/coupon/edetail?e=TqC4b23ZYIkSq1SNqCiMajJ5c1Of7J2HYHCuomB9NOfDOnaW0ID6pq8DWykFTzaVh2mtO2Msln2dwVCEmPgFhIvQBacu2NEx%2BGoSNEAjokQ4M2%2FUXT47GFZrtTOLgXkcjDppvlX%2Bob%2BBtHA6T8TZnQSD1EQRCW%2BLzEFJTNph2qt7ov5%2F0O2eVTDOaNhNJ6ONPOnbN1rJJNM%3D&traceId=0b01ddd715686261550806539e&union_lens=lensId:0b0b6072_0bbf_16d39679e61_dbb9&xId=xViicNkUOF8jGNZwoNCzZgjR94cCM72IjwhKASvPfk5gYMXsQhiBkznezPs10u8SyPakwXo5T6KRkHayUpPm9o");
        activityProduct.setRefundsPrice(new BigDecimal("9.98"));
        activityProduct.setAfterPrice(new BigDecimal("9.98"));
        activityProduct.setCommissionRatio(80);
        activityProduct.setLimitNum(100);
        activityProduct.setWeight(1000);
        activityProduct.setVirtualSaleNum(500);
        activityProduct.setQualType(1);
        activityProduct.setQualJson(null);
        activityProduct.setEndTime(new Date());
        return activityProduct;
    }

    private PasswordRequest buildPasswordRequest(String phone, String password) {
        PasswordRequest passwordRequest = new PasswordRequest();
        passwordRequest.setMobileCode("872634");
        passwordRequest.setPassword(password);
        passwordRequest.setUserPhone(phone);
        return passwordRequest;
    }

    private TaskDoRecord getTaskDoRecord(List<TaskDoRecord> taskDoRecords,Integer subType){
        for (TaskDoRecord taskDoRecord : taskDoRecords) {
            if (taskDoRecord.getSubType() == subType){
                return taskDoRecord;
            }
        }
        return null;
    }


    /**
     * 手机注册
     */
    @Test
    public void processPhoneRegister_1(){

        UserRequest userRequest = buildUserRequest("15673908888", "893657", "123456",
                null, null, null, "测试1",
                null,null,null,null);
        customerUserService.phoneRegister(userRequest,request);
        User user2 = userMapper.getUserByPhone("15673908888");
        logger.debug("user2 is : {}",user2);
        Assert.assertEquals("15673908888",user2.getPhone());
        Assert.assertEquals("测试1",user2.getUserNickName());
        Assert.assertEquals(EncryptUtil.encryptPassword("123456"),user2.getUserPassword());
        List<UserCouponRsp> userCouponRsps = couponGetRecordMapper.selectCouponDetailByUserId(user2.getUserId());
        Assert.assertTrue(userCouponRsps.size() > 0);
        couponGetRecordMapper.deleteCouponDetailByUserId(user2.getUserId());
    }

    /**
     * 微信注册
     */
    @Test
    public void processWeiXinRegister_1(){
        UserRequest userRequest = buildUserRequest("15673909999", "893657", "123456",
                null, null, null,
                "测试2", "oGqLj6XUWhFeDz2NoWia9n47c3D4",null,null,null);
        customerUserService.weixinRegister(userRequest,request);
        User user = userMapper.getUserByPhone("15673909999");
        logger.debug("user is : {}",user);
        Assert.assertEquals("15673909999",user.getPhone());
        Assert.assertEquals("测试2",user.getUserNickName());
        Assert.assertEquals(EncryptUtil.encryptPassword("123456"),user.getUserPassword());
        Assert.assertEquals("oGqLj6XUWhFeDz2NoWia9n47c3D4",user.getOpenId());
        List<UserCouponRsp> userCouponRsps = couponGetRecordMapper.selectCouponDetailByUserId(user.getUserId());
        Assert.assertTrue(userCouponRsps.size() > 0);
        couponGetRecordMapper.deleteCouponDetailByUserId(user.getUserId());

        // 校验微信授权奖励记录
        List<TaskDoRecord> taskDoRecords = taskDoRecordMapper.selectTaskDoRecordByUserId(user.getUserId());
        TaskDoRecord taskDoRecord = getTaskDoRecord(taskDoRecords, 12);
        Assert.assertEquals(Integer.valueOf(12),taskDoRecord.getSubType());
        Assert.assertEquals(Integer.valueOf(1),taskDoRecord.getType());
    }

    /**
     * h5注册 -》 有上级
     */
    @Test
    public void processH5Register_1(){

        User user1 = buildUser(1L, "xiao", null, null, "15673900000");
        userMapper.insertSelective(user1);
        UserRequest userRequest = buildUserRequest("15673901111", "893657", "123456",
                1L, null, null,
                "测试3", null,"xiao",null,null);
        customerUserService.h5Register(userRequest,request);
        User user = userMapper.getUserByPhone("15673901111");
        logger.debug("user is : {}",user);
        Assert.assertEquals("15673901111",user.getPhone());
        Assert.assertEquals("测试3",user.getUserNickName());
        Assert.assertEquals(Long.valueOf(1),user.getParentUserId());
        Assert.assertEquals(EncryptUtil.encryptPassword("123456"),user.getUserPassword());
        Assert.assertEquals(Long.valueOf(1),user.getParentUserId());
        List<UserCouponRsp> userCouponRsps = couponGetRecordMapper.selectCouponDetailByUserId(user.getUserId());
        Assert.assertTrue(userCouponRsps.size() > 0);
        couponGetRecordMapper.deleteCouponDetailByUserId(user.getUserId());

        // 校验上级粉丝数是否加1
        User user2 = userMapper.selectById(1L);
        Assert.assertEquals(Integer.valueOf(1),user2.getTotalFansNum());

    }

    /**
     * h5注册 -》 有上上级
     */
    @Test
    public void processH5Register_2(){

        User user4 = buildUser(5L, "123", null, null, "15673902222");
        userMapper.insertSelective(user4);
        User user1 = buildUser(1L, "xiao", 5L, "123", "15673900000");
        userMapper.insertSelective(user1);
        UserRequest userRequest = buildUserRequest("15673901111", "893657", "123456",
                1L, null, null, "测试4", null,"xiao",null,null);
        customerUserService.h5Register(userRequest,request);
        User user = userMapper.getUserByPhone("15673901111");
        logger.debug("user is : {}",user);
        Assert.assertEquals("15673901111",user.getPhone());
        Assert.assertEquals("测试4",user.getUserNickName());
        Assert.assertEquals(Long.valueOf(1),user.getParentUserId());
        Assert.assertEquals(EncryptUtil.encryptPassword("123456"),user.getUserPassword());
        Assert.assertEquals(Long.valueOf(1),user.getParentUserId());
        List<UserCouponRsp> userCouponRsps = couponGetRecordMapper.selectCouponDetailByUserId(user.getUserId());
        Assert.assertTrue(userCouponRsps.size() > 0);
        couponGetRecordMapper.deleteCouponDetailByUserId(user.getUserId());

        // 校验上级粉丝数是否加1
        User user2 = userMapper.selectById(1L);
        Assert.assertEquals(Integer.valueOf(1),user2.getTotalFansNum());
        User user3 = userMapper.selectById(5L);
        Assert.assertEquals(Integer.valueOf(1),user3.getTotalFansNum());
    }

    /**
     * 手机号已注册
     */
    @Test
    public void processPhoneRegister_2(){

        User user1 = buildUser(1L, "xiao", null, null, "15673900000");
        userMapper.insertSelective(user1);
        UserRequest userRequest = buildUserRequest("15673900000", "893657", "123456",
                null, null, null,
                "mmm1", null,null,null,null);
        try {
            customerUserService.phoneRegister(userRequest, request);
        }catch (PhoneAlreadyRegistedException exception){
            Assert.assertEquals("手机号已经注册",exception.getMessage());
        }
    }

    @Test
    public void processWeiXinRegister_2(){
        User user1 = buildUser(1L, "xiao", null, null, "15673900000");
        userMapper.insertSelective(user1);
        UserRequest userRequest = buildUserRequest("15673900000", "893657", "123456",
                null, null, null,
                "mmm2", "oGqLj6XUWhFeDz2NoWia9n47c3D4",null,null,null);
        customerUserService.weixinRegister(userRequest,request);

    }

    @Test
    public void processH5Register_3(){
        User user1 = buildUser(6L, "123", null, null, "15673901111");
        userMapper.insertSelective(user1);
        User user4 = buildUser(5L, "123", null, null, "15673902222");
        userMapper.insertSelective(user4);
        User user2 = buildUser(1L, "xiao", 5L, "123", "15673900000");
        userMapper.insertSelective(user2);
        UserRequest userRequest = buildUserRequest("15673901111", "893657", "123456",
                1L, null, null, "测试4", null,"xiao",null,null);
        try {
            customerUserService.h5Register(userRequest,request);
        }catch (PhoneAlreadyRegistedException exception){
            logger.debug("exception is : {}",exception.getMessage());
            Assert.assertEquals("手机号已经注册",exception.getMessage());
        }
    }

    /**
     * 微信授权 -- 成功
     */
    @Test
    public void processWeiXinAuth_1(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);
        customerUserService.weixinAuth(1L,"oGqLj6XUWhFeDz2NoWia9n11111");
        User user1 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user1.getUserId());
        Assert.assertEquals("oGqLj6XUWhFeDz2NoWia9n11111",user1.getOpenId());

        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 7);
        logger.debug("userIncome is : {}",userIncome );

        // 校验微信授权奖励记录
        List<TaskDoRecord> taskDoRecords = taskDoRecordMapper.selectTaskDoRecordByUserId(1L);
        TaskDoRecord taskDoRecord = getTaskDoRecord(taskDoRecords, 12);
        Assert.assertEquals(Integer.valueOf(12),taskDoRecord.getSubType());
        Assert.assertEquals(Integer.valueOf(1),taskDoRecord.getType());

    }

    /**
     * 微信重复授权
     */
    @Test
    public void processWeiXinAuth_2(){
        User user1 = buildUser(1L, "mm1", null, null, "18973209999");
        user1.setOpenId("oGqLj6XUWhFeDz2NoWia9n11111");
        userMapper.insertSelective(user1);

        User user2 = buildUser(2L, "mm1", null, null, "18973208888");
        userMapper.insertSelective(user2);
        try {
            customerUserService.weixinAuth(2L, "oGqLj6XUWhFeDz2NoWia9n11111");
        }catch (BusinessException e){
            Assert.assertEquals("微信重复授权",e.getMessage());
        }
    }

    /**
     * 微信已在当前用户下授权
     */
    @Test
    public void processWeiXinAuth_3(){
        User user1 = buildUser(1L, "mm1", null, null, "18973209999");
        user1.setOpenId("oGqLj6XUWhFeDz2NoWia9n11111");
        userMapper.insertSelective(user1);
        customerUserService.weixinAuth(1L,"oGqLj6XUWhFeDz2NoWia9n11111");
        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals("oGqLj6XUWhFeDz2NoWia9n11111",user.getOpenId());
    }

    /**
     * 检查用户id是否和数据库相同
     */
    @Test
    public void processCheckRelationId_1(){
        User user1 = buildUser(1L, "mm1", null, null, "18973209999");
        user1.setRelationId(675762354L);
        userMapper.insertSelective(user1);
        boolean b = customerUserService.checkRelationId(1L, 675762354L);
        Assert.assertEquals(true,b);
        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals(Long.valueOf(675762354L),user.getRelationId());
    }

    /**
     * 检查用户id是否和数据库相同
     * 用户不存在
     */
    @Test
    public void processCheckRelationId_2(){
        try {
            customerUserService.checkRelationId(1L, 675762354L);
        }catch (BusinessException e){
            Assert.assertEquals("用户不存在",e.getMessage());
        }
    }

    /**
     * 计时奖励
     */
    @Test
    public void processRewardUser_1(){
        User user1 = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user1);
        customerUserService.rewardUser(1L);
        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals(Long.valueOf(5),user.getUserIntgeral());

        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 2);
        Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getType());
        Assert.assertEquals(Long.valueOf(5),userIncome.getIntgeralNum());

        List<TaskDoRecord> taskDoRecords = taskDoRecordMapper.selectTaskDoRecordByUserId(1L);
        Assert.assertEquals(1,taskDoRecords.size());
        for (TaskDoRecord taskDoRecord : taskDoRecords) {
            Assert.assertEquals(Long.valueOf(1L), taskDoRecord.getUserId());
            Assert.assertEquals(Integer.valueOf(2), taskDoRecord.getType());
            Assert.assertEquals(Integer.valueOf(21), taskDoRecord.getSubType());
        }

    }

    /**
     * 计时奖励
     */
    @Test
    public void processRewardUser_2(){
        // 第一次计时浏览
        processRewardUser_1();

        // 第二次计时浏览
        customerUserService.rewardUser(1L);
        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals(Long.valueOf(10),user.getUserIntgeral());

        List<UserIncome> userIncomeByUserId = userIncomeMapper.getUserIncomeByUserId(1L);
        Assert.assertEquals(2,userIncomeByUserId.size());
        for (UserIncome userIncome :userIncomeByUserId){
            Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
            Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
            Assert.assertEquals(Integer.valueOf(2),userIncome.getType());
            Assert.assertEquals(Long.valueOf(5),userIncome.getIntgeralNum());
        }

        List<TaskDoRecord> taskDoRecords = taskDoRecordMapper.selectTaskDoRecordByUserId(1L);
        Assert.assertEquals(2,taskDoRecords.size());
        for (TaskDoRecord taskDoRecord : taskDoRecords) {
            Assert.assertEquals(Long.valueOf(1L), taskDoRecord.getUserId());
            Assert.assertEquals(Integer.valueOf(2), taskDoRecord.getType());
            Assert.assertEquals(Integer.valueOf(21), taskDoRecord.getSubType());
        }
    }

    /**
     * 当天计时浏览任务已达到最大次数
     */
    @Test
    public void processRewardUser_3(){
        processRewardUser_2();

        try {
            customerUserService.rewardUser(1L);
        }catch (BusinessException e){
            Assert.assertEquals("今天阅读奖励次数达上限制",e.getMessage());
        }
    }

    /**
     * 找回密码  -- 需要连接redis
     */
    @Test
    public void processFindPassword_1(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);

        PasswordRequest passwordRequest = buildPasswordRequest("18973209999", "123456");
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        customerUserService.findPassword(passwordRequest);

        User user1 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user1.getUserId());
        Assert.assertEquals(EncryptUtil.encryptPassword("123456"),user1.getUserPassword());

        //清除redis数据库验证码记录
        stringRedisTemplate.delete(key);
    }

    /**
     * 找回密码  -- 需要连接redis
     */
    @Test
    public void processFindPassword_2(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);

        PasswordRequest passwordRequest = buildPasswordRequest("18973209990", "123456");
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        try{
           customerUserService.findPassword(passwordRequest);
        }catch (MobileCodeNotRightException existsException){
            Assert.assertEquals("短信验证码错误",existsException.getMessage());
        }

        //清除redis数据库验证码记录
        stringRedisTemplate.delete(key);
    }

    /**
     * 修改密码 -- 需要连接redis
     */
    @Test
    public void processUpdatePassword_1(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);

        String key = RedisConstant.MOBILE_CODE_CUSTOMER_PASSWORD_PHONE_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);

        UserRequest userRequest = buildUserRequest("18973209999","872634","654321",null,
                null,null,null,null,null,null,null);
        customerUserService.updatePassword(userRequest,1L);

        User user1 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user1.getUserId());
        Assert.assertEquals(EncryptUtil.encryptPassword("654321"),user1.getUserPassword());

        //清除redis数据库验证码记录
        stringRedisTemplate.delete(key);
    }

    /**
     * 修改密码 -- 需要连接redis
     * 验证码出错
     */
    @Test
    public void processUpdatePassword_2(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);

        String key = RedisConstant.MOBILE_CODE_CUSTOMER_PASSWORD_PHONE_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);

        UserRequest userRequest = buildUserRequest("18973209999","872034","654321",
                null,null,null,null,null,null,null,null);
        try {
            customerUserService.updatePassword(userRequest, 1L);
        }catch (MobileCodeNotRightException exception){
            Assert.assertEquals("短信验证码错误",exception.getMessage());
        }
    }

    /**
     * 修改手机号  -- 需要连接redis
     */
    @Test
    public void processUpdatePhone_1(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);

        String key = RedisConstant.MOBILE_CODE_CUSTOMER_NEW_PHONE_PREFIX + "18973208888";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest("18973208888","872634","654321",null,
                null,null,null,null,null,null,null);
        customerUserService.updatePhone(userRequest,1L);

        User user1 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user1.getUserId());
        Assert.assertEquals("18973208888",user1.getPhone());
    }

    /**
     * 修改手机号  -- 需要连接redis
     * 会抛异常  --  提示手机号重复绑定
     */
    @Test
    public void processUpdatePhone_3(){
        User user1 = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user1);
        User user2 = buildUser(2L, "mm1", null, null, "18973208888");
        userMapper.insertSelective(user2);

        String key = RedisConstant.MOBILE_CODE_CUSTOMER_NEW_PHONE_PREFIX + "18973208888";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest("18973208888","872634","654321",null,
                null,null,null,null,null,null,null);
        customerUserService.updatePhone(userRequest,1L);

    }

    /**
     * 修改手机号  -- 需要连接redis
     * 短信验证码错误
     */
    @Test
    public void processUpdatePhone_2(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);

        String key = RedisConstant.MOBILE_CODE_CUSTOMER_NEW_PHONE_PREFIX + "18973208888";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest("18973209999","872034","654321",null,
                null,null,null,null,null,null,null);
        customerUserService.updatePassword(userRequest, 1L);
    }

    /**
     * 绑定更新支付宝账号信息 -- 需要连接redis
     * 支付宝账号信息为null
     */
    @Test
    public void processUpdateAlipayAccount_1(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_ALIPAY_BIND_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest(null, "872634", null, null, null,
                null, null, null, null, "2446576918", "mmm");

        customerUserService.updateAlipayAccount(userRequest,1L);
        User user1 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user1.getUserId());
        Assert.assertEquals("2446576918",user1.getAlipayAccount());
        Assert.assertEquals("mmm",user1.getRealName());
        Assert.assertEquals(Long.valueOf(5),user1.getUserIntgeralTreasure());

        // 授权奖励
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 9);
        logger.debug("userIncome is : {}",userIncome);
        Assert.assertNotNull(userIncome);
        Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(9),userIncome.getType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
        Assert.assertTrue(new BigDecimal("0.05").compareTo(userIncome.getMoney()) == 0);
    }

    /**
     * 更新支付宝账号 -- 需要连接redis
     */
    @Test
    public void processUpdateAlipayAccount_2(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        user.setAlipayAccount("93847592");
        user.setRealName("gggg");
        userMapper.insertSelective(user);
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_ALIPAY_BIND_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest(null, "872634", null, null, null,
                null, null, null, null, "2446576918", "mmm");
        customerUserService.updateAlipayAccount(userRequest,1L);
        User user1 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user1.getUserId());
        Assert.assertEquals("2446576918",user1.getAlipayAccount());
        Assert.assertEquals("mmm",user1.getRealName());
    }

    /**
     * 更新支付宝账号 -- 需要连接redis
     * 验证码出错
     */
    @Test
    public void processUpdateAlipayAccount_3(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        user.setAlipayAccount("93847592");
        user.setRealName("gggg");
        userMapper.insertSelective(user);
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_ALIPAY_BIND_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest(null, "870634", null, null, null,
                null, null, null, null, "2446576918", "mmm");

        try {
            customerUserService.updatePassword(userRequest, 1L);
        }catch (MobileCodeNotRightException exception){
            Assert.assertEquals("短信验证码错误",exception.getMessage());
        }
    }

    /**
     * 绑定支付宝账号  -- 需要连接redis
     * 支付宝重复绑定
     */
    @Test
    public void processUpdateAlipayAccount_4(){
        User aa = buildUser(2L, "aa", null, null, "18973208888");
        aa.setAlipayAccount("93847592");
        aa.setRealName("gggg");
        userMapper.insertSelective(aa);

        User user = buildUser(1L, "mm1", null, null, "18973209999");
        userMapper.insertSelective(user);
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_ALIPAY_BIND_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest(null, "872634", null, null, null,
                null, null, null, null, "93847592", "mmm");
        try {
            customerUserService.updateAlipayAccount(userRequest, 1L);
        }catch (BusinessException e){
            Assert.assertEquals("支付宝重复绑定,已绑定的用户信息为：用户id:2;用户名:aa;手机号:18973208888",e.getMessage());
        }
    }

    /**
     * 更新支付宝账号 -- 需要连接redis
     * 支付宝重复绑定
     */
    @Test
    public void processUpdateAlipayAccount_5(){
        User aa = buildUser(2L, "aa", null, null, "18973208888");
        aa.setAlipayAccount("93847592");
        aa.setRealName("gggg");
        userMapper.insertSelective(aa);

        User user = buildUser(1L, "mm1", null, null, "18973209999");
        aa.setAlipayAccount("93848888888");
        aa.setRealName("88888");
        userMapper.insertSelective(user);
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_ALIPAY_BIND_PREFIX + "18973209999";
        stringRedisTemplate.opsForValue().set(key,"872634",RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
        UserRequest userRequest = buildUserRequest(null, "872634", null, null, null,
                null, null, null, null, "93847592", "mmm");
        try {
            customerUserService.updateAlipayAccount(userRequest, 1L);
        }catch (BusinessException e){
            Assert.assertEquals("支付宝重复绑定,已绑定的用户信息为：用户id:2;用户名:aa;手机号:18973208888",e.getMessage());
        }
    }

    /**
     * 生成验证码  -- 需要连接redis
     *
     */
    @Test
    public void processGenerateValidCodeByType(){
        customerUserService.generateValidCodeByType("18973209999",1,request);
        String key = MobileCodeMapper.getRedisValidCodeKeyPrefixByType(1) + "18973209999";
        String mobileCode = stringRedisTemplate.opsForValue().get(key);
        Assert.assertEquals(6,mobileCode.length());
    }

    /**
     * 获取收益记录
     */
    @Test
    public void processGetUserIncomeRecordDetail(){
        User user = buildUser(1L, "mm1", null, null, "18973209999");
        user.setAlipayAccount("93847592");
        user.setRealName("gggg");
        userMapper.insertSelective(user);
        ResponseResult<UserIncomeRecordResponse> userIncomeRecordDetail = customerUserService.getUserIncomeRecordDetail(1L);
        UserIncomeRecordResponse data = userIncomeRecordDetail.getData();
        Assert.assertTrue(new BigDecimal("0.00").compareTo(data.getUserAmount()) == 0);
        Assert.assertTrue(new BigDecimal("0.00").compareTo(data.getLastDayPaidCommission()) == 0);
        Assert.assertTrue(new BigDecimal("0.00").compareTo(data.getLastMonthPaidEstimate()) == 0);
        Assert.assertTrue(new BigDecimal("0.00").compareTo(data.getLastMonthSettlementEstimate()) == 0);
        Assert.assertTrue(new BigDecimal("0.00").compareTo(data.getThisDayPaidCommission()) == 0);
        Assert.assertTrue(new BigDecimal("0.00").compareTo(data.getThisMonthPaidEstimate()) == 0);
        Assert.assertEquals(Integer.valueOf(0),data.getLastDayPaidCount());
        Assert.assertEquals(Integer.valueOf(0),data.getThisDayPaidCount());
    }

}
