package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.LogOffApplyConstant;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.EntitySettingsMapper;
import com.alipay.rebate.shop.dao.mapper.LogOffApplyMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.LogOffApply;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.service.customer.CustomerLogOffApplyService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerLogOffApplyServiceTest {

    @Resource
    UserMapper userMapper;
    @Resource
    CustomerLogOffApplyService customerLogOffApplyService;
    @Resource
    LogOffApplyMapper logOffApplyMapper;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        logOffApplyMapper.deleteByUserId(1L);
        User user = buildUser(1L, "mm1", null, null, "13476259833");
        userMapper.insertSelective(user);
    }
    @After
    public void after(){
        userMapper.deleteUserById(1L);
        logOffApplyMapper.deleteByUserId(1L);
    }

    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private LogOffApply buildLogOffApply(Long userId,String userName,String phone,Integer status){
        LogOffApply logOffApply = new LogOffApply();
        logOffApply.setUserId(userId);
        logOffApply.setUserName(userName);
        logOffApply.setPhone(phone);
        Date date = new Date();
        logOffApply.setCreateTime(date);
        logOffApply.setUpdateTime(date);
        logOffApply.setStatus(status);
        return logOffApply;
    }

    /**
     * 提交注销申请
     */
    @Test
    public void processAdd_1(){
        int add = customerLogOffApplyService.add(1L);
        LogOffApply logOffApply = logOffApplyMapper.selectLatestByUserId(1L);
        Assert.assertEquals(Long.valueOf(1),logOffApply.getUserId());
        Assert.assertEquals(Integer.valueOf(0),logOffApply.getStatus());
        Assert.assertEquals("13476259833",logOffApply.getPhone());
        Assert.assertEquals("mm1",logOffApply.getUserName());
    }

    /**
     * 重复提交注销申请
     */
    @Test
    public void processAdd_2(){
        processAdd_1();
        try {
            customerLogOffApplyService.add(1L);
        }catch (BusinessException exception){
            Assert.assertEquals("您已提交注销申请",exception.getMessage());
        }
    }

    /**
     * 账号已注销
     */
    @Test
    public void processAdd_3(){
        LogOffApply logOffApply = buildLogOffApply(1L, "mm1", "13476259833", 1);
        logOffApplyMapper.insertSelective(logOffApply);

        try {
            customerLogOffApplyService.add(1L);
        }catch (BusinessException exception){
            Assert.assertEquals("账号已注销",exception.getMessage());
        }
    }

    /**
     * 获取最新申请注销详情
     */
    @Test
    public void processLatestApply(){
        processAdd_1();
        LogOffApply logOffApply = customerLogOffApplyService.latestApply(1L);
        Assert.assertEquals(Long.valueOf(1),logOffApply.getUserId());
        Assert.assertEquals(Integer.valueOf(0),logOffApply.getStatus());
        Assert.assertEquals("13476259833",logOffApply.getPhone());
        Assert.assertEquals("mm1",logOffApply.getUserName());

    }

}
