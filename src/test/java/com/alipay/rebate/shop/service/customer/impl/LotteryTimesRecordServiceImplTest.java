package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.LotteryMapper;
import com.alipay.rebate.shop.dao.mapper.LotteryTimesRecordMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.helper.UserHelper;
import com.alipay.rebate.shop.model.Lottery;
import com.alipay.rebate.shop.model.LotteryTimesRecord;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.pojo.user.customer.PlusOrReduceVirMoneyReq;
import com.alipay.rebate.shop.service.customer.CustomerLotteryTimesRecordService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import gherkin.lexer.Da;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class LotteryTimesRecordServiceImplTest {

    private Logger logger = LoggerFactory.getLogger(LotteryTimesRecordServiceImplTest.class);
    @Resource
    private LotteryTimesRecordMapper lotteryTimesRecordMapper;
    @Resource
    private LotteryMapper lotteryMapper;
    @Resource
    private UserHelper userHelper;
    @Resource
    private UserMapper userMapper;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    CustomerLotteryTimesRecordService customerLotteryTimesRecordService;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        lotteryMapper.deleteLotteryById(1);
        lotteryMapper.deleteLotteryById(2);
        lotteryTimesRecordMapper.deleteLotteryTimesRecordById(1L);
        lotteryTimesRecordMapper.deleteLotteryTimesRecordById(2L);

        userIncomeMapper.deleteUserIncomeByUserId(1L);
        userIncomeMapper.deleteUserIncomeByUserId(2L);

        // 准备用户信息
        User user = buildUser(1L, "mm1", null, null, "1287623475");
        userMapper.insertSelective(user);

        // 准备一条抽奖记录
        Lottery lottery = buildLottery(2, 2);
        lotteryMapper.insertSelective(lottery);

    }

    @After
    public void after(){
        userMapper.deleteUserById(1L);
        lotteryMapper.deleteLotteryById(1);
        userMapper.deleteUserById(2L);
        lotteryMapper.deleteLotteryById(2);
        lotteryTimesRecordMapper.deleteLotteryTimesRecordById(1L);
        lotteryTimesRecordMapper.deleteLotteryTimesRecordById(2L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        userIncomeMapper.deleteUserIncomeByUserId(2L);

    }

    private PlusOrReduceVirMoneyReq buildPlusOrReduceVirMoneyReq(Long num, Integer type, Integer awardType, boolean flag){
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = new PlusOrReduceVirMoneyReq();
        plusOrReduceVirMoneyReq.setAwardType(awardType);
        plusOrReduceVirMoneyReq.setNum(num);
        plusOrReduceVirMoneyReq.setType(type);
        plusOrReduceVirMoneyReq.setFlag(flag);
        return plusOrReduceVirMoneyReq;
    }

    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone) {
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private Lottery buildLottery(Integer maxTime,Integer freeTime){
        Lottery lottery = new Lottery();
        lottery.setId(1);
        lottery.setTitle("测试");
        lottery.setUpdateTime(new Date());
        lottery.setCreateTime(new Date());
        lottery.setMaxTime(maxTime);
        lottery.setFreeTime(freeTime);
        lottery.setConsumeJf(10);
        lottery.setJsonData("{\"1\":{\"type\":\"top\",\"data\":[{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"module\":\"顶部·\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"\",\"oper\":\"编辑\",\"expand\":\"\"}]},\"2\":{\"type\":\"middle\",\"data\":[{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"1\",\"awardType\":\"积分\",\"awardNum\":\"1\",\"awardRate\":\"1\",\"awardTip\":\"\",\"awardRedirect\":\"\",\"awardRemark\":\"\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"2\",\"awardType\":\"积分\",\"awardNum\":\"10\",\"awardRate\":\"10\",\"awardTip\":\"2\",\"awardRedirect\":\"\",\"awardRemark\":\"\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"3\",\"awardType\":\"积分\",\"awardNum\":\"10\",\"awardRate\":\"10\",\"awardTip\":\"3\",\"awardRedirect\":\"\",\"awardRemark\":\"\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"4\",\"awardType\":\"积分\",\"awardNum\":\"10\",\"awardRate\":\"10\",\"awardTip\":\"4\",\"awardRedirect\":\"\",\"awardRemark\":\"\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/03.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"开始按钮\",\"bgPic\":\"http://picdata.appwangzhan.fltlm.com/1562843647795_抽奖自定义_02.png\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"5\",\"awardType\":\"积分\",\"awardNum\":\"10\",\"awardRate\":\"10\",\"awardTip\":\"5\",\"awardRedirect\":\"\",\"awardRemark\":\"\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"6\",\"awardType\":\"积分\",\"awardNum\":\"10\",\"awardRate\":\"10\",\"awardTip\":\"6\",\"awardRedirect\":\"\",\"awardRemark\":\"\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"7\",\"awardType\":\"积分\",\"awardNum\":\"10\",\"awardRate\":\"10\",\"awardTip\":\"7\",\"awardRedirect\":\"\",\"awardRemark\":\"\"},{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1562837362537_抽奖自定义_01.png\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"appUrl\",\"oper\":\"编辑\",\"expand\":\"\",\"awardName\":\"8\",\"awardType\":\"积分\",\"awardNum\":\"10\",\"awardRate\":\"10\",\"awardTip\":\"8\",\"awardRedirect\":\"\",\"awardRemark\":\"\"}]},\"3\":{\"type\":\"bottom\",\"data\":[{\"pic\":\"http://picdata.appwangzhan.fltlm.com/1563768102228_抽奖自定义-2_03 (8).png\",\"module\":\"底部\",\"url\":\"html/windows/goodgoods_win.html\",\"urlType\":\"\",\"oper\":\"编辑\",\"expand\":\"\"}]}}");
        lottery.setIsCurrentUse(1);
        lottery.setLotteryDescribe("仅供测试");
        return lottery;
    }

    private LotteryTimesRecord buildLotteryTimesRecord(Long userId,Integer times){
        LotteryTimesRecord lotteryTimesRecord = new LotteryTimesRecord();
        lotteryTimesRecord.setId(1L);
        lotteryTimesRecord.setUserId(userId);
        lotteryTimesRecord.setTimes(times);
        lotteryTimesRecord.setCreateDate(new Date());
        lotteryTimesRecord.setCreateTime(new Date());
        return lotteryTimesRecord;
    }

    private List<UserIncome> getUserIncomes(List<UserIncome> userIncomes,Long userId){
        List<UserIncome> userIncomeList = new ArrayList<>();
        for (UserIncome userIncome : userIncomes) {
            if (userIncome.getUserId() == userId){
                userIncomeList.add(userIncome);
            }
        }
        return userIncomeList;
    }


    /**
     * 增加当天抽奖次数和奖励用户
     * 测试此方法需要修改LotteryTimesRecordServiceImpl类 addOrUpdateCurDayTimes方法查询抽奖配置
     * 用户第一次抽奖
     */
    @Test
    public void processAddOrUpdateCurDayTimes_1(){
        // 奖励集分宝
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = buildPlusOrReduceVirMoneyReq(10L, 6, 2, true);
        customerLotteryTimesRecordService.addOrUpdateCurDayTimes(1L,plusOrReduceVirMoneyReq);

        // 检验抽奖记录
        LotteryTimesRecord lotteryTimesRecord = lotteryTimesRecordMapper.selectCurDayTimesRecordByUserId(1L);
        Assert.assertEquals(Long.valueOf(1L),lotteryTimesRecord.getUserId());
        Assert.assertEquals(Integer.valueOf(1),lotteryTimesRecord.getTimes());

        // 校验用户收益记录
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserId(1L);
        Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
        Assert.assertEquals(Integer.valueOf(6),userIncome.getType());
        Assert.assertTrue(new BigDecimal("0.1").compareTo(userIncome.getMoney()) == 0);

        // 校验用户集分宝是否添加成功
        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(10),user.getUserIntgeralTreasure());
    }

    /**
     * 增加当天抽奖次数和奖励用户
     * 测试此方法需要修改LotteryTimesRecordServiceImpl类 addOrUpdateCurDayTimes方法查询抽奖配置
     * 用户第二次抽奖
     */
    @Test
    public void processAddOrUpdateCurDayTimes_2(){
        // 第一次抽奖
        processAddOrUpdateCurDayTimes_1();

        // 奖励集分宝
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = buildPlusOrReduceVirMoneyReq(10L, 6, 2, true);
        customerLotteryTimesRecordService.addOrUpdateCurDayTimes(1L,plusOrReduceVirMoneyReq);

        // 检验抽奖记录
        LotteryTimesRecord lotteryTimesRecord = lotteryTimesRecordMapper.selectCurDayTimesRecordByUserId(1L);
        Assert.assertEquals(Long.valueOf(1L),lotteryTimesRecord.getUserId());
        Assert.assertEquals(Integer.valueOf(2),lotteryTimesRecord.getTimes());

        // 校验用户集分宝是否添加成功
        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(20),user.getUserIntgeralTreasure());

        // 校验用户收益记录
        List<UserIncome> userIncomes = userIncomeMapper.selectAllIncome();
        List<UserIncome> userIncomes1 = getUserIncomes(userIncomes, 1L);
        Assert.assertEquals(2,userIncomes1.size());
        for (UserIncome userIncome : userIncomes1) {
            Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
            Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
            Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
            Assert.assertEquals(Integer.valueOf(6),userIncome.getType());
            Assert.assertTrue(new BigDecimal("0.1").compareTo(userIncome.getMoney()) == 0);
        }

    }

    /**
     * 增加当天抽奖次数和奖励用户
     * 测试此方法需要修改LotteryTimesRecordServiceImpl类 addOrUpdateCurDayTimes方法查询抽奖配置
     *
     * 抽奖次数达到上限
     */
    @Test
    public void processAddOrUpdateCurDayTimes_3(){

        // 第二次
        processAddOrUpdateCurDayTimes_2();

//        try {
            // 奖励集分宝
            PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = buildPlusOrReduceVirMoneyReq(10L, 6, 2, true);
            customerLotteryTimesRecordService.addOrUpdateCurDayTimes(1L, plusOrReduceVirMoneyReq);
//        }catch (BusinessException exception){
//            Assert.assertEquals("抽奖次数已达上限",exception.getMessage());
//        }
    }


    /**
     * 增加当天抽奖次数和奖励用户
     * 测试此方法需要修改LotteryTimesRecordServiceImpl类 addOrUpdateCurDayTimes方法查询抽奖配置
     *
     * 没有免费抽奖次数，抽奖消耗用户积分
     */
    @Test
    public void processAddOrUpdateCurDayTimes_4(){
        // 准备一条抽奖记录
        Lottery lottery1 = lotteryMapper.selectLotteryById(1);
        lottery1.setFreeTime(0);
        lotteryMapper.updateByPrimaryKeySelective(lottery1);

//        try {
            // 奖励集分宝
            PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = buildPlusOrReduceVirMoneyReq(10L, 6, 2, true);
            customerLotteryTimesRecordService.addOrUpdateCurDayTimes(1L, plusOrReduceVirMoneyReq);
//        }catch (BusinessException exception){
//            Assert.assertEquals("积分不足",exception.getMessage());
//        }

    }

    /**
     * 增加当天抽奖次数和奖励用户
     * 测试此方法需要修改LotteryTimesRecordServiceImpl类 addOrUpdateCurDayTimes方法查询抽奖配置
     *
     * 没有免费抽奖次数，抽奖消耗用户积分
     */
    @Test
    public void processAddOrUpdateCurDayTimes_6(){
        // 准备一条抽奖记录
        Lottery lottery1 = lotteryMapper.selectLotteryById(1);
        lottery1.setFreeTime(0);
        lotteryMapper.updateByPrimaryKeySelective(lottery1);
        User user = userMapper.selectById(1L);
        user.setUserIntgeral(20L);
        userMapper.updateUser(user);
        // 抽奖
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = buildPlusOrReduceVirMoneyReq(0L, 6, 2, true);
        customerLotteryTimesRecordService.addOrUpdateCurDayTimes(1L, plusOrReduceVirMoneyReq);

        // 查新用户积分是否减少
        User user1 = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(10),user1.getUserIntgeral());
    }


    /**
     * 增加当天抽奖次数和奖励用户
     * 测试此方法需要修改LotteryTimesRecordServiceImpl类 addOrUpdateCurDayTimes方法查询抽奖配置
     *
     * 没有免费抽奖次数，抽奖消耗用户积分
     */
    @Test
    public void processAddOrUpdateCurDayTimes_5(){
        // 准备一条抽奖记录
        Lottery lottery1 = lotteryMapper.selectLotteryById(1);
        lottery1.setFreeTime(0);
        lotteryMapper.updateByPrimaryKeySelective(lottery1);
        // 准备用户信息,准备30积分
        User user = buildUser(2L, "mm1", null, null, "1287623475");
        user.setUserIntgeral(30L);
        userMapper.insertSelective(user);

        // 奖励集分宝
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = buildPlusOrReduceVirMoneyReq(10L, 6, 2, true);
        customerLotteryTimesRecordService.addOrUpdateCurDayTimes(2L, plusOrReduceVirMoneyReq);

        // 校验是否减少用户积分
        User user1 = userMapper.selectById(2L);
        Assert.assertEquals(Long.valueOf(30),user1.getUserIntgeral());
        Assert.assertEquals(Long.valueOf(10),user1.getUserIntgeralTreasure());

        // 校验用户收益记录
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserId(2L);
        Assert.assertEquals(Long.valueOf(2),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
        Assert.assertEquals(Integer.valueOf(6),userIncome.getType());
        Assert.assertTrue(new BigDecimal("0.1").compareTo(userIncome.getMoney()) == 0);

    }

}