package com.alipay.rebate.shop.service.customer.impl;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.TaskDoRecordMapper;
import com.alipay.rebate.shop.dao.mapper.TaskSettingsMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.exceptoin.BusinessException;
import com.alipay.rebate.shop.model.TaskDoRecord;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.service.customer.CustomerTaskDoRecordService;
import com.alipay.rebate.shop.utils.EncryptUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class CustomerTaskDoRecordServiceImplTest {

    private Logger logger = LoggerFactory.getLogger(CustomerTaskDoRecordServiceImpl.class);
    @Resource
    private TaskDoRecordMapper taskDoRecordMapper;
    @Resource
    private TaskSettingsMapper taskSettingsMapper;
    @Resource
    private UserIncomeMapper userIncomeMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    CustomerTaskDoRecordService customerTaskDoRecordService;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        taskDoRecordMapper.deleteByPrimaryKey(1L);
        taskSettingsMapper.deleteByPrimaryKey(1);
        taskSettingsMapper.deleteByPrimaryKey(2);
        taskSettingsMapper.deleteByPrimaryKey(3);
        taskSettingsMapper.deleteByPrimaryKey(4);
        taskSettingsMapper.deleteByPrimaryKey(5);
        // 准备一条用户信息
        User user = buildUser(1L, "mm1", null, null, "13452678635");
        userMapper.insertSelective(user);

        // 准备每日任务
        TaskSettings taskSettings = buildTaskSettings(1, 5, 1, 1, "绑定支付宝");
        taskSettingsMapper.insertSelective(taskSettings);
        TaskSettings taskSettings1 = buildTaskSettings(2, 5, 1, 2, "微信授权");
        taskSettingsMapper.insertSelective(taskSettings1);
        TaskSettings taskSettings2 = buildTaskSettings(3, 5, 1, 3, "淘宝授权");
        taskSettingsMapper.insertSelective(taskSettings2);
        TaskSettings taskSettings3 = buildTaskSettings(4, 5, 1, 4, "计时浏览");
        taskSettingsMapper.insertSelective(taskSettings3);
        TaskSettings taskSettings4 = buildTaskSettings(5, 5, 1, 5, "绑定支付宝");
        taskSettingsMapper.insertSelective(taskSettings4);

    }

    @After
    public void after(){
        userMapper.deleteUserById(1L);
        taskDoRecordMapper.deleteByPrimaryKey(1L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        taskSettingsMapper.deleteByPrimaryKey(1);
        taskSettingsMapper.deleteByPrimaryKey(2);
        taskSettingsMapper.deleteByPrimaryKey(3);
        taskSettingsMapper.deleteByPrimaryKey(4);
        taskSettingsMapper.deleteByPrimaryKey(5);
    }

    private TaskSettings buildTaskSettings(Integer id,Integer num,Integer maxTimes,Integer subType,String name){
        TaskSettings taskSettings = new TaskSettings();
        taskSettings.setId(id);
        taskSettings.setIsUse(1);
        taskSettings.setAwardNum(num);
        taskSettings.setAwardType(2);
        taskSettings.setMaxTimes(maxTimes);
        taskSettings.setSubType(subType);
        taskSettings.setType(1);
        taskSettings.setName(name);
        taskSettings.setCreateTime(new Date());
        taskSettings.setUpdateTime(new Date());
        return taskSettings;
    }

    private User buildUser(Long userId, String userName, Long parentUserId, String parentUserName, String phone){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setUserPassword(EncryptUtil.encryptPassword("123456"));
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private TaskDoRecord buildTaskDoRecord(Integer subType,Long id,Integer type,Long userId){
        TaskDoRecord taskDoRecord = new TaskDoRecord();
        taskDoRecord.setId(id);
        taskDoRecord.setSubType(subType);
        taskDoRecord.setType(type);
        taskDoRecord.setUserId(userId);
        taskDoRecord.setCreateTime(new Date());
        taskDoRecord.setUpdateTime(new Date());
        return taskDoRecord;
    }

    private Date getDate(int type){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, type); //得到前一du天
        Date date = calendar.getTime();
        return date;
    }

    private TaskDoRecord getTaskDoRecordBySubType(List<TaskDoRecord> taskDoRecords,Integer subType){
        for (TaskDoRecord taskDoRecord : taskDoRecords){
            if (subType == taskDoRecord.getSubType()){
                return taskDoRecord;
            }
        }
        return null;
    }
    /**
     * 添加每日任务记录
     */
    @Test
    public void processAddTaskDoRecord_1(){

        TaskDoRecord taskDoRecord = buildTaskDoRecord(4, 1L, 2, 1L);
        customerTaskDoRecordService.addTaskDoRecord(taskDoRecord);
        List<TaskDoRecord> taskDoRecords = taskDoRecordMapper.selectTaskDoRecordByUserId(1L);

        TaskDoRecord taskDoRecordBySubType = getTaskDoRecordBySubType(taskDoRecords, 4);
        Assert.assertEquals(Long.valueOf(1L),taskDoRecordBySubType.getUserId());
        Assert.assertEquals(Integer.valueOf(4),taskDoRecordBySubType.getSubType());
        Assert.assertEquals(Integer.valueOf(2),taskDoRecordBySubType.getType());

        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(1L, 2);
        logger.debug("userIncome is : {} ",userIncome);
        Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getType());
        Assert.assertTrue(new BigDecimal("0.05").compareTo(userIncome.getMoney()) == 0);
    }

    /**
     *
     */
    @Test
    public void processAddTaskDoRecord_2(){
        processAddTaskDoRecord_1();

        try {
            TaskDoRecord taskDoRecord = buildTaskDoRecord(4, 1L, 2, 1L);
            customerTaskDoRecordService.addTaskDoRecord(taskDoRecord);
        }catch (BusinessException exception){
            Assert.assertEquals("每日任务次数已达上限",exception.getMessage());
        }
    }

}
