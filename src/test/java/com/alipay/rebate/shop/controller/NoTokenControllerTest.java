package com.alipay.rebate.shop.controller;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.MobileCodeMapper;
import com.alipay.rebate.shop.constants.RedisConstant;
import com.alipay.rebate.shop.controller.customer.NoTokenController;
import com.alipay.rebate.shop.dao.mapper.AppSettingsMapper;
import com.alipay.rebate.shop.dao.mapper.MobileCodeInformationMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.helper.MobileCodeInformationHelper;
import com.alipay.rebate.shop.model.MobileCodeInformation;
import com.alipay.rebate.shop.service.customer.CustomerUserService;
import com.alipay.rebate.shop.utils.AliUtil;
import com.aliyuncs.exceptions.ClientException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class NoTokenControllerTest {

    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Resource
    MobileCodeInformationMapper informationMapper;
    @Resource
    MobileCodeInformationHelper informationHelper;

    private Logger logger = LoggerFactory.getLogger(NoTokenController.class);

    private String generateMobileCode() {
        String mobileCode = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
        return mobileCode;
    }

    private void generateMobileVerificationCode(String phone) {
        // 生成六位数字验证码
        String mobileCode = generateMobileCode();
        logger.debug("mobileCode is : {}", mobileCode);
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX + phone;
//        String key = AppSettingsHelper.getAppKey() + "_" + phone;
        logger.debug("key is : {}", key);
        int type = MobileCodeMapper.getRedisValidCodeValuePrefixByKey(RedisConstant.MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX);
        MobileCodeInformation information = informationHelper.getMobileCodeInformation(phone, type, mobileCode);
        // 发送验证码
        try {
            // 先往redis存放验证码信息，再发送给客户
            stringRedisTemplate.opsForValue().set(key, mobileCode, RedisConstant.MOBILE_CODE_CUSTOMER_TIME_OUT, TimeUnit.MINUTES);
            logger.debug("mobileCode is : {}", stringRedisTemplate.opsForValue().get(key));
            AliUtil.sendSms(phone, mobileCode);
            informationMapper.insertMobileCodeInformation(information);
            logger.debug("redis key is : {}, value is : {}", key, phone);

        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试时候生成验证码
     */
    @Test
    public void processMobileVerificationCode(){
        generateMobileVerificationCode("15678398350");
        String key = RedisConstant.MOBILE_CODE_CUSTOMER_NO_LOGIN_PREFIX + "15678398350";
        String value = stringRedisTemplate.opsForValue().get(key);
        int length = value.length();
        Assert.assertEquals(Long.valueOf(6),Long.valueOf(length));
    }
}
