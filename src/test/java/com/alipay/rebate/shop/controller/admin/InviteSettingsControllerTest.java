package com.alipay.rebate.shop.controller.admin;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.dao.mapper.InviteSettingsMapper;
import com.alipay.rebate.shop.model.InviteSettings;
import com.alipay.rebate.shop.service.admin.InviteSettingsService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class InviteSettingsControllerTest {

    @Resource
    private InviteSettingsService inviteSettingsService;
    @Resource
    InviteSettingsMapper inviteSettingsMapper;

    @Before
    public void before(){

        inviteSettingsMapper.deleteByPrimaryKey(2);

        InviteSettings inviteSettings = buildInviteSettings();
        inviteSettingsMapper.insertSelective(inviteSettings);


    }
    @After
    public void after(){
        inviteSettingsMapper.deleteByPrimaryKey(2);
    }

    private InviteSettings buildInviteSettings(){
        InviteSettings inviteSettings = new InviteSettings();
        inviteSettings.setId(2);
        inviteSettings.setAwardGradJson("{\"1\":{\"begin\":1,\"end\":3,\"award\":\"10\"},\"2\":{\"begin\":4,\"" +
                "end\":10,\"award\":\"20\"},\"3\":{\"begin\":11,\"end\":1000,\"award\":\"30\"}}");
        inviteSettings.setBillPicUrl("http://picdata.appwangzhan.fltlm.com/42737c40c73b623f7c3faa063625fec.png");
        inviteSettings.setConditionJson("9.9");
        inviteSettings.setRuleHtml("<p><span style=\"font-weight: bold;\">【基础奖励】</span></p><p><span style=\"font-weight: bold;\">当天邀请1-3人奖励为0，邀请4人奖励5元，邀请5人奖励10元，邀请6人奖励15元，以此类推。</span></p><p><span style=\"font-weight: bold;\">【上升奖励】</span></p><p><span style=\"font-weight: bold;\">当日邀请人数达到10人或者10人以上的时候，会从原来的基础奖励变为上升奖励，从原来的每邀请一人奖励5元上升为每邀请一人奖励8元，邀请10人后累计奖励38元，邀请11人奖励46元，邀请12人奖励54元，以上每增加1位奖励8元，以此类推。</span></p><p><span style=\"font-weight: bold;\">【规则提示】</span></p><p><span style=\"font-weight: bold;\">[1]" +
                " 什么是有效用户？有购物满10元淘宝产品的为有效用户，用户需要确认收货为准，用户退款了产品会扣除对应的邀请奖励。</span></p><p><span style=\"font-weight: bold;\">[2] 用户是以邀请当天奖励为准，例：用户9月10号邀请，9月30号下单付款，10月5号确认收货，这个用户是按照9月10号当天邀请了多少人来奖励的。</span></p>");
        inviteSettings.setUpdateTime(new Date());
        inviteSettings.setCreateTime(new Date());
        return inviteSettings;
    }

    private InviteSettings getInviteSettings(List<InviteSettings> inviteSettings1,Integer id){
        for (InviteSettings inviteSettings : inviteSettings1) {
            if (inviteSettings.getId() == id){
                return inviteSettings;
            }
        }
        return null;
    }

    /**
     * 更新拉新奖励
     */
    @Test
    public void update() {
        InviteSettings inviteSettings = buildInviteSettings();
        inviteSettings.setRuleHtml("html");
        inviteSettings.setAwardGradJson("lsidjfoisj");
        inviteSettings.setConditionJson("爆款");
        inviteSettingsService.update(inviteSettings);

        List<InviteSettings> inviteSettings1 = inviteSettingsMapper.selectAll();
        InviteSettings inviteSettings2 = getInviteSettings(inviteSettings1, 2);
        Assert.assertEquals(Integer.valueOf(2),inviteSettings2.getId());
        Assert.assertEquals("html",inviteSettings2.getRuleHtml());
        Assert.assertEquals("lsidjfoisj",inviteSettings2.getAwardGradJson());
        Assert.assertEquals("爆款",inviteSettings2.getConditionJson());

    }

    @Test
    public void detail() {
        InviteSettings byId = inviteSettingsService.findById(2);
        Assert.assertEquals(Integer.valueOf(2),byId.getId());
        Assert.assertEquals("http://picdata.appwangzhan.fltlm.com/42737c40c73b623f7c3faa063625fec.png",byId.getBillPicUrl());
        Assert.assertEquals("9.9",byId.getConditionJson());
    }
}