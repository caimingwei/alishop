//package com.alipay.rebate.shop.utils;
//
//import com.alipay.rebate.shop.pojo.HttpResponse;
//import java.io.IOException;
//import java.util.Map;
//import org.apache.http.HttpEntity;
//import org.apache.http.client.config.RequestConfig;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpDelete;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// * @projectName:openapi
// * @author:
// * @createTime: 2019/04/24 14:55
// * @description:
// */
//public class HttpUtil {
//
//    private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);
//
//    public static String doGet(String url, Map<String,String> headers) {
//        CloseableHttpClient client = HttpClients.createDefault();
//        HttpGet httpGet = new HttpGet(url);
//        setGetHeaders(httpGet,headers);
//        CloseableHttpResponse response = null;
//        logger.debug("http GET method");
//        try {
//            response = client.execute(httpGet);
//            HttpEntity entity = response.getEntity();
//            logger.debug("http GET method");
//            return  EntityUtils.toString(entity, "UTF-8");
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (response != null) {
//                logger.debug("http GET method");
//                try {
//                    response.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            try {
//                client.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//
//    public static String doDelete(String url, Map<String,String> headers) {
//        CloseableHttpClient client = HttpClients.createDefault();
//        HttpDelete httpDelete = new HttpDelete(url);
//        setDeleteHeaders(httpDelete,headers);
//        CloseableHttpResponse response = null;
//        logger.debug("http DELETE method");
//        try {
//            response = client.execute(httpDelete);
//            HttpEntity entity = response.getEntity();
//            logger.debug("http DELETE method");
//            return EntityUtils.toString(entity, "UTF-8");
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (response != null) {
//                logger.debug("http GET method");
//                try {
//                    response.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            try {
//                client.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//
//
//    public static HttpResponse doPost(String url, String object, Map<String,String> headers) {
//        //CloseableHttpClient：建立一个可以关闭的httpClient
//        //这样使得创建出来的HTTP实体，可以被Java虚拟机回收掉，不至于出现一直占用资源的情况。
//        CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
//
//        //设置请求超时时间
//        RequestConfig requestConfig = RequestConfig.custom()
//            .setSocketTimeout(60000)
//            .setConnectTimeout(60000)
//            .setConnectionRequestTimeout(60000)
//            .build();
//
//        try {
//            HttpPost post = new HttpPost(url);
//            post.setConfig(requestConfig);
//            setPostHeaders(post,headers);
////            发送的参数数据
////            Map<String,String> map = Maps.newHashMap();
////            map.put("content","wwgfgfbb");
////            map.put("value","rerrrh");
//            //设置发送的数据
//            StringEntity s = new StringEntity(object);
//            s.setContentEncoding("UTF-8");
//            s.setContentType("application/json");//发送json数据需要设置contentType
//            post.setEntity(s);
//            //获取返回值
//            CloseableHttpResponse res = closeableHttpClient.execute(post);
////            if(res.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
//            String result = EntityUtils.toString(res.getEntity());// 返回json格式：
//            logger.info("POST请求返回的数据是："+result);
//
//            HttpResponse httpResponse = new HttpResponse();
//            httpResponse.setBody(result);
//            httpResponse.setHeaders(res.getAllHeaders());
//            return httpResponse;
////            }
//        }
//        catch (Exception e){
//            logger.info("发生了异常："+e.getMessage());
//        }
//        finally {
//            try {                //关闭流并释放资源
//                closeableHttpClient.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//
//    public static String doPut(String url, String object, Map<String,String> headers) {
//        //CloseableHttpClient：建立一个可以关闭的httpClient
//        //这样使得创建出来的HTTP实体，可以被Java虚拟机回收掉，不至于出现一直占用资源的情况。
//        CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
//
//        //设置请求超时时间
//        RequestConfig requestConfig = RequestConfig.custom()
//            .setSocketTimeout(60000)
//            .setConnectTimeout(60000)
//            .setConnectionRequestTimeout(60000)
//            .build();
//
//        try {
//            HttpPut put = new HttpPut(url);
//            put.setConfig(requestConfig);
//            setPutHeaders(put,headers);
////            发送的参数数据
////            Map<String,String> map = Maps.newHashMap();
////            map.put("content","wwgfgfbb");
////            map.put("value","rerrrh");
//            //设置发送的数据
//            StringEntity s = new StringEntity(object);
//            s.setContentEncoding("UTF-8");
//            s.setContentType("application/json");//发送json数据需要设置contentType
//            put.setEntity(s);
//            //获取返回值
//            CloseableHttpResponse res = closeableHttpClient.execute(put);
////            if(res.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
//            String result = EntityUtils.toString(res.getEntity());// 返回json格式：
//            logger.info("POST请求返回的数据是："+result);
//            return result;
////            }
//        }
//        catch (Exception e){
//            logger.info("发生了异常："+e.getMessage());
//        }
//        finally {
//            try {                //关闭流并释放资源
//                closeableHttpClient.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//
//    private static void setGetHeaders(HttpGet httpGet,Map<String,String> headers){
//
//        if(headers == null ) return;
//
//        for(Map.Entry<String, String> header:headers.entrySet()){
//            String key = header.getKey();
//            String value = header.getValue();
//            httpGet.addHeader(key,value);
//        }
//
//    }
//
//    private static void setPostHeaders(HttpPost httpPost,Map<String,String> headers){
//
//        if(headers == null ) return;
//
//        for(Map.Entry<String, String> header:headers.entrySet()){
//            String key = header.getKey();
//            String value = header.getValue();
//            httpPost.addHeader(key,value);
//        }
//
//    }
//
//    private static void setPutHeaders(HttpPut httpPut,Map<String,String> headers){
//
//        if(headers == null ) return;
//
//        for(Map.Entry<String, String> header:headers.entrySet()){
//            String key = header.getKey();
//            String value = header.getValue();
//            httpPut.addHeader(key,value);
//        }
//
//    }
//
//    private static void setDeleteHeaders(HttpDelete httpDelete,Map<String,String> headers){
//
//        if(headers == null ) return;
//
//        for(Map.Entry<String, String> header:headers.entrySet()){
//            String key = header.getKey();
//            String value = header.getValue();
//            httpDelete.addHeader(key,value);
//        }
//
//    }
//}
