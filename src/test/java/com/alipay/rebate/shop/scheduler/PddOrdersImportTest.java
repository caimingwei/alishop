package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.scheduler.orderimport.PddOrdersImport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class PddOrdersImportTest {

    @Resource
    UserMapper userMapper;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    UserJdPidMapper userJdPidMapper;
    @Resource
    PddOrdersImport pddOrdersImport;
    @Resource
    PddOrdersMapper pddOrdersMapper;
    @Resource
    ActivityBrowseProductMapper activityBrowseProductMapper;
    @Resource
    ActivityProductMapper activityProductMapper;

    private Logger logger = LoggerFactory.getLogger(PddOrdersImportTest.class);
//
//    @Before
//    public void before(){
//
//        userMapper.deleteUserById(10L);
//        userMapper.deleteUserById(11L);
//        userMapper.deleteUserById(12L);
//        userMapper.deleteUserById(13L);
//        userMapper.deleteUserById(14L);
//        userJdPidMapper.deleteUserJdPidByUserId(10L);
//        userJdPidMapper.deleteUserJdPidByUserId(11L);
//        userJdPidMapper.deleteUserJdPidByUserId(12L);
//        userJdPidMapper.deleteUserJdPidByUserId(13L);
//        userJdPidMapper.deleteUserJdPidByUserId(14L);
//        userIncomeMapper.deleteByTradeId("200425-11111111111111");
//        userIncomeMapper.deleteByTradeId("200425-2222222222222");
//        userIncomeMapper.deleteByTradeId("200425-333333333333333");
//        pddOrdersMapper.deletePddOrdersByOrderSn("200425-11111111111111");
//        pddOrdersMapper.deletePddOrdersByOrderSn("200425-2222222222222");
//        pddOrdersMapper.deletePddOrdersByOrderSn("200425-333333333333333");
//        activityBrowseProductMapper.deleteByUserId(11L);
//        activityProductMapper.deleteByPrimaryKey(888);
//
//
//
//
//        User user1 = buildUser(10L,"测试1",null,null,"13418636932");
//        userMapper.insertSelective(user1);
//        // 有上级，上上级
//        User user2 = buildUser(11L,"测试2",12L,"测试3","13418638888");
//        userMapper.insertSelective(user2);
//        // 有上级
//        User user3 = buildUser(12L,"测试3",13L,"测试4","13418636666");
//        userMapper.insertSelective(user3);
//        // 有下级，下下级
//        User user4 = buildUser(13L,"测试4",null,null,"13418632222");
//        userMapper.insertSelective(user4);
//
//        // 为新用户添加拼多多pid
//        UserJdPid userJdPid1 = buildUserJdPid(666L,10L,"10390596_140370520");
//        userJdPidMapper.insertSelective(userJdPid1);
//        UserJdPid userJdPid2 = buildUserJdPid(667L,11L,"10390596_140370511");
//        userJdPidMapper.insertSelective(userJdPid2);
//        UserJdPid userJdPid3 = buildUserJdPid(668L,12L,"10390596_140370512");
//        userJdPidMapper.insertSelective(userJdPid3);
//        UserJdPid userJdPid4 = buildUserJdPid(669L,13L,"10390596_140370513");
//        userJdPidMapper.insertSelective(userJdPid4);
//        UserJdPid userJdPid5 = buildUserJdPid(700L,14L,"10390596_140370514");
//        userJdPidMapper.insertSelective(userJdPid5);
//
//        ActivityBrowseProduct activityBrowseProduct = buildActivityBrowseProduct(11L, 185928347L, 1);
//        activityBrowseProductMapper.insert(activityBrowseProduct);
//    }
//
//    @After
//    public void after(){
//        userMapper.deleteUserById(10L);
//        userMapper.deleteUserById(11L);
//        userMapper.deleteUserById(12L);
//        userMapper.deleteUserById(13L);
//        userMapper.deleteUserById(14L);
//        userJdPidMapper.deleteUserJdPidByUserId(10L);
//        userJdPidMapper.deleteUserJdPidByUserId(11L);
//        userJdPidMapper.deleteUserJdPidByUserId(12L);
//        userJdPidMapper.deleteUserJdPidByUserId(13L);
//        userJdPidMapper.deleteUserJdPidByUserId(14L);
//        userIncomeMapper.deleteByTradeId("200425-11111111111111");
//        userIncomeMapper.deleteByTradeId("200425-2222222222222");
//        userIncomeMapper.deleteByTradeId("200425-333333333333333");
//        pddOrdersMapper.deletePddOrdersByOrderSn("200425-11111111111111");
//        pddOrdersMapper.deletePddOrdersByOrderSn("200425-2222222222222");
//        pddOrdersMapper.deletePddOrdersByOrderSn("200425-333333333333333");
//        activityBrowseProductMapper.deleteByUserId(11L);
//        activityProductMapper.deleteByPrimaryKey(888);
//    }
//
//    private User buildUser(Long userId,String userName,Long parentUserId,String parentUserName,String phone){
//        User user = new User();
//        user.setUserId(userId);
//        user.setUserNickName(userName);
//        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
//        user.setIsAuth(UserContant.IS_AUTH);
//        user.setRelationId(null);
//        user.setSpecialId(null);
//        user.setParentUserId(null);
//        user.setTotalFansOrdersNum(0);
//        user.setUserIntgeralTreasure(0L);
//        user.setPhone(phone);
//        user.setOrdersNum(0L);
//        user.setUserGradeId(1);
//        user.setUserIntgeral(0L);
//        user.setUserAmount(new BigDecimal("0.00"));
//        user.setUserIncome(new BigDecimal("0.00"));
//        user.setUserCommision(new BigDecimal("0.00"));
//        user.setTaobaoUserId(null);
//        user.setCreateTime(new Date());
//        user.setUpdateTime(new Date());
//        user.setParentUserId(parentUserId);
//        user.setParentUserName(parentUserName);
//        return user;
//    }
//
//    private UserJdPid buildUserJdPid(Long id,Long userId,String pddPid){
//        UserJdPid userJdPid = new UserJdPid();
//        userJdPid.setId(id);
//        userJdPid.setAddTime(new Date());
//        userJdPid.setUpdateTime(new Date());
//        userJdPid.setUserId(userId);
//        userJdPid.setPddPId(pddPid);
//        return userJdPid;
//    }
//
//    private PddOrders buildPddOrders(){
//        PddOrders  orders = new PddOrders();
//        orders.setGoodsId(185928347L);
//        orders.setGoodsName("美粮坊手抓饼50片-20片早餐灌煎饼面饼皮原味手抓饼批发家庭装");
//        orders.setCpaNew(1);
//        orders.setGoodsQuantity(1l);
//        orders.setGoodsThumbnailUrl("http://t00img.yangkeduo.com/goods/images/2019-06-10/f5df7b940e11e653631e5532f2e17d2c.jpeg");
//        orders.setOrderAmount(new BigDecimal("2.0"));
//        orders.setOrderCreateTime("2020-05-16 22:30:45");
//        orders.setOrderGroupSuccessTime("2020-05-20 12:23:34");
//        orders.setOrderModifyAt("2020-05-20 12:23:34");
//        orders.setOrderPayTime("2020-05-16 22:40:45");
//        orders.setOrderStatusDesc("已支付");
//        orders.setGoodsPrice(new BigDecimal("2.0"));
//        orders.setOrderSn("200425-11111111111111");
//        orders.setOrderStatus(0);
//        orders.setOrderId("43ynqdW2p2JeU+n6cXX9pg==");
//        orders.setPromotionAmount(new BigDecimal("0.1"));
//        orders.setpId("10390596_140370520");
//        orders.setPromotionRate(20l);
//        orders.setCustomParameters(" ");
//        return orders;
//    }
//
//    private ActivityBrowseProduct buildActivityBrowseProduct(Long userId,Long goodsId,Integer activityId){
//        ActivityBrowseProduct activityBrowseProduct = new ActivityBrowseProduct();
//        activityBrowseProduct.setUserId(userId);
//        activityBrowseProduct.setActivityId(activityId);
//        activityBrowseProduct.setGoodsId(goodsId);
//        activityBrowseProduct.setClickTime(new Date());
//        return activityBrowseProduct;
//    }
//
//    private ActivityProduct buildActivityProduct(Integer id,String goodsId,Integer activityId){
//        ActivityProduct activityProduct = new ActivityProduct();
//        activityProduct.setId(id);
//        activityProduct.setActivityId(activityId);
//        activityProduct.setProductId(goodsId);
//        activityProduct.setShortProductUrl("https://detail.tmall.com/item.htm?id="+goodsId);
//        activityProduct.setShortTitle("新用户0元购芦荟胶100g");
//        activityProduct.setPicUrl("www.baidu.com");
//        activityProduct.setVoucherUrl("https://uland.taobao.com/coupon/edetail?e=TqC4b23ZYIkSq1SNqCiMajJ5c1Of7J2HYHCuomB9NOfDOnaW0ID6pq8DWykFTzaVh2mtO2Msln2dwVCEmPgFhIvQBacu2NEx%2BGoSNEAjokQ4M2%2FUXT47GFZrtTOLgXkcjDppvlX%2Bob%2BBtHA6T8TZnQSD1EQRCW%2BLzEFJTNph2qt7ov5%2F0O2eVTDOaNhNJ6ONPOnbN1rJJNM%3D&traceId=0b01ddd715686261550806539e&union_lens=lensId:0b0b6072_0bbf_16d39679e61_dbb9&xId=xViicNkUOF8jGNZwoNCzZgjR94cCM72IjwhKASvPfk5gYMXsQhiBkznezPs10u8SyPakwXo5T6KRkHayUpPm9o");
//        activityProduct.setRefundsPrice(new BigDecimal("9.98"));
//        activityProduct.setAfterPrice(new BigDecimal("9.98"));
//        activityProduct.setCommissionRatio(80);
//        activityProduct.setLimitNum(100);
//        activityProduct.setWeight(1000);
//        activityProduct.setVirtualSaleNum(500);
//        activityProduct.setQualType(1);
//        activityProduct.setQualJson(null);
//        activityProduct.setEndTime(new Date());
//        return activityProduct;
//    }
//
//    private UserIncome getUserIncomeByUserId(List<UserIncome> userIncomeList,Long userId){
//        logger.debug("getUserIncomeByUserId");
//        for (UserIncome income : userIncomeList) {
//            if (income.getUserId().equals(userId)){
//                return income;
//            }
//        }
//        return null;
//    }
//
//    private void compareUserIncome(UserIncome userIncome,Long userId, BigDecimal money,
//                                   String tradeId,
//                                   Long tkStatus, Integer status){
//        logger.debug("compareUserIncome");
//        logger.debug("userId is : {},{}",userId,userIncome.getUserId());
//        Assert.assertEquals(userId,userIncome.getUserId());
//        logger.debug("userIncome money is : {}",userIncome.getMoney());
//        logger.debug("money is : {}",money);
//        Assert.assertTrue(money.compareTo(userIncome.getMoney()) == 0);
//        Assert.assertEquals(tradeId,userIncome.getTradeId());
//        Assert.assertEquals(tkStatus,userIncome.getTkStatus());
//        Assert.assertEquals(status,userIncome.getStatus());
//    }
//
//    private void comparePddOrders(PddOrders orders,PddOrders previousPddOrders){
//        Assert.assertEquals(orders.getOrderSn(),previousPddOrders.getOrderSn());
//        Assert.assertEquals(orders.getOrderStatus(),previousPddOrders.getOrderStatus());
//        Assert.assertEquals(orders.getOrderCreateTime(),previousPddOrders.getOrderCreateTime());
//        Assert.assertEquals(orders.getUserId(),previousPddOrders.getUserId());
//        Assert.assertEquals(orders.getGoodsId(),previousPddOrders.getGoodsId());
//        Assert.assertEquals(orders.getGoodsName(),previousPddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(previousPddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",previousPddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",previousPddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",previousPddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders.getUserCommission());
//        Assert.assertEquals(orders.getGoodsQuantity(),previousPddOrders.getGoodsQuantity());
//        Assert.assertTrue(orders.getUserUit().compareTo(previousPddOrders.getUserUit()) == 0);
//        Assert.assertTrue(orders.getPromotionAmount().compareTo(previousPddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders.getPromotionRate().compareTo(previousPddOrders.getPromotionRate()) == 0);
//        Assert.assertTrue(orders.getUserCommission().compareTo(previousPddOrders.getUserCommission()) == 0);
//        Assert.assertTrue(orders.getUserUit().compareTo(previousPddOrders.getUserUit()) == 0);
//    }
//
//    // 开启首单奖励时有用
//    private void compareFirstOrdersIncome(Long userId){
//        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(userId, 10);
//        logger.debug("userIncome is : {}",userIncome);
//        Assert.assertEquals(Long.valueOf(10),userIncome.getUserId());
//        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
//        Assert.assertEquals(Integer.valueOf(2), userIncome.getAwardType());
//        logger.debug("userIncome money is : {}",userIncome.getMoney());
//        Assert.assertTrue(new BigDecimal("0.50").compareTo(userIncome.getMoney()) == 0);
////        User user = userMapper.selectById(10L);
////        Assert.assertEquals(Long.valueOf(230),user.getUserIntgeralTreasure());
//    }
//
//    private PddOrders buildPaidPddOrdersAndUserIncomeInDb(){
//        logger.debug("buildPaidPddOrdersAndUserIncomeInDb");
//        PddOrders orders = buildPddOrders();
//        pddOrdersImport.dealWithOrder(orders);
//
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//
//        User user = userMapper.selectById(orders.getUserId());
//        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
//
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        //获取用户收益记录
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, user.getUserId());
//        compareUserIncome(userIncomeByUserId,10L,new BigDecimal("0.09"),"200425-11111111111111",0L,0);
//        return orders;
//    }
//    // 准备没有上级，上上级的用户订单，和收益记录
//    private PddOrders buildClusteringPddOrdersAndUserIncomeInDb(){
//        logger.debug("buildPaidPddOrdersAndUserIncomeInDb");
//        PddOrders orders = buildPddOrders();
//        pddOrdersImport.dealWithOrder(orders);
//
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//
//        User user = userMapper.selectById(orders.getUserId());
//        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
//
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        //获取用户收益记录
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, user.getUserId());
//        compareUserIncome(userIncomeByUserId,10L,new BigDecimal("0.09"),"200425-11111111111111",0L,0);
//        return orders;
//    }
//
//    // 准备有上级，和收益记录
//    private PddOrders buildHasParentUserPaidPddOrdersAndUserIncomeInDb(){
//
//        logger.debug("buildHasParentUserPaidPddOrdersAndUserIncomeInDb");
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("100"));
//        orders.setOrderAmount(new BigDecimal("100"));
//        orders.setPromotionAmount(new BigDecimal("10"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370512");
//        orders.setOrderSn("200425-2222222222222");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        Assert.assertTrue(orders.getOneLevelCommission().compareTo(pddOrders.getOneLevelCommission()) == 0);
//        Assert.assertTrue(orders.getOneLevelUserUit().compareTo(pddOrders.getOneLevelUserUit()) == 0);
////        Assert.assertTrue(orders.getOneLevelUserUit().compareTo(pddOrders.getOneLevelUserUit());
//        User user = userMapper.selectById(orders.getUserId());
//        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        //获取用户收益记录
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncomeByUserId,12L,new BigDecimal("7.50"),"200425-2222222222222",0L,0);
//
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncomeByUserId1,13L,new BigDecimal("0.40"),"200425-2222222222222",0L,0);
//
//        return orders;
//    }
//
//    // 准备有上级，上上级的用户订单，和收益记录
//    private PddOrders buildSuperiorUserPaidPddOrdersAndUserIncomeInDb(){
//        logger.debug("buildSuperiorUserPaidPddOrdersAndUserIncomeInDb");
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("300"));
//        orders.setOrderAmount(new BigDecimal("300"));
//        orders.setPromotionAmount(new BigDecimal("40"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370511");
//        orders.setOrderSn("200425-333333333333333");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        Assert.assertTrue(orders.getOneLevelCommission().compareTo(pddOrders.getOneLevelCommission()) == 0);
//        Assert.assertTrue(orders.getOneLevelUserUit().compareTo(pddOrders.getOneLevelUserUit()) == 0);
//        Assert.assertTrue(orders.getTwoLevelUserUit().compareTo(pddOrders.getTwoLevelUserUit()) == 0);
//        Assert.assertTrue(orders.getTwoLevelCommission().compareTo(pddOrders.getTwoLevelCommission()) == 0);
//
//        User user = userMapper.selectById(11L);
//        logger.debug("user orders num is : {}",user.getOrdersNum());
//        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        logger.debug("userIncome is : {}",userIncomes);
//        //获取用户收益记录
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes, 11L);
//        compareUserIncome(userIncomeByUserId1,11L,new BigDecimal("30.00"),"200425-333333333333333",0L,0);
//        UserIncome userIncomeByUserId2 = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncomeByUserId2,12L,new BigDecimal("1.60"),"200425-333333333333333",0L,0);
//        UserIncome userIncomeByUserId3 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncomeByUserId3,13L,new BigDecimal("0.40"),"200425-333333333333333",0L,0);
//        return orders;
//    }
//    // 比较没上级用户的订单和收益
//    private void compareOrdersAndUserIncome(PddOrders orders,Integer orderStatus,String describe,Long tkStatus,Integer status){
//        orders.setOrderStatus(orderStatus);
//        orders.setOrderStatusDesc(describe);
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 10L);
//        logger.debug("userIncomeByUserId is : {}",userIncomeByUserId);
//        compareUserIncome(userIncomeByUserId,10L,new BigDecimal("0.09"),"200425-11111111111111",tkStatus,status);
//    }
//    // 比较有上级的用户订单和收益
//    private void compareHasParentUserOrdersAndUserIncome(PddOrders orders,Integer orderStatus,String describe,Long tkStatus,Integer status){
//        orders.setOrderStatus(orderStatus);
//        orders.setOrderStatusDesc(describe);
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        //获取用户收益记录
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncomeByUserId,12L,new BigDecimal("7.50"),"200425-2222222222222",tkStatus,status);
//
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncomeByUserId1,13L,new BigDecimal("0.40"),"200425-2222222222222",tkStatus,status);
//    }
//    // 比较有上上级的用户订单和收益
//    private void compareHasSuperiorUserOrdersAndUserIncome(PddOrders orders,Integer orderStatus,String describe,Long tkStatus,Integer status){
//        orders.setOrderStatus(orderStatus);
//        orders.setOrderStatusDesc(describe);
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        //获取用户收益记录
//        UserIncome userIncome = getUserIncomeByUserId(userIncomes,11L);
//        compareUserIncome(userIncome,11L,new BigDecimal("30"),"200425-333333333333333",tkStatus,status);
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncomeByUserId,12L,new BigDecimal("1.6"),"200425-333333333333333",tkStatus,status);
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncomeByUserId1,13L,new BigDecimal("0.40"),"200425-333333333333333",tkStatus,status);
//    }
//    /**
//     * 没上级
//     * 付款订单
//     */
//    @Test
//    public void processPaidPddOrders(){
//        buildPaidPddOrdersAndUserIncomeInDb();
//
//    }
//
//    /**
//     * 没上级
//     * 付款 -> 审核失败
//     */
//    @Test
//    public void processPaidToOverDuePddOrders(){
//
//        // 准备一笔付款订单在数据库
//        PddOrders orders = buildPaidPddOrdersAndUserIncomeInDb();
////        PddOrders orders = buildPddOrders();
//        orders.setOrderStatus(4);
//        orders.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        Assert.assertEquals(orders.getOrderSn(),pddOrders.getOrderSn());
//        Assert.assertEquals(orders.getOrderStatus(),pddOrders.getOrderStatus());
//        Assert.assertEquals(orders.getOrderCreateTime(),pddOrders.getOrderCreateTime());
//        Assert.assertEquals(orders.getUserId(),pddOrders.getUserId());
//        Assert.assertEquals(orders.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders.getGoodsName(),pddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(pddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",pddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",pddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",pddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders.getUserCommission());
//        Assert.assertEquals(orders.getGoodsQuantity(),pddOrders.getGoodsQuantity());
////        Assert.assertTrue(orders.getUserUit().compareTo(pddOrders.getUserUit()) == 0);
////        Assert.assertTrue(orders.getUserCommission().compareTo(pddOrders.getUserCommission()) == 0);
//        // 直接插入失效订单时不会计算用户的收益与用户的佣金,因此都为空
//        Assert.assertEquals(orders.getUserUit(),pddOrders.getUserUit());
//        Assert.assertEquals(orders.getUserCommission(),pddOrders.getUserCommission());
//        Assert.assertTrue(orders.getPromotionAmount().compareTo(pddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders.getPromotionRate().compareTo(pddOrders.getPromotionRate()) == 0);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        Assert.assertEquals(0,userIncomes.size());
//    }
//
//    /**
//     * 没上级  --> 佣金没冻结
//     * 付款 -> 结算
//     */
//    @Test
//    public void processPaidToAccountPddOrders() {
//
//        PddOrders orders = buildPaidPddOrdersAndUserIncomeInDb();
//        compareOrdersAndUserIncome(orders,5,"结算",5L,1);
//
//        // 校验新人奖励收益记录
////        compareFirstOrdersIncome(orders.getUserId());
//
//        User user = userMapper.selectById(10L);
//        Assert.assertEquals(Long.valueOf(9),user.getUserIntgeralTreasure());
//    }
//
//    /**
//     * 没上级 -- 佣金不冻结
//     * 结算订单
//     */
//    @Test
//    public void processAccountPddOrders() {
//
//        PddOrders orders = buildPddOrders();
//        compareOrdersAndUserIncome(orders,5,"结算",5L,1);
//        User user = userMapper.selectById(10L);
//        Assert.assertEquals(Long.valueOf(9),user.getUserIntgeralTreasure());
//
//        // 校验新人奖励收益记录
//        //compareFirstOrdersIncome(orders.getUserId());
//
//    }
//
//    /**
//     * 没上级
//     * 失效订单
//     */
//    @Test
//    public void processOverDuePddOrders(){
//        PddOrders orders = buildPddOrders();
//        orders.setOrderStatus(4);
//        orders.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        Assert.assertEquals(orders.getOrderSn(),pddOrders.getOrderSn());
//        Assert.assertEquals(orders.getOrderStatus(),pddOrders.getOrderStatus());
//        Assert.assertEquals(orders.getOrderCreateTime(),pddOrders.getOrderCreateTime());
//        Assert.assertEquals(orders.getUserId(),pddOrders.getUserId());
//        Assert.assertEquals(orders.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders.getGoodsName(),pddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(pddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",pddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",pddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",pddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders.getUserCommission());
//        Assert.assertEquals(orders.getGoodsQuantity(),pddOrders.getGoodsQuantity());
//        // 直接导入失效订单不会计算用户收益
//        Assert.assertEquals(orders.getUserUit(),pddOrders.getUserUit());
//        Assert.assertEquals(orders.getUserCommission(),pddOrders.getUserCommission());
////        Assert.assertTrue(orders.getUserUit().compareTo(pddOrders.getUserUit()) == 0);
//        Assert.assertTrue(orders.getPromotionAmount().compareTo(pddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders.getPromotionRate().compareTo(pddOrders.getPromotionRate()) == 0);
////        Assert.assertTrue(orders.getUserCommission().compareTo(pddOrders.getUserCommission()) == 0);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        Assert.assertEquals(0,userIncomes.size());
//
//        PddOrders orders2 = buildPddOrders();
//        orders2.setOrderStatus(4);
//        orders2.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders2);
////        PddOrders pddOrders2 = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        // 再次导入失效订单，与数据库存在的订单作比较
//        Assert.assertEquals(orders2.getOrderSn(),pddOrders.getOrderSn());
//        Assert.assertEquals(orders2.getOrderStatus(),pddOrders.getOrderStatus());
//        Assert.assertEquals(orders2.getOrderCreateTime(),pddOrders.getOrderCreateTime());
//        logger.debug("orders2 goodsId is : {},{}",orders2.getUserId(),pddOrders.getUserId());
//        Assert.assertEquals(orders2.getUserId(),pddOrders.getUserId());
//        logger.debug("orders2 goodsId is : {},{}",orders2.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders2.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders2.getGoodsName(),pddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(pddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders2.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",pddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders2.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",pddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders2.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",pddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders2.getUserCommission());
//        Assert.assertEquals(orders2.getGoodsQuantity(),pddOrders.getGoodsQuantity());
//        // 直接导入失效订单不会计算用户收益
//        Assert.assertEquals(orders2.getUserUit(),pddOrders.getUserUit());
//        Assert.assertEquals(orders2.getUserCommission(),pddOrders.getUserCommission());
////        Assert.assertTrue(orders.getUserUit().compareTo(pddOrders.getUserUit()) == 0);
//        Assert.assertTrue(orders2.getPromotionAmount().compareTo(pddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders2.getPromotionRate().compareTo(pddOrders.getPromotionRate()) == 0);
////        Assert.assertTrue(orders.getUserCommission().compareTo(pddOrders.getUserCommission()) == 0);
//        List<UserIncome> userIncomes2 = userIncomeMapper.selectUserIncomeByOrdersId(orders2.getOrderSn());
//        Assert.assertEquals(0,userIncomes2.size());
//    }
//
//    /**
//     * 没上级
//     * 已支付 -> 已成团
//     */
//    @Test
//    public void processPaidToClustering(){
//        PddOrders orders = buildPaidPddOrdersAndUserIncomeInDb();
//        compareOrdersAndUserIncome(orders,1,"已成团",1L,0);
//    }
//
//    /**
//     * 没上级
//     * 已支付 -> 审核成功
//     */
//    @Test
//    public void processPaidToSuccess(){
//        PddOrders orders = buildPaidPddOrdersAndUserIncomeInDb();
//        compareOrdersAndUserIncome(orders,3,"审核成功",3L,0);
//    }
//
//    /**
//     * 没上级
//     * 已支付 -> 确认收货
//     */
//    @Test
//    public void processPaidToConfirm() {
//        PddOrders orders = buildPaidPddOrdersAndUserIncomeInDb();
//        compareOrdersAndUserIncome(orders,2,"确认收货",2L,0);
//    }
//
//    /**
//     * 没上级
//     * 已成团 -> 审核成功
//     */
//    @Test
//    public void processClusteringToSuccess(){
//
//        PddOrders orders = buildClusteringPddOrdersAndUserIncomeInDb();
//        compareOrdersAndUserIncome(orders,3,"审核成功",3L,0);
//
//    }
//
//    /**
//     * 没上级
//     * 已成团 -> 确认收货
//     */
//    @Test
//    public void processClusteringToConfirm(){
//
//        PddOrders orders = buildClusteringPddOrdersAndUserIncomeInDb();
//        compareOrdersAndUserIncome(orders,2,"确认收货",2L,0);
//
//    }
//
//    /**
//     * 没上级
//     * 已成团 -> 结算
//     */
//    @Test
//    public void processClusteringToAccountPddOrders(){
//        PddOrders orders = buildClusteringPddOrdersAndUserIncomeInDb();
//        compareOrdersAndUserIncome(orders,5,"结算",5L,1);
//
//        // 校验新人奖励收益记录
//        //compareFirstOrdersIncome(orders.getUserId());
//    }
//
//    /**
//     *  有上级
//     *  付款
//     */
//    @Test
//    public void processHasParentUserPaidPddOrders(){
//        buildHasParentUserPaidPddOrdersAndUserIncomeInDb();
//    }
//
//    /**
//     * 有上级
//     * 付款 -> 失效
//     */
//    @Test
//    public void processHasParentUserToOverDuePddOrders(){
//        PddOrders orders = buildHasParentUserPaidPddOrdersAndUserIncomeInDb();
////        compareHasParentUserOrdersAndUserIncome(orders,4,"审核失败",);
//        orders.setOrderStatus(4);
//        orders.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        Assert.assertEquals(0,userIncomes.size());
//    }
//
//    /**
//     * 有上级  --> 佣金没冻结
//     * 付款 -> 结算
//     */
//    @Test
//    public void processHasParentPaidToAccountPddOrders(){
//        PddOrders orders = buildHasParentUserPaidPddOrdersAndUserIncomeInDb();
//        compareHasParentUserOrdersAndUserIncome(orders,5,"结算",5L,1);
//
//        User user1 = userMapper.selectById(12L);
//        Assert.assertEquals(Long.valueOf(750),user1.getUserIntgeralTreasure());
//        User user2 = userMapper.selectById(13L);
//        // 拉新10   订单收益40
//        Assert.assertEquals(Long.valueOf(50),user2.getUserIntgeralTreasure());
//
//        // 校验拉新收益记录
//        UserIncome userIncomeByTypeAndUserId = userIncomeMapper.getUserIncomeByTypeAndUserId(13L);
//        logger.debug("userIncomeByTypeAndUserId is : {}",userIncomeByTypeAndUserId);
//        compareUserIncome(userIncomeByTypeAndUserId,13L,new BigDecimal("0.1"),"200425-2222222222222",5L,1);
//
//        // 校验新人奖励收益记录
//        //compareFirstOrdersIncome(orders.getUserId());
//    }
//
//    /**
//     * 有上级 -- 佣金不冻结
//     * 结算订单
//     */
//    @Test
//    public void processHasParentAccountPddOrders(){
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("100"));
//        orders.setOrderAmount(new BigDecimal("100"));
//        orders.setPromotionAmount(new BigDecimal("10"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370512");
//        orders.setOrderSn("200425-2222222222222");
//        orders.setOrderStatus(5);
//        orders.setOrderStatusDesc("结算");
//        pddOrdersImport.dealWithOrder(orders);
//        User user = userMapper.selectById(orders.getUserId());
//        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
//
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        // 获取用户收益记录
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncomeByUserId,12L,new BigDecimal("7.50"),"200425-2222222222222",5L,1);
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncomeByUserId1,13L,new BigDecimal("0.40"),"200425-2222222222222",5L,1);
//        // 校验拉新收益记录
//        UserIncome userIncomeByTypeAndUserId = userIncomeMapper.getUserIncomeByTypeAndUserId(13L);
//        logger.debug("userIncomeByTypeAndUserId is : {}",userIncomeByTypeAndUserId);
//        compareUserIncome(userIncomeByTypeAndUserId,13L,new BigDecimal("0.1"),"200425-2222222222222",5L,1);
//
//        User user1 = userMapper.selectById(12L);
//        Assert.assertEquals(Long.valueOf(750),user1.getUserIntgeralTreasure());
//        User user2 = userMapper.selectById(13L);
//        // 拉新10   订单收益40
//        Assert.assertEquals(Long.valueOf(50),user2.getUserIntgeralTreasure());
//
//        // 校验新人奖励收益记录
//        //compareFirstOrdersIncome(orders.getUserId());
//
//    }
//
//    /**
//     * 有上级
//     * 失效订单
//     */
//    @Test
//    public void processHasParentOverDuePddOrders(){
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("100"));
//        orders.setOrderAmount(new BigDecimal("100"));
//        orders.setPromotionAmount(new BigDecimal("10"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370512");
//        orders.setOrderSn("200425-2222222222222");
//        orders.setOrderStatus(4);
//        orders.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn("200425-2222222222222");
//        Assert.assertEquals(orders.getOrderSn(),pddOrders.getOrderSn());
//        Assert.assertEquals(orders.getOrderStatus(),pddOrders.getOrderStatus());
//        Assert.assertEquals(orders.getOrderCreateTime(),pddOrders.getOrderCreateTime());
//        Assert.assertEquals(orders.getUserId(),pddOrders.getUserId());
//        Assert.assertEquals(orders.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders.getGoodsName(),pddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(pddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",pddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",pddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",pddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders.getUserCommission());
//        Assert.assertEquals(orders.getGoodsQuantity(),pddOrders.getGoodsQuantity());
//        // 直接导入失效订单不会计算用户收益
//        Assert.assertEquals(orders.getUserUit(),pddOrders.getUserUit());
//        Assert.assertEquals(orders.getUserCommission(),pddOrders.getUserCommission());
////        Assert.assertTrue(orders.getUserUit().compareTo(pddOrders.getUserUit()) == 0);
//        Assert.assertTrue(orders.getPromotionAmount().compareTo(pddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders.getPromotionRate().compareTo(pddOrders.getPromotionRate()) == 0);
////        Assert.assertTrue(orders.getUserCommission().compareTo(pddOrders.getUserCommission()) == 0);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("200425-2222222222222");
//        Assert.assertEquals(0,userIncomes.size());
//
//        PddOrders orders2 = buildPddOrders();
//        orders2.setGoodsPrice(new BigDecimal("100"));
//        orders2.setOrderAmount(new BigDecimal("100"));
//        orders2.setPromotionAmount(new BigDecimal("10"));
//        orders2.setPromotionRate(20L);
//        orders2.setpId("10390596_140370512");
//        orders2.setOrderSn("200425-2222222222222");
//        orders2.setOrderStatus(4);
//        orders2.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders2);
////        PddOrders pddOrders2 = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        // 再次导入失效订单，与数据库存在的订单作比较
//        Assert.assertEquals(orders2.getOrderSn(),pddOrders.getOrderSn());
//        Assert.assertEquals(orders2.getOrderStatus(),pddOrders.getOrderStatus());
//        Assert.assertEquals(orders2.getOrderCreateTime(),pddOrders.getOrderCreateTime());
//        logger.debug("orders2 goodsId is : {},{}",orders2.getUserId(),pddOrders.getUserId());
//        Assert.assertEquals(orders2.getUserId(),pddOrders.getUserId());
//        logger.debug("orders2 goodsId is : {},{}",orders2.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders2.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders2.getGoodsName(),pddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(pddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders2.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",pddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders2.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",pddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders2.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",pddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders2.getUserCommission());
//        Assert.assertEquals(orders2.getGoodsQuantity(),pddOrders.getGoodsQuantity());
//        // 直接导入失效订单不会计算用户收益
//        Assert.assertEquals(orders2.getUserUit(),pddOrders.getUserUit());
//        Assert.assertEquals(orders2.getUserCommission(),pddOrders.getUserCommission());
////        Assert.assertTrue(orders.getUserUit().compareTo(pddOrders.getUserUit()) == 0);
//        Assert.assertTrue(orders2.getPromotionAmount().compareTo(pddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders2.getPromotionRate().compareTo(pddOrders.getPromotionRate()) == 0);
////        Assert.assertTrue(orders.getUserCommission().compareTo(pddOrders.getUserCommission()) == 0);
//        List<UserIncome> userIncomes2 = userIncomeMapper.selectUserIncomeByOrdersId(orders2.getOrderSn());
//        Assert.assertEquals(0,userIncomes2.size());
//    }
//
//    /**
//     * 没上级
//     * 已支付 -> 已成团
//     */
//    @Test
//    public void processHasPaidToClustering(){
//        PddOrders orders = buildHasParentUserPaidPddOrdersAndUserIncomeInDb();
//        compareHasParentUserOrdersAndUserIncome(orders,1,"已成团",1L,0);
//    }
//
//    /**
//     * 有上级
//     * 已支付 -> 审核成功
//     */
//    @Test
//    public void processHasParentPaidToSuccess(){
//        PddOrders orders = buildHasParentUserPaidPddOrdersAndUserIncomeInDb();
//        compareHasParentUserOrdersAndUserIncome(orders,3,"审核成功",3L,0);
//    }
//
//    /**
//     * 有上级
//     * 已支付 -> 确认收货
//     */
//    @Test
//    public void processHasPaidToConfirm(){
//        PddOrders orders = buildHasParentUserPaidPddOrdersAndUserIncomeInDb();
//        compareHasParentUserOrdersAndUserIncome(orders,2,"审核成功",2L,0);
//    }
//
//    /**
//     * 有上级，上上级别
//     * 付款订单
//     */
//    @Test
//    public void processHasSuperiorPaidPddOrders(){
//        buildSuperiorUserPaidPddOrdersAndUserIncomeInDb();
//    }
//
//    /**
//     * 有上级，上上级
//     * 付款 -> 审核失败
//     */
//    @Test
//    public void processHasSuperiorPaidToOverDuePddOrders(){
//        PddOrders orders = buildSuperiorUserPaidPddOrdersAndUserIncomeInDb();
//        orders.setOrderStatus(4);
//        orders.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        Assert.assertEquals(0,userIncomes.size());
//    }
//
//    /**
//     * 有上级，上上级
//     * 付款 -> 结算
//     */
//    @Test
//    public void processHasSuperiorPaidToAccountPddOrders(){
//        PddOrders orders = buildSuperiorUserPaidPddOrdersAndUserIncomeInDb();
//
//        orders.setOrderStatus(5);
//        orders.setOrderStatusDesc("结算");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        // 获取用户收益记录，冻结状态
//        UserIncome userIncome = getUserIncomeByUserId(userIncomes,11L);
//        compareUserIncome(userIncome,11L,new BigDecimal("30"),"200425-333333333333333",5L,2);
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncomeByUserId,12L,new BigDecimal("1.6"),"200425-333333333333333",5L,2);
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncomeByUserId1,13L,new BigDecimal("0.40"),"200425-333333333333333",5L,2);
//
//        // 校验拉新收益记录
//        UserIncome userIncomeByTypeAndUserId = userIncomeMapper.getUserIncomeByTypeAndUserId(12L);
//        logger.debug("userIncomeByTypeAndUserId is : {}",userIncomeByTypeAndUserId);
//        compareUserIncome(userIncomeByTypeAndUserId,12L,new BigDecimal("0.1"),"200425-333333333333333",5L,1);
//
//        // 校验新人奖励收益记录
//        //compareFirstOrdersIncome(orders.getUserId());
//    }
//
//    /**
//     * 有上级，上上级  -- 佣金为冻结状态
//     * 结算订单
//     */
//    @Test
//    public void processHasSuperiorAccountPddOrders(){
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("300"));
//        orders.setOrderAmount(new BigDecimal("300"));
//        orders.setPromotionAmount(new BigDecimal("40"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370511");
//        orders.setOrderSn("200425-333333333333333");
//        orders.setOrderStatus(5);
//        orders.setOrderStatusDesc("结算");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        comparePddOrders(orders,pddOrders);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(orders.getOrderSn());
//        // 获取用户收益记录，冻结状态
//        UserIncome userIncome = getUserIncomeByUserId(userIncomes,11L);
//        compareUserIncome(userIncome,11L,new BigDecimal("30"),"200425-333333333333333",5L,2);
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncomeByUserId,12L,new BigDecimal("1.6"),"200425-333333333333333",5L,2);
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncomeByUserId1,13L,new BigDecimal("0.40"),"200425-333333333333333",5L,2);
//        // 校验拉新收益记录
//        UserIncome userIncomeByTypeAndUserId = userIncomeMapper.getUserIncomeByTypeAndUserId(12L);
//        logger.debug("userIncomeByTypeAndUserId is : {}",userIncomeByTypeAndUserId);
//        compareUserIncome(userIncomeByTypeAndUserId,12L,new BigDecimal("0.1"),"200425-333333333333333",5L,1);
//
//        // 校验新人奖励收益记录
//        //compareFirstOrdersIncome(orders.getUserId());
//    }
//
//    /**
//     * 有上级，上上级
//     * 失效
//     */
//    @Test
//    public void processHasSuperiorOverDuePddOrders(){
//
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("300"));
//        orders.setOrderAmount(new BigDecimal("300"));
//        orders.setPromotionAmount(new BigDecimal("40"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370511");
//        orders.setOrderSn("200425-333333333333333");
//        orders.setOrderStatus(4);
//        orders.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders);
//        PddOrders pddOrders = pddOrdersMapper.selectPddOrdersByOrderSn("200425-333333333333333");
//        Assert.assertEquals(orders.getOrderSn(),pddOrders.getOrderSn());
//        Assert.assertEquals(orders.getOrderStatus(),pddOrders.getOrderStatus());
//        Assert.assertEquals(orders.getOrderCreateTime(),pddOrders.getOrderCreateTime());
//        Assert.assertEquals(orders.getUserId(),pddOrders.getUserId());
//        Assert.assertEquals(orders.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders.getGoodsName(),pddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(pddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",pddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",pddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",pddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders.getUserCommission());
//        Assert.assertEquals(orders.getGoodsQuantity(),pddOrders.getGoodsQuantity());
//        // 直接导入失效订单不会计算用户收益
//        Assert.assertEquals(orders.getUserUit(),pddOrders.getUserUit());
//        Assert.assertEquals(orders.getUserCommission(),pddOrders.getUserCommission());
////        Assert.assertTrue(orders.getUserUit().compareTo(pddOrders.getUserUit()) == 0);
//        Assert.assertTrue(orders.getPromotionAmount().compareTo(pddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders.getPromotionRate().compareTo(pddOrders.getPromotionRate()) == 0);
////        Assert.assertTrue(orders.getUserCommission().compareTo(pddOrders.getUserCommission()) == 0);
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("200425-333333333333333");
//        Assert.assertEquals(0,userIncomes.size());
//
//        PddOrders orders2 = buildPddOrders();
//        orders2.setGoodsPrice(new BigDecimal("300"));
//        orders2.setOrderAmount(new BigDecimal("300"));
//        orders2.setPromotionAmount(new BigDecimal("40"));
//        orders2.setPromotionRate(20L);
//        orders2.setpId("10390596_140370511");
//        orders2.setOrderSn("200425-333333333333333");
//        orders2.setOrderStatus(4);
//        orders2.setOrderStatusDesc("审核失败");
//        pddOrdersImport.dealWithOrder(orders2);
////        PddOrders pddOrders2 = pddOrdersMapper.selectPddOrdersByOrderSn(orders.getOrderSn());
//        // 再次导入失效订单，与数据库存在的订单作比较
//        Assert.assertEquals(orders2.getOrderSn(),pddOrders.getOrderSn());
//        Assert.assertEquals(orders2.getOrderStatus(),pddOrders.getOrderStatus());
//        Assert.assertEquals(orders2.getOrderCreateTime(),pddOrders.getOrderCreateTime());
//        logger.debug("orders2 goodsId is : {},{}",orders2.getUserId(),pddOrders.getUserId());
//        Assert.assertEquals(orders2.getUserId(),pddOrders.getUserId());
//        logger.debug("orders2 goodsId is : {},{}",orders2.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders2.getGoodsId(),pddOrders.getGoodsId());
//        Assert.assertEquals(orders2.getGoodsName(),pddOrders.getGoodsName());
//        Assert.assertTrue(orders.getGoodsPrice().compareTo(pddOrders.getGoodsPrice()) == 0);
//        logger.debug("ordersUserUit is : {}",orders2.getUserUit());
//        logger.debug("pddOrdersUserUit is : {}",pddOrders.getUserUit());
//        logger.debug("getPromotionAmount is : {}",orders2.getPromotionAmount());
//        logger.debug("getPromotionAmount is : {}",pddOrders.getPromotionAmount());
//        logger.debug("getPromotionRate is : {}",orders2.getPromotionRate());
//        logger.debug("getPromotionRate is : {}",pddOrders.getPromotionRate());
//        logger.debug("getUserCommission is : {}",orders2.getUserCommission());
//        Assert.assertEquals(orders2.getGoodsQuantity(),pddOrders.getGoodsQuantity());
//        // 直接导入失效订单不会计算用户收益
//        Assert.assertEquals(orders2.getUserUit(),pddOrders.getUserUit());
//        Assert.assertEquals(orders2.getUserCommission(),pddOrders.getUserCommission());
////        Assert.assertTrue(orders.getUserUit().compareTo(pddOrders.getUserUit()) == 0);
//        Assert.assertTrue(orders2.getPromotionAmount().compareTo(pddOrders.getPromotionAmount()) == 0);
//        Assert.assertTrue(orders2.getPromotionRate().compareTo(pddOrders.getPromotionRate()) == 0);
////        Assert.assertTrue(orders.getUserCommission().compareTo(pddOrders.getUserCommission()) == 0);
//        List<UserIncome> userIncomes2 = userIncomeMapper.selectUserIncomeByOrdersId(orders2.getOrderSn());
//        Assert.assertEquals(0,userIncomes2.size());
//    }
//
//    /**
//     * 有上级，上上级
//     * 已支付 -> 已成团
//     */
//    @Test
//    public void processHasSuperiorPaidToClustering(){
//        PddOrders orders = buildSuperiorUserPaidPddOrdersAndUserIncomeInDb();
//        compareHasSuperiorUserOrdersAndUserIncome(orders,1,"已成团",1L,0);
//    }
//
//    /**
//     * 有上级，上上级
//     * 已支付 -> 确认收货
//     */
//    @Test
//    public void processHasSuperiorPaidToConfirm(){
//        PddOrders orders = buildSuperiorUserPaidPddOrdersAndUserIncomeInDb();
//        compareHasSuperiorUserOrdersAndUserIncome(orders,2,"确认收货",2L,0);
//    }
//
//    /**
//     * 有上级，上上级
//     * 已支付 -> 审核成功
//     */
//    @Test
//    public void processHasSuperiorPaidToSuccess(){
//        PddOrders orders = buildSuperiorUserPaidPddOrdersAndUserIncomeInDb();
//        compareHasSuperiorUserOrdersAndUserIncome(orders,3,"审核成功",3L,0);
//    }
//
//    /**
//     * 0元购活动订单,活动商品不存在
//     * 结算状态下才会判断是否冻结
//     * 按照常规订单处理
//     */
//    @Test
//    public void processActivityOrders(){
//
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("300"));
//        orders.setOrderAmount(new BigDecimal("300"));
//        orders.setPromotionAmount(new BigDecimal("20"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370511");
//        orders.setOrderSn("200425-333333333333333");
//        orders.setCustomParameters("1234");
//        pddOrdersImport.dealWithOrder(orders);
//
//        User user = userMapper.selectById(11L);
//        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("200425-333333333333333");
//        logger.debug("userIncomes is : {}",userIncomes);
//        Assert.assertTrue(Integer.valueOf(3).compareTo(userIncomes.size()) == 0);
//        UserIncome userIncome1 = getUserIncomeByUserId(userIncomes, 11L);
//        logger.debug("userIncomeByUserId is : {}",userIncome1);
//        compareUserIncome(userIncome1,11L,new BigDecimal("15.00"),"200425-333333333333333",0L,0);
//        UserIncome userIncome2 = getUserIncomeByUserId(userIncomes, 12L);
//        compareUserIncome(userIncome2,12L,new BigDecimal("0.80"),"200425-333333333333333",0L,0);
//        UserIncome userIncome3 = getUserIncomeByUserId(userIncomes, 13L);
//        compareUserIncome(userIncome3,13L,new BigDecimal("0.20"),"200425-333333333333333",0L,0);
//    }
//
//    /**
//     * 0元购活动订单,有活动商品
//     */
//    @Test
//    public void processHasActivityProduct(){
//        ActivityProduct activityProduct = buildActivityProduct(888, "185928347", 1);
//        activityProductMapper.insert(activityProduct);
//        ActivityProduct activityProduct1 = activityProductMapper
//                .selectProductByActivityAndProductId(1, "185928347");
//        logger.debug("activityProduct1 is : {}",activityProduct1);
//        PddOrders orders = buildPddOrders();
//        orders.setGoodsPrice(new BigDecimal("300"));
//        orders.setOrderAmount(new BigDecimal("300"));
//        orders.setPromotionAmount(new BigDecimal("20"));
//        orders.setPromotionRate(20L);
//        orders.setpId("10390596_140370511");
//        orders.setOrderSn("200425-333333333333333");
//        orders.setCustomParameters("activityOrders");
//        pddOrdersImport.dealWithOrder(orders);
//
//        User user = userMapper.selectById(11L);
//        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
//        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("200425-333333333333333");
//        logger.debug("userIncomes is : {}",userIncomes);
//        Assert.assertTrue(Integer.valueOf(1).compareTo(userIncomes.size()) == 0);
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 11L);
//        compareUserIncome(userIncomeByUserId,11L,new BigDecimal("9.98"),"200425-333333333333333",0L,0);
//    }
//
//



   @Test
    public void importPddOrdersTest() throws IOException, ParseException {
        logger.debug("import PddOrders start");
        long end = System.currentTimeMillis();
        long time = end/1000;
        pddOrdersImport.importOrders(time-86400,time,null,null);
    }
}
