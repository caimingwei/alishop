package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.JdOrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserJdPidMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.JdOrders;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.model.UserJdPid;
import com.alipay.rebate.shop.scheduler.orderimport.JdOrdersImport;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class JdOrdersImportTest {

    @Resource
    JdOrdersImport jdOrdersImport;
    @Resource
    UserMapper userMapper;
    @Resource
    UserJdPidMapper userJdPidMapper;
    @Resource
    UserIncomeMapper userIncomeMapper;
    @Resource
    JdOrdersMapper jdOrdersMapper;
    private Logger logger = LoggerFactory.getLogger(JdOrdersImportTest.class);

    @Before
    public void before(){

        userMapper.deleteUserById(10L);
        userMapper.deleteUserById(11L);
        userMapper.deleteUserById(12L);
        userMapper.deleteUserById(13L);
        userMapper.deleteUserById(14L);
        userJdPidMapper.deleteUserJdPidByUserId(10L);
        userJdPidMapper.deleteUserJdPidByUserId(11L);
        userJdPidMapper.deleteUserJdPidByUserId(12L);
        userJdPidMapper.deleteUserJdPidByUserId(13L);
        userJdPidMapper.deleteUserJdPidByUserId(14L);
        userIncomeMapper.deleteUserIncomeByUserId(10L);
        userIncomeMapper.deleteUserIncomeByUserId(11L);
        userIncomeMapper.deleteUserIncomeByUserId(12L);
        userIncomeMapper.deleteUserIncomeByUserId(13L);
        userIncomeMapper.deleteUserIncomeByUserId(14L);
        userIncomeMapper.deleteByTradeId("20298345177250");
        userIncomeMapper.deleteByTradeId("202983451222222");
        userIncomeMapper.deleteByTradeId("2029834513333333");
        jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177250L);
        jdOrdersMapper.deleteJdOrdersByOrdersId(202983451222222L);
        jdOrdersMapper.deleteJdOrdersByOrdersId(2029834513333333L);

        // 插入用户
        User user1 = buildUser(10L,"测试1",null,null,"13418636932");
        userMapper.insertSelective(user1);
        // 有上级，上上级
        User user2 = buildUser(11L,"测试2",12L,"测试3","13418638888");
        userMapper.insertSelective(user2);
        // 有上级
        User user3 = buildUser(12L,"测试3",13L,"测试4","13418636666");
        userMapper.insertSelective(user3);
        // 有下级，下下级
        User user4 = buildUser(13L,"测试4",null,null,"13418632222");
        userMapper.insertSelective(user4);

        // 为用户准备京东pid
        UserJdPid userJdPid1 = buildUserJdPid(1L, 10L, "200000555550");
        userJdPidMapper.insertSelective(userJdPid1);
        UserJdPid userJdPid2 = buildUserJdPid(2L, 11L, "200000555551");
        userJdPidMapper.insertSelective(userJdPid2);
        UserJdPid userJdPid3 = buildUserJdPid(3L, 12L, "200000555552");
        userJdPidMapper.insertSelective(userJdPid3);
        UserJdPid userJdPid4 = buildUserJdPid(4L, 13L, "200000555553");
        userJdPidMapper.insertSelective(userJdPid4);

    }

    @After
    public void after(){
        userMapper.deleteUserById(10L);
        userMapper.deleteUserById(11L);
        userMapper.deleteUserById(12L);
        userMapper.deleteUserById(13L);
        userMapper.deleteUserById(14L);
        userJdPidMapper.deleteUserJdPidByUserId(10L);
        userJdPidMapper.deleteUserJdPidByUserId(11L);
        userJdPidMapper.deleteUserJdPidByUserId(12L);
        userJdPidMapper.deleteUserJdPidByUserId(13L);
        userJdPidMapper.deleteUserJdPidByUserId(14L);
        userIncomeMapper.deleteUserIncomeByUserId(10L);
        userIncomeMapper.deleteUserIncomeByUserId(11L);
        userIncomeMapper.deleteUserIncomeByUserId(12L);
        userIncomeMapper.deleteUserIncomeByUserId(13L);
        userIncomeMapper.deleteUserIncomeByUserId(14L);
        userIncomeMapper.deleteByTradeId("20298345177250");
        userIncomeMapper.deleteByTradeId("202983451222222");
        userIncomeMapper.deleteByTradeId("2029834513333333");
        jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177250L);
        jdOrdersMapper.deleteJdOrdersByOrdersId(202983451222222L);
        jdOrdersMapper.deleteJdOrdersByOrdersId(2029834513333333L);
    }

    private UserIncome getUserIncomeByUserId(List<UserIncome> userIncomeList,Long userId){
        logger.debug("getUserIncomeByUserId");
        for (UserIncome income : userIncomeList) {
            if (income.getUserId().equals(userId)){
                return income;
            }
        }
        return null;
    }

    private void compareUserIncome(UserIncome userIncome,Long userId, BigDecimal money,
                                   String tradeId,
                                   Long tkStatus, Integer status){
        logger.debug("compareUserIncome");
        logger.debug("userId is : {},{}",userId,userIncome.getUserId());
        Assert.assertEquals(userId,userIncome.getUserId());
        logger.debug("userIncome money is : {}",userIncome.getMoney());
        logger.debug("money is : {}",money);
        Assert.assertTrue(money.compareTo(userIncome.getMoney()) == 0);
        Assert.assertEquals(tradeId,userIncome.getTradeId());
        Assert.assertEquals(tkStatus,userIncome.getTkStatus());
//        Assert.assertEquals(status,userIncome.getStatus());
    }

    private void compareJdOrders(JdOrders jdOrders,JdOrders previousJdOrders){
        Assert.assertTrue(jdOrders.getUserUit().compareTo(previousJdOrders.getUserUit()) == 0);
        Assert.assertTrue(jdOrders.getUserCommission().compareTo(previousJdOrders.getUserCommission()) == 0);
        Assert.assertEquals(jdOrders.getUserName(),previousJdOrders.getUserName());
        Assert.assertEquals(jdOrders.getUserId(),previousJdOrders.getUserId());
        Assert.assertEquals(jdOrders.getValidCode(),previousJdOrders.getValidCode());
        Assert.assertEquals(jdOrders.getFrozenSkuNum(),previousJdOrders.getFrozenSkuNum());
        Assert.assertEquals(jdOrders.getUnionTrafficGroup(),previousJdOrders.getUnionTrafficGroup());
        Assert.assertEquals(jdOrders.getUnionTag(),previousJdOrders.getUnionTag());
        Assert.assertEquals(jdOrders.getUnionRole(),previousJdOrders.getUnionRole());
        Assert.assertEquals(jdOrders.getUnionId(),previousJdOrders.getUnionId());
        Assert.assertEquals(jdOrders.getUnionAlias(),previousJdOrders.getUnionAlias());
        Assert.assertEquals(jdOrders.getTraceType(),previousJdOrders.getTraceType());
        Assert.assertEquals(jdOrders.getSubUnionId(),previousJdOrders.getSubUnionId());
        Assert.assertEquals(jdOrders.getSkuReturnNum(),previousJdOrders.getSkuReturnNum());
        Assert.assertEquals(jdOrders.getSkuNum(),previousJdOrders.getSkuNum());
        Assert.assertEquals(jdOrders.getSkuName(),previousJdOrders.getSkuName());
        Assert.assertEquals(jdOrders.getSkuinfoPayMonth(),previousJdOrders.getSkuinfoPayMonth());
        Assert.assertTrue(jdOrders.getSubsidyRate().compareTo(previousJdOrders.getSubsidyRate()) == 0);
        Assert.assertTrue(jdOrders.getSkuinfoValidCode().compareTo(previousJdOrders.getSkuinfoValidCode()) == 0);
        Assert.assertTrue(jdOrders.getSubSideRate().compareTo(previousJdOrders.getSubSideRate()) == 0);
        Assert.assertTrue(jdOrders.getPrice().compareTo(previousJdOrders.getPrice()) == 0);
        Assert.assertTrue(jdOrders.getPositionId().compareTo(previousJdOrders.getPositionId()) == 0);
        Assert.assertEquals(jdOrders.getOrderId(),previousJdOrders.getOrderId());
        Assert.assertTrue(jdOrders.getFinalRate().compareTo(previousJdOrders.getFinalRate()) == 0);
        Assert.assertTrue(jdOrders.getEstimateFee().compareTo(previousJdOrders.getEstimateFee()) == 0);
        Assert.assertTrue(jdOrders.getEstimateCosPrice().compareTo(previousJdOrders.getEstimateCosPrice()) == 0);
        Assert.assertTrue(jdOrders.getCommissionRate().compareTo(previousJdOrders.getCommissionRate())==0);
        Assert.assertTrue(jdOrders.getActualFee().compareTo(previousJdOrders.getActualFee()) == 0);
        Assert.assertTrue(jdOrders.getActualCosPrice().compareTo(previousJdOrders.getActualCosPrice()) == 0);

    }

    private void compareJdOrdersAndUserIncome(JdOrders orders,Integer validCode,Long tkStatus,Integer status){
        orders.setValidCode(validCode);
        jdOrdersImport.dealWithJdOrders(orders);
        JdOrders preJdOrders = jdOrdersMapper.selectOrderByOrderId(20298345177250L);
        compareJdOrders(orders,preJdOrders);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("20298345177250");
        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 10L);
        logger.debug("userIncomeByUserId is : {}",userIncomeByUserId);
        compareUserIncome(userIncomeByUserId,10L,new BigDecimal("750.00"),"20298345177250",tkStatus,status);
    }

    private User buildUser(Long userId,String userName,Long parentUserId,String parentUserName,String phone){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName(userName);
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(null);
        user.setSpecialId(null);
        user.setParentUserId(null);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(null);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setParentUserId(parentUserId);
        user.setParentUserName(parentUserName);
        return user;
    }

    private UserJdPid buildUserJdPid(Long id,Long userId,String jdPid){
        UserJdPid userJdPid = new UserJdPid();
        userJdPid.setId(id);
        userJdPid.setAddTime(new Date());
        userJdPid.setUpdateTime(new Date());
        userJdPid.setUserId(userId);
        userJdPid.setPositionId(jdPid);
        return userJdPid;
    }

    private JdOrders buildJdOrders(){
        JdOrders jdOrders = new JdOrders();
        jdOrders.setFinishTime("2020-05-20 09:34:45");
        jdOrders.setOrderEmt(2);
        jdOrders.setOrderId(20298345177250L);
        jdOrders.setOrderTime("2020-05-17 08:45:50");
        jdOrders.setParentId(0l);
        jdOrders.setPayMonth("20191018");
        jdOrders.setPlus(0);
        jdOrders.setParentId(0l);
        jdOrders.setActualCosPrice(new BigDecimal("23"));
        jdOrders.setActualFee(new BigDecimal("2.28"));
        jdOrders.setCid1(1672l);
        jdOrders.setCid2(2599l);
        jdOrders.setCid3(13667l);
        jdOrders.setCommissionRate(new BigDecimal("11"));
        jdOrders.setCpActid(0l);
        jdOrders.setEstimateFee(new BigDecimal("2.28"));
        jdOrders.setEstimateCosPrice(new BigDecimal("23"));
        jdOrders.setFinalRate(new BigDecimal("90"));
        jdOrders.setPid("200000555550");
        jdOrders.setPositionId(200000555550L);
        jdOrders.setPopId(789438L);
        jdOrders.setPrice(new BigDecimal("23"));
        jdOrders.setSiteId(27916941095l);
        jdOrders.setSkuName("沉香倒流香佛系流云檀香香粒高山流水瀑布塔香卧室熏香艾草倒流香炉创意摆件沉香炉倒流 香薰 樱花 玻璃瓶装 送小荷花");
        jdOrders.setSkuNum(1l);
        jdOrders.setSkuReturnNum(0l);
        jdOrders.setSubSideRate(new BigDecimal("90"));
        jdOrders.setSubsidyRate(new BigDecimal("0"));
        jdOrders.setTraceType(2);
        jdOrders.setUnionRole(1);
        jdOrders.setUnionTrafficGroup(4);
        jdOrders.setUnionTag("00000100");
        jdOrders.setValidCode(16);
        jdOrders.setUnionId(1000521676L);
        jdOrders.setSkuinfoValidCode(16);
        return jdOrders;
    }

    // 准备没上级处理过的付款订单在数据库
    private JdOrders buildNoParentPaidJdOrdersInDb(){
        JdOrders jdOrders = buildJdOrders();
        jdOrdersImport.dealWithJdOrders(jdOrders);
        User user = userMapper.selectById(10L);
        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
        JdOrders selectOrderByOrderId = jdOrdersMapper.selectOrderByOrderId(20298345177250L);
        compareJdOrders(jdOrders,selectOrderByOrderId);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("20298345177250");
        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 10L);
        compareUserIncome(userIncomeByUserId,10L,new BigDecimal("1.93"),"20298345177250",16L,0);
        return jdOrders;
    }

    // 准备有上级处理过的付款订单在数据库
    private JdOrders buildHasParentPaidJdOrdersInDb(){
        logger.debug("buildHasParentPaidJdOrdersInDb");
        JdOrders jdOrders = buildJdOrders();
        jdOrders.setPid("200000555551");
        jdOrders.setPositionId(200000555552L);
        jdOrders.setOrderId(202983451222222L);
        jdOrders.setPrice(new BigDecimal("60.00"));
        jdOrders.setEstimateCosPrice(new BigDecimal("61.8"));
        jdOrders.setEstimateFee(new BigDecimal("12.0"));
        jdOrdersImport.dealWithJdOrders(jdOrders);
        User user = userMapper.selectById(12L);
        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
        JdOrders selectOrderByOrderId = jdOrdersMapper.selectOrderByOrderId(202983451222222L);
        compareJdOrders(jdOrders,selectOrderByOrderId);
        Assert.assertTrue(jdOrders.getOneLevelCommission().compareTo(selectOrderByOrderId.getOneLevelCommission()) == 0);
        Assert.assertTrue(jdOrders.getOneLevelUserUit().compareTo(selectOrderByOrderId.getOneLevelUserUit()) == 0);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("202983451222222");
        logger.debug("buildHasParentPaidJdOrdersInDb userIncomes is : {}",userIncomes);
        UserIncome userIncome1 = getUserIncomeByUserId(userIncomes, 12L);
        compareUserIncome(userIncome1,12L,new BigDecimal("9.48"),"202983451222222",16L,0);
        UserIncome userIncome2= getUserIncomeByUserId(userIncomes, 13L);
        compareUserIncome(userIncome2,13L,new BigDecimal("0.48"),"202983451222222",16L,0);
        return jdOrders;
    }

    private void compareHasParentJdOrdersAndUserIncome(JdOrders orders,Integer validCode,Long tkStatus,Integer status){
        orders.setValidCode(validCode);
        jdOrdersImport.dealWithJdOrders(orders);
        JdOrders preJdOrders = jdOrdersMapper.selectOrderByOrderId(202983451222222L);
        compareJdOrders(orders,preJdOrders);
        Assert.assertTrue(orders.getOneLevelCommission().compareTo(preJdOrders.getOneLevelCommission()) == 0);
        Assert.assertTrue(orders.getOneLevelUserUit().compareTo(preJdOrders.getOneLevelUserUit()) == 0);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("202983451222222");
        logger.debug("buildHasParentPaidJdOrdersInDb userIncomes is : {}",userIncomes);
        UserIncome userIncome1 = getUserIncomeByUserId(userIncomes, 12L);
        compareUserIncome(userIncome1,12L,new BigDecimal("9.48"),"202983451222222",tkStatus,status);
        UserIncome userIncome2= getUserIncomeByUserId(userIncomes, 13L);
        compareUserIncome(userIncome2,13L,new BigDecimal("0.48"),"202983451222222",tkStatus,status);
    }

    // 准备有上级,上上级处理过的付款订单在数据库
    private JdOrders buildHasSuperiorUserPaidJdOrdersInDb(){
        logger.debug("processHasSuperiorUserPaidJdOrdersInDb");
        JdOrders jdOrders = buildJdOrders();
        jdOrders.setPid("200000555551");
        jdOrders.setPositionId(200000555551L);
        jdOrders.setOrderId(2029834513333333L);
        jdOrders.setPrice(new BigDecimal("150.00"));
        jdOrders.setEstimateCosPrice(new BigDecimal("61.8"));
        jdOrders.setEstimateFee(new BigDecimal("20.0"));
        jdOrdersImport.dealWithJdOrders(jdOrders);
        User user = userMapper.selectById(11L);
        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
        JdOrders selectOrderByOrderId = jdOrdersMapper.selectOrderByOrderId(2029834513333333L);
        compareJdOrders(jdOrders,selectOrderByOrderId);
        Assert.assertTrue(jdOrders.getOneLevelCommission().compareTo(selectOrderByOrderId.getOneLevelCommission()) == 0);
        Assert.assertTrue(jdOrders.getOneLevelUserUit().compareTo(selectOrderByOrderId.getOneLevelUserUit()) == 0);
        Assert.assertTrue(jdOrders.getTwoLevelCommission().compareTo(selectOrderByOrderId.getTwoLevelCommission()) == 0);
        Assert.assertTrue(jdOrders.getTwoLevelUserUit().compareTo(selectOrderByOrderId.getTwoLevelUserUit()) == 0);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("2029834513333333");
        logger.debug("processHasSuperiorUserPaidJdOrdersInDb userIncomes is : {}",userIncomes);
        UserIncome userIncome = getUserIncomeByUserId(userIncomes, 11L);
        compareUserIncome(userIncome,11L,new BigDecimal("15.80"),"2029834513333333",16L,0);
        UserIncome userIncome1 = getUserIncomeByUserId(userIncomes, 12L);
        compareUserIncome(userIncome1,12L,new BigDecimal("0.80"),"2029834513333333",16L,0);
        UserIncome userIncome2= getUserIncomeByUserId(userIncomes, 13L);
        compareUserIncome(userIncome2,13L,new BigDecimal("0.20"),"2029834513333333",16L,0);
        return jdOrders;
    }

    private void compareHasSuperiorJdOrdersAndUserIncome(JdOrders orders,Integer validCode,Long tkStatus,Integer status){
        orders.setValidCode(validCode);
        jdOrdersImport.dealWithJdOrders(orders);
        JdOrders preJdOrders = jdOrdersMapper.selectOrderByOrderId(2029834513333333L);
        compareJdOrders(orders,preJdOrders);
        Assert.assertTrue(orders.getOneLevelCommission().compareTo(preJdOrders.getOneLevelCommission()) == 0);
        Assert.assertTrue(orders.getOneLevelUserUit().compareTo(preJdOrders.getOneLevelUserUit()) == 0);
        Assert.assertTrue(orders.getTwoLevelCommission().compareTo(preJdOrders.getTwoLevelCommission()) == 0);
        Assert.assertTrue(orders.getTwoLevelUserUit().compareTo(preJdOrders.getTwoLevelUserUit()) == 0);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("2029834513333333");
        logger.debug("processHasSuperiorUserPaidJdOrdersInDb userIncomes is : {}",userIncomes);
        UserIncome userIncome = getUserIncomeByUserId(userIncomes, 11L);
        compareUserIncome(userIncome,11L,new BigDecimal("15.80"),"2029834513333333",tkStatus,status);
        UserIncome userIncome1 = getUserIncomeByUserId(userIncomes, 12L);
        compareUserIncome(userIncome1,12L,new BigDecimal("0.80"),"2029834513333333",tkStatus,status);
        UserIncome userIncome2= getUserIncomeByUserId(userIncomes, 13L);
        compareUserIncome(userIncome2,13L,new BigDecimal("0.20"),"2029834513333333",tkStatus,status);
    }
    // 开启首单奖励时有用
    private void compareFirstOrdersIncome(Long userId){
        UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(userId, 10);
        logger.debug("userIncome is : {}",userIncome);
        Assert.assertEquals(Long.valueOf(10),userIncome.getUserId());
        Assert.assertEquals(Integer.valueOf(1),userIncome.getStatus());
        Assert.assertEquals(Integer.valueOf(2), userIncome.getAwardType());
        logger.debug("userIncome money is : {}",userIncome.getMoney());
        Assert.assertTrue(new BigDecimal("0.50").compareTo(userIncome.getMoney()) == 0);
//        User user = userMapper.selectById(10L);
        // 若订单佣金未冻结，用户集分宝需加上新人奖励的集分宝
//        Assert.assertEquals(Long.valueOf(230),user.getUserIntgeralTreasure());
    }

    private void compareOverDueJdOrders(JdOrders jdOrders,JdOrders previousJdOrders){
        // 失效订单，不计算用户的佣金
//        Assert.assertTrue(jdOrders.getUserUit().compareTo(previousJdOrders.getUserUit()) == 0);
//        Assert.assertTrue(jdOrders.getUserCommission().compareTo(previousJdOrders.getUserCommission()) == 0);
//        Assert.assertEquals(jdOrders.getUserName(),previousJdOrders.getUserName());
//        Assert.assertEquals(jdOrders.getUserId(),previousJdOrders.getUserId());
        Assert.assertEquals(jdOrders.getValidCode(),previousJdOrders.getValidCode());
        Assert.assertEquals(jdOrders.getFrozenSkuNum(),previousJdOrders.getFrozenSkuNum());
        Assert.assertEquals(jdOrders.getUnionTrafficGroup(),previousJdOrders.getUnionTrafficGroup());
        Assert.assertEquals(jdOrders.getUnionTag(),previousJdOrders.getUnionTag());
        Assert.assertEquals(jdOrders.getUnionRole(),previousJdOrders.getUnionRole());
        Assert.assertEquals(jdOrders.getUnionId(),previousJdOrders.getUnionId());
        Assert.assertEquals(jdOrders.getUnionAlias(),previousJdOrders.getUnionAlias());
        Assert.assertEquals(jdOrders.getTraceType(),previousJdOrders.getTraceType());
        Assert.assertEquals(jdOrders.getSubUnionId(),previousJdOrders.getSubUnionId());
        Assert.assertEquals(jdOrders.getSkuReturnNum(),previousJdOrders.getSkuReturnNum());
        Assert.assertEquals(jdOrders.getSkuNum(),previousJdOrders.getSkuNum());
        Assert.assertEquals(jdOrders.getSkuName(),previousJdOrders.getSkuName());
        Assert.assertEquals(jdOrders.getSkuinfoPayMonth(),previousJdOrders.getSkuinfoPayMonth());
        Assert.assertTrue(jdOrders.getSubsidyRate().compareTo(previousJdOrders.getSubsidyRate()) == 0);
        Assert.assertTrue(jdOrders.getSkuinfoValidCode().compareTo(previousJdOrders.getSkuinfoValidCode()) == 0);
        Assert.assertTrue(jdOrders.getSubSideRate().compareTo(previousJdOrders.getSubSideRate()) == 0);
        Assert.assertTrue(jdOrders.getPrice().compareTo(previousJdOrders.getPrice()) == 0);
        Assert.assertTrue(jdOrders.getPositionId().compareTo(previousJdOrders.getPositionId()) == 0);
        Assert.assertEquals(jdOrders.getOrderId(),previousJdOrders.getOrderId());
        Assert.assertTrue(jdOrders.getFinalRate().compareTo(previousJdOrders.getFinalRate()) == 0);
        Assert.assertTrue(jdOrders.getEstimateFee().compareTo(previousJdOrders.getEstimateFee()) == 0);
        Assert.assertTrue(jdOrders.getEstimateCosPrice().compareTo(previousJdOrders.getEstimateCosPrice()) == 0);
        Assert.assertTrue(jdOrders.getCommissionRate().compareTo(previousJdOrders.getCommissionRate())==0);
        Assert.assertTrue(jdOrders.getActualFee().compareTo(previousJdOrders.getActualFee()) == 0);
        Assert.assertTrue(jdOrders.getActualCosPrice().compareTo(previousJdOrders.getActualCosPrice()) == 0);
    }

    private void buildOverDueJdOrdersAndCompare(JdOrders jdOrders,Integer validCode,Long userId,String tradeId,Long orderId){
        jdOrders.setValidCode(validCode);
        jdOrdersImport.dealWithJdOrders(jdOrders);
        User user = userMapper.selectById(userId);
        Assert.assertEquals(Long.valueOf(1),user.getOrdersNum());
        JdOrders previousJdOrders = jdOrdersMapper.selectOrderByOrderId(orderId);
        compareOverDueJdOrders(jdOrders,previousJdOrders);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId(tradeId);
        Assert.assertEquals(0,userIncomes.size());
    }

    /**
     * 没上级
     * 付款订单
     */
    @Test
    public void processPaidJdOrders() {
        buildNoParentPaidJdOrdersInDb();
    }

    /**
     * 付款 -> 完成 -> 结算
     */
    @Test
    public void processPaidToSuccessJdOrders() {
        JdOrders jdOrders = buildNoParentPaidJdOrdersInDb();

        jdOrders.setValidCode(17);
        jdOrdersImport.dealWithJdOrders(jdOrders);
        JdOrders preJdOrders = jdOrdersMapper.selectOrderByOrderId(20298345177250L);
        compareJdOrders(jdOrders,preJdOrders);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("20298345177250");
        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 10L);
        logger.debug("userIncomeByUserId is : {}",userIncomeByUserId);
        compareUserIncome(userIncomeByUserId,10L,new BigDecimal("1.93"),"20298345177250",17L,0);

        User user = userMapper.selectById(10L);
        // 未到结算状态，用户集分宝不变
        Assert.assertEquals(Long.valueOf(0),user.getUserIntgeralTreasure());

        jdOrders.setValidCode(18);
        jdOrdersImport.dealWithJdOrders(jdOrders);
        JdOrders preJdOrders2 = jdOrdersMapper.selectOrderByOrderId(20298345177250L);
        compareJdOrders(jdOrders,preJdOrders2);
        List<UserIncome> userIncomes2 = userIncomeMapper.selectUserIncomeByOrdersId("20298345177250");
        UserIncome userIncomeByUserId2 = getUserIncomeByUserId(userIncomes2, 10L);
        logger.debug("userIncomeByUserId is : {}",userIncomeByUserId);
        compareUserIncome(userIncomeByUserId2,10L,new BigDecimal("1.93"),"20298345177250",18L,1);
        User user2 = userMapper.selectById(10L);
        Assert.assertEquals(Long.valueOf(193),user2.getUserIntgeralTreasure());
    }

    /**
     * 付款 -> 完成 -> 失效
     */
    @Test
    public void processPaidToSuccessToOverDueJdOrders() {
        JdOrders jdOrders = buildNoParentPaidJdOrdersInDb();

        jdOrders.setValidCode(17);
        jdOrdersImport.dealWithJdOrders(jdOrders);
        JdOrders preJdOrders = jdOrdersMapper.selectOrderByOrderId(20298345177250L);
        compareJdOrders(jdOrders, preJdOrders);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("20298345177250");
        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 10L);
        logger.debug("userIncomeByUserId is : {}", userIncomeByUserId);
        compareUserIncome(userIncomeByUserId, 10L, new BigDecimal("1.93"), "20298345177250", 17L, 0);

        User user = userMapper.selectById(10L);
        // 未到结算状态，用户集分宝不变
        Assert.assertEquals(Long.valueOf(0), user.getUserIntgeralTreasure());

        buildOverDueJdOrdersAndCompare(preJdOrders,-1,10L,"20298345177250",20298345177250L);
    }


        /**
         * 付款 -> 结算
         */
    @Test
    public void processPaidToAccountJdOrders() {
        JdOrders jdOrders = buildNoParentPaidJdOrdersInDb();

        jdOrders.setValidCode(18);
        jdOrdersImport.dealWithJdOrders(jdOrders);
        JdOrders preJdOrders = jdOrdersMapper.selectOrderByOrderId(20298345177250L);
        compareJdOrders(jdOrders,preJdOrders);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("20298345177250");
        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomes, 10L);
        logger.debug("userIncomeByUserId is : {}",userIncomeByUserId);
        compareUserIncome(userIncomeByUserId,10L,new BigDecimal("1.93"),"20298345177250",18L,1);
        User user = userMapper.selectById(10L);
        Assert.assertEquals(Long.valueOf(193),user.getUserIntgeralTreasure());
        // 新人首单奖励
       //compareFirstOrdersIncome(10L);
    }

    /**
     * 结算
     */
    @Test
    public void processAccountJdOrders(){
        JdOrders jdOrders = buildJdOrders();
        jdOrders.setEstimateFee(new BigDecimal("1000"));
        jdOrders.setEstimateCosPrice(new BigDecimal("1000"));
        jdOrders.setEstimateFee(new BigDecimal("1000"));
        jdOrders.setEstimateCosPrice(new BigDecimal("1000"));

        compareJdOrdersAndUserIncome(jdOrders,18,18L,2);
        User user = userMapper.selectById(10L);
        // 佣金冻结中，用户集分宝不变
        Assert.assertEquals(Long.valueOf(0),user.getUserIntgeralTreasure());

        // 新人首单奖励
//        compareFirstOrdersIncome(10L);
    }

    /**
     * 失效
     */
    @Test
    public void processOverDueJdOrders_1(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,-1,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_2(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,2,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_3(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,3,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_4(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,4,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_5(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,5,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_6(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,6,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_7(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,7,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_8(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,8,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_9(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,9,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_20(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,10,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_11(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,11,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_12(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,12,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_13(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,13,10L,"20298345177250",20298345177250L);
    }
    @Test
    public void processOverDueJdOrders_14(){
        JdOrders jdOrders = buildJdOrders();
        buildOverDueJdOrdersAndCompare(jdOrders,14,10L,"20298345177250",20298345177250L);
    }

    /**
     * 有上级
     * 付款
     */
    @Test
    public void processHasParentPaidJdOrders(){
        buildHasParentPaidJdOrdersInDb();
    }

    /**
     * 有上级
     * 付款 -> 完成
     */
    @Test
    public void processHasParentPaidToSuccessJdOrders(){
        JdOrders jdOrders = buildHasParentPaidJdOrdersInDb();
        compareHasParentJdOrdersAndUserIncome(jdOrders,17,17L,0);
        User user1 = userMapper.selectById(12L);
        Assert.assertEquals(Long.valueOf(0),user1.getUserIntgeralTreasure());
    }

    /**
     * 付款 -> 结算
     */
    @Test
    public void processHasParentPaidToAccountJdOrders(){
        JdOrders jdOrders = buildHasParentPaidJdOrdersInDb();
        compareHasParentJdOrdersAndUserIncome(jdOrders,18,18L,1);
        User user1 = userMapper.selectById(12L);
        Assert.assertEquals(Long.valueOf(948),user1.getUserIntgeralTreasure());

        // 校验拉新奖励
//        UserIncome userIncomeByTypeAndUserId = userIncomeMapper.getUserIncomeByTypeAndUserId(13L);
//        logger.debug("userIncomeByTypeAndUserId is : {}",userIncomeByTypeAndUserId);
//        compareUserIncome(userIncomeByTypeAndUserId,13L,new BigDecimal("0.1"),"202983451222222",18L,1);
//
//        // 校验上级用户的集分宝
//        User user2 = userMapper.selectById(13L);
//        Assert.assertEquals(Long.valueOf(58),user2.getUserIntgeralTreasure());
    }



//    /**
//     * 结算
//     * 上个月完成状态的订单 -- 下个月20奖励用户 -- 结算时间为20号之后不会再次奖励用户
//     * 测试此方法需要手动修改 JdOrdersImport 中的 compareDate()方法修改当前的日期
//     * 需要修改订单的下单时间
//     */
//    @Test
//    public void processHasParentPaidToAccountJdOrders_2(){
//        JdOrders jdOrders = buildHasParentPaidJdOrdersInDb();
//        jdOrders.setOrderTime("2020-07-02 00:00:00");
//        compareHasParentJdOrdersAndUserIncome(jdOrders,18,18L,1);
//
//
//        User user1 = userMapper.selectById(12L);
//        Assert.assertEquals(Long.valueOf(0),user1.getUserIntgeralTreasure());
//        User user2 = userMapper.selectById(13L);
//        Assert.assertEquals(Long.valueOf(0),user2.getUserIntgeralTreasure());
//
//    }
//
//    /**
//     * 上个月完成状态的订单
//     * 测试此方法需要手动修改 JdOrdersImport 中的 compareDate()方法修改当前的日期
//     * 需要修改订单的下单时间
//     */
//    @Test
//    public void processHasParentPaidToAccountJdOrders_3(){
//        JdOrders jdOrders = buildHasParentPaidJdOrdersInDb();
//        jdOrders.setOrderTime("2020-07-10 00:00:00");
//        compareHasParentJdOrdersAndUserIncome(jdOrders,17,17L,0);
//        // 正常请求完成状态不会进行用户奖励
//        User user1 = userMapper.selectById(12L);
//        Assert.assertEquals(Long.valueOf(0),user1.getUserIntgeralTreasure());
//        User user2 = userMapper.selectById(13L);
//        Assert.assertEquals(Long.valueOf(0),user2.getUserIntgeralTreasure());
//
//        // 完成状态下手动奖励
//        JdOrders jdOrders1 = jdOrdersMapper.selectOrderByOrderId(jdOrders.getOrderId());
//        jdOrdersImport.testHandleLastMonthCompletionStatusOrders(jdOrders1);
//
//        User user3 = userMapper.selectById(12L);
//        Assert.assertEquals(Long.valueOf(948),user3.getUserIntgeralTreasure());
//        User user4 = userMapper.selectById(13L);
//        Assert.assertEquals(Long.valueOf(48),user4.getUserIntgeralTreasure());
//
//        List<UserIncome> userIncomeList = userIncomeMapper.selectUserIncomeByOrdersId(String.valueOf(jdOrders.getOrderId()));
//        UserIncome userIncomeByUserId = getUserIncomeByUserId(userIncomeList, 12L);
//        Assert.assertEquals(Long.valueOf(12),userIncomeByUserId.getUserId());
//        Assert.assertEquals(Integer.valueOf(1),userIncomeByUserId.getStatus());
//        Assert.assertEquals(Long.valueOf(17),userIncomeByUserId.getTkStatus());
//        Assert.assertTrue(new BigDecimal("9.48").compareTo(userIncomeByUserId.getMoney()) == 0);
//
//        UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomeList, 13L);
//        Assert.assertEquals(Long.valueOf(13),userIncomeByUserId1.getUserId());
//        Assert.assertEquals(Integer.valueOf(1),userIncomeByUserId1.getStatus());
//        Assert.assertEquals(Long.valueOf(17),userIncomeByUserId1.getTkStatus());
//        Assert.assertTrue(new BigDecimal("0.48").compareTo(userIncomeByUserId1.getMoney()) == 0);
//
//        // 奖励用户后进入结算状态，且结算时间为20号之后不会再次奖励用户
//        jdOrders1.setValidCode(18);
//        jdOrdersImport.dealWithJdOrders(jdOrders1);
//
//        // 用户集分不变
//        User user5 = userMapper.selectById(12L);
//        Assert.assertEquals(Long.valueOf(948),user5.getUserIntgeralTreasure());
//        User user6 = userMapper.selectById(13L);
//        Assert.assertEquals(Long.valueOf(48),user6.getUserIntgeralTreasure());
//
//        // 收益状态更新为结算状态
//        List<UserIncome> userIncomeList2 = userIncomeMapper.selectUserIncomeByOrdersId(String.valueOf(jdOrders.getOrderId()));
//        UserIncome userIncomeByUserId2 = getUserIncomeByUserId(userIncomeList2, 12L);
//        Assert.assertEquals(Long.valueOf(12),userIncomeByUserId2.getUserId());
//        Assert.assertEquals(Integer.valueOf(1),userIncomeByUserId2.getStatus());
//        Assert.assertEquals(Long.valueOf(18),userIncomeByUserId2.getTkStatus());
//        Assert.assertTrue(new BigDecimal("9.48").compareTo(userIncomeByUserId2.getMoney()) == 0);
//
//        UserIncome userIncomeByUserId3 = getUserIncomeByUserId(userIncomeList2, 13L);
//        Assert.assertEquals(Long.valueOf(13),userIncomeByUserId3.getUserId());
//        Assert.assertEquals(Integer.valueOf(1),userIncomeByUserId3.getStatus());
//        Assert.assertEquals(Long.valueOf(18),userIncomeByUserId3.getTkStatus());
//        Assert.assertTrue(new BigDecimal("0.48").compareTo(userIncomeByUserId3.getMoney()) == 0);
//
//    }


    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_1(){
        JdOrders jdOrders1 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders1,-1,12L,"202983451222222",202983451222222L);
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_3(){

        JdOrders jdOrders3 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders3,3,12L,"202983451222222",202983451222222L);
//
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_4(){
        JdOrders jdOrders4 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders4,4,12L,"202983451222222",202983451222222L);
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_5(){
        JdOrders jdOrders5 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders5,5,12L,"202983451222222",202983451222222L);

    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_6(){

        JdOrders jdOrders6 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders6,6,12L,"202983451222222",202983451222222L);
//
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_7(){

        JdOrders jdOrders7 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders7,7,12L,"202983451222222",202983451222222L);

    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_8(){

        JdOrders jdOrders8 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders8,8,12L,"202983451222222",202983451222222L);
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_9(){

        JdOrders jdOrders9 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders9,9,12L,"202983451222222",202983451222222L);
//
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_11(){

        JdOrders jdOrders10 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders10,10,12L,"202983451222222",202983451222222L);
//
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_12(){

        JdOrders jdOrders12 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders12,12,12L,"202983451222222",202983451222222L);
//
    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_13(){

        JdOrders jdOrders13 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders13,13,12L,"202983451222222",202983451222222L);

    }

    /**
     * 有上级
     * 付款 -> 失效
     */
    @Test
    public void processHasParentPaidToOverDueJdOrders_14(){

        JdOrders jdOrders14 = buildHasParentPaidJdOrdersInDb();
        buildOverDueJdOrdersAndCompare(jdOrders14,14,12L,"202983451222222",202983451222222L);
    }

    /**
     * 有上级，上上级
     * 付款
     */
    @Test
    public void processHasSuperiorPaidJdOrders(){
        buildHasSuperiorUserPaidJdOrdersInDb();
    }

    /**
     * 有上级，上上级
     * 付款 -> 完成
     */
    @Test
    public void processHasSuperiorPaidToSuccessJdOrders(){
        JdOrders jdOrders = buildHasSuperiorUserPaidJdOrdersInDb();
        compareHasSuperiorJdOrdersAndUserIncome(jdOrders,17,17L,0);
    }

    /**
     * 有上级，上上级
     * 付款 -> 结算
     */
    @Test
    public void processHasSuperiorPaidToAccountJdOrders(){
        JdOrders jdOrders = buildHasSuperiorUserPaidJdOrdersInDb();
        compareHasSuperiorJdOrdersAndUserIncome(jdOrders,18,18L,2);

        // 校验拉新奖励
//        UserIncome userIncomeByTypeAndUserId = userIncomeMapper.getUserIncomeByTypeAndUserId(12L);
//        logger.debug("userIncomeByTypeAndUserId is : {}",userIncomeByTypeAndUserId);
//        compareUserIncome(userIncomeByTypeAndUserId,12L,new BigDecimal("0.1"),"2029834513333333",18L,1);
//        User parentUser = userMapper.selectById(12L);
//        Assert.assertEquals(Long.valueOf(10),parentUser.getUserIntgeralTreasure());
    }

//    /**
//     * 有上级，上上级
//     * 付款 -> 失效
//     */
//    @Test
//    public void processHasSuperiorPaidToOverDueJdOrders(){
//        JdOrders jdOrders1 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders1,-1,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders2 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders2,2,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders3 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders3,3,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders4 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders4,4,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders5 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders5,5,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders6 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders6,6,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders7 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders7,7,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders8 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders8,8,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders9 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders9,9,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders10 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders10,10,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders11 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders11,11,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders12 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders12,12,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders13 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders13,13,11L,"2029834513333333",2029834513333333L);
//        JdOrders jdOrders14 = buildHasSuperiorUserPaidJdOrdersInDb();
//        buildOverDueJdOrdersAndCompare(jdOrders14,14,11L,"2029834513333333",2029834513333333L);
//    }

    @Test
    public void importJdOrders() throws IOException {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String time = sdf.format(date);
        //2020-06-16 02:37:32
        jdOrdersImport.importJdOrders("2020083102",1,200,1,null);
    }

    @Test
    public void handleLastMonthCompletionStatusOrders(){
        jdOrdersImport.handleLastMonthCompletionStatusOrders();
    }

}
