package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.model.MeituanOrders;
import com.alipay.rebate.shop.model.MeituanRefundOrders;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import com.alipay.rebate.shop.scheduler.orderimport.MeiTuanOrderImporter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class MeiTuanOrderImporterTest {

    @Resource
    private MeiTuanOrderImporter meiTuanOrderImporter;
    @Resource
    private UserMapper userMapper;
    @Resource
    private UserIncomeMapper userIncomeMapper;
    @Resource
    private MeituanOrdersMapper meituanOrdersMapper;
    @Resource
    private UserGradeMapper userGradeMapper;
    @Resource
    private MeituanRefundOrdersMapper meituanRefundOrdersMapper;
    private Logger logger = LoggerFactory.getLogger(MeiTuanOrderImporterTest.class);

    @Before
    public void before(){
//        userMapper.deleteUserById(1L);
//        userMapper.deleteUserById(2L);
//        userMapper.deleteUserById(3L);
//        userIncomeMapper.deleteByTradeId("978234982342");
//        meituanOrdersMapper.deleteByOrderId("978234982342");
//        meituanRefundOrdersMapper.deleteByOrderId("978234982342");

        // 插入用户1
        User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
        userMapper.insertSelective(user1);
        // 插入用户2
        User user2 = buildUser(2L,2L,2L,3L,"18819258221","2222222222");
        userMapper.insertSelective(user2);
        // 插入用户3
        User user3 = buildUser(3L,3L,3L,4L,"18819258222","3333333333");
        userMapper.insertSelective(user3);
    }

    @After
    public void after(){
//        userMapper.deleteUserById(1L);
//        userMapper.deleteUserById(2L);
//        userMapper.deleteUserById(3L);
//        userIncomeMapper.deleteByTradeId("978234982342");
//        meituanOrdersMapper.deleteByOrderId("978234982342");
//        meituanRefundOrdersMapper.deleteByOrderId("978234982342");
    }

    private User buildUser(
            Long userId,
            Long relationId , Long specialId,
            Long parentUserId, String phone,
            String taobaoUserId
    ){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName("mmm1");
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(relationId);
        user.setSpecialId(specialId);
        user.setParentUserId(parentUserId);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserGradeId(1);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(taobaoUserId);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return user;
    }

    private MeituanOrders buildMeituanOrders(){
        MeituanOrders orders = new MeituanOrders();
        orders.setOrderId("978234982342");
        orders.setDirect(new BigDecimal("50.00"));
        orders.setTotal(new BigDecimal("50.00"));
        orders.setNewBuyer("1");
        orders.setQuantity("1");
        orders.setPrice(new BigDecimal("50.00"));
        orders.setProfit(new BigDecimal("10.00"));
        orders.setSequence("1");
        orders.setSid("1");
        orders.setUid("lskdjflksdf");
        orders.setSign("uoweorijsodfjsf");
        orders.setSmsTitle("頭壹號大油条（回龙观店）");
        orders.setPayTime("2020-08-10 00:00:00");
        orders.setUseTime("2020-08-16 00:00:00");
        orders.setModtime("2020-08-15 00:00:00");
        return orders;
    }

    private MeituanRefundOrders buildMeituanRefundOrders(){
        MeituanRefundOrders refundOrders = new MeituanRefundOrders();
        refundOrders.setMoney(new BigDecimal("50.00"));
        refundOrders.setOrderId("978234982342");
        refundOrders.setProfit(new BigDecimal("10.00"));
        Date date = new Date();
        refundOrders.setCreateTime(date);
        refundOrders.setUpdateTime(date);
        refundOrders.setRefundTime("2020-08-20 00:00:00");
        return refundOrders;
    }

    private UserIncome getUserIncomeByUserId(Long userId, List<UserIncome> incomeList){

        for(UserIncome userIncome: incomeList){
            if(userIncome.getUserId().equals(userId)){
                return userIncome;
            }
        }
        return null;
    }

    /**
     * 美团订单,找到用户信息，以前订单为空
     */
    @Test
    public void handleMeiTuanOrders_1() {
        MeituanOrders orders = buildMeituanOrders();
        meiTuanOrderImporter.handleMeiTuanOrders(orders);

        MeituanOrders orders1 = meituanOrdersMapper.selectMeituanOrdersByOrderId("978234982342");
        logger.debug("orders1 is : {}",orders1);
        Assert.assertEquals(orders.getOrderId(),orders1.getOrderId());
        Assert.assertEquals(orders.getPayTime(),orders1.getPayTime());
        Assert.assertEquals(orders.getSid(),orders1.getSid());
        Assert.assertEquals(orders.getSmsTitle(),orders1.getSmsTitle());
        Assert.assertEquals(orders.getSign(),orders1.getSign());
        Assert.assertEquals(orders.getNewBuyer(),orders1.getNewBuyer());
        Assert.assertTrue(orders.getTotal().compareTo(orders1.getTotal()) == 0);
        Assert.assertTrue(orders.getProfit().compareTo(orders1.getProfit()) == 0);
        Assert.assertTrue(orders.getDirect().compareTo(orders1.getDirect()) == 0);
        Assert.assertTrue(new BigDecimal("0.40").compareTo(orders1.getOneLevelCommission()) == 0);
        Assert.assertTrue(new BigDecimal("0.10").compareTo(orders1.getTwoLevelCommission()) == 0);
        Assert.assertTrue(new BigDecimal("8.30").compareTo(orders1.getUserCommission()) == 0);


        List<UserIncome> userIncomeList = userIncomeMapper.selectUserIncomeByOrdersId("978234982342");
        logger.debug("userIncomeList is : {}",userIncomeList);
        UserIncome userIncome1 = getUserIncomeByUserId(1L, userIncomeList);
        logger.debug("getMoney is : {}",userIncome1.getMoney());
        Assert.assertEquals(Long.valueOf(1),userIncome1.getUserId());
        Assert.assertTrue(new BigDecimal("8.30").compareTo(userIncome1.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(0),userIncome1.getStatus());
        Assert.assertEquals(Integer.valueOf(21),userIncome1.getType());
        UserIncome userIncome2 = getUserIncomeByUserId(2L, userIncomeList);
        Assert.assertEquals(Long.valueOf(2),userIncome2.getUserId());
        Assert.assertTrue(new BigDecimal("0.40").compareTo(userIncome2.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(0),userIncome2.getStatus());
        Assert.assertEquals(Integer.valueOf(21),userIncome2.getType());
        UserIncome userIncome3 = getUserIncomeByUserId(3L, userIncomeList);
        Assert.assertEquals(Long.valueOf(3),userIncome3.getUserId());
        Assert.assertTrue(new BigDecimal("0.10").compareTo(userIncome3.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(0),userIncome3.getStatus());
        Assert.assertEquals(Integer.valueOf(21),userIncome3.getType());
    }



    /**
     * 美团订单,找不到用户
     */
    @Test
    public void handleMeiTuanOrders_2() {
        MeituanOrders orders = buildMeituanOrders();
        orders.setSid("9823745");
        meiTuanOrderImporter.handleMeiTuanOrders(orders);

        MeituanOrders orders1 = meituanOrdersMapper.selectMeituanOrdersByOrderId("978234982342");
        logger.debug("orders1 is : {}",orders1);
        Assert.assertEquals(orders.getOrderId(),orders1.getOrderId());
        Assert.assertEquals(orders.getPayTime(),orders1.getPayTime());
        Assert.assertEquals(orders.getSid(),orders1.getSid());
        Assert.assertEquals(orders.getSmsTitle(),orders1.getSmsTitle());
        Assert.assertEquals(orders.getSign(),orders1.getSign());
        Assert.assertEquals(orders.getNewBuyer(),orders1.getNewBuyer());
        Assert.assertTrue(orders.getTotal().compareTo(orders1.getTotal()) == 0);
        Assert.assertTrue(orders.getProfit().compareTo(orders1.getProfit()) == 0);
        Assert.assertTrue(orders.getDirect().compareTo(orders1.getDirect()) == 0);
        Assert.assertNull(orders1.getOneLevelCommission());
        Assert.assertNull(orders1.getTwoLevelCommission());
        Assert.assertNull(orders1.getUserCommission());


        List<UserIncome> userIncomeList = userIncomeMapper.selectUserIncomeByOrdersId("978234982342");
        Assert.assertEquals(0,userIncomeList.size());

    }


    @Test
    public void handleRefundOrders() {

        handleMeiTuanOrders_1();

        MeituanRefundOrders refundOrders = buildMeituanRefundOrders();
        meiTuanOrderImporter.handleRefundOrders(refundOrders);
        MeituanRefundOrders refundOrders1 = meituanRefundOrdersMapper.selectMeituanRefundOrders("978234982342");
        Assert.assertEquals(Long.valueOf(1),refundOrders1.getUserId());

        List<UserIncome> userIncomeList = userIncomeMapper.selectUserIncomeByOrdersId("978234982342");
        logger.debug("userIncomeList is : {}",userIncomeList);
        UserIncome userIncome1 = getUserIncomeByUserId(1L, userIncomeList);
        logger.debug("getMoney is : {}",userIncome1.getMoney());
        Assert.assertEquals(Long.valueOf(1),userIncome1.getUserId());
        Assert.assertTrue(new BigDecimal("8.30").compareTo(userIncome1.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(1),userIncome1.getStatus());
        Assert.assertEquals(Integer.valueOf(21),userIncome1.getType());
        UserIncome userIncome2 = getUserIncomeByUserId(2L, userIncomeList);
        Assert.assertEquals(Long.valueOf(2),userIncome2.getUserId());
        Assert.assertTrue(new BigDecimal("0.40").compareTo(userIncome2.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(1),userIncome2.getStatus());
        Assert.assertEquals(Integer.valueOf(21),userIncome2.getType());
        UserIncome userIncome3 = getUserIncomeByUserId(3L, userIncomeList);
        Assert.assertEquals(Long.valueOf(3),userIncome3.getUserId());
        Assert.assertTrue(new BigDecimal("0.10").compareTo(userIncome3.getMoney()) == 0);
        Assert.assertEquals(Integer.valueOf(1),userIncome3.getStatus());
        Assert.assertEquals(Integer.valueOf(21),userIncome3.getType());
    }
}