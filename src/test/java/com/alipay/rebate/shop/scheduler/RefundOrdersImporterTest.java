package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.RefundOrdersMapper;
import com.alipay.rebate.shop.dao.mapper.UserIncomeMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.RefundOrders;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserIncome;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import com.alipay.rebate.shop.scheduler.orderimport.RefundOrdersImporter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class RefundOrdersImporterTest {

  private Logger logger = LoggerFactory.getLogger(UserIncomeScannerTest.class);

  @Resource
  private UserIncomeMapper userIncomeMapper;
  @Resource
  private UserMapper userMapper;
  @Resource
  private RefundOrdersImporter refundOrdersImporter;
  @Resource
  private OrdersMapper ordersMapper;
  @Resource
  private RefundOrdersMapper refundOrdersMapper;

  @Before
  public void before(){

    logger.debug("before test code , clear user , user_income , orders recode");

    userMapper.deleteUserById(1L);
    userMapper.deleteUserById(2L);
    userMapper.deleteUserById(3L);
    userMapper.deleteUserById(4L);
    userMapper.deleteUserById(5L);

    ordersMapper.deleteByPrimaryKey("510202563513111111");
    ordersMapper.deleteByPrimaryKey("510202563513222222");
    ordersMapper.deleteByPrimaryKey("510202563513333333");
    ordersMapper.deleteByPrimaryKey("510202563513444444");
    ordersMapper.deleteByPrimaryKey("510202563513555555");

    userIncomeMapper.deleteByTradeId("510202563513111111");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513333333");

    refundOrdersMapper.deleteByPrimaryKey("510202563513111111");


    // 插入用户1
    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
    user1.setUserIntgeralTreasure(1000L);
    userMapper.insertSelective(user1);
    // 插入用户2
    User user2 = buildUser(2L,2L,2L,3L,"18819258221","2222222222");
    user2.setUserIntgeralTreasure(1000L);
    userMapper.insertSelective(user2);
    // 插入用户3
    User user3 = buildUser(3L,3L,3L,4L,"18819258222","3333333333");
    user3.setUserIntgeralTreasure(1000L);
    userMapper.insertSelective(user3);
    // 插入用户3
    User user4 = buildUser(4L,4L,4L,5L,"18819258223","3333333333");
    userMapper.insertSelective(user4);
  }

  private User buildUser(
      Long userId,
      Long relationId , Long specialId,
      Long parentUserId, String phone,
      String taobaoUserId
  ){
    User user = new User();
    user.setUserId(userId);
    user.setUserNickName("mmm1");
    user.setUserStatus(UserContant.USER_STATUS_NORMAL);
    user.setIsAuth(UserContant.IS_AUTH);
    user.setRelationId(relationId);
    user.setSpecialId(specialId);
    user.setParentUserId(parentUserId);
    user.setTotalFansOrdersNum(0);
    user.setUserIntgeralTreasure(0L);
    user.setPhone(phone);
    user.setOrdersNum(0L);
    user.setUserGradeId(1);
    user.setUserIntgeral(0L);
    user.setUserAmount(new BigDecimal("0.00"));
    user.setUserIncome(new BigDecimal("0.00"));
    user.setUserCommision(new BigDecimal("0.00"));
    user.setTaobaoUserId(taobaoUserId);
    user.setCreateTime(new Date());
    user.setUpdateTime(new Date());
    return user;
  }


  private UserIncome buildUserIncome(Long incomeId, BigDecimal money,
      Long userId,String createTime, String tradeId
  ){

    UserIncome userIncome = new UserIncome();
    userIncome.setUserIncomeId(incomeId);
    userIncome.setMoney(money);
    userIncome.setStatus(UserIncomeConstant.FREEZE);
    userIncome.setCreateTime(createTime);
    userIncome.setUpdateTime(createTime);
    userIncome.setTradeId(tradeId);
    userIncome.setType(UserIncomeConstant.ORDERS_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    userIncome.setTkStatus(3L);
    userIncome.setUserId(userId);
    return userIncome;
  }

  private Orders buildInitialOrders(){
    Orders orders = new Orders();
    orders.setTradeId("510202563513111111");
    orders.setAdzoneId(109205150467L);
    orders.setAdzoneName(" 渠道ID专属");
    orders.setClickTime("2020-04-25 00:20:30");
    orders.setTkPaidTime("2020-04-25 00:20:30");
    orders.setTbPaidTime("2020-04-25 00:20:30");
    orders.setCreateTime("2020-04-25 00:20:30");
    orders.setTkStatus(3L);
    orders.setRelationId(1L);
    orders.setPubSharePreFee(new BigDecimal("100"));
    orders.setTotalCommissionRate(new BigDecimal("10"));
    orders.setTotalCommissionFee(new BigDecimal("100"));
    orders.setRefundTag(0L);
    orders.setPayPrice(new BigDecimal("1000"));
    orders.setAlipayTotalPrice(new BigDecimal("1000"));
    orders.setItemNum(1L);
    orders.setItemImg("111");
    orders.setItemTitle("111");
    orders.setItemLink("111");
    orders.setOrderType("天猫");
    orders.setSiteId(620000087L);
    return orders;
  }

  private RefundOrders buildInitialRefundOrders(){
    RefundOrders refundOrders = new RefundOrders();
    refundOrders.setTbTradeId("510202563513111111");
    refundOrders.setTbTradeParentId("510202563513111111");
    refundOrders.setRefundStatus(2L);
    return refundOrders;
  }

  @Test
  public void processRefundOrdersList() {

    Orders orders1 = buildInitialOrders();
    orders1.setUserId(1L);
    ordersMapper.insertSelective(orders1);

    RefundOrders refundOrders = buildInitialRefundOrders();
    List<RefundOrders> refundOrdersList = new ArrayList<>();
    refundOrdersList.add(refundOrders);

    UserIncome userIncome1 = buildUserIncome(1L,new BigDecimal(50),1L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome1);
    UserIncome userIncome2 = buildUserIncome(2L,new BigDecimal(20),2L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome2);
    UserIncome userIncome3 = buildUserIncome(3L,new BigDecimal(5),3L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome3);

    refundOrdersImporter.processRefundOrdersList(refundOrdersList);

    // 校验订单的维权状态是否置为1
    Orders orders = ordersMapper.selectOrdersByTradeId("510202563513111111");
    Assert.assertEquals(Integer.valueOf(1),orders.getRefundStatus());

    User user1 = userMapper.selectById(1L);
    Assert.assertEquals(Long.valueOf("1000"),user1.getUserIntgeralTreasure());
    User user2 = userMapper.selectById(2L);
    Assert.assertEquals(Long.valueOf("1000"),user2.getUserIntgeralTreasure());
    User user3 = userMapper.selectById(3L);
    Assert.assertEquals(Long.valueOf("1000"),user3.getUserIntgeralTreasure());

    userIncome1 = userIncomeMapper.selectByPrimaryKey(1L);
    Assert.assertEquals(Integer.valueOf(1),userIncome1.getStatus());
    userIncome2 = userIncomeMapper.selectByPrimaryKey(2L);
    Assert.assertEquals(Integer.valueOf(1),userIncome2.getStatus());
    userIncome3 = userIncomeMapper.selectByPrimaryKey(3L);
    Assert.assertEquals(Integer.valueOf(1),userIncome3.getStatus());

  }

  @Test
  public void processRefundOrdersList_1() {

    Orders orders1 = buildInitialOrders();
    orders1.setUserId(1L);
    ordersMapper.insertSelective(orders1);

    RefundOrders refundOrders = buildInitialRefundOrders();
    List<RefundOrders> refundOrdersList = new ArrayList<>();
    refundOrdersList.add(refundOrders);

    UserIncome userIncome1 = buildUserIncome(1L,new BigDecimal(50),1L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncome1.setStatus(1);
    userIncomeMapper.insertSelective(userIncome1);
    UserIncome userIncome2 = buildUserIncome(2L,new BigDecimal(20),2L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncome2.setStatus(1);
    userIncomeMapper.insertSelective(userIncome2);
    UserIncome userIncome3 = buildUserIncome(3L,new BigDecimal(5),3L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncome3.setStatus(1);
    userIncomeMapper.insertSelective(userIncome3);

    refundOrdersImporter.processRefundOrdersList(refundOrdersList);

    // 校验订单的维权状态是否置为1
    Orders orders = ordersMapper.selectOrdersByTradeId("510202563513111111");
    Assert.assertEquals(Integer.valueOf(1),orders.getRefundStatus());

    User user1 = userMapper.selectById(1L);
    Assert.assertEquals(Long.valueOf("-4000"),user1.getUserIntgeralTreasure());
    User user2 = userMapper.selectById(2L);
    Assert.assertEquals(Long.valueOf("-1000"),user2.getUserIntgeralTreasure());
    User user3 = userMapper.selectById(3L);
    Assert.assertEquals(Long.valueOf("500"),user3.getUserIntgeralTreasure());

    userIncome1 = userIncomeMapper.selectByPrimaryKey(1L);
    Assert.assertEquals(Integer.valueOf(1),userIncome1.getStatus());
    userIncome2 = userIncomeMapper.selectByPrimaryKey(2L);
    Assert.assertEquals(Integer.valueOf(1),userIncome2.getStatus());
    userIncome3 = userIncomeMapper.selectByPrimaryKey(3L);
    Assert.assertEquals(Integer.valueOf(1),userIncome3.getStatus());

  }

}