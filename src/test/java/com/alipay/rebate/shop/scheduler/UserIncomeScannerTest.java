package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.model.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import com.alipay.rebate.shop.pojo.userincome.IncomeSchedulerRsp;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class UserIncomeScannerTest {

  private Logger logger = LoggerFactory.getLogger(UserIncomeScannerTest.class);

  @Resource
  private UserIncomeMapper userIncomeMapper;
  @Resource
  private UserMapper userMapper;
  @Resource
  private UserIncomeScanner userIncomeScanner;
  @Resource
  private OrdersMapper ordersMapper;
  @Resource
  private UserJdPidMapper userJdPidMapper;
  @Resource
  private JdOrdersMapper jdOrdersMapper;


  @Before
  public void before(){

    logger.debug("before test code , clear user , user_income , orders recode");

    userMapper.deleteUserById(1L);
    userMapper.deleteUserById(2L);
    userMapper.deleteUserById(3L);
    userMapper.deleteUserById(4L);
    userMapper.deleteUserById(5L);

    ordersMapper.deleteByPrimaryKey("510202563513111111");
    ordersMapper.deleteByPrimaryKey("510202563513222222");
    ordersMapper.deleteByPrimaryKey("510202563513333333");
    ordersMapper.deleteByPrimaryKey("510202563513444444");
    ordersMapper.deleteByPrimaryKey("510202563513555555");

    userIncomeMapper.deleteByTradeId("510202563513111111");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513222222");
    userIncomeMapper.deleteByTradeId("510202563513333333");

    jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177250L);
    jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177333L);
    jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177222l);


    // 插入用户1
    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
    userMapper.insertSelective(user1);
    // 插入用户2
    User user2 = buildUser(2L,2L,2L,3L,"18819258221","2222222222");
    userMapper.insertSelective(user2);
    // 插入用户3
    User user3 = buildUser(3L,3L,3L,4L,"18819258222","3333333333");
    userMapper.insertSelective(user3);
    // 插入用户3
    User user4 = buildUser(4L,4L,4L,5L,"18819258223","3333333333");
    userMapper.insertSelective(user4);
  }

//  @After
//  public void after(){
//    userMapper.deleteUserById(1L);
//    userMapper.deleteUserById(2L);
//    userMapper.deleteUserById(3L);
//
//    userIncomeMapper.deleteByTradeId("510202563513111111");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//  }


  private User buildUser(
      Long userId,
      Long relationId , Long specialId,
      Long parentUserId, String phone,
      String taobaoUserId
  ){
    User user = new User();
    user.setUserId(userId);
    user.setUserNickName("mmm1");
    user.setUserStatus(UserContant.USER_STATUS_NORMAL);
    user.setIsAuth(UserContant.IS_AUTH);
    user.setRelationId(relationId);
    user.setSpecialId(specialId);
    user.setParentUserId(parentUserId);
    user.setTotalFansOrdersNum(0);
    user.setUserIntgeralTreasure(0L);
    user.setPhone(phone);
    user.setOrdersNum(0L);
    user.setUserGradeId(1);
    user.setUserIntgeral(0L);
    user.setUserAmount(new BigDecimal("0.00"));
    user.setUserIncome(new BigDecimal("0.00"));
    user.setUserCommision(new BigDecimal("0.00"));
    user.setTaobaoUserId(taobaoUserId);
    user.setCreateTime(new Date());
    user.setUpdateTime(new Date());
    return user;
  }

  private UserJdPid buildUserJdPid(Long id, Long userId, String jdPid){
    UserJdPid userJdPid = new UserJdPid();
    userJdPid.setId(id);
    userJdPid.setAddTime(new Date());
    userJdPid.setUpdateTime(new Date());
    userJdPid.setUserId(userId);
    userJdPid.setPositionId(jdPid);
    return userJdPid;
  }


  private UserIncome buildUserIncome(Long incomeId, BigDecimal money,
      Long userId,String createTime, String tradeId
      ){

    UserIncome userIncome = new UserIncome();
    userIncome.setUserIncomeId(incomeId);
    userIncome.setMoney(money);
    userIncome.setStatus(UserIncomeConstant.FREEZE);
    userIncome.setCreateTime(createTime);
    userIncome.setUpdateTime(createTime);
    userIncome.setTradeId(tradeId);
    userIncome.setType(UserIncomeConstant.ORDERS_AWARD_TYPE);
    userIncome.setAwardType(UserIncomeConstant.UIT_AWARD_TYPE);
    userIncome.setTkStatus(3L);
    userIncome.setUserId(userId);
    return userIncome;
  }

  private Orders buildInitialOrders(){
    Orders orders = new Orders();
    orders.setTradeId("510202563513111111");
    orders.setAdzoneId(109205150467L);
    orders.setAdzoneName(" 渠道ID专属");
    orders.setClickTime("2020-04-25 00:20:30");
    orders.setTkPaidTime("2020-04-25 00:20:30");
    orders.setTbPaidTime("2020-04-25 00:20:30");
    orders.setCreateTime("2020-04-25 00:20:30");
    orders.setTkStatus(3L);
    orders.setRelationId(1L);
    orders.setPubSharePreFee(new BigDecimal("100"));
    orders.setTotalCommissionRate(new BigDecimal("10"));
    orders.setTotalCommissionFee(new BigDecimal("100"));
    orders.setRefundTag(0L);
    orders.setPayPrice(new BigDecimal("1000"));
    orders.setAlipayTotalPrice(new BigDecimal("1000"));
    orders.setItemNum(1L);
    orders.setItemImg("111");
    orders.setItemTitle("111");
    orders.setItemLink("111");
    orders.setOrderType("天猫");
    orders.setSiteId(620000087L);
    return orders;
  }

  private JdOrders buildJdOrders(){
    JdOrders jdOrders = new JdOrders();
    jdOrders.setFinishTime("2020-05-20 09:34:45");
    jdOrders.setOrderEmt(2);
    jdOrders.setOrderId(20298345177250L);
    jdOrders.setOrderTime("2020-05-17 08:45:50");
    jdOrders.setParentId(0l);
    jdOrders.setPayMonth("20191018");
    jdOrders.setPlus(0);
    jdOrders.setParentId(0l);
    jdOrders.setActualCosPrice(new BigDecimal("23"));
    jdOrders.setActualFee(new BigDecimal("2.28"));
    jdOrders.setCid1(1672l);
    jdOrders.setCid2(2599l);
    jdOrders.setCid3(13667l);
    jdOrders.setCommissionRate(new BigDecimal("11"));
    jdOrders.setCpActid(0l);
    jdOrders.setEstimateFee(new BigDecimal("2.28"));
    jdOrders.setEstimateCosPrice(new BigDecimal("23"));
    jdOrders.setFinalRate(new BigDecimal("90"));
    jdOrders.setPid("200000555550");
    jdOrders.setPositionId(200000555550L);
    jdOrders.setPopId(789438L);
    jdOrders.setPrice(new BigDecimal("23"));
    jdOrders.setSiteId(27916941095l);
    jdOrders.setSkuName("沉香倒流香佛系流云檀香香粒高山流水瀑布塔香卧室熏香艾草倒流香炉创意摆件沉香炉倒流 香薰 樱花 玻璃瓶装 送小荷花");
    jdOrders.setSkuNum(1l);
    jdOrders.setSkuReturnNum(0l);
    jdOrders.setSubSideRate(new BigDecimal("90"));
    jdOrders.setSubsidyRate(new BigDecimal("0"));
    jdOrders.setTraceType(2);
    jdOrders.setUnionRole(1);
    jdOrders.setUnionTrafficGroup(4);
    jdOrders.setUnionTag("00000100");
    jdOrders.setValidCode(16);
    jdOrders.setUnionId(1000521676L);
    jdOrders.setSkuinfoValidCode(16);
    return jdOrders;
  }


  private UserIncome getUserIncomeByUserId(List<UserIncome> userIncomeList,Long userId){
    logger.debug("getUserIncomeByUserId");
    for (UserIncome income : userIncomeList) {
      if (income.getUserId().equals(userId)){
        return income;
      }
    }
    return null;
  }


  @Test
  public void scanFreezeUserIncome() {

    Orders orders1 = buildInitialOrders();
    ordersMapper.insertSelective(orders1);

    Orders orders2 = buildInitialOrders();
    orders2.setTradeId("510202563513222222");
    ordersMapper.insertSelective(orders2);

    Orders orders3 = buildInitialOrders();
    orders3.setClickTime("2020-04-26 00:20:30");
    orders3.setTkPaidTime("2020-04-26 00:20:30");
    orders3.setTbPaidTime("2020-04-26 00:20:30");
    orders3.setCreateTime("2020-04-26 00:20:30");
    orders3.setTradeId("510202563513333333");
    ordersMapper.insertSelective(orders3);

    // 创建三条满足转账条件收益记录(即订单付款天数超过数据库表的冻结配置时间)
    UserIncome userIncome1 = buildUserIncome(1L,new BigDecimal(50),1L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome1);
    UserIncome userIncome2 = buildUserIncome(2L,new BigDecimal(20),2L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome2);
    UserIncome userIncome3 = buildUserIncome(3L,new BigDecimal(5),3L,
        "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome3);
    // 创建1条满足转账条件收益记录(即订单付款天数超过数据库表的冻结配置时间)
    UserIncome userIncome4 = buildUserIncome(4L,new BigDecimal(5),4L,
        "2020-04-25 00:20:30", "510202563513222222");
    userIncomeMapper.insertSelective(userIncome4);

    // 创建三条不满足桩长条件记录
    UserIncome userIncome5 = buildUserIncome(5L,new BigDecimal(50),1L,
        "2020-04-25 00:20:30", "510202563513333333");
    userIncomeMapper.insertSelective(userIncome5);
    UserIncome userIncome6 = buildUserIncome(6L,new BigDecimal(20),2L,
        "2020-04-25 00:20:30", "510202563513333333");
    userIncomeMapper.insertSelective(userIncome6);
    UserIncome userIncome7 = buildUserIncome(7L,new BigDecimal(5),3L,
        "2020-04-25 00:20:30", "510202563513333333");
    userIncomeMapper.insertSelective(userIncome7);

    userIncomeScanner.scanFreezeUserIncome();
    User user1 = userMapper.selectById(1L);
    Assert.assertEquals(Long.valueOf("5000"),user1.getUserIntgeralTreasure());
    User user2 = userMapper.selectById(2L);
    Assert.assertEquals(Long.valueOf("2000"),user2.getUserIntgeralTreasure());
    User user3 = userMapper.selectById(3L);
    Assert.assertEquals(Long.valueOf("500"),user3.getUserIntgeralTreasure());
    User user4 = userMapper.selectById(4L);
    Assert.assertEquals(Long.valueOf("500"),user4.getUserIntgeralTreasure());

    userIncome1 = userIncomeMapper.selectByPrimaryKey(1L);
    Assert.assertEquals(Integer.valueOf(1),userIncome1.getStatus());
    userIncome2 = userIncomeMapper.selectByPrimaryKey(2L);
    Assert.assertEquals(Integer.valueOf(1),userIncome2.getStatus());
    userIncome3 = userIncomeMapper.selectByPrimaryKey(3L);
    Assert.assertEquals(Integer.valueOf(1),userIncome3.getStatus());
    userIncome4 = userIncomeMapper.selectByPrimaryKey(4L);
    Assert.assertEquals(Integer.valueOf(1),userIncome4.getStatus());


  }

  @Test
  public void processRoadTypeIsNullUserIncome(){

    // 准备roadType为空的收益记录
    UserIncome userIncome1 = buildUserIncome(1L,new BigDecimal(50),1L,
            "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome1);
    UserIncome userIncome2 = buildUserIncome(2L,new BigDecimal(20),2L,
            "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome2);
    UserIncome userIncome3 = buildUserIncome(3L,new BigDecimal(5),3L,
            "2020-04-25 00:20:30", "510202563513111111");
    userIncomeMapper.insertSelective(userIncome3);
    // 创建1条满足转账条件收益记录(即订单付款天数超过数据库表的冻结配置时间)
    UserIncome userIncome4 = buildUserIncome(4L,new BigDecimal(5),4L,
            "2020-04-25 00:20:30", "510202563513222222");
    userIncomeMapper.insertSelective(userIncome4);

    List<String> tradeIds = new ArrayList<>();
    tradeIds.add("510202563513111111");
    tradeIds.add("510202563513222222");
    List<IncomeSchedulerRsp> incomeSchedulerRsps = userIncomeMapper.selectUserIncomeByTradeId(tradeIds);
    userIncomeScanner.processRoadTypeIsNullUserIncome(incomeSchedulerRsps);

    List<UserIncome> userIncomes1 = userIncomeMapper.selectUserIncomeByOrdersId("510202563513111111");
    UserIncome userIncomeByUserId1 = getUserIncomeByUserId(userIncomes1, 1L);
    Assert.assertEquals(Integer.valueOf(0),userIncomeByUserId1.getRoadType());
    UserIncome userIncomeByUserId2 = getUserIncomeByUserId(userIncomes1, 2L);
    Assert.assertEquals(Integer.valueOf(1),userIncomeByUserId2.getRoadType());
    UserIncome userIncomeByUserId3 = getUserIncomeByUserId(userIncomes1, 3L);
    Assert.assertEquals(Integer.valueOf(2),userIncomeByUserId3.getRoadType());


    List<UserIncome> userIncomes2 = userIncomeMapper.selectUserIncomeByOrdersId("510202563513222222");
    UserIncome userIncomeByUserId4 = getUserIncomeByUserId(userIncomes2, 4L);
    Assert.assertEquals(Integer.valueOf(0),userIncomeByUserId4.getRoadType());

    List<IncomeSchedulerRsp> incomeSchedulerRsps2 = userIncomeMapper.selectUserIncomeByType();
    userIncomeScanner.processRoadTypeIsNullUserIncome(incomeSchedulerRsps2);

  }
}