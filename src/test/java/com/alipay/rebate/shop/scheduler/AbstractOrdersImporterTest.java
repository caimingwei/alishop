package com.alipay.rebate.shop.scheduler;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.constants.UserIncomeConstant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.model.*;
import com.alipay.rebate.shop.pojo.taobao.TkOrderRsponse;
import com.alipay.rebate.shop.scheduler.orderimport.AcOrdersImporter;
import com.alipay.rebate.shop.scheduler.orderimport.CrOrdersImporter;
import com.alipay.rebate.shop.scheduler.orderimport.OverdueOrdersImporter;
import com.alipay.rebate.shop.scheduler.orderimport.PaidOrdersImporter;
import com.alipay.rebate.shop.utils.DateUtil;
import com.alipay.rebate.shop.utils.DingdanxiaUtil;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class AbstractOrdersImporterTest {

  private Logger logger = LoggerFactory.getLogger(AbstractOrdersImporterTest.class);

  @Autowired
  private PaidOrdersImporter paidOrdersImporter;
  @Autowired
  private AcOrdersImporter acOrdersImporter;
  @Autowired
  private OverdueOrdersImporter overdueOrdersImporter;
  @Autowired
  private CrOrdersImporter crOrdersImporter;
  @Resource
  private UserMapper userMapper;
  @Resource
  private UserIncomeMapper userIncomeMapper;
  @Resource
  private OrdersMapper ordersMapper;
  @Resource
  private ActivityBrowseProductMapper activityBrowseProductMapper;
  @Resource
  private ActivityProductMapper activityProductMapper;

//  @Before
//  public void before(){
//
//    logger.debug("before test code , clear user , user_income , orders recode");
//
//    userMapper.deleteUserById(1L);
//    userMapper.deleteUserById(2L);
//    userMapper.deleteUserById(3L);
//    userMapper.deleteUserById(4L);
//    userMapper.deleteUserById(5L);
//    userMapper.deleteUserById(6L);
//    userMapper.deleteUserById(7L);
//    userMapper.deleteUserById(9L);
//    userMapper.deleteUserById(8L);
//
//    ordersMapper.deleteByPrimaryKey("510202563513111111");
//    ordersMapper.deleteByPrimaryKey("510202563513222222");
//    ordersMapper.deleteByPrimaryKey("510202563513333333");
//    ordersMapper.deleteByPrimaryKey("510202563513444444");
//    ordersMapper.deleteByPrimaryKey("510202563513555555");
//    ordersMapper.deleteByPrimaryKey("510202563513888888");
//    ordersMapper.deleteByPrimaryKey("510202563513666666");
//    ordersMapper.deleteByPrimaryKey("510202563513777777");
//    ordersMapper.deleteByPrimaryKey("510202563513999999");
//    ordersMapper.deleteByPrimaryKey("510202563513656565");
//
//
//    userIncomeMapper.deleteByTradeId("510202563513111111");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513333333");
//    userIncomeMapper.deleteByTradeId("510202563513444444");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513555555");
//    userIncomeMapper.deleteByTradeId("510202563513888888");
//    userIncomeMapper.deleteByTradeId("510202563513666666");
//    userIncomeMapper.deleteByTradeId("510202563513777777");
//    userIncomeMapper.deleteByTradeId("510202563513999999");
//    userIncomeMapper.deleteByTradeId("510202563513656565");
//
//
//    activityProductMapper.deleteByPrimaryKey(1);
//    activityBrowseProductMapper.deleteByUserId(1L);
//
//
//    // 插入用户1
//    User user1 = buildUser(1L,1L,1L,2L,"18819258220","1111111111");
//    userMapper.insertSelective(user1);
//    // 插入用户2
//    User user2 = buildUser(2L,2L,2L,3L,"18819258221","2222222222");
//    userMapper.insertSelective(user2);
//    // 插入用户3
//    User user3 = buildUser(3L,3L,3L,4L,"18819258222","3333333333");
//    userMapper.insertSelective(user3);
//    // 插入用户4
//    User user4 = buildUser(4L,4L,4L,5L,"18819258223","4444555555");
//    userMapper.insertSelective(user4);
//    // 插入用户5
//    User user5 = buildUser(5L,5L,5L,null,"18819258224","5555555555");
//    userMapper.insertSelective(user5);
//    // 插入用户5
//    User user6 = buildUser(6L,6L,6L,2l,"18819258229","6666666666");
//    userMapper.insertSelective(user6);
//    // 插入用户5
//    User user7 = buildUser(7L,7L,7L,2l,"18819258228","7777777777");
//    userMapper.insertSelective(user7);
//    // 插入用户5
//    User user9 = buildUser(9L,9L,9L,2l,"18819258227","9999999999");
//    userMapper.insertSelective(user9);
//
////    // 插入用户5
////    User user8 = buildUser(8L,8L,8L,null,"18819258227","88888888");
////    userMapper.insertSelective(user8);
//  }
//
//  @After
//  public void after(){
//    userMapper.deleteUserById(1L);
//    userMapper.deleteUserById(2L);
//    userMapper.deleteUserById(3L);
//    userMapper.deleteUserById(4L);
//    userMapper.deleteUserById(5L);
//    userMapper.deleteUserById(6L);
//    userMapper.deleteUserById(7L);
//    userMapper.deleteUserById(9L);
//    userMapper.deleteUserById(8L);
//
//    ordersMapper.deleteByPrimaryKey("510202563513111111");
//    ordersMapper.deleteByPrimaryKey("510202563513222222");
//    ordersMapper.deleteByPrimaryKey("510202563513333333");
//    ordersMapper.deleteByPrimaryKey("510202563513444444");
//    ordersMapper.deleteByPrimaryKey("510202563513555555");
//    ordersMapper.deleteByPrimaryKey("510202563513888888");
//    ordersMapper.deleteByPrimaryKey("510202563513666666");
//    ordersMapper.deleteByPrimaryKey("510202563513777777");
//    ordersMapper.deleteByPrimaryKey("510202563513999999");
//    ordersMapper.deleteByPrimaryKey("510202563513656565");
//
//    userIncomeMapper.deleteByTradeId("510202563513111111");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513222222");
//    userIncomeMapper.deleteByTradeId("510202563513555555");
//    userIncomeMapper.deleteByTradeId("510202563513888888");
//    userIncomeMapper.deleteByTradeId("510202563513666666");
//    userIncomeMapper.deleteByTradeId("510202563513777777");
//    userIncomeMapper.deleteByTradeId("510202563513999999");
//    userIncomeMapper.deleteByTradeId("510202563513656565");
//  }

  private User buildUser(
      Long userId,
      Long relationId , Long specialId,
      Long parentUserId, String phone,
      String taobaoUserId
  ){
    User user = new User();
    user.setUserId(userId);
    user.setUserNickName("mmm1");
    user.setUserStatus(UserContant.USER_STATUS_NORMAL);
    user.setIsAuth(UserContant.IS_AUTH);
    user.setRelationId(relationId);
    user.setSpecialId(specialId);
    user.setParentUserId(parentUserId);
    user.setTotalFansOrdersNum(0);
    user.setUserIntgeralTreasure(0L);
    user.setPhone(phone);
    user.setOrdersNum(0L);
    user.setUserGradeId(1);
    user.setUserIntgeral(0L);
    user.setUserAmount(new BigDecimal("0.00"));
    user.setUserIncome(new BigDecimal("0.00"));
    user.setUserCommision(new BigDecimal("0.00"));
    user.setTaobaoUserId(taobaoUserId);
    user.setCreateTime(new Date());
    user.setUpdateTime(new Date());
    return user;
  }

  private Orders buildInitialOrders(){
    Orders orders = new Orders();
    orders.setTradeId("510202563513111111");
    orders.setAdzoneId(109205150467L);
    orders.setAdzoneName(" 渠道ID专属");
    orders.setClickTime("2020-4-27 00:50:0");
    orders.setTkPaidTime("2020-4-27 00:50:10");
    orders.setTbPaidTime("2020-4-27 00:50:10");
    orders.setCreateTime("2020-4-27 00:50:10");
    orders.setTkStatus(12L);
    orders.setRelationId(1L);
    orders.setPubSharePreFee(new BigDecimal("100"));
    orders.setTotalCommissionRate(new BigDecimal("10"));
    orders.setTotalCommissionFee(new BigDecimal("100"));
    orders.setRefundTag(0L);
    orders.setPayPrice(new BigDecimal("1000"));
    orders.setAlipayTotalPrice(new BigDecimal("1000"));
    orders.setItemNum(1L);
    orders.setItemImg("111");
    orders.setItemTitle("111");
    orders.setItemLink("111");
    orders.setOrderType("天猫");
    orders.setSiteId(620000087L);
    return orders;
  }


  private UserIncome getUserIncomeByUserId(Long userId, List<UserIncome> incomeList){

    for(UserIncome userIncome: incomeList){
      if(userIncome.getUserId().equals(userId)){
        return userIncome;
      }
    }
    return null;
  }

  private void compareUserIncome(UserIncome userIncome,
      Long userId, BigDecimal money,
      Long relationId, Long specialId,
      String tradeId, String parentTradeId,
      Long tkStatus, Integer status
  ){

    logger.debug("money is : {}",money);
    logger.debug("userIncome money is : {}",userIncome.getMoney());
    Assert.assertEquals(userId,userIncome.getUserId());
    Assert.assertTrue(money.compareTo(userIncome.getMoney()) == 0);
    Assert.assertEquals(relationId,userIncome.getRelationId());
    Assert.assertEquals(specialId,userIncome.getSpecialId());
    Assert.assertEquals(tradeId,userIncome.getTradeId());
    Assert.assertEquals(parentTradeId,userIncome.getParentTradeId());
    Assert.assertEquals(tkStatus,userIncome.getTkStatus());
    Assert.assertEquals(status,userIncome.getStatus());
    Assert.assertEquals(Integer.valueOf(UserIncomeConstant.ORDERS_AWARD_TYPE),userIncome.getType());
    Assert.assertEquals(Integer.valueOf(UserIncomeConstant.UIT_AWARD_TYPE),userIncome.getAwardType());
  }

  /**
   * 校验拉新奖励
   */
  @Test
  public void processRelationTkOrderRsponse_14(){
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    ordersList.add(orders);
    rsp.setData(ordersList);

    orders.setTkStatus(3L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList.size());
    logger.debug("incomeList is : {}",incomeList);
    // 拉新奖励 -- 第一个人 奖励0
    UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(2L, 3);
    Assert.assertTrue(new BigDecimal("0.00").compareTo(userIncome.getMoney()) == 0);

    orders.setTkStatus(3L);
    orders.setTradeId("510202563513666666");
    orders.setRelationId(6L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList2 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList2.size());
    // 拉新奖励 -- 第二个人 奖励0
    UserIncome userIncome1 = userIncomeMapper.selectUserIncomeByTradeIdAndType(orders.getTradeId());
    Assert.assertTrue(new BigDecimal("0.00").compareTo(userIncome1.getMoney()) == 0);
    UserIncome u11 = getUserIncomeByUserId(1L,incomeList2);

    orders.setTkStatus(3L);
    orders.setTradeId("510202563513999999");
    orders.setRelationId(9L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList3 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList3.size());
    logger.debug("incomeList is : {}",incomeList3);
    // 拉新奖励 -- 第三个人 奖励0
    UserIncome userIncome3 = userIncomeMapper.selectUserIncomeByTradeIdAndType(orders.getTradeId());
    Assert.assertTrue(new BigDecimal("0.00").compareTo(userIncome3.getMoney()) == 0);


    orders.setTkStatus(3L);
    orders.setTradeId("510202563513777777");
    orders.setRelationId(7L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList4 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList4.size());
    logger.debug("incomeList is : {}",incomeList4);
    // 拉新奖励 -- 第四个人 奖励50
    UserIncome userIncome4 = userIncomeMapper.selectUserIncomeByTradeIdAndType(orders.getTradeId());
    logger.debug("userIncome4 is : {}",userIncome4);
    Assert.assertTrue(new BigDecimal("5.00").compareTo(userIncome4.getMoney()) == 0);

  }

  /**
   *  --> 渠道订单
   *  --> 付款 --> 结算
   *
   *  购买人 userId 1 , relationId 1
   *  pUser 2 pPuser 3
   *
   *
   */
  @Test
  public void processRelationTkOrderRsponse() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);


  }


  /**
   *  --> 渠道订单
   *  --> 付款 --> 结算
   *
   *  购买人 userId 1 , relationId 2
   *  pUser 3 pPuser 4
   *
   *
   */
  @Test
  public void processRelationTkOrderRsponse_2() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(2L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u1,2L, new BigDecimal(75),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u2,3L, new BigDecimal(4),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(4L,incomeList);
    compareUserIncome(u3,4L, new BigDecimal(1),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u1,2L, new BigDecimal(75),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u2,3L, new BigDecimal(4),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(4L,incomeList);
    compareUserIncome(u3,4L, new BigDecimal(1),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);


  }

  /**
   *  --> 渠道订单
   *  --> 付款 --> 结算 --> 失效
   *
   *  购买人 userId 1 , relationId 2
   *  pUser 3 pPuser 4
   *
   *
   */
  @Test
  public void processRelationTkOrderRsponse_3() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(2L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u1,2L, new BigDecimal(75),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u2,3L, new BigDecimal(4),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(4L,incomeList);
    compareUserIncome(u3,4L, new BigDecimal(1),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u1,2L, new BigDecimal(75),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u2,3L, new BigDecimal(4),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(4L,incomeList);
    compareUserIncome(u3,4L, new BigDecimal(1),2L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    orders.setTkStatus(13L);
    overdueOrdersImporter.processRelationTkOrderRsponse(rsp);
    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
        Assert.assertEquals(0,incomeList.size());

  }


  /**
   *  --> 渠道订单
   *  --> 结算
   *
   *  购买人 userId 1 , relationId 1
   *  pUser 2 pPuser 3
   *
   *
   */
  @Test
  public void processRelationTkOrderRsponse_4() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(3L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),1L,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }


  /**
   *  --> 渠道订单
   *  --> 失效
   *
   *  购买人 userId 1 , relationId 1
   *  pUser 2 pPuser 3
   *
   *
   */
  @Test
  public void processRelationTkOrderRsponse_5() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(13L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    overdueOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());

  }

  /**
   *  --> 渠道订单
   *  渠道id为空
   *
   *
   */
  @Test
  public void processRelationTkOrderRsponse_6() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setRelationId(null);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);
    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(0));

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());

  }

  /**
   *  --> 渠道订单
   *  渠道id为空  -- 找不到用户，用户找回订单后绑定上用户信息，订单进入到结算状态，进入订单处理逻辑冻结佣金
   *
   */
  @Test
  public void processRelationTkOrderRsponse_12() {

    // 准备一笔没找到用户的订单
    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setRelationId(null);
    orders.setPromoterId(5L);
    orders.setPromoterName("mmm1");
    orders.setUserUit(7500L);
    orders.setUserCommission(new BigDecimal("75.00"));
    orders.setIsRecord(2);
    ordersMapper.insertSelective(orders);
    // 准备收益记录
    UserIncome userIncome = new UserIncome();
    userIncome.setStatus(0);
    userIncome.setTkStatus(12L);
    userIncome.setType(1);
    userIncome.setTradeId(orders.getTradeId());
    userIncome.setParentTradeId(orders.getTradeId());
    userIncome.setUserId(5L);
    userIncome.setCreateTime("2020-08-10 00:00:00");
    userIncome.setUpdateTime("2020-08-15 00:00:00");
    userIncomeMapper.insertSelective(userIncome);

    // 手动绑定订单后，订单进入结算状态
    TkOrderRsponse rsp2  = new TkOrderRsponse();
    rsp2.setHas_next(false);
    rsp2.setPosition_index("---");
    List<Orders> ordersList2 = new ArrayList<>();
    // 创建初始化订单
    Orders orders2 = buildInitialOrders();
    orders2.setTkStatus(3L);
    orders2.setRelationId(null);
    ordersList2.add(orders2);
    rsp2.setData(ordersList2);
    acOrdersImporter.processRelationTkOrderRsponse(rsp2);
    Orders dbOrder2 = ordersMapper.selectByPrimaryKey(orders2.getTradeId());
    Assert.assertEquals(orders2.getTradeId(),dbOrder2.getTradeId());
    Assert.assertEquals(orders2.getAdzoneId(),dbOrder2.getAdzoneId());
    Assert.assertEquals(orders2.getAdzoneName(),dbOrder2.getAdzoneName());
    Assert.assertEquals(orders2.getClickTime(),dbOrder2.getClickTime());
    Assert.assertEquals(orders2.getTkPaidTime(),dbOrder2.getTkPaidTime());
    Assert.assertEquals(orders2.getTbPaidTime(),dbOrder2.getTbPaidTime());
    Assert.assertEquals(orders2.getCreateTime(),dbOrder2.getCreateTime());
    Assert.assertEquals(orders2.getTkStatus(),dbOrder2.getTkStatus());
    Assert.assertEquals(orders2.getRelationId(),dbOrder2.getRelationId());
    Assert.assertTrue(orders2.getPubSharePreFee().compareTo(dbOrder2.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders2.getTotalCommissionRate().compareTo(dbOrder2.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders2.getTotalCommissionFee().compareTo(dbOrder2.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders2.getRefundTag(),dbOrder2.getRefundTag());
    Assert.assertTrue(orders2.getPayPrice().compareTo(dbOrder2.getPayPrice()) == 0);
    Assert.assertEquals(orders2.getItemNum(),dbOrder2.getItemNum());
    Assert.assertEquals(orders2.getItemImg(),dbOrder2.getItemImg());
    Assert.assertEquals(orders2.getItemTitle(),dbOrder2.getItemTitle());
    Assert.assertEquals(orders2.getItemLink(),dbOrder2.getItemLink());
    Assert.assertEquals(orders2.getOrderType(),dbOrder2.getOrderType());
    Assert.assertEquals(dbOrder2.getUserUit(),Long.valueOf(7500));
    Assert.assertTrue(dbOrder2.getUserCommission().compareTo(new BigDecimal("75.00")) == 0);
    Assert.assertEquals(dbOrder2.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder2.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder2.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder2.getTwoLevelCommission(), null);
    Assert.assertEquals(dbOrder2.getIsRecord(), Integer.valueOf(2));

    List<UserIncome> incomeList2 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    logger.debug("incomeList2 is : {}",incomeList2);
    Assert.assertEquals(1,incomeList2.size());

    UserIncome userIncomeByUserId = getUserIncomeByUserId(5L, incomeList2);
    Assert.assertTrue(new BigDecimal("75.00").compareTo(userIncomeByUserId.getMoney()) == 0);
    Assert.assertEquals(Long.valueOf(3),userIncomeByUserId.getTkStatus());
    Assert.assertEquals(Integer.valueOf(2),userIncomeByUserId.getStatus());

    // 佣金冻结中
//    User user = userMapper.selectById(5L);
//    Assert.assertEquals(Long.valueOf(7500),user.getUserIntgeralTreasure());

  }


    /**
     *  --> 渠道订单
     *  淘宝id 重复
     *  根据渠道id查询到用户信息
     *
     */
  @Test
  public void processRelationTkOrderRsponse_7() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setTradeId("510202563513555555");
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf(7500));
    Assert.assertTrue(new BigDecimal("75.00").compareTo(dbOrder.getUserCommission()) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf(400));
    Assert.assertTrue(new BigDecimal("4.00").compareTo(dbOrder.getOneLevelCommission()) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), Long.valueOf(100));
    Assert.assertTrue(new BigDecimal("1.00").compareTo(dbOrder.getTwoLevelCommission()) == 0);
    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(1));

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    orders.setTkStatus(3L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

//    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());
    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());

  }

  /**
   *  --> 渠道订单
   *  淘宝id 重复
   *  没有根据渠道id查询到用户信息,直接入库
   *
   */
  @Test
  public void processRelationTkOrderRsponse_8() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setTradeId("510202563513555555");
    orders.setRelationId(1111L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);
    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(0));
  }

  /**
   *  --> 渠道订单
   *  淘宝id -> 不重复
   *  渠道id为空
   *
   */
  @Test
  public void processRelationTkOrderRsponse_9() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setRelationId(null);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);
    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);
    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(0));

  }

  /**
   *  --> 渠道订单
   *  淘宝id -> 不重复
   *  渠道id不为空 -> 根据渠道id查询到用户信息
   *
   */
  @Test
  public void processRelationTkOrderRsponse_10() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setTradeId("510202563513111111");
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);
    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf(7500));
    Assert.assertTrue(new BigDecimal("75.00").compareTo(dbOrder.getUserCommission()) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf(400));
    Assert.assertTrue(new BigDecimal("4.00").compareTo(dbOrder.getOneLevelCommission()) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), Long.valueOf(100));
    Assert.assertTrue(new BigDecimal("1.00").compareTo(dbOrder.getTwoLevelCommission()) == 0);
    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(1));

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    orders.setTkStatus(3L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList.size());
    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
  }

  /**
   * 渠道订单
   * 淘宝id不重复  -> 根据渠道id没查询到用户信息-直接入库
   *
   */
  @Test
  public void processRelationTkOrderRsponse_11(){
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setTradeId("510202563513111111");
    orders.setRelationId(1111L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processRelationTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);
    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(0));
  }

//  /**
//   * 常规订单 --> 渠道订单
//   * 常规订单 -- 无渠道，根据淘宝后六位查询不到用户信息
//   * 变为渠道订单--携带渠道id -- 逻辑已更改，此方法不可用
//   */
//  @Test
//  public void processRelationTkOrderRsponse_12(){
//    TkOrderRsponse rsp  = new TkOrderRsponse();
//    rsp.setHas_next(false);
//    rsp.setPosition_index("---");
//    // 第一次拉到是常规订单,且没查询到用户，直接入库
//    List<Orders> ordersList = new ArrayList<>();
//    Orders orders = buildInitialOrders();
//    orders.setRelationId(null);
//    orders.setTkStatus(12L);
//    orders.setTradeId("510202563513888888");
//    ordersList.add(orders);
//    rsp.setData(ordersList);
//    paidOrdersImporter.processNormalTkOrderRsponse(rsp);
//    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
//    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
//    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
//    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
//    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
//    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
//    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
//    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
//    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
//    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
//    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
//    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
//    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
//    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
//    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
//    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
//    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
//    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
//    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
//    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
//    Assert.assertEquals(dbOrder.getUserUit(),null);
//    Assert.assertEquals(dbOrder.getUserCommission(),null);
//    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
//    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
//    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
//    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);
//    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(0));
//
//    // 第二次被判定为渠道订单，携带渠道id
//    TkOrderRsponse rsp2  = new TkOrderRsponse();
//    rsp2.setHas_next(false);
//    rsp2.setPosition_index("---");
//
//    List<Orders> ordersList2 = new ArrayList<>();
//
//    // 创建初始化订单
//    Orders orders2 = buildInitialOrders();
//    orders2.setTkStatus(12L);
//    orders2.setTradeId("510202563513888888");
//    ordersList2.add(orders2);
//    rsp2.setData(ordersList2);
//
//    paidOrdersImporter.processRelationTkOrderRsponse(rsp2);
//    Orders dbOrder2 = ordersMapper.selectByPrimaryKey(orders2.getTradeId());
//    Assert.assertEquals(orders2.getTradeId(),dbOrder2.getTradeId());
//    Assert.assertEquals(orders2.getAdzoneId(),dbOrder2.getAdzoneId());
//    Assert.assertEquals(orders2.getAdzoneName(),dbOrder2.getAdzoneName());
//    Assert.assertEquals(orders2.getClickTime(),dbOrder2.getClickTime());
//    Assert.assertEquals(orders2.getTkPaidTime(),dbOrder2.getTkPaidTime());
//    Assert.assertEquals(orders2.getTbPaidTime(),dbOrder2.getTbPaidTime());
//    Assert.assertEquals(orders2.getCreateTime(),dbOrder2.getCreateTime());
//    Assert.assertEquals(orders2.getTkStatus(),dbOrder2.getTkStatus());
//    Assert.assertEquals(orders2.getRelationId(),dbOrder2.getRelationId());
//    Assert.assertTrue(orders2.getPubSharePreFee().compareTo(dbOrder2.getPubSharePreFee()) == 0);
//    Assert.assertTrue(orders2.getTotalCommissionRate().compareTo(dbOrder2.getTotalCommissionRate()) == 0);
//    Assert.assertTrue(orders2.getTotalCommissionFee().compareTo(dbOrder2.getTotalCommissionFee()) == 0);
//    Assert.assertEquals(orders2.getRefundTag(),dbOrder2.getRefundTag());
//    Assert.assertTrue(orders2.getPayPrice().compareTo(dbOrder2.getPayPrice()) == 0);
//    Assert.assertEquals(orders2.getItemNum(),dbOrder2.getItemNum());
//    Assert.assertEquals(orders2.getItemImg(),dbOrder2.getItemImg());
//    Assert.assertEquals(orders2.getItemTitle(),dbOrder2.getItemTitle());
//    Assert.assertEquals(orders2.getItemLink(),dbOrder2.getItemLink());
//    Assert.assertEquals(orders2.getOrderType(),dbOrder2.getOrderType());
//    Assert.assertEquals(dbOrder2.getUserUit(),Long.valueOf(7500));
//    Assert.assertTrue(new BigDecimal("75.00").compareTo(dbOrder2.getUserCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getOneLevelUserUit(),Long.valueOf(400));
//    Assert.assertTrue(new BigDecimal("4.00").compareTo(dbOrder2.getOneLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getTwoLevelUserUit(), Long.valueOf(100));
//    Assert.assertTrue(new BigDecimal("1.00").compareTo(dbOrder2.getTwoLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getIsRecord(), Integer.valueOf(1));
//
//    List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("510202563513888888");
//    UserIncome userIncomeByUserId = getUserIncomeByUserId(1L, userIncomes);
//    Assert.assertEquals(Long.valueOf(1),userIncomeByUserId.getUserId());
//    Assert.assertEquals(Integer.valueOf(2),userIncomeByUserId.getAwardType());
//    Assert.assertTrue(new BigDecimal("75.00").compareTo(userIncomeByUserId.getMoney()) == 0);
//  }

  /**
   * 渠道订单
   * 第一次 -- 用户付款定金金额
   * 第二次 -- 用户尾款金额
   * 预估佣金发生改变 ， 订单状态一致，也会进行处理更新订单与用户收益
   *
   * 相同状态下进行处理，会出问题，每次导入结算订单都会给用户添加集分宝
   * 付款状态下，第二次处理时会判定为冻结，也会多给用户集分宝
   */
//  @Test
//  public void processRelationTkOrderRsponse_13(){
//    TkOrderRsponse rsp  = new TkOrderRsponse();
//    rsp.setHas_next(false);
//    rsp.setPosition_index("---");
//
//    List<Orders> ordersList = new ArrayList<>();
//
//    // 创建初始化订单
//    Orders orders = buildInitialOrders();
//    orders.setTkStatus(12L);
//    orders.setTradeId("510202563513111111");
//    ordersList.add(orders);
//    rsp.setData(ordersList);
//
//    paidOrdersImporter.processRelationTkOrderRsponse(rsp);
//    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
//    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
//    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
//    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
//    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
//    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
//    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
//    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
//    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
//    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
//    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
//    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
//    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
//    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
//    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
//    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
//    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
//    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
//    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
//    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
//    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf(7500));
//    Assert.assertTrue(new BigDecimal("75.00").compareTo(dbOrder.getUserCommission()) == 0);
//    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf(400));
//    Assert.assertTrue(new BigDecimal("4.00").compareTo(dbOrder.getOneLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), Long.valueOf(100));
//    Assert.assertTrue(new BigDecimal("1.00").compareTo(dbOrder.getTwoLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(1));
//
//    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());
//    UserIncome userIncome4 = getUserIncomeByUserId(1L, incomeList);
//    logger.debug("incomeList2 is : {}",incomeList);
//    Assert.assertTrue(new BigDecimal("75.00").compareTo(userIncome4.getMoney()) == 0);
//    Assert.assertEquals(Long.valueOf(1),userIncome4.getUserId());
//    UserIncome userIncome5 = getUserIncomeByUserId(2L, incomeList);
//    Assert.assertTrue(new BigDecimal("4.00").compareTo(userIncome5.getMoney()) == 0);
//    Assert.assertEquals(Long.valueOf(2),userIncome5.getUserId());
//    UserIncome userIncome6 = getUserIncomeByUserId(3L, incomeList);
//    Assert.assertTrue(new BigDecimal("1.00").compareTo(userIncome6.getMoney()) == 0);
//    Assert.assertEquals(Long.valueOf(3),userIncome6.getUserId());
//
//
//    // 第二次预估佣金改变
//    TkOrderRsponse rsp2  = new TkOrderRsponse();
//    rsp2.setHas_next(false);
//    rsp2.setPosition_index("---");
//
//    List<Orders> ordersList2 = new ArrayList<>();
//
//    // 创建初始化订单
//    Orders orders2 = buildInitialOrders();
//    orders2.setTkStatus(12L);
//    orders2.setPubSharePreFee(new BigDecimal("300"));
//    orders2.setPubShareFee(new BigDecimal("300"));
//    orders2.setTradeId("510202563513111111");
//    ordersList2.add(orders2);
//    rsp2.setData(ordersList2);
//
//    paidOrdersImporter.processRelationTkOrderRsponse(rsp2);
//    Orders dbOrder2 = ordersMapper.selectByPrimaryKey(orders2.getTradeId());
//    Assert.assertEquals(orders2.getTradeId(),dbOrder2.getTradeId());
//    Assert.assertEquals(orders2.getAdzoneId(),dbOrder2.getAdzoneId());
//    Assert.assertEquals(orders2.getAdzoneName(),dbOrder2.getAdzoneName());
//    Assert.assertEquals(orders2.getClickTime(),dbOrder2.getClickTime());
//    Assert.assertEquals(orders2.getTkPaidTime(),dbOrder2.getTkPaidTime());
//    Assert.assertEquals(orders2.getTbPaidTime(),dbOrder2.getTbPaidTime());
//    Assert.assertEquals(orders2.getCreateTime(),dbOrder2.getCreateTime());
//    Assert.assertEquals(orders2.getTkStatus(),dbOrder2.getTkStatus());
//    Assert.assertEquals(orders2.getRelationId(),dbOrder2.getRelationId());
//    Assert.assertTrue(orders2.getPubSharePreFee().compareTo(dbOrder2.getPubSharePreFee()) == 0);
//    Assert.assertTrue(orders2.getTotalCommissionRate().compareTo(dbOrder2.getTotalCommissionRate()) == 0);
//    Assert.assertTrue(orders2.getTotalCommissionFee().compareTo(dbOrder2.getTotalCommissionFee()) == 0);
//    Assert.assertEquals(orders2.getRefundTag(),dbOrder2.getRefundTag());
//    Assert.assertTrue(orders2.getPayPrice().compareTo(dbOrder2.getPayPrice()) == 0);
//    Assert.assertEquals(orders2.getItemNum(),dbOrder2.getItemNum());
//    Assert.assertEquals(orders2.getItemImg(),dbOrder2.getItemImg());
//    Assert.assertEquals(orders2.getItemTitle(),dbOrder2.getItemTitle());
//    Assert.assertEquals(orders2.getItemLink(),dbOrder2.getItemLink());
//    Assert.assertEquals(orders2.getOrderType(),dbOrder2.getOrderType());
//    Assert.assertEquals(dbOrder2.getUserUit(),Long.valueOf(22500));
//    logger.debug("dbOrders is : {}",dbOrder2);
//    Assert.assertTrue(new BigDecimal("225.00").compareTo(dbOrder2.getUserCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getOneLevelUserUit(),Long.valueOf(1200));
//    Assert.assertTrue(new BigDecimal("12.00").compareTo(dbOrder2.getOneLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getTwoLevelUserUit(), Long.valueOf(300));
//    Assert.assertTrue(new BigDecimal("3.00").compareTo(dbOrder2.getTwoLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getIsRecord(), Integer.valueOf(1));
//
//    List<UserIncome> incomeList2 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());
//
//    // 收益记录也相应修改
//    UserIncome userIncome = getUserIncomeByUserId(1L, incomeList2);
//    logger.debug("incomeList2 is : {}",incomeList2);
//    Assert.assertTrue(new BigDecimal("225.00").compareTo(userIncome.getMoney()) == 0);
//    Assert.assertEquals(Long.valueOf(1),userIncome.getUserId());
//    UserIncome userIncome2 = getUserIncomeByUserId(2L, incomeList2);
//    Assert.assertTrue(new BigDecimal("12.00").compareTo(userIncome2.getMoney()) == 0);
//    Assert.assertEquals(Long.valueOf(2),userIncome2.getUserId());
//    UserIncome userIncome3 = getUserIncomeByUserId(3L, incomeList2);
//    Assert.assertTrue(new BigDecimal("3.00").compareTo(userIncome3.getMoney()) == 0);
//    Assert.assertEquals(Long.valueOf(3),userIncome3.getUserId());
//
//  }

//  /**
//   * 常规订单
//   * 第一次 -- 用户付款定金金额
//   * 第二次 -- 用户尾款金额
//   * 预估佣金发生改变 ， 订单状态一致，也会进行处理更新订单与用户收益
//   */
//  @Test
//  public void processNormalTkOrderRsponse_6(){
//    TkOrderRsponse rsp  = new TkOrderRsponse();
//    rsp.setHas_next(false);
//    rsp.setPosition_index("---");
//
//    List<Orders> ordersList = new ArrayList<>();
//
//    // 创建初始化订单
//    Orders orders = buildInitialOrders();
//    orders.setRelationId(null);
//    ordersList.add(orders);
//    rsp.setData(ordersList);
//
//    paidOrdersImporter.processNormalTkOrderRsponse(rsp);
//
//    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
//    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
//    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
//    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
//    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
//    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
//    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
//    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
//    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
//    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
//    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
//    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
//    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
//    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
//    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
//    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
//    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
//    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
//    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
//    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
//    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
//    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
//    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
//    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
//    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
//    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);
//
//    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());
//
//    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
//    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);
//
//    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
//    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);
//
//    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
//    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);
//
//
//    // 第二次预估佣金改变
//    TkOrderRsponse rsp2  = new TkOrderRsponse();
//    rsp2.setHas_next(false);
//    rsp2.setPosition_index("---");
//
//    List<Orders> ordersList2 = new ArrayList<>();
//
//    // 创建初始化订单
//    Orders orders2 = buildInitialOrders();
//    orders2.setTkStatus(12L);
//    orders2.setRelationId(null);
//    orders2.setPubSharePreFee(new BigDecimal("300"));
//    orders2.setPubShareFee(new BigDecimal("300"));
//    orders2.setTradeId("510202563513111111");
//    ordersList2.add(orders2);
//    rsp2.setData(ordersList2);
//
//    paidOrdersImporter.processRelationTkOrderRsponse(rsp2);
//    Orders dbOrder2 = ordersMapper.selectByPrimaryKey(orders2.getTradeId());
//    Assert.assertEquals(orders2.getTradeId(),dbOrder2.getTradeId());
//    Assert.assertEquals(orders2.getAdzoneId(),dbOrder2.getAdzoneId());
//    Assert.assertEquals(orders2.getAdzoneName(),dbOrder2.getAdzoneName());
//    Assert.assertEquals(orders2.getClickTime(),dbOrder2.getClickTime());
//    Assert.assertEquals(orders2.getTkPaidTime(),dbOrder2.getTkPaidTime());
//    Assert.assertEquals(orders2.getTbPaidTime(),dbOrder2.getTbPaidTime());
//    Assert.assertEquals(orders2.getCreateTime(),dbOrder2.getCreateTime());
//    Assert.assertEquals(orders2.getTkStatus(),dbOrder2.getTkStatus());
//    Assert.assertEquals(orders2.getRelationId(),dbOrder2.getRelationId());
//    Assert.assertTrue(orders2.getPubSharePreFee().compareTo(dbOrder2.getPubSharePreFee()) == 0);
//    Assert.assertTrue(orders2.getTotalCommissionRate().compareTo(dbOrder2.getTotalCommissionRate()) == 0);
//    Assert.assertTrue(orders2.getTotalCommissionFee().compareTo(dbOrder2.getTotalCommissionFee()) == 0);
//    Assert.assertEquals(orders2.getRefundTag(),dbOrder2.getRefundTag());
//    Assert.assertTrue(orders2.getPayPrice().compareTo(dbOrder2.getPayPrice()) == 0);
//    Assert.assertEquals(orders2.getItemNum(),dbOrder2.getItemNum());
//    Assert.assertEquals(orders2.getItemImg(),dbOrder2.getItemImg());
//    Assert.assertEquals(orders2.getItemTitle(),dbOrder2.getItemTitle());
//    Assert.assertEquals(orders2.getItemLink(),dbOrder2.getItemLink());
//    Assert.assertEquals(orders2.getOrderType(),dbOrder2.getOrderType());
//    Assert.assertEquals(dbOrder2.getUserUit(),Long.valueOf(22500));
//    logger.debug("dbOrders is : {}",dbOrder2);
//    Assert.assertTrue(new BigDecimal("225.00").compareTo(dbOrder2.getUserCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getOneLevelUserUit(),Long.valueOf(1200));
//    Assert.assertTrue(new BigDecimal("12.00").compareTo(dbOrder2.getOneLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getTwoLevelUserUit(), Long.valueOf(300));
//    Assert.assertTrue(new BigDecimal("3.00").compareTo(dbOrder2.getTwoLevelCommission()) == 0);
//    Assert.assertEquals(dbOrder2.getIsRecord(), Integer.valueOf(1));
//
//    List<UserIncome> incomeList2 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList2.size());
//
//    UserIncome u4 = getUserIncomeByUserId(1L,incomeList2);
//    compareUserIncome(u4,1L, new BigDecimal("225.00"),null,null,orders2.getTradeId(),orders2.getTradeParentId(),orders2.getTkStatus(),UserIncomeConstant.FREEZE);
//
//    UserIncome u5 = getUserIncomeByUserId(2L,incomeList2);
//    compareUserIncome(u5,2L, new BigDecimal("12.00"),null,null,orders2.getTradeId(),orders2.getTradeParentId(),orders2.getTkStatus(),UserIncomeConstant.FREEZE);
//
//    UserIncome u6 = getUserIncomeByUserId(3L,incomeList2);
//    compareUserIncome(u6,3L, new BigDecimal("3.00"),null,null,orders2.getTradeId(),orders2.getTradeParentId(),orders2.getTkStatus(),UserIncomeConstant.FREEZE);
//
//  }

  /**
   * 常规订单
   * 购买人 userId 1
   * pUserId 2 , pPuserId 3
   */
  @Test
  public void processNormalTkOrderRsponse() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(null);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processNormalTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }

  /**
   * 常规订单
   * 购买人 userId 1
   * pUserId 2 , pPuserId 3
   */
  @Test
  public void processNormalTkOrderRsponse_1() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(null);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processNormalTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    orders.setTkStatus(13L);
    overdueOrdersImporter.processNormalTkOrderRsponse(rsp);
    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());
  }

  /**
   *  --> 常规订单
   *  --> 结算
   *
   *  购买人 userId 1 , relationId 1
   *  pUser 2 pPuser 3
   *
   *
   */
  @Test
  public void processNormalTkOrderRsponse_2() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(3L);
    orders.setRelationId(null);
    ordersList.add(orders);
    rsp.setData(ordersList);

    acOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }


  /**
   *  --> 常规订单
   *  --> 失效
   *
   *  购买人 userId 1 , relationId 1
   *  pUser 2 pPuser 3
   *
   *
   */
  @Test
  public void processNormalTkOrderRsponse_3() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(13L);
    orders.setRelationId(null);
    ordersList.add(orders);
    rsp.setData(ordersList);

    overdueOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());

  }

  /**
   *  --> 常规订单
   *  淘宝id 重复
   *
   *
   */
  @Test
  public void processNormalTkOrderRsponse_4() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(12L);
    orders.setRelationId(null);
    orders.setTradeId("510202563513555555");
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);
    Assert.assertEquals(dbOrder.getIsRecord(), Integer.valueOf(0));

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());

    orders.setTkStatus(3L);
    acOrdersImporter.processNormalTkOrderRsponse(rsp);

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());
    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());

  }

  /**
   * 常规订单 -- 根据淘宝后六位查询找不到用户
   */
  @Test
  public void processNormalTkOrderRsponse_6(){
    // 准备一笔没找到用户的订单
    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTradeParentId("510202563513656565");
    orders.setTradeId("510202563513656565");
    orders.setTkStatus(12L);
    orders.setRelationId(null);
    orders.setPromoterId(5L);
    orders.setPromoterName("mmm1");
    orders.setUserUit(7500L);
    orders.setUserCommission(new BigDecimal("75.00"));
    orders.setIsRecord(2);
    ordersMapper.insertSelective(orders);
    // 准备收益记录
    UserIncome userIncome = new UserIncome();
    userIncome.setStatus(0);
    userIncome.setTkStatus(12L);
    userIncome.setType(1);
    userIncome.setTradeId(orders.getTradeId());
    userIncome.setParentTradeId(orders.getTradeId());
    userIncome.setUserId(5L);
    userIncome.setCreateTime("2020-08-10 00:00:00");
    userIncome.setUpdateTime("2020-08-15 00:00:00");
    userIncomeMapper.insertSelective(userIncome);


    // 手动绑定订单后，订单进入结算状态
    TkOrderRsponse rsp2  = new TkOrderRsponse();
    rsp2.setHas_next(false);
    rsp2.setPosition_index("---");
    List<Orders> ordersList2 = new ArrayList<>();
    // 创建初始化订单
    Orders orders2 = buildInitialOrders();
    orders2.setTradeParentId("510202563513656565");
    orders2.setTradeId("510202563513656565");
    orders2.setTkStatus(3L);
    orders2.setRelationId(null);
    ordersList2.add(orders2);
    rsp2.setData(ordersList2);
    acOrdersImporter.processNormalTkOrderRsponse(rsp2);
    Orders dbOrder2 = ordersMapper.selectByPrimaryKey(orders2.getTradeId());
    Assert.assertEquals(orders2.getTradeId(),dbOrder2.getTradeId());
    Assert.assertEquals(orders2.getAdzoneId(),dbOrder2.getAdzoneId());
    Assert.assertEquals(orders2.getAdzoneName(),dbOrder2.getAdzoneName());
    Assert.assertEquals(orders2.getClickTime(),dbOrder2.getClickTime());
    Assert.assertEquals(orders2.getTkPaidTime(),dbOrder2.getTkPaidTime());
    Assert.assertEquals(orders2.getTbPaidTime(),dbOrder2.getTbPaidTime());
    Assert.assertEquals(orders2.getCreateTime(),dbOrder2.getCreateTime());
    Assert.assertEquals(orders2.getTkStatus(),dbOrder2.getTkStatus());
    Assert.assertEquals(orders2.getRelationId(),dbOrder2.getRelationId());
    Assert.assertTrue(orders2.getPubSharePreFee().compareTo(dbOrder2.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders2.getTotalCommissionRate().compareTo(dbOrder2.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders2.getTotalCommissionFee().compareTo(dbOrder2.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders2.getRefundTag(),dbOrder2.getRefundTag());
    Assert.assertTrue(orders2.getPayPrice().compareTo(dbOrder2.getPayPrice()) == 0);
    Assert.assertEquals(orders2.getItemNum(),dbOrder2.getItemNum());
    Assert.assertEquals(orders2.getItemImg(),dbOrder2.getItemImg());
    Assert.assertEquals(orders2.getItemTitle(),dbOrder2.getItemTitle());
    Assert.assertEquals(orders2.getItemLink(),dbOrder2.getItemLink());
    Assert.assertEquals(orders2.getOrderType(),dbOrder2.getOrderType());
    Assert.assertEquals(dbOrder2.getUserUit(),Long.valueOf(7500));
    Assert.assertTrue(dbOrder2.getUserCommission().compareTo(new BigDecimal("75.00")) == 0);
    Assert.assertEquals(dbOrder2.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder2.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder2.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder2.getTwoLevelCommission(), null);
    Assert.assertEquals(dbOrder2.getIsRecord(), Integer.valueOf(2));

    List<UserIncome> incomeList2 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    logger.debug("incomeList2 is : {}",incomeList2);
    Assert.assertEquals(1,incomeList2.size());

    UserIncome userIncomeByUserId = getUserIncomeByUserId(5L, incomeList2);
    Assert.assertTrue(new BigDecimal("75.00").compareTo(userIncomeByUserId.getMoney()) == 0);
    Assert.assertEquals(Long.valueOf(3),userIncomeByUserId.getTkStatus());
    Assert.assertEquals(Integer.valueOf(2),userIncomeByUserId.getStatus());

    // 佣金冻结中
//    User user = userMapper.selectById(5L);
//    Assert.assertEquals(Long.valueOf(7500),user.getUserIntgeralTreasure());
  }

  /**
   * 因为此方法用了数据库的真实数据，只能在测试环境测试
   * 处理渠道订单时 --- 查询渠道订单会员订单是否包含此订单，若包含则不会进行处理也不入库
   * 用户数据库真实数据，作为常规订单处理时不会进入处理逻辑，因为之前已经保存到了数据库，就不比较订单的信息
   * 正常情况下如果是渠道订单或会员订单查询到是不会入库的
   */
  @Test
  public void processNormalTkOrderRsponse_5(){
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");
    List<Orders> ordersList = new ArrayList<>();
    // 此订单是数据库真实数据
    Orders orders = ordersMapper.selectByPrimaryKey("598312006601041697");
    ordersList.add(orders);
    rsp.setData(ordersList);
    paidOrdersImporter.processNormalTkOrderRsponse(rsp);
  }

  @Test
  public void processSpecialTkOrderRsponse() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(null);
    orders.setSpecialId(1L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processSpecialTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processSpecialTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }

  @Test
  public void processSpecialTkOrderRsponse_1() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(null);
    orders.setSpecialId(1L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processSpecialTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processSpecialTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    orders.setTkStatus(13L);
    overdueOrdersImporter.processSpecialTkOrderRsponse(rsp);
    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());

  }

  /**
   *  --> 会员订单
   *  --> 结算
   *
   *  购买人 userId 1 , relationId 1
   *  pUser 2 pPuser 3
   *
   *
   */
  @Test
  public void processSpecialTkOrderRsponse_2() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(3L);
    orders.setRelationId(null);
    orders.setSpecialId(1L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    acOrdersImporter.processSpecialTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,1L,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }


  /**
   *  --> 会员订单
   *  --> 失效
   *
   *  购买人 userId 1 , relationId 1
   *  pUser 2 pPuser 3
   *
   *
   */
  @Test
  public void processSpecialTkOrderRsponse_3() {

    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setTkStatus(13L);
    orders.setRelationId(null);
    orders.setSpecialId(1L);
    ordersList.add(orders);
    rsp.setData(ordersList);

    overdueOrdersImporter.processSpecialTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),null);
    Assert.assertEquals(dbOrder.getUserCommission(),null);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),null);
    Assert.assertEquals(dbOrder.getOneLevelCommission(),null);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(), null);
    Assert.assertEquals(dbOrder.getTwoLevelCommission(), null);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(0,incomeList.size());

  }

  private ActivityProduct buildActivityProduct(Date startTime, Date endTime){
    ActivityProduct product = new ActivityProduct();
    product.setId(1);
    product.setActivityId(1);
    product.setProductId("9595990416");
    product.setShortProductUrl("https://item.taobao.com/item.htm?spm=a219t.11817059.0.dc95490c2.5d8275a5Iq28LJ&id=9595990414&scm=null&pvid=100_11.14.172.66_120012_3971568626115985993&app_pvid=59590_11.1.186.151_31715_1568626115980&ptl=floorId:20311;originalFloorId:20311;pvid:100_11.14.172.66_120012_3971568626115985993;app_pvid:59590_11.1.186.151_31715_1568626115980&union_lens=lensId%3AOPT%401568626116%400b01ba97_0ec6_16d396705f2_bba4%4001");
    product.setShortTitle("广东电信10元话费充值");
    product.setVoucherUrl("https://uland.taobao.com/coupon/edetail?e=TqC4b23ZYIkSq1SNqCiMajJ5c1Of7J2HYHCuomB9NOfDOnaW0ID6pq8DWykFTzaVh2mtO2Msln2dwVCEmPgFhIvQBacu2NEx%2BGoSNEAjokQ4M2%2FUXT47GFZrtTOLgXkcjDppvlX%2Bob%2BBtHA6T8TZnQSD1EQRCW%2BLzEFJTNph2qt7ov5%2F0O2eVTDOaNhNJ6ONPOnbN1rJJNM%3D&traceId=0b01ddd715686261550806539e&union_lens=lensId:0b0b6072_0bbf_16d39679e61_dbb9&xId=xViicNkUOF8jGNZwoNCzZgjR94cCM72IjwhKASvPfk5gYMXsQhiBkznezPs10u8SyPakwXo5T6KRkHayUpPm9o");
    product.setRefundsPrice(new BigDecimal("9.98"));
    product.setAfterPrice(new BigDecimal("9.98"));
    product.setCommissionRatio(0);
    product.setLimitNum(100);
    product.setWeight(1000);
    product.setVirtualSaleNum(500);
    product.setQualType(0);
    product.setQualJson("{}");
    Date date = new Date();
    product.setBeginTime(date);
    product.setEndTime(new Date(date.getTime() + 10000000));
    return product;
  }

  /**
   * 常规0元购活动订单
   * 购买人 userId 1
   * pUserId 2 , pPuserId 3
   */
  @Test
  public void processActivityNormalTkOrderRsponse() {

    LocalDateTime localDateTime = LocalDateTime.now();

    Date date1 =  DateUtil.localDateTime2Date(localDateTime.minusMinutes(30));
    Date date2 =  DateUtil.localDateTime2Date(localDateTime.plusMinutes(30));
    Date date3 =  DateUtil.localDateTime2Date(localDateTime.minusMinutes(1));


    // 数据库存在0元购活动商品
    ActivityProduct activityProduct = buildActivityProduct(date1,date2);
    activityProductMapper.insertSelective(activityProduct);

    // 用户1, 一分钟前点击浏览了活动商品
    ActivityBrowseProduct activityBrowseProduct = new ActivityBrowseProduct();
    activityBrowseProduct.setId(1L);
    activityBrowseProduct.setProductRepoId(null);
    activityBrowseProduct.setJson("{}");
    activityBrowseProduct.setGoodsId(9595990416L);
    activityBrowseProduct.setClickTime(date3);
    activityBrowseProduct.setActivityId(1);
    activityBrowseProduct.setUserId(1L);
    activityBrowseProductMapper.insertSelective(activityBrowseProduct);


    // 用户1 下单
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(null);
    orders.setAdzoneId(Long.valueOf(DingdanxiaUtil.ADZONE_ID_ACTIVITY));
    orders.setNumIid(9595990416L);
    orders.setClickTime(DateUtil.getNowStr());
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("998"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("9.98")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("0"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("0")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("0"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("0")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    logger.debug("incomeList is : {}",incomeList);
    Assert.assertEquals(1,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal("9.98"),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);


    orders.setTkStatus(3L);
    acOrdersImporter.processNormalTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal("9.98"),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }


  /**
   * 常规0元购活动订单, 找不到活动商品
   * 购买人 userId 1
   * pUserId 2 , pPuserId 3
   */
  @Test
  public void processActivityNormalTkOrderRsponse_1() {

    LocalDateTime localDateTime = LocalDateTime.now();

    Date date1 =  DateUtil.localDateTime2Date(localDateTime.minusMinutes(30));
    Date date2 =  DateUtil.localDateTime2Date(localDateTime.plusMinutes(30));
    Date date3 =  DateUtil.localDateTime2Date(localDateTime.minusMinutes(1));


    // 数据库存在0元购活动商品
    ActivityProduct activityProduct = buildActivityProduct(date1,date2);
    activityProductMapper.insertSelective(activityProduct);

    // 用户1, 一分钟前点击浏览了活动商品
    ActivityBrowseProduct activityBrowseProduct = new ActivityBrowseProduct();
    activityBrowseProduct.setId(1L);
    activityBrowseProduct.setProductRepoId(null);
    activityBrowseProduct.setJson("{}");
    activityBrowseProduct.setGoodsId(9595990417L);
    activityBrowseProduct.setClickTime(date3);
    activityBrowseProduct.setActivityId(1);
    activityBrowseProduct.setUserId(1L);
    activityBrowseProductMapper.insertSelective(activityBrowseProduct);


    // 用户1 下单
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(null);
    orders.setAdzoneId(Long.valueOf(DingdanxiaUtil.ADZONE_ID_ACTIVITY));
    orders.setNumIid(9595990417L);
    orders.setClickTime(DateUtil.getNowStr());
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processNormalTkOrderRsponse(rsp);

    paidOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("7500"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("75")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("400"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("4")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("100"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("1")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(3,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    UserIncome u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);

    orders.setTkStatus(3L);
    acOrdersImporter.processNormalTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal(75),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u2 = getUserIncomeByUserId(2L,incomeList);
    compareUserIncome(u2,2L, new BigDecimal(4),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

    u3 = getUserIncomeByUserId(3L,incomeList);
    compareUserIncome(u3,3L, new BigDecimal(1),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }

  /**
   * 常规0元购活动订单
   * 购买人 userId 1
   * pUserId 2 , pPuserId 3
   */
  @Test
  public void processActivityNormalTkOrderRsponse_2() {

    LocalDateTime localDateTime = LocalDateTime.now();

//    Date date1 =  DateUtil.localDateTime2Date(localDateTime.minusMinutes(30));
//    Date date2 =  DateUtil.localDateTime2Date(localDateTime.plusMinutes(30));
    Date date3 =  DateUtil.localDateTime2Date(localDateTime.minusMinutes(1));

    // 用户1, 一分钟前点击浏览了活动商品
    ActivityBrowseProduct activityBrowseProduct = new ActivityBrowseProduct();
    activityBrowseProduct.setId(1L);
    activityBrowseProduct.setProductRepoId(null);
    activityBrowseProduct.setJson("{}");
    activityBrowseProduct.setGoodsId(9595990416L);
    activityBrowseProduct.setClickTime(date3);
    activityBrowseProduct.setActivityId(2);
    activityBrowseProduct.setUserId(1L);
    activityBrowseProductMapper.insertSelective(activityBrowseProduct);


    // 用户1 下单
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    orders.setRelationId(null);
    orders.setAdzoneId(Long.valueOf(DingdanxiaUtil.ADZONE_ID_ACTIVITY));
    orders.setNumIid(9595990416L);
    orders.setClickTime(DateUtil.getNowStr());
    ordersList.add(orders);
    rsp.setData(ordersList);

    paidOrdersImporter.processNormalTkOrderRsponse(rsp);

    Orders dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(orders.getTradeId(),dbOrder.getTradeId());
    Assert.assertEquals(orders.getAdzoneId(),dbOrder.getAdzoneId());
    Assert.assertEquals(orders.getAdzoneName(),dbOrder.getAdzoneName());
    Assert.assertEquals(orders.getClickTime(),dbOrder.getClickTime());
    Assert.assertEquals(orders.getTkPaidTime(),dbOrder.getTkPaidTime());
    Assert.assertEquals(orders.getTbPaidTime(),dbOrder.getTbPaidTime());
    Assert.assertEquals(orders.getCreateTime(),dbOrder.getCreateTime());
    Assert.assertEquals(orders.getTkStatus(),dbOrder.getTkStatus());
    Assert.assertEquals(orders.getRelationId(),dbOrder.getRelationId());
    Assert.assertTrue(orders.getPubSharePreFee().compareTo(dbOrder.getPubSharePreFee()) == 0);
    Assert.assertTrue(orders.getTotalCommissionRate().compareTo(dbOrder.getTotalCommissionRate()) == 0);
    Assert.assertTrue(orders.getTotalCommissionFee().compareTo(dbOrder.getTotalCommissionFee()) == 0);
    Assert.assertEquals(orders.getRefundTag(),dbOrder.getRefundTag());
    Assert.assertTrue(orders.getPayPrice().compareTo(dbOrder.getPayPrice()) == 0);
    Assert.assertEquals(orders.getItemNum(),dbOrder.getItemNum());
    Assert.assertEquals(orders.getItemImg(),dbOrder.getItemImg());
    Assert.assertEquals(orders.getItemTitle(),dbOrder.getItemTitle());
    Assert.assertEquals(orders.getItemLink(),dbOrder.getItemLink());
    Assert.assertEquals(orders.getOrderType(),dbOrder.getOrderType());
    Assert.assertEquals(dbOrder.getUserUit(),Long.valueOf("100000"));
    Assert.assertTrue(dbOrder.getUserCommission().compareTo(new BigDecimal("1000")) == 0);
    Assert.assertEquals(dbOrder.getOneLevelUserUit(),Long.valueOf("0"));
    Assert.assertTrue(dbOrder.getOneLevelCommission().compareTo(new BigDecimal("0")) == 0);
    Assert.assertEquals(dbOrder.getTwoLevelUserUit(),Long.valueOf("0"));
    Assert.assertTrue(dbOrder.getTwoLevelCommission().compareTo(new BigDecimal("0")) == 0);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    logger.debug("incomeList is : {}",incomeList);
    Assert.assertEquals(1,incomeList.size());

    UserIncome u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal("1000"),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.UN_PAID);


    orders.setTkStatus(3L);
    acOrdersImporter.processNormalTkOrderRsponse(rsp);

    dbOrder = ordersMapper.selectByPrimaryKey(orders.getTradeId());
    Assert.assertEquals(Long.valueOf(3L),dbOrder.getTkStatus());

    incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
//    Assert.assertEquals(3,incomeList.size());

    u1 = getUserIncomeByUserId(1L,incomeList);
    compareUserIncome(u1,1L, new BigDecimal("1000"),null,null,orders.getTradeId(),orders.getTradeParentId(),orders.getTkStatus(),UserIncomeConstant.FREEZE);

  }

  /**
   * 拉新奖励 -- 订单userId不为null的情况下正常奖励
   */
  @Test
  public void processOrdersCheckAndAwardToUser_1(){
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    ordersList.add(orders);
    rsp.setData(ordersList);

    orders.setTkStatus(3L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList.size());
    logger.debug("incomeList is : {}",incomeList);
    // 拉新奖励 -- 第一个人 奖励0
    UserIncome userIncome = userIncomeMapper.selectUserIncomeByUserIdAndType(2L, 3);
    Assert.assertTrue(new BigDecimal("0.00").compareTo(userIncome.getMoney()) == 0);

    orders.setTkStatus(3L);
    orders.setTradeId("510202563513666666");
    orders.setRelationId(6L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList2 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList2.size());
    // 拉新奖励 -- 第二个人 奖励0
    UserIncome userIncome1 = userIncomeMapper.selectUserIncomeByTradeIdAndType(orders.getTradeId());
    Assert.assertTrue(new BigDecimal("0.00").compareTo(userIncome1.getMoney()) == 0);
    UserIncome u11 = getUserIncomeByUserId(1L,incomeList2);

    orders.setTkStatus(3L);
    orders.setTradeId("510202563513999999");
    orders.setRelationId(9L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList3 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList3.size());
    logger.debug("incomeList is : {}",incomeList3);
    // 拉新奖励 -- 第三个人 奖励0
    UserIncome userIncome3 = userIncomeMapper.selectUserIncomeByTradeIdAndType(orders.getTradeId());
    Assert.assertTrue(new BigDecimal("0.00").compareTo(userIncome3.getMoney()) == 0);


    orders.setTkStatus(3L);
    orders.setTradeId("510202563513777777");
    orders.setRelationId(7L);
    acOrdersImporter.processRelationTkOrderRsponse(rsp);

    List<UserIncome> incomeList4 = userIncomeMapper.selectUserIncomeByOrdersId(orders.getTradeId());
    Assert.assertEquals(4,incomeList4.size());
    logger.debug("incomeList is : {}",incomeList4);
    // 拉新奖励 -- 第四个人 奖励500集分宝
    UserIncome userIncome4 = userIncomeMapper.selectUserIncomeByTradeIdAndType(orders.getTradeId());
    logger.debug("userIncome4 is : {}",userIncome4);
    Assert.assertTrue(new BigDecimal("5.00").compareTo(userIncome4.getMoney()) == 0);
  }

  /**
   * 订单结算
   * 拉新奖励 -- 用户上级id为null，不会进行拉新奖励，拉新奖励记录为null
   */
  @Test
  public void processOrdersCheckAndAwardToUser_2(){
    TkOrderRsponse rsp  = new TkOrderRsponse();
    rsp.setHas_next(false);
    rsp.setPosition_index("---");

    List<Orders> ordersList = new ArrayList<>();

    // 创建初始化订单
    Orders orders = buildInitialOrders();
    ordersList.add(orders);
    rsp.setData(ordersList);

    orders.setTkStatus(3L);
    orders.setRelationId(8L);
    orders.setTradeId("510202563513888888");
    acOrdersImporter.processRelationTkOrderRsponse(rsp);
    // 查询上级的拉新收益记录为null
    UserIncome userIncome = userIncomeMapper.selectUserIncomeByTradeIdAndType(orders.getTradeId());
    Assert.assertNull(userIncome);
  }


//  @Test
//  public void importOrders(){
//    paidOrdersImporter.importOrders("2020-07-22 10:32:00","2020-07-22 10:32:59", TypeTkStatus.RELATION_PAID);
//    paidOrdersImporter.importOrders("2020-07-22 10:32:00","2020-07-22 10:32:59", TypeTkStatus.SPECIAL_PAID);
//    paidOrdersImporter.importOrders("2020-07-22 10:32:00","2020-07-22 10:32:59", TypeTkStatus.NORMAL_PAID);
//    acOrdersImporter.importOrders("2020-08-02 09:50:51","2020-08-02 09:55:51", TypeTkStatus.RELATION_PAID);
//    acOrdersImporter.importOrders("2020-08-02 09:50:51","2020-08-02 09:55:51", TypeTkStatus.SPECIAL_PAID);
//    acOrdersImporter.importOrders("2020-08-02 09:50:51","2020-08-02 09:55:51", TypeTkStatus.NORMAL_PAID);
//  }


  @Test
  public void importFifteenDayOrders() throws InterruptedException {
    LocalDateTime localDateTime = LocalDateTime.now();
    int year = localDateTime.getYear();
    int month = localDateTime.getMonthValue();
    int day = localDateTime.getDayOfMonth();
    LocalDateTime dateTime = LocalDateTime.of(year,month,day,16,30,0);
    String date = DateUtil.getFifteenDayBefore(dateTime);
    paidOrdersImporter.importPaidAndAccountingOrders("2020-10-01 00:00:00",localDateTime,TypeTkStatus.RELATION_PAID);
    paidOrdersImporter.importPaidAndAccountingOrders("2020-10-01 00:00:00",localDateTime,TypeTkStatus.SPECIAL_PAID);
    paidOrdersImporter.importPaidAndAccountingOrders("2020-10-01 00:00:00",localDateTime,TypeTkStatus.NORMAL_PAID);

    acOrdersImporter.importPaidAndAccountingOrders("2020-10-01 00:00:00",localDateTime,TypeTkStatus.RELATION_ACCOUNTING);
    acOrdersImporter.importPaidAndAccountingOrders("2020-10-01 00:00:00",localDateTime,TypeTkStatus.SPECIAL_ACCOUNTING);
    acOrdersImporter.importPaidAndAccountingOrders("2020-10-01 00:00:00",localDateTime,TypeTkStatus.NORMAL_ACCOUNTING);

    overdueOrdersImporter.importOverDueOrders("2020-10-01 00:00:00",dateTime, TypeTkStatus.RELATION_OVERDUE);
    overdueOrdersImporter.importOverDueOrders("2020-10-01 00:00:00",dateTime, TypeTkStatus.SPECIAL_OVERDUE);
    overdueOrdersImporter.importOverDueOrders("2020-10-01 00:00:00",dateTime, TypeTkStatus.NORMAL_OVERDUE);
  }


}