package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.UserJdPidMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.model.UserJdPid;
import com.alipay.rebate.shop.pojo.dingdanxia.ByUnionidPromotionTwo;
import com.alipay.rebate.shop.pojo.dingdanxia.PddConvertReq;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class UserJdPidHelperTest {

    @Resource
    UserMapper userMapper;
    @Resource
    UserJdPidMapper userJdPidMapper;
    @Resource
    UserJdPidHelper userJdPidHelper;

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        userJdPidMapper.deleteUserJdPidByUserId(1L);
        buildUser(1L,null,null,null,"134235324556",null);
    }

    @After
    public void after(){
        userMapper.deleteUserById(1L);
        userJdPidMapper.deleteUserJdPidByUserId(1L);
    }


    private User buildUser(
            Long userId,
            Long relationId , Long specialId,
            Long parentUserId, String phone,
            String taobaoUserId
    ){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName("mmm1");
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(relationId);
        user.setSpecialId(specialId);
        user.setParentUserId(parentUserId);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(taobaoUserId);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return user;
    }

    private ByUnionidPromotionTwo buildByUnionidPromotionTwo(){
        ByUnionidPromotionTwo byUnionidPromotionTwo = new ByUnionidPromotionTwo();
        byUnionidPromotionTwo.setMaterialId("100007926326");
        byUnionidPromotionTwo.setUnionId(1000141445L);
        return byUnionidPromotionTwo;
    }

    private PddConvertReq buildPddConvertReq(){
        PddConvertReq pddConvertReq = new PddConvertReq();
        pddConvertReq.setGoods_id_list("2574921541");
        return pddConvertReq;
    }

    /**
     * 生成用户京东pid
     * @throws IOException
     */
    @Test
    public void processGetByUnionidPromotionTwo() throws IOException {

        ByUnionidPromotionTwo byUnionidPromotionTwo = buildByUnionidPromotionTwo();
        userJdPidHelper.getByUnionidPromotionTwo(byUnionidPromotionTwo,1L);

        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(1L);
        Assert.assertEquals(Long.valueOf(1),userJdPid.getUserId());
        Assert.assertNotNull(userJdPid.getPositionId());

    }

    /**
     * 生成拼多多pid
     * @throws IOException
     */
    @Test
    public void processGetPddConvert() throws IOException {

        PddConvertReq pddConvertReq = buildPddConvertReq();
        userJdPidHelper.getPddConvert(pddConvertReq, 1L);

        String pid = userJdPidMapper.selectUserPddPidByUserId(1L);
        Assert.assertNotNull(pid);
    }

    /**
     * 判断拼多多Pid是否存在，不存在生成
     */
    @Test
    public void processGetPddPid_1() throws IOException {

        // 准备用户jd Pid在数据库
        processGetByUnionidPromotionTwo();

        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(1L);

        String pddPid = userJdPidHelper.getPddPid(userJdPid, 1L);
        Assert.assertNotNull(pddPid);

        String pid = userJdPidMapper.selectUserPddPidByUserId(1L);
        Assert.assertNotNull(pid);
    }

//    /**
//     * 京东，拼多多pid都不存在
//     */
//    @Test
//    public void processGetPddPid_2() throws IOException {
//        userJdPidHelper.getPddPid(null,1L);
//
//        UserJdPid userJdPid = userJdPidMapper.selectUserJdPidByUserId(1L);
//        Assert.assertNotNull(userJdPid.getPddPId());
//        Assert.assertEquals(Long.valueOf(1),userJdPid.getUserId());
//    }
}