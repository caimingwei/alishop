package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.*;
import com.alipay.rebate.shop.model.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class UserIncomeHelperTest {

    @Resource
    UserMapper userMapper;
    @Resource
    UserJdPidMapper userJdPidMapper;
    @Resource
    OrdersMapper ordersMapper;
    @Resource
    PddOrdersMapper pddOrdersmapper;
    @Resource
    JdOrdersMapper jdOrdersMapper;
    @Resource
    UserIncomeHelper userIncomeHelper;
    @Resource
    UserIncomeMapper userIncomeMapper;
    Logger logger = LoggerFactory.getLogger(UserIncomeHelperTest.class);

    @Before
    public void before(){
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        userIncomeMapper.deleteUserIncomeByUserId(2L);
        userJdPidMapper.deleteUserJdPidByUserId(1L);
        userJdPidMapper.deleteUserJdPidByUserId(2L);
        ordersMapper.deleteByPrimaryKey("510202563513111111");
        ordersMapper.deleteByPrimaryKey("510202563513222222");
        pddOrdersmapper.deletePddOrdersByOrderSn("200425-11111111111111");
        pddOrdersmapper.deletePddOrdersByOrderSn("200425-222222222222222");
        jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177250L);
        jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177777L);

        // 插入用户1
        User user = buildUser(1L,1L,1L,null,"18819258220","1111111111");
        userMapper.insertSelective(user);

        // 插入用户1
        User user2 = buildUser(2L,2L,2L,null,"18819258220","1111111111");
        userMapper.insertSelective(user2);

        UserJdPid userJdPid = buildUserJdPid(1L, 1L, "200000555550", "10390596_140370520");
        userJdPidMapper.insertSelective(userJdPid);

        UserJdPid userJdPid2 = buildUserJdPid(2L, 2L, "200000555555", "10390596_140370000");
        userJdPidMapper.insertSelective(userJdPid2);

    }

    @After
    public void after(){
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userIncomeMapper.deleteUserIncomeByUserId(1L);
        userIncomeMapper.deleteUserIncomeByUserId(2L);
        userJdPidMapper.deleteUserJdPidByUserId(1L);
        userJdPidMapper.deleteUserJdPidByUserId(2L);
        ordersMapper.deleteByPrimaryKey("510202563513111111");
        ordersMapper.deleteByPrimaryKey("510202563513222222");
        pddOrdersmapper.deletePddOrdersByOrderSn("200425-11111111111111");
        pddOrdersmapper.deletePddOrdersByOrderSn("200425-222222222222222");
        jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177250L);
        jdOrdersMapper.deleteJdOrdersByOrdersId(20298345177777L);
    }

    private User buildUser(
            Long userId,
            Long relationId , Long specialId,
            Long parentUserId, String phone,
            String taobaoUserId
    ){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName("mmm1");
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(relationId);
        user.setSpecialId(specialId);
        user.setParentUserId(parentUserId);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(taobaoUserId);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return user;
    }

    private UserJdPid buildUserJdPid(Long id, Long userId, String jdPid,String pddPid){
        UserJdPid userJdPid = new UserJdPid();
        userJdPid.setId(id);
        userJdPid.setAddTime(new Date());
        userJdPid.setUpdateTime(new Date());
        userJdPid.setUserId(userId);
        userJdPid.setPddPId(pddPid);
        userJdPid.setPositionId(jdPid);
        return userJdPid;
    }


    private Orders buildInitialOrders(){
        Orders orders = new Orders();
        orders.setTradeId("510202563513111111");
        orders.setAdzoneId(109205150467L);
        orders.setAdzoneName(" 渠道ID专属");
        orders.setClickTime("2020-4-27 00:50:0");
        orders.setTkPaidTime("2020-4-27 00:50:10");
        orders.setTbPaidTime("2020-4-27 00:50:10");
        orders.setCreateTime("2020-4-27 00:50:10");
        orders.setTkStatus(12L);
        orders.setRelationId(1L);
        orders.setPubSharePreFee(new BigDecimal("100"));
        orders.setTotalCommissionRate(new BigDecimal("10"));
        orders.setTotalCommissionFee(new BigDecimal("100"));
        orders.setRefundTag(0L);
        orders.setPayPrice(new BigDecimal("1000"));
        orders.setAlipayTotalPrice(new BigDecimal("1000"));
        orders.setItemNum(1L);
        orders.setItemImg("111");
        orders.setItemTitle("111");
        orders.setItemLink("111");
        orders.setOrderType("天猫");
        orders.setSiteId(620000087L);
        return orders;
    }

    private JdOrders buildJdOrders(){
        JdOrders jdOrders = new JdOrders();
        jdOrders.setFinishTime("2020-05-20 09:34:45");
        jdOrders.setOrderEmt(2);
        jdOrders.setOrderId(20298345177250L);
        jdOrders.setOrderTime("2020-05-17 08:45:50");
        jdOrders.setParentId(0l);
        jdOrders.setPayMonth("20191018");
        jdOrders.setPlus(0);
        jdOrders.setParentId(0l);
        jdOrders.setActualCosPrice(new BigDecimal("23"));
        jdOrders.setActualFee(new BigDecimal("2.28"));
        jdOrders.setCid1(1672l);
        jdOrders.setCid2(2599l);
        jdOrders.setCid3(13667l);
        jdOrders.setCommissionRate(new BigDecimal("11"));
        jdOrders.setCpActid(0l);
        jdOrders.setEstimateFee(new BigDecimal("2.28"));
        jdOrders.setEstimateCosPrice(new BigDecimal("23"));
        jdOrders.setFinalRate(new BigDecimal("90"));
        jdOrders.setPid("200000555550");
        jdOrders.setPositionId(200000555550L);
        jdOrders.setPopId(789438L);
        jdOrders.setPrice(new BigDecimal("23"));
        jdOrders.setSiteId(27916941095l);
        jdOrders.setSkuName("沉香倒流香佛系流云檀香香粒高山流水瀑布塔香卧室熏香艾草倒流香炉创意摆件沉香炉倒流 香薰 樱花 玻璃瓶装 送小荷花");
        jdOrders.setSkuNum(1l);
        jdOrders.setSkuReturnNum(0l);
        jdOrders.setSubSideRate(new BigDecimal("90"));
        jdOrders.setSubsidyRate(new BigDecimal("0"));
        jdOrders.setTraceType(2);
        jdOrders.setUnionRole(1);
        jdOrders.setUnionTrafficGroup(4);
        jdOrders.setUnionTag("00000100");
        jdOrders.setValidCode(16);
        jdOrders.setUnionId(1000521676L);
        jdOrders.setSkuinfoValidCode(16);
        return jdOrders;
    }

    private PddOrders buildPddOrders(){
        PddOrders  orders = new PddOrders();
        orders.setGoodsId(185928347L);
        orders.setGoodsName("美粮坊手抓饼50片-20片早餐灌煎饼面饼皮原味手抓饼批发家庭装");
        orders.setCpaNew(1);
        orders.setGoodsQuantity(1l);
        orders.setGoodsThumbnailUrl("http://t00img.yangkeduo.com/goods/images/2019-06-10/f5df7b940e11e653631e5532f2e17d2c.jpeg");
        orders.setOrderAmount(new BigDecimal("2.0"));
        orders.setOrderCreateTime("2020-05-16 22:30:45");
        orders.setOrderGroupSuccessTime("2020-05-20 12:23:34");
        orders.setOrderModifyAt("2020-05-20 12:23:34");
        orders.setOrderPayTime("2020-05-16 22:40:45");
        orders.setOrderStatusDesc("已支付");
        orders.setGoodsPrice(new BigDecimal("2.0"));
        orders.setOrderSn("200425-11111111111111");
        orders.setOrderStatus(0);
        orders.setOrderId("43ynqdW2p2JeU+n6cXX9pg==");
        orders.setPromotionAmount(new BigDecimal("0.1"));
        orders.setpId("10390596_140370520");
        orders.setPromotionRate(20l);
        orders.setCustomParameters(" ");
        return orders;
    }

    private UserIncome getUserIncome(List<UserIncome> userIncomes ,Long userId){
        for (UserIncome userIncome : userIncomes) {
            if (userIncome.getUserId() == userId){
                return userIncome;
            }
        }
        return null;
    }

    private void compareUserIncome(UserIncome userIncome,String tradeId,Integer type,Integer status,Long userId){
        Assert.assertEquals(userId,userIncome.getUserId());
        Assert.assertEquals(type,userIncome.getType());
        Assert.assertEquals(Integer.valueOf(2),userIncome.getAwardType());
        Assert.assertEquals(status,userIncome.getStatus());
        Assert.assertEquals(Integer.valueOf(0),userIncome.getRoadType());
        Assert.assertEquals(tradeId,userIncome.getTradeId());
        Assert.assertTrue(new BigDecimal("0.01").compareTo(userIncome.getMoney()) == 0);
    }

    @Test
    public void inserOrderCommissionIncome() {
        Orders orders = buildInitialOrders();
        BigDecimal money = new BigDecimal("0.01");
        userIncomeHelper.inserOrderCommissionIncome(orders,money,1L,0,false);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("510202563513111111");
        UserIncome userIncome = getUserIncome(userIncomes, 1L);
        compareUserIncome(userIncome,"510202563513111111",1,0,1L);


        Orders orders1 = buildInitialOrders();
        orders1.setTradeId("510202563513222222");
        BigDecimal money1 = new BigDecimal("0.01");
        userIncomeHelper.inserOrderCommissionIncome(orders1,money1,2L,0,true);
        List<UserIncome> userIncomes1 = userIncomeMapper.selectUserIncomeByOrdersId("510202563513222222");
        UserIncome userIncome1 = getUserIncome(userIncomes1, 2L);
        compareUserIncome(userIncome1,"510202563513222222",1,0,2L);

    }

    @Test
    public void insertPddOrderCommissionIncome() {
        PddOrders orders = buildPddOrders();
        BigDecimal money = new BigDecimal("0.01");
        userIncomeHelper.insertPddOrderCommissionIncome(orders,money,1L,0,false);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("200425-11111111111111");
        UserIncome userIncome = getUserIncome(userIncomes, 1L);
        compareUserIncome(userIncome,"200425-11111111111111",16,0,1L);

        PddOrders orders1 = buildPddOrders();
        orders1.setOrderSn("200425-222222222222222");
        orders.setpId("10390596_140370000");
        BigDecimal money1 = new BigDecimal("0.01");
        userIncomeHelper.insertPddOrderCommissionIncome(orders1,money1,2L,0,true);
        List<UserIncome> userIncomes1 = userIncomeMapper.selectUserIncomeByOrdersId("200425-222222222222222");
        UserIncome userIncome1 = getUserIncome(userIncomes1, 2L);
        compareUserIncome(userIncome1,"200425-222222222222222",16,0,2L);
    }

    @Test
    public void insertJdOrderCommissionIncome() {
        JdOrders jdOrders = buildJdOrders();
        BigDecimal money = new BigDecimal("0.01");
        userIncomeHelper.insertJdOrderCommissionIncome(jdOrders,money,1L,0,false);
        List<UserIncome> userIncomes = userIncomeMapper.selectUserIncomeByOrdersId("20298345177250");
        UserIncome userIncome = getUserIncome(userIncomes, 1L);
        compareUserIncome(userIncome,"20298345177250",17,0,1L);

        JdOrders jdOrders1 = buildJdOrders();
        jdOrders.setPid("200000555555");
        jdOrders.setPositionId(200000555555L);
        jdOrders1.setOrderId(20298345177777L);
        BigDecimal money1 = new BigDecimal("0.01");
        userIncomeHelper.insertJdOrderCommissionIncome(jdOrders1,money1,2L,0,true);
        List<UserIncome> userIncomes1 = userIncomeMapper.selectUserIncomeByOrdersId("20298345177777");
        UserIncome userIncome1 = getUserIncome(userIncomes1, 2L);
        compareUserIncome(userIncome1,"20298345177777",17,0,2L);
    }
}