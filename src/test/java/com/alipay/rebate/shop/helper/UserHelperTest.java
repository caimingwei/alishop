package com.alipay.rebate.shop.helper;

import com.alipay.rebate.shop.Application;
import com.alipay.rebate.shop.constants.UserContant;
import com.alipay.rebate.shop.dao.mapper.OrdersMapper;
import com.alipay.rebate.shop.dao.mapper.TaskSettingsMapper;
import com.alipay.rebate.shop.dao.mapper.UserMapper;
import com.alipay.rebate.shop.model.Orders;
import com.alipay.rebate.shop.model.TaskSettings;
import com.alipay.rebate.shop.model.User;
import com.alipay.rebate.shop.pojo.user.customer.PlusOrReduceVirMoneyReq;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration
public class UserHelperTest {
    @Resource
    UserMapper userMapper;
    @Resource
    TaskSettingsMapper taskSettingsMapper;
    @Resource
    OrdersMapper ordersMapper;
    @Resource
    UserHelper userHelper;
    Logger logger = LoggerFactory.getLogger(UserHelperTest.class);

    @Before
    public void before(){

        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userMapper.deleteUserById(3L);
        userMapper.deleteUserById(4L);

        User user = buildUser(1L, null, null, null, "13424533248", null);
        userMapper.insertSelective(user);

        // 有上级，上上级
        User user2 = buildUser(2L, null, null, 3L, "13424533248", null);
        userMapper.insertSelective(user2);
        // 有上级
        User user3 = buildUser(3L, null, null, 4L, "13424533248", null);
        userMapper.insertSelective(user3);
        // 有下级，下下级
        User user4 = buildUser(4L, null, null, null, "13424533248", null);
        userMapper.insertSelective(user4);

    }

    @After
    public void after(){
        userMapper.deleteUserById(1L);
        userMapper.deleteUserById(2L);
        userMapper.deleteUserById(3L);
        userMapper.deleteUserById(4L);

    }

    private User buildUser(
            Long userId,
            Long relationId , Long specialId,
            Long parentUserId, String phone,
            String taobaoUserId
    ){
        User user = new User();
        user.setUserId(userId);
        user.setUserNickName("mmm1");
        user.setUserStatus(UserContant.USER_STATUS_NORMAL);
        user.setIsAuth(UserContant.IS_AUTH);
        user.setRelationId(relationId);
        user.setSpecialId(specialId);
        user.setParentUserId(parentUserId);
        user.setTotalFansOrdersNum(0);
        user.setUserIntgeralTreasure(0L);
        user.setPhone(phone);
        user.setOrdersNum(0L);
        user.setUserGradeId(1);
        user.setUserIntgeral(0L);
        user.setUserAmount(new BigDecimal("0.00"));
        user.setUserIncome(new BigDecimal("0.00"));
        user.setUserCommision(new BigDecimal("0.00"));
        user.setTaobaoUserId(taobaoUserId);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return user;
    }


    private TaskSettings buildTaskSettings(Integer id, Integer num, Integer maxTimes,
                                           Integer subType, String name,Integer awardType){
        TaskSettings taskSettings = new TaskSettings();
        taskSettings.setId(id);
        taskSettings.setIsUse(1);
        taskSettings.setAwardNum(num);
        taskSettings.setAwardType(awardType);
        taskSettings.setMaxTimes(maxTimes);
        taskSettings.setSubType(subType);
        taskSettings.setType(1);
        taskSettings.setName(name);
        taskSettings.setCreateTime(new Date());
        taskSettings.setUpdateTime(new Date());
        return taskSettings;
    }

    private PlusOrReduceVirMoneyReq buildPlusOrReduceVirMoneyReq(Long num,Integer type,Integer awardType,boolean flag){
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq = new PlusOrReduceVirMoneyReq();
        plusOrReduceVirMoneyReq.setAwardType(awardType);
        plusOrReduceVirMoneyReq.setNum(num);
        plusOrReduceVirMoneyReq.setType(type);
        plusOrReduceVirMoneyReq.setFlag(flag);
        return plusOrReduceVirMoneyReq;
    }

    private Orders buildInitialOrders(Long userUit,Long oneLevelUit,Long twoLevelUit,Long userId,Long tkStatus){
        Orders orders = new Orders();
        orders.setTradeId("510202563513111111");
        orders.setAdzoneId(109205150467L);
        orders.setAdzoneName(" 渠道ID专属");
        orders.setClickTime("2020-4-27 00:50:0");
        orders.setTkPaidTime("2020-4-27 00:50:10");
        orders.setTbPaidTime("2020-4-27 00:50:10");
        orders.setCreateTime("2020-4-27 00:50:10");
        orders.setTkStatus(tkStatus);
        orders.setRelationId(1L);
        orders.setPubSharePreFee(new BigDecimal("100"));
        orders.setTotalCommissionRate(new BigDecimal("10"));
        orders.setTotalCommissionFee(new BigDecimal("100"));
        orders.setRefundTag(0L);
        orders.setPayPrice(new BigDecimal("1000"));
        orders.setAlipayTotalPrice(new BigDecimal("1000"));
        orders.setItemNum(1L);
        orders.setItemImg("111");
        orders.setItemTitle("111");
        orders.setItemLink("111");
        orders.setOrderType("天猫");
        orders.setSiteId(620000087L);
        orders.setUserId(userId);
        orders.setUserUit(userUit);
        orders.setOneLevelUserUit(oneLevelUit);
        orders.setTwoLevelUserUit(twoLevelUit);
        return orders;
    }


    @Test
    public void pluUserVirtualMoneyByType() {
        // 准备每日任务
        // 金额
        TaskSettings taskSettings = buildTaskSettings(1, 5, 1, 1, "绑定支付宝",2);
        userHelper.pluUserVirtualMoneyByType(taskSettings,1L);

        // 集分宝
        TaskSettings taskSettings1 = buildTaskSettings(2, 5, 1, 2, "微信授权",1);
        userHelper.pluUserVirtualMoneyByType(taskSettings1,1L);

        // 积分
        TaskSettings taskSettings3 = buildTaskSettings(4, 5, 1, 4, "计时浏览",3);
        userHelper.pluUserVirtualMoneyByType(taskSettings3,1L);

        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals(Long.valueOf(5),user.getUserIntgeralTreasure());
        Assert.assertEquals(Long.valueOf(5),user.getUserIntgeral());
        Assert.assertTrue(new BigDecimal("5.00").compareTo(user.getUserAmount()) == 0);
    }

    @Test
    public void pluUserVirtualMoneyAndUserIncomeByType() {
        // 积分
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq1 = buildPlusOrReduceVirMoneyReq(10L, 2, 3, true);
        userHelper.pluUserVirtualMoneyAndUserIncomeByType(plusOrReduceVirMoneyReq1,1L);

        // 集分宝
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq2 = buildPlusOrReduceVirMoneyReq(10L, 6, 2, true);
        userHelper.pluUserVirtualMoneyAndUserIncomeByType(plusOrReduceVirMoneyReq2,1L);

        // 金额
        PlusOrReduceVirMoneyReq plusOrReduceVirMoneyReq3 = buildPlusOrReduceVirMoneyReq(10L, 6, 1, true);
        userHelper.pluUserVirtualMoneyAndUserIncomeByType(plusOrReduceVirMoneyReq3,1L);

        User user = userMapper.selectById(1L);
        Assert.assertEquals(Long.valueOf(1),user.getUserId());
        Assert.assertEquals(Long.valueOf(10),user.getUserIntgeralTreasure());
        Assert.assertEquals(Long.valueOf(10),user.getUserIntgeral());
        Assert.assertTrue(new BigDecimal("10.00").compareTo(user.getUserAmount()) == 0);

    }

    @Test
    public void plusUserUit() {

        // 准备付款订单
        Orders orders1 = buildInitialOrders(340L, 30L, 4L, 4L, 12L);
        // 准备结算订单
        Orders orders2 = buildInitialOrders(340L, 30L, 4L, 4L, 3L);
        User user = userMapper.selectById(4L);
        // 没有上级
        userHelper.plusUserUit(orders2,orders1,user,null);
        User user1 = userMapper.selectById(4L);
        Assert.assertEquals(Long.valueOf(4),user1.getUserId());
        Assert.assertEquals(Long.valueOf(340),user1.getUserIntgeralTreasure());



        // 准备付款订单
        Orders orders3 = buildInitialOrders(340L, 30L, 4L, 3L, 12L);
        // 准备结算订单
        Orders orders4 = buildInitialOrders(340L, 30L, 4L, 3L, 3L);
        User user3 = userMapper.selectById(3L);
        User user2 = userMapper.selectById(4L);
        // 有上级
        userHelper.plusUserUit(orders4,orders3,user3,user2);
        User user4 = userMapper.selectById(3L);
        Assert.assertEquals(Long.valueOf(3),user4.getUserId());
        Assert.assertEquals(Long.valueOf(340),user4.getUserIntgeralTreasure());

        User user5 = userMapper.selectById(4L);
        Assert.assertEquals(Long.valueOf(4),user5.getUserId());
        Assert.assertEquals(Long.valueOf(370),user5.getUserIntgeralTreasure());



        // 准备付款订单
        Orders orders5 = buildInitialOrders(340L, 30L, 4L, 2L, 12L);
        // 准备结算订单
        Orders orders6 = buildInitialOrders(340L, 30L, 4L, 2L, 3L);
        User user6 = userMapper.selectById(2L);
        User user7 = userMapper.selectById(3L);
        // 有上上级
        userHelper.plusUserUit(orders6,orders5,user6,user7);
        User user10 = userMapper.selectById(2L);
        Assert.assertEquals(Long.valueOf(2),user10.getUserId());
        Assert.assertEquals(Long.valueOf(340),user10.getUserIntgeralTreasure());


        User user8 = userMapper.selectById(3L);
        Assert.assertEquals(Long.valueOf(3),user8.getUserId());
        Assert.assertEquals(Long.valueOf(370),user8.getUserIntgeralTreasure());

        User user9 = userMapper.selectById(4L);
        Assert.assertEquals(Long.valueOf(4),user9.getUserId());
        Assert.assertEquals(Long.valueOf(374),user9.getUserIntgeralTreasure());

    }
}